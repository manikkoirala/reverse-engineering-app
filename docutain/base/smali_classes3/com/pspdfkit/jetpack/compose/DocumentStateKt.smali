.class public final Lcom/pspdfkit/jetpack/compose/DocumentStateKt;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u001a)\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0007H\u0007\u00a2\u0006\u0002\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "rememberDocumentState",
        "Lcom/pspdfkit/jetpack/compose/DocumentState;",
        "documentUri",
        "Landroid/net/Uri;",
        "configuration",
        "Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;",
        "startingPage",
        "",
        "(Landroid/net/Uri;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;ILandroidx/compose/runtime/Composer;II)Lcom/pspdfkit/jetpack/compose/DocumentState;",
        "pspdfkit_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x8,
        0x0
    }
    xi = 0x30
.end annotation


# direct methods
.method public static final rememberDocumentState(Landroid/net/Uri;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;ILandroidx/compose/runtime/Composer;II)Lcom/pspdfkit/jetpack/compose/DocumentState;
    .locals 9

    const-string v0, "documentUri"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x6c676a1d

    invoke-interface {p3, v0}, Landroidx/compose/runtime/Composer;->startReplaceableGroup(I)V

    and-int/lit8 v1, p5, 0x2

    if-eqz v1, :cond_0

    .line 1
    new-instance p1, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    invoke-static {}, Landroidx/compose/ui/platform/AndroidCompositionLocals_androidKt;->getLocalContext()Landroidx/compose/runtime/ProvidableCompositionLocal;

    move-result-object v1

    .line 71
    invoke-interface {p3, v1}, Landroidx/compose/runtime/Composer;->consume(Landroidx/compose/runtime/CompositionLocal;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 72
    invoke-direct {p1, v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p1

    const-string v1, "Builder(LocalContext.current).build()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    and-int/lit8 p5, p5, 0x4

    const/4 v1, 0x0

    if-eqz p5, :cond_1

    const/4 p2, 0x0

    .line 73
    :cond_1
    invoke-static {}, Landroidx/compose/runtime/ComposerKt;->isTraceInProgress()Z

    move-result p5

    if-eqz p5, :cond_2

    const/4 p5, -0x1

    const-string v2, "com.pspdfkit.jetpack.compose.rememberDocumentState (DocumentState.kt:34)"

    .line 74
    invoke-static {v0, p4, p5, v2}, Landroidx/compose/runtime/ComposerKt;->traceEventStart(IIILjava/lang/String;)V

    :cond_2
    const/4 p4, 0x2

    new-array v2, p4, [Ljava/lang/Object;

    aput-object p0, v2, v1

    const/4 p4, 0x1

    aput-object p1, v2, p4

    sget-object p4, Lcom/pspdfkit/jetpack/compose/DocumentState;->Companion:Lcom/pspdfkit/jetpack/compose/DocumentState$Companion;

    invoke-virtual {p4}, Lcom/pspdfkit/jetpack/compose/DocumentState$Companion;->getSaver()Landroidx/compose/runtime/saveable/Saver;

    move-result-object v3

    new-instance v5, Lcom/pspdfkit/jetpack/compose/DocumentStateKt$rememberDocumentState$1;

    invoke-direct {v5, p0, p1, p2}, Lcom/pspdfkit/jetpack/compose/DocumentStateKt$rememberDocumentState$1;-><init>(Landroid/net/Uri;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;I)V

    const/4 v4, 0x0

    const/16 v7, 0x48

    const/4 v8, 0x4

    move-object v6, p3

    invoke-static/range {v2 .. v8}, Landroidx/compose/runtime/saveable/RememberSaveableKt;->rememberSaveable([Ljava/lang/Object;Landroidx/compose/runtime/saveable/Saver;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Landroidx/compose/runtime/Composer;II)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/jetpack/compose/DocumentState;

    invoke-static {}, Landroidx/compose/runtime/ComposerKt;->isTraceInProgress()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-static {}, Landroidx/compose/runtime/ComposerKt;->traceEventEnd()V

    :cond_3
    invoke-interface {p3}, Landroidx/compose/runtime/Composer;->endReplaceableGroup()V

    return-object p0
.end method

.class final Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$3;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt;->ImageDocumentView(Lcom/pspdfkit/jetpack/compose/DocumentState;Landroidx/compose/ui/Modifier;Landroidx/compose/runtime/Composer;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroidx/fragment/app/FragmentContainerView;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    k = 0x3
    mv = {
        0x1,
        0x8,
        0x0
    }
    xi = 0x30
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/ui/PdfUiFragment;

.field final synthetic b:Lcom/pspdfkit/jetpack/compose/DocumentState;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/PdfUiFragment;Lcom/pspdfkit/jetpack/compose/DocumentState;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$3;->a:Lcom/pspdfkit/ui/PdfUiFragment;

    iput-object p2, p0, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$3;->b:Lcom/pspdfkit/jetpack/compose/DocumentState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, Landroidx/fragment/app/FragmentContainerView;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$3;->invoke(Landroidx/fragment/app/FragmentContainerView;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroidx/fragment/app/FragmentContainerView;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$3;->a:Lcom/pspdfkit/ui/PdfUiFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/lifecycle/Lifecycle;->getCurrentState()Landroidx/lifecycle/Lifecycle$State;

    move-result-object p1

    sget-object v0, Landroidx/lifecycle/Lifecycle$State;->CREATED:Landroidx/lifecycle/Lifecycle$State;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/Lifecycle$State;->isAtLeast(Landroidx/lifecycle/Lifecycle$State;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$3;->a:Lcom/pspdfkit/ui/PdfUiFragment;

    iget-object v0, p0, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$3;->b:Lcom/pspdfkit/jetpack/compose/DocumentState;

    invoke-virtual {v0}, Lcom/pspdfkit/jetpack/compose/DocumentState;->getCurrentPage()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/PdfUi;->setPageIndex(I)V

    :cond_0
    return-void
.end method

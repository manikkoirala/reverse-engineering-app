.class public final Lcom/pspdfkit/jetpack/compose/DocumentState;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/jetpack/compose/DocumentState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\r\u0008\u0007\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B#\u0008\u0000\u0012\u0006\u0010\u000c\u001a\u00020\u0007\u0012\u0006\u0010\u0012\u001a\u00020\r\u0012\u0008\u0008\u0001\u0010\u0016\u001a\u00020\u0002\u00a2\u0006\u0004\u0008\u0017\u0010\u0018J\u001d\u0010\u0005\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0002H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0004\u0008\u0005\u0010\u0006R\u0017\u0010\u000c\u001a\u00020\u00078\u0006\u00a2\u0006\u000c\n\u0004\u0008\u0008\u0010\t\u001a\u0004\u0008\n\u0010\u000bR\u0017\u0010\u0012\u001a\u00020\r8\u0006\u00a2\u0006\u000c\n\u0004\u0008\u000e\u0010\u000f\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0015\u001a\u00020\u00028G\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0014\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/pspdfkit/jetpack/compose/DocumentState;",
        "",
        "",
        "page",
        "",
        "scrollToPage",
        "(ILkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "Landroid/net/Uri;",
        "a",
        "Landroid/net/Uri;",
        "getDocumentUri",
        "()Landroid/net/Uri;",
        "documentUri",
        "Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;",
        "b",
        "Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;",
        "getConfiguration",
        "()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;",
        "configuration",
        "getCurrentPage",
        "()I",
        "currentPage",
        "startingPage",
        "<init>",
        "(Landroid/net/Uri;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;I)V",
        "Companion",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# static fields
.field public static final $stable:I

.field public static final Companion:Lcom/pspdfkit/jetpack/compose/DocumentState$Companion;

.field private static final d:Landroidx/compose/runtime/saveable/Saver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/compose/runtime/saveable/Saver<",
            "Lcom/pspdfkit/jetpack/compose/DocumentState;",
            "*>;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

.field private final c:Landroidx/compose/runtime/MutableState;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/jetpack/compose/DocumentState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/jetpack/compose/DocumentState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/pspdfkit/jetpack/compose/DocumentState;->Companion:Lcom/pspdfkit/jetpack/compose/DocumentState$Companion;

    const/16 v0, 0x8

    sput v0, Lcom/pspdfkit/jetpack/compose/DocumentState;->$stable:I

    .line 1
    sget-object v0, Lcom/pspdfkit/jetpack/compose/DocumentState$Companion$Saver$1;->INSTANCE:Lcom/pspdfkit/jetpack/compose/DocumentState$Companion$Saver$1;

    sget-object v1, Lcom/pspdfkit/jetpack/compose/DocumentState$Companion$Saver$2;->INSTANCE:Lcom/pspdfkit/jetpack/compose/DocumentState$Companion$Saver$2;

    invoke-static {v0, v1}, Landroidx/compose/runtime/saveable/ListSaverKt;->listSaver(Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;)Landroidx/compose/runtime/saveable/Saver;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/jetpack/compose/DocumentState;->d:Landroidx/compose/runtime/saveable/Saver;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;I)V
    .locals 1

    const-string v0, "documentUri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/jetpack/compose/DocumentState;->a:Landroid/net/Uri;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/jetpack/compose/DocumentState;->b:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 4
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x2

    invoke-static {p1, p2, p3, p2}, Landroidx/compose/runtime/SnapshotStateKt;->mutableStateOf$default(Ljava/lang/Object;Landroidx/compose/runtime/SnapshotMutationPolicy;ILjava/lang/Object;)Landroidx/compose/runtime/MutableState;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/jetpack/compose/DocumentState;->c:Landroidx/compose/runtime/MutableState;

    return-void
.end method

.method public static final synthetic access$getSaver$cp()Landroidx/compose/runtime/saveable/Saver;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/jetpack/compose/DocumentState;->d:Landroidx/compose/runtime/saveable/Saver;

    return-object v0
.end method


# virtual methods
.method public final getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/jetpack/compose/DocumentState;->b:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    return-object v0
.end method

.method public final getCurrentPage()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/jetpack/compose/DocumentState;->c:Landroidx/compose/runtime/MutableState;

    .line 2
    invoke-interface {v0}, Landroidx/compose/runtime/State;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public final getDocumentUri()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/jetpack/compose/DocumentState;->a:Landroid/net/Uri;

    return-object v0
.end method

.method public final scrollToPage(ILkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/jetpack/compose/DocumentState;->c:Landroidx/compose/runtime/MutableState;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 48
    invoke-interface {p2, p1}, Landroidx/compose/runtime/MutableState;->setValue(Ljava/lang/Object;)V

    .line 49
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

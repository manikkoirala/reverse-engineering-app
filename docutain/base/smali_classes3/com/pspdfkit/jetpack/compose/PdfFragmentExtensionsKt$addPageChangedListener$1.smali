.class public final Lcom/pspdfkit/jetpack/compose/PdfFragmentExtensionsKt$addPageChangedListener$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/listeners/DocumentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/jetpack/compose/PdfFragmentExtensionsKt;->addPageChangedListener(Lcom/pspdfkit/ui/PdfFragment;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "com/pspdfkit/jetpack/compose/PdfFragmentExtensionsKt$addPageChangedListener$1",
        "Lcom/pspdfkit/listeners/DocumentListener;",
        "onPageChanged",
        "",
        "document",
        "Lcom/pspdfkit/document/PdfDocument;",
        "pageIndex",
        "",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
    xi = 0x30
.end annotation


# instance fields
.field final synthetic b:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/pspdfkit/jetpack/compose/PdfFragmentExtensionsKt$addPageChangedListener$1;->b:Lkotlin/jvm/functions/Function1;

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic onDocumentClick()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentClick(Lcom/pspdfkit/listeners/DocumentListener;)Z

    move-result v0

    return v0
.end method

.method public synthetic onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentLoadFailed(Lcom/pspdfkit/listeners/DocumentListener;Ljava/lang/Throwable;)V

    return-void
.end method

.method public synthetic onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentLoaded(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public synthetic onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSave(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result p1

    return p1
.end method

.method public synthetic onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSaveCancelled(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public synthetic onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSaveFailed(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V

    return-void
.end method

.method public synthetic onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSaved(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public synthetic onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentZoomed(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;IF)V

    return-void
.end method

.method public onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/jetpack/compose/PdfFragmentExtensionsKt$addPageChangedListener$1;->b:Lkotlin/jvm/functions/Function1;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public synthetic onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onPageClick(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    return p1
.end method

.method public synthetic onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onPageUpdated(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;I)V

    return-void
.end method

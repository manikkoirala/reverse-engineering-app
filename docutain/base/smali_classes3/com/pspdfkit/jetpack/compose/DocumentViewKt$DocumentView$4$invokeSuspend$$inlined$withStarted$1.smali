.class public final Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4$invokeSuspend$$inlined$withStarted$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u0002H\u0001\"\u0004\u0008\u0000\u0010\u0001H\n\u00a2\u0006\u0004\u0008\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "<anonymous>",
        "R",
        "invoke",
        "()Ljava/lang/Object;",
        "androidx/lifecycle/WithLifecycleStateKt$withStateAtLeastUnchecked$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x8,
        0x0
    }
    xi = 0x30
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/ui/PdfUiFragment;

.field final synthetic b:Lkotlinx/coroutines/CoroutineScope;

.field final synthetic c:Lcom/pspdfkit/jetpack/compose/DocumentState;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/ui/PdfUiFragment;Lkotlinx/coroutines/CoroutineScope;Lcom/pspdfkit/jetpack/compose/DocumentState;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4$invokeSuspend$$inlined$withStarted$1;->a:Lcom/pspdfkit/ui/PdfUiFragment;

    iput-object p2, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4$invokeSuspend$$inlined$withStarted$1;->b:Lkotlinx/coroutines/CoroutineScope;

    iput-object p3, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4$invokeSuspend$$inlined$withStarted$1;->c:Lcom/pspdfkit/jetpack/compose/DocumentState;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/Unit;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4$invokeSuspend$$inlined$withStarted$1;->a:Lcom/pspdfkit/ui/PdfUiFragment;

    invoke-interface {v0}, Lcom/pspdfkit/ui/PdfUi;->requirePdfFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    const-string v1, "fragment.requirePdfFragment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4$1$1;

    iget-object v2, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4$invokeSuspend$$inlined$withStarted$1;->b:Lkotlinx/coroutines/CoroutineScope;

    iget-object v3, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4$invokeSuspend$$inlined$withStarted$1;->c:Lcom/pspdfkit/jetpack/compose/DocumentState;

    invoke-direct {v1, v2, v3}, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4$1$1;-><init>(Lkotlinx/coroutines/CoroutineScope;Lcom/pspdfkit/jetpack/compose/DocumentState;)V

    invoke-static {v0, v1}, Lcom/pspdfkit/jetpack/compose/PdfFragmentExtensionsKt;->addPageChangedListener(Lcom/pspdfkit/ui/PdfFragment;Lkotlin/jvm/functions/Function1;)V

    .line 9
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.class public final Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u001f\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u0007\u00a2\u0006\u0002\u0010\u0006\u001a\u001f\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u0007\u00a2\u0006\u0002\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "ImageDocumentView",
        "",
        "imageUri",
        "Landroid/net/Uri;",
        "modifier",
        "Landroidx/compose/ui/Modifier;",
        "(Landroid/net/Uri;Landroidx/compose/ui/Modifier;Landroidx/compose/runtime/Composer;II)V",
        "documentState",
        "Lcom/pspdfkit/jetpack/compose/DocumentState;",
        "(Lcom/pspdfkit/jetpack/compose/DocumentState;Landroidx/compose/ui/Modifier;Landroidx/compose/runtime/Composer;II)V",
        "pspdfkit_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x8,
        0x0
    }
    xi = 0x30
.end annotation


# direct methods
.method public static final ImageDocumentView(Landroid/net/Uri;Landroidx/compose/ui/Modifier;Landroidx/compose/runtime/Composer;II)V
    .locals 7

    const-string v0, "imageUri"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, -0x1277631

    .line 1
    invoke-interface {p2, v0}, Landroidx/compose/runtime/Composer;->startRestartGroup(I)Landroidx/compose/runtime/Composer;

    move-result-object p2

    and-int/lit8 v1, p4, 0x2

    if-eqz v1, :cond_0

    .line 2
    sget-object p1, Landroidx/compose/ui/Modifier;->Companion:Landroidx/compose/ui/Modifier$Companion;

    :cond_0
    invoke-static {}, Landroidx/compose/runtime/ComposerKt;->isTraceInProgress()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, -0x1

    const-string v2, "com.pspdfkit.jetpack.compose.ImageDocumentView (ImageDocumentView.kt:41)"

    .line 3
    invoke-static {v0, p3, v1, v2}, Landroidx/compose/runtime/ComposerKt;->traceEventStart(IIILjava/lang/String;)V

    .line 4
    :cond_1
    new-instance v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    invoke-static {}, Landroidx/compose/ui/platform/AndroidCompositionLocals_androidKt;->getLocalContext()Landroidx/compose/runtime/ProvidableCompositionLocal;

    move-result-object v1

    .line 68
    invoke-interface {p2, v1}, Landroidx/compose/runtime/Composer;->consume(Landroidx/compose/runtime/CompositionLocal;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 69
    invoke-direct {v0, v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v0

    const-string v1, "Builder(LocalContext.current).build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-static {v0}, Lcom/pspdfkit/document/ImageDocumentLoader;->getDefaultImageDocumentActivityConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v2

    const-string v0, "getDefaultImageDocumentA\u2026on(activityConfiguration)"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/16 v5, 0x48

    const/4 v6, 0x4

    move-object v1, p0

    move-object v4, p2

    .line 72
    invoke-static/range {v1 .. v6}, Lcom/pspdfkit/jetpack/compose/DocumentStateKt;->rememberDocumentState(Landroid/net/Uri;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;ILandroidx/compose/runtime/Composer;II)Lcom/pspdfkit/jetpack/compose/DocumentState;

    move-result-object v0

    and-int/lit8 v1, p3, 0x70

    or-int/lit8 v1, v1, 0x8

    const/4 v2, 0x0

    .line 73
    invoke-static {v0, p1, p2, v1, v2}, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt;->ImageDocumentView(Lcom/pspdfkit/jetpack/compose/DocumentState;Landroidx/compose/ui/Modifier;Landroidx/compose/runtime/Composer;II)V

    invoke-static {}, Landroidx/compose/runtime/ComposerKt;->isTraceInProgress()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Landroidx/compose/runtime/ComposerKt;->traceEventEnd()V

    :cond_2
    invoke-interface {p2}, Landroidx/compose/runtime/Composer;->endRestartGroup()Landroidx/compose/runtime/ScopeUpdateScope;

    move-result-object p2

    if-nez p2, :cond_3

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$1;

    invoke-direct {v0, p0, p1, p3, p4}, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$1;-><init>(Landroid/net/Uri;Landroidx/compose/ui/Modifier;II)V

    invoke-interface {p2, v0}, Landroidx/compose/runtime/ScopeUpdateScope;->updateScope(Lkotlin/jvm/functions/Function2;)V

    :goto_0
    return-void
.end method

.method public static final ImageDocumentView(Lcom/pspdfkit/jetpack/compose/DocumentState;Landroidx/compose/ui/Modifier;Landroidx/compose/runtime/Composer;II)V
    .locals 9

    const-string v0, "documentState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x16eee885

    .line 74
    invoke-interface {p2, v0}, Landroidx/compose/runtime/Composer;->startRestartGroup(I)Landroidx/compose/runtime/Composer;

    move-result-object p2

    and-int/lit8 v1, p4, 0x2

    if-eqz v1, :cond_0

    .line 75
    sget-object p1, Landroidx/compose/ui/Modifier;->Companion:Landroidx/compose/ui/Modifier$Companion;

    :cond_0
    invoke-static {}, Landroidx/compose/runtime/ComposerKt;->isTraceInProgress()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, -0x1

    const-string v2, "com.pspdfkit.jetpack.compose.ImageDocumentView (ImageDocumentView.kt:63)"

    .line 76
    invoke-static {v0, p3, v1, v2}, Landroidx/compose/runtime/ComposerKt;->traceEventStart(IIILjava/lang/String;)V

    .line 77
    :cond_1
    invoke-static {}, Landroidx/compose/ui/platform/AndroidCompositionLocals_androidKt;->getLocalContext()Landroidx/compose/runtime/ProvidableCompositionLocal;

    move-result-object v0

    .line 120
    invoke-interface {p2, v0}, Landroidx/compose/runtime/Composer;->consume(Landroidx/compose/runtime/CompositionLocal;)Ljava/lang/Object;

    move-result-object v0

    .line 121
    instance-of v1, v0, Landroidx/fragment/app/FragmentActivity;

    if-eqz v1, :cond_2

    check-cast v0, Landroidx/fragment/app/FragmentActivity;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_8

    .line 122
    invoke-virtual {p0}, Lcom/pspdfkit/jetpack/compose/DocumentState;->getDocumentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/pspdfkit/jetpack/compose/DocumentState;->getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v2

    const v3, 0x1e7b2b64

    invoke-interface {p2, v3}, Landroidx/compose/runtime/Composer;->startReplaceableGroup(I)V

    .line 165
    invoke-interface {p2, v1}, Landroidx/compose/runtime/Composer;->changed(Ljava/lang/Object;)Z

    move-result v1

    invoke-interface {p2, v2}, Landroidx/compose/runtime/Composer;->changed(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v1, v2

    .line 167
    invoke-interface {p2}, Landroidx/compose/runtime/Composer;->rememberedValue()Ljava/lang/Object;

    move-result-object v2

    if-nez v1, :cond_3

    .line 168
    sget-object v1, Landroidx/compose/runtime/Composer;->Companion:Landroidx/compose/runtime/Composer$Companion;

    invoke-virtual {v1}, Landroidx/compose/runtime/Composer$Companion;->getEmpty()Ljava/lang/Object;

    move-result-object v1

    if-ne v2, v1, :cond_4

    .line 169
    :cond_3
    invoke-virtual {p0}, Lcom/pspdfkit/jetpack/compose/DocumentState;->getDocumentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pspdfkit/ui/PdfUiFragmentBuilder;->fromImageUri(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/ui/PdfUiFragmentBuilder;

    move-result-object v1

    .line 170
    invoke-virtual {p0}, Lcom/pspdfkit/jetpack/compose/DocumentState;->getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/PdfUiFragmentBuilder;->configuration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Lcom/pspdfkit/ui/PdfUiFragmentBuilder;

    move-result-object v1

    .line 171
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfUiFragmentBuilder;->build()Lcom/pspdfkit/ui/PdfUiFragment;

    move-result-object v2

    .line 216
    invoke-interface {p2, v2}, Landroidx/compose/runtime/Composer;->updateRememberedValue(Ljava/lang/Object;)V

    .line 217
    :cond_4
    invoke-interface {p2}, Landroidx/compose/runtime/Composer;->endReplaceableGroup()V

    const-string v1, "remember(documentState.d\u2026           .build()\n    }"

    .line 218
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, v2

    check-cast v7, Lcom/pspdfkit/ui/PdfUiFragment;

    .line 224
    sget-object v1, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$2;->INSTANCE:Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$2;

    new-instance v3, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$3;

    invoke-direct {v3, v7, p0}, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$3;-><init>(Lcom/pspdfkit/ui/PdfUiFragment;Lcom/pspdfkit/jetpack/compose/DocumentState;)V

    and-int/lit8 v2, p3, 0x70

    or-int/lit8 v5, v2, 0x6

    const/4 v6, 0x0

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v1 .. v6}, Landroidx/compose/ui/viewinterop/AndroidView_androidKt;->AndroidView(Lkotlin/jvm/functions/Function1;Landroidx/compose/ui/Modifier;Lkotlin/jvm/functions/Function1;Landroidx/compose/runtime/Composer;II)V

    const v1, 0x2e20b340

    .line 238
    invoke-interface {p2, v1}, Landroidx/compose/runtime/Composer;->startReplaceableGroup(I)V

    const v1, -0x1d58f75c

    .line 272
    invoke-interface {p2, v1}, Landroidx/compose/runtime/Composer;->startReplaceableGroup(I)V

    .line 274
    invoke-interface {p2}, Landroidx/compose/runtime/Composer;->rememberedValue()Ljava/lang/Object;

    move-result-object v1

    .line 275
    sget-object v2, Landroidx/compose/runtime/Composer;->Companion:Landroidx/compose/runtime/Composer$Companion;

    invoke-virtual {v2}, Landroidx/compose/runtime/Composer$Companion;->getEmpty()Ljava/lang/Object;

    move-result-object v2

    if-ne v1, v2, :cond_5

    .line 279
    sget-object v1, Lkotlin/coroutines/EmptyCoroutineContext;->INSTANCE:Lkotlin/coroutines/EmptyCoroutineContext;

    .line 280
    invoke-static {v1, p2}, Landroidx/compose/runtime/EffectsKt;->createCompositionCoroutineScope(Lkotlin/coroutines/CoroutineContext;Landroidx/compose/runtime/Composer;)Lkotlinx/coroutines/CoroutineScope;

    move-result-object v1

    .line 281
    new-instance v2, Landroidx/compose/runtime/CompositionScopedCoroutineScopeCanceller;

    invoke-direct {v2, v1}, Landroidx/compose/runtime/CompositionScopedCoroutineScopeCanceller;-><init>(Lkotlinx/coroutines/CoroutineScope;)V

    .line 284
    invoke-interface {p2, v2}, Landroidx/compose/runtime/Composer;->updateRememberedValue(Ljava/lang/Object;)V

    move-object v1, v2

    .line 285
    :cond_5
    invoke-interface {p2}, Landroidx/compose/runtime/Composer;->endReplaceableGroup()V

    .line 286
    check-cast v1, Landroidx/compose/runtime/CompositionScopedCoroutineScopeCanceller;

    .line 297
    invoke-virtual {v1}, Landroidx/compose/runtime/CompositionScopedCoroutineScopeCanceller;->getCoroutineScope()Lkotlinx/coroutines/CoroutineScope;

    move-result-object v4

    invoke-interface {p2}, Landroidx/compose/runtime/Composer;->endReplaceableGroup()V

    .line 298
    new-instance v8, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$4;

    const/4 v6, 0x0

    move-object v1, v8

    move-object v2, v0

    move-object v3, v7

    move-object v5, p0

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$4;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/ui/PdfUiFragment;Lkotlinx/coroutines/CoroutineScope;Lcom/pspdfkit/jetpack/compose/DocumentState;Lkotlin/coroutines/Continuation;)V

    const/16 v0, 0x48

    invoke-static {v7, v8, p2, v0}, Landroidx/compose/runtime/EffectsKt;->LaunchedEffect(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;Landroidx/compose/runtime/Composer;I)V

    invoke-static {}, Landroidx/compose/runtime/ComposerKt;->isTraceInProgress()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Landroidx/compose/runtime/ComposerKt;->traceEventEnd()V

    :cond_6
    invoke-interface {p2}, Landroidx/compose/runtime/Composer;->endRestartGroup()Landroidx/compose/runtime/ScopeUpdateScope;

    move-result-object p2

    if-nez p2, :cond_7

    goto :goto_1

    :cond_7
    new-instance v0, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$5;

    invoke-direct {v0, p0, p1, p3, p4}, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$5;-><init>(Lcom/pspdfkit/jetpack/compose/DocumentState;Landroidx/compose/ui/Modifier;II)V

    invoke-interface {p2, v0}, Landroidx/compose/runtime/ScopeUpdateScope;->updateScope(Lkotlin/jvm/functions/Function2;)V

    :goto_1
    return-void

    .line 299
    :cond_8
    new-instance p0, Lcom/pspdfkit/jetpack/compose/NonFragmentActivityException;

    invoke-direct {p0}, Lcom/pspdfkit/jetpack/compose/NonFragmentActivityException;-><init>()V

    throw p0
.end method

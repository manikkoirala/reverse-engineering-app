.class final Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/jetpack/compose/DocumentViewKt;->DocumentView(Lcom/pspdfkit/jetpack/compose/DocumentState;Landroidx/compose/ui/Modifier;Landroidx/compose/runtime/Composer;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    k = 0x3
    mv = {
        0x1,
        0x8,
        0x0
    }
    xi = 0x30
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.pspdfkit.jetpack.compose.DocumentViewKt$DocumentView$4"
    f = "DocumentView.kt"
    i = {}
    l = {
        0x73
    }
    m = "invokeSuspend"
    n = {}
    s = {}
.end annotation


# instance fields
.field a:I

.field final synthetic b:Landroidx/fragment/app/FragmentActivity;

.field final synthetic c:Lcom/pspdfkit/ui/PdfUiFragment;

.field final synthetic d:Lkotlinx/coroutines/CoroutineScope;

.field final synthetic e:Lcom/pspdfkit/jetpack/compose/DocumentState;


# direct methods
.method constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/ui/PdfUiFragment;Lkotlinx/coroutines/CoroutineScope;Lcom/pspdfkit/jetpack/compose/DocumentState;Lkotlin/coroutines/Continuation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Lcom/pspdfkit/ui/PdfUiFragment;",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lcom/pspdfkit/jetpack/compose/DocumentState;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->b:Landroidx/fragment/app/FragmentActivity;

    iput-object p2, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->c:Lcom/pspdfkit/ui/PdfUiFragment;

    iput-object p3, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->d:Lkotlinx/coroutines/CoroutineScope;

    iput-object p4, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->e:Lcom/pspdfkit/jetpack/compose/DocumentState;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p5}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    new-instance p1, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;

    iget-object v1, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->b:Landroidx/fragment/app/FragmentActivity;

    iget-object v2, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->c:Lcom/pspdfkit/ui/PdfUiFragment;

    iget-object v3, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->d:Lkotlinx/coroutines/CoroutineScope;

    iget-object v4, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->e:Lcom/pspdfkit/jetpack/compose/DocumentState;

    move-object v0, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/ui/PdfUiFragment;Lkotlinx/coroutines/CoroutineScope;Lcom/pspdfkit/jetpack/compose/DocumentState;Lkotlin/coroutines/Continuation;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->invoke(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 1
    iget v1, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->a:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->b:Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    .line 3
    sget v1, Lcom/pspdfkit/R$id;->pspdf__compose_fragment_container:I

    iget-object v3, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->c:Lcom/pspdfkit/ui/PdfUiFragment;

    invoke-virtual {p1, v1, v3}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    .line 4
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->c:Lcom/pspdfkit/ui/PdfUiFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    move-result-object v3

    const-string p1, "fragment.lifecycle"

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->c:Lcom/pspdfkit/ui/PdfUiFragment;

    iget-object v1, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->d:Lkotlinx/coroutines/CoroutineScope;

    iget-object v4, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->e:Lcom/pspdfkit/jetpack/compose/DocumentState;

    .line 22
    sget-object v5, Landroidx/lifecycle/Lifecycle$State;->STARTED:Landroidx/lifecycle/Lifecycle$State;

    .line 23
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getMain()Lkotlinx/coroutines/MainCoroutineDispatcher;

    move-result-object v6

    invoke-virtual {v6}, Lkotlinx/coroutines/MainCoroutineDispatcher;->getImmediate()Lkotlinx/coroutines/MainCoroutineDispatcher;

    move-result-object v6

    .line 24
    invoke-virtual {p0}, Lkotlin/coroutines/jvm/internal/ContinuationImpl;->getContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v7

    invoke-virtual {v6, v7}, Lkotlinx/coroutines/MainCoroutineDispatcher;->isDispatchNeeded(Lkotlin/coroutines/CoroutineContext;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 26
    invoke-virtual {v3}, Landroidx/lifecycle/Lifecycle;->getCurrentState()Landroidx/lifecycle/Lifecycle$State;

    move-result-object v8

    sget-object v9, Landroidx/lifecycle/Lifecycle$State;->DESTROYED:Landroidx/lifecycle/Lifecycle$State;

    if-eq v8, v9, :cond_2

    .line 27
    invoke-virtual {v3}, Landroidx/lifecycle/Lifecycle;->getCurrentState()Landroidx/lifecycle/Lifecycle$State;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/Enum;->compareTo(Ljava/lang/Enum;)I

    move-result v8

    if-ltz v8, :cond_3

    .line 28
    invoke-interface {p1}, Lcom/pspdfkit/ui/PdfUi;->requirePdfFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    const-string v0, "fragment.requirePdfFragment()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4$1$1;

    invoke-direct {v0, v1, v4}, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4$1$1;-><init>(Lkotlinx/coroutines/CoroutineScope;Lcom/pspdfkit/jetpack/compose/DocumentState;)V

    invoke-static {p1, v0}, Lcom/pspdfkit/jetpack/compose/PdfFragmentExtensionsKt;->addPageChangedListener(Lcom/pspdfkit/ui/PdfFragment;Lkotlin/jvm/functions/Function1;)V

    .line 36
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    goto :goto_0

    .line 45
    :cond_2
    new-instance p1, Landroidx/lifecycle/LifecycleDestroyedException;

    invoke-direct {p1}, Landroidx/lifecycle/LifecycleDestroyedException;-><init>()V

    throw p1

    .line 49
    :cond_3
    new-instance v8, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4$invokeSuspend$$inlined$withStarted$1;

    invoke-direct {v8, p1, v1, v4}, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4$invokeSuspend$$inlined$withStarted$1;-><init>(Lcom/pspdfkit/ui/PdfUiFragment;Lkotlinx/coroutines/CoroutineScope;Lcom/pspdfkit/jetpack/compose/DocumentState;)V

    iput v2, p0, Lcom/pspdfkit/jetpack/compose/DocumentViewKt$DocumentView$4;->a:I

    move-object v4, v5

    move v5, v7

    move-object v7, v8

    move-object v8, p0

    invoke-static/range {v3 .. v8}, Landroidx/lifecycle/WithLifecycleStateKt;->suspendWithStateAtLeastUnchecked(Landroidx/lifecycle/Lifecycle;Landroidx/lifecycle/Lifecycle$State;ZLkotlinx/coroutines/CoroutineDispatcher;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_4

    return-object v0

    .line 50
    :cond_4
    :goto_0
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

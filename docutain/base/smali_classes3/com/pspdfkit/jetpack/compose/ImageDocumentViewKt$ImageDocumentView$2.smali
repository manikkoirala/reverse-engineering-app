.class final Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$2;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt;->ImageDocumentView(Lcom/pspdfkit/jetpack/compose/DocumentState;Landroidx/compose/ui/Modifier;Landroidx/compose/runtime/Composer;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/content/Context;",
        "Landroidx/fragment/app/FragmentContainerView;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    k = 0x3
    mv = {
        0x1,
        0x8,
        0x0
    }
    xi = 0x30
.end annotation


# static fields
.field public static final INSTANCE:Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$2;

    invoke-direct {v0}, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$2;-><init>()V

    sput-object v0, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$2;->INSTANCE:Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Landroid/content/Context;)Landroidx/fragment/app/FragmentContainerView;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    new-instance v0, Landroidx/fragment/app/FragmentContainerView;

    invoke-direct {v0, p1}, Landroidx/fragment/app/FragmentContainerView;-><init>(Landroid/content/Context;)V

    .line 3
    sget p1, Lcom/pspdfkit/R$id;->pspdf__compose_fragment_container:I

    invoke-virtual {v0, p1}, Landroid/view/View;->setId(I)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/jetpack/compose/ImageDocumentViewKt$ImageDocumentView$2;->invoke(Landroid/content/Context;)Landroidx/fragment/app/FragmentContainerView;

    move-result-object p1

    return-object p1
.end method

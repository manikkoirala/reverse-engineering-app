.class public Lcom/pspdfkit/ui/PopupToolbar;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarViewItemClickedListener;,
        Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarItemClickedListener;
    }
.end annotation


# instance fields
.field private currentPageIndex:I

.field private currentX:F

.field private currentY:F

.field private lastOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field protected onPopupToolbarItemClickedListener:Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarItemClickedListener;

.field protected final pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

.field private final popupToolbarView:Lcom/pspdfkit/internal/un;

.field protected final popupWindow:Landroid/widget/PopupWindow;

.field private final reuseMatrix:Landroid/graphics/Matrix;


# direct methods
.method static bridge synthetic -$$Nest$fgetpopupToolbarView(Lcom/pspdfkit/ui/PopupToolbar;)Lcom/pspdfkit/internal/un;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PopupToolbar;->popupToolbarView:Lcom/pspdfkit/internal/un;

    return-object p0
.end method

.method public constructor <init>(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/PopupToolbar;->reuseMatrix:Landroid/graphics/Matrix;

    const/4 v0, 0x0

    .line 13
    iput v0, p0, Lcom/pspdfkit/ui/PopupToolbar;->currentX:F

    .line 14
    iput v0, p0, Lcom/pspdfkit/ui/PopupToolbar;->currentY:F

    .line 24
    iput-object p1, p0, Lcom/pspdfkit/ui/PopupToolbar;->pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

    .line 25
    new-instance v0, Lcom/pspdfkit/internal/un;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/un;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/PopupToolbar;->popupToolbarView:Lcom/pspdfkit/internal/un;

    .line 26
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PopupToolbar;->getViewId()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/view/View;->setId(I)V

    .line 27
    new-instance p1, Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarViewItemClickedListener;

    const/4 v1, 0x0

    invoke-direct {p1, p0, v1}, Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarViewItemClickedListener;-><init>(Lcom/pspdfkit/ui/PopupToolbar;Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarViewItemClickedListener-IA;)V

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/un;->setOnPopupToolbarViewItemClickedListener(Lcom/pspdfkit/internal/un$a;)V

    .line 28
    new-instance p1, Landroid/widget/PopupWindow;

    const/4 v1, -0x2

    invoke-direct {p1, v0, v1, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object p1, p0, Lcom/pspdfkit/ui/PopupToolbar;->popupWindow:Landroid/widget/PopupWindow;

    .line 30
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_Animation:I

    invoke-virtual {p1, v0}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    const/high16 v0, 0x41a00000    # 20.0f

    .line 31
    invoke-virtual {p1, v0}, Landroid/widget/PopupWindow;->setElevation(F)V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PopupToolbar;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    return-void
.end method

.method public getMenuItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PopupToolbar;->popupToolbarView:Lcom/pspdfkit/internal/un;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/un;->getMenuItems()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getViewId()I
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__popup_toolbar:I

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PopupToolbar;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method lambda$show$0$com-pspdfkit-ui-PopupToolbar(Landroid/graphics/PointF;III[ZLandroid/view/View;IIIIIIII)V
    .locals 11

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    sub-int v3, p13, p11

    sub-int v4, p14, p12

    sub-int v5, p9, p7

    sub-int v6, p10, p8

    .line 1
    iget v7, v1, Landroid/graphics/PointF;->x:F

    float-to-int v7, v7

    div-int/lit8 v8, v5, 0x2

    sub-int/2addr v7, v8

    sub-int v8, p3, v2

    sub-int/2addr v8, v5

    .line 2
    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-static {p2, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 3
    iget v1, v1, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    div-int/lit8 v8, v6, 0x2

    sub-int/2addr v1, v8

    sub-int v8, p4, v2

    sub-int/2addr v8, v6

    .line 4
    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 5
    iget-object v2, v0, Lcom/pspdfkit/ui/PopupToolbar;->popupWindow:Landroid/widget/PopupWindow;

    const/4 v8, -0x1

    const/4 v9, -0x1

    const/4 v10, 0x1

    move-object/from16 p6, v2

    move/from16 p7, v7

    move/from16 p8, v1

    move/from16 p9, v8

    move/from16 p10, v9

    move/from16 p11, v10

    invoke-virtual/range {p6 .. p11}, Landroid/widget/PopupWindow;->update(IIIIZ)V

    .line 21
    iget-object v2, v0, Lcom/pspdfkit/ui/PopupToolbar;->popupWindow:Landroid/widget/PopupWindow;

    sget v8, Lcom/pspdfkit/R$style;->PSPDFKit_Animation:I

    invoke-virtual {v2, v8}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    if-ne v3, v5, :cond_0

    if-eq v4, v6, :cond_2

    :cond_0
    const/4 v2, 0x0

    .line 23
    aget-boolean v3, p5, v2

    if-eqz v3, :cond_1

    .line 26
    iget-object v3, v0, Lcom/pspdfkit/ui/PopupToolbar;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v2}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 28
    :cond_1
    iget-object v3, v0, Lcom/pspdfkit/ui/PopupToolbar;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->dismiss()V

    .line 29
    iget-object v3, v0, Lcom/pspdfkit/ui/PopupToolbar;->popupWindow:Landroid/widget/PopupWindow;

    iget-object v4, v0, Lcom/pspdfkit/ui/PopupToolbar;->pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v4}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4, v2, v7, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    const/4 v1, 0x1

    .line 30
    aput-boolean v1, p5, v2

    :cond_2
    return-void
.end method

.method public onItemClicked(Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PopupToolbar;->onPopupToolbarItemClickedListener:Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarItemClickedListener;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarItemClickedListener;->onItemClicked(Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public setMenuItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PopupToolbar;->popupToolbarView:Lcom/pspdfkit/internal/un;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/un;->a()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PopupToolbar;->popupToolbarView:Lcom/pspdfkit/internal/un;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/un;->setMenuItems(Ljava/util/List;)V

    return-void
.end method

.method public setOnPopupToolbarItemClickedListener(Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarItemClickedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PopupToolbar;->onPopupToolbarItemClickedListener:Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarItemClickedListener;

    return-void
.end method

.method public show(IFF)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PopupToolbar;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 5
    :cond_0
    iput p1, p0, Lcom/pspdfkit/ui/PopupToolbar;->currentPageIndex:I

    .line 6
    iput p2, p0, Lcom/pspdfkit/ui/PopupToolbar;->currentX:F

    .line 7
    iput p3, p0, Lcom/pspdfkit/ui/PopupToolbar;->currentY:F

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/ui/PopupToolbar;->pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/ag;->getViewCoordinator()Lcom/pspdfkit/internal/xm;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/PopupToolbar;->reuseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/xm;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 11
    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1, p2, p3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 12
    new-instance p2, Landroid/graphics/PointF;

    invoke-direct {p2}, Landroid/graphics/PointF;-><init>()V

    .line 13
    iget-object p3, p0, Lcom/pspdfkit/ui/PopupToolbar;->reuseMatrix:Landroid/graphics/Matrix;

    invoke-static {p1, p2, p3}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/ui/PopupToolbar;->pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 18
    :cond_1
    iget-object p3, p0, Lcom/pspdfkit/ui/PopupToolbar;->pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

    .line 19
    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/pspdfkit/R$dimen;->pspdf__popup_toolbar_edge_padding:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p3

    float-to-int v3, p3

    .line 20
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    .line 21
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    const/4 p1, 0x1

    new-array v6, p1, [Z

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/ui/PopupToolbar;->lastOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    if-eqz p1, :cond_2

    .line 28
    iget-object p3, p0, Lcom/pspdfkit/ui/PopupToolbar;->popupToolbarView:Lcom/pspdfkit/internal/un;

    invoke-virtual {p3, p1}, Landroid/widget/LinearLayout;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 30
    :cond_2
    new-instance p1, Lcom/pspdfkit/ui/PopupToolbar$$ExternalSyntheticLambda0;

    move-object v0, p1

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/ui/PopupToolbar$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/PopupToolbar;Landroid/graphics/PointF;III[Z)V

    iput-object p1, p0, Lcom/pspdfkit/ui/PopupToolbar;->lastOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 73
    iget-object p3, p0, Lcom/pspdfkit/ui/PopupToolbar;->popupToolbarView:Lcom/pspdfkit/internal/un;

    invoke-virtual {p3, p1}, Landroid/widget/LinearLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 76
    iget-object p1, p0, Lcom/pspdfkit/ui/PopupToolbar;->pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    .line 77
    instance-of p3, p1, Lcom/pspdfkit/ui/PdfActivity;

    if-eqz p3, :cond_3

    check-cast p1, Lcom/pspdfkit/ui/PdfActivity;

    .line 78
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfActivity;->getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isImmersiveMode()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 81
    iget-object p1, p0, Lcom/pspdfkit/ui/PopupToolbar;->pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/ce;->b(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object p1

    .line 82
    iget p3, p2, Landroid/graphics/PointF;->y:F

    iget p1, p1, Landroid/graphics/Rect;->top:I

    int-to-float p1, p1

    sub-float/2addr p3, p1

    iput p3, p2, Landroid/graphics/PointF;->y:F

    .line 86
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/ui/PopupToolbar;->popupWindow:Landroid/widget/PopupWindow;

    sget p3, Lcom/pspdfkit/R$style;->PSPDFKit_Animation:I

    invoke-virtual {p1, p3}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 87
    iget-object p1, p0, Lcom/pspdfkit/ui/PopupToolbar;->popupWindow:Landroid/widget/PopupWindow;

    iget-object p3, p0, Lcom/pspdfkit/ui/PopupToolbar;->pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object p3

    iget v0, p2, Landroid/graphics/PointF;->x:F

    float-to-int v0, v0

    iget p2, p2, Landroid/graphics/PointF;->y:F

    float-to-int p2, p2

    const/4 v1, 0x0

    invoke-virtual {p1, p3, v1, v0, p2}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    return-void
.end method

.method public showAgain()V
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/PopupToolbar;->currentPageIndex:I

    iget v1, p0, Lcom/pspdfkit/ui/PopupToolbar;->currentX:F

    iget v2, p0, Lcom/pspdfkit/ui/PopupToolbar;->currentY:F

    invoke-virtual {p0, v0, v1, v2}, Lcom/pspdfkit/ui/PopupToolbar;->show(IFF)V

    return-void
.end method

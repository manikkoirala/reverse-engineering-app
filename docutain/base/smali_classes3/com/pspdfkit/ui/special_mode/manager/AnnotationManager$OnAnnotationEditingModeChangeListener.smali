.class public interface abstract Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnAnnotationEditingModeChangeListener"
.end annotation


# virtual methods
.method public abstract onChangeAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
.end method

.method public abstract onEnterAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
.end method

.method public abstract onExitAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
.end method

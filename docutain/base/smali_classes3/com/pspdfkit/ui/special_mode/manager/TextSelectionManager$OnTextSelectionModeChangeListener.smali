.class public interface abstract Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionModeChangeListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnTextSelectionModeChangeListener"
.end annotation


# virtual methods
.method public abstract onEnterTextSelectionMode(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V
.end method

.method public abstract onExitTextSelectionMode(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V
.end method

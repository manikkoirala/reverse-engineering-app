.class public interface abstract Lcom/pspdfkit/ui/special_mode/manager/FormManager;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;,
        Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;,
        Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;,
        Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;,
        Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;,
        Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;
    }
.end annotation


# virtual methods
.method public abstract addOnFormElementClickedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;)V
.end method

.method public abstract addOnFormElementDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;)V
.end method

.method public abstract addOnFormElementEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V
.end method

.method public abstract addOnFormElementSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V
.end method

.method public abstract addOnFormElementUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;)V
.end method

.method public abstract addOnFormElementViewUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;)V
.end method

.method public abstract removeOnFormElementClickedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;)V
.end method

.method public abstract removeOnFormElementDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;)V
.end method

.method public abstract removeOnFormElementEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V
.end method

.method public abstract removeOnFormElementSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V
.end method

.method public abstract removeOnFormElementUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;)V
.end method

.method public abstract removeOnFormElementViewUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;)V
.end method

.class public interface abstract Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;,
        Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;,
        Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;,
        Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;,
        Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;
    }
.end annotation


# virtual methods
.method public abstract addOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V
.end method

.method public abstract addOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V
.end method

.method public abstract addOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V
.end method

.method public abstract addOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V
.end method

.method public abstract addOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V
.end method

.method public abstract addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
.end method

.method public abstract removeOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V
.end method

.method public abstract removeOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V
.end method

.method public abstract removeOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V
.end method

.method public abstract removeOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V
.end method

.method public abstract removeOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V
.end method

.method public abstract removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
.end method

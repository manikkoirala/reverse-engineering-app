.class public interface abstract Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnContentEditingContentChangeListener"
.end annotation


# virtual methods
.method public abstract onContentChange(Ljava/util/UUID;)V
.end method

.method public abstract onContentSelectionChange(Ljava/util/UUID;IILcom/pspdfkit/internal/jt;Z)V
.end method

.method public abstract onFinishEditingContentBlock(Ljava/util/UUID;)V
.end method

.method public abstract onStartEditingContentBlock(Ljava/util/UUID;)V
.end method

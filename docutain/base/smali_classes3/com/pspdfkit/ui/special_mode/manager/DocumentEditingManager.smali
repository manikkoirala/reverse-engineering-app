.class public interface abstract Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingPageSelectionChangeListener;,
        Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingModeChangeListener;
    }
.end annotation


# virtual methods
.method public abstract addOnDocumentEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingModeChangeListener;)V
.end method

.method public abstract addOnDocumentEditingPageSelectionChangeListener(Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingPageSelectionChangeListener;)V
.end method

.method public abstract removeOnDocumentEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingModeChangeListener;)V
.end method

.method public abstract removeOnDocumentEditingPageSelectionChangeListener(Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingPageSelectionChangeListener;)V
.end method

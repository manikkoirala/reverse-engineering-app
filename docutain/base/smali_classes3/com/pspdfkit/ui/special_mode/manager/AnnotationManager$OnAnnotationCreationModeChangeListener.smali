.class public interface abstract Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnAnnotationCreationModeChangeListener"
.end annotation


# virtual methods
.method public abstract onChangeAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
.end method

.method public abstract onEnterAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
.end method

.method public abstract onExitAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
.end method

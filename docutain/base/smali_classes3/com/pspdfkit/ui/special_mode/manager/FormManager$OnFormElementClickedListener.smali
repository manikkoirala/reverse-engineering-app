.class public interface abstract Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/special_mode/manager/FormManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnFormElementClickedListener"
.end annotation


# virtual methods
.method public abstract isFormElementClickable(Lcom/pspdfkit/forms/FormElement;)Z
.end method

.method public abstract onFormElementClicked(Lcom/pspdfkit/forms/FormElement;)Z
.end method

.class public interface abstract Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnAnnotationSelectedListener"
.end annotation


# virtual methods
.method public abstract onAnnotationSelected(Lcom/pspdfkit/annotations/Annotation;Z)V
.end method

.method public abstract onPrepareAnnotationSelection(Lcom/pspdfkit/ui/special_mode/controller/AnnotationSelectionController;Lcom/pspdfkit/annotations/Annotation;Z)Z
.end method

.class public final synthetic Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController$-CC;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public static $default$isRedoEnabled(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)Z
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/undo/UndoManager;->canRedo()Z

    move-result v0

    return v0
.end method

.method public static $default$isUndoEnabled(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)Z
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/undo/UndoManager;->canUndo()Z

    move-result v0

    return v0
.end method

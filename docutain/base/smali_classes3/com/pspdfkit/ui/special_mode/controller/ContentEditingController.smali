.class public interface abstract Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;


# virtual methods
.method public abstract bindContentEditingInspectorController(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;)V
.end method

.method public abstract clearContentEditing()V
.end method

.method public abstract displayColorPicker(Lcom/pspdfkit/internal/jt;)V
.end method

.method public abstract displayFontNamesSheet(Lcom/pspdfkit/internal/jt;)V
.end method

.method public abstract displayFontSizesSheet(Lcom/pspdfkit/internal/jt;)V
.end method

.method public abstract finishContentEditingSession()V
.end method

.method public abstract finishContentEditingSession(Z)V
.end method

.method public abstract getActiveContentEditingStylingItem()Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;
.end method

.method public abstract getContentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;
.end method

.method public abstract getCurrentFormatter()Lcom/pspdfkit/internal/h6;
.end method

.method public abstract getCurrentStyleInfo()Lcom/pspdfkit/internal/jt;
.end method

.method public abstract getUndoManager()Lcom/pspdfkit/undo/UndoManager;
.end method

.method public abstract hasUnsavedChanges()Z
.end method

.method public abstract isBoldStyleButtonEnabled(Lcom/pspdfkit/internal/jt;)Z
.end method

.method public abstract isClearContentEditingEnabled()Z
.end method

.method public abstract isItalicStyleButtonEnabled(Lcom/pspdfkit/internal/jt;)Z
.end method

.method public abstract isRedoEnabled()Z
.end method

.method public abstract isSaveEnabled()Z
.end method

.method public abstract isUndoEnabled()Z
.end method

.method public abstract unbindContentEditingInspectorController()V
.end method

.class public interface abstract Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract hasAnnotationInspector()Z
.end method

.method public abstract hideAnnotationInspector(Z)V
.end method

.method public abstract isAnnotationInspectorVisible()Z
.end method

.method public abstract showAnnotationInspector(Z)V
.end method

.method public abstract toggleAnnotationInspector(Z)V
.end method

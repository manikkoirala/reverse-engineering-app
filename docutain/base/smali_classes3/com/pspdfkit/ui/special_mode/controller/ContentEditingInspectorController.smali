.class public interface abstract Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract bindContentEditingController(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V
.end method

.method public abstract displayColorPicker(ZLcom/pspdfkit/internal/jt;)V
.end method

.method public abstract displayFontNamesSheet(ZLjava/util/List;Lcom/pspdfkit/internal/jt;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/db;",
            ">;",
            "Lcom/pspdfkit/internal/jt;",
            ")V"
        }
    .end annotation
.end method

.method public abstract displayFontSizesSheet(ZLcom/pspdfkit/internal/jt;)V
.end method

.method public abstract isContentEditingInspectorVisible()Z
.end method

.method public abstract onRestoreInstanceState(Landroid/os/Bundle;)V
.end method

.method public abstract onSaveInstanceState(Landroid/os/Bundle;)V
.end method

.method public abstract unbindContentEditingController()V
.end method

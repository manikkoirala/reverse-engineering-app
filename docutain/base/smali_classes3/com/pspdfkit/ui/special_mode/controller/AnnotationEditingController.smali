.class public interface abstract Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;


# virtual methods
.method public abstract bindAnnotationInspectorController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;)V
.end method

.method public abstract deleteCurrentlySelectedAnnotation()V
.end method

.method public abstract enterAudioPlaybackMode()V
.end method

.method public abstract enterAudioRecordingMode()V
.end method

.method public abstract getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;
.end method

.method public abstract getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;
.end method

.method public abstract getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;
.end method

.method public abstract recordAnnotationZIndexEdit(Lcom/pspdfkit/annotations/Annotation;II)V
.end method

.method public abstract saveCurrentlySelectedAnnotation()V
.end method

.method public abstract shouldDisplayPicker()Z
.end method

.method public abstract shouldDisplayPlayAudioButton()Z
.end method

.method public abstract shouldDisplayRecordAudioButton()Z
.end method

.method public abstract showAnnotationEditor(Lcom/pspdfkit/annotations/Annotation;)V
.end method

.method public abstract showEditedAnnotationPositionOnThePage(I)V
.end method

.method public abstract startRecording()V
.end method

.method public abstract stopRecording()V
.end method

.method public abstract toggleAnnotationInspector()V
.end method

.method public abstract unbindAnnotationInspectorController()V
.end method

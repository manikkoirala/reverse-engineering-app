.class public final enum Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum CAMERA:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum CIRCLE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum ERASER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum FREETEXT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum FREETEXT_CALLOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum HIGHLIGHT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum IMAGE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum INSTANT_COMMENT_MARKER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum INSTANT_HIGHLIGHT_COMMENT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum LINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum MAGIC_INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum MEASUREMENT_AREA_ELLIPSE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum MEASUREMENT_AREA_POLYGON:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum MEASUREMENT_AREA_RECT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum MEASUREMENT_DISTANCE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum MEASUREMENT_PERIMETER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum NONE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum POLYGON:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum POLYLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum REDACTION:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum SIGNATURE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum SOUND:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum SQUARE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum SQUIGGLY:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum STAMP:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum STRIKEOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field public static final enum UNDERLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;


# instance fields
.field private final annotationType:Lcom/pspdfkit/annotations/AnnotationType;


# direct methods
.method static constructor <clinit>()V
    .locals 32

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->NONE:Lcom/pspdfkit/annotations/AnnotationType;

    const-string v2, "NONE"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NONE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 4
    new-instance v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->HIGHLIGHT:Lcom/pspdfkit/annotations/AnnotationType;

    const-string v5, "HIGHLIGHT"

    const/4 v6, 0x1

    invoke-direct {v2, v5, v6, v4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->HIGHLIGHT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 6
    new-instance v5, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v7, Lcom/pspdfkit/annotations/AnnotationType;->STRIKEOUT:Lcom/pspdfkit/annotations/AnnotationType;

    const-string v8, "STRIKEOUT"

    const/4 v9, 0x2

    invoke-direct {v5, v8, v9, v7}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v5, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->STRIKEOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 8
    new-instance v7, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v8, Lcom/pspdfkit/annotations/AnnotationType;->UNDERLINE:Lcom/pspdfkit/annotations/AnnotationType;

    const-string v10, "UNDERLINE"

    const/4 v11, 0x3

    invoke-direct {v7, v10, v11, v8}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v7, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->UNDERLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 11
    new-instance v8, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v10, Lcom/pspdfkit/annotations/AnnotationType;->SQUIGGLY:Lcom/pspdfkit/annotations/AnnotationType;

    const-string v12, "SQUIGGLY"

    const/4 v13, 0x4

    invoke-direct {v8, v12, v13, v10}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v8, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SQUIGGLY:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 13
    new-instance v10, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v12, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    const-string v14, "FREETEXT"

    const/4 v15, 0x5

    invoke-direct {v10, v14, v15, v12}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v10, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->FREETEXT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 15
    new-instance v14, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-string v15, "FREETEXT_CALLOUT"

    const/4 v13, 0x6

    invoke-direct {v14, v15, v13, v12}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v14, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->FREETEXT_CALLOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 18
    new-instance v12, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v15, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    const-string v13, "INK"

    const/4 v11, 0x7

    invoke-direct {v12, v13, v11, v15}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v12, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 20
    new-instance v13, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-string v11, "MAGIC_INK"

    const/16 v9, 0x8

    invoke-direct {v13, v11, v9, v15}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v13, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MAGIC_INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 22
    new-instance v11, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-string v9, "SIGNATURE"

    const/16 v6, 0x9

    invoke-direct {v11, v9, v6, v15}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v11, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SIGNATURE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 25
    new-instance v9, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v15, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    const-string v6, "NOTE"

    const/16 v3, 0xa

    invoke-direct {v9, v6, v3, v15}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v9, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 28
    new-instance v6, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->LINE:Lcom/pspdfkit/annotations/AnnotationType;

    move-object/from16 v16, v9

    const-string v9, "LINE"

    move-object/from16 v17, v11

    const/16 v11, 0xb

    invoke-direct {v6, v9, v11, v3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v6, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->LINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 30
    new-instance v9, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v11, Lcom/pspdfkit/annotations/AnnotationType;->SQUARE:Lcom/pspdfkit/annotations/AnnotationType;

    move-object/from16 v18, v6

    const-string v6, "SQUARE"

    move-object/from16 v19, v13

    const/16 v13, 0xc

    invoke-direct {v9, v6, v13, v11}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v9, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SQUARE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 32
    new-instance v6, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v13, Lcom/pspdfkit/annotations/AnnotationType;->CIRCLE:Lcom/pspdfkit/annotations/AnnotationType;

    move-object/from16 v20, v9

    const-string v9, "CIRCLE"

    move-object/from16 v21, v12

    const/16 v12, 0xd

    invoke-direct {v6, v9, v12, v13}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v6, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->CIRCLE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 34
    new-instance v9, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v12, Lcom/pspdfkit/annotations/AnnotationType;->POLYGON:Lcom/pspdfkit/annotations/AnnotationType;

    move-object/from16 v22, v6

    const-string v6, "POLYGON"

    move-object/from16 v23, v14

    const/16 v14, 0xe

    invoke-direct {v9, v6, v14, v12}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v9, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->POLYGON:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 36
    new-instance v6, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v14, Lcom/pspdfkit/annotations/AnnotationType;->POLYLINE:Lcom/pspdfkit/annotations/AnnotationType;

    move-object/from16 v24, v9

    const-string v9, "POLYLINE"

    move-object/from16 v25, v10

    const/16 v10, 0xf

    invoke-direct {v6, v9, v10, v14}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v6, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->POLYLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 39
    new-instance v9, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-string v10, "MEASUREMENT_DISTANCE"

    move-object/from16 v26, v6

    const/16 v6, 0x10

    invoke-direct {v9, v10, v6, v3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v9, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_DISTANCE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 41
    new-instance v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-string v10, "MEASUREMENT_PERIMETER"

    const/16 v6, 0x11

    invoke-direct {v3, v10, v6, v14}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_PERIMETER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 43
    new-instance v10, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-string v14, "MEASUREMENT_AREA_POLYGON"

    const/16 v6, 0x12

    invoke-direct {v10, v14, v6, v12}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v10, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_POLYGON:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 45
    new-instance v12, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-string v14, "MEASUREMENT_AREA_ELLIPSE"

    const/16 v6, 0x13

    invoke-direct {v12, v14, v6, v13}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v12, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_ELLIPSE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 47
    new-instance v13, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-string v14, "MEASUREMENT_AREA_RECT"

    const/16 v6, 0x14

    invoke-direct {v13, v14, v6, v11}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v13, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_RECT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 50
    new-instance v11, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v14, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    const-string v6, "STAMP"

    move-object/from16 v27, v13

    const/16 v13, 0x15

    invoke-direct {v11, v6, v13, v14}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v11, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->STAMP:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 52
    new-instance v6, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-string v13, "IMAGE"

    move-object/from16 v28, v11

    const/16 v11, 0x16

    invoke-direct {v6, v13, v11, v14}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v6, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->IMAGE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 54
    new-instance v11, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-string v13, "CAMERA"

    move-object/from16 v29, v6

    const/16 v6, 0x17

    invoke-direct {v11, v13, v6, v14}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v11, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->CAMERA:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 57
    new-instance v6, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v13, Lcom/pspdfkit/annotations/AnnotationType;->SOUND:Lcom/pspdfkit/annotations/AnnotationType;

    const-string v14, "SOUND"

    move-object/from16 v30, v11

    const/16 v11, 0x18

    invoke-direct {v6, v14, v11, v13}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v6, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SOUND:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 60
    new-instance v11, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-string v13, "ERASER"

    const/16 v14, 0x19

    invoke-direct {v11, v13, v14, v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v11, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->ERASER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 62
    new-instance v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v13, Lcom/pspdfkit/annotations/AnnotationType;->REDACT:Lcom/pspdfkit/annotations/AnnotationType;

    const-string v14, "REDACTION"

    move-object/from16 v31, v11

    const/16 v11, 0x1a

    invoke-direct {v1, v14, v11, v13}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->REDACTION:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 65
    new-instance v11, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-string v13, "INSTANT_COMMENT_MARKER"

    const/16 v14, 0x1b

    invoke-direct {v11, v13, v14, v15}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v11, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INSTANT_COMMENT_MARKER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 67
    new-instance v13, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-string v14, "INSTANT_HIGHLIGHT_COMMENT"

    const/16 v15, 0x1c

    invoke-direct {v13, v14, v15, v4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V

    sput-object v13, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INSTANT_HIGHLIGHT_COMMENT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/16 v4, 0x1d

    new-array v4, v4, [Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/4 v14, 0x0

    aput-object v0, v4, v14

    const/4 v0, 0x1

    aput-object v2, v4, v0

    const/4 v0, 0x2

    aput-object v5, v4, v0

    const/4 v0, 0x3

    aput-object v7, v4, v0

    const/4 v0, 0x4

    aput-object v8, v4, v0

    const/4 v0, 0x5

    aput-object v25, v4, v0

    const/4 v0, 0x6

    aput-object v23, v4, v0

    const/4 v0, 0x7

    aput-object v21, v4, v0

    const/16 v0, 0x8

    aput-object v19, v4, v0

    const/16 v0, 0x9

    aput-object v17, v4, v0

    const/16 v0, 0xa

    aput-object v16, v4, v0

    const/16 v0, 0xb

    aput-object v18, v4, v0

    const/16 v0, 0xc

    aput-object v20, v4, v0

    const/16 v0, 0xd

    aput-object v22, v4, v0

    const/16 v0, 0xe

    aput-object v24, v4, v0

    const/16 v0, 0xf

    aput-object v26, v4, v0

    const/16 v0, 0x10

    aput-object v9, v4, v0

    const/16 v0, 0x11

    aput-object v3, v4, v0

    const/16 v0, 0x12

    aput-object v10, v4, v0

    const/16 v0, 0x13

    aput-object v12, v4, v0

    const/16 v0, 0x14

    aput-object v27, v4, v0

    const/16 v0, 0x15

    aput-object v28, v4, v0

    const/16 v0, 0x16

    aput-object v29, v4, v0

    const/16 v0, 0x17

    aput-object v30, v4, v0

    const/16 v0, 0x18

    aput-object v6, v4, v0

    const/16 v0, 0x19

    aput-object v31, v4, v0

    const/16 v0, 0x1a

    aput-object v1, v4, v0

    const/16 v0, 0x1b

    aput-object v11, v4, v0

    const/16 v0, 0x1c

    aput-object v13, v4, v0

    .line 68
    sput-object v4, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->$VALUES:[Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/pspdfkit/annotations/AnnotationType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    iput-object p3, p0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->annotationType:Lcom/pspdfkit/annotations/AnnotationType;

    return-void
.end method

.method public static fromAnnotationType(Lcom/pspdfkit/annotations/AnnotationType;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 5

    if-nez p0, :cond_0

    .line 1
    sget-object p0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NONE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object p0

    .line 2
    :cond_0
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->values()[Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    .line 3
    invoke-virtual {v3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->toAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v4

    if-ne v4, p0, :cond_1

    return-object v3

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 7
    :cond_2
    sget-object p0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NONE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->$VALUES:[Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-virtual {v0}, [Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method


# virtual methods
.method public toAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->annotationType:Lcom/pspdfkit/annotations/AnnotationType;

    return-object v0
.end method

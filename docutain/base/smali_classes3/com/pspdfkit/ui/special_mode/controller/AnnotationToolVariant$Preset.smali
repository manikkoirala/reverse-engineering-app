.class public final enum Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Preset"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

.field public static final enum ARROW:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

.field public static final enum CALLOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

.field public static final enum CLOUDY:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

.field public static final enum DASHED:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

.field public static final enum DEFAULT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

.field public static final enum HIGHLIGHTER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

.field public static final enum MAGIC:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

.field public static final enum PEN:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fgetname(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->name:Ljava/lang/String;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    const-string v1, "DEFAULT"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->DEFAULT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 5
    new-instance v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    const-string v3, "PEN"

    const/4 v4, 0x1

    const-string v5, "Pen"

    invoke-direct {v1, v3, v4, v5}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->PEN:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 10
    new-instance v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    const-string v5, "HIGHLIGHTER"

    const/4 v6, 0x2

    const-string v7, "Highlighter"

    invoke-direct {v3, v5, v6, v7}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->HIGHLIGHTER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 15
    new-instance v5, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    const-string v7, "ARROW"

    const/4 v8, 0x3

    const-string v9, "Arrow"

    invoke-direct {v5, v7, v8, v9}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->ARROW:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 20
    new-instance v7, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    const-string v9, "MAGIC"

    const/4 v10, 0x4

    const-string v11, "Magic"

    invoke-direct {v7, v9, v10, v11}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->MAGIC:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 25
    new-instance v9, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    const-string v11, "CALLOUT"

    const/4 v12, 0x5

    const-string v13, "Callout"

    invoke-direct {v9, v11, v12, v13}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->CALLOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 30
    new-instance v11, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    const-string v13, "CLOUDY"

    const/4 v14, 0x6

    const-string v15, "Cloudy"

    invoke-direct {v11, v13, v14, v15}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v11, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->CLOUDY:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 35
    new-instance v13, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    const-string v15, "DASHED"

    const/4 v14, 0x7

    const-string v12, "Dashed"

    invoke-direct {v13, v15, v14, v12}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v13, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->DASHED:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    const/16 v12, 0x8

    new-array v12, v12, [Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    aput-object v0, v12, v2

    aput-object v1, v12, v4

    aput-object v3, v12, v6

    aput-object v5, v12, v8

    aput-object v7, v12, v10

    const/4 v0, 0x5

    aput-object v9, v12, v0

    const/4 v0, 0x6

    aput-object v11, v12, v0

    aput-object v13, v12, v14

    .line 36
    sput-object v12, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->$VALUES:[Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    iput-object p3, p0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->name:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->$VALUES:[Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    invoke-virtual {v0}, [Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->name:Ljava/lang/String;

    return-object v0
.end method

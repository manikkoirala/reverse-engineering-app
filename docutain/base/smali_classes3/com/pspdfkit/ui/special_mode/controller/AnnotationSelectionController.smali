.class public interface abstract Lcom/pspdfkit/ui/special_mode/controller/AnnotationSelectionController;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getAnnotationSelectionViewThemeConfiguration()Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;
.end method

.method public abstract isDraggingEnabled()Z
.end method

.method public abstract isKeepAspectRatioEnabled()Ljava/lang/Boolean;
.end method

.method public abstract isResizeEnabled()Z
.end method

.method public abstract isResizeGuidesEnabled()Z
.end method

.method public abstract isRotationEnabled()Z
.end method

.method public abstract setAnnotationSelectionViewThemeConfiguration(Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;)V
.end method

.method public abstract setDraggingEnabled(Z)V
.end method

.method public abstract setKeepAspectRatioEnabled(Z)V
.end method

.method public abstract setResizeEnabled(Z)V
.end method

.method public abstract setResizeGuidesEnabled(Z)V
.end method

.method public abstract setRotationEnabled(Z)V
.end method

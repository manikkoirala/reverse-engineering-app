.class public interface abstract Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;


# virtual methods
.method public abstract bindAnnotationInspectorController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;)V
.end method

.method public abstract changeAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
.end method

.method public abstract getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
.end method

.method public abstract getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
.end method

.method public abstract getAlpha()F
.end method

.method public abstract getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;
.end method

.method public abstract getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;
.end method

.method public abstract getBorderStylePreset()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;
.end method

.method public abstract getColor()I
.end method

.method public abstract getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;
.end method

.method public abstract getFillColor()I
.end method

.method public abstract getFloatPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;
.end method

.method public abstract getFont()Lcom/pspdfkit/ui/fonts/Font;
.end method

.method public abstract getLineEnds()Landroidx/core/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;
.end method

.method public abstract getOutlineColor()I
.end method

.method public abstract getOverlayText()Ljava/lang/String;
.end method

.method public abstract getRepeatOverlayText()Z
.end method

.method public abstract getTextSize()F
.end method

.method public abstract getThickness()F
.end method

.method public abstract isMeasurementSnappingEnabled()Z
.end method

.method public abstract setAlpha(F)V
.end method

.method public abstract setBorderStylePreset(Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
.end method

.method public abstract setColor(I)V
.end method

.method public abstract setFillColor(I)V
.end method

.method public abstract setFloatPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
.end method

.method public abstract setFont(Lcom/pspdfkit/ui/fonts/Font;)V
.end method

.method public abstract setLineEnds(Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V
.end method

.method public abstract setMeasurementScale(Lcom/pspdfkit/annotations/measurements/Scale;)V
.end method

.method public abstract setMeasurementSnappingEnabled(Z)V
.end method

.method public abstract setOutlineColor(I)V
.end method

.method public abstract setOverlayText(Ljava/lang/String;)V
.end method

.method public abstract setRepeatOverlayText(Z)V
.end method

.method public abstract setTextSize(F)V
.end method

.method public abstract setThickness(F)V
.end method

.method public abstract shouldDisplayPicker()Z
.end method

.method public abstract showAnnotationEditor(Lcom/pspdfkit/annotations/Annotation;)V
.end method

.method public abstract toggleAnnotationInspector()V
.end method

.method public abstract unbindAnnotationInspectorController()V
.end method

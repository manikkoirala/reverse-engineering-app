.class Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarViewItemClickedListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/un$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/PopupToolbar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnPopupToolbarViewItemClickedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/PopupToolbar;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/ui/PopupToolbar;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarViewItemClickedListener;->this$0:Lcom/pspdfkit/ui/PopupToolbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/ui/PopupToolbar;Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarViewItemClickedListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarViewItemClickedListener;-><init>(Lcom/pspdfkit/ui/PopupToolbar;)V

    return-void
.end method


# virtual methods
.method public onBackItemClicked()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarViewItemClickedListener;->this$0:Lcom/pspdfkit/ui/PopupToolbar;

    invoke-static {v0}, Lcom/pspdfkit/ui/PopupToolbar;->-$$Nest$fgetpopupToolbarView(Lcom/pspdfkit/ui/PopupToolbar;)Lcom/pspdfkit/internal/un;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/un;->b()V

    return-void
.end method

.method public onItemClicked(Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarViewItemClickedListener;->this$0:Lcom/pspdfkit/ui/PopupToolbar;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PopupToolbar;->onItemClicked(Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)Z

    return-void
.end method

.method public onOverflowItemClicked()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarViewItemClickedListener;->this$0:Lcom/pspdfkit/ui/PopupToolbar;

    invoke-static {v0}, Lcom/pspdfkit/ui/PopupToolbar;->-$$Nest$fgetpopupToolbarView(Lcom/pspdfkit/ui/PopupToolbar;)Lcom/pspdfkit/internal/un;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/un;->e()V

    return-void
.end method

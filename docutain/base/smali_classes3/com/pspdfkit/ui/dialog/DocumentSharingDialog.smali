.class public Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;
.super Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;
    }
.end annotation


# static fields
.field static final FRAGMENT_TAG:Ljava/lang/String; = "com.pspdfkit.ui.dialog.DocumentSharingDialog.FRAGMENT_TAG"


# instance fields
.field private shareDialogLayout:Lcom/pspdfkit/internal/w9;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;-><init>()V

    return-void
.end method

.method private static getInstance(Landroidx/fragment/app/FragmentManager;)Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-static {p0, v0}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->getInstance(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;)Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;

    move-result-object p0

    return-object p0
.end method

.method private static getInstance(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;)Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;
    .locals 1

    const-string v0, "com.pspdfkit.ui.dialog.DocumentSharingDialog.FRAGMENT_TAG"

    .line 2
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;

    if-nez p0, :cond_1

    if-nez p1, :cond_0

    .line 5
    new-instance p1, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;

    invoke-direct {p1}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;-><init>()V

    .line 9
    :cond_0
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p1, p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->setArguments(Landroid/os/Bundle;)V

    move-object p0, p1

    :cond_1
    return-object p0
.end method

.method public static hide(Landroidx/fragment/app/FragmentManager;)V
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->isVisible(Landroidx/fragment/app/FragmentManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-static {p0}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->getInstance(Landroidx/fragment/app/FragmentManager;)Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;

    move-result-object p0

    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method public static isVisible(Landroidx/fragment/app/FragmentManager;)Z
    .locals 1

    const-string v0, "com.pspdfkit.ui.dialog.DocumentSharingDialog.FRAGMENT_TAG"

    .line 1
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;

    if-eqz p0, :cond_0

    .line 2
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->isAdded()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;)V
    .locals 1

    const-string v0, "com.pspdfkit.ui.dialog.DocumentSharingDialog.FRAGMENT_TAG"

    .line 1
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;

    if-eqz p0, :cond_0

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;->listener:Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;

    :cond_0
    return-void
.end method

.method public static show(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-static {v0, p0, p1, p2}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->show(Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;)V

    return-void
.end method

.method public static show(Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;)V
    .locals 3

    const-string v0, "manager"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 54
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 56
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 108
    invoke-static {p1, p0}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->getInstance(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;)Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;

    move-result-object p0

    .line 109
    iput-object p3, p0, Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;->listener:Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;

    .line 110
    iput-object p2, p0, Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;->configuration:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;

    .line 111
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->isAdded()Z

    move-result p2

    if-nez p2, :cond_0

    const-string p2, "com.pspdfkit.ui.dialog.DocumentSharingDialog.FRAGMENT_TAG"

    .line 112
    invoke-virtual {p0, p1, p2}, Landroidx/appcompat/app/AppCompatDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method synthetic lambda$onCreateDialog$0$com-pspdfkit-ui-dialog-DocumentSharingDialog(Lcom/pspdfkit/internal/w9;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;->listener:Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;

    if-eqz p1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->shareDialogLayout:Lcom/pspdfkit/internal/w9;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/w9;->getSharingOptions()Lcom/pspdfkit/document/sharing/SharingOptions;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;->onAccept(Lcom/pspdfkit/document/sharing/SharingOptions;)V

    .line 3
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .line 1
    new-instance p1, Lcom/pspdfkit/internal/w9;

    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;->configuration:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;

    const/4 v2, 0x0

    .line 2
    invoke-direct {p1, v0, v1, v2}, Lcom/pspdfkit/internal/w9;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;Ljava/util/ArrayList;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->shareDialogLayout:Lcom/pspdfkit/internal/w9;

    .line 4
    new-instance v0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;)V

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/w9;->setOnConfirmDocumentSharingListener(Lcom/pspdfkit/internal/w9$b;)V

    .line 10
    new-instance p1, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->shareDialogLayout:Lcom/pspdfkit/internal/w9;

    .line 12
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    .line 13
    invoke-virtual {p1}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public onStart()V
    .locals 10

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 2
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    instance-of v0, v0, Landroidx/appcompat/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->shareDialogLayout:Lcom/pspdfkit/internal/w9;

    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/app/AlertDialog;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v0, 0x0

    .line 4
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    invoke-virtual {v1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__alert_dialog_inset:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v9

    .line 7
    new-instance v5, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v5}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    const/4 v2, 0x0

    .line 8
    invoke-virtual {v5, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 9
    invoke-virtual {v5, v2}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 10
    invoke-virtual {v5, v0}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 12
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/InsetDrawable;

    move-object v4, v1

    move v6, v9

    move v7, v9

    move v8, v9

    invoke-direct/range {v4 .. v9}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_0
    return-void
.end method

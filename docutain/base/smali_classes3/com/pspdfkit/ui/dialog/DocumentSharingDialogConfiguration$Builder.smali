.class public Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private currentPage:I

.field private dialogTitle:Ljava/lang/String;

.field private documentPages:I

.field private initialDocumentName:Ljava/lang/String;

.field private initialPagesSpinnerAllPages:Z

.field private positiveButtonText:Ljava/lang/String;

.field private savingFlow:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    sget v0, Lcom/pspdfkit/R$string;->pspdf__share:I

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->dialogTitle:Ljava/lang/String;

    .line 4
    sget v0, Lcom/pspdfkit/R$string;->pspdf__share:I

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->positiveButtonText:Ljava/lang/String;

    const/4 p1, 0x0

    .line 5
    iput p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->currentPage:I

    .line 6
    iput p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->documentPages:I

    const-string v0, ""

    .line 7
    iput-object v0, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->initialDocumentName:Ljava/lang/String;

    const/4 v0, 0x1

    .line 8
    iput-boolean v0, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->initialPagesSpinnerAllPages:Z

    .line 9
    iput-boolean p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->savingFlow:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context"

    .line 11
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    sget v0, Lcom/pspdfkit/R$string;->pspdf__share:I

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->dialogTitle:Ljava/lang/String;

    .line 13
    sget v0, Lcom/pspdfkit/R$string;->pspdf__share:I

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->positiveButtonText:Ljava/lang/String;

    .line 14
    iput p3, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->currentPage:I

    .line 15
    invoke-interface {p2}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p3

    iput p3, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->documentPages:I

    .line 16
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->initialDocumentName:Ljava/lang/String;

    const/4 p1, 0x1

    .line 17
    iput-boolean p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->initialPagesSpinnerAllPages:Z

    const/4 p1, 0x0

    .line 18
    iput-boolean p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->savingFlow:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/sharing/ShareAction;Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context"

    .line 20
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shareAction"

    .line 21
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->getShareDialogTitle(Landroid/content/Context;Lcom/pspdfkit/document/sharing/ShareAction;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->dialogTitle:Ljava/lang/String;

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->getShareButtonText(Landroid/content/Context;Lcom/pspdfkit/document/sharing/ShareAction;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->positiveButtonText:Ljava/lang/String;

    .line 24
    iput p4, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->currentPage:I

    .line 25
    invoke-interface {p3}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p2

    iput p2, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->documentPages:I

    .line 26
    invoke-static {p1, p3}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->initialDocumentName:Ljava/lang/String;

    const/4 p1, 0x1

    .line 27
    iput-boolean p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->initialPagesSpinnerAllPages:Z

    const/4 p1, 0x0

    .line 28
    iput-boolean p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->savingFlow:Z

    return-void
.end method

.method private getShareButtonText(Landroid/content/Context;Lcom/pspdfkit/document/sharing/ShareAction;)Ljava/lang/String;
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->savingFlow:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2
    sget p2, Lcom/pspdfkit/R$string;->pspdf__save:I

    .line 3
    invoke-static {p1, p2, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 4
    :cond_0
    sget-object v0, Lcom/pspdfkit/document/sharing/ShareAction;->VIEW:Lcom/pspdfkit/document/sharing/ShareAction;

    if-ne p2, v0, :cond_1

    .line 5
    sget p2, Lcom/pspdfkit/R$string;->pspdf__open:I

    .line 6
    invoke-static {p1, p2, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 7
    :cond_1
    sget p2, Lcom/pspdfkit/R$string;->pspdf__share:I

    .line 8
    invoke-static {p1, p2, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private getShareDialogTitle(Landroid/content/Context;Lcom/pspdfkit/document/sharing/ShareAction;)Ljava/lang/String;
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->savingFlow:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2
    sget p2, Lcom/pspdfkit/R$string;->pspdf__save_as:I

    .line 3
    invoke-static {p1, p2, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 4
    :cond_0
    sget-object v0, Lcom/pspdfkit/document/sharing/ShareAction;->VIEW:Lcom/pspdfkit/document/sharing/ShareAction;

    if-ne p2, v0, :cond_1

    sget p2, Lcom/pspdfkit/R$string;->pspdf__open:I

    goto :goto_0

    :cond_1
    sget p2, Lcom/pspdfkit/R$string;->pspdf__share:I

    .line 5
    :goto_0
    invoke-static {p1, p2, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "\u2026"

    .line 6
    invoke-virtual {p1, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public build()Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->dialogTitle:Ljava/lang/String;

    iget-object v1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->positiveButtonText:Ljava/lang/String;

    iget v2, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->currentPage:I

    iget v3, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->documentPages:I

    iget-object v4, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->initialDocumentName:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->initialPagesSpinnerAllPages:Z

    iget-boolean v6, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->savingFlow:Z

    invoke-static/range {v0 .. v6}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;->-$$Nest$smcreate(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ZZ)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public currentPage(I)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->currentPage:I

    return-object p0
.end method

.method public dialogTitle(Ljava/lang/String;)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;
    .locals 2

    const-string v0, "dialogTitle"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->dialogTitle:Ljava/lang/String;

    return-object p0
.end method

.method public documentPages(I)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->documentPages:I

    return-object p0
.end method

.method public initialDocumentName(Ljava/lang/String;)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;
    .locals 2

    const-string v0, "initialDocumentName"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->initialDocumentName:Ljava/lang/String;

    return-object p0
.end method

.method public positiveButtonText(Ljava/lang/String;)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;
    .locals 2

    const-string v0, "positiveButtonText"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->positiveButtonText:Ljava/lang/String;

    return-object p0
.end method

.method public setInitialPagesSpinnerAllPages(Z)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->initialPagesSpinnerAllPages:Z

    return-object p0
.end method

.method public setSavingFlow(ZLandroid/content/Context;)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;
    .locals 2

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-boolean p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->savingFlow:Z

    .line 55
    sget p1, Lcom/pspdfkit/R$string;->pspdf__save_as:I

    .line 56
    invoke-static {p2, p1, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    .line 57
    iput-object p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->dialogTitle:Ljava/lang/String;

    .line 58
    sget p1, Lcom/pspdfkit/R$string;->pspdf__save:I

    .line 59
    invoke-static {p2, p1, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    .line 60
    iput-object p1, p0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->positiveButtonText:Ljava/lang/String;

    return-object p0
.end method

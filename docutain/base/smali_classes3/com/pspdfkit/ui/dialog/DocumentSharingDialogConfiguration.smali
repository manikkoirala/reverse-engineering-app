.class public abstract Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;
    }
.end annotation


# direct methods
.method static bridge synthetic -$$Nest$smcreate(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ZZ)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;
    .locals 0

    invoke-static/range {p0 .. p6}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;->create(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ZZ)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static create(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ZZ)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;
    .locals 9

    .line 1
    new-instance v8, Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ZZ)V

    return-object v8
.end method


# virtual methods
.method public abstract getCurrentPage()I
.end method

.method public abstract getDialogTitle()Ljava/lang/String;
.end method

.method public abstract getDocumentPages()I
.end method

.method public abstract getInitialDocumentName()Ljava/lang/String;
.end method

.method public abstract getPositiveButtonText()Ljava/lang/String;
.end method

.method public abstract isInitialPagesSpinnerAllPages()Z
.end method

.method public abstract isSavingFlow()Z
.end method

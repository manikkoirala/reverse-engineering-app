.class final Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;
.super Lcom/pspdfkit/ui/dialog/$AutoValue_DocumentSharingDialogConfiguration;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration$1;

    invoke-direct {v0}, Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration$1;-><init>()V

    sput-object v0, Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ZZ)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p7}, Lcom/pspdfkit/ui/dialog/$AutoValue_DocumentSharingDialogConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ZZ)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/dialog/$AutoValue_DocumentSharingDialogConfiguration;->getDialogTitle()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/dialog/$AutoValue_DocumentSharingDialogConfiguration;->getPositiveButtonText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/ui/dialog/$AutoValue_DocumentSharingDialogConfiguration;->getCurrentPage()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/ui/dialog/$AutoValue_DocumentSharingDialogConfiguration;->getDocumentPages()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/ui/dialog/$AutoValue_DocumentSharingDialogConfiguration;->getInitialDocumentName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/ui/dialog/$AutoValue_DocumentSharingDialogConfiguration;->isInitialPagesSpinnerAllPages()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/ui/dialog/$AutoValue_DocumentSharingDialogConfiguration;->isSavingFlow()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

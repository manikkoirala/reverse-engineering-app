.class Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;
    .locals 10

    .line 2
    new-instance v8, Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;

    .line 3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 4
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 5
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 6
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 7
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 8
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-ne v0, v7, :cond_0

    const/4 v9, 0x1

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    .line 9
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    if-ne p1, v7, :cond_1

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    :goto_1
    move-object v0, v8

    move v6, v9

    invoke-direct/range {v0 .. v7}, Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ZZ)V

    return-object v8
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration$1;->createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;
    .locals 0

    .line 2
    new-array p1, p1, [Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration$1;->newArray(I)[Lcom/pspdfkit/ui/dialog/AutoValue_DocumentSharingDialogConfiguration;

    move-result-object p1

    return-object p1
.end method

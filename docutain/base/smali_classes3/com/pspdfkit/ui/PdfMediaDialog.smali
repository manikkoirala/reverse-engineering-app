.class public Lcom/pspdfkit/ui/PdfMediaDialog;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/media/MediaViewListener;


# static fields
.field public static final ARG_MEDIA_URI:Ljava/lang/String; = "PSPDFKit.MediaURI"

.field public static final ARG_VIDEO_PLAYBACK_ENABLED:Ljava/lang/String; = "PSPDFKit.VideoPlaybackEnabled"

.field public static final TAG:Ljava/lang/String; = "PSPDFKit.MediaDialog"


# instance fields
.field private inflatedView:Landroid/view/View;

.field private mediaUri:Lcom/pspdfkit/media/MediaUri;

.field private progressBar:Landroid/widget/ProgressBar;

.field private rootView:Landroid/widget/RelativeLayout;

.field private videoPlaybackEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentError()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onContentReady()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 2
    sget p1, Lcom/pspdfkit/R$layout;->pspdf__media_dialog:I

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setContentView(I)V

    .line 4
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "PSPDFKit.MediaURI"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/media/MediaUri;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->mediaUri:Lcom/pspdfkit/media/MediaUri;

    .line 5
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "PSPDFKit.VideoPlaybackEnabled"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->videoPlaybackEnabled:Z

    .line 6
    sget p1, Lcom/pspdfkit/R$id;->pspdf__media_dialog_root:I

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RelativeLayout;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->rootView:Landroid/widget/RelativeLayout;

    .line 7
    sget p1, Lcom/pspdfkit/R$id;->pspdf__loading_progress:I

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->progressBar:Landroid/widget/ProgressBar;

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->inflatedView:Landroid/view/View;

    instance-of v1, v0, Lcom/pspdfkit/media/MediaWebView;

    if-eqz v1, :cond_0

    .line 2
    check-cast v0, Lcom/pspdfkit/media/MediaWebView;

    .line 3
    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    :cond_0
    const/4 v0, 0x0

    .line 6
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->inflatedView:Landroid/view/View;

    .line 7
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onStart()V
    .locals 4

    .line 1
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 3
    sget-object v0, Lcom/pspdfkit/ui/PdfMediaDialog$1;->$SwitchMap$com$pspdfkit$media$MediaUri$UriType:[I

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->mediaUri:Lcom/pspdfkit/media/MediaUri;

    invoke-virtual {v1}, Lcom/pspdfkit/media/MediaUri;->getType()Lcom/pspdfkit/media/MediaUri$UriType;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    .line 32
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    .line 33
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->videoPlaybackEnabled:Z

    if-nez v0, :cond_1

    .line 34
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    .line 40
    :cond_1
    :try_start_0
    new-instance v0, Lcom/pspdfkit/media/MediaWebView;

    invoke-direct {v0, p0}, Lcom/pspdfkit/media/MediaWebView;-><init>(Landroid/content/Context;)V

    .line 41
    iget-object v2, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->mediaUri:Lcom/pspdfkit/media/MediaUri;

    invoke-virtual {v2}, Lcom/pspdfkit/media/MediaUri;->getOptions()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->mediaUri:Lcom/pspdfkit/media/MediaUri;

    invoke-virtual {v3}, Lcom/pspdfkit/media/MediaUri;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/pspdfkit/media/MediaWebView;->start(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-virtual {v0, p0}, Lcom/pspdfkit/media/MediaWebView;->setMediaViewListener(Lcom/pspdfkit/media/MediaViewListener;)V

    .line 43
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->inflatedView:Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.ActionResolver"

    const-string v3, "Can\'t initialize WebView for media display."

    .line 45
    invoke-static {v2, v0, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    .line 47
    :cond_2
    new-instance v0, Lcom/pspdfkit/media/MediaGalleryView;

    invoke-direct {v0, p0}, Lcom/pspdfkit/media/MediaGalleryView;-><init>(Landroid/content/Context;)V

    .line 48
    invoke-virtual {v0, p0}, Lcom/pspdfkit/media/MediaGalleryView;->setMediaViewListener(Lcom/pspdfkit/media/MediaViewListener;)V

    .line 49
    iget-object v2, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->mediaUri:Lcom/pspdfkit/media/MediaUri;

    invoke-virtual {v2}, Lcom/pspdfkit/media/MediaUri;->getOptions()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->mediaUri:Lcom/pspdfkit/media/MediaUri;

    invoke-virtual {v3}, Lcom/pspdfkit/media/MediaUri;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/pspdfkit/media/MediaGalleryView;->start(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->inflatedView:Landroid/view/View;

    .line 80
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->inflatedView:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 82
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 84
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->rootView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfMediaDialog;->inflatedView:Landroid/view/View;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :cond_3
    return-void
.end method

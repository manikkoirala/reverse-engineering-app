.class public Lcom/pspdfkit/ui/PdfFragment;
.super Landroidx/fragment/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/listeners/DocumentListener;
.implements Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;
.implements Lcom/pspdfkit/ui/drawable/PdfDrawableManager;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;
.implements Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;
.implements Lcom/pspdfkit/ui/special_mode/manager/FormManager;
.implements Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager;
.implements Lcom/pspdfkit/internal/ms;
.implements Lcom/pspdfkit/ui/navigation/PageNavigator;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;
.implements Lcom/pspdfkit/annotations/actions/ActionResolver;
.implements Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;
.implements Lcom/pspdfkit/internal/p9$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;
    }
.end annotation


# static fields
.field public static final DEFAULT_ZOOM:F = 1.0f

.field public static final DOCUMENTSTORE_KEY_LAST_VIEWED_PAGE_INDEX:Ljava/lang/String; = "PSPDFKit.lastViewedPage"

.field public static final DOCUMENT_VIEW_ID:I

.field public static final MAX_ZOOM:F = 15.0f

.field public static final MIN_ZOOM:F = 0.8f

.field private static final PARAM_AUDIO_MANAGER_STATE:Ljava/lang/String; = "PSPDFKit.AudioManagerState"

.field public static final PARAM_CONFIGURATION:Ljava/lang/String; = "PSPDFKit.Configuration"

.field private static final PARAM_CURRENT_VIEW_STATE:Ljava/lang/String; = "PSPDFKit.ViewState"

.field private static final PARAM_DOCUMENT_LOADING_PROGRESS:Ljava/lang/String; = "PSPDFKit.DocumentLoadingProgress"

.field private static final PARAM_FRAGMENT_STATE:Ljava/lang/String; = "PSPDFKit.PSPDFFragmentState"

.field public static final PARAM_IMAGE_DOCUMENT_SOURCE:Ljava/lang/String; = "PSPDFKit.ImageDocument.Source"

.field private static final PARAM_LAST_ENABLED_SPECIAL_MODE_STATE:Ljava/lang/String; = "PSPDFKit.LastEnabledSpecialModeState"

.field private static final PARAM_MEDIA_CONTENT_STATES:Ljava/lang/String; = "PSPDFKit.MediaContentStates"

.field private static final PARAM_NAVIGATION_HISTORY:Ljava/lang/String; = "PSPDFKit.NavigationHistory"

.field private static final PARAM_PASSWORD:Ljava/lang/String; = "PSPDFKit.UserP"

.field private static final PARAM_REDACTION_PREVIEW_STATE:Ljava/lang/String; = "PSPDFKit.RedactionPreviewState"

.field public static final PARAM_SOURCES:Ljava/lang/String; = "PSPDFKit.Sources"

.field private static final PARAM_SPECIAL_MODE_STATE:Ljava/lang/String; = "PSPDFKit.SpecialModeState"


# instance fields
.field private animatePageTransition:Ljava/lang/Boolean;

.field private final audioModeManager:Lcom/pspdfkit/internal/a3;

.field private configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private final contentEditingUndoManager:Lcom/pspdfkit/internal/vu;

.field private final defaultOnDocumentLongPressListener:Lcom/pspdfkit/listeners/OnDocumentLongPressListener;

.field private displayedPage:I

.field private document:Lcom/pspdfkit/internal/zf;

.field private documentListeners:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/listeners/DocumentListener;",
            ">;"
        }
    .end annotation
.end field

.field private documentLoadDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

.field private documentLoadingProgressDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

.field documentSaver:Lcom/pspdfkit/internal/p9;

.field private final documentScrollListeners:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;",
            ">;"
        }
    .end annotation
.end field

.field documentSources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;"
        }
    .end annotation
.end field

.field private final formFieldUpdatedListener:Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;

.field private fragmentState:Landroid/os/Bundle;

.field private historyActionInProgress:Z

.field private imageDocument:Lcom/pspdfkit/document/ImageDocument;

.field private imageDocumentSource:Lcom/pspdfkit/document/DocumentSource;

.field private insetsBottom:I

.field private insetsLeft:I

.field private insetsRight:I

.field private insetsTop:I

.field private final internalAPI:Lcom/pspdfkit/internal/ag;

.field private final internalDocumentListener:Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;

.field private isDocumentInteractionEnabled:Z

.field private isUserInterfaceEnabled:Z

.field private final javaScriptPlatformDelegate:Lcom/pspdfkit/internal/wm;

.field private lastEnabledSpecialModeState:Lcom/pspdfkit/internal/ns;

.field private lastViewedPageRestorationDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

.field private lifecycleDisposable:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

.field private final navigateOnUndoListener:Lcom/pspdfkit/internal/k4$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/k4$a<",
            "-",
            "Lcom/pspdfkit/internal/zl;",
            ">;"
        }
    .end annotation
.end field

.field private navigationEndPage:Ljava/lang/Integer;

.field private final navigationHistory:Lcom/pspdfkit/ui/navigation/NavigationBackStack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/ui/navigation/NavigationBackStack<",
            "Lcom/pspdfkit/ui/navigation/NavigationBackStack$NavigationItem<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final navigationItemBackStackListener:Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener<",
            "Lcom/pspdfkit/ui/navigation/NavigationBackStack$NavigationItem<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private navigationStartPage:Ljava/lang/Integer;

.field private onDocumentLongPressListener:Lcom/pspdfkit/listeners/OnDocumentLongPressListener;

.field private pageChangeSubject:Lio/reactivex/rxjava3/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/subjects/BehaviorSubject<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private password:Ljava/lang/String;

.field private pasteManager:Lcom/pspdfkit/internal/um;

.field private redactionAnnotationPreviewEnabled:Z

.field private final signatureFormSigningHandler:Lcom/pspdfkit/internal/pr;

.field private signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

.field private signatureStorage:Lcom/pspdfkit/signatures/storage/SignatureStorage;

.field private startZoomScale:F

.field private final undoManager:Lcom/pspdfkit/internal/vu;

.field private userInterfaceListeners:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/internal/ev;",
            ">;"
        }
    .end annotation
.end field

.field private final viewCoordinator:Lcom/pspdfkit/internal/xm;

.field private final viewProjectionImpl:Lcom/pspdfkit/projection/ViewProjection;

.field private weakDocumentListeners:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/listeners/DocumentListener;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetdocument(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/zf;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetdocumentListeners(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/nh;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetinternalDocumentListener(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfFragment;->internalDocumentListener:Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetlastViewedPageRestorationDisposable(Lcom/pspdfkit/ui/PdfFragment;)Lio/reactivex/rxjava3/disposables/Disposable;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfFragment;->lastViewedPageRestorationDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetnavigationHistory(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/ui/navigation/NavigationBackStack;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationHistory:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetpasteManager(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/um;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfFragment;->pasteManager:Lcom/pspdfkit/internal/um;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetundoManager(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/vu;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfFragment;->undoManager:Lcom/pspdfkit/internal/vu;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetuserInterfaceListeners(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/nh;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfFragment;->userInterfaceListeners:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetviewCoordinator(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/xm;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputdocument(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/zf;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputhistoryActionInProgress(Lcom/pspdfkit/ui/PdfFragment;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfFragment;->historyActionInProgress:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mdisplayDocument(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/zf;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->displayDocument(Lcom/pspdfkit/internal/zf;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetDocument(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->resetDocument()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__document_view:I

    sput v0, Lcom/pspdfkit/ui/PdfFragment;->DOCUMENT_VIEW_ID:I

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/PdfFragment;->redactionAnnotationPreviewEnabled:Z

    .line 360
    new-instance v1, Lcom/pspdfkit/internal/vu;

    invoke-direct {v1}, Lcom/pspdfkit/internal/vu;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->undoManager:Lcom/pspdfkit/internal/vu;

    .line 366
    new-instance v2, Lcom/pspdfkit/internal/vu;

    invoke-direct {v2}, Lcom/pspdfkit/internal/vu;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->contentEditingUndoManager:Lcom/pspdfkit/internal/vu;

    .line 370
    new-instance v2, Lcom/pspdfkit/internal/a3;

    invoke-direct {v2, p0, v1}, Lcom/pspdfkit/internal/a3;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/vu;)V

    iput-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->audioModeManager:Lcom/pspdfkit/internal/a3;

    .line 374
    new-instance v3, Lcom/pspdfkit/internal/nh;

    invoke-direct {v3}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v3, p0, Lcom/pspdfkit/ui/PdfFragment;->documentScrollListeners:Lcom/pspdfkit/internal/nh;

    .line 382
    new-instance v3, Lcom/pspdfkit/internal/pr;

    invoke-direct {v3, p0, v1}, Lcom/pspdfkit/internal/pr;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/vu;)V

    iput-object v3, p0, Lcom/pspdfkit/ui/PdfFragment;->signatureFormSigningHandler:Lcom/pspdfkit/internal/pr;

    .line 386
    new-instance v4, Lcom/pspdfkit/internal/xm;

    invoke-direct {v4, p0, v1, v3, v2}, Lcom/pspdfkit/internal/xm;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/vu;Lcom/pspdfkit/internal/pr;Lcom/pspdfkit/internal/a3;)V

    iput-object v4, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    .line 391
    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda43;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda43;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    iput-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->defaultOnDocumentLongPressListener:Lcom/pspdfkit/listeners/OnDocumentLongPressListener;

    .line 421
    new-instance v1, Lcom/pspdfkit/internal/nh;

    invoke-direct {v1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    .line 424
    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->weakDocumentListeners:Ljava/lang/ref/WeakReference;

    .line 435
    iput v0, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    const/4 v1, 0x0

    .line 458
    iput-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->animatePageTransition:Ljava/lang/Boolean;

    .line 477
    iput-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->signatureStorage:Lcom/pspdfkit/signatures/storage/SignatureStorage;

    .line 481
    iput-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    .line 484
    new-instance v2, Lcom/pspdfkit/ui/PdfFragment$1;

    invoke-direct {v2, p0}, Lcom/pspdfkit/ui/PdfFragment$1;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    iput-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->formFieldUpdatedListener:Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;

    .line 501
    new-instance v2, Lcom/pspdfkit/internal/wm;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/wm;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    iput-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->javaScriptPlatformDelegate:Lcom/pspdfkit/internal/wm;

    .line 505
    new-instance v2, Lcom/pspdfkit/internal/nh;

    invoke-direct {v2}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->userInterfaceListeners:Lcom/pspdfkit/internal/nh;

    .line 515
    iput-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->lastViewedPageRestorationDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 526
    new-instance v2, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {v2}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->lifecycleDisposable:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 550
    iput-boolean v0, p0, Lcom/pspdfkit/ui/PdfFragment;->historyActionInProgress:Z

    .line 552
    new-instance v2, Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    invoke-direct {v2}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationHistory:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    .line 556
    iput-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationStartPage:Ljava/lang/Integer;

    .line 559
    iput-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationEndPage:Ljava/lang/Integer;

    .line 566
    new-instance v2, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda44;

    invoke-direct {v2, p0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda44;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    iput-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->navigateOnUndoListener:Lcom/pspdfkit/internal/k4$a;

    .line 579
    new-instance v2, Lcom/pspdfkit/ui/PdfFragment$2;

    invoke-direct {v2, p0}, Lcom/pspdfkit/ui/PdfFragment$2;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    iput-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationItemBackStackListener:Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;

    .line 599
    iput-boolean v0, p0, Lcom/pspdfkit/ui/PdfFragment;->isUserInterfaceEnabled:Z

    const/4 v0, 0x1

    .line 604
    iput-boolean v0, p0, Lcom/pspdfkit/ui/PdfFragment;->isDocumentInteractionEnabled:Z

    .line 611
    new-instance v0, Lcom/pspdfkit/internal/nv;

    invoke-direct {v0, p0, v4}, Lcom/pspdfkit/internal/nv;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/xm;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewProjectionImpl:Lcom/pspdfkit/projection/ViewProjection;

    .line 618
    new-instance v0, Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener-IA;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->internalDocumentListener:Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;

    .line 625
    new-instance v0, Lcom/pspdfkit/ui/PdfFragment$3;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/PdfFragment$3;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->internalAPI:Lcom/pspdfkit/internal/ag;

    return-void
.end method

.method private cancelRestorePagePosition()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->lastViewedPageRestorationDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->lastViewedPageRestorationDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private copyUri(Landroid/content/Context;Lcom/pspdfkit/annotations/actions/UriAction;)V
    .locals 3

    .line 1
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/actions/UriAction;->getUri()Ljava/lang/String;

    move-result-object p2

    sget v0, Lcom/pspdfkit/R$string;->pspdf__text_copied_to_clipboard:I

    const-string v1, "Link annotation URL"

    const/16 v2, 0x30

    .line 3
    invoke-static {p2, v1, p1, v0, v2}, Lcom/pspdfkit/internal/j5;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;II)Z

    return-void
.end method

.method private displayDocument(Lcom/pspdfkit/internal/zf;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->z()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/zf;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v0, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda23;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda23;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    const/4 v1, 0x0

    .line 8
    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$d;Z)V

    return-void
.end method

.method private getMediaContentStates()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/xi;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "PdfFragment#getMediaContentStates() may only be called from the main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->i()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private getPageEditorForCurrentPage()Lcom/pspdfkit/internal/am;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->j()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/xm;->c(I)Lcom/pspdfkit/internal/am;

    move-result-object v0

    return-object v0
.end method

.method private getPageEditorForPage(I)Lcom/pspdfkit/internal/am;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/xm;->c(I)Lcom/pspdfkit/internal/am;

    move-result-object p1

    return-object p1
.end method

.method private getSpecialModeState()Lcom/pspdfkit/internal/ns;
    .locals 8

    .line 1
    new-instance v7, Lcom/pspdfkit/internal/ns;

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v1

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v2

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getSelectedAnnotations()Ljava/util/List;

    move-result-object v3

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getSelectedFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v4

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v5

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getContentEditingState()Lcom/pspdfkit/internal/w6;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/ns;-><init>(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/util/List;Lcom/pspdfkit/forms/FormElement;Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/w6;)V

    return-object v7
.end method

.method private handleDocumentLoadingError(Ljava/lang/Throwable;Z)V
    .locals 3

    if-nez p1, :cond_0

    const-string v0, ""

    goto :goto_0

    .line 1
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 2
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/xm;->z()V

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v2, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda3;

    invoke-direct {v2, p0, v0, p2, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/ui/PdfFragment;Ljava/lang/String;ZLjava/lang/Throwable;)V

    const/4 p1, 0x0

    .line 4
    invoke-virtual {v1, v2, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$d;Z)V

    return-void
.end method

.method static synthetic lambda$addAnnotationToPage$24(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfFragment"

    const-string v2, "Unable to add annotation to page"

    .line 1
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$addDocumentActionListener$41(Lcom/pspdfkit/document/DocumentActionListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getActionResolver()Lcom/pspdfkit/annotations/actions/ActionResolver;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/annotations/actions/ActionResolver;->addDocumentActionListener(Lcom/pspdfkit/document/DocumentActionListener;)V

    return-void
.end method

.method static synthetic lambda$addDrawableProvider$50(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->addDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    return-void
.end method

.method static synthetic lambda$addOnAnnotationCreationModeChangeListener$60(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getAnnotationListeners()Lcom/pspdfkit/internal/i1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V

    return-void
.end method

.method static synthetic lambda$addOnAnnotationCreationModeSettingsChangeListener$62(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getAnnotationListeners()Lcom/pspdfkit/internal/i1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    return-void
.end method

.method static synthetic lambda$addOnAnnotationDeselectedListener$56(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getAnnotationListeners()Lcom/pspdfkit/internal/i1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V

    return-void
.end method

.method static synthetic lambda$addOnAnnotationEditingModeChangeListener$64(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getAnnotationListeners()Lcom/pspdfkit/internal/i1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V

    return-void
.end method

.method static synthetic lambda$addOnAnnotationSelectedListener$54(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getAnnotationListeners()Lcom/pspdfkit/internal/i1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V

    return-void
.end method

.method static synthetic lambda$addOnAnnotationUpdatedListener$58(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getAnnotationListeners()Lcom/pspdfkit/internal/i1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    return-void
.end method

.method static synthetic lambda$addOnContentEditingContentChangeListener$68(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getContentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;->addOnContentEditingContentChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;)V

    return-void
.end method

.method static synthetic lambda$addOnContentEditingModeChangeListener$66(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getContentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;->addOnContentEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;)V

    return-void
.end method

.method static synthetic lambda$addOnFormElementClickedListener$91(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getFormListeners()Lcom/pspdfkit/internal/ac;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->addOnFormElementClickedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;)V

    return-void
.end method

.method static synthetic lambda$addOnFormElementDeselectedListener$85(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getFormListeners()Lcom/pspdfkit/internal/ac;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->addOnFormElementDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;)V

    return-void
.end method

.method static synthetic lambda$addOnFormElementEditingModeChangeListener$89(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getFormListeners()Lcom/pspdfkit/internal/ac;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->addOnFormElementEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V

    return-void
.end method

.method static synthetic lambda$addOnFormElementSelectedListener$83(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getFormListeners()Lcom/pspdfkit/internal/ac;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->addOnFormElementSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V

    return-void
.end method

.method static synthetic lambda$addOnFormElementUpdatedListener$87(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getFormListeners()Lcom/pspdfkit/internal/ac;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->addOnFormElementUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;)V

    return-void
.end method

.method static synthetic lambda$addOnFormElementViewUpdatedListener$93(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getFormListeners()Lcom/pspdfkit/internal/ac;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->addOnFormElementViewUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;)V

    return-void
.end method

.method static synthetic lambda$addOnTextSelectionChangeListener$80(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getTextSelectionListeners()Lcom/pspdfkit/internal/yt;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/yt;->addOnTextSelectionChangeListener(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionChangeListener;)V

    return-void
.end method

.method static synthetic lambda$addOnTextSelectionModeChangeListener$78(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionModeChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getTextSelectionListeners()Lcom/pspdfkit/internal/yt;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/yt;->addOnTextSelectionModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionModeChangeListener;)V

    return-void
.end method

.method static synthetic lambda$addOverlayViewProvider$52(Lcom/pspdfkit/ui/overlay/OverlayViewProvider;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/ui/overlay/OverlayViewProvider;)V

    return-void
.end method

.method static synthetic lambda$enterContentEditingMode$71(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->e()V

    return-void
.end method

.method static synthetic lambda$enterFormEditingMode$76(Lcom/pspdfkit/forms/FormElement;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/forms/FormElement;)V

    return-void
.end method

.method static synthetic lambda$enterTextSelectionMode$73(ILcom/pspdfkit/datastructures/Range;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p2, p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(ILcom/pspdfkit/datastructures/Range;)V

    return-void
.end method

.method static synthetic lambda$enterTextSelectionMode$74(ILjava/util/ArrayList;Ljava/util/ArrayList;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/datastructures/TextSelectionRectangles;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/datastructures/TextSelectionRectangles;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {p3, p0, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(ILcom/pspdfkit/datastructures/TextSelectionRectangles;)V

    return-void
.end method

.method static synthetic lambda$executeAction$40(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->getActionResolver()Lcom/pspdfkit/annotations/actions/ActionResolver;

    move-result-object p2

    invoke-interface {p2, p0, p1}, Lcom/pspdfkit/annotations/actions/ActionResolver;->executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V

    return-void
.end method

.method static synthetic lambda$exitCurrentlyActiveMode$77(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->exitCurrentlyActiveMode()V

    return-void
.end method

.method static synthetic lambda$load$13([Ljava/lang/Object;)Ljava/lang/Double;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    array-length v0, p0

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_0

    aget-object v4, p0, v3

    .line 2
    check-cast v4, Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    add-double/2addr v1, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 4
    :cond_0
    array-length p0, p0

    int-to-double v3, p0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$notifyLayoutChanged$82(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->p()V

    return-void
.end method

.method static synthetic lambda$onDocumentLoaded$45(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfFragment"

    const-string v2, "Unable to initialize document data store to restore the last viewed page."

    .line 1
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$onDocumentLoaded$46(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/internal/i8;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i8;->a(Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/h8;

    move-result-object p0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h8;->a()V

    return-void
.end method

.method static synthetic lambda$onDocumentLoaded$47(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfFragment"

    const-string v2, "Unable to initialize document data store to clear the last viewed page."

    .line 1
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$onStop$10(Lcom/pspdfkit/document/PdfDocument;ILcom/pspdfkit/internal/i8;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p2, p0}, Lcom/pspdfkit/internal/i8;->a(Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/h8;

    move-result-object p0

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/h8;->b(I)V

    return-void
.end method

.method static synthetic lambda$onStop$11(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfFragment"

    const-string v2, "Unable to initialize document data store to save the last viewed page."

    .line 1
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$onStop$8(Lcom/pspdfkit/document/PdfDocument;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p0

    invoke-interface {p0}, Lcom/pspdfkit/annotations/AnnotationProvider;->hasUnsavedChanges()Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onStop$9(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Boolean;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/yl;->a(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    goto :goto_0

    .line 3
    :cond_0
    new-instance p0, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda2;

    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda2;-><init>()V

    :goto_0
    return-object p0
.end method

.method static synthetic lambda$removeDocumentActionListener$42(Lcom/pspdfkit/document/DocumentActionListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getActionResolver()Lcom/pspdfkit/annotations/actions/ActionResolver;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/annotations/actions/ActionResolver;->removeDocumentActionListener(Lcom/pspdfkit/document/DocumentActionListener;)V

    return-void
.end method

.method static synthetic lambda$removeDrawableProvider$51(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->removeDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    return-void
.end method

.method static synthetic lambda$removeOnAnnotationCreationModeChangeListener$61(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getAnnotationListeners()Lcom/pspdfkit/internal/i1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnAnnotationCreationModeSettingsChangeListener$63(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getAnnotationListeners()Lcom/pspdfkit/internal/i1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnAnnotationDeselectedListener$57(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getAnnotationListeners()Lcom/pspdfkit/internal/i1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnAnnotationEditingModeChangeListener$65(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getAnnotationListeners()Lcom/pspdfkit/internal/i1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnAnnotationSelectedListener$55(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getAnnotationListeners()Lcom/pspdfkit/internal/i1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnAnnotationUpdatedListener$59(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getAnnotationListeners()Lcom/pspdfkit/internal/i1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnContentEditingContentChangeListener$69(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getContentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;->removeOnContentEditingContentChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnContentEditingModeChangeListener$67(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getContentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;->removeOnContentEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnFormElementClickedListener$92(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getFormListeners()Lcom/pspdfkit/internal/ac;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->removeOnFormElementClickedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnFormElementDeselectedListener$86(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getFormListeners()Lcom/pspdfkit/internal/ac;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->removeOnFormElementDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnFormElementEditingModeChangeListener$90(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getFormListeners()Lcom/pspdfkit/internal/ac;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->removeOnFormElementEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnFormElementSelectedListener$84(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getFormListeners()Lcom/pspdfkit/internal/ac;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->removeOnFormElementSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnFormElementUpdatedListener$88(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getFormListeners()Lcom/pspdfkit/internal/ac;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->removeOnFormElementUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnFormElementViewUpdatedListener$94(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getFormListeners()Lcom/pspdfkit/internal/ac;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->removeOnFormElementViewUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnTextSelectionChangeListener$81(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getTextSelectionListeners()Lcom/pspdfkit/internal/yt;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/yt;->removeOnTextSelectionChangeListener(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionChangeListener;)V

    return-void
.end method

.method static synthetic lambda$removeOnTextSelectionModeChangeListener$79(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionModeChangeListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getTextSelectionListeners()Lcom/pspdfkit/internal/yt;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/yt;->removeOnTextSelectionModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionModeChangeListener;)V

    return-void
.end method

.method static synthetic lambda$removeOverlayViewProvider$53(Lcom/pspdfkit/ui/overlay/OverlayViewProvider;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Lcom/pspdfkit/ui/overlay/OverlayViewProvider;)V

    return-void
.end method

.method static synthetic lambda$restoreContentEditing$5(Lcom/pspdfkit/internal/w6;Ljava/lang/Integer;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/w6;->a()I

    move-result p0

    if-ne p1, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$restoreTextSelection$3(Lcom/pspdfkit/datastructures/TextSelection;Ljava/lang/Integer;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget p0, p0, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    if-ne p1, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$scrollTo$34(Landroid/graphics/RectF;IJZLcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 6

    move-object v0, p5

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move v5, p4

    .line 1
    invoke-virtual/range {v0 .. v5}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Landroid/graphics/RectF;IJZ)V

    return-void
.end method

.method static lambda$setDocumentInsets$70(IIIILcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 2

    .line 1
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2
    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_2

    .line 6
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 7
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-ne v1, p1, :cond_0

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    if-ne v1, p3, :cond_0

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-ne v1, p0, :cond_0

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    if-eq v1, p2, :cond_1

    .line 11
    :cond_0
    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 12
    invoke-virtual {p4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void

    .line 13
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Can\'t add document insets if DocumentView parent does not support margins."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method static synthetic lambda$setOnPreparePopupToolbarListener$37(Lcom/pspdfkit/listeners/OnPreparePopupToolbarListener;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setOnPreparePopupToolbarListener(Lcom/pspdfkit/listeners/OnPreparePopupToolbarListener;)V

    return-void
.end method

.method static synthetic lambda$setOverlaidAnnotationTypes$95(Ljava/util/EnumSet;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setOverlaidAnnotationTypes(Ljava/util/EnumSet;)V

    return-void
.end method

.method static synthetic lambda$setOverlaidAnnotations$96(Ljava/util/List;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setOverlaidAnnotations(Ljava/util/List;)V

    return-void
.end method

.method static synthetic lambda$setPageIndex$28(ILcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setPage(I)V

    return-void
.end method

.method static synthetic lambda$setSelectedAnnotations$21(ILjava/lang/Integer;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p1, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$setSelectedFormElement$26(ILjava/lang/Integer;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p1, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$zoomBy$31(IIIFJLcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 7

    move-object v0, p6

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-wide v5, p4

    .line 1
    invoke-virtual/range {v0 .. v6}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(IIIFJ)V

    return-void
.end method

.method static synthetic lambda$zoomTo$32(IIIFJLcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 7

    move-object v0, p6

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-wide v5, p4

    .line 1
    invoke-virtual/range {v0 .. v6}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(IIIFJ)V

    return-void
.end method

.method static synthetic lambda$zoomTo$33(Landroid/graphics/RectF;IJLcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p4, p0, p1, p2, p3}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Landroid/graphics/RectF;IJ)V

    return-void
.end method

.method private load()V
    .locals 5

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfFragment"

    const-string v2, "Load invoked without initializing PSPDFKit, skipping..."

    .line 2
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_1

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->shouldReloadDocument()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 14
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    .line 18
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->imageDocumentSource:Lcom/pspdfkit/document/DocumentSource;

    if-eqz v0, :cond_3

    .line 19
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->openImageDocumentAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 20
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 21
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda87;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda87;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 22
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/BiConsumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 41
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->lifecycleDisposable:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v1, v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    goto :goto_0

    .line 44
    :cond_3
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getDocumentLoadingProgressObservables()Ljava/util/List;

    move-result-object v0

    .line 45
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 49
    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda88;

    invoke-direct {v1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda88;-><init>()V

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lio/reactivex/rxjava3/core/Flowable;->combineLatest(Ljava/lang/Iterable;Lio/reactivex/rxjava3/functions/Function;I)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x7d0

    .line 59
    invoke-virtual {v0, v3, v4, v1}, Lio/reactivex/rxjava3/core/Flowable;->delaySubscription(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    .line 60
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v3

    invoke-virtual {v0, v3}, Lio/reactivex/rxjava3/core/Flowable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    const-wide/16 v3, 0x10

    .line 61
    invoke-virtual {v0, v3, v4, v1, v2}, Lio/reactivex/rxjava3/core/Flowable;->sample(JLjava/util/concurrent/TimeUnit;Z)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    .line 62
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Flowable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$4;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/PdfFragment$4;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 63
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Flowable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadingProgressDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 77
    :cond_4
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->openDocumentAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 78
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 79
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda89;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda89;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 80
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda90;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda90;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 81
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/BiConsumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 100
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->lifecycleDisposable:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v1, v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    :goto_0
    return-void
.end method

.method public static newImageInstance(Landroid/net/Uri;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 3

    const-string v0, "documentUri"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    new-instance v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v0, p0}, Lcom/pspdfkit/document/DocumentSource;-><init>(Landroid/net/Uri;)V

    invoke-static {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->newImageInstance(Lcom/pspdfkit/document/DocumentSource;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    return-object p0
.end method

.method public static newImageInstance(Lcom/pspdfkit/document/DocumentSource;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 2

    .line 215
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "PSPDFKit.Configuration"

    .line 216
    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 218
    invoke-static {p0}, Lcom/pspdfkit/internal/mm;->a(Lcom/pspdfkit/document/DocumentSource;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 219
    new-instance p1, Lcom/pspdfkit/internal/mm;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/mm;-><init>(Lcom/pspdfkit/document/DocumentSource;)V

    const-string v1, "PSPDFKit.ImageDocument.Source"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 222
    :cond_0
    new-instance p1, Lcom/pspdfkit/ui/PdfFragment;

    invoke-direct {p1}, Lcom/pspdfkit/ui/PdfFragment;-><init>()V

    .line 223
    invoke-virtual {p1, v0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 224
    invoke-static {p0}, Lcom/pspdfkit/internal/mm;->a(Lcom/pspdfkit/document/DocumentSource;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 225
    iput-object p0, p1, Lcom/pspdfkit/ui/PdfFragment;->imageDocumentSource:Lcom/pspdfkit/document/DocumentSource;

    :cond_1
    return-object p1
.end method

.method public static newImageInstance(Lcom/pspdfkit/document/providers/DataProvider;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 3

    const-string v0, "source"

    const-string v1, "argumentName"

    .line 109
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 160
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 162
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 214
    new-instance v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v0, p0}, Lcom/pspdfkit/document/DocumentSource;-><init>(Lcom/pspdfkit/document/providers/DataProvider;)V

    invoke-static {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->newImageInstance(Lcom/pspdfkit/document/DocumentSource;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    return-object p0
.end method

.method public static newInstance(Landroid/net/Uri;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    const/4 v0, 0x0

    .line 182
    invoke-static {p0, v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->newInstance(Landroid/net/Uri;Ljava/lang/String;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    return-object p0
.end method

.method public static newInstance(Landroid/net/Uri;Ljava/lang/String;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    const/4 v0, 0x0

    .line 183
    invoke-static {p0, p1, v0, p2}, Lcom/pspdfkit/ui/PdfFragment;->newInstance(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    return-object p0
.end method

.method public static newInstance(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 3

    const-string v0, "documentUri"

    const-string v1, "argumentName"

    .line 185
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 236
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 238
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 289
    invoke-static {p3, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 290
    new-instance v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/document/DocumentSource;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    .line 292
    invoke-static {p0, p3}, Lcom/pspdfkit/ui/PdfFragment;->newInstanceFromDocumentSources(Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    return-object p0
.end method

.method public static newInstance(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    .line 405
    invoke-interface {p0}, Lcom/pspdfkit/document/PdfDocument;->getDocumentSources()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->newInstanceFromDocumentSources(Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    .line 406
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/ag;->setDocument(Lcom/pspdfkit/document/PdfDocument;)V

    return-object p1
.end method

.method public static newInstance(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    const/4 v0, 0x0

    .line 295
    invoke-static {p0, p1, v0, p2}, Lcom/pspdfkit/ui/PdfFragment;->newInstance(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    return-object p0
.end method

.method public static newInstance(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 3

    const-string v0, "source"

    const-string v1, "argumentName"

    .line 297
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 348
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 350
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 401
    invoke-static {p3, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 402
    new-instance v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/document/DocumentSource;-><init>(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    .line 404
    invoke-static {p0, p3}, Lcom/pspdfkit/ui/PdfFragment;->newInstanceFromDocumentSources(Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    return-object p0
.end method

.method public static newInstance(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 2

    .line 407
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getState()Landroid/os/Bundle;

    move-result-object v0

    .line 408
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 409
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->newInstance(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    .line 410
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/PdfFragment;->setState(Landroid/os/Bundle;)V

    return-object p0

    .line 413
    :cond_0
    iget-object p0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSources:Ljava/util/List;

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->newInstanceFromDocumentSources(Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    .line 414
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/PdfFragment;->setState(Landroid/os/Bundle;)V

    return-object p0
.end method

.method public static newInstance(Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/pspdfkit/configuration/PdfConfiguration;",
            ")",
            "Lcom/pspdfkit/ui/PdfFragment;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 293
    invoke-static {p0, v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->newInstance(Ljava/util/List;Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    return-object p0
.end method

.method public static newInstance(Ljava/util/List;Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/pspdfkit/configuration/PdfConfiguration;",
            ")",
            "Lcom/pspdfkit/ui/PdfFragment;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 294
    invoke-static {p0, p1, v0, p2}, Lcom/pspdfkit/ui/PdfFragment;->newInstance(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    return-object p0
.end method

.method public static newInstance(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/pspdfkit/configuration/PdfConfiguration;",
            ")",
            "Lcom/pspdfkit/ui/PdfFragment;"
        }
    .end annotation

    const-string v0, "documentUris"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 52
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 53
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-static {p3, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "uris"

    .line 105
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 169
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_4

    .line 171
    new-instance v4, Lcom/pspdfkit/document/DocumentSource;

    .line 172
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    if-nez p1, :cond_0

    goto :goto_1

    .line 173
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-gt v6, v3, :cond_1

    :goto_1
    move-object v6, v2

    goto :goto_2

    .line 174
    :cond_1
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    .line 175
    :goto_2
    check-cast v6, Ljava/lang/String;

    if-nez p2, :cond_2

    goto :goto_3

    .line 176
    :cond_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    if-gt v7, v3, :cond_3

    :goto_3
    move-object v7, v2

    goto :goto_4

    .line 177
    :cond_3
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .line 178
    :goto_4
    check-cast v7, Ljava/lang/String;

    .line 179
    invoke-direct {v4, v5, v6, v7}, Lcom/pspdfkit/document/DocumentSource;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 181
    :cond_4
    invoke-static {v0, p3}, Lcom/pspdfkit/ui/PdfFragment;->newInstanceFromDocumentSources(Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    return-object p0
.end method

.method public static newInstanceFromDocumentSources(Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;",
            "Lcom/pspdfkit/configuration/PdfConfiguration;",
            ")",
            "Lcom/pspdfkit/ui/PdfFragment;"
        }
    .end annotation

    .line 1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "PSPDFKit.Configuration"

    .line 2
    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 5
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/document/DocumentSource;

    .line 6
    invoke-static {v1}, Lcom/pspdfkit/internal/mm;->a(Lcom/pspdfkit/document/DocumentSource;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    :goto_0
    if-eqz p1, :cond_3

    .line 7
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/pspdfkit/internal/mm;

    .line 8
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 9
    new-instance v3, Lcom/pspdfkit/internal/mm;

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v3, v4}, Lcom/pspdfkit/internal/mm;-><init>(Lcom/pspdfkit/document/DocumentSource;)V

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const-string v2, "PSPDFKit.Sources"

    .line 10
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 13
    :cond_3
    new-instance v1, Lcom/pspdfkit/ui/PdfFragment;

    invoke-direct {v1}, Lcom/pspdfkit/ui/PdfFragment;-><init>()V

    .line 14
    invoke-virtual {v1, v0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    if-nez p1, :cond_4

    .line 16
    invoke-virtual {v1, p0}, Lcom/pspdfkit/ui/PdfFragment;->setCustomPdfSources(Ljava/util/List;)V

    :cond_4
    return-object v1
.end method

.method public static newInstanceFromSources(Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/providers/DataProvider;",
            ">;",
            "Lcom/pspdfkit/configuration/PdfConfiguration;",
            ")",
            "Lcom/pspdfkit/ui/PdfFragment;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 160
    invoke-static {p0, v0, v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->newInstanceFromSources(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    return-object p0
.end method

.method public static newInstanceFromSources(Ljava/util/List;Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/providers/DataProvider;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/pspdfkit/configuration/PdfConfiguration;",
            ")",
            "Lcom/pspdfkit/ui/PdfFragment;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 161
    invoke-static {p0, p1, v0, p2}, Lcom/pspdfkit/ui/PdfFragment;->newInstanceFromSources(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    return-object p0
.end method

.method public static newInstanceFromSources(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/providers/DataProvider;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/pspdfkit/configuration/PdfConfiguration;",
            ")",
            "Lcom/pspdfkit/ui/PdfFragment;"
        }
    .end annotation

    const-string v0, "sources"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 52
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v3, "configuration"

    .line 53
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-static {p3, v3, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 105
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 147
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_4

    .line 149
    new-instance v4, Lcom/pspdfkit/document/DocumentSource;

    .line 150
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/document/providers/DataProvider;

    if-nez p1, :cond_0

    goto :goto_1

    .line 151
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-gt v6, v3, :cond_1

    :goto_1
    move-object v6, v2

    goto :goto_2

    .line 152
    :cond_1
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    .line 153
    :goto_2
    check-cast v6, Ljava/lang/String;

    if-nez p2, :cond_2

    goto :goto_3

    .line 154
    :cond_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    if-gt v7, v3, :cond_3

    :goto_3
    move-object v7, v2

    goto :goto_4

    .line 155
    :cond_3
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .line 156
    :goto_4
    check-cast v7, Ljava/lang/String;

    .line 157
    invoke-direct {v4, v5, v6, v7}, Lcom/pspdfkit/document/DocumentSource;-><init>(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 159
    :cond_4
    invoke-static {v0, p3}, Lcom/pspdfkit/ui/PdfFragment;->newInstanceFromDocumentSources(Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    return-object p0
.end method

.method private onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3

    const/high16 v0, 0x3f800000    # 1.0f

    .line 1
    iput v0, p0, Lcom/pspdfkit/ui/PdfFragment;->startZoomScale:F

    const-string v0, "PSPDFKit.PSPDFFragmentState"

    .line 2
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->fragmentState:Landroid/os/Bundle;

    const-string v0, "PSPDFKit.UserP"

    .line 5
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 7
    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/PdfFragment;->setPassword(Ljava/lang/String;)V

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->signatureFormSigningHandler:Lcom/pspdfkit/internal/pr;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/pr;->a(Landroid/os/Bundle;)V

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->fragmentState:Landroid/os/Bundle;

    if-nez p1, :cond_1

    return-void

    :cond_1
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-string v2, "PSPDFKit.DocumentLoadingProgress"

    .line 15
    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->setDocumentLoadingProgressState(D)V

    return-void
.end method

.method private openImageDocumentAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/document/ImageDocument;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->imageDocumentSource:Lcom/pspdfkit/document/DocumentSource;

    invoke-static {v0, v1}, Lcom/pspdfkit/document/ImageDocumentLoader;->openDocumentAsync(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method private pepareAnnotationUndoManager(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 5

    .line 1
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->undoManager:Lcom/pspdfkit/internal/vu;

    new-instance v2, Lcom/pspdfkit/internal/y;

    check-cast p1, Lcom/pspdfkit/internal/zf;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v3

    iget-object v4, p0, Lcom/pspdfkit/ui/PdfFragment;->navigateOnUndoListener:Lcom/pspdfkit/internal/k4$a;

    invoke-direct {v2, v3, v0, p0, v4}, Lcom/pspdfkit/internal/y;-><init>(Lcom/pspdfkit/internal/qf;Landroid/util/SparseIntArray;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/k4$a;)V

    .line 6
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/vu;->a(Lcom/pspdfkit/internal/k4;)V

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->undoManager:Lcom/pspdfkit/internal/vu;

    new-instance v2, Lcom/pspdfkit/internal/q1;

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v3

    iget-object v4, p0, Lcom/pspdfkit/ui/PdfFragment;->navigateOnUndoListener:Lcom/pspdfkit/internal/k4$a;

    invoke-direct {v2, v3, v0, p0, v4}, Lcom/pspdfkit/internal/q1;-><init>(Lcom/pspdfkit/internal/qf;Landroid/util/SparseIntArray;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/k4$a;)V

    .line 15
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/vu;->a(Lcom/pspdfkit/internal/k4;)V

    .line 22
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->undoManager:Lcom/pspdfkit/internal/vu;

    new-instance v2, Lcom/pspdfkit/internal/u3;

    .line 23
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v3

    iget-object v4, p0, Lcom/pspdfkit/ui/PdfFragment;->navigateOnUndoListener:Lcom/pspdfkit/internal/k4$a;

    invoke-direct {v2, v3, v0, v4}, Lcom/pspdfkit/internal/u3;-><init>(Lcom/pspdfkit/internal/qf;Landroid/util/SparseIntArray;Lcom/pspdfkit/internal/k4$a;)V

    .line 24
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/vu;->a(Lcom/pspdfkit/internal/k4;)V

    .line 28
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->undoManager:Lcom/pspdfkit/internal/vu;

    new-instance v2, Lcom/pspdfkit/internal/l2;

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p1

    iget-object v3, p0, Lcom/pspdfkit/ui/PdfFragment;->navigateOnUndoListener:Lcom/pspdfkit/internal/k4$a;

    invoke-direct {v2, p1, v0, v3}, Lcom/pspdfkit/internal/l2;-><init>(Lcom/pspdfkit/internal/qf;Landroid/util/SparseIntArray;Lcom/pspdfkit/internal/k4$a;)V

    .line 30
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/vu;->a(Lcom/pspdfkit/internal/k4;)V

    return-void
.end method

.method private prepareContentEditingUndoManager()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda84;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda84;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    const/4 v2, 0x0

    .line 2
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method private prepareUndoManager(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->pepareAnnotationUndoManager(Lcom/pspdfkit/document/PdfDocument;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->prepareContentEditingUndoManager()V

    return-void
.end method

.method private previewUri(Landroid/content/Context;Lcom/pspdfkit/annotations/actions/UriAction;)V
    .locals 5

    .line 1
    new-instance v0, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2
    sget v1, Lcom/pspdfkit/R$layout;->pspdf__preview_uri_dialog:I

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(I)Landroidx/appcompat/app/AlertDialog$Builder;

    .line 3
    invoke-virtual {v0}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 6
    sget v1, Lcom/pspdfkit/R$id;->pspdf__uri_item_link:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 7
    sget v2, Lcom/pspdfkit/R$id;->pspdf__uri_item_open:I

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 8
    sget v3, Lcom/pspdfkit/R$id;->pspdf__uri_item_copy:I

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 10
    new-instance v4, Landroid/widget/Scroller;

    invoke-direct {v4, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setScroller(Landroid/widget/Scroller;)V

    const/4 v4, 0x1

    .line 11
    invoke-virtual {v1, v4}, Landroid/view/View;->setVerticalScrollBarEnabled(Z)V

    .line 12
    new-instance v4, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v4}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 13
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/actions/UriAction;->getUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 15
    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda26;

    invoke-direct {v1, p0, p2, v0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda26;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/annotations/actions/UriAction;Landroidx/appcompat/app/AlertDialog;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda27;

    invoke-direct {v1, p0, p1, p2, v0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda27;-><init>(Lcom/pspdfkit/ui/PdfFragment;Landroid/content/Context;Lcom/pspdfkit/annotations/actions/UriAction;Landroidx/appcompat/app/AlertDialog;)V

    invoke-virtual {v3, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private refreshUserInterfaceState()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSaver:Lcom/pspdfkit/internal/p9;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/p9;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->setUserInterfaceEnabledInternal(ZZ)V

    return-void
.end method

.method private resetDocument()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->internalDocumentListener:Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/zf;->b(Lcom/pspdfkit/internal/zf$f;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ig;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ig;->b()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->e()Lcom/pspdfkit/internal/uf;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->formFieldUpdatedListener:Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;

    invoke-interface {v0, v2}, Lcom/pspdfkit/forms/FormProvider;->removeOnFormFieldUpdatedListener(Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;)V

    .line 5
    iput-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSaver:Lcom/pspdfkit/internal/p9;

    if-eqz v0, :cond_1

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/internal/p9;->e()V

    .line 9
    iput-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSaver:Lcom/pspdfkit/internal/p9;

    :cond_1
    return-void
.end method

.method private restoreContentEditing(Lcom/pspdfkit/internal/ns;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ns;->c()Lcom/pspdfkit/internal/w6;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/w6;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->enterContentEditingMode()V

    goto :goto_0

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->lifecycleDisposable:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->pageChangeSubject:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    new-instance v2, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda45;

    invoke-direct {v2, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda45;-><init>(Lcom/pspdfkit/internal/w6;)V

    .line 7
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 8
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Observable;->firstOrError()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda46;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda46;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 9
    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    .line 10
    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    :goto_0
    return-void
.end method

.method private restorePagePosition(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 9

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/PdfFragment;->startZoomScale:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->animatePageTransition:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0xc8

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 3
    :goto_1
    iget v1, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    .line 4
    invoke-interface {p1, v1}, Lcom/pspdfkit/document/PdfDocument;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v1

    iget v1, v1, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v1, v1

    div-int/lit8 v3, v1, 0x2

    iget v1, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    .line 5
    invoke-interface {p1, v1}, Lcom/pspdfkit/document/PdfDocument;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int p1, p1

    div-int/lit8 v4, p1, 0x2

    iget v5, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    iget v6, p0, Lcom/pspdfkit/ui/PdfFragment;->startZoomScale:F

    int-to-long v7, v0

    move-object v2, p0

    .line 6
    invoke-virtual/range {v2 .. v8}, Lcom/pspdfkit/ui/PdfFragment;->zoomTo(IIIFJ)V

    goto :goto_2

    .line 13
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->animatePageTransition:Ljava/lang/Boolean;

    if-nez p1, :cond_3

    .line 14
    iget p1, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(I)V

    goto :goto_2

    .line 16
    :cond_3
    iget v0, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(IZ)V

    :goto_2
    return-void
.end method

.method private restoreSelectedAnnotations(Lcom/pspdfkit/internal/ns;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ns;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->lifecycleDisposable:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    .line 3
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/ns;->a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 4
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda99;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda99;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 5
    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    .line 6
    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    const/4 p1, 0x1

    return p1
.end method

.method private restoreSelectedFormElements(Lcom/pspdfkit/internal/ns;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ns;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->lifecycleDisposable:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    .line 3
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/ns;->b(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 4
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda56;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda56;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 5
    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    .line 6
    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    const/4 p1, 0x1

    return p1
.end method

.method private restoreTextSelection(Lcom/pspdfkit/internal/ns;)Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ns;->d()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 3
    :cond_0
    iget v0, p1, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    if-nez v0, :cond_1

    .line 4
    iget-object p1, p1, Lcom/pspdfkit/datastructures/TextSelection;->textRange:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->enterTextSelectionMode(ILcom/pspdfkit/datastructures/Range;)V

    goto :goto_0

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->lifecycleDisposable:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->pageChangeSubject:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    new-instance v2, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda10;

    invoke-direct {v2, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/datastructures/TextSelection;)V

    .line 7
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v1

    .line 8
    invoke-virtual {v1}, Lio/reactivex/rxjava3/core/Observable;->firstOrError()Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda11;

    invoke-direct {v2, p0, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda11;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/datastructures/TextSelection;)V

    .line 9
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    .line 10
    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method private setDocumentInsets(IIII)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda83;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda83;-><init>(IIII)V

    const/4 p1, 0x0

    .line 2
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method private setDocumentLoadingProgressState(D)V
    .locals 3

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpg-double v2, p1, v0

    if-gez v2, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/xm;->a(D)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadingProgressDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result p1

    if-nez p1, :cond_0

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/xm;->A()V

    :cond_0
    return-void
.end method

.method private setFragmentUiState(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "PSPDFKit.ViewState"

    .line 1
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ug$a;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/PdfFragment;->setViewState(Lcom/pspdfkit/internal/ug$a;)V

    :cond_0
    const-string v0, "PSPDFKit.SpecialModeState"

    .line 7
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ns;

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/PdfFragment;->setSpecialModeState(Lcom/pspdfkit/internal/ns;)V

    const-string v0, "PSPDFKit.MediaContentStates"

    .line 11
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 12
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/PdfFragment;->setMediaContentStates(Ljava/util/List;)V

    const-string v0, "PSPDFKit.AudioManagerState"

    .line 15
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/v3;

    if-eqz v0, :cond_2

    .line 17
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->audioModeManager:Lcom/pspdfkit/internal/a3;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/a3;->a(Lcom/pspdfkit/internal/v3;)V

    :cond_2
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-string v2, "PSPDFKit.DocumentLoadingProgress"

    .line 21
    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->setDocumentLoadingProgressState(D)V

    return-void
.end method

.method private setMediaContentStates(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/xi;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "PdfFragment#setMediaContentStates() may only be called from the main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/xm;->a(Ljava/util/List;)V

    return-void
.end method

.method private setPassword(Ljava/lang/String;)V
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSources:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/document/DocumentSource;

    .line 3
    invoke-virtual {v2, p1}, Lcom/pspdfkit/document/DocumentSource;->cloneWithPassword(Ljava/lang/String;)Lcom/pspdfkit/document/DocumentSource;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5
    :cond_0
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSources:Ljava/util/List;

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->password:Ljava/lang/String;

    return-void
.end method

.method private setSpecialModeState(Lcom/pspdfkit/internal/ns;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 1
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ns;->a()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ns;->b()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ns;->a()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object p1

    if-nez v0, :cond_1

    .line 5
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    .line 6
    :cond_1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->enterAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    return-void

    .line 13
    :cond_2
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->restoreSelectedAnnotations(Lcom/pspdfkit/internal/ns;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 14
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->restoreSelectedFormElements(Lcom/pspdfkit/internal/ns;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 15
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->restoreTextSelection(Lcom/pspdfkit/internal/ns;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 16
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->restoreContentEditing(Lcom/pspdfkit/internal/ns;)V

    :cond_3
    return-void
.end method

.method private setUserInterfaceEnabledInternal(ZZ)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda97;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda97;-><init>(Lcom/pspdfkit/ui/PdfFragment;ZZ)V

    const/4 p1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$d;Z)V

    return-void
.end method


# virtual methods
.method public addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, p2, v0}, Lcom/pspdfkit/ui/PdfFragment;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;ZLjava/lang/Runnable;)V

    return-void
.end method

.method public addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;ZLjava/lang/Runnable;)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v2, "PdfFragment#addAnnotationToPage() may only be called after document has been loaded."

    invoke-static {v2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    const-string v0, "annotation"

    const-string v2, "argumentName"

    .line 6
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 57
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 58
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v2, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda96;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda96;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/annotations/Annotation;ZLjava/lang/Runnable;)V

    .line 61
    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addDocumentActionListener(Lcom/pspdfkit/document/DocumentActionListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda51;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda51;-><init>(Lcom/pspdfkit/document/DocumentActionListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V
    .locals 2

    const-string v0, "documentListener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public addDocumentScrollListener(Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;)V
    .locals 2

    const-string v0, "documentScrollListener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentScrollListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public addDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V
    .locals 2

    const-string v0, "drawableProvider"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda54;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda54;-><init>(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addInsets(IIII)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/PdfFragment;->insetsLeft:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/pspdfkit/ui/PdfFragment;->insetsLeft:I

    .line 2
    iget p1, p0, Lcom/pspdfkit/ui/PdfFragment;->insetsTop:I

    add-int/2addr p1, p2

    iput p1, p0, Lcom/pspdfkit/ui/PdfFragment;->insetsTop:I

    .line 3
    iget p2, p0, Lcom/pspdfkit/ui/PdfFragment;->insetsRight:I

    add-int/2addr p2, p3

    iput p2, p0, Lcom/pspdfkit/ui/PdfFragment;->insetsRight:I

    .line 4
    iget p3, p0, Lcom/pspdfkit/ui/PdfFragment;->insetsBottom:I

    add-int/2addr p3, p4

    iput p3, p0, Lcom/pspdfkit/ui/PdfFragment;->insetsBottom:I

    .line 5
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/pspdfkit/ui/PdfFragment;->setDocumentInsets(IIII)V

    return-void
.end method

.method public addOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda91;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda91;-><init>(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda75;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda75;-><init>(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda4;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda24;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda24;-><init>(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda22;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda22;-><init>(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda33;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda33;-><init>(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnContentEditingContentChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda80;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda80;-><init>(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnContentEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda47;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda47;-><init>(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnFormElementClickedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda12;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda12;-><init>(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnFormElementDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda28;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda28;-><init>(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnFormElementEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda67;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda67;-><init>(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnFormElementSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda92;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda92;-><init>(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnFormElementUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda53;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda53;-><init>(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnFormElementViewUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda49;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda49;-><init>(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnTextSelectionChangeListener(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda86;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda86;-><init>(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOnTextSelectionModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionModeChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda95;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda95;-><init>(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionModeChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public addOverlayViewProvider(Lcom/pspdfkit/ui/overlay/OverlayViewProvider;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "overlayViewProvider"

    const-string v1, "argumentName"

    .line 6
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 57
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 58
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda66;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda66;-><init>(Lcom/pspdfkit/ui/overlay/OverlayViewProvider;)V

    const/4 p1, 0x0

    .line 59
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void

    .line 60
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Using addOverlayViewProvider() requires the annotations component."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public beginNavigation()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationStartPage:Ljava/lang/Integer;

    return-void
.end method

.method public clearSelectedAnnotations()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "PdfFragment#clearSelectedAnnotations() may only be called from the main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->a()Z

    move-result v0

    return v0
.end method

.method public endNavigation()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationStartPage:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationEndPage:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 3
    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationHistory:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    new-instance v1, Lcom/pspdfkit/ui/navigation/NavigationBackStack$NavigationItem;

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationStartPage:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationEndPage:Ljava/lang/Integer;

    invoke-direct {v1, v2, v3}, Lcom/pspdfkit/ui/navigation/NavigationBackStack$NavigationItem;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->addItem(Ljava/lang/Object;)V

    :cond_0
    const/4 v0, 0x0

    .line 6
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationStartPage:Ljava/lang/Integer;

    .line 7
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationEndPage:Ljava/lang/Integer;

    return-void
.end method

.method public enterAnnotationCreationMode()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->get(Landroid/content/Context;)Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->getLastAnnotationTools()Ljava/util/List;

    move-result-object v0

    .line 3
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    sget-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NONE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_0

    :cond_0
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 5
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 6
    :goto_1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v2, v3, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    .line 8
    :cond_2
    sget-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NONE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 9
    :goto_2
    invoke-virtual {p0, v1, v0}, Lcom/pspdfkit/ui/PdfFragment;->enterAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    return-void
.end method

.method public enterAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)V
    .locals 1

    .line 10
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->enterAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    return-void
.end method

.method public enterAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 3

    const-string v0, "annotationTool"

    const-string v1, "argumentName"

    .line 12
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 63
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "annotationToolVariant"

    .line 65
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 117
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda17;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda17;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    const/4 p1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public enterAnnotationEditingMode(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda9;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/annotations/Annotation;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public enterContentEditingMode()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda58;

    invoke-direct {v1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda58;-><init>()V

    const/4 v2, 0x0

    .line 2
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public enterFormEditingMode(Lcom/pspdfkit/forms/FormElement;)V
    .locals 2

    const-string v0, "formElement"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda50;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda50;-><init>(Lcom/pspdfkit/forms/FormElement;)V

    const/4 p1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public enterTextSelectionMode(ILcom/pspdfkit/datastructures/Range;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v2, "Document must be loaded before entering text selection mode."

    invoke-static {v2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    if-ltz p1, :cond_2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 11
    invoke-virtual {p2}, Lcom/pspdfkit/datastructures/Range;->getEndPosition()I

    move-result v0

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2, p1}, Lcom/pspdfkit/internal/zf;->getPageTextLength(I)I

    move-result v2

    if-gt v0, v2, :cond_1

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v2, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda8;

    invoke-direct {v2, p1, p2}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda8;-><init>(ILcom/pspdfkit/datastructures/Range;)V

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void

    .line 16
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid textRange "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, ". Range exceeds text on page."

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 17
    :cond_2
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Invalid page index "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ". Valid page indexes are [0, "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    .line 20
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result p1

    sub-int/2addr p1, v1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public enterTextSelectionMode(ILcom/pspdfkit/datastructures/TextSelectionRectangles;)V
    .locals 12

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v3, "Document must be loaded before entering text selection mode."

    invoke-static {v3, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    if-ltz p1, :cond_6

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    if-ge p1, v0, :cond_6

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zf;->e(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 32
    iget-object v3, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v3, p1}, Lcom/pspdfkit/internal/zf;->f(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 34
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 35
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v6, 0x0

    .line 37
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v6, v7, :cond_4

    .line 38
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/RectF;

    .line 39
    invoke-virtual {p2}, Lcom/pspdfkit/datastructures/TextSelectionRectangles;->getMarkupRectangles()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/RectF;

    .line 40
    iget v10, v7, Landroid/graphics/RectF;->left:F

    iget v11, v9, Landroid/graphics/RectF;->right:F

    cmpg-float v10, v10, v11

    if-gez v10, :cond_2

    iget v10, v9, Landroid/graphics/RectF;->left:F

    iget v11, v7, Landroid/graphics/RectF;->right:F

    cmpg-float v10, v10, v11

    if-gez v10, :cond_2

    iget v10, v7, Landroid/graphics/RectF;->top:F

    iget v11, v9, Landroid/graphics/RectF;->bottom:F

    cmpl-float v10, v10, v11

    if-lez v10, :cond_2

    iget v9, v9, Landroid/graphics/RectF;->top:F

    iget v10, v7, Landroid/graphics/RectF;->bottom:F

    cmpl-float v9, v9, v10

    if-lez v9, :cond_2

    const/4 v9, 0x1

    goto :goto_3

    :cond_2
    const/4 v9, 0x0

    :goto_3
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 41
    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 42
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/RectF;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 48
    :cond_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 53
    iget-object p2, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v0, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda36;

    invoke-direct {v0, p1, v4, v5}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda36;-><init>(ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {p2, v0, v2}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void

    .line 54
    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid list of touched rectangles "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, ". Text rectangles on page don\'t contain any of the touchedTextRects."

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 55
    :cond_6
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid page index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ". Valid page indexes are [0, "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    .line 58
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result p1

    sub-int/2addr p1, v2

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public executeAction(Lcom/pspdfkit/annotations/actions/Action;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V

    return-void
.end method

.method public executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V
    .locals 2

    const-string v0, "action"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda5;

    invoke-direct {v1, p1, p2}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V

    const/4 p1, 0x0

    .line 56
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public exitCurrentlyActiveMode()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda82;

    invoke-direct {v1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda82;-><init>()V

    const/4 v2, 0x0

    .line 2
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    return-object v0
.end method

.method public getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->d()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    return-object v0
.end method

.method public getAnnotationConfiguration()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/xm;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/l1;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/l1;->a()Lcom/pspdfkit/internal/i0;

    move-result-object v0

    return-object v0

    .line 6
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getAnnotationConfiguration() must be called after views are created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/xm;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/l1;

    move-result-object v0

    return-object v0

    .line 6
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getAnnotationPreferences() must be called after views are created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getAudioModeManager()Lcom/pspdfkit/ui/audio/AudioModeManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->audioModeManager:Lcom/pspdfkit/internal/a3;

    return-object v0
.end method

.method public getBackgroundColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->e()I

    move-result v0

    return v0
.end method

.method public getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-object v0
.end method

.method public getContentEditingConfiguration()Lcom/pspdfkit/contentediting/ContentEditingFillColorConfiguration;
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/xm;->b(Landroid/content/Context;)Lcom/pspdfkit/internal/n6;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/n6;->a()Lcom/pspdfkit/internal/b6;

    move-result-object v0

    return-object v0

    .line 6
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getContentEditingConfiguration() must be called after views are created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getContentEditingPreferences()Lcom/pspdfkit/contentediting/defaults/ContentEditingPreferencesManager;
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/xm;->b(Landroid/content/Context;)Lcom/pspdfkit/internal/n6;

    move-result-object v0

    return-object v0

    .line 6
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getContentEditingPreferences() must be called after views are created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getContentEditingState()Lcom/pspdfkit/internal/w6;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->f()Lcom/pspdfkit/internal/w6;

    move-result-object v0

    return-object v0
.end method

.method public getContentEditingUndoManager()Lcom/pspdfkit/undo/UndoManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->contentEditingUndoManager:Lcom/pspdfkit/internal/vu;

    return-object v0
.end method

.method public getDocument()Lcom/pspdfkit/document/PdfDocument;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    return-object v0
.end method

.method protected getDocumentLoadingProgressObservables()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Ljava/lang/Double;",
            ">;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSources:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSources:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/document/DocumentSource;

    .line 3
    invoke-virtual {v2}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v3

    instance-of v3, v3, Lcom/pspdfkit/document/providers/ProgressDataProvider;

    if-eqz v3, :cond_0

    .line 4
    invoke-virtual {v2}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/document/providers/ProgressDataProvider;

    .line 7
    invoke-interface {v2}, Lcom/pspdfkit/document/providers/ProgressDataProvider;->observeProgress()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Flowable;->startWithItem(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getImageDocument()Lcom/pspdfkit/document/ImageDocument;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->imageDocument:Lcom/pspdfkit/document/ImageDocument;

    return-object v0
.end method

.method public getInternal()Lcom/pspdfkit/internal/ag;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->internalAPI:Lcom/pspdfkit/internal/ag;

    return-object v0
.end method

.method public getNavigationHistory()Lcom/pspdfkit/ui/navigation/NavigationBackStack;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/pspdfkit/ui/navigation/NavigationBackStack<",
            "Lcom/pspdfkit/ui/navigation/NavigationBackStack$NavigationItem<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationHistory:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    return-object v0
.end method

.method public getOverlaidAnnotationTypes()Ljava/util/EnumSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/xm;->a(Z)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getOverlaidAnnotationTypes()Ljava/util/EnumSet;

    move-result-object v0

    return-object v0

    .line 6
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getOverlaidAnnotationTypes() must be called after views are created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getOverlaidAnnotations()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/xm;->a(Z)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getOverlaidAnnotations()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 6
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getOverlaidAnnotations() must be called after views are created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getPageCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    return v0
.end method

.method public getPageIndex()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->j()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 4
    iget v0, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    :cond_1
    return v0
.end method

.method public getPasswordView()Lcom/pspdfkit/ui/PdfPasswordView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->k()Lcom/pspdfkit/ui/PdfPasswordView;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedAnnotations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->l()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedFormElement()Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->m()Lcom/pspdfkit/forms/FormElement;

    move-result-object v0

    return-object v0
.end method

.method public getSiblingPageIndex(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/xm;->d(I)I

    move-result p1

    return p1
.end method

.method public getSignatureMetadata()Lcom/pspdfkit/signatures/SignatureMetadata;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    return-object v0
.end method

.method public getSignatureStorage()Lcom/pspdfkit/signatures/storage/SignatureStorage;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->signatureStorage:Lcom/pspdfkit/signatures/storage/SignatureStorage;

    return-object v0
.end method

.method public getState()Landroid/os/Bundle;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->fragmentState:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    return-object v0

    .line 4
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/xm;->o()Lcom/pspdfkit/internal/ug$a;

    move-result-object v1

    const-string v2, "PSPDFKit.ViewState"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 10
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->getSpecialModeState()Lcom/pspdfkit/internal/ns;

    move-result-object v1

    const-string v2, "PSPDFKit.SpecialModeState"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->lastEnabledSpecialModeState:Lcom/pspdfkit/internal/ns;

    if-eqz v1, :cond_1

    .line 12
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->getSpecialModeState()Lcom/pspdfkit/internal/ns;

    move-result-object v1

    const-string v2, "PSPDFKit.LastEnabledSpecialModeState"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 16
    :cond_1
    iget-boolean v1, p0, Lcom/pspdfkit/ui/PdfFragment;->redactionAnnotationPreviewEnabled:Z

    const-string v2, "PSPDFKit.RedactionPreviewState"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 19
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->getMediaContentStates()Ljava/util/List;

    move-result-object v1

    .line 20
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 21
    check-cast v1, Ljava/util/ArrayList;

    const-string v2, "PSPDFKit.MediaContentStates"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 26
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->audioModeManager:Lcom/pspdfkit/internal/a3;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/a3;->b()Lcom/pspdfkit/internal/v3;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v2, "PSPDFKit.AudioManagerState"

    .line 28
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 32
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationHistory:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    const-string v2, "PSPDFKit.NavigationHistory"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 35
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/xm;->g()D

    move-result-wide v1

    const-string v3, "PSPDFKit.DocumentLoadingProgress"

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 39
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->isChangingConfigurations()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    .line 40
    :goto_0
    invoke-static {v1}, Lcom/pspdfkit/internal/gj;->a(Z)V

    return-object v0
.end method

.method public getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->n()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    return-object v0
.end method

.method public getUndoManager()Lcom/pspdfkit/undo/UndoManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->undoManager:Lcom/pspdfkit/internal/vu;

    return-object v0
.end method

.method public getViewProjection()Lcom/pspdfkit/projection/ViewProjection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewProjectionImpl:Lcom/pspdfkit/projection/ViewProjection;

    return-object v0
.end method

.method public getVisiblePages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->p()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getVisiblePdfRect(Landroid/graphics/RectF;I)Z
    .locals 2

    const-string v0, "targetRect"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/xm;->a(Landroid/graphics/RectF;I)Z

    move-result p1

    return p1
.end method

.method public getZoomScale(I)F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/xm;->a(I)F

    move-result p1

    return p1
.end method

.method public isDocumentInteractionEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfFragment;->isDocumentInteractionEnabled:Z

    return v0
.end method

.method isIdle()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isImageDocument()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->f()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 3
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->imageDocumentSource:Lcom/pspdfkit/document/DocumentSource;

    if-eqz v0, :cond_2

    return v2

    .line 6
    :cond_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v3, "PSPDFKit.ImageDocument.Source"

    .line 7
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method public isInSpecialMode()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    const/4 v1, 0x0

    .line 2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/xm;->a(Z)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public isRedactionAnnotationPreviewEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfFragment;->redactionAnnotationPreviewEnabled:Z

    return v0
.end method

.method public isScrollingEnabled()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->w()Z

    move-result v0

    return v0
.end method

.method public isUserInterfaceEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfFragment;->isUserInterfaceEnabled:Z

    return v0
.end method

.method public isZoomingEnabled()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->x()Z

    move-result v0

    return v0
.end method

.method synthetic lambda$addAnnotationToPage$23$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/annotations/Annotation;ZLjava/lang/Runnable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "create_annotation"

    .line 2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 7
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    if-eqz p2, :cond_0

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result p2

    if-ltz p2, :cond_0

    .line 14
    invoke-direct {p0, p2}, Lcom/pspdfkit/ui/PdfFragment;->getPageEditorForPage(I)Lcom/pspdfkit/internal/am;

    move-result-object p2

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/pspdfkit/annotations/Annotation;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 15
    invoke-virtual {p2, v0, v1}, Lcom/pspdfkit/internal/am;->a(Z[Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 17
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->enterAnnotationEditingMode(Lcom/pspdfkit/annotations/Annotation;)V

    :cond_0
    if-eqz p3, :cond_1

    .line 24
    invoke-interface {p3}, Ljava/lang/Runnable;->run()V

    :cond_1
    return-void
.end method

.method synthetic lambda$addAnnotationToPage$25$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/annotations/Annotation;ZLjava/lang/Runnable;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 1

    .line 1
    iget-object p4, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    if-nez p4, :cond_0

    return-void

    .line 5
    :cond_0
    invoke-virtual {p4}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p4

    .line 6
    check-cast p4, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p4, p1}, Lcom/pspdfkit/internal/r1;->addAnnotationToPageAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p4

    .line 7
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p4, v0}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p4

    new-instance v0, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda34;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda34;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/annotations/Annotation;ZLjava/lang/Runnable;)V

    new-instance p1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda35;

    invoke-direct {p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda35;-><init>()V

    .line 8
    invoke-virtual {p4, v0, p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method synthetic lambda$displayDocument$20$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/internal/xm$b;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->onDocumentLongPressListener:Lcom/pspdfkit/listeners/OnDocumentLongPressListener;

    if-nez v0, :cond_0

    .line 2
    iget-object p1, p1, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->defaultOnDocumentLongPressListener:Lcom/pspdfkit/listeners/OnDocumentLongPressListener;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setOnDocumentLongPressListener(Lcom/pspdfkit/listeners/OnDocumentLongPressListener;)V

    .line 4
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/xm;->d(Z)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/xm;->b(Z)V

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->refreshUserInterfaceState()V

    return-void
.end method

.method synthetic lambda$enterAnnotationCreationMode$72$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->isAnnotationCreatorSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    invoke-virtual {p3, p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->enterAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    goto :goto_0

    .line 13
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$6;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/pspdfkit/ui/PdfFragment$6;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    const/4 p1, 0x0

    .line 14
    invoke-static {v0, p1, v1}, Lcom/pspdfkit/ui/AnnotationCreatorInputDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/pspdfkit/ui/AnnotationCreatorInputDialogFragment$OnAnnotationCreatorSetListener;)V

    .line 27
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    const-string p2, "show_annotation_creator_dialog"

    .line 28
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    :goto_0
    return-void

    .line 30
    :cond_1
    new-instance p2, Lcom/pspdfkit/exceptions/PSPDFKitException;

    new-instance p3, Ljava/lang/StringBuilder;

    const-string v0, "Entering annotation creation mode for "

    invoke-direct {p3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " is not permitted, either by the license or configuration."

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method synthetic lambda$enterAnnotationEditingMode$75$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/annotations/Annotation;)V

    return-void

    .line 4
    :cond_0
    new-instance p2, Lcom/pspdfkit/exceptions/PSPDFKitException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Entering annotation editing mode for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " is not permitted, either by the license or configuration."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method synthetic lambda$handleDocumentLoadingError$18$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/ui/PdfPasswordView;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p2}, Lcom/pspdfkit/ui/PdfFragment;->setPassword(Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->load()V

    return-void
.end method

.method synthetic lambda$handleDocumentLoadingError$19$com-pspdfkit-ui-PdfFragment(Ljava/lang/String;ZLjava/lang/Throwable;Lcom/pspdfkit/internal/xm$b;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "failed_document_load"

    .line 2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "value"

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/xm;->c(Z)V

    const/4 p1, 0x1

    const/4 v1, 0x4

    if-eqz p2, :cond_1

    .line 9
    iget-object p2, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/xm;->k()Lcom/pspdfkit/ui/PdfPasswordView;

    move-result-object p2

    .line 10
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result p3

    if-nez p3, :cond_0

    .line 11
    invoke-virtual {p2}, Lcom/pspdfkit/ui/PdfPasswordView;->showPasswordError()V

    .line 14
    :cond_0
    iget-object p3, p4, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 16
    iget-object p3, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {p3, v0}, Lcom/pspdfkit/internal/xm;->b(Z)V

    .line 17
    iget-object p3, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {p3, p1}, Lcom/pspdfkit/internal/xm;->d(Z)V

    .line 18
    new-instance p1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda70;

    invoke-direct {p1, p0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda70;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    invoke-virtual {p2, p1}, Lcom/pspdfkit/ui/PdfPasswordView;->setOnPasswordSubmitListener(Lcom/pspdfkit/ui/PdfPasswordView$OnPasswordSubmitListener;)V

    goto :goto_1

    .line 24
    :cond_1
    iget-object p2, p4, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 25
    iget-object p2, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/xm;->d(Z)V

    .line 26
    iget-object p2, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/xm;->b(Z)V

    .line 28
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/listeners/DocumentListener;

    .line 29
    invoke-interface {p2, p3}, Lcom/pspdfkit/listeners/DocumentListener;->onDocumentLoadFailed(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    new-array p1, v0, [Ljava/lang/Object;

    const-string p2, "PSPDFKit.PdfFragment"

    const-string p4, "Failed to open document."

    .line 32
    invoke-static {p2, p3, p4, p1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method synthetic lambda$load$12$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/document/ImageDocument;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->imageDocument:Lcom/pspdfkit/document/ImageDocument;

    if-eqz p1, :cond_0

    .line 3
    invoke-interface {p1}, Lcom/pspdfkit/document/ImageDocument;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->imageDocument:Lcom/pspdfkit/document/ImageDocument;

    invoke-interface {p1}, Lcom/pspdfkit/document/ImageDocument;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/zf;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    .line 5
    iget-object p2, p0, Lcom/pspdfkit/ui/PdfFragment;->internalDocumentListener:Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/zf;->a(Lcom/pspdfkit/internal/zf$f;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->invalidateCache()V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->displayDocument(Lcom/pspdfkit/internal/zf;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 11
    invoke-direct {p0, p2, p1}, Lcom/pspdfkit/ui/PdfFragment;->handleDocumentLoadingError(Ljava/lang/Throwable;Z)V

    .line 13
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadingProgressDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz p1, :cond_1

    .line 14
    invoke-interface {p1}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    .line 15
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadingProgressDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/xm;->r()V

    :cond_1
    return-void
.end method

.method synthetic lambda$load$14$com-pspdfkit-ui-PdfFragment()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method synthetic lambda$load$15$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/zf;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/ui/PdfFragment;->internalDocumentListener:Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/zf;->a(Lcom/pspdfkit/internal/zf$f;)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->invalidateCache()V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->displayDocument(Lcom/pspdfkit/internal/zf;)V

    goto :goto_1

    .line 7
    :cond_0
    instance-of p1, p2, Lcom/pspdfkit/exceptions/InvalidPasswordException;

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSources:Ljava/util/List;

    .line 8
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-ne p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 9
    :goto_0
    invoke-direct {p0, p2, v0}, Lcom/pspdfkit/ui/PdfFragment;->handleDocumentLoadingError(Ljava/lang/Throwable;Z)V

    .line 11
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadingProgressDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz p1, :cond_2

    .line 12
    invoke-interface {p1}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    const/4 p1, 0x0

    .line 13
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadingProgressDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/xm;->r()V

    :cond_2
    return-void
.end method

.method synthetic lambda$new$0$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    const/4 p1, 0x0

    if-eqz p5, :cond_0

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 3
    instance-of p2, p5, Lcom/pspdfkit/annotations/LinkAnnotation;

    if-eqz p2, :cond_0

    .line 4
    check-cast p5, Lcom/pspdfkit/annotations/LinkAnnotation;

    invoke-virtual {p5}, Lcom/pspdfkit/annotations/LinkAnnotation;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 5
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/actions/Action;->getType()Lcom/pspdfkit/annotations/actions/ActionType;

    move-result-object p3

    sget-object p4, Lcom/pspdfkit/annotations/actions/ActionType;->URI:Lcom/pspdfkit/annotations/actions/ActionType;

    if-ne p3, p4, :cond_0

    .line 6
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    check-cast p2, Lcom/pspdfkit/annotations/actions/UriAction;

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/PdfFragment;->previewUri(Landroid/content/Context;Lcom/pspdfkit/annotations/actions/UriAction;)V

    const/4 p1, 0x1

    :cond_0
    return p1
.end method

.method synthetic lambda$new$1$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/internal/k4;Lcom/pspdfkit/internal/zl;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result p1

    .line 2
    iget v0, p2, Lcom/pspdfkit/internal/zl;->a:I

    if-eq v0, p1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->beginNavigation()V

    .line 4
    iget p1, p2, Lcom/pspdfkit/internal/zl;->a:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(I)V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->endNavigation()V

    :cond_0
    return-void
.end method

.method synthetic lambda$onConfigurationChanged$7$com-pspdfkit-ui-PdfFragment(Landroid/os/Bundle;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setState(Landroid/os/Bundle;)V

    return-void
.end method

.method synthetic lambda$onDocumentLoaded$43$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/internal/i8;)Ljava/lang/Integer;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/i8;->a(Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/h8;

    move-result-object p1

    iget p2, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    .line 2
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/h8;->a(I)I

    move-result p1

    .line 3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method synthetic lambda$onDocumentLoaded$44$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Integer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    goto :goto_0

    .line 3
    :cond_0
    iget p2, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    :goto_0
    iput p2, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    .line 4
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->restorePagePosition(Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method synthetic lambda$onDocumentLoaded$48$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object p3, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSaver:Lcom/pspdfkit/internal/p9;

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/pspdfkit/internal/p9;->a()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p3

    if-eq p3, p1, :cond_2

    .line 2
    :cond_0
    iget-object p3, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSaver:Lcom/pspdfkit/internal/p9;

    if-eqz p3, :cond_1

    .line 5
    invoke-virtual {p3}, Lcom/pspdfkit/internal/p9;->e()V

    .line 7
    :cond_1
    new-instance p3, Lcom/pspdfkit/internal/p9;

    invoke-direct {p3, p2, p0}, Lcom/pspdfkit/internal/p9;-><init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/p9$a;)V

    iput-object p3, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSaver:Lcom/pspdfkit/internal/p9;

    .line 10
    :cond_2
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->prepareUndoManager(Lcom/pspdfkit/document/PdfDocument;)V

    .line 11
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->refreshUserInterfaceState()V

    .line 14
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->initPageCache()V

    .line 16
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p3

    const-string v0, "load_document"

    invoke-virtual {p3, v0}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p3

    invoke-virtual {p3}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 18
    iget-object p3, p0, Lcom/pspdfkit/ui/PdfFragment;->fragmentState:Landroid/os/Bundle;

    if-eqz p3, :cond_3

    .line 19
    invoke-virtual {p0, p3}, Lcom/pspdfkit/ui/PdfFragment;->setState(Landroid/os/Bundle;)V

    const/4 p3, 0x0

    .line 20
    iput-object p3, p0, Lcom/pspdfkit/ui/PdfFragment;->fragmentState:Landroid/os/Bundle;

    goto :goto_0

    .line 23
    :cond_3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {p3}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->get(Landroid/content/Context;)Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    move-result-object p3

    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NONE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-virtual {p3, v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->setLastAnnotationTool(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)V

    .line 28
    iget-object p3, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {p3}, Lcom/pspdfkit/configuration/PdfConfiguration;->isLastViewedPageRestorationEnabled()Z

    move-result p3

    if-eqz p3, :cond_4

    .line 29
    invoke-static {}, Lcom/pspdfkit/internal/i8;->b()Lio/reactivex/rxjava3/core/Single;

    move-result-object p3

    new-instance v0, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda59;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda59;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/document/PdfDocument;)V

    .line 30
    invoke-virtual {p3, v0}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p3

    .line 33
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p3, v0}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p3

    new-instance v0, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda60;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda60;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/document/PdfDocument;)V

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda61;

    invoke-direct {v1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda61;-><init>()V

    .line 34
    invoke-virtual {p3, v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/ui/PdfFragment;->lastViewedPageRestorationDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_0

    .line 51
    :cond_4
    invoke-static {}, Lcom/pspdfkit/internal/i8;->b()Lio/reactivex/rxjava3/core/Single;

    move-result-object p3

    new-instance v0, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda62;

    invoke-direct {v0, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda62;-><init>(Lcom/pspdfkit/document/PdfDocument;)V

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda63;

    invoke-direct {v1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda63;-><init>()V

    .line 52
    invoke-virtual {p3, v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    .line 61
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->restorePagePosition(Lcom/pspdfkit/document/PdfDocument;)V

    .line 65
    :goto_0
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getFormProvider()Lcom/pspdfkit/forms/FormProvider;

    move-result-object p3

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->formFieldUpdatedListener:Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;

    invoke-interface {p3, v0}, Lcom/pspdfkit/forms/FormProvider;->addOnFormFieldUpdatedListener(Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;)V

    .line 67
    iget-object p3, p0, Lcom/pspdfkit/ui/PdfFragment;->pasteManager:Lcom/pspdfkit/internal/um;

    if-eqz p3, :cond_5

    .line 68
    invoke-virtual {p3, p2}, Lcom/pspdfkit/internal/um;->a(Lcom/pspdfkit/internal/zf;)V

    .line 71
    :cond_5
    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object p3

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isJavaScriptEnabled()Z

    move-result v0

    check-cast p3, Lcom/pspdfkit/internal/ig;

    invoke-virtual {p3, v0}, Lcom/pspdfkit/internal/ig;->setJavaScriptEnabled(Z)V

    .line 72
    iget-object p3, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {p3}, Lcom/pspdfkit/configuration/PdfConfiguration;->isJavaScriptEnabled()Z

    move-result p3

    if-eqz p3, :cond_6

    .line 73
    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object p2

    iget-object p3, p0, Lcom/pspdfkit/ui/PdfFragment;->javaScriptPlatformDelegate:Lcom/pspdfkit/internal/wm;

    check-cast p2, Lcom/pspdfkit/internal/ig;

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/ig;->a(Lcom/pspdfkit/internal/lg;)V

    .line 77
    :cond_6
    iget-object p2, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_7

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/pspdfkit/listeners/DocumentListener;

    .line 78
    invoke-interface {p3, p1}, Lcom/pspdfkit/listeners/DocumentListener;->onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V

    goto :goto_1

    .line 81
    :cond_7
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getFormProvider()Lcom/pspdfkit/forms/FormProvider;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/ui/PdfFragment;->formFieldUpdatedListener:Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;

    invoke-interface {p1, p2}, Lcom/pspdfkit/forms/FormProvider;->addOnFormFieldUpdatedListener(Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;)V

    return-void
.end method

.method synthetic lambda$prepareContentEditingUndoManager$49$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->contentEditingUndoManager:Lcom/pspdfkit/internal/vu;

    new-instance v1, Lcom/pspdfkit/internal/m6;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getContentEditingHandler()Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    move-result-object p1

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->navigateOnUndoListener:Lcom/pspdfkit/internal/k4$a;

    invoke-direct {v1, p1, v2}, Lcom/pspdfkit/internal/m6;-><init>(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;Lcom/pspdfkit/internal/k4$a;)V

    .line 3
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/vu;->a(Lcom/pspdfkit/internal/k4;)V

    return-void
.end method

.method synthetic lambda$previewUri$38$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/annotations/actions/UriAction;Landroidx/appcompat/app/AlertDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->executeAction(Lcom/pspdfkit/annotations/actions/Action;)V

    .line 2
    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method synthetic lambda$previewUri$39$com-pspdfkit-ui-PdfFragment(Landroid/content/Context;Lcom/pspdfkit/annotations/actions/UriAction;Landroidx/appcompat/app/AlertDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/PdfFragment;->copyUri(Landroid/content/Context;Lcom/pspdfkit/annotations/actions/UriAction;)V

    .line 2
    invoke-virtual {p3}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method synthetic lambda$restoreContentEditing$6$com-pspdfkit-ui-PdfFragment(Ljava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->enterContentEditingMode()V

    return-void
.end method

.method synthetic lambda$restoreSelectedAnnotations$2$com-pspdfkit-ui-PdfFragment(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setSelectedAnnotations(Ljava/util/Collection;)V

    :cond_0
    return-void
.end method

.method synthetic lambda$restoreTextSelection$4$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/datastructures/TextSelection;Ljava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget p2, p1, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    iget-object p1, p1, Lcom/pspdfkit/datastructures/TextSelection;->textRange:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {p0, p2, p1}, Lcom/pspdfkit/ui/PdfFragment;->enterTextSelectionMode(ILcom/pspdfkit/datastructures/Range;)V

    return-void
.end method

.method synthetic lambda$setCustomPdfSources$35$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/internal/xm$b;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->load()V

    return-void
.end method

.method synthetic lambda$setDocumentInteractionEnabled$16$com-pspdfkit-ui-PdfFragment(ZLcom/pspdfkit/internal/xm$b;)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfFragment;->isDocumentInteractionEnabled:Z

    .line 2
    iget-object p2, p2, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz p1, :cond_0

    iget-boolean p1, p0, Lcom/pspdfkit/ui/PdfFragment;->isUserInterfaceEnabled:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p2, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method synthetic lambda$setOnDocumentLongPressListener$36$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/listeners/OnDocumentLongPressListener;Lcom/pspdfkit/internal/xm$b;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 1
    iget-object p2, p2, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->setOnDocumentLongPressListener(Lcom/pspdfkit/listeners/OnDocumentLongPressListener;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->onDocumentLongPressListener:Lcom/pspdfkit/listeners/OnDocumentLongPressListener;

    goto :goto_0

    .line 4
    :cond_0
    iget-object p1, p2, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget-object p2, p0, Lcom/pspdfkit/ui/PdfFragment;->defaultOnDocumentLongPressListener:Lcom/pspdfkit/listeners/OnDocumentLongPressListener;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->setOnDocumentLongPressListener(Lcom/pspdfkit/listeners/OnDocumentLongPressListener;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->defaultOnDocumentLongPressListener:Lcom/pspdfkit/listeners/OnDocumentLongPressListener;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->onDocumentLongPressListener:Lcom/pspdfkit/listeners/OnDocumentLongPressListener;

    :goto_0
    return-void
.end method

.method synthetic lambda$setPageIndex$29$com-pspdfkit-ui-PdfFragment(IZLcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-ltz p1, :cond_1

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt p1, v0, :cond_1

    .line 10
    invoke-virtual {p3, p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(IZ)V

    const/4 p1, 0x0

    .line 11
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->animatePageTransition:Ljava/lang/Boolean;

    return-void

    .line 12
    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    const-string v0, "Invalid page index "

    invoke-direct {p3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " - valid page indexes are [0, "

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method synthetic lambda$setRedactionAnnotationPreviewEnabled$97$com-pspdfkit-ui-PdfFragment(ZLcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfFragment;->redactionAnnotationPreviewEnabled:Z

    .line 2
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->setRedactionAnnotationPreviewEnabled(Z)V

    return-void
.end method

.method synthetic lambda$setSelectedAnnotations$22$com-pspdfkit-ui-PdfFragment(Ljava/util/Collection;Ljava/lang/Integer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->getPageEditorForCurrentPage()Lcom/pspdfkit/internal/am;

    move-result-object p2

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/pspdfkit/annotations/Annotation;

    .line 3
    invoke-interface {p1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/am;->a([Lcom/pspdfkit/annotations/Annotation;)V

    :cond_0
    return-void
.end method

.method synthetic lambda$setSelectedFormElement$27$com-pspdfkit-ui-PdfFragment(ILcom/pspdfkit/forms/FormElement;Ljava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object p3, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {p3, p1}, Lcom/pspdfkit/internal/xm;->b(I)Lcom/pspdfkit/internal/tb;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/tb;->d(Lcom/pspdfkit/forms/FormElement;)V

    :cond_0
    return-void
.end method

.method synthetic lambda$setUserInterfaceEnabledInternal$17$com-pspdfkit-ui-PdfFragment(ZZLcom/pspdfkit/internal/xm$b;)V
    .locals 4

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfFragment;->isUserInterfaceEnabled:Z

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    .line 7
    invoke-virtual {v2}, Lcom/pspdfkit/internal/xm;->u()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    .line 8
    invoke-virtual {v2}, Lcom/pspdfkit/internal/xm;->v()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    .line 9
    invoke-virtual {v2}, Lcom/pspdfkit/internal/xm;->s()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 10
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/xm;->c(Z)V

    .line 12
    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v2, :cond_3

    if-nez p2, :cond_2

    if-eqz p1, :cond_3

    .line 13
    :cond_2
    iget-object p2, p3, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 15
    :cond_3
    iget-object p2, p3, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    const/4 v2, 0x4

    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 18
    :goto_1
    iget-object p2, p3, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget-boolean v2, p0, Lcom/pspdfkit/ui/PdfFragment;->isDocumentInteractionEnabled:Z

    if-eqz v2, :cond_4

    if-eqz p1, :cond_4

    const/4 v2, 0x1

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {p2, v2}, Landroid/view/View;->setEnabled(Z)V

    if-eqz p1, :cond_7

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    if-nez p1, :cond_5

    goto :goto_4

    .line 34
    :cond_5
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->lastEnabledSpecialModeState:Lcom/pspdfkit/internal/ns;

    if-eqz p1, :cond_6

    .line 35
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setSpecialModeState(Lcom/pspdfkit/internal/ns;)V

    const/4 p1, 0x0

    .line 36
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->lastEnabledSpecialModeState:Lcom/pspdfkit/internal/ns;

    .line 39
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->userInterfaceListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_9

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/ev;

    .line 40
    invoke-interface {p2, v0}, Lcom/pspdfkit/internal/ev;->onUserInterfaceEnabled(Z)V

    goto :goto_3

    .line 41
    :cond_7
    :goto_4
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->userInterfaceListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/ev;

    .line 42
    invoke-interface {p2, v1}, Lcom/pspdfkit/internal/ev;->onUserInterfaceEnabled(Z)V

    goto :goto_5

    .line 45
    :cond_8
    iget-object p1, p3, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->k()Z

    move-result p1

    if-eqz p1, :cond_9

    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->lastEnabledSpecialModeState:Lcom/pspdfkit/internal/ns;

    if-nez p1, :cond_9

    .line 46
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->getSpecialModeState()Lcom/pspdfkit/internal/ns;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->lastEnabledSpecialModeState:Lcom/pspdfkit/internal/ns;

    .line 47
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    :cond_9
    return-void
.end method

.method synthetic lambda$setViewState$30$com-pspdfkit-ui-PdfFragment(Lcom/pspdfkit/internal/ug$a;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->setViewState(Lcom/pspdfkit/internal/ug$a;)V

    :cond_0
    return-void
.end method

.method public notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    const/4 v1, 0x0

    .line 55
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/xm;->a(Z)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 56
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public notifyLayoutChanged()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda37;

    invoke-direct {v1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda37;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 3
    invoke-virtual {p0, p0}, Lcom/pspdfkit/ui/PdfFragment;->addOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V

    .line 4
    invoke-virtual {p0, p0}, Lcom/pspdfkit/ui/PdfFragment;->addOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V

    .line 5
    invoke-virtual {p0, p0}, Lcom/pspdfkit/ui/PdfFragment;->addOnFormElementSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V

    .line 6
    invoke-virtual {p0, p0}, Lcom/pspdfkit/ui/PdfFragment;->addOnFormElementDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;)V

    if-eqz p1, :cond_0

    .line 9
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->onRestoreInstanceState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onAnnotationDeselected(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 0

    if-nez p2, :cond_0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    :cond_0
    return-void
.end method

.method public onAnnotationSelected(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->enterAnnotationEditingMode(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 3
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->weakDocumentListeners:Ljava/lang/ref/WeakReference;

    .line 6
    invoke-static {p1}, Lcom/pspdfkit/internal/gj;->a(Landroid/content/Context;)V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getState()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "PSPDFKit.ViewState"

    .line 6
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda57;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda57;-><init>(Lcom/pspdfkit/ui/PdfFragment;Landroid/os/Bundle;)V

    const/4 p1, 0x0

    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 6
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->setRetainInstance(Z)V

    .line 9
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_a

    const-string v2, "PSPDFKit.Configuration"

    .line 13
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/configuration/PdfConfiguration;

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    if-eqz v0, :cond_9

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSources:Ljava/util/List;

    if-nez v0, :cond_3

    .line 19
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "PSPDFKit.Sources"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .line 21
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "PSPDFKit.ImageDocument.Source"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/mm;

    if-eqz v0, :cond_1

    .line 24
    invoke-static {v0}, Lcom/pspdfkit/internal/mm;->a([Landroid/os/Parcelable;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSources:Ljava/util/List;

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    .line 26
    invoke-virtual {v2}, Lcom/pspdfkit/internal/mm;->a()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->imageDocumentSource:Lcom/pspdfkit/document/DocumentSource;

    goto :goto_0

    .line 28
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSources:Ljava/util/List;

    .line 32
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getStartZoomScale()F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/PdfFragment;->startZoomScale:F

    .line 35
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    sget v2, Lcom/pspdfkit/internal/wl;->b:I

    const-string v2, "context"

    .line 36
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_5

    .line 92
    invoke-static {v0}, Lcom/pspdfkit/internal/wl;->a(Landroid/content/Context;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->onErrorComplete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V

    .line 95
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_1

    .line 96
    :cond_4
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;

    const-string v0, "PSPDFKit is not initialized!"

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 97
    :cond_5
    :goto_1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object v0

    .line 98
    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMemoryCacheSize()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/yl;->a(I)V

    .line 100
    invoke-static {}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->create()Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->pageChangeSubject:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    if-nez p1, :cond_6

    .line 103
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationHistory:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationItemBackStackListener:Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->addBackStackListener(Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;)V

    goto :goto_2

    .line 105
    :cond_6
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 107
    :goto_2
    new-instance p1, Lcom/pspdfkit/internal/um;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->undoManager:Lcom/pspdfkit/internal/vu;

    invoke-direct {p1, v0, p0, v2}, Lcom/pspdfkit/internal/um;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/vu;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->pasteManager:Lcom/pspdfkit/internal/um;

    .line 110
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isUndoEnabled()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 111
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isRedoEnabled()Z

    move-result p1

    if-eqz p1, :cond_7

    const/4 v1, 0x3

    goto :goto_3

    :cond_7
    const/4 v1, 0x2

    .line 117
    :cond_8
    :goto_3
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->undoManager:Lcom/pspdfkit/internal/vu;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/vu;->a(I)V

    .line 118
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->contentEditingUndoManager:Lcom/pspdfkit/internal/vu;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/vu;->a(I)V

    return-void

    .line 119
    :cond_9
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "PdfFragment was missing the PdfConfiguration argument!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 120
    :cond_a
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "PdfFragment was missing the arguments bundle!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/pspdfkit/internal/x5;->a(Landroid/content/Context;)V

    .line 5
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object p2

    const/4 p3, 0x1

    new-array p3, p3, [I

    sget v0, Lcom/pspdfkit/R$attr;->pspdf__backgroundColor:I

    const/4 v1, 0x0

    aput v0, p3, v1

    invoke-virtual {p2, p3}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 7
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object p3

    sget v0, Lcom/pspdfkit/R$color;->pspdf__color_gray_light:I

    invoke-static {p3, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p3

    invoke-virtual {p2, v1, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    .line 8
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 9
    invoke-virtual {p0, p3}, Lcom/pspdfkit/ui/PdfFragment;->setBackgroundColor(I)V

    .line 11
    iget-object p2, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/xm;->a(Landroid/view/LayoutInflater;)Landroid/widget/FrameLayout;

    move-result-object p1

    .line 16
    iget-object p2, p0, Lcom/pspdfkit/ui/PdfFragment;->signatureFormSigningHandler:Lcom/pspdfkit/internal/pr;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/pr;->a()V

    .line 18
    iget-object p2, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    if-nez p2, :cond_0

    .line 19
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->load()V

    goto :goto_0

    .line 21
    :cond_0
    invoke-direct {p0, p2}, Lcom/pspdfkit/ui/PdfFragment;->displayDocument(Lcom/pspdfkit/internal/zf;)V

    :goto_0
    return-object p1
.end method

.method public onDestroy()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->lifecycleDisposable:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->dispose()V

    .line 6
    new-instance v0, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->lifecycleDisposable:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->javaScriptPlatformDelegate:Lcom/pspdfkit/internal/wm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wm;->c()V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 13
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 14
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 15
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadingProgressDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 16
    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 17
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentLoadingProgressDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 19
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->resetDocument()V

    .line 21
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/n4;->a()V

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->audioModeManager:Lcom/pspdfkit/internal/a3;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/a3;->exitActiveAudioMode()V

    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroyView()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->signatureFormSigningHandler:Lcom/pspdfkit/internal/pr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pr;->b()V

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->cancelRestorePagePosition()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->j()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    .line 7
    invoke-virtual {p0, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V

    .line 8
    invoke-virtual {p0, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V

    .line 9
    invoke-virtual {p0, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeOnFormElementSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V

    .line 10
    invoke-virtual {p0, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeOnFormElementDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->b()V

    .line 14
    invoke-static {}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->create()Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->pageChangeSubject:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    return-void
.end method

.method public onDetach()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDetach()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentScrollListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->userInterfaceListeners:Lcom/pspdfkit/internal/nh;

    return-void
.end method

.method public onDocumentClick()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/listeners/DocumentListener;

    .line 2
    invoke-interface {v2}, Lcom/pspdfkit/listeners/DocumentListener;->onDocumentClick()Z

    move-result v2

    or-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return v1
.end method

.method public onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method public onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 4

    .line 1
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/internal/zf;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isJavaScriptEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object v1

    .line 7
    check-cast v1, Lcom/pspdfkit/internal/ig;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ig;->executeDocumentLevelScriptsAsync()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 8
    invoke-virtual {v1}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V

    .line 11
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->lifecycleDisposable:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/xm;->h()Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    new-instance v3, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda1;

    invoke-direct {v3, p0, p1, v0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/internal/zf;)V

    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    invoke-virtual {v1, p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    return-void
.end method

.method public onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->weakDocumentListeners:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nh;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 5
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x1

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/listeners/DocumentListener;

    .line 6
    invoke-interface {v4, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener;->onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 8
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Document save has been cancelled by "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v6, v2, [Ljava/lang/Object;

    const-string v7, "PSPDFKit.PdfFragment"

    invoke-static {v7, v4, v6}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    if-eqz v3, :cond_2

    if-eqz v5, :cond_2

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    if-eqz v3, :cond_4

    .line 15
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->refreshUserInterfaceState()V

    :cond_4
    return v3
.end method

.method public onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->refreshUserInterfaceState()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->weakDocumentListeners:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nh;

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/listeners/DocumentListener;

    .line 5
    invoke-interface {v1, p1}, Lcom/pspdfkit/listeners/DocumentListener;->onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->refreshUserInterfaceState()V

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object v0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/yl;->a(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    const/4 v2, 0x5

    .line 5
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    .line 6
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfFragment"

    const-string v2, "Document saving failed, clearing the document cache."

    .line 9
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->weakDocumentListeners:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nh;

    if-nez v0, :cond_0

    return-void

    .line 13
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/listeners/DocumentListener;

    .line 14
    invoke-interface {v1, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener;->onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->refreshUserInterfaceState()V

    .line 4
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 7
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v0

    .line 9
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->imageDocument:Lcom/pspdfkit/document/ImageDocument;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 10
    invoke-static {v0, v1}, Lcom/pspdfkit/document/ImageDocumentUtils;->refreshMediaStore(Landroid/content/Context;Lcom/pspdfkit/document/ImageDocument;)V

    .line 13
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->weakDocumentListeners:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nh;

    if-nez v0, :cond_2

    return-void

    .line 15
    :cond_2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/listeners/DocumentListener;

    .line 16
    invoke-interface {v1, p1}, Lcom/pspdfkit/listeners/DocumentListener;->onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public onDocumentScrolled(Lcom/pspdfkit/ui/PdfFragment;IIIIII)V
    .locals 10

    move-object v8, p0

    .line 1
    iget-object v0, v8, Lcom/pspdfkit/ui/PdfFragment;->documentScrollListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;

    move-object v1, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    .line 2
    invoke-interface/range {v0 .. v7}, Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;->onDocumentScrolled(Lcom/pspdfkit/ui/PdfFragment;IIIIII)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/listeners/DocumentListener;

    .line 2
    invoke-interface {v1, p1, p2, p3}, Lcom/pspdfkit/listeners/DocumentListener;->onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onFormElementDeselected(Lcom/pspdfkit/forms/FormElement;Z)V
    .locals 0

    if-nez p2, :cond_0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    :cond_0
    return-void
.end method

.method public onFormElementSelected(Lcom/pspdfkit/forms/FormElement;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->enterFormEditingMode(Lcom/pspdfkit/forms/FormElement;)V

    return-void
.end method

.method public onLowMemory()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onLowMemory()V

    .line 2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/yl;->a()V

    .line 5
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/n4;->a()V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/xm;->a(Z)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    .line 10
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/xm;->a(Z)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->r()V

    :cond_0
    return-void
.end method

.method public onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->pageChangeSubject:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    if-eqz v0, :cond_0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/listeners/DocumentListener;

    .line 3
    invoke-interface {v1, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener;->onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V

    goto :goto_0

    .line 6
    :cond_1
    iget-boolean p1, p0, Lcom/pspdfkit/ui/PdfFragment;->historyActionInProgress:Z

    if-nez p1, :cond_2

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationHistory:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->resetForwardList()V

    :cond_2
    const/4 p1, 0x0

    .line 9
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfFragment;->historyActionInProgress:Z

    return-void
.end method

.method public onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/pspdfkit/listeners/DocumentListener;

    move-object v4, p1

    move v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    .line 2
    invoke-interface/range {v3 .. v8}, Lcom/pspdfkit/listeners/DocumentListener;->onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    or-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return v1
.end method

.method public onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/listeners/DocumentListener;

    .line 2
    invoke-interface {v1, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener;->onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onPrepareAnnotationSelection(Lcom/pspdfkit/ui/special_mode/controller/AnnotationSelectionController;Lcom/pspdfkit/annotations/Annotation;Z)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public synthetic onPrepareFormElementSelection(Lcom/pspdfkit/forms/FormElement;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener$-CC;->$default$onPrepareFormElementSelection(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;Lcom/pspdfkit/forms/FormElement;)Z

    move-result p1

    return p1
.end method

.method public onResume()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->refreshUserInterfaceState()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->javaScriptPlatformDelegate:Lcom/pspdfkit/internal/wm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wm;->d()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->signatureFormSigningHandler:Lcom/pspdfkit/internal/pr;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/pr;->b(Landroid/os/Bundle;)V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getState()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PSPDFKit.PSPDFFragmentState"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->password:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v1, "PSPDFKit.UserP"

    .line 7
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/listeners/scrolling/ScrollState;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentScrollListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;

    .line 2
    invoke-interface {v0, p0, p2}, Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;->onScrollStateChanged(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/listeners/scrolling/ScrollState;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 4

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStop()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAutosaveEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->save()V

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    .line 13
    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda13;

    invoke-direct {v1, v0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda13;-><init>(Lcom/pspdfkit/document/PdfDocument;)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    .line 14
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/u;

    const/4 v3, 0x5

    .line 15
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    .line 16
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda14;

    invoke-direct {v2, v0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda14;-><init>(Lcom/pspdfkit/document/PdfDocument;)V

    .line 17
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Single;->flatMapCompletable(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 20
    invoke-virtual {v1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 24
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isLastViewedPageRestorationEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 26
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/xm;->j()I

    move-result v1

    const/4 v2, -0x1

    if-le v1, v2, :cond_2

    .line 29
    invoke-static {}, Lcom/pspdfkit/internal/i8;->b()Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    new-instance v3, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda15;

    invoke-direct {v3, v0, v1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda15;-><init>(Lcom/pspdfkit/document/PdfDocument;I)V

    new-instance v0, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda16;

    invoke-direct {v0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda16;-><init>()V

    .line 30
    invoke-virtual {v2, v3, v0}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    .line 42
    :cond_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 43
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_3

    .line 44
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->audioModeManager:Lcom/pspdfkit/internal/a3;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/a3;->c()V

    .line 45
    invoke-static {}, Lcom/pspdfkit/internal/bu;->a()V

    :cond_3
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/xm;->z()V

    return-void
.end method

.method protected openDocumentAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "+",
            "Lcom/pspdfkit/document/PdfDocument;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSources:Ljava/util/List;

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isMultithreadedRenderingEnabled()Z

    move-result v2

    .line 2
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocumentsAsync(Landroid/content/Context;Ljava/util/List;Z)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public removeDocumentActionListener(Lcom/pspdfkit/document/DocumentActionListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda79;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda79;-><init>(Lcom/pspdfkit/document/DocumentActionListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V
    .locals 2

    const-string v0, "documentListener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public removeDocumentScrollListener(Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;)V
    .locals 2

    const-string v0, "documentScrollListener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentScrollListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public removeDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V
    .locals 2

    const-string v0, "drawableProvider"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda85;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda85;-><init>(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda21;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda21;-><init>(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda41;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda41;-><init>(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda20;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda20;-><init>(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda25;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda25;-><init>(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda7;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnContentEditingContentChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda94;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda94;-><init>(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnContentEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda65;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda65;-><init>(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnFormElementClickedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda6;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnFormElementDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda77;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda77;-><init>(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnFormElementEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda19;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda19;-><init>(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnFormElementSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda69;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda69;-><init>(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnFormElementUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda32;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda32;-><init>(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnFormElementViewUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda93;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda93;-><init>(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnTextSelectionChangeListener(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda76;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda76;-><init>(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOnTextSelectionModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionModeChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda42;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda42;-><init>(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionModeChangeListener;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public removeOverlayViewProvider(Lcom/pspdfkit/ui/overlay/OverlayViewProvider;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "overlayViewProvider"

    const-string v1, "argumentName"

    .line 6
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 57
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 58
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda72;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda72;-><init>(Lcom/pspdfkit/ui/overlay/OverlayViewProvider;)V

    const/4 p1, 0x0

    .line 59
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void

    .line 60
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Using removeOverlayViewProvider() requires the annotations component."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public save()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSaver:Lcom/pspdfkit/internal/p9;

    if-nez v0, :cond_0

    return-void

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->weakDocumentListeners:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nh;

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSaver:Lcom/pspdfkit/internal/p9;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/p9;->f()Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/ui/PdfFragment$5;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/ui/PdfFragment$5;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/nh;)V

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/core/SingleObserver;)V

    return-void
.end method

.method public scrollTo(Landroid/graphics/RectF;IJZ)V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->cancelRestorePagePosition()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v7, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda98;

    move-object v1, v7

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda98;-><init>(Landroid/graphics/RectF;IJZ)V

    const/4 p1, 0x0

    .line 3
    invoke-virtual {v0, v7, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public setAnnotationOverlayRenderStrategy(Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;)V

    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/xm;->e(I)V

    return-void
.end method

.method public setCustomPdfSource(Lcom/pspdfkit/document/DocumentSource;)V
    .locals 2

    const-string v0, "source"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setCustomPdfSources(Ljava/util/List;)V

    return-void
.end method

.method public setCustomPdfSources(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;)V"
        }
    .end annotation

    const-string v0, "sources"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSources:Ljava/util/List;

    .line 57
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->resetDocument()V

    .line 60
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/xm;->z()V

    .line 61
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v0, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda38;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda38;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    const/4 v1, 0x0

    .line 62
    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$d;Z)V

    return-void
.end method

.method public setDocumentInteractionEnabled(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda55;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda55;-><init>(Lcom/pspdfkit/ui/PdfFragment;Z)V

    const/4 p1, 0x0

    .line 2
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$d;Z)V

    return-void
.end method

.method public setDocumentSigningListener(Lcom/pspdfkit/listeners/DocumentSigningListener;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DIGITAL_SIGNATURES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->signatureFormSigningHandler:Lcom/pspdfkit/internal/pr;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/pr;->a(Lcom/pspdfkit/listeners/DocumentSigningListener;)V

    return-void

    .line 6
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Setting a document singing listener requires the digital signature feature in your license."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setInsets(IIII)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/PdfFragment;->insetsLeft:I

    .line 2
    iput p2, p0, Lcom/pspdfkit/ui/PdfFragment;->insetsTop:I

    .line 3
    iput p3, p0, Lcom/pspdfkit/ui/PdfFragment;->insetsRight:I

    .line 4
    iput p4, p0, Lcom/pspdfkit/ui/PdfFragment;->insetsBottom:I

    .line 5
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/ui/PdfFragment;->setDocumentInsets(IIII)V

    return-void
.end method

.method public setOnDocumentLongPressListener(Lcom/pspdfkit/listeners/OnDocumentLongPressListener;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda48;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda48;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/listeners/OnDocumentLongPressListener;)V

    const/4 p1, 0x0

    .line 2
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$d;Z)V

    return-void
.end method

.method public setOnPreparePopupToolbarListener(Lcom/pspdfkit/listeners/OnPreparePopupToolbarListener;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda64;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda64;-><init>(Lcom/pspdfkit/listeners/OnPreparePopupToolbarListener;)V

    const/4 p1, 0x0

    .line 2
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public setOverlaidAnnotationTypes(Ljava/util/EnumSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)V"
        }
    .end annotation

    const-string v0, "getOverlaidAnnotationTypes"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda78;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda78;-><init>(Ljava/util/EnumSet;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public setOverlaidAnnotations(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    const-string v0, "overlayAnnotations"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda18;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda18;-><init>(Ljava/util/List;)V

    const/4 p1, 0x0

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public setPageIndex(I)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->cancelRestorePagePosition()V

    .line 2
    iput p1, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->animatePageTransition:Ljava/lang/Boolean;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-ltz p1, :cond_2

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt p1, v0, :cond_2

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationStartPage:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 16
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationEndPage:Ljava/lang/Integer;

    .line 19
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda68;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda68;-><init>(I)V

    const/4 p1, 0x0

    .line 20
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void

    .line 21
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid page index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " - valid page indexes are [0, "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    .line 24
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setPageIndex(IZ)V
    .locals 2

    .line 25
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->cancelRestorePagePosition()V

    .line 26
    iput p1, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    .line 28
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->animatePageTransition:Ljava/lang/Boolean;

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    return-void

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationStartPage:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 32
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationEndPage:Ljava/lang/Integer;

    .line 35
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda29;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda29;-><init>(Lcom/pspdfkit/ui/PdfFragment;IZ)V

    const/4 p1, 0x0

    .line 36
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public setPageLoadingDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/xm;->a(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setPasswordView(Lcom/pspdfkit/ui/PdfPasswordView;)V
    .locals 2

    const-string v0, "pdfPasswordView"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/ui/PdfPasswordView;)V

    return-void
.end method

.method public setRedactionAnnotationPreviewEnabled(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda52;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda52;-><init>(Lcom/pspdfkit/ui/PdfFragment;Z)V

    const/4 p1, 0x0

    .line 2
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public setScrollingEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/xm;->e(Z)V

    return-void
.end method

.method public setSelectedAnnotation(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "PdfFragment#setSelectedAnnotation() may only be called from the main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 4
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 55
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 56
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setSelectedAnnotations(Ljava/util/Collection;)V

    return-void
.end method

.method public setSelectedAnnotations(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "PdfFragment#setSelectedAnnotations() may only be called from the main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 6
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->clearSelectedAnnotations()Z

    .line 11
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 12
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    .line 13
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 14
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v2

    if-ne v2, v1, :cond_1

    goto :goto_0

    .line 15
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "You may only select annotations that are on the same document page."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 21
    :cond_2
    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/PdfFragment;->getPageEditorForPage(I)Lcom/pspdfkit/internal/am;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/pspdfkit/annotations/Annotation;

    .line 23
    invoke-interface {p1, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/am;->a([Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_1

    .line 25
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->lifecycleDisposable:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->pageChangeSubject:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    new-instance v3, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda39;

    invoke-direct {v3, v1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda39;-><init>(I)V

    .line 26
    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v1

    .line 27
    invoke-virtual {v1}, Lio/reactivex/rxjava3/core/Observable;->firstOrError()Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda40;

    invoke-direct {v2, p0, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda40;-><init>(Lcom/pspdfkit/ui/PdfFragment;Ljava/util/Collection;)V

    .line 28
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    .line 29
    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    :goto_1
    return-void
.end method

.method public setSelectedFormElement(Lcom/pspdfkit/forms/FormElement;)V
    .locals 4

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "PdfFragment#setSelectedFormElement() may only be called from the main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/xm;->b(I)Lcom/pspdfkit/internal/tb;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 9
    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/tb;->d(Lcom/pspdfkit/forms/FormElement;)V

    goto :goto_0

    .line 17
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->lifecycleDisposable:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfFragment;->pageChangeSubject:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    new-instance v3, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda73;

    invoke-direct {v3, v0}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda73;-><init>(I)V

    .line 18
    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v2

    .line 19
    invoke-virtual {v2}, Lio/reactivex/rxjava3/core/Observable;->firstOrError()Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    new-instance v3, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda74;

    invoke-direct {v3, p0, v0, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda74;-><init>(Lcom/pspdfkit/ui/PdfFragment;ILcom/pspdfkit/forms/FormElement;)V

    .line 20
    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    .line 21
    invoke-virtual {v1, p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    const/4 p1, 0x1

    .line 30
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(IZ)V

    :goto_0
    return-void
.end method

.method public setSignatureMetadata(Lcom/pspdfkit/signatures/SignatureMetadata;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    return-void
.end method

.method public setSignatureStorage(Lcom/pspdfkit/signatures/storage/SignatureStorage;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->signatureStorage:Lcom/pspdfkit/signatures/storage/SignatureStorage;

    return-void
.end method

.method public setState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "PdfFragment#setState() may only be called from the main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    const-string v0, "PSPDFKit.NavigationHistory"

    .line 5
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    if-eqz v0, :cond_0

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationHistory:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->replaceWith(Lcom/pspdfkit/ui/navigation/NavigationBackStack;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationHistory:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->navigationItemBackStackListener:Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->addBackStackListener(Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;)V

    :cond_0
    const-string v0, "PSPDFKit.ViewState"

    .line 12
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ug$a;

    if-eqz v0, :cond_1

    .line 14
    iget v0, v0, Lcom/pspdfkit/internal/ug$a;->c:I

    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    .line 15
    iput-boolean v0, p0, Lcom/pspdfkit/ui/PdfFragment;->historyActionInProgress:Z

    :cond_1
    const-string v0, "PSPDFKit.LastEnabledSpecialModeState"

    .line 19
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ns;

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->lastEnabledSpecialModeState:Lcom/pspdfkit/internal/ns;

    const-string v0, "PSPDFKit.RedactionPreviewState"

    .line 22
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/PdfFragment;->setRedactionAnnotationPreviewEnabled(Z)V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xm;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 26
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setFragmentUiState(Landroid/os/Bundle;)V

    goto :goto_0

    .line 28
    :cond_2
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment;->fragmentState:Landroid/os/Bundle;

    :goto_0
    return-void
.end method

.method public setUserInterfaceEnabled(Z)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->setUserInterfaceEnabledInternal(ZZ)V

    return-void
.end method

.method setViewState(Lcom/pspdfkit/internal/ug$a;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->cancelRestorePagePosition()V

    .line 4
    iget v0, p1, Lcom/pspdfkit/internal/ug$a;->c:I

    iput v0, p0, Lcom/pspdfkit/ui/PdfFragment;->displayedPage:I

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda71;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda71;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/ug$a;)V

    const/4 p1, 0x0

    .line 7
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public setZoomingEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/xm;->f(Z)V

    return-void
.end method

.method protected shouldReloadDocument()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_1

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getDocumentSources()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment;->documentSources:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public zoomBy(IIIFJ)V
    .locals 9

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->cancelRestorePagePosition()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v8, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda31;

    move-object v1, v8

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-wide v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda31;-><init>(IIIFJ)V

    const/4 p1, 0x0

    .line 3
    invoke-virtual {v0, v8, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public zoomTo(IIIFJ)V
    .locals 9

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->cancelRestorePagePosition()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v8, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda81;

    move-object v1, v8

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-wide v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda81;-><init>(IIIFJ)V

    const/4 p1, 0x0

    .line 3
    invoke-virtual {v0, v8, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public zoomTo(Landroid/graphics/RectF;IJ)V
    .locals 2

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;->cancelRestorePagePosition()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment;->viewCoordinator:Lcom/pspdfkit/internal/xm;

    new-instance v1, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda30;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/pspdfkit/ui/PdfFragment$$ExternalSyntheticLambda30;-><init>(Landroid/graphics/RectF;IJ)V

    const/4 p1, 0x0

    .line 6
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.class public Lcom/pspdfkit/ui/editor/AnnotationEditor;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/editor/AnnotationEditor$OnDismissedListener;
    }
.end annotation


# static fields
.field public static final FRAGMENT_EDITOR_TAG:Ljava/lang/String; = "PSPDFKit.AnnotationEditor"


# instance fields
.field fragment:Lcom/pspdfkit/internal/v0;

.field private final fragmentManager:Landroidx/fragment/app/FragmentManager;

.field private onDismissedListener:Lcom/pspdfkit/ui/editor/AnnotationEditor$OnDismissedListener;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/v0;Landroidx/fragment/app/FragmentManager;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/editor/AnnotationEditor;->fragment:Lcom/pspdfkit/internal/v0;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/ui/editor/AnnotationEditor;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    .line 5
    new-instance p2, Lcom/pspdfkit/ui/editor/AnnotationEditor$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/pspdfkit/ui/editor/AnnotationEditor$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/editor/AnnotationEditor;)V

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/v0;->a(Lcom/pspdfkit/internal/v0$a;)V

    return-void
.end method

.method private static createEditorFragment(Ljava/lang/Class;Landroidx/fragment/app/FragmentManager;)Lcom/pspdfkit/internal/v0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/pspdfkit/internal/v0;",
            ">;",
            "Landroidx/fragment/app/FragmentManager;",
            ")",
            "Lcom/pspdfkit/internal/v0;"
        }
    .end annotation

    const-string v0, "PSPDFKit.AnnotationEditor"

    .line 1
    invoke-virtual {p1, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/v0;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    :try_start_0
    new-array v0, p1, [Ljava/lang/Class;

    .line 4
    invoke-virtual {p0, v0}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object p0

    new-array p1, p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    move-object p1, p0

    check-cast p1, Lcom/pspdfkit/internal/v0;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Could not instantiate annotation editor fragment. Fragment requires a public empty constructor!"

    invoke-direct {p1, v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    :cond_0
    :goto_0
    return-object p1
.end method

.method public static forAnnotation(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/ui/editor/AnnotationEditor;
    .locals 5

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotation"

    const/4 v1, 0x0

    .line 52
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "fragment"

    const-string v2, "argumentName"

    .line 53
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fragment"

    .line 104
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "onEditRecordedListener"

    const-string v2, "argumentName"

    .line 105
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEditRecordedListener"

    .line 156
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 157
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->hasInstantComments()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-class v2, Lcom/pspdfkit/internal/rk;

    invoke-static {v2, v0}, Lcom/pspdfkit/ui/editor/AnnotationEditor;->createEditorFragment(Ljava/lang/Class;Landroidx/fragment/app/FragmentManager;)Lcom/pspdfkit/internal/v0;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 165
    :goto_0
    invoke-static {p0}, Lcom/pspdfkit/internal/ao;->e(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 166
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v2, v3, :cond_2

    .line 167
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v2, v3, :cond_3

    .line 168
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v2

    .line 169
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v3

    monitor-enter v2

    :try_start_0
    const-string v4, "configuration"

    .line 170
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 385
    sget-object v4, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_REPLIES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v2, v4}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 386
    invoke-virtual {v3}, Lcom/pspdfkit/configuration/PdfConfiguration;->getAnnotationReplyFeatures()Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;->DISABLED:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v3, v4, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    monitor-exit v2

    if-eqz v3, :cond_3

    goto :goto_2

    :catchall_0
    move-exception p0

    monitor-exit v2

    throw p0

    .line 387
    :cond_2
    :goto_2
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-class v2, Lcom/pspdfkit/internal/rk;

    invoke-static {v2, v0}, Lcom/pspdfkit/ui/editor/AnnotationEditor;->createEditorFragment(Ljava/lang/Class;Landroidx/fragment/app/FragmentManager;)Lcom/pspdfkit/internal/v0;

    move-result-object v0

    :cond_3
    if-eqz v0, :cond_4

    .line 390
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 391
    new-instance v1, Lcom/pspdfkit/ui/editor/AnnotationEditor;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/pspdfkit/ui/editor/AnnotationEditor;-><init>(Lcom/pspdfkit/internal/v0;Landroidx/fragment/app/FragmentManager;)V

    .line 392
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/v0;->b(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;)V

    .line 393
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/v0;->a(Lcom/pspdfkit/annotations/Annotation;)V

    :cond_4
    return-object v1
.end method

.method public static restoreFromState(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/ui/editor/AnnotationEditor;
    .locals 3

    const-string v0, "onEditRecordedListener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v2, "PSPDFKit.AnnotationEditor"

    invoke-virtual {v0, v2}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/v0;

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 59
    invoke-virtual {v0, p0, p1}, Lcom/pspdfkit/internal/v0;->a(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;)V

    .line 60
    new-instance v1, Lcom/pspdfkit/ui/editor/AnnotationEditor;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p0

    invoke-direct {v1, v0, p0}, Lcom/pspdfkit/ui/editor/AnnotationEditor;-><init>(Lcom/pspdfkit/internal/v0;Landroidx/fragment/app/FragmentManager;)V

    :cond_0
    return-object v1
.end method


# virtual methods
.method public getAnnotation(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/document/PdfDocument;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/editor/AnnotationEditor;->fragment:Lcom/pspdfkit/internal/v0;

    check-cast p1, Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/v0;->a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method synthetic lambda$new$0$com-pspdfkit-ui-editor-AnnotationEditor(Lcom/pspdfkit/internal/v0;Z)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/editor/AnnotationEditor;->onDismissedListener:Lcom/pspdfkit/ui/editor/AnnotationEditor$OnDismissedListener;

    if-eqz p1, :cond_0

    .line 4
    invoke-interface {p1, p0, p2}, Lcom/pspdfkit/ui/editor/AnnotationEditor$OnDismissedListener;->onAnnotationEditorDismissed(Lcom/pspdfkit/ui/editor/AnnotationEditor;Z)V

    :cond_0
    return-void
.end method

.method public setOnDismissedListener(Lcom/pspdfkit/ui/editor/AnnotationEditor$OnDismissedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/editor/AnnotationEditor;->onDismissedListener:Lcom/pspdfkit/ui/editor/AnnotationEditor$OnDismissedListener;

    return-void
.end method

.method public show()V
    .locals 1

    const/4 v0, 0x0

    .line 4
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/editor/AnnotationEditor;->show(Z)V

    return-void
.end method

.method public show(Z)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/editor/AnnotationEditor;->fragment:Lcom/pspdfkit/internal/v0;

    invoke-virtual {p1}, Landroidx/fragment/app/DialogFragment;->isAdded()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/editor/AnnotationEditor;->fragment:Lcom/pspdfkit/internal/v0;

    iget-object v0, p0, Lcom/pspdfkit/ui/editor/AnnotationEditor;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    const-string v1, "PSPDFKit.AnnotationEditor"

    invoke-virtual {p1, v0, v1}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/editor/AnnotationEditor;->fragment:Lcom/pspdfkit/internal/v0;

    invoke-virtual {p1}, Landroidx/fragment/app/DialogFragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->executePendingTransactions()Z

    return-void
.end method

.class public Lcom/pspdfkit/ui/editor/UnitSelectionEditText;
.super Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/editor/UnitSelectionEditText$UnitSelectionListener;
    }
.end annotation


# static fields
.field private static final ACTION_ID:I = 0x6

.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final REGEX_ALL_NOT_DIGITS:Ljava/lang/String; = "[^0-9]"

.field private static final REGEX_ONLY_DIGITS:Ljava/lang/String; = "[0-9]"


# instance fields
.field private compiledInputStringPattern:Ljava/util/regex/Pattern;

.field private defaultValue:I

.field private listener:Lcom/pspdfkit/ui/editor/UnitSelectionEditText$UnitSelectionListener;

.field private maximumValue:I

.field private minimumValue:I

.field private onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field private onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private textWatcher:Landroid/text/TextWatcher;

.field private unit:Ljava/lang/String;

.field private unitLabel:Ljava/lang/String;

.field private unitLengthNotSelectable:I


# direct methods
.method static bridge synthetic -$$Nest$fgetcompiledInputStringPattern(Lcom/pspdfkit/ui/editor/UnitSelectionEditText;)Ljava/util/regex/Pattern;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->compiledInputStringPattern:Ljava/util/regex/Pattern;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmaximumValue(Lcom/pspdfkit/ui/editor/UnitSelectionEditText;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->maximumValue:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetminimumValue(Lcom/pspdfkit/ui/editor/UnitSelectionEditText;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->minimumValue:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetunit(Lcom/pspdfkit/ui/editor/UnitSelectionEditText;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unit:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetunitLabel(Lcom/pspdfkit/ui/editor/UnitSelectionEditText;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLabel:Ljava/lang/String;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;-><init>(Landroid/content/Context;)V

    const-string p1, ""

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLabel:Ljava/lang/String;

    .line 48
    invoke-direct {p0}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string p1, ""

    .line 50
    iput-object p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLabel:Ljava/lang/String;

    .line 101
    invoke-direct {p0}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 102
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string p1, ""

    .line 103
    iput-object p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLabel:Ljava/lang/String;

    .line 160
    invoke-direct {p0}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->init()V

    return-void
.end method

.method private generateCompiledInputStringPattern(I)V
    .locals 3

    .line 1
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    .line 3
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    iget-object p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unit:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const-string p1, "^\\d{0,%d}%s$"

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 4
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->compiledInputStringPattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method private init()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLengthNotSelectable:I

    const/4 v0, 0x6

    .line 2
    invoke-virtual {p0, v0}, Landroidx/appcompat/widget/AppCompatEditText;->setImeOptions(I)V

    const-string v0, ""

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLabel:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public focusCheck()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Landroid/widget/EditText;->setSelection(II)V

    :cond_0
    return-void
.end method

.method public getDefaultValue()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->defaultValue:I

    return v0
.end method

.method public getMaximumValue()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->maximumValue:I

    return v0
.end method

.method public getMinimumValue()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->minimumValue:I

    return v0
.end method

.method public getUnitLabel()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getUnitLengthNotSelectable()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLengthNotSelectable:I

    return v0
.end method

.method public getValue()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2
    iget v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->defaultValue:I

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->parseValue(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0
.end method

.method synthetic lambda$setUnitLabel$0$com-pspdfkit-ui-editor-UnitSelectionEditText(Landroid/view/View;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 1
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unit:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result p1

    const/4 p2, 0x0

    invoke-virtual {p0, p2, p1}, Landroid/widget/EditText;->setSelection(II)V

    :cond_0
    return-void
.end method

.method synthetic lambda$setUnitLabel$1$com-pspdfkit-ui-editor-UnitSelectionEditText(Lcom/pspdfkit/ui/editor/UnitSelectionEditText$UnitSelectionListener;Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p4, 0x6

    if-ne p3, p4, :cond_1

    .line 1
    invoke-static {p2}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->getValue()I

    move-result p2

    .line 4
    invoke-interface {p1, p0, p2}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText$UnitSelectionListener;->onValueSet(Lcom/pspdfkit/ui/editor/UnitSelectionEditText;I)V

    .line 6
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->clearFocus()V

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method protected onSelectionChanged(II)V
    .locals 2

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;->onSelectionChanged(II)V

    .line 2
    iget v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLengthNotSelectable:I

    invoke-virtual {p0}, Landroid/widget/TextView;->length()I

    move-result v1

    if-le v0, v1, :cond_0

    return-void

    .line 6
    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->length()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLengthNotSelectable:I

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_1

    invoke-virtual {p0}, Landroid/widget/TextView;->length()I

    move-result p1

    iget v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLengthNotSelectable:I

    sub-int/2addr p1, v0

    if-le p2, p1, :cond_2

    .line 7
    :cond_1
    invoke-virtual {p0}, Landroid/widget/TextView;->length()I

    move-result p1

    iget p2, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLengthNotSelectable:I

    sub-int/2addr p1, p2

    invoke-virtual {p0, p1}, Landroid/widget/EditText;->setSelection(I)V

    :cond_2
    return-void
.end method

.method public parseValue(Ljava/lang/String;)I
    .locals 2

    const-string v0, "[^0-9]"

    const-string v1, ""

    .line 1
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 5
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    iget v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->minimumValue:I

    iget v1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->maximumValue:I

    .line 6
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 7
    :catch_0
    iget p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->defaultValue:I

    :goto_0
    return p1
.end method

.method public setDefaultValue(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->defaultValue:I

    return-void
.end method

.method public setMaximumValue(I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->generateCompiledInputStringPattern(I)V

    .line 2
    iput p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->maximumValue:I

    return-void
.end method

.method public setMinimumValue(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->minimumValue:I

    return-void
.end method

.method public setTextToDefault()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->defaultValue:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->setTextToFormat(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->listener:Lcom/pspdfkit/ui/editor/UnitSelectionEditText$UnitSelectionListener;

    if-eqz v0, :cond_0

    .line 3
    iget v1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->defaultValue:I

    invoke-interface {v0, p0, v1}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText$UnitSelectionListener;->onValueSet(Lcom/pspdfkit/ui/editor/UnitSelectionEditText;I)V

    :cond_0
    return-void
.end method

.method public setTextToFormat(I)V
    .locals 4

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->minimumValue:I

    iget v1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->maximumValue:I

    .line 2
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 3
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLabel:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setUnitLabel(Ljava/lang/String;IIILcom/pspdfkit/ui/editor/UnitSelectionEditText$UnitSelectionListener;)V
    .locals 5

    const-string v0, "unitLabel"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLabel:Ljava/lang/String;

    .line 55
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, p1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "[0-9]"

    const-string v2, ""

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unit:Ljava/lang/String;

    .line 56
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->unitLengthNotSelectable:I

    .line 57
    iput p2, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->defaultValue:I

    if-le p3, p2, :cond_0

    .line 59
    iput p2, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->minimumValue:I

    goto :goto_0

    .line 61
    :cond_0
    iput p3, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->minimumValue:I

    :goto_0
    if-ge p4, p2, :cond_1

    .line 64
    iput p2, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->maximumValue:I

    goto :goto_1

    .line 66
    :cond_1
    iput p4, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->maximumValue:I

    .line 68
    :goto_1
    iget p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->maximumValue:I

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->generateCompiledInputStringPattern(I)V

    .line 69
    iput-object p5, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->listener:Lcom/pspdfkit/ui/editor/UnitSelectionEditText$UnitSelectionListener;

    .line 72
    iget-object p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->textWatcher:Landroid/text/TextWatcher;

    if-eqz p1, :cond_2

    .line 73
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 76
    :cond_2
    new-instance p1, Lcom/pspdfkit/ui/editor/UnitSelectionEditText$1;

    invoke-direct {p1, p0}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText$1;-><init>(Lcom/pspdfkit/ui/editor/UnitSelectionEditText;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->textWatcher:Landroid/text/TextWatcher;

    .line 95
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 98
    iget-object p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    if-eqz p1, :cond_3

    .line 99
    invoke-virtual {p0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 102
    :cond_3
    new-instance p1, Lcom/pspdfkit/ui/editor/UnitSelectionEditText$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/editor/UnitSelectionEditText;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 107
    invoke-virtual {p0, p1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 110
    iget-object p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    if-eqz p1, :cond_4

    .line 111
    invoke-virtual {p0, v1}, Landroidx/appcompat/widget/AppCompatEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 113
    :cond_4
    new-instance p1, Lcom/pspdfkit/ui/editor/UnitSelectionEditText$$ExternalSyntheticLambda1;

    invoke-direct {p1, p0, p5}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/editor/UnitSelectionEditText;Lcom/pspdfkit/ui/editor/UnitSelectionEditText$UnitSelectionListener;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 128
    invoke-virtual {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

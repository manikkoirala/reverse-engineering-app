.class Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/PdfThumbnailGrid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecyclerViewListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/ui/PdfThumbnailGrid;Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;-><init>(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V

    return-void
.end method


# virtual methods
.method public onPageClick(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->-$$Nest$fgetonPageClickListeners(Lcom/pspdfkit/ui/PdfThumbnailGrid;)Ljava/util/List;

    move-result-object v0

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-static {v1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->-$$Nest$fgetonPageClickListeners(Lcom/pspdfkit/ui/PdfThumbnailGrid;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-static {v1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->-$$Nest$fgetonPageClickListeners(Lcom/pspdfkit/ui/PdfThumbnailGrid;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/PdfThumbnailGrid$OnPageClickListener;

    .line 5
    iget-object v3, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-interface {v2, v3, p1}, Lcom/pspdfkit/ui/PdfThumbnailGrid$OnPageClickListener;->onPageClick(Lcom/pspdfkit/ui/PdfThumbnailGrid;I)V

    goto :goto_0

    .line 8
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public onPageLongClick(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    iget-object v0, v0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditModeActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->-$$Nest$fgetdocumentEditorEnabled(Lcom/pspdfkit/ui/PdfThumbnailGrid;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->enterDocumentEditingMode()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->-$$Nest$fgetrecyclerView(Lcom/pspdfkit/ui/PdfThumbnailGrid;)Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->e(I)V

    :cond_0
    return-void
.end method

.method public onPageMoved(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->-$$Nest$fgetdocumentEditor(Lcom/pspdfkit/ui/PdfThumbnailGrid;)Lcom/pspdfkit/internal/k8;

    move-result-object v0

    if-eqz v0, :cond_1

    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    .line 6
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 7
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-static {p1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->-$$Nest$fgetdocumentEditor(Lcom/pspdfkit/ui/PdfThumbnailGrid;)Lcom/pspdfkit/internal/k8;

    move-result-object p1

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/k8;->movePages(Ljava/util/Set;I)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    :cond_0
    return-void

    .line 9
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "Document Editor cannot be null."

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method public onPageSelectionStateChanged()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->-$$Nest$fgetdocumentEditorEnabled(Lcom/pspdfkit/ui/PdfThumbnailGrid;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m8;->b()V

    :cond_0
    return-void
.end method

.method public onStartDraggingPages()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->-$$Nest$manimateHideFab(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V

    return-void
.end method

.method public onStopDraggingPages()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->-$$Nest$manimateShowFab(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->-$$Nest$fgetdocumentEditorEnabled(Lcom/pspdfkit/ui/PdfThumbnailGrid;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;->this$0:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m8;->b()V

    :cond_0
    return-void
.end method

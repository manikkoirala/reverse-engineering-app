.class public Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;
    }
.end annotation


# static fields
.field private static final ANIMATION_DURATION_MS:I = 0xfa


# instance fields
.field private backgroundColor:Ljava/lang/Integer;

.field private boldButton:Landroidx/appcompat/widget/AppCompatCheckBox;

.field private clearButton:Landroid/widget/TextView;

.field private colorButtonFillColor:Ljava/lang/Integer;

.field private contentEditingBarLayout:Landroid/view/View;

.field private controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

.field private currentStyleInfo:Lcom/pspdfkit/internal/jt;

.field private currentlyEditedTextBlockId:Ljava/util/UUID;

.field private decreaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

.field private fontButtonsTintColor:Ljava/lang/Integer;

.field private fontButtonsTintColorChecked:Ljava/lang/Integer;

.field private fontColorButton:Landroidx/appcompat/widget/AppCompatImageView;

.field private fontNameButtonPhone:Landroidx/appcompat/widget/AppCompatImageView;

.field private fontNameText:Landroid/widget/TextView;

.field private fontSizeButton:Landroid/view/View;

.field private fontSizeButtonPhone:Landroidx/appcompat/widget/AppCompatImageView;

.field private fontSizeText:Landroid/widget/TextView;

.field private fontSizeUnit:Ljava/lang/String;

.field private fontSizeUnitText:Landroid/widget/TextView;

.field private fontSmallButtonsTintColor:Ljava/lang/Integer;

.field private iconBorderColor:Ljava/lang/Integer;

.field private iconColor:Ljava/lang/Integer;

.field private increaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

.field private isDisplayed:Z

.field private italicButton:Landroidx/appcompat/widget/AppCompatCheckBox;

.field private final lifecycleListeners:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;",
            ">;"
        }
    .end annotation
.end field

.field private systemUiVisibleLock:Lcom/pspdfkit/internal/ui/a$b;

.field private textColor:Ljava/lang/Integer;

.field private unknownColorOverlay:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    const/4 v0, 0x0

    .line 12
    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentStyleInfo:Lcom/pspdfkit/internal/jt;

    const-string v1, "pt"

    .line 41
    iput-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeUnit:Ljava/lang/String;

    .line 620
    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentlyEditedTextBlockId:Ljava/util/UUID;

    const/4 v1, 0x0

    .line 621
    invoke-direct {p0, p1, v0, v1, v1}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->init(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 622
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 623
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    const/4 v0, 0x0

    .line 633
    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentStyleInfo:Lcom/pspdfkit/internal/jt;

    const-string v1, "pt"

    .line 662
    iput-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeUnit:Ljava/lang/String;

    .line 1241
    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentlyEditedTextBlockId:Ljava/util/UUID;

    const/4 v0, 0x0

    .line 1242
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->init(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 1243
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1244
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    const/4 v0, 0x0

    .line 1254
    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentStyleInfo:Lcom/pspdfkit/internal/jt;

    const-string v1, "pt"

    .line 1283
    iput-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeUnit:Ljava/lang/String;

    .line 1862
    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentlyEditedTextBlockId:Ljava/util/UUID;

    const/4 v0, 0x0

    .line 1863
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->init(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 2

    .line 1864
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 1865
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    const/4 v0, 0x0

    .line 1875
    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentStyleInfo:Lcom/pspdfkit/internal/jt;

    const-string v1, "pt"

    .line 1904
    iput-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeUnit:Ljava/lang/String;

    .line 2483
    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentlyEditedTextBlockId:Ljava/util/UUID;

    .line 2484
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->init(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method private createColorCircleDrawable()Lcom/pspdfkit/internal/o5;
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->iconBorderColor:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->colorButtonFillColor:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/o5;->a(Landroid/content/Context;II)Lcom/pspdfkit/internal/o5;

    move-result-object v0

    return-object v0
.end method

.method private extractFontSizeUnit(Landroid/content/Context;)V
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$string;->pspdf__unit_pt:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "%1$s"

    const-string v1, ""

    .line 2
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 10
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne p1, v1, :cond_0

    return-void

    .line 12
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeUnit:Ljava/lang/String;

    return-void
.end method

.method private hide()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->isDisplayed:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->isDisplayed:Z

    const/4 v0, 0x0

    .line 4
    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    .line 5
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0xfa

    .line 8
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V

    .line 9
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;

    .line 18
    invoke-interface {v1, p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;->onRemoveContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x1010440

    const/4 v2, 0x0

    aput v1, v0, v2

    .line 2
    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 4
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget p4, Lcom/pspdfkit/R$dimen;->pspdf__form_editing_bar_elevation:I

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p3

    .line 5
    invoke-virtual {p2, v2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result p3

    .line 7
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    int-to-float p2, p3

    .line 9
    invoke-static {p0, p2}, Landroidx/core/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    .line 11
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->extractFontSizeUnit(Landroid/content/Context;)V

    const/16 p1, 0x8

    .line 14
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setVisibility(I)V

    return-void
.end method

.method private prepareForDisplay()V
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->contentEditingBarLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__content_editing_bar:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 7
    sget v1, Lcom/pspdfkit/R$id;->pspdf__content_editing_bar_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->contentEditingBarLayout:Landroid/view/View;

    .line 8
    sget v1, Lcom/pspdfkit/R$id;->top_divider:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 9
    sget v3, Lcom/pspdfkit/R$id;->pspdf__font_bold:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroidx/appcompat/widget/AppCompatCheckBox;

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->boldButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 10
    sget v3, Lcom/pspdfkit/R$id;->pspdf__font_italic:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroidx/appcompat/widget/AppCompatCheckBox;

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->italicButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 13
    sget v3, Lcom/pspdfkit/R$id;->pspdf__content_editing_font_name_textbutton:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontNameText:Landroid/widget/TextView;

    .line 14
    sget v3, Lcom/pspdfkit/R$id;->pspdf__content_editing_font_size_text:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeText:Landroid/widget/TextView;

    .line 15
    sget v3, Lcom/pspdfkit/R$id;->pspdf__content_editing_font_size_unit_text:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeUnitText:Landroid/widget/TextView;

    .line 16
    sget v3, Lcom/pspdfkit/R$id;->pspdf__content_editing_increase_font_size_button:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroidx/appcompat/widget/AppCompatImageView;

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->increaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    .line 17
    sget v3, Lcom/pspdfkit/R$id;->pspdf__content_editing_decrease_font_size_button:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroidx/appcompat/widget/AppCompatImageView;

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->decreaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    .line 18
    sget v3, Lcom/pspdfkit/R$id;->pspdf__layout_content_editing_font_size_compound_button:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeButton:Landroid/view/View;

    .line 21
    sget v3, Lcom/pspdfkit/R$id;->pspdf__content_editing_font_name_imagebutton:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroidx/appcompat/widget/AppCompatImageView;

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontNameButtonPhone:Landroidx/appcompat/widget/AppCompatImageView;

    .line 22
    sget v3, Lcom/pspdfkit/R$id;->pspdf__content_editing_font_size_imagebutton:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroidx/appcompat/widget/AppCompatImageView;

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeButtonPhone:Landroidx/appcompat/widget/AppCompatImageView;

    .line 25
    sget v3, Lcom/pspdfkit/R$id;->pspdf__content_editing_font_color:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroidx/appcompat/widget/AppCompatImageView;

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontColorButton:Landroidx/appcompat/widget/AppCompatImageView;

    .line 26
    sget v3, Lcom/pspdfkit/R$id;->pspdf_unknown_color_overlay:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->unknownColorOverlay:Landroid/view/View;

    .line 27
    sget v3, Lcom/pspdfkit/R$id;->pspdf__font_bold:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroidx/appcompat/widget/AppCompatCheckBox;

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->boldButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 28
    sget v3, Lcom/pspdfkit/R$id;->pspdf__font_italic:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroidx/appcompat/widget/AppCompatCheckBox;

    iput-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->italicButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 29
    sget v3, Lcom/pspdfkit/R$id;->pspdf__content_editing_clear_button:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->clearButton:Landroid/widget/TextView;

    const/16 v3, 0xa

    new-array v3, v3, [Landroid/view/View;

    .line 31
    iget-object v4, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontNameText:Landroid/widget/TextView;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->increaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    aput-object v4, v3, v2

    iget-object v2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->decreaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v4, 0x2

    aput-object v2, v3, v4

    iget-object v2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeButton:Landroid/view/View;

    const/4 v4, 0x3

    aput-object v2, v3, v4

    iget-object v2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontNameButtonPhone:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v4, 0x4

    aput-object v2, v3, v4

    iget-object v2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeButtonPhone:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v4, 0x5

    aput-object v2, v3, v4

    iget-object v2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontColorButton:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v4, 0x6

    aput-object v2, v3, v4

    iget-object v2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->boldButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    const/4 v4, 0x7

    aput-object v2, v3, v4

    iget-object v2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->italicButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    const/16 v4, 0x8

    aput-object v2, v3, v4

    const/16 v2, 0x9

    aput-object v0, v3, v2

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 43
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    if-eqz v2, :cond_1

    .line 44
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 57
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v2, Lcom/pspdfkit/R$styleable;->pspdf__contentEditingStylingBar:[I

    sget v3, Lcom/pspdfkit/R$attr;->pspdf__contentEditingStylingBarStyle:I

    sget v4, Lcom/pspdfkit/R$style;->PSPDFKit_ContentEditingStylingBar:I

    const/4 v5, 0x0

    .line 59
    invoke-virtual {v0, v5, v2, v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 64
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_black:I

    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 65
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/pspdfkit/R$color;->pspdf__gray_10:I

    invoke-static {v3, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 66
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/pspdfkit/R$color;->pspdf__gray_20:I

    invoke-static {v4, v5}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    .line 67
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/pspdfkit/R$color;->pspdf__gray_20:I

    invoke-static {v5, v6}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v5

    .line 69
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/pspdfkit/R$color;->pspdf__color_gray_light:I

    const v8, 0x1010031

    invoke-static {v6, v8, v7}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;II)I

    move-result v6

    .line 70
    iget-object v7, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->backgroundColor:Ljava/lang/Integer;

    if-eqz v7, :cond_3

    .line 71
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto :goto_1

    .line 72
    :cond_3
    sget v7, Lcom/pspdfkit/R$styleable;->pspdf__contentEditingStylingBar_pspdf__backgroundColor:I

    invoke-virtual {v0, v7, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v6

    .line 74
    :goto_1
    iget-object v7, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->textColor:Ljava/lang/Integer;

    if-eqz v7, :cond_4

    .line 75
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    goto :goto_2

    .line 76
    :cond_4
    sget v7, Lcom/pspdfkit/R$styleable;->pspdf__contentEditingStylingBar_pspdf__textColor:I

    invoke-virtual {v0, v7, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v7

    .line 77
    :goto_2
    iget-object v8, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->iconColor:Ljava/lang/Integer;

    if-eqz v8, :cond_5

    .line 78
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    goto :goto_3

    .line 79
    :cond_5
    sget v8, Lcom/pspdfkit/R$styleable;->pspdf__contentEditingStylingBar_pspdf__iconsColor:I

    invoke-virtual {v0, v8, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v8

    .line 80
    :goto_3
    iget-object v9, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->iconBorderColor:Ljava/lang/Integer;

    if-eqz v9, :cond_6

    .line 81
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    goto :goto_4

    .line 82
    :cond_6
    sget v9, Lcom/pspdfkit/R$styleable;->pspdf__contentEditingStylingBar_pspdf__iconBorderColor:I

    invoke-virtual {v0, v9, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v9

    .line 83
    :goto_4
    iget-object v10, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontButtonsTintColor:Ljava/lang/Integer;

    if-eqz v10, :cond_7

    .line 84
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    goto :goto_5

    .line 85
    :cond_7
    sget v10, Lcom/pspdfkit/R$styleable;->pspdf__contentEditingStylingBar_pspdf__fontButtonsTintColor:I

    invoke-virtual {v0, v10, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v10

    .line 88
    :goto_5
    iget-object v11, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontButtonsTintColor:Ljava/lang/Integer;

    if-eqz v11, :cond_8

    .line 89
    iget-object v4, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontButtonsTintColorChecked:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_6

    .line 90
    :cond_8
    sget v11, Lcom/pspdfkit/R$styleable;->pspdf__contentEditingStylingBar_pspdf__fontButtonsTintColorChecked:I

    invoke-virtual {v0, v11, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    .line 93
    :goto_6
    iget-object v11, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontButtonsTintColor:Ljava/lang/Integer;

    if-eqz v11, :cond_9

    .line 94
    iget-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSmallButtonsTintColor:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto :goto_7

    .line 95
    :cond_9
    sget v11, Lcom/pspdfkit/R$styleable;->pspdf__contentEditingStylingBar_pspdf__fontSmallButtonsTintColor:I

    invoke-virtual {v0, v11, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 99
    :goto_7
    sget v11, Lcom/pspdfkit/R$styleable;->pspdf__contentEditingStylingBar_pspdf__borderColor:I

    invoke-virtual {v0, v11, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    .line 100
    iget-object v11, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->colorButtonFillColor:Ljava/lang/Integer;

    if-eqz v11, :cond_a

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 101
    :cond_a
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 103
    invoke-virtual {v1, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 104
    invoke-virtual {p0, v8}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setIconsColor(I)V

    .line 105
    invoke-direct {p0, v2}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setColorButtonFillColor(I)V

    .line 106
    invoke-virtual {p0, v9}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setIconBorderColor(I)V

    .line 107
    invoke-virtual {p0, v10}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setFontButtonsColor(I)V

    .line 108
    invoke-virtual {p0, v4}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setFontButtonsColorChecked(I)V

    .line 109
    invoke-virtual {p0, v3}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setFontSmallButtonsColor(I)V

    .line 111
    invoke-direct {p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->createColorCircleDrawable()Lcom/pspdfkit/internal/o5;

    move-result-object v0

    .line 113
    invoke-direct {p0, v10, v4, v3}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setButtonsBackgroundColor(III)V

    .line 114
    invoke-virtual {p0, v6}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setBackgroundColor(I)V

    .line 115
    invoke-virtual {p0, v7}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setTextColor(I)V

    .line 116
    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontColorButton:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 117
    invoke-direct {p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->updateClearButton()V

    return-void
.end method

.method private setButtonsBackgroundColor(III)V
    .locals 7

    const/4 v0, 0x5

    new-array v0, v0, [Landroid/view/View;

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontNameText:Landroid/widget/TextView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontNameButtonPhone:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeButton:Landroid/view/View;

    const/4 v4, 0x2

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeButtonPhone:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v5, 0x3

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontColorButton:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v6, 0x4

    aput-object v1, v0, v6

    .line 2
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 3
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 4
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setStateBackgroundColors(Ljava/util/List;Landroid/content/res/ColorStateList;)V

    new-array v0, v4, [Landroid/view/View;

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->increaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->decreaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    aput-object v1, v0, v3

    .line 9
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 10
    invoke-static {p3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p3

    .line 11
    invoke-direct {p0, v0, p3}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setStateBackgroundColors(Ljava/util/List;Landroid/content/res/ColorStateList;)V

    new-array p3, v6, [[I

    new-array v0, v4, [I

    .line 15
    fill-array-data v0, :array_0

    aput-object v0, p3, v2

    new-array v0, v3, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    aput-object v0, p3, v3

    new-array v0, v3, [I

    const v1, 0x10100a0

    aput v1, v0, v2

    aput-object v0, p3, v4

    new-array v0, v2, [I

    aput-object v0, p3, v5

    new-array v0, v6, [I

    aput p2, v0, v2

    aput p2, v0, v3

    aput p2, v0, v4

    aput p1, v0, v5

    new-array p1, v4, [Landroid/view/View;

    .line 26
    iget-object p2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->boldButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    aput-object p2, p1, v2

    iget-object p2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->italicButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    aput-object p2, p1, v3

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-instance p2, Landroid/content/res/ColorStateList;

    invoke-direct {p2, p3, v0}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setStateBackgroundColors(Ljava/util/List;Landroid/content/res/ColorStateList;)V

    return-void

    :array_0
    .array-data 4
        0x10100a0
        0x10100a7
    .end array-data
.end method

.method private setColorButtonFillColor(I)V
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->colorButtonFillColor:Ljava/lang/Integer;

    return-void
.end method

.method private setPrimaryTextColor(Landroid/widget/TextView;I)V
    .locals 8

    .line 1
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    sget-object v1, Landroid/widget/FrameLayout;->EMPTY_STATE_SET:[I

    invoke-virtual {v0, v1, p2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    goto :goto_0

    :cond_0
    move v0, p2

    .line 4
    :goto_0
    new-instance v1, Landroid/content/res/ColorStateList;

    const/4 v2, 0x2

    new-array v3, v2, [[I

    const/4 v4, 0x1

    new-array v5, v4, [I

    const v6, 0x101009e

    const/4 v7, 0x0

    aput v6, v5, v7

    aput-object v5, v3, v7

    sget-object v5, Landroid/widget/FrameLayout;->EMPTY_STATE_SET:[I

    aput-object v5, v3, v4

    new-array v2, v2, [I

    aput p2, v2, v7

    aput v0, v2, v4

    invoke-direct {v1, v3, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 7
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method private setStateBackgroundColors(Ljava/util/List;Landroid/content/res/ColorStateList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Landroid/content/res/ColorStateList;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p2}, Landroid/view/View;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private show()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->isDisplayed:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->isDisplayed:Z

    const/4 v0, 0x0

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setVisibility(I)V

    .line 5
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0xfa

    .line 8
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V

    .line 9
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 15
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->systemUiVisibleLock:Lcom/pspdfkit/internal/ui/a$b;

    .line 16
    invoke-static {v0}, Lcom/pspdfkit/internal/ce;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/ui/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 18
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/a;->e()Lcom/pspdfkit/internal/ui/a$b;

    move-result-object v2

    if-eqz v1, :cond_2

    .line 20
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/a;->a(Lcom/pspdfkit/internal/ui/a$b;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 21
    :cond_2
    :goto_0
    iput-object v2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->systemUiVisibleLock:Lcom/pspdfkit/internal/ui/a$b;

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;

    .line 24
    invoke-interface {v1, p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;->onPrepareContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method private updateCheckButton(Landroidx/appcompat/widget/AppCompatCheckBox;ZZ)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 1
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2
    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 3
    invoke-virtual {p1, p3}, Landroid/view/View;->setEnabled(Z)V

    if-eqz p3, :cond_1

    const/high16 p2, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_1
    const/high16 p2, 0x3f000000    # 0.5f

    .line 4
    :goto_0
    invoke-virtual {p1, p2}, Landroidx/appcompat/widget/AppCompatCheckBox;->setAlpha(F)V

    .line 5
    invoke-virtual {p1, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method private updateClearButton()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->clearButton:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->isClearContentEditingEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method private updateColorButton(Ljava/lang/Integer;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontColorButton:Landroidx/appcompat/widget/AppCompatImageView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x4

    if-nez p1, :cond_1

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontButtonsTintColor:Ljava/lang/Integer;

    const/4 v0, 0x0

    .line 8
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setColorButtonFillColor(I)V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontColorButton:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-direct {p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->createColorCircleDrawable()Lcom/pspdfkit/internal/o5;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->unknownColorOverlay:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private updateDisplayedStyle(Lcom/pspdfkit/internal/jt;)V
    .locals 6

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/jt;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->j()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setFontNameText(Ljava/lang/String;Z)V

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, " ? "

    .line 3
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeUnit:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setFontSizeText(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->c()Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->updateColorButton(Ljava/lang/Integer;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->boldButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->a()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    iget-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_1

    .line 8
    invoke-interface {v3, p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->isBoldStyleButtonEnabled(Lcom/pspdfkit/internal/jt;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    .line 9
    :goto_0
    invoke-direct {p0, v0, v2, v3}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->updateCheckButton(Landroidx/appcompat/widget/AppCompatCheckBox;ZZ)V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->italicButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->f()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v2, :cond_2

    .line 16
    invoke-interface {v2, p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->isItalicStyleButtonEnabled(Lcom/pspdfkit/internal/jt;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    .line 17
    :goto_1
    invoke-direct {p0, v0, v1, v4}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->updateCheckButton(Landroidx/appcompat/widget/AppCompatCheckBox;ZZ)V

    .line 21
    iput-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentStyleInfo:Lcom/pspdfkit/internal/jt;

    .line 22
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->updateSizeButtons(Lcom/pspdfkit/internal/jt;)V

    return-void
.end method

.method private updateSizeButtons(Lcom/pspdfkit/internal/jt;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getCurrentFormatter()Lcom/pspdfkit/internal/h6;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 3
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->increaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x3f000000    # 0.5f

    if-eqz v3, :cond_3

    if-eqz v0, :cond_1

    .line 4
    iget-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 5
    invoke-interface {v3}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getCurrentFormatter()Lcom/pspdfkit/internal/h6;

    move-result-object v3

    invoke-interface {v3, p1}, Lcom/pspdfkit/internal/h6;->b(Lcom/pspdfkit/internal/jt;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    .line 6
    :goto_1
    iget-object v6, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->increaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v6, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 7
    iget-object v6, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->increaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    if-eqz v3, :cond_2

    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_2
    const/high16 v3, 0x3f000000    # 0.5f

    :goto_2
    invoke-virtual {v6, v3}, Landroidx/appcompat/widget/AppCompatImageView;->setAlpha(F)V

    .line 10
    :cond_3
    iget-object v3, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->decreaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    if-eqz v3, :cond_6

    if-eqz v0, :cond_4

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 12
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getCurrentFormatter()Lcom/pspdfkit/internal/h6;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/h6;->d(Lcom/pspdfkit/internal/jt;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    .line 13
    :goto_3
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->decreaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p1, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->decreaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    if-eqz v1, :cond_5

    goto :goto_4

    :cond_5
    const/high16 v4, 0x3f000000    # 0.5f

    :goto_4
    invoke-virtual {p1, v4}, Landroidx/appcompat/widget/AppCompatImageView;->setAlpha(F)V

    :cond_6
    return-void
.end method


# virtual methods
.method public addOnContentEditingBarLifecycleListener(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;)V
    .locals 2

    const-string v0, "Content Editing Listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public bindController(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getContentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;->addOnContentEditingContentChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;)V

    .line 3
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getCurrentStyleInfo()Lcom/pspdfkit/internal/jt;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 4
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->updateDisplayedStyle(Lcom/pspdfkit/internal/jt;)V

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->updateClearButton()V

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->show()V

    return-void
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 3

    .line 1
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->right:I

    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v1, p1}, Landroid/view/View;->setPadding(IIII)V

    return v2
.end method

.method public getBackgroundColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->backgroundColor:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public isDisplayed()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->isDisplayed:Z

    return v0
.end method

.method lambda$hide$1$com-pspdfkit-ui-contentediting-ContentEditingStylingBar()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->systemUiVisibleLock:Lcom/pspdfkit/internal/ui/a$b;

    if-eqz v0, :cond_1

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->systemUiVisibleLock:Lcom/pspdfkit/internal/ui/a$b;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/ce;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/ui/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/a;->a(Lcom/pspdfkit/internal/ui/a$b;)V

    :cond_0
    const/4 v0, 0x0

    .line 6
    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->systemUiVisibleLock:Lcom/pspdfkit/internal/ui/a$b;

    :cond_1
    const/4 v0, 0x4

    .line 8
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setVisibility(I)V

    return-void
.end method

.method synthetic lambda$show$0$com-pspdfkit-ui-contentediting-ContentEditingStylingBar()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;

    .line 2
    invoke-interface {v1, p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;->onDisplayContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getCurrentFormatter()Lcom/pspdfkit/internal/h6;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 4
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->boldButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    if-ne p1, v0, :cond_2

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getCurrentFormatter()Lcom/pspdfkit/internal/h6;

    move-result-object p1

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/h6;->setBold(Z)V

    goto :goto_0

    .line 6
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->italicButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    if-ne p1, v0, :cond_3

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getCurrentFormatter()Lcom/pspdfkit/internal/h6;

    move-result-object p1

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/h6;->setItalic(Z)V

    :cond_3
    :goto_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->clearButton:Landroid/widget/TextView;

    if-ne p1, v1, :cond_1

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->clearContentEditing()V

    goto :goto_2

    .line 4
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontNameText:Landroid/widget/TextView;

    if-eq p1, v1, :cond_7

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontNameButtonPhone:Landroidx/appcompat/widget/AppCompatImageView;

    if-ne p1, v1, :cond_2

    goto :goto_1

    .line 6
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeButton:Landroid/view/View;

    if-eq p1, v1, :cond_6

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeButtonPhone:Landroidx/appcompat/widget/AppCompatImageView;

    if-ne p1, v1, :cond_3

    goto :goto_0

    .line 8
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontColorButton:Landroidx/appcompat/widget/AppCompatImageView;

    if-ne p1, v1, :cond_4

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentStyleInfo:Lcom/pspdfkit/internal/jt;

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->displayColorPicker(Lcom/pspdfkit/internal/jt;)V

    goto :goto_2

    .line 10
    :cond_4
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getCurrentFormatter()Lcom/pspdfkit/internal/h6;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentStyleInfo:Lcom/pspdfkit/internal/jt;

    if-eqz v0, :cond_8

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->increaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    if-ne p1, v0, :cond_5

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getCurrentFormatter()Lcom/pspdfkit/internal/h6;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentStyleInfo:Lcom/pspdfkit/internal/jt;

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/h6;->c(Lcom/pspdfkit/internal/jt;)V

    goto :goto_2

    .line 13
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->decreaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    if-ne p1, v0, :cond_8

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getCurrentFormatter()Lcom/pspdfkit/internal/h6;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentStyleInfo:Lcom/pspdfkit/internal/jt;

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/h6;->a(Lcom/pspdfkit/internal/jt;)V

    goto :goto_2

    .line 15
    :cond_6
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentStyleInfo:Lcom/pspdfkit/internal/jt;

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->displayFontSizesSheet(Lcom/pspdfkit/internal/jt;)V

    goto :goto_2

    .line 16
    :cond_7
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentStyleInfo:Lcom/pspdfkit/internal/jt;

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->displayFontNamesSheet(Lcom/pspdfkit/internal/jt;)V

    :cond_8
    :goto_2
    return-void
.end method

.method public onContentChange(Ljava/util/UUID;)V
    .locals 0

    return-void
.end method

.method public onContentSelectionChange(Ljava/util/UUID;IILcom/pspdfkit/internal/jt;Z)V
    .locals 0

    if-nez p5, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentStyleInfo:Lcom/pspdfkit/internal/jt;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p4}, Lcom/pspdfkit/internal/jt;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 2
    :cond_0
    invoke-direct {p0, p4}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->updateDisplayedStyle(Lcom/pspdfkit/internal/jt;)V

    :cond_1
    return-void
.end method

.method public onEnterContentEditingMode(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V
    .locals 0

    return-void
.end method

.method public onExitContentEditingMode(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V
    .locals 0

    return-void
.end method

.method public onFinishEditingContentBlock(Ljava/util/UUID;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentlyEditedTextBlockId:Ljava/util/UUID;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentlyEditedTextBlockId:Ljava/util/UUID;

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentStyleInfo:Lcom/pspdfkit/internal/jt;

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->updateClearButton()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 2
    iget-boolean p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->isDisplayed:Z

    if-nez p1, :cond_0

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    int-to-float p1, p1

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    :cond_0
    return-void
.end method

.method public onStartEditingContentBlock(Ljava/util/UUID;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->currentlyEditedTextBlockId:Ljava/util/UUID;

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->updateClearButton()V

    return-void
.end method

.method public removeOnContentEditingBarLifecycleListener(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;)V
    .locals 2

    const-string v0, "Content Editing Listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->backgroundColor:Ljava/lang/Integer;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->contentEditingBarLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 5
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    return-void
.end method

.method public setFontButtonsColor(I)V
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontButtonsTintColor:Ljava/lang/Integer;

    return-void
.end method

.method public setFontButtonsColorChecked(I)V
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontButtonsTintColorChecked:Ljava/lang/Integer;

    return-void
.end method

.method public setFontNameText(Ljava/lang/String;Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontNameText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "fontName"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    if-nez p2, :cond_1

    .line 55
    new-instance p2, Landroid/text/SpannableString;

    invoke-direct {p2, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 56
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 57
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v1, 0x0

    const/16 v2, 0x21

    .line 58
    invoke-virtual {p2, v0, v1, p1, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 60
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontNameText:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 63
    :cond_1
    iget-object p2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontNameText:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setFontSizeText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeUnitText:Landroid/widget/TextView;

    if-nez p1, :cond_1

    return-void

    .line 4
    :cond_1
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setFontSmallButtonsColor(I)V
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSmallButtonsTintColor:Ljava/lang/Integer;

    return-void
.end method

.method public setIconBorderColor(I)V
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->iconBorderColor:Ljava/lang/Integer;

    return-void
.end method

.method public setIconsColor(I)V
    .locals 6

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->iconColor:Ljava/lang/Integer;

    const/4 v0, 0x4

    new-array v0, v0, [Landroidx/appcompat/widget/AppCompatImageView;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontNameButtonPhone:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeButtonPhone:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->increaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v4, 0x2

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->decreaseFontSizeButton:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v5, 0x3

    aput-object v1, v0, v5

    .line 3
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-array v1, v4, [Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 4
    iget-object v4, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->boldButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    aput-object v4, v1, v2

    iget-object v2, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->italicButton:Landroidx/appcompat/widget/AppCompatCheckBox;

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/appcompat/widget/AppCompatImageView;

    if-eqz v2, :cond_0

    .line 6
    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 7
    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v2, p1}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    goto :goto_0

    .line 10
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 11
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-static {v1, v2}, Landroidx/core/widget/CompoundButtonCompat;->setButtonTintList(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public setTextColor(I)V
    .locals 3

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->textColor:Ljava/lang/Integer;

    const/4 v0, 0x4

    new-array v0, v0, [Landroid/widget/TextView;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontNameText:Landroid/widget/TextView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeText:Landroid/widget/TextView;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->fontSizeUnitText:Landroid/widget/TextView;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->clearButton:Landroid/widget/TextView;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 3
    invoke-direct {p0, v1, p1}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->setPrimaryTextColor(Landroid/widget/TextView;I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setVisibility(I)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    if-nez p1, :cond_0

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->prepareForDisplay()V

    :cond_0
    return-void
.end method

.method public unbindController()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getContentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;->removeOnContentEditingContentChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->hide()V

    return-void
.end method

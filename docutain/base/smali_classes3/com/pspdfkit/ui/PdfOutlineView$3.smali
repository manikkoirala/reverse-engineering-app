.class Lcom/pspdfkit/ui/PdfOutlineView$3;
.super Lcom/pspdfkit/listeners/SimpleDocumentListener;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/PdfOutlineView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/PdfOutlineView;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/PdfOutlineView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView$3;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-direct {p0}, Lcom/pspdfkit/listeners/SimpleDocumentListener;-><init>()V

    return-void
.end method

.method static synthetic lambda$onPageChanged$0(ILcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetbookmarkListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/t4;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/t4;->setCurrentPageIndex(I)V

    return-void
.end method

.method static synthetic lambda$onPageUpdated$1(Lcom/pspdfkit/document/PdfDocument;ILcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p2}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetbookmarkListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/t4;

    move-result-object p2

    invoke-virtual {p2, p0, p1}, Lcom/pspdfkit/internal/t4;->a(Lcom/pspdfkit/document/PdfDocument;I)V

    return-void
.end method


# virtual methods
.method public onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView$3;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView;->-$$Nest$fgetpagerAdapter(Lcom/pspdfkit/ui/PdfOutlineView;)Lcom/pspdfkit/internal/zg;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/ui/PdfOutlineView$3$$ExternalSyntheticLambda1;

    invoke-direct {v0, p2}, Lcom/pspdfkit/ui/PdfOutlineView$3$$ExternalSyntheticLambda1;-><init>(I)V

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$3;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfOutlineView;->-$$Nest$fgetpagerAdapter(Lcom/pspdfkit/ui/PdfOutlineView;)Lcom/pspdfkit/internal/zg;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$3$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1, p2}, Lcom/pspdfkit/ui/PdfOutlineView$3$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/document/PdfDocument;I)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

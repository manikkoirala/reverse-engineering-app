.class public abstract Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/PageObjectProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$DrawableProviderObserver;
    }
.end annotation


# static fields
.field public static final NO_FILTERED_PAGES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final drawableProviderObservers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$DrawableProviderObserver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->drawableProviderObservers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public abstract getDrawablesForPage(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/document/PdfDocument;",
            "I)",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/ui/drawable/PdfDrawable;",
            ">;"
        }
    .end annotation
.end method

.method public getDrawablesForPageAsync(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Lio/reactivex/rxjava3/core/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/document/PdfDocument;",
            "I)",
            "Lio/reactivex/rxjava3/core/Observable<",
            "+",
            "Lcom/pspdfkit/ui/drawable/PdfDrawable;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "document"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    new-instance v0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getFilteredPages()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->NO_FILTERED_PAGES:Ljava/util/Set;

    return-object v0
.end method

.method synthetic lambda$getDrawablesForPageAsync$0$com-pspdfkit-ui-drawable-PdfDrawableProvider(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Lio/reactivex/rxjava3/core/ObservableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->getDrawablesForPage(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    invoke-static {p1}, Lio/reactivex/rxjava3/core/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    goto :goto_0

    .line 4
    :cond_0
    invoke-static {}, Lio/reactivex/rxjava3/core/Observable;->empty()Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public notifyDrawablesChanged()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->drawableProviderObservers:Ljava/util/List;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->drawableProviderObservers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$DrawableProviderObserver;

    .line 3
    invoke-interface {v2, p0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$DrawableProviderObserver;->onDrawablesChanged(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    goto :goto_0

    .line 5
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public notifyDrawablesChanged(I)V
    .locals 3

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->drawableProviderObservers:Ljava/util/List;

    monitor-enter v0

    .line 7
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->drawableProviderObservers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$DrawableProviderObserver;

    .line 8
    invoke-interface {v2, p0, p1}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$DrawableProviderObserver;->onDrawablesChanged(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;I)V

    goto :goto_0

    .line 10
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public registerDrawableProviderObserver(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$DrawableProviderObserver;)V
    .locals 2

    const-string v0, "drawableProviderObserver"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->drawableProviderObservers:Ljava/util/List;

    monitor-enter v0

    .line 55
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->drawableProviderObservers:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->drawableProviderObservers:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public unregisterDrawableProviderObserver(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$DrawableProviderObserver;)V
    .locals 2

    const-string v0, "drawableProviderObserver"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->drawableProviderObservers:Ljava/util/List;

    monitor-enter v0

    .line 55
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->drawableProviderObservers:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 56
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

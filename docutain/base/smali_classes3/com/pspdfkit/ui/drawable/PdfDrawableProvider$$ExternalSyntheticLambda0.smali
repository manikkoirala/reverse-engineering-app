.class public final synthetic Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$$ExternalSyntheticLambda0;
.super Ljava/lang/Object;
.source "D8$$SyntheticClass"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Supplier;


# instance fields
.field public final synthetic f$0:Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;

.field public final synthetic f$1:Landroid/content/Context;

.field public final synthetic f$2:Lcom/pspdfkit/document/PdfDocument;

.field public final synthetic f$3:I


# direct methods
.method public synthetic constructor <init>(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$$ExternalSyntheticLambda0;->f$0:Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;

    iput-object p2, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$$ExternalSyntheticLambda0;->f$1:Landroid/content/Context;

    iput-object p3, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$$ExternalSyntheticLambda0;->f$2:Lcom/pspdfkit/document/PdfDocument;

    iput p4, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$$ExternalSyntheticLambda0;->f$3:I

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$$ExternalSyntheticLambda0;->f$0:Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;

    iget-object v1, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$$ExternalSyntheticLambda0;->f$1:Landroid/content/Context;

    iget-object v2, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$$ExternalSyntheticLambda0;->f$2:Lcom/pspdfkit/document/PdfDocument;

    iget v3, p0, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$$ExternalSyntheticLambda0;->f$3:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->lambda$getDrawablesForPageAsync$0$com-pspdfkit-ui-drawable-PdfDrawableProvider(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Lio/reactivex/rxjava3/core/ObservableSource;

    move-result-object v0

    return-object v0
.end method

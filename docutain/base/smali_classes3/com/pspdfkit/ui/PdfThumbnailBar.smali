.class public Lcom/pspdfkit/ui/PdfThumbnailBar;
.super Lcom/pspdfkit/ui/PdfFrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;
.implements Lcom/pspdfkit/ui/drawable/PdfDrawableManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;,
        Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "PSPDFKit.ThumbnailBar"


# instance fields
.field private final drawableProviderCollection:Lcom/pspdfkit/internal/fm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/fm<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;"
        }
    .end annotation
.end field

.field private onPageChangedListener:Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

.field thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

.field private thumbnailBarMode:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__thumbnailBarStyle:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/pspdfkit/ui/PdfFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2
    new-instance p1, Lcom/pspdfkit/internal/fm;

    invoke-direct {p1}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    .line 15
    sget-object p1, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_NONE:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarMode:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    .line 24
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailBar;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 25
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__thumbnailBarStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/ui/PdfFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    new-instance p1, Lcom/pspdfkit/internal/fm;

    invoke-direct {p1}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    .line 39
    sget-object p1, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_NONE:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarMode:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    .line 53
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailBar;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/ui/PdfFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    new-instance p1, Lcom/pspdfkit/internal/fm;

    invoke-direct {p1}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    .line 68
    sget-object p1, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_NONE:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarMode:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    .line 87
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailBar;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .line 88
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/ui/PdfFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 89
    new-instance p1, Lcom/pspdfkit/internal/fm;

    invoke-direct {p1}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    .line 102
    sget-object p1, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_NONE:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarMode:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    .line 130
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailBar;->init()V

    return-void
.end method

.method private init()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2
    sget-object v0, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_FLOATING:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/PdfThumbnailBar;->setThumbnailBarMode(Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;)V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailBar;->subscribeForCustomDrawableUpdates()V

    return-void
.end method

.method static synthetic lambda$setThumbnailBarMode$3(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;Landroid/view/View;Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result p1

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    .line 4
    invoke-virtual {p2}, Landroid/view/WindowInsets;->getMandatorySystemGestureInsets()Landroid/graphics/Insets;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Insets;->bottom:I

    .line 5
    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    return-object p2
.end method

.method static synthetic lambda$setThumbnailBarMode$4(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result p1

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    .line 4
    invoke-virtual {p2}, Landroidx/core/view/WindowInsetsCompat;->getSystemWindowInsetBottom()I

    move-result v2

    .line 5
    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    return-object p2
.end method

.method private subscribeForCustomDrawableUpdates()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fm;->b()Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 3
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailBar;->updateViewState()Lio/reactivex/rxjava3/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private updateViewState()Lio/reactivex/rxjava3/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/functions/Consumer<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/ui/PdfThumbnailBar;)V

    return-object v0
.end method


# virtual methods
.method public addDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V
    .locals 2

    const-string v0, "drawableProvider"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/fm;->a(Lcom/pspdfkit/ui/PageObjectProvider;)V

    return-void
.end method

.method public addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 55
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    :cond_0
    return-void
.end method

.method public clearDocument()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->clearDocument()V

    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 6
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public getDocumentListener()Lcom/pspdfkit/listeners/DocumentListener;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->getDocumentListener()Lcom/pspdfkit/listeners/DocumentListener;

    move-result-object v0

    return-object v0

    .line 5
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Thumbnail bar mode not set"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method getOnPageChangedListener()Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->onPageChangedListener:Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

    return-object v0
.end method

.method public getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_THUMBNAIL_BAR:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    return-object v0
.end method

.method public getSelectedThumbnailBorderColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->getSelectedThumbnailBorderColor()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getThumbnailBorderColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->getThumbnailBorderColor()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getThumbnailHeight()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->getThumbnailHeight()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public getThumbnailWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->getThumbnailWidth()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public hide()V
    .locals 0

    return-void
.end method

.method public isBackgroundTransparent()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->isBackgroundTransparent()Z

    move-result v0

    return v0

    .line 5
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Thumbnail bar mode not set"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public isDisplayed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isRedactionAnnotationPreviewEnabled()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->isRedactionAnnotationPreviewEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isUsingPageAspectRatio()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->isUsingPageAspectRatio()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method synthetic lambda$setThumbnailBarMode$1$com-pspdfkit-ui-PdfThumbnailBar(Landroid/widget/FrameLayout$LayoutParams;ILcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;Landroid/view/View;Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p4

    sget v0, Lcom/pspdfkit/R$dimen;->pspdf__floating_thumbnail_bar_margin_above_navigation:I

    .line 2
    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p4

    float-to-int p4, p4

    .line 4
    invoke-virtual {p5}, Landroid/view/WindowInsets;->getMandatorySystemGestureInsets()Landroid/graphics/Insets;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Insets;->bottom:I

    add-int/2addr v0, p4

    .line 5
    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 6
    invoke-virtual {p3, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object p5
.end method

.method synthetic lambda$setThumbnailBarMode$2$com-pspdfkit-ui-PdfThumbnailBar(Landroid/widget/FrameLayout$LayoutParams;ILcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p4

    sget v0, Lcom/pspdfkit/R$dimen;->pspdf__floating_thumbnail_bar_margin_above_navigation:I

    .line 2
    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p4

    float-to-int p4, p4

    .line 4
    invoke-virtual {p5}, Landroidx/core/view/WindowInsetsCompat;->getSystemWindowInsetBottom()I

    move-result v0

    add-int/2addr v0, p4

    .line 5
    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 6
    invoke-virtual {p3, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object p5
.end method

.method synthetic lambda$setThumbnailBarMode$5$com-pspdfkit-ui-PdfThumbnailBar(Landroid/widget/FrameLayout$LayoutParams;Landroid/view/View;Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/pspdfkit/R$dimen;->pspdf__floating_thumbnail_bar_margin_above_navigation:I

    .line 2
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    float-to-int p2, p2

    .line 4
    invoke-virtual {p3}, Landroid/view/WindowInsets;->getMandatorySystemGestureInsets()Landroid/graphics/Insets;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Insets;->bottom:I

    add-int/2addr v0, p2

    .line 5
    iget p2, p1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    return-object p3
.end method

.method synthetic lambda$setThumbnailBarMode$6$com-pspdfkit-ui-PdfThumbnailBar(Landroid/widget/FrameLayout$LayoutParams;Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/pspdfkit/R$dimen;->pspdf__floating_thumbnail_bar_margin_above_navigation:I

    .line 2
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    float-to-int p2, p2

    .line 4
    invoke-virtual {p3}, Landroidx/core/view/WindowInsetsCompat;->getSystemWindowInsetBottom()I

    move-result v0

    add-int/2addr v0, p2

    .line 5
    iget p2, p1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    return-object p3
.end method

.method synthetic lambda$updateViewState$0$com-pspdfkit-ui-PdfThumbnailBar(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->setDrawableProviders(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public removeDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V
    .locals 2

    const-string v0, "drawableProvider"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/fm;->b(Lcom/pspdfkit/ui/PageObjectProvider;)V

    return-void
.end method

.method public removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 55
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    :cond_0
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->setBackgroundColor(I)V

    :cond_0
    return-void
.end method

.method public setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 3

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_1

    .line 110
    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    :cond_1
    return-void
.end method

.method public final setOnPageChangedListener(Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->onPageChangedListener:Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 3
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->setOnPageChangedListener(Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;)V

    :cond_0
    return-void
.end method

.method public setRedactionAnnotationPreviewEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->setRedactionAnnotationPreviewEnabled(Z)V

    :cond_0
    return-void
.end method

.method public setSelectedThumbnailBorderColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->setSelectedThumbnailBorderColor(I)V

    :cond_0
    return-void
.end method

.method public setThumbnailBarMode(Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;)V
    .locals 4

    const-string v0, "thumbnailBarMode"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarMode:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    if-ne v0, p1, :cond_0

    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_1

    .line 57
    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 58
    iput-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    .line 61
    :cond_1
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarMode:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    .line 63
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x51

    .line 65
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 67
    sget-object v1, Lcom/pspdfkit/ui/PdfThumbnailBar$1;->$SwitchMap$com$pspdfkit$configuration$activity$ThumbnailBarMode:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_6

    const/4 v3, 0x2

    if-eq v1, v3, :cond_4

    const/4 v3, 0x3

    if-eq v1, v3, :cond_2

    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "PSPDFKit.ThumbnailBar"

    const-string v1, "Encountered unhandled thumbnail bar mode: %s"

    .line 162
    invoke-static {p1, v1, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 163
    :cond_2
    new-instance p1, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;-><init>(Landroid/content/Context;)V

    .line 164
    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 165
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    .line 167
    invoke-static {}, Lcom/pspdfkit/internal/v;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 168
    new-instance p1, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0, v0}, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/PdfThumbnailBar;Landroid/widget/FrameLayout$LayoutParams;)V

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    goto/16 :goto_0

    .line 178
    :cond_3
    new-instance v1, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, v0}, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/PdfThumbnailBar;Landroid/widget/FrameLayout$LayoutParams;)V

    invoke-static {p1, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    goto/16 :goto_0

    .line 179
    :cond_4
    new-instance p1, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;-><init>(Landroid/content/Context;)V

    .line 180
    sget-object v1, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;->PINNED:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->setLayoutStyle(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;)V

    .line 181
    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 182
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    .line 184
    invoke-static {}, Lcom/pspdfkit/internal/v;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 185
    new-instance v0, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda2;

    invoke-direct {v0, p1}, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)V

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    goto :goto_0

    .line 195
    :cond_5
    new-instance v0, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda3;

    invoke-direct {v0, p1}, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)V

    invoke-static {p1, v0}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    goto :goto_0

    .line 196
    :cond_6
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__floating_thumbnail_bar_margin_top:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    float-to-int p1, p1

    .line 198
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$dimen;->pspdf__floating_thumbnail_bar_margin_bottom:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 200
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__floating_thumbnail_bar_margin_horizontal:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 201
    invoke-virtual {v0, v2, p1, v2, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 203
    new-instance p1, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p1, v2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;-><init>(Landroid/content/Context;)V

    .line 204
    sget-object v2, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;->FLOATING:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    invoke-virtual {p1, v2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->setLayoutStyle(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;)V

    .line 205
    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 206
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    .line 208
    invoke-static {}, Lcom/pspdfkit/internal/v;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 209
    new-instance v2, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda4;

    invoke-direct {v2, p0, v0, v1, p1}, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/ui/PdfThumbnailBar;Landroid/widget/FrameLayout$LayoutParams;ILcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)V

    invoke-virtual {p0, v2}, Landroid/widget/FrameLayout;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    goto :goto_0

    .line 222
    :cond_7
    new-instance v2, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda5;

    invoke-direct {v2, p0, v0, v1, p1}, Lcom/pspdfkit/ui/PdfThumbnailBar$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/ui/PdfThumbnailBar;Landroid/widget/FrameLayout$LayoutParams;ILcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)V

    invoke-static {p1, v2}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    :goto_0
    return-void
.end method

.method public setThumbnailBorderColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->setThumbnailBorderColor(I)V

    :cond_0
    return-void
.end method

.method public setThumbnailHeight(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->setThumbnailHeight(I)V

    :cond_0
    return-void
.end method

.method public setThumbnailWidth(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->setThumbnailWidth(I)V

    :cond_0
    return-void
.end method

.method setUsePageAspectRatio(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar;->thumbnailBarController:Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;->setUsePageAspectRatio(Z)V

    :cond_0
    return-void
.end method

.method public show()V
    .locals 0

    return-void
.end method

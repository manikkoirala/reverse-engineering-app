.class Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/zf$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/PdfFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalDocumentListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/PdfFragment;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    return-void
.end method


# virtual methods
.method synthetic lambda$onPageRotationOffsetChanged$0$com-pspdfkit-ui-PdfFragment$InternalDocumentListener(ILcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fgetdocument(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/zf;

    move-result-object v1

    invoke-virtual {p2, v1, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/ui/PdfFragment;)V

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(IZ)V

    return-void
.end method

.method public onInternalDocumentSaveFailed(Lcom/pspdfkit/internal/zf;Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method public onInternalDocumentSaved(Lcom/pspdfkit/internal/zf;)V
    .locals 0

    return-void
.end method

.method public final onPageBindingChanged()V
    .locals 0

    return-void
.end method

.method public final onPageRotationOffsetChanged()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fgetdocument(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/zf;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fgetundoManager(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/vu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vu;->clearHistory()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {v1}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fgetviewCoordinator(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/xm;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;I)V

    const/4 v0, 0x0

    .line 6
    invoke-virtual {v1, v2, v0}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    :cond_0
    return-void
.end method

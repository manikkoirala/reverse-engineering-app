.class public interface abstract Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/PdfOutlineView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DocumentOutlineProvider"
.end annotation


# virtual methods
.method public abstract getOutlineElements()Lio/reactivex/rxjava3/core/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/OutlineElement;",
            ">;>;"
        }
    .end annotation
.end method

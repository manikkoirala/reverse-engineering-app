.class public abstract Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/actionmenu/ActionMenuItem$MenuItemType;
    }
.end annotation


# instance fields
.field private final icon:Landroid/graphics/drawable/Drawable;

.field private isEnabled:Z

.field private final itemId:I

.field private final itemType:Lcom/pspdfkit/ui/actionmenu/ActionMenuItem$MenuItemType;

.field private final label:Ljava/lang/String;


# direct methods
.method protected constructor <init>(ILcom/pspdfkit/ui/actionmenu/ActionMenuItem$MenuItemType;Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->isEnabled:Z

    const-string v0, "itemType"

    .line 17
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "icon"

    .line 18
    invoke-static {p3, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "label"

    .line 19
    invoke-static {p4, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iput p1, p0, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->itemId:I

    .line 21
    iput-object p2, p0, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->itemType:Lcom/pspdfkit/ui/actionmenu/ActionMenuItem$MenuItemType;

    .line 22
    iput-object p3, p0, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->icon:Landroid/graphics/drawable/Drawable;

    .line 23
    iput-object p4, p0, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->label:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->icon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getItemId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->itemId:I

    return v0
.end method

.method public getItemType()Lcom/pspdfkit/ui/actionmenu/ActionMenuItem$MenuItemType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->itemType:Lcom/pspdfkit/ui/actionmenu/ActionMenuItem$MenuItemType;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->label:Ljava/lang/String;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->isEnabled:Z

    return v0
.end method

.method public setEnabled(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->isEnabled:Z

    return-void
.end method

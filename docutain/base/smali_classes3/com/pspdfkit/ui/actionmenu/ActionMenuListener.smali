.class public interface abstract Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onActionMenuItemClicked(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z
.end method

.method public abstract onActionMenuItemLongClicked(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z
.end method

.method public abstract onDisplayActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V
.end method

.method public abstract onPrepareActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)Z
.end method

.method public abstract onRemoveActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V
.end method

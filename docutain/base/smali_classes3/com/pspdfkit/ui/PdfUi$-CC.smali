.class public final synthetic Lcom/pspdfkit/ui/PdfUi$-CC;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public static $default$addPropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
    .locals 2
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    const-string v0, "lifecycleListener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getPropertyInspectorCoordinatorLayout()Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    move-result-object v0

    .line 56
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->addPropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V

    return-void
.end method

.method public static $default$getDocument(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/document/PdfDocument;
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 4
    :cond_0
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    return-object v0
.end method

.method public static $default$getDocumentCoordinator(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/DocumentCoordinator;
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public static $default$getPSPDFKitViews(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/PSPDFKitViews;
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getViews()Lcom/pspdfkit/internal/yf;

    move-result-object v0

    return-object v0
.end method

.method public static $default$getPageIndex(Lcom/pspdfkit/ui/PdfUi;)I
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getPageIndex()I

    move-result v0

    return v0
.end method

.method public static $default$getPdfFragment(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getViews()Lcom/pspdfkit/internal/yf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->a()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    return-object v0
.end method

.method public static $default$getPropertyInspectorCoordinator(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getPropertyInspectorCoordinatorLayout()Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    move-result-object v0

    return-object v0
.end method

.method public static $default$getScreenTimeout(Lcom/pspdfkit/ui/PdfUi;)J
    .locals 2
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getScreenTimeout()J

    move-result-wide v0

    return-wide v0
.end method

.method public static $default$getSiblingPageIndex(Lcom/pspdfkit/ui/PdfUi;I)I
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->getSiblingPageIndex(I)I

    move-result p1

    return p1
.end method

.method public static $default$getUserInterfaceViewMode(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getUserInterfaceCoordinator()Lcom/pspdfkit/internal/ui/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->getUserInterfaceViewMode()Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    move-result-object v0

    return-object v0
.end method

.method public static $default$hideUserInterface(Lcom/pspdfkit/ui/PdfUi;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getUserInterfaceCoordinator()Lcom/pspdfkit/internal/ui/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->hideUserInterface()V

    return-void
.end method

.method public static $default$isDocumentInteractionEnabled(Lcom/pspdfkit/ui/PdfUi;)Z
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->isDocumentInteractionEnabled()Z

    move-result v0

    return v0
.end method

.method public static $default$isImageDocument(Lcom/pspdfkit/ui/PdfUi;)Z
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->isImageDocument()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static $default$isUserInterfaceEnabled(Lcom/pspdfkit/ui/PdfUi;)Z
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->isUserInterfaceEnabled()Z

    move-result v0

    return v0
.end method

.method public static $default$isUserInterfaceVisible(Lcom/pspdfkit/ui/PdfUi;)Z
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getUserInterfaceCoordinator()Lcom/pspdfkit/internal/ui/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->isUserInterfaceVisible()Z

    move-result v0

    return v0
.end method

.method public static $default$removePropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
    .locals 2
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    const-string v0, "lifecycleListener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getPropertyInspectorCoordinatorLayout()Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    move-result-object v0

    .line 56
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->removePropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V

    return-void
.end method

.method public static $default$requirePdfFragment(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 3
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "PdfFragment is not initialized yet!"

    .line 2
    invoke-static {v2, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static $default$setAnnotationCreationInspectorController(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;)V
    .locals 2
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    const-string v0, "annotationCreationInspectorController"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setAnnotationCreationInspectorController(Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;)V

    return-void
.end method

.method public static $default$setAnnotationEditingInspectorController(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;)V
    .locals 2
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    const-string v0, "annotationEditingInspectorController"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setAnnotationEditingInspectorController(Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;)V

    return-void
.end method

.method public static $default$setDocumentFromDataProvider(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)V
    .locals 3
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    const-string v0, "dataProvider"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v2, "setDocumentFromDataProvider() may only be called from the UI thread."

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 55
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;

    move-result-object v0

    invoke-static {p1, p2, v1}, Lcom/pspdfkit/ui/DocumentDescriptor;->fromDataProvider(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/ui/DocumentDescriptor;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/DocumentCoordinator;->setDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    return-void
.end method

.method public static $default$setDocumentFromDataProviders(Lcom/pspdfkit/ui/PdfUi;Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    const-string v0, "dataProviders"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v2, "setDocumentFromDataProvider() may only be called from the UI thread."

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 55
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;

    move-result-object v0

    invoke-static {p1, p2, v1}, Lcom/pspdfkit/ui/DocumentDescriptor;->fromDataProviders(Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/pspdfkit/ui/DocumentDescriptor;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/DocumentCoordinator;->setDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    return-void
.end method

.method public static $default$setDocumentFromUri(Lcom/pspdfkit/ui/PdfUi;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    const-string v0, "documentUri"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-static {p1}, Lcom/pspdfkit/internal/fv;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-static {p2}, Lcom/pspdfkit/internal/fv;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object p2

    invoke-interface {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi;->setDocumentFromUris(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static $default$setDocumentFromUris(Lcom/pspdfkit/ui/PdfUi;Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    const-string v0, "documentUris"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v2, "setDocumentFromUris() may only be called from the UI thread."

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 55
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;

    move-result-object v0

    invoke-static {p1, p2, v1}, Lcom/pspdfkit/ui/DocumentDescriptor;->fromUris(Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/pspdfkit/ui/DocumentDescriptor;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/DocumentCoordinator;->setDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    return-void
.end method

.method public static $default$setDocumentInteractionEnabled(Lcom/pspdfkit/ui/PdfUi;Z)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setDocumentInteractionEnabled(Z)V

    return-void
.end method

.method public static $default$setDocumentPrintDialogFactory(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setDocumentPrintDialogFactory(Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;)V

    return-void
.end method

.method public static $default$setDocumentSharingDialogFactory(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setDocumentSharingDialogFactory(Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;)V

    return-void
.end method

.method public static $default$setOnContextualToolbarLifecycleListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarLifecycleListener;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setOnContextualToolbarLifecycleListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarLifecycleListener;)V

    return-void
.end method

.method public static $default$setOnContextualToolbarMovementListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarMovementListener;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setOnContextualToolbarMovementListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarMovementListener;)V

    return-void
.end method

.method public static $default$setOnContextualToolbarPositionListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setOnContextualToolbarPositionListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;)V

    return-void
.end method

.method public static $default$setPageIndex(Lcom/pspdfkit/ui/PdfUi;I)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setPageIndex(I)V

    return-void
.end method

.method public static $default$setPageIndex(Lcom/pspdfkit/ui/PdfUi;IZ)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 2
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/ui/f;->setPageIndex(IZ)V

    return-void
.end method

.method public static $default$setPrintOptionsProvider(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setPrintOptionsProvider(Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V

    return-void
.end method

.method public static $default$setScreenTimeout(Lcom/pspdfkit/ui/PdfUi;J)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/ui/f;->setScreenTimeout(J)V

    return-void
.end method

.method public static $default$setSharingActionMenuListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setSharingActionMenuListener(Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;)V

    return-void
.end method

.method public static $default$setSharingOptionsProvider(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/document/sharing/SharingOptionsProvider;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setSharingOptionsProvider(Lcom/pspdfkit/document/sharing/SharingOptionsProvider;)V

    return-void
.end method

.method public static $default$setUserInterfaceEnabled(Lcom/pspdfkit/ui/PdfUi;Z)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setUserInterfaceEnabled(Z)V

    return-void
.end method

.method public static $default$setUserInterfaceViewMode(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getUserInterfaceCoordinator()Lcom/pspdfkit/internal/ui/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/c;->setUserInterfaceViewMode(Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;)V

    return-void
.end method

.method public static $default$setUserInterfaceVisible(Lcom/pspdfkit/ui/PdfUi;ZZ)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getUserInterfaceCoordinator()Lcom/pspdfkit/internal/ui/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/ui/c;->setUserInterfaceVisible(ZZ)V

    return-void
.end method

.method public static $default$showUserInterface(Lcom/pspdfkit/ui/PdfUi;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getUserInterfaceCoordinator()Lcom/pspdfkit/internal/ui/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->showUserInterface()V

    return-void
.end method

.method public static $default$toggleUserInterface(Lcom/pspdfkit/ui/PdfUi;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/PdfUi;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getUserInterfaceCoordinator()Lcom/pspdfkit/internal/ui/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->toggleUserInterface()V

    return-void
.end method

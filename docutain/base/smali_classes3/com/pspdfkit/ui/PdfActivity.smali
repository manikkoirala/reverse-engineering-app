.class public Lcom/pspdfkit/ui/PdfActivity;
.super Landroidx/appcompat/app/AppCompatActivity;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/PdfUi;
.implements Lcom/pspdfkit/listeners/PdfActivityListener;
.implements Lcom/pspdfkit/ui/PdfActivityComponentsApi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;
    }
.end annotation


# static fields
.field public static final MENU_OPTION_DOCUMENT_INFO:I

.field public static final MENU_OPTION_EDIT_ANNOTATIONS:I

.field public static final MENU_OPTION_EDIT_CONTENT:I

.field public static final MENU_OPTION_OUTLINE:I

.field public static final MENU_OPTION_READER_VIEW:I

.field public static final MENU_OPTION_SEARCH:I

.field public static final MENU_OPTION_SETTINGS:I

.field public static final MENU_OPTION_SHARE:I

.field public static final MENU_OPTION_SIGNATURE:I

.field public static final MENU_OPTION_THUMBNAIL_GRID:I

.field private static final PARAM_HIERARCHY_STATE_STATE:Ljava/lang/String; = "PdfActivity.HierarchyState"

.field private static final STATE_FRAGMENT:Ljava/lang/String; = "PdfActivity.ConfigurationChanged.FragmentState"


# instance fields
.field private configurationToApply:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

.field implementation:Lcom/pspdfkit/internal/ui/f;

.field protected final internalPdfUi:Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;


# direct methods
.method static bridge synthetic -$$Nest$mapplyConfiguration(Lcom/pspdfkit/ui/PdfActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfActivity;->applyConfiguration()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_option_thumbnail_grid:I

    sput v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_THUMBNAIL_GRID:I

    .line 5
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_option_search:I

    sput v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SEARCH:I

    .line 9
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_option_outline:I

    sput v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_OUTLINE:I

    .line 13
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_option_edit_annotations:I

    sput v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_ANNOTATIONS:I

    .line 17
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_option_edit_content:I

    sput v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_CONTENT:I

    .line 21
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_option_signature:I

    sput v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SIGNATURE:I

    .line 25
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_option_share:I

    sput v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SHARE:I

    .line 29
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_option_settings:I

    sput v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SETTINGS:I

    .line 33
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_option_reader_view:I

    sput v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_READER_VIEW:I

    .line 37
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_option_info_view:I

    sput v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_DOCUMENT_INFO:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatActivity;-><init>()V

    .line 120
    new-instance v0, Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;-><init>(Lcom/pspdfkit/ui/PdfActivity;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->internalPdfUi:Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;

    return-void
.end method

.method private applyConfiguration()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "PSPDF.InternalExtras"

    .line 2
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 3
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 5
    invoke-virtual {v2, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 9
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 10
    iget-object v3, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v4, v4}, Lcom/pspdfkit/internal/ui/f;->onSaveInstanceState(Landroid/os/Bundle;ZZ)V

    const-string v3, "activityState"

    .line 12
    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 16
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->saveHierarchyState()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "PdfActivity.HierarchyState"

    .line 17
    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/ui/f;->retainedDocument:Lcom/pspdfkit/document/PdfDocument;

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/fragment/app/FragmentTransaction;->remove(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    .line 30
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    .line 31
    invoke-virtual {p0, v0, v1}, Landroidx/appcompat/app/AppCompatActivity;->overridePendingTransition(II)V

    .line 32
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 33
    invoke-virtual {p0, v0, v1}, Landroidx/appcompat/app/AppCompatActivity;->overridePendingTransition(II)V

    return-void

    .line 35
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "PdfActivity was not initialized with proper arguments."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private requirePdfParameters()Landroid/os/Bundle;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->internalPdfUi:Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;->getPdfParameters()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 3
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PdfActivity was not initialized with proper arguments!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static showDocument(Landroid/content/Context;Landroid/net/Uri;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-static {p0, p1, v0, p2}, Lcom/pspdfkit/ui/PdfActivity;->showDocument(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    return-void
.end method

.method public static showDocument(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 3

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 54
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "documentUri"

    .line 56
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/net/Uri;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 108
    invoke-static {p0, v1}, Lcom/pspdfkit/ui/PdfActivityIntentBuilder;->fromUri(Landroid/content/Context;[Landroid/net/Uri;)Lcom/pspdfkit/ui/PdfActivityIntentBuilder;

    move-result-object p1

    new-array v0, v0, [Ljava/lang/String;

    aput-object p2, v0, v2

    .line 109
    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfActivityIntentBuilder;->passwords([Ljava/lang/String;)Lcom/pspdfkit/ui/PdfActivityIntentBuilder;

    move-result-object p1

    .line 110
    invoke-virtual {p1, p3}, Lcom/pspdfkit/ui/PdfActivityIntentBuilder;->configuration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Lcom/pspdfkit/ui/PdfActivityIntentBuilder;

    move-result-object p1

    .line 111
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfActivityIntentBuilder;->build()Landroid/content/Intent;

    move-result-object p1

    .line 113
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static showImage(Landroid/content/Context;Landroid/net/Uri;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 3

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "imageUri"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfActivityIntentBuilder;->fromImageUri(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/ui/PdfActivityIntentBuilder;

    move-result-object p1

    .line 108
    invoke-virtual {p1, p2}, Lcom/pspdfkit/ui/PdfActivityIntentBuilder;->configuration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Lcom/pspdfkit/ui/PdfActivityIntentBuilder;

    move-result-object p1

    .line 109
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfActivityIntentBuilder;->build()Landroid/content/Intent;

    move-result-object p1

    .line 110
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public synthetic addPropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$addPropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V

    return-void
.end method

.method protected createImplementation()Lcom/pspdfkit/internal/ui/f;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ui/f;

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfActivity;->internalPdfUi:Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;

    invoke-direct {v0, p0, p0, v1}, Lcom/pspdfkit/internal/ui/f;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/internal/bg;)V

    return-object v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v0

    return-object v0

    .line 7
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfActivity;->requirePdfParameters()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PSPDF.Configuration"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    const-string v2, "PdfActivity requires a configuration extra!"

    .line 8
    invoke-static {v2, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public synthetic getDocument()Lcom/pspdfkit/document/PdfDocument;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getDocument(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getDocumentCoordinator(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/DocumentCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public getImplementation()Lcom/pspdfkit/internal/ui/f;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    return-object v0
.end method

.method public synthetic getPSPDFKitViews()Lcom/pspdfkit/ui/PSPDFKitViews;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getPSPDFKitViews(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/PSPDFKitViews;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getPageIndex()I
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getPageIndex(Lcom/pspdfkit/ui/PdfUi;)I

    move-result v0

    return v0
.end method

.method public synthetic getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getPdfFragment(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getPropertyInspectorCoordinator()Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getPropertyInspectorCoordinator(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getScreenTimeout()J
    .locals 2

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getScreenTimeout(Lcom/pspdfkit/ui/PdfUi;)J

    move-result-wide v0

    return-wide v0
.end method

.method public synthetic getSiblingPageIndex(I)I
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getSiblingPageIndex(Lcom/pspdfkit/ui/PdfUi;I)I

    move-result p1

    return p1
.end method

.method public synthetic getUserInterfaceViewMode()Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getUserInterfaceViewMode(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    move-result-object v0

    return-object v0
.end method

.method public synthetic hideUserInterface()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$hideUserInterface(Lcom/pspdfkit/ui/PdfUi;)V

    return-void
.end method

.method public synthetic isDocumentInteractionEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$isDocumentInteractionEnabled(Lcom/pspdfkit/ui/PdfUi;)Z

    move-result v0

    return v0
.end method

.method public synthetic isImageDocument()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$isImageDocument(Lcom/pspdfkit/ui/PdfUi;)Z

    move-result v0

    return v0
.end method

.method public synthetic isUserInterfaceEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$isUserInterfaceEnabled(Lcom/pspdfkit/ui/PdfUi;)Z

    move-result v0

    return v0
.end method

.method public synthetic isUserInterfaceVisible()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$isUserInterfaceVisible(Lcom/pspdfkit/ui/PdfUi;)Z

    move-result v0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/f;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/16 v0, 0x6c

    .line 1
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->supportRequestWindowFeature(I)Z

    const/16 v0, 0x6d

    .line 2
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->supportRequestWindowFeature(I)Z

    const/16 v0, 0xa

    .line 3
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->supportRequestWindowFeature(I)Z

    .line 5
    invoke-static {p0}, Lcom/pspdfkit/internal/pg;->a(Landroid/content/Context;)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x30

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    const-wide/16 v2, 0x10

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const-string v0, "adjustResize"

    goto :goto_0

    :cond_0
    const-string v0, "adjustPan"

    .line 10
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Soft input mode in PdfActivity window is set to `"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "`. Using soft input mode other than `adjustNothing` could lead to unpredictable behavior!"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.PdfActivity"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    :cond_1
    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfActivity;->createImplementation()Lcom/pspdfkit/internal/ui/f;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->configurationToApply:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    if-eqz v0, :cond_2

    .line 25
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfActivity;->requirePdfParameters()Landroid/os/Bundle;

    move-result-object v1

    .line 26
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/internal/ui/f;->applyConfigurationToParamsAndState(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Landroid/os/Bundle;Landroid/os/Bundle;)V

    const/4 v0, 0x0

    .line 28
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->configurationToApply:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 30
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method protected onDestroy()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onDestroy()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onDestroy()V

    return-void
.end method

.method public onDocumentClick()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 0

    const/4 p1, 0x0

    .line 1
    invoke-virtual {p0, p1}, Landroid/app/Activity;->setResult(I)V

    return-void
.end method

.method public onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    const/4 p1, -0x1

    .line 1
    invoke-virtual {p0, p1}, Landroid/app/Activity;->setResult(I)V

    return-void
.end method

.method public onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method public onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V
    .locals 0

    return-void
.end method

.method public onGenerateMenuItemIds(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    return-object p1
.end method

.method public onGetShowAsAction(II)I
    .locals 0

    return p2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    return-void
.end method

.method public onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onPause()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onPause()V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 4
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PSPDF.InternalExtras"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    const-string p1, "PdfActivity.HierarchyState"

    .line 5
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    .line 7
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->restoreHierarchyState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method protected onResume()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onResume()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onResume()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/activity/ComponentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 5
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getState()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PdfActivity.ConfigurationChanged.FragmentState"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public onSetActivityTitle(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/ui/f;->onSetActivityTitle(Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStart()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStop()V

    .line 3
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->executePendingTransactions()Z

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onStop()V

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/activity/ComponentActivity;->onTrimMemory(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->onTrimMemory(I)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onUserInteraction()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onUserInteraction()V

    return-void
.end method

.method public onUserInterfaceVisibilityChanged(Z)V
    .locals 0

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->onWindowFocusChanged(Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->onWindowFocusChanged(Z)V

    return-void
.end method

.method public synthetic removePropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$removePropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V

    return-void
.end method

.method public synthetic requirePdfFragment()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$requirePdfFragment(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setAnnotationCreationInspectorController(Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setAnnotationCreationInspectorController(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;)V

    return-void
.end method

.method public synthetic setAnnotationEditingInspectorController(Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setAnnotationEditingInspectorController(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;)V

    return-void
.end method

.method public setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 2

    const-string v0, "configuration"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity;->implementation:Lcom/pspdfkit/internal/ui/f;

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    goto :goto_0

    .line 58
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfActivity;->configurationToApply:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    :goto_0
    return-void
.end method

.method public synthetic setDocumentFromDataProvider(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentFromDataProvider(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic setDocumentFromDataProviders(Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentFromDataProviders(Lcom/pspdfkit/ui/PdfUi;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public synthetic setDocumentFromUri(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentFromUri(Lcom/pspdfkit/ui/PdfUi;Landroid/net/Uri;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic setDocumentFromUris(Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentFromUris(Lcom/pspdfkit/ui/PdfUi;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public synthetic setDocumentInteractionEnabled(Z)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentInteractionEnabled(Lcom/pspdfkit/ui/PdfUi;Z)V

    return-void
.end method

.method public synthetic setDocumentPrintDialogFactory(Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentPrintDialogFactory(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;)V

    return-void
.end method

.method public synthetic setDocumentSharingDialogFactory(Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentSharingDialogFactory(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;)V

    return-void
.end method

.method public synthetic setOnContextualToolbarLifecycleListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarLifecycleListener;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setOnContextualToolbarLifecycleListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarLifecycleListener;)V

    return-void
.end method

.method public synthetic setOnContextualToolbarMovementListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarMovementListener;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setOnContextualToolbarMovementListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarMovementListener;)V

    return-void
.end method

.method public synthetic setOnContextualToolbarPositionListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setOnContextualToolbarPositionListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;)V

    return-void
.end method

.method public synthetic setPageIndex(I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setPageIndex(Lcom/pspdfkit/ui/PdfUi;I)V

    return-void
.end method

.method public synthetic setPageIndex(IZ)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setPageIndex(Lcom/pspdfkit/ui/PdfUi;IZ)V

    return-void
.end method

.method public synthetic setPrintOptionsProvider(Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setPrintOptionsProvider(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V

    return-void
.end method

.method public synthetic setScreenTimeout(J)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setScreenTimeout(Lcom/pspdfkit/ui/PdfUi;J)V

    return-void
.end method

.method public synthetic setSharingActionMenuListener(Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setSharingActionMenuListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;)V

    return-void
.end method

.method public synthetic setSharingOptionsProvider(Lcom/pspdfkit/document/sharing/SharingOptionsProvider;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setSharingOptionsProvider(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/document/sharing/SharingOptionsProvider;)V

    return-void
.end method

.method public synthetic setUserInterfaceEnabled(Z)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setUserInterfaceEnabled(Lcom/pspdfkit/ui/PdfUi;Z)V

    return-void
.end method

.method public synthetic setUserInterfaceViewMode(Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setUserInterfaceViewMode(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;)V

    return-void
.end method

.method public synthetic setUserInterfaceVisible(ZZ)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setUserInterfaceVisible(Lcom/pspdfkit/ui/PdfUi;ZZ)V

    return-void
.end method

.method public synthetic showUserInterface()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$showUserInterface(Lcom/pspdfkit/ui/PdfUi;)V

    return-void
.end method

.method public synthetic toggleUserInterface()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$toggleUserInterface(Lcom/pspdfkit/ui/PdfUi;)V

    return-void
.end method

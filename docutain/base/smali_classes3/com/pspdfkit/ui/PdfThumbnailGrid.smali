.class public Lcom/pspdfkit/ui/PdfThumbnailGrid;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;
.implements Lcom/pspdfkit/listeners/DocumentListener;
.implements Lcom/pspdfkit/ui/drawable/PdfDrawableManager;
.implements Lcom/pspdfkit/internal/gl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;,
        Lcom/pspdfkit/ui/PdfThumbnailGrid$OnDocumentSavedListener;,
        Lcom/pspdfkit/ui/PdfThumbnailGrid$OnPageClickListener;
    }
.end annotation


# static fields
.field private static final RETAINED_STATE_FRAGMENT_TAG:Ljava/lang/String; = "com.pspdfkit.ui.PSPDFThumbnailGrid.RETAINED_STATE_FRAGMENT"

.field private static final SHOW_ANIMATION_DURATION_MS:J = 0x64L


# instance fields
.field private backgroundColor:I

.field private configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private document:Lcom/pspdfkit/document/PdfDocument;

.field final documentEditModeActive:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private documentEditor:Lcom/pspdfkit/internal/k8;

.field private documentEditorEnabled:Z

.field private documentEditorSavingToolbarHandler:Lcom/pspdfkit/internal/m8;

.field private final drawableProviderCollection:Lcom/pspdfkit/internal/fm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/fm<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;"
        }
    .end annotation
.end field

.field private exportEnabled:Ljava/lang/Boolean;

.field private fabAddIcon:Landroid/graphics/drawable/Drawable;

.field private fabEditIcon:Landroid/graphics/drawable/Drawable;

.field private fabIconColor:I

.field private filePicker:Lcom/pspdfkit/document/editor/FilePicker;

.field private floatingActionButton:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private highlightedPageIndex:I

.field private isDisplayed:Z

.field private isRedactionAnnotationPreviewEnabled:Z

.field private itemLabelBackgroundDrawableRes:I

.field private itemLabelTextStyle:I

.field private newPageFactory:Lcom/pspdfkit/document/editor/page/NewPageFactory;

.field private final onDocumentSavedListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/PdfThumbnailGrid$OnDocumentSavedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final onPageClickListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/PdfThumbnailGrid$OnPageClickListener;",
            ">;"
        }
    .end annotation
.end field

.field private recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

.field private retainedDocumentEditorHolder:Lcom/pspdfkit/internal/xp;

.field private retainedNativeDocumentEditor:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

.field private saveAsEnabled:Ljava/lang/Boolean;

.field private showPageLabels:Z

.field private final thumbnailGridVisibilityListeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetdocumentEditor(Lcom/pspdfkit/ui/PdfThumbnailGrid;)Lcom/pspdfkit/internal/k8;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetdocumentEditorEnabled(Lcom/pspdfkit/ui/PdfThumbnailGrid;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetonPageClickListeners(Lcom/pspdfkit/ui/PdfThumbnailGrid;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onPageClickListeners:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetrecyclerView(Lcom/pspdfkit/ui/PdfThumbnailGrid;)Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$manimateHideFab(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->animateHideFab()V

    return-void
.end method

.method static bridge synthetic -$$Nest$manimateShowFab(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->animateShowFab()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->saveAsEnabled:Ljava/lang/Boolean;

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->exportEnabled:Ljava/lang/Boolean;

    .line 8
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditModeActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 12
    new-instance p1, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-direct {p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->thumbnailGridVisibilityListeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    .line 16
    new-instance p1, Lcom/pspdfkit/internal/fm;

    invoke-direct {p1}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    .line 20
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 21
    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onPageClickListeners:Ljava/util/List;

    .line 22
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 23
    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onDocumentSavedListeners:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 25
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->saveAsEnabled:Ljava/lang/Boolean;

    .line 28
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->exportEnabled:Ljava/lang/Boolean;

    .line 31
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditModeActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 35
    new-instance p1, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-direct {p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->thumbnailGridVisibilityListeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    .line 39
    new-instance p1, Lcom/pspdfkit/internal/fm;

    invoke-direct {p1}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    .line 43
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 44
    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onPageClickListeners:Ljava/util/List;

    .line 45
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 46
    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onDocumentSavedListeners:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    .line 48
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->saveAsEnabled:Ljava/lang/Boolean;

    .line 51
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->exportEnabled:Ljava/lang/Boolean;

    .line 54
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditModeActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 58
    new-instance p1, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-direct {p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->thumbnailGridVisibilityListeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    .line 62
    new-instance p1, Lcom/pspdfkit/internal/fm;

    invoke-direct {p1}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    .line 66
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 67
    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onPageClickListeners:Ljava/util/List;

    .line 68
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 69
    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onDocumentSavedListeners:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .line 70
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p1, 0x0

    .line 71
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->saveAsEnabled:Ljava/lang/Boolean;

    .line 74
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->exportEnabled:Ljava/lang/Boolean;

    .line 77
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditModeActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 81
    new-instance p1, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-direct {p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->thumbnailGridVisibilityListeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    .line 85
    new-instance p1, Lcom/pspdfkit/internal/fm;

    invoke-direct {p1}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    .line 89
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 90
    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onPageClickListeners:Ljava/util/List;

    .line 91
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 92
    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onDocumentSavedListeners:Ljava/util/List;

    return-void
.end method

.method private animateHideFab()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->floatingActionButton:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->floatingActionButton:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 4
    invoke-virtual {v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->floatingActionButton:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 5
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v2, v0

    int-to-float v0, v2

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AnticipateInterpolator;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-direct {v1, v2}, Landroid/view/animation/AnticipateInterpolator;-><init>(F)V

    .line 6
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 7
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method private animateShowFab()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->floatingActionButton:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 2
    invoke-virtual {v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 3
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    .line 4
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method private applyTheme()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    if-eqz v0, :cond_0

    .line 2
    iget v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->backgroundColor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    iget v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->itemLabelTextStyle:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->setItemLabelTextStyle(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    iget v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->itemLabelBackgroundDrawableRes:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->setItemLabelBackground(I)V

    :cond_0
    return-void
.end method

.method private createDefaultNewPageFactory()Lcom/pspdfkit/document/editor/page/NewPageFactory;
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;)Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    if-eqz v1, :cond_1

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/k8;->getRotatedPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 4
    :goto_0
    new-instance v2, Lcom/pspdfkit/document/editor/page/DialogNewPageFactory;

    invoke-direct {v2, v0, v1}, Lcom/pspdfkit/document/editor/page/DialogNewPageFactory;-><init>(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/utils/Size;)V

    iput-object v2, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->newPageFactory:Lcom/pspdfkit/document/editor/page/NewPageFactory;

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDefaultNewPageDialogCallback()Lcom/pspdfkit/document/editor/page/NewPageDialog$Callback;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pspdfkit/document/editor/page/NewPageDialog;->restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/document/editor/page/NewPageDialog$Callback;)Z

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->newPageFactory:Lcom/pspdfkit/document/editor/page/NewPageFactory;

    return-object v0

    .line 8
    :cond_1
    new-instance v0, Lcom/pspdfkit/document/editor/page/ValueNewPageFactory;

    sget-object v1, Lcom/pspdfkit/document/processor/NewPage;->PAGE_SIZE_A4:Lcom/pspdfkit/utils/Size;

    .line 9
    invoke-static {v1}, Lcom/pspdfkit/document/processor/NewPage;->emptyPage(Lcom/pspdfkit/utils/Size;)Lcom/pspdfkit/document/processor/NewPage$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/document/processor/NewPage$Builder;->build()Lcom/pspdfkit/document/processor/NewPage;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/document/editor/page/ValueNewPageFactory;-><init>(Lcom/pspdfkit/document/processor/NewPage;)V

    return-object v0
.end method

.method private ensureNewPageFactory()Lcom/pspdfkit/document/editor/page/NewPageFactory;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->newPageFactory:Lcom/pspdfkit/document/editor/page/NewPageFactory;

    if-nez v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->createDefaultNewPageFactory()Lcom/pspdfkit/document/editor/page/NewPageFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->newPageFactory:Lcom/pspdfkit/document/editor/page/NewPageFactory;

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->newPageFactory:Lcom/pspdfkit/document/editor/page/NewPageFactory;

    return-object v0
.end method

.method private invalidateFab()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->floatingActionButton:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-eqz v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditModeActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->fabAddIcon:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->fabEditIcon:Landroid/graphics/drawable/Drawable;

    .line 3
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->floatingActionButton:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    return-void
.end method

.method private prepareForDisplay()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    if-eqz v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/R$styleable;->pspdf__ThumbnailGrid:[I

    sget v2, Lcom/pspdfkit/R$attr;->pspdf__thumbnailGridStyle:I

    sget v3, Lcom/pspdfkit/R$style;->PSPDFKit_ThumbnailGrid:I

    const/4 v4, 0x0

    .line 5
    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 10
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ThumbnailGrid_pspdf__backgroundColor:I

    .line 12
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_gray_light:I

    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 13
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->backgroundColor:I

    .line 16
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ThumbnailGrid_pspdf__itemLabelTextStyle:I

    sget v2, Lcom/pspdfkit/R$style;->PSPDFKit_ThumbnailGridItemLabelDefStyle:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->itemLabelTextStyle:I

    .line 19
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ThumbnailGrid_pspdf__itemLabelBackground:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__grid_list_label_background:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->itemLabelBackgroundDrawableRes:I

    .line 23
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ThumbnailGrid_pspdf_fabIconColor:I

    .line 25
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 26
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->fabIconColor:I

    .line 29
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 32
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__thumbnail_grid_view:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 33
    sget v0, Lcom/pspdfkit/R$id;->pspdf__thumbnail_grid_recycler_view:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    .line 34
    new-instance v1, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;

    invoke-direct {v1, p0, v4}, Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener;-><init>(Lcom/pspdfkit/ui/PdfThumbnailGrid;Lcom/pspdfkit/ui/PdfThumbnailGrid$RecyclerViewListener-IA;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->setThumbnailGridListener(Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;)V

    .line 36
    sget v0, Lcom/pspdfkit/R$id;->pspdf__fab:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->floatingActionButton:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 37
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_edit:I

    iget v2, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->fabIconColor:I

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->fabEditIcon:Landroid/graphics/drawable/Drawable;

    .line 38
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_add:I

    iget v2, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->fabIconColor:I

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->fabAddIcon:Landroid/graphics/drawable/Drawable;

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->floatingActionButton:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    new-instance v1, Lcom/pspdfkit/ui/PdfThumbnailGrid$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->applyTheme()V

    .line 50
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->subscribeForCustomDrawableUpdates()V

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->document:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    if-eqz v1, :cond_1

    .line 54
    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    iget v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->highlightedPageIndex:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->setHighlightedItem(I)V

    .line 57
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    iget-boolean v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->isRedactionAnnotationPreviewEnabled:Z

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->setRedactionAnnotationPreviewEnabled(Z)V

    return-void
.end method

.method private restoreDocumentEditingMode(Lcom/pspdfkit/internal/jni/NativeDocumentEditor;)V
    .locals 2

    const-string v0, "retainedNativeDocumentEditor"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorEnabled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditModeActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->invalidateFab()V

    .line 58
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/m8;->a(Lcom/pspdfkit/internal/jni/NativeDocumentEditor;)V

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/m8;->c()V

    .line 62
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->ensureNewPageFactory()Lcom/pspdfkit/document/editor/page/NewPageFactory;

    :cond_1
    return-void
.end method

.method private subscribeForCustomDrawableUpdates()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fm;->b()Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 4
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->updateViewState()Lio/reactivex/rxjava3/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private updateViewState()Lio/reactivex/rxjava3/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/functions/Consumer<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/PdfThumbnailGrid$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V

    return-object v0
.end method


# virtual methods
.method public addDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V
    .locals 2

    const-string v0, "drawableProvider"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/fm;->a(Lcom/pspdfkit/ui/PageObjectProvider;)V

    return-void
.end method

.method public addOnDocumentSavedListener(Lcom/pspdfkit/ui/PdfThumbnailGrid$OnDocumentSavedListener;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onDocumentSavedListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addOnPageClickListener(Lcom/pspdfkit/ui/PdfThumbnailGrid$OnPageClickListener;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onPageClickListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->thumbnailGridVisibilityListeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    return-void
.end method

.method public clearDocument()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->hide()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->document:Lcom/pspdfkit/document/PdfDocument;

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a()V

    :cond_0
    return-void
.end method

.method public enterDocumentEditingMode()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditModeActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->invalidateFab()V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/m8;->a(Lcom/pspdfkit/internal/jni/NativeDocumentEditor;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    iget v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->highlightedPageIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/k8;->a(Ljava/lang/Integer;)V

    :cond_0
    return-void
.end method

.method public exitDocumentEditingMode()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditModeActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->invalidateFab()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d()V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m8;->a()V

    :cond_0
    return-void
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 3

    .line 1
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0, v1, v2, p1}, Landroid/view/View;->setPadding(IIII)V

    const/4 p1, 0x0

    return p1
.end method

.method public focusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 7

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 6
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;

    if-nez v0, :cond_1

    .line 8
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 12
    :cond_1
    instance-of v1, p1, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    if-ne p2, v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 16
    :goto_0
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getPosition()Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;->LEFT:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    if-ne v4, v5, :cond_3

    const/16 v4, 0x11

    if-ne p2, v4, :cond_3

    const/4 v4, 0x1

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    .line 19
    :goto_1
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getPosition()Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    move-result-object v5

    sget-object v6, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;->RIGHT:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    if-ne v5, v6, :cond_4

    const/16 v5, 0x42

    if-ne p2, v5, :cond_4

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    if-nez v1, :cond_6

    if-nez v4, :cond_6

    if-eqz v2, :cond_5

    goto :goto_3

    .line 26
    :cond_5
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    return-object p1

    :cond_6
    :goto_3
    return-object v0
.end method

.method public getBackgroundColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->backgroundColor:I

    return v0
.end method

.method public getDefaultNewPageDialogCallback()Lcom/pspdfkit/document/editor/page/NewPageDialog$Callback;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/PdfThumbnailGrid$1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid$1;-><init>(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V

    return-object v0
.end method

.method public getDocumentEditor()Lcom/pspdfkit/document/editor/PdfDocumentEditor;
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    return-object v0

    .line 3
    :cond_0
    new-instance v0, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v1, "Your current license does not allow editing of PDF documents."

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;
    .locals 3

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorSavingToolbarHandler:Lcom/pspdfkit/internal/m8;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    if-eqz v1, :cond_1

    .line 3
    new-instance v2, Lcom/pspdfkit/internal/m8;

    invoke-direct {v2, p0, v0, p0, v1}, Lcom/pspdfkit/internal/m8;-><init>(Lcom/pspdfkit/internal/gl;Lcom/pspdfkit/internal/k8;Lcom/pspdfkit/ui/PdfThumbnailGrid;Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;)V

    iput-object v2, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorSavingToolbarHandler:Lcom/pspdfkit/internal/m8;

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->saveAsEnabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 9
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/m8;->b(Z)V

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->exportEnabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorSavingToolbarHandler:Lcom/pspdfkit/internal/m8;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/m8;->a(Z)V

    .line 16
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorSavingToolbarHandler:Lcom/pspdfkit/internal/m8;

    return-object v0

    .line 17
    :cond_2
    new-instance v0, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v1, "Your current license does not allow editing of PDF documents."

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFilePicker()Lcom/pspdfkit/document/editor/FilePicker;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->filePicker:Lcom/pspdfkit/document/editor/FilePicker;

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/x7;

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/app/AppCompatActivity;

    .line 5
    invoke-static {}, Lcom/pspdfkit/internal/t$a;->a()Lcom/pspdfkit/internal/t;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/x7;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/internal/t;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->filePicker:Lcom/pspdfkit/document/editor/FilePicker;

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->filePicker:Lcom/pspdfkit/document/editor/FilePicker;

    return-object v0
.end method

.method public getItemLabelBackground()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->itemLabelBackgroundDrawableRes:I

    return v0
.end method

.method public getItemLabelTextStyle()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->itemLabelTextStyle:I

    return v0
.end method

.method public getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_THUMBNAIL_GRID:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    return-object v0
.end method

.method public getSelectedPages()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorSavingToolbarHandler:Lcom/pspdfkit/internal/m8;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m8;->getSelectedPages()Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    .line 3
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    :goto_0
    return-object v0
.end method

.method public hide()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->isDisplayed:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->isDisplayed:Z

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->thumbnailGridVisibilityListeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->onHide(Landroid/view/View;)V

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->exitDocumentEditingMode()V

    .line 6
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/PdfThumbnailGrid$2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid$2;-><init>(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method public isDisplayed()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->isDisplayed:Z

    return v0
.end method

.method public isDocumentEditorEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorEnabled:Z

    return v0
.end method

.method public isRedactionAnnotationPreviewEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->isRedactionAnnotationPreviewEnabled:Z

    return v0
.end method

.method public isShowPageLabels()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->showPageLabels:Z

    return v0
.end method

.method synthetic lambda$prepareForDisplay$0$com-pspdfkit-ui-PdfThumbnailGrid(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditModeActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-nez p1, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->enterDocumentEditingMode()V

    goto :goto_0

    .line 4
    :cond_0
    iget-boolean p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorEnabled:Z

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->ensureNewPageFactory()Lcom/pspdfkit/document/editor/page/NewPageFactory;

    move-result-object p1

    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/pspdfkit/document/editor/page/NewPageFactory;->onCreateNewPage(Lcom/pspdfkit/document/editor/page/NewPageFactory$OnNewPageReadyListener;)V

    :cond_1
    :goto_0
    return-void
.end method

.method synthetic lambda$updateViewState$1$com-pspdfkit-ui-PdfThumbnailGrid(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->setDrawableProviders(Ljava/util/List;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->b()V

    :cond_0
    return-void
.end method

.method public onDocumentClick()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDocumentExported(Landroid/net/Uri;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->exitDocumentEditingMode()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onDocumentSavedListeners:Ljava/util/List;

    monitor-enter v0

    .line 3
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onDocumentSavedListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onDocumentSavedListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/PdfThumbnailGrid$OnDocumentSavedListener;

    .line 5
    invoke-interface {v2, p1}, Lcom/pspdfkit/ui/PdfThumbnailGrid$OnDocumentSavedListener;->onDocumentExported(Landroid/net/Uri;)V

    goto :goto_0

    .line 8
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method public onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorEnabled:Z

    if-eqz p1, :cond_1

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->retainedNativeDocumentEditor:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    if-eqz p1, :cond_0

    .line 5
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->restoreDocumentEditingMode(Lcom/pspdfkit/internal/jni/NativeDocumentEditor;)V

    const/4 p1, 0x0

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->retainedNativeDocumentEditor:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;)Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;)Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/document/editor/page/NewPageDialog;->hide(Landroidx/fragment/app/FragmentManager;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method public onDocumentSaved()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->exitDocumentEditingMode()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onDocumentSavedListeners:Ljava/util/List;

    monitor-enter v0

    .line 3
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onDocumentSavedListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onDocumentSavedListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/PdfThumbnailGrid$OnDocumentSavedListener;

    .line 5
    invoke-interface {v2}, Lcom/pspdfkit/ui/PdfThumbnailGrid$OnDocumentSavedListener;->onDocumentSaved()V

    goto :goto_0

    .line 8
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V
    .locals 0

    return-void
.end method

.method public onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    .line 1
    iput p2, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->highlightedPageIndex:I

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->setHighlightedItem(I)V

    :cond_0
    return-void
.end method

.method public onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->retainedDocumentEditorHolder:Lcom/pspdfkit/internal/xp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditModeActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->retainedDocumentEditorHolder:Lcom/pspdfkit/internal/xp;

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/xp;->a(Ljava/lang/Object;)V

    .line 7
    :cond_0
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public removeDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V
    .locals 2

    const-string v0, "drawableProvider"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->drawableProviderCollection:Lcom/pspdfkit/internal/fm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/fm;->b(Lcom/pspdfkit/ui/PageObjectProvider;)V

    return-void
.end method

.method public removeOnDocumentSavedListener(Lcom/pspdfkit/ui/PdfThumbnailGrid$OnDocumentSavedListener;)Z
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onDocumentSavedListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public removeOnDocumentSavedListener(Lcom/pspdfkit/ui/PdfThumbnailGrid$OnPageClickListener;)Z
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 1
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onPageClickListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->thumbnailGridVisibilityListeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->backgroundColor:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->applyTheme()V

    return-void
.end method

.method public setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 5

    const-string v0, "configuration"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    if-eqz v0, :cond_8

    const/4 v2, 0x0

    if-nez p1, :cond_0

    .line 56
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a()V

    .line 57
    iput-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    goto :goto_0

    .line 59
    :cond_0
    move-object v3, p1

    check-cast v3, Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0, v3, p2}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    .line 60
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    iget-boolean v3, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->showPageLabels:Z

    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a(Z)V

    .line 62
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->isDisplayed:Z

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->f()V

    .line 66
    :cond_1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorEnabled:Z

    if-eqz v0, :cond_7

    .line 68
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;)Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 70
    new-instance v3, Lcom/pspdfkit/internal/xp;

    const-string v4, "com.pspdfkit.ui.PSPDFThumbnailGrid.RETAINED_STATE_FRAGMENT"

    invoke-direct {v3, v0, v4}, Lcom/pspdfkit/internal/xp;-><init>(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->retainedDocumentEditorHolder:Lcom/pspdfkit/internal/xp;

    .line 72
    invoke-virtual {v3}, Lcom/pspdfkit/internal/xp;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/k8;

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    if-eqz v0, :cond_2

    const/4 v3, 0x1

    .line 73
    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 74
    iput-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->retainedNativeDocumentEditor:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    .line 76
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->retainedDocumentEditorHolder:Lcom/pspdfkit/internal/xp;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xp;->c()Lcom/pspdfkit/internal/xp$a;

    .line 79
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k8;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eq v0, p1, :cond_5

    .line 80
    :cond_4
    invoke-static {p1}, Lcom/pspdfkit/document/editor/PdfDocumentEditorFactory;->createForDocument(Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/document/editor/PdfDocumentEditor;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/k8;

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    .line 81
    iput-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->retainedNativeDocumentEditor:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    .line 84
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorSavingToolbarHandler:Lcom/pspdfkit/internal/m8;

    if-eqz v0, :cond_6

    .line 88
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditor:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/m8;->a(Lcom/pspdfkit/internal/k8;)V

    .line 91
    :cond_6
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->floatingActionButton:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 98
    :cond_7
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->document:Lcom/pspdfkit/document/PdfDocument;

    if-eq v0, p1, :cond_8

    .line 99
    iput v2, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->highlightedPageIndex:I

    .line 103
    :cond_8
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->document:Lcom/pspdfkit/document/PdfDocument;

    .line 104
    iput-object p2, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-void
.end method

.method public setDocumentEditorEnabled(Z)V
    .locals 2

    if-eqz p1, :cond_1

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Your current license does not allow editing of PDF documents."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 3
    :cond_1
    :goto_0
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorEnabled:Z

    return-void
.end method

.method public setDocumentEditorExportEnabled(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/m8;->a(Z)V

    goto :goto_0

    .line 4
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->exportEnabled:Ljava/lang/Boolean;

    :goto_0
    return-void
.end method

.method public setDocumentEditorSaveAsEnabled(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/m8;->b(Z)V

    goto :goto_0

    .line 4
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->saveAsEnabled:Ljava/lang/Boolean;

    :goto_0
    return-void
.end method

.method public setFilePicker(Lcom/pspdfkit/document/editor/FilePicker;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->filePicker:Lcom/pspdfkit/document/editor/FilePicker;

    return-void
.end method

.method public setItemLabelBackground(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->itemLabelBackgroundDrawableRes:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->applyTheme()V

    return-void
.end method

.method public setItemLabelTextStyle(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->itemLabelTextStyle:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->applyTheme()V

    return-void
.end method

.method public final setNewPageFactory(Lcom/pspdfkit/document/editor/page/NewPageFactory;)V
    .locals 0

    if-nez p1, :cond_0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->createDefaultNewPageFactory()Lcom/pspdfkit/document/editor/page/NewPageFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->newPageFactory:Lcom/pspdfkit/document/editor/page/NewPageFactory;

    goto :goto_0

    .line 3
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->newPageFactory:Lcom/pspdfkit/document/editor/page/NewPageFactory;

    :goto_0
    return-void
.end method

.method public setOnPageClickListener(Lcom/pspdfkit/ui/PdfThumbnailGrid$OnPageClickListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onPageClickListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    if-eqz p1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->onPageClickListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public setRedactionAnnotationPreviewEnabled(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->isRedactionAnnotationPreviewEnabled:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->setRedactionAnnotationPreviewEnabled(Z)V

    :cond_0
    return-void
.end method

.method public setShowPageLabels(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->showPageLabels:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a(Z)V

    :cond_0
    return-void
.end method

.method public show()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->isDisplayed:Z

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->prepareForDisplay()V

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->isDisplayed:Z

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->thumbnailGridVisibilityListeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->onShow(Landroid/view/View;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->f()V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    iget v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->highlightedPageIndex:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->setHighlightedItem(I)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->recyclerView:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    iget v1, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->highlightedPageIndex:I

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    .line 10
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailGrid;->documentEditorEnabled:Z

    if-eqz v0, :cond_1

    .line 11
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->invalidateFab()V

    :cond_1
    const/4 v0, 0x0

    .line 14
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 15
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 16
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 18
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "open_thumbnail_grid"

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

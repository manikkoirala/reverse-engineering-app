.class public Lcom/pspdfkit/ui/PdfUiFragment;
.super Landroidx/fragment/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/PdfUi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;
    }
.end annotation


# static fields
.field private static final STATE_FRAGMENT:Ljava/lang/String; = "PdfActivity.ConfigurationChanged.FragmentState"


# instance fields
.field private configurationToApply:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

.field private implementation:Lcom/pspdfkit/internal/ui/f;

.field private final internalPdfUi:Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;

.field private viewContainer:Landroid/widget/FrameLayout;


# direct methods
.method static bridge synthetic -$$Nest$fgetviewContainer(Lcom/pspdfkit/ui/PdfUiFragment;)Landroid/widget/FrameLayout;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->viewContainer:Landroid/widget/FrameLayout;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mapplyConfiguration(Lcom/pspdfkit/ui/PdfUiFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfUiFragment;->applyConfiguration()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 19
    new-instance v0, Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;-><init>(Lcom/pspdfkit/ui/PdfUiFragment;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->internalPdfUi:Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;

    return-void
.end method

.method private applyConfiguration()V
    .locals 3

    .line 1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v2}, Lcom/pspdfkit/internal/ui/f;->onSaveInstanceState(Landroid/os/Bundle;ZZ)V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfUiFragment;->requirePdfParameters()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "activityState"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/ui/f;->retainedDocument:Lcom/pspdfkit/document/PdfDocument;

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfUiFragment;->internalPdfUi:Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;

    .line 13
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    .line 14
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    .line 15
    invoke-virtual {v1, v0}, Landroidx/fragment/app/FragmentTransaction;->remove(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onPause()V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onStop()V

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onDestroy()V

    .line 23
    new-instance v0, Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/app/AppCompatActivity;

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfUiFragment;->internalPdfUi:Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;

    invoke-direct {v0, v1, p0, v2}, Lcom/pspdfkit/internal/ui/f;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/internal/bg;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    const/4 v1, 0x0

    .line 24
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/f;->onCreate(Landroid/os/Bundle;)V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onStart()V

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onResume()V

    return-void
.end method

.method private requirePdfParameters()Landroid/os/Bundle;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->internalPdfUi:Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;->getPdfParameters()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 3
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PdfUiFragment was not initialized with proper arguments!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public synthetic addPropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$addPropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V

    return-void
.end method

.method public getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v0

    return-object v0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->configurationToApply:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    if-eqz v0, :cond_1

    return-object v0

    .line 9
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfUiFragment;->requirePdfParameters()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PSPDF.Configuration"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    const-string v2, "PdfConfiguration may not be null!"

    .line 10
    invoke-static {v2, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public synthetic getDocument()Lcom/pspdfkit/document/PdfDocument;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getDocument(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getDocumentCoordinator(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/DocumentCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public getImplementation()Lcom/pspdfkit/internal/ui/f;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    return-object v0
.end method

.method public synthetic getPSPDFKitViews()Lcom/pspdfkit/ui/PSPDFKitViews;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getPSPDFKitViews(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/PSPDFKitViews;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getPageIndex()I
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getPageIndex(Lcom/pspdfkit/ui/PdfUi;)I

    move-result v0

    return v0
.end method

.method public synthetic getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getPdfFragment(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getPropertyInspectorCoordinator()Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getPropertyInspectorCoordinator(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getScreenTimeout()J
    .locals 2

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getScreenTimeout(Lcom/pspdfkit/ui/PdfUi;)J

    move-result-wide v0

    return-wide v0
.end method

.method public synthetic getSiblingPageIndex(I)I
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getSiblingPageIndex(Lcom/pspdfkit/ui/PdfUi;I)I

    move-result p1

    return p1
.end method

.method public synthetic getUserInterfaceViewMode()Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$getUserInterfaceViewMode(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    move-result-object v0

    return-object v0
.end method

.method public synthetic hideUserInterface()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$hideUserInterface(Lcom/pspdfkit/ui/PdfUi;)V

    return-void
.end method

.method public synthetic isDocumentInteractionEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$isDocumentInteractionEnabled(Lcom/pspdfkit/ui/PdfUi;)Z

    move-result v0

    return v0
.end method

.method public synthetic isImageDocument()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$isImageDocument(Lcom/pspdfkit/ui/PdfUi;)Z

    move-result v0

    return v0
.end method

.method public synthetic isUserInterfaceEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$isUserInterfaceEnabled(Lcom/pspdfkit/ui/PdfUi;)Z

    move-result v0

    return v0
.end method

.method public synthetic isUserInterfaceVisible()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$isUserInterfaceVisible(Lcom/pspdfkit/ui/PdfUi;)Z

    move-result v0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/f;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onBackPressed()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/ui/f;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .line 1
    new-instance p1, Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    check-cast p2, Landroidx/appcompat/app/AppCompatActivity;

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->internalPdfUi:Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;

    invoke-direct {p1, p2, p0, v0}, Lcom/pspdfkit/internal/ui/f;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/internal/bg;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    .line 2
    new-instance p1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfUiFragment;->viewContainer:Landroid/widget/FrameLayout;

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfUiFragment;->configurationToApply:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    if-eqz p1, :cond_0

    .line 8
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfUiFragment;->requirePdfParameters()Landroid/os/Bundle;

    move-result-object p2

    .line 9
    invoke-static {p1, p2, p3}, Lcom/pspdfkit/internal/ui/f;->applyConfigurationToParamsAndState(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Landroid/os/Bundle;Landroid/os/Bundle;)V

    const/4 p1, 0x0

    .line 11
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfUiFragment;->configurationToApply:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 13
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/ui/f;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 14
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->setHasOptionsMenu(Z)V

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfUiFragment;->viewContainer:Landroid/widget/FrameLayout;

    return-object p1
.end method

.method public onDestroy()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onDestroy()V

    return-void
.end method

.method public onDocumentClick()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method public onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method public onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V
    .locals 0

    return-void
.end method

.method public onGenerateMenuItemIds(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    return-object p1
.end method

.method public onGetShowAsAction(II)I
    .locals 0

    return p2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    return-void
.end method

.method public onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    return-void
.end method

.method public onPause()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    return-void
.end method

.method public onResume()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 5
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6
    invoke-interface {p0}, Lcom/pspdfkit/ui/PdfUi;->getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getState()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PdfActivity.ConfigurationChanged.FragmentState"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public onSetActivityTitle(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/ui/f;->onSetActivityTitle(Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public onStart()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStop()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->onStop()V

    return-void
.end method

.method public onUserInterfaceVisibilityChanged(Z)V
    .locals 0

    return-void
.end method

.method public synthetic removePropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$removePropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V

    return-void
.end method

.method public synthetic requirePdfFragment()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$requirePdfFragment(Lcom/pspdfkit/ui/PdfUi;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setAnnotationCreationInspectorController(Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setAnnotationCreationInspectorController(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;)V

    return-void
.end method

.method public synthetic setAnnotationEditingInspectorController(Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setAnnotationEditingInspectorController(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;)V

    return-void
.end method

.method public setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 2

    const-string v0, "configuration"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment;->implementation:Lcom/pspdfkit/internal/ui/f;

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    goto :goto_0

    .line 58
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfUiFragment;->configurationToApply:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    :goto_0
    return-void
.end method

.method public synthetic setDocumentFromDataProvider(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentFromDataProvider(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic setDocumentFromDataProviders(Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentFromDataProviders(Lcom/pspdfkit/ui/PdfUi;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public synthetic setDocumentFromUri(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentFromUri(Lcom/pspdfkit/ui/PdfUi;Landroid/net/Uri;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic setDocumentFromUris(Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentFromUris(Lcom/pspdfkit/ui/PdfUi;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public synthetic setDocumentInteractionEnabled(Z)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentInteractionEnabled(Lcom/pspdfkit/ui/PdfUi;Z)V

    return-void
.end method

.method public synthetic setDocumentPrintDialogFactory(Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentPrintDialogFactory(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;)V

    return-void
.end method

.method public synthetic setDocumentSharingDialogFactory(Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setDocumentSharingDialogFactory(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;)V

    return-void
.end method

.method public synthetic setOnContextualToolbarLifecycleListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarLifecycleListener;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setOnContextualToolbarLifecycleListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarLifecycleListener;)V

    return-void
.end method

.method public synthetic setOnContextualToolbarMovementListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarMovementListener;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setOnContextualToolbarMovementListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarMovementListener;)V

    return-void
.end method

.method public synthetic setOnContextualToolbarPositionListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setOnContextualToolbarPositionListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;)V

    return-void
.end method

.method public synthetic setPageIndex(I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setPageIndex(Lcom/pspdfkit/ui/PdfUi;I)V

    return-void
.end method

.method public synthetic setPageIndex(IZ)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setPageIndex(Lcom/pspdfkit/ui/PdfUi;IZ)V

    return-void
.end method

.method public synthetic setPrintOptionsProvider(Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setPrintOptionsProvider(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V

    return-void
.end method

.method public synthetic setScreenTimeout(J)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setScreenTimeout(Lcom/pspdfkit/ui/PdfUi;J)V

    return-void
.end method

.method public synthetic setSharingActionMenuListener(Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setSharingActionMenuListener(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;)V

    return-void
.end method

.method public synthetic setSharingOptionsProvider(Lcom/pspdfkit/document/sharing/SharingOptionsProvider;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setSharingOptionsProvider(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/document/sharing/SharingOptionsProvider;)V

    return-void
.end method

.method public synthetic setUserInterfaceEnabled(Z)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setUserInterfaceEnabled(Lcom/pspdfkit/ui/PdfUi;Z)V

    return-void
.end method

.method public synthetic setUserInterfaceViewMode(Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setUserInterfaceViewMode(Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;)V

    return-void
.end method

.method public synthetic setUserInterfaceVisible(ZZ)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$setUserInterfaceVisible(Lcom/pspdfkit/ui/PdfUi;ZZ)V

    return-void
.end method

.method public synthetic showUserInterface()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$showUserInterface(Lcom/pspdfkit/ui/PdfUi;)V

    return-void
.end method

.method public synthetic toggleUserInterface()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/PdfUi$-CC;->$default$toggleUserInterface(Lcom/pspdfkit/ui/PdfUi;)V

    return-void
.end method

.class public interface abstract Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioRecordingModeChangeListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/audio/AudioModeListeners;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AudioRecordingModeChangeListener"
.end annotation


# virtual methods
.method public abstract onChangeAudioRecordingMode(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V
.end method

.method public abstract onEnterAudioRecordingMode(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V
.end method

.method public abstract onExitAudioRecordingMode(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V
.end method

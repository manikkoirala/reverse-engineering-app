.class public interface abstract Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioPlaybackModeChangeListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/audio/AudioModeListeners;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AudioPlaybackModeChangeListener"
.end annotation


# virtual methods
.method public abstract onChangeAudioPlaybackMode(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V
.end method

.method public abstract onEnterAudioPlaybackMode(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V
.end method

.method public abstract onExitAudioPlaybackMode(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V
.end method

.class public final synthetic Lcom/pspdfkit/ui/audio/AudioPlaybackController$-CC;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public static $default$toggle(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/audio/AudioPlaybackController;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/audio/AudioPlaybackController;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {p0}, Lcom/pspdfkit/ui/audio/AudioPlaybackController;->pause()V

    goto :goto_0

    .line 4
    :cond_0
    invoke-interface {p0}, Lcom/pspdfkit/ui/audio/AudioPlaybackController;->resume()V

    :goto_0
    return-void
.end method

.class public final synthetic Lcom/pspdfkit/ui/audio/AudioRecordingController$-CC;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public static $default$exitAudioRecordingMode(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/audio/AudioRecordingController;

    const/4 v0, 0x0

    .line 1
    invoke-interface {p0, v0}, Lcom/pspdfkit/ui/audio/AudioRecordingController;->exitAudioRecordingMode(Z)V

    return-void
.end method

.method public static $default$toggle(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/ui/audio/AudioRecordingController;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/audio/AudioRecordingController;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {p0}, Lcom/pspdfkit/ui/audio/AudioRecordingController;->pause()V

    goto :goto_0

    .line 4
    :cond_0
    invoke-interface {p0}, Lcom/pspdfkit/ui/audio/AudioRecordingController;->resume()V

    :goto_0
    return-void
.end method

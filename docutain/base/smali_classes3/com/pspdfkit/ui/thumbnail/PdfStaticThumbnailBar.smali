.class public Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;
.super Lcom/pspdfkit/internal/views/utils/a;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;
.implements Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;,
        Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$AddBorderToBitmap;,
        Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "PSPDFKit.StaticThumbnailBar"

.field private static final NON_RENDERED_SELECTED_THUMBNAIL_ALPHA:F = 0.4f

.field private static final PAGE_INDEX_NONE:I = -0x1

.field private static final THUMBNAIL_RENDERING_DEBOUNCE_MS:I = 0x64


# instance fields
.field private addBorderToBitmap:Lio/reactivex/rxjava3/functions/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/functions/Function<",
            "Landroid/graphics/Bitmap;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private availableWidthForThumbnailLayout:I

.field private borderSizePx:I

.field private contentPaddingPx:I

.field private currentPageDeferRenderingInitializationDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

.field currentPageIndex:I

.field private currentPageSiblingIndex:I

.field private detector:Landroid/view/GestureDetector;

.field private final dirtyPages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final dirtyPagesRunnables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private document:Lcom/pspdfkit/internal/zf;

.field private final drawableProviders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;"
        }
    .end annotation
.end field

.field private excludedAnnotationTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private firstPageSingle:Z

.field private gotoPageCallQueried:Z

.field private gotoPageCalledQueriedTargetIndex:I

.field private isRTL:Z

.field private layoutStyle:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

.field private leftDrawable:Lcom/pspdfkit/internal/z4;

.field private leftSelectedImage:Landroid/widget/ImageView;

.field private leftSelectedImageRenderDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

.field private onPageChangedListener:Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

.field private pageRenderingConfiguration:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

.field private pinnedModeTopSeparator:Landroid/widget/FrameLayout;

.field private redactionAnnotationPreviewEnabled:Z

.field private rightDrawable:Lcom/pspdfkit/internal/z4;

.field rightSelectedImage:Landroid/widget/ImageView;

.field private rightSelectedImageRenderDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

.field private staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

.field private themeConfig:Lcom/pspdfkit/internal/bt;

.field private thumbnailCount:I

.field private final thumbnailDisposables:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

.field private thumbnailHeight:I

.field private thumbnailPaddingPx:I

.field private thumbnailStrokePaint:Landroid/graphics/Paint;

.field private thumbnailWidth:I

.field private useDoublePageMode:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetcontentPaddingPx(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->contentPaddingPx:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetdocument(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Lcom/pspdfkit/internal/zf;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetfirstPageSingle(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetgotoPageCalledQueriedTargetIndex(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetisRTL(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetonPageChangedListener(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->onPageChangedListener:Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetstaticThumbnailLayout(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Lcom/pspdfkit/internal/ct;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetthumbnailCount(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailCount:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetthumbnailHeight(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailHeight:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetuseDoublePageMode(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputgotoPageCallQueried(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCallQueried:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputgotoPageCalledQueriedTargetIndex(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;I)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mrefresh(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->refresh()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 1
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__thumbnailBarStyle:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/pspdfkit/internal/views/utils/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->availableWidthForThumbnailLayout:I

    .line 34
    iput-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->redactionAnnotationPreviewEnabled:Z

    .line 44
    new-instance v2, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {v2}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailDisposables:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 96
    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailCount:I

    const/4 v2, -0x1

    .line 102
    iput v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    .line 106
    iput v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    .line 112
    iput-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCallQueried:Z

    .line 115
    iput v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    .line 126
    iput-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    .line 129
    iput-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    .line 132
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPages:Ljava/util/Set;

    .line 136
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    .line 143
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->drawableProviders:Ljava/util/List;

    .line 146
    iput-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    .line 149
    iput-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pinnedModeTopSeparator:Landroid/widget/FrameLayout;

    .line 159
    sget-object v0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;->FLOATING:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->layoutStyle:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    .line 174
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 175
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__thumbnailBarStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/internal/views/utils/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    .line 176
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->availableWidthForThumbnailLayout:I

    .line 208
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->redactionAnnotationPreviewEnabled:Z

    .line 218
    new-instance v0, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailDisposables:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 270
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailCount:I

    const/4 v0, -0x1

    .line 276
    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    .line 280
    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    .line 286
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCallQueried:Z

    .line 289
    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    .line 300
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    .line 303
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    .line 306
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPages:Ljava/util/Set;

    .line 310
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    .line 317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->drawableProviders:Ljava/util/List;

    .line 320
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    const/4 p2, 0x0

    .line 323
    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pinnedModeTopSeparator:Landroid/widget/FrameLayout;

    .line 333
    sget-object p2, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;->FLOATING:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->layoutStyle:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    .line 353
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 354
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/views/utils/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    .line 355
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->availableWidthForThumbnailLayout:I

    .line 387
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->redactionAnnotationPreviewEnabled:Z

    .line 397
    new-instance p3, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {p3}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailDisposables:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 449
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailCount:I

    const/4 p3, -0x1

    .line 455
    iput p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    .line 459
    iput p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    .line 465
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCallQueried:Z

    .line 468
    iput p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    .line 479
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    .line 482
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    .line 485
    new-instance p3, Ljava/util/HashSet;

    invoke-direct {p3}, Ljava/util/HashSet;-><init>()V

    iput-object p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPages:Ljava/util/Set;

    .line 489
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    iput-object p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    .line 496
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    iput-object p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->drawableProviders:Ljava/util/List;

    .line 499
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    const/4 p2, 0x0

    .line 502
    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pinnedModeTopSeparator:Landroid/widget/FrameLayout;

    .line 512
    sget-object p2, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;->FLOATING:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->layoutStyle:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    .line 538
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .line 539
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/views/utils/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p2, 0x0

    .line 540
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->availableWidthForThumbnailLayout:I

    .line 572
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->redactionAnnotationPreviewEnabled:Z

    .line 582
    new-instance p3, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {p3}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailDisposables:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 634
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailCount:I

    const/4 p3, -0x1

    .line 640
    iput p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    .line 644
    iput p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    .line 650
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCallQueried:Z

    .line 653
    iput p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    .line 664
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    .line 667
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    .line 670
    new-instance p3, Ljava/util/HashSet;

    invoke-direct {p3}, Ljava/util/HashSet;-><init>()V

    iput-object p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPages:Ljava/util/Set;

    .line 674
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    iput-object p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    .line 681
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    iput-object p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->drawableProviders:Ljava/util/List;

    .line 684
    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    const/4 p2, 0x0

    .line 687
    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pinnedModeTopSeparator:Landroid/widget/FrameLayout;

    .line 697
    sget-object p2, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;->FLOATING:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->layoutStyle:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    .line 732
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method private addThumbnailImage(Landroid/content/Context;Lcom/pspdfkit/internal/lu;Lcom/pspdfkit/utils/Size;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pageRenderingConfiguration:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 4
    sget-object p1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 5
    invoke-static {v0}, Lcom/pspdfkit/internal/ov;->c(Landroid/view/View;)V

    .line 7
    new-instance p1, Lcom/pspdfkit/internal/z4;

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pageRenderingConfiguration:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    iget-boolean v2, v1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->invertColors:Z

    if-eqz v2, :cond_1

    .line 9
    iget v1, v1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->paperColor:I

    invoke-static {v1}, Lcom/pspdfkit/internal/ga;->c(I)I

    move-result v1

    goto :goto_0

    .line 10
    :cond_1
    iget v1, v1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->paperColor:I

    :goto_0
    iget v2, p3, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v2, v2

    iget v3, p3, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int v3, v3

    iget-object v4, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailStrokePaint:Landroid/graphics/Paint;

    invoke-direct {p1, v1, v2, v3, v4}, Lcom/pspdfkit/internal/z4;-><init>(IIILandroid/graphics/Paint;)V

    .line 14
    iget v1, p3, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v1, v1

    iget p3, p3, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int p3, p3

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v2, v1, p3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 16
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p3, Lcom/pspdfkit/R$string;->pspdf__page_with_number:I

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/lu;->a()I

    move-result v4

    add-int/2addr v4, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {p1, p3, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 19
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 21
    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 22
    sget p1, Lcom/pspdfkit/R$id;->pspdf__tag_key_page_index:I

    invoke-virtual {p2}, Lcom/pspdfkit/internal/lu;->a()I

    move-result p3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {v0, p1, p3}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 23
    sget p1, Lcom/pspdfkit/R$id;->pspdf__tag_key_thumbnail_position:I

    invoke-virtual {v0, p1, p2}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 26
    new-instance p1, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$$ExternalSyntheticLambda4;

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;Lcom/pspdfkit/internal/lu;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    new-instance p3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/lu;->c()Lcom/pspdfkit/utils/Size;

    move-result-object v3

    iget v3, v3, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v3, v3

    .line 34
    invoke-virtual {p2}, Lcom/pspdfkit/internal/lu;->c()Lcom/pspdfkit/utils/Size;

    move-result-object v4

    iget v4, v4, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int v4, v4

    invoke-direct {p3, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 35
    invoke-virtual {p0, v0, p1, p3}, Landroid/view/ViewGroup;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 39
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailDisposables:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 40
    invoke-virtual {p2}, Lcom/pspdfkit/internal/lu;->a()I

    move-result p2

    invoke-direct {p0, v0, p2, v1, v2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->renderThumbnailIntoImageView(Landroid/widget/ImageView;IZZ)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p2

    .line 41
    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    return-void
.end method

.method private addThumbnailsToGroup(Landroid/content/Context;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    .line 2
    iput v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailCount:I

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ct;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/lu;

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/lu;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v2

    .line 5
    invoke-direct {p0, p1, v1, v2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->addThumbnailImage(Landroid/content/Context;Lcom/pspdfkit/internal/lu;Lcom/pspdfkit/utils/Size;)V

    .line 6
    iget v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailCount:I

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private cancelAllRenderingSubscriptions()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImageRenderDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImageRenderDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImageRenderDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 5
    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 6
    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImageRenderDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private createThumbnailsAfterLayout()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    if-nez v0, :cond_0

    goto :goto_2

    .line 2
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->removeChildViews()V

    .line 5
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->availableWidthForThumbnailLayout:I

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 7
    :goto_0
    iget v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->contentPaddingPx:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    iget v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    iget-boolean v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    iget-boolean v4, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    iget-boolean v5, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    iget-object v6, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    invoke-virtual/range {v1 .. v6}, Lcom/pspdfkit/internal/ct;->a(IZZZLcom/pspdfkit/internal/bt;)V

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ct;->b(I)V

    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 15
    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->addThumbnailsToGroup(Landroid/content/Context;)V

    .line 17
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 20
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->enlargeSelectedThumbnail(Landroid/content/Context;Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/ImageView;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    .line 23
    iget-boolean v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    if-eqz v2, :cond_2

    .line 24
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->enlargeSelectedThumbnail(Landroid/content/Context;Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/ImageView;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    .line 28
    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    .line 31
    :goto_1
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    :cond_3
    :goto_2
    return-void
.end method

.method private enlargeSelectedThumbnail(Landroid/content/Context;Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/ImageView;
    .locals 2

    .line 1
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x4

    .line 2
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/ov;->c(Landroid/view/View;)V

    const/4 p1, 0x1

    .line 4
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 5
    sget-object p1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailBorderColor()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 7
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    invoke-virtual {v0, p1, p1, p1, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pageRenderingConfiguration:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    if-eqz p1, :cond_0

    .line 9
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pageRenderingConfiguration:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    iget v1, v1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->paperColor:I

    invoke-direct {p1, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    const/4 p1, 0x0

    .line 12
    invoke-virtual {v0, p1}, Landroid/view/View;->setFocusable(Z)V

    .line 13
    invoke-virtual {v0, p1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 14
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    invoke-virtual {p0, v0, p1, p2}, Landroid/view/ViewGroup;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    return-object v0
.end method

.method private getPdfDrawablesForGivenPage(Landroid/content/Context;I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawable;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v1, :cond_1

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->drawableProviders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;

    .line 5
    iget-object v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    .line 6
    invoke-virtual {v2, p1, v3, p2}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->getDrawablesForPage(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 8
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 9
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private getPinnedModeTopSeparator()Landroid/view/View;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pinnedModeTopSeparator:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pinnedModeTopSeparator:Landroid/widget/FrameLayout;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iget v1, v1, Lcom/pspdfkit/internal/bt;->f:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pinnedModeTopSeparator:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private getRenderedThumbnailImageSize(III)Lcom/pspdfkit/utils/Size;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_1

    if-ltz p1, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    int-to-float p2, p2

    .line 5
    iget v0, p1, Lcom/pspdfkit/utils/Size;->width:F

    div-float/2addr p2, v0

    int-to-float p3, p3

    .line 6
    iget v0, p1, Lcom/pspdfkit/utils/Size;->height:F

    div-float/2addr p3, v0

    .line 7
    invoke-static {p2, p3}, Ljava/lang/Math;->min(FF)F

    move-result p2

    .line 9
    new-instance p3, Lcom/pspdfkit/utils/Size;

    iget v0, p1, Lcom/pspdfkit/utils/Size;->width:F

    mul-float v0, v0, p2

    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    mul-float p1, p1, p2

    invoke-direct {p3, v0, p1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    return-object p3

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private getSelectedThumbnailHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailHeight(I)I

    move-result v0

    return v0
.end method

.method private getSelectedThumbnailHeight(I)I
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 5
    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ct;->a(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    mul-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    add-float/2addr p1, v0

    float-to-int p1, p1

    return p1
.end method

.method private getSelectedThumbnailWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailWidth(I)I

    move-result v0

    return v0
.end method

.method private getSelectedThumbnailWidth(I)I
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 5
    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ct;->a(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    mul-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    add-float/2addr p1, v0

    float-to-int p1, p1

    return p1
.end method

.method private getSiblingSelectedThumbnailHeight()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2
    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailHeight(I)I

    move-result v0

    return v0

    .line 4
    :cond_0
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailHeight(I)I

    move-result v0

    return v0
.end method

.method private getSiblingSelectedThumbnailWidth()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2
    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailWidth(I)I

    move-result v0

    return v0

    .line 4
    :cond_0
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailWidth(I)I

    move-result v0

    return v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 3

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__static_thumbnail_bar:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    .line 2
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;-><init>(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener-IA;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->detector:Landroid/view/GestureDetector;

    const/4 v1, 0x0

    .line 3
    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    .line 6
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailStrokePaint:Landroid/graphics/Paint;

    .line 7
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/pspdfkit/R$dimen;->pspdf__thumbnail_bar_thumbnails_padding:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/pspdfkit/R$dimen;->pspdf__thumbnail_bar_content_padding:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->contentPaddingPx:I

    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/pspdfkit/R$dimen;->pspdf__thumbnail_bar_border_size:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->borderSizePx:I

    .line 15
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 17
    new-instance p1, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$AddBorderToBitmap;

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailStrokePaint:Landroid/graphics/Paint;

    invoke-direct {p1, v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$AddBorderToBitmap;-><init>(Landroid/graphics/Paint;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->addBorderToBitmap:Lio/reactivex/rxjava3/functions/Function;

    .line 19
    new-instance p1, Lcom/pspdfkit/internal/bt;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/pspdfkit/internal/bt;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    .line 21
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->refresh()V

    return-void
.end method

.method static synthetic lambda$renderThumbnailIntoImageView$3(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.StaticThumbnailBar"

    const-string v2, "Failed to render thumbnail image!"

    .line 1
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private notifyPageUpdated(I)V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    .line 2
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v3, v3, Landroid/widget/ImageView;

    if-nez v3, :cond_0

    goto :goto_1

    .line 3
    :cond_0
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 4
    sget v4, Lcom/pspdfkit/R$id;->pspdf__tag_key_page_index:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 5
    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, p1, :cond_1

    .line 6
    invoke-direct {p0, v3, p1, v1, v1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->renderThumbnailIntoImageView(Landroid/widget/ImageView;IZZ)Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private refresh()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->cancelAllRenderingSubscriptions()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->layoutStyle:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    sget-object v1, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;->FLOATING:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    if-ne v0, v1, :cond_2

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__thumbnail_bar_background:I

    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 12
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iget v1, v1, Lcom/pspdfkit/internal/bt;->a:I

    .line 13
    invoke-static {v0}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 14
    invoke-static {v0, v1}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 15
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__thumbnail_bar_background:I

    .line 16
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 18
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 19
    instance-of v2, v1, Landroid/graphics/drawable/GradientDrawable;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 20
    move-object v2, v1

    check-cast v2, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 21
    iget v4, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->borderSizePx:I

    iget-object v5, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iget v5, v5, Lcom/pspdfkit/internal/bt;->f:I

    invoke-virtual {v2, v4, v5}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 23
    :cond_0
    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v4, v3

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-direct {v2, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    move-object v0, v2

    .line 25
    :cond_1
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 28
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iget v0, v0, Lcom/pspdfkit/internal/bt;->a:I

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 30
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__floating_thumbnail_bar_elevation:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {p0, v0}, Landroidx/core/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailStrokePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iget v1, v1, Lcom/pspdfkit/internal/bt;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iget v1, v0, Lcom/pspdfkit/internal/bt;->d:I

    iput v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailWidth:I

    .line 35
    iget v0, v0, Lcom/pspdfkit/internal/bt;->e:I

    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailHeight:I

    .line 37
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->createThumbnailsAfterLayout()V

    .line 38
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->updateCurrentlySelectedPageView()V

    return-void
.end method

.method private removeChildViews()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailDisposables:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    const/4 v0, 0x0

    .line 4
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 5
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 6
    instance-of v2, v1, Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 7
    move-object v2, v1

    check-cast v2, Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 8
    sget v2, Lcom/pspdfkit/R$id;->pspdf__tag_key_bitmap:I

    invoke-virtual {v1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    .line 9
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/n4;->d(Landroid/graphics/Bitmap;)V

    .line 10
    :cond_0
    sget v2, Lcom/pspdfkit/R$id;->pspdf__tag_key_page_index:I

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14
    :cond_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    return-void
.end method

.method private renderThumbnailIntoImageView(Landroid/widget/ImageView;IZZ)Lio/reactivex/rxjava3/disposables/Disposable;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pageRenderingConfiguration:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    if-nez v1, :cond_0

    goto/16 :goto_0

    .line 5
    :cond_0
    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v0

    .line 6
    iget v1, v0, Lcom/pspdfkit/utils/Size;->width:F

    float-to-double v1, v1

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    float-to-double v3, v0

    div-double/2addr v1, v3

    .line 9
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailHeight:I

    int-to-double v3, v0

    mul-double v3, v3, v1

    double-to-int v1, v3

    const/4 v2, 0x1

    .line 10
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 12
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    .line 13
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 15
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$id;->pspdf__tag_key_bitmap:I

    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/n4;->d(Landroid/graphics/Bitmap;)V

    .line 16
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/pspdfkit/internal/n4;->a(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 17
    sget v1, Lcom/pspdfkit/R$id;->pspdf__tag_key_bitmap:I

    invoke-virtual {p1, v1, v0}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 18
    sget v1, Lcom/pspdfkit/R$id;->pspdf__tag_key_page_index:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 20
    new-instance v1, Lcom/pspdfkit/internal/rc$a;

    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    invoke-direct {v1, v2, p2}, Lcom/pspdfkit/internal/rc$a;-><init>(Lcom/pspdfkit/internal/zf;I)V

    const/4 v2, 0x3

    .line 21
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/g4$a;->c(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/rc$a;

    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pageRenderingConfiguration:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    .line 22
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/rc$a;->b(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/rc$a;

    move-result-object v1

    .line 23
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/g4$a;->a(Landroid/graphics/Bitmap;)Lcom/pspdfkit/internal/g4$a;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/rc$a;

    .line 24
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/g4$a;->b(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/rc$a;

    .line 25
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/g4$a;->a(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/rc$a;

    const/4 v1, 0x0

    .line 26
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/lang/Integer;)Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/rc$a;

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->excludedAnnotationTypes:Ljava/util/ArrayList;

    .line 27
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/rc$a;

    .line 28
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getPdfDrawablesForGivenPage(Landroid/content/Context;I)Ljava/util/List;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/util/List;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/rc$a;

    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->redactionAnnotationPreviewEnabled:Z

    .line 29
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/g4$a;->b(Z)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/rc$a;

    .line 30
    invoke-virtual {p2}, Lcom/pspdfkit/internal/rc$a;->b()Lcom/pspdfkit/internal/rc;

    move-result-object p2

    .line 32
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 33
    invoke-static {p2}, Lcom/pspdfkit/internal/hm;->a(Lcom/pspdfkit/internal/rc;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    .line 34
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    const/4 v2, 0x5

    .line 35
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    .line 36
    invoke-virtual {p2, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->addBorderToBitmap:Lio/reactivex/rxjava3/functions/Function;

    .line 37
    invoke-virtual {p2, v1}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    new-instance v1, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;

    .line 38
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move-object v2, v1

    move v4, p3

    invoke-direct/range {v2 .. v7}, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;-><init>(Landroid/content/res/Resources;ZJLandroid/graphics/drawable/Drawable;)V

    invoke-virtual {p2, v1}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 39
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$$ExternalSyntheticLambda1;

    invoke-direct {p2, p0, v0, p4}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;Ljava/lang/ref/WeakReference;Z)V

    new-instance p3, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$$ExternalSyntheticLambda2;

    invoke-direct {p3}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$$ExternalSyntheticLambda2;-><init>()V

    .line 40
    invoke-virtual {p1, p2, p3}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    return-object p1

    .line 41
    :cond_1
    :goto_0
    invoke-static {}, Lio/reactivex/rxjava3/disposables/Disposable$-CC;->disposed()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method private shouldFindFocus(Landroid/view/View;I)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 1
    sget v2, Lcom/pspdfkit/R$id;->pspdf__tag_key_page_index:I

    .line 2
    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v0

    :cond_0
    const/4 v2, 0x2

    if-ne p2, v2, :cond_1

    .line 6
    sget p2, Lcom/pspdfkit/R$id;->pspdf__tag_key_page_index:I

    .line 7
    invoke-virtual {p1, p2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result p2

    sub-int/2addr p2, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method private updateCurrentlySelectedPageView()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_c

    .line 3
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_c

    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pageRenderingConfiguration:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    if-nez v0, :cond_0

    goto/16 :goto_5

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->cancel()V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 11
    invoke-static {v0}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->cancel()V

    .line 14
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageDeferRenderingInitializationDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 15
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 16
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->cancelAllRenderingSubscriptions()V

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    iget v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v0

    .line 20
    iget v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    if-eq v2, v1, :cond_2

    iget-object v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 21
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    iget v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    .line 24
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pageRenderingConfiguration:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    iget-boolean v4, v3, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->invertColors:Z

    .line 25
    iget v3, v3, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->paperColor:I

    .line 27
    iget-object v5, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftDrawable:Lcom/pspdfkit/internal/z4;

    if-nez v5, :cond_4

    .line 28
    new-instance v5, Lcom/pspdfkit/internal/z4;

    if-eqz v4, :cond_3

    .line 29
    invoke-static {v3}, Lcom/pspdfkit/internal/ga;->c(I)I

    move-result v6

    goto :goto_1

    :cond_3
    move v6, v3

    :goto_1
    iget v7, v0, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v7, v7

    iget v8, v0, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int v8, v8

    iget-object v9, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailStrokePaint:Landroid/graphics/Paint;

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/pspdfkit/internal/z4;-><init>(IIILandroid/graphics/Paint;)V

    iput-object v5, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftDrawable:Lcom/pspdfkit/internal/z4;

    .line 34
    :cond_4
    iget-object v5, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftDrawable:Lcom/pspdfkit/internal/z4;

    iget v6, v0, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v6, v6

    iget v7, v0, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int v7, v7

    const/4 v8, 0x0

    invoke-virtual {v5, v8, v8, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 35
    iget-object v5, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftDrawable:Lcom/pspdfkit/internal/z4;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 37
    iget-object v5, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    if-eqz v5, :cond_7

    if-eqz v2, :cond_7

    .line 38
    iget-object v5, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightDrawable:Lcom/pspdfkit/internal/z4;

    if-nez v5, :cond_6

    .line 39
    new-instance v5, Lcom/pspdfkit/internal/z4;

    if-eqz v4, :cond_5

    .line 40
    invoke-static {v3}, Lcom/pspdfkit/internal/ga;->c(I)I

    move-result v3

    :cond_5
    iget v4, v2, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v4, v4

    iget v2, v2, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int v2, v2

    iget-object v6, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailStrokePaint:Landroid/graphics/Paint;

    invoke-direct {v5, v3, v4, v2, v6}, Lcom/pspdfkit/internal/z4;-><init>(IIILandroid/graphics/Paint;)V

    iput-object v5, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightDrawable:Lcom/pspdfkit/internal/z4;

    .line 45
    :cond_6
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightDrawable:Lcom/pspdfkit/internal/z4;

    iget v3, v0, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v3, v3

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int v0, v0

    invoke-virtual {v2, v8, v8, v3, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 46
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightDrawable:Lcom/pspdfkit/internal/z4;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 49
    :cond_7
    new-instance v0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0xc8

    .line 73
    invoke-virtual {v0, v3, v4, v2}, Lio/reactivex/rxjava3/core/Observable;->delaySubscription(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 74
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->computation()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Observable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageDeferRenderingInitializationDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 77
    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    if-eqz v0, :cond_8

    .line 78
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getDoublePageThumbnailOffsetX(I)I

    move-result v0

    goto :goto_2

    .line 79
    :cond_8
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getThumbnailOffsetX(I)I

    move-result v0

    :goto_2
    int-to-float v0, v0

    .line 80
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    .line 81
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/pspdfkit/R$string;->pspdf__page_with_number:I

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/Object;

    iget v7, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    add-int/2addr v7, v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v3, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 82
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 85
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    iget v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    const/4 v4, 0x4

    if-ltz v3, :cond_a

    const/4 v3, 0x0

    cmpg-float v3, v0, v3

    if-gez v3, :cond_9

    goto :goto_3

    :cond_9
    const/4 v3, 0x0

    goto :goto_4

    :cond_a
    :goto_3
    const/4 v3, 0x4

    :goto_4
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 86
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    const v3, 0x3ecccccd    # 0.4f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 88
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    if-eqz v2, :cond_c

    .line 90
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/pspdfkit/R$string;->pspdf__page_with_number:I

    new-array v9, v5, [Ljava/lang/Object;

    iget v10, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    add-int/2addr v10, v5

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v9, v8

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 91
    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 94
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    .line 95
    iget v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    if-ne v2, v1, :cond_b

    const/4 v8, 0x4

    .line 96
    :cond_b
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_c
    :goto_5
    return-void
.end method


# virtual methods
.method public addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 0

    return-void
.end method

.method public clearDocument()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->cancelAllRenderingSubscriptions()V

    .line 3
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method public focusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 1

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->shouldFindFocus(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 6
    :cond_0
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    .line 10
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getBackgroundColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iget v0, v0, Lcom/pspdfkit/internal/bt;->a:I

    return v0
.end method

.method public getDocumentListener()Lcom/pspdfkit/listeners/DocumentListener;
    .locals 0

    return-object p0
.end method

.method getDoublePageThumbnailOffsetX(I)I
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    const/4 v1, 0x0

    if-eqz v0, :cond_a

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ct;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_a

    .line 3
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 9
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    .line 10
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/vg;->a(IZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    add-int/lit8 p1, p1, -0x1

    .line 11
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    .line 12
    invoke-virtual {v2}, Lcom/pspdfkit/internal/ct;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 13
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/ct;->b()Ljava/util/ArrayList;

    move-result-object v2

    .line 14
    iget-boolean v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    if-eqz v3, :cond_2

    .line 16
    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 19
    :cond_2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v3

    const/4 v4, 0x1

    if-ltz v3, :cond_5

    .line 22
    iget-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    if-eqz p1, :cond_3

    .line 24
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result p1

    sub-int/2addr p1, v4

    sub-int v3, p1, v3

    .line 30
    :cond_3
    iget-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    .line 31
    invoke-static {v3, p1, v1}, Lcom/pspdfkit/internal/vg;->a(IZZ)Z

    move-result p1

    if-nez p1, :cond_4

    add-int/lit8 v3, v3, -0x1

    .line 32
    :cond_4
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result p1

    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    :goto_1
    sub-int/2addr p1, v0

    return p1

    :cond_5
    neg-int v5, v3

    add-int/lit8 v5, v5, -0x2

    .line 40
    iget-boolean v6, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    if-eqz v6, :cond_6

    .line 42
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int/2addr v2, v4

    add-int/2addr v2, v3

    goto :goto_2

    :cond_6
    add-int/lit8 v2, v5, -0x1

    :goto_2
    const/4 v3, -0x1

    if-ge v2, v3, :cond_7

    return v1

    :cond_7
    if-ne v2, v3, :cond_8

    goto :goto_3

    :cond_8
    add-int/lit8 v4, v2, 0x2

    move v1, v2

    :goto_3
    add-int/lit8 v2, v1, 0x2

    .line 60
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_9

    .line 61
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 62
    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int/2addr v3, v2

    .line 66
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/lu;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/lu;->a()I

    move-result v4

    .line 67
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/lu;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/lu;->a()I

    move-result v5

    sub-int/2addr v4, v5

    .line 69
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/lu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/lu;->a()I

    move-result v0

    sub-int/2addr p1, v0

    int-to-float v0, v3

    int-to-float v1, v4

    div-float/2addr v0, v1

    int-to-float p1, p1

    mul-float v0, v0, p1

    float-to-int p1, v0

    add-int/2addr v2, p1

    .line 71
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v2, p1

    return v2

    .line 75
    :cond_9
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result p1

    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    mul-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_a
    :goto_4
    return v1
.end method

.method public getLayoutStyle()Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->layoutStyle:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    return-object v0
.end method

.method public getLeftSelectedImage()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_THUMBNAIL_BAR:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    return-object v0
.end method

.method public getSelectedThumbnailBorderColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iget v0, v0, Lcom/pspdfkit/internal/bt;->c:I

    return v0
.end method

.method public getThumbnailBorderColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iget v0, v0, Lcom/pspdfkit/internal/bt;->b:I

    return v0
.end method

.method public getThumbnailHeight()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iget v0, v0, Lcom/pspdfkit/internal/bt;->e:I

    return v0
.end method

.method public getThumbnailOffsetX(I)I
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ct;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_7

    .line 3
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 7
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    .line 8
    invoke-virtual {v2}, Lcom/pspdfkit/internal/ct;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/ct;->b()Ljava/util/ArrayList;

    move-result-object v2

    .line 10
    iget-boolean v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    if-eqz v3, :cond_1

    .line 12
    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 15
    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v3

    if-ltz v3, :cond_3

    .line 19
    iget-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    if-eqz p1, :cond_2

    .line 21
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    sub-int v3, p1, v3

    .line 26
    :cond_2
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result p1

    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    :goto_0
    sub-int/2addr p1, v0

    return p1

    :cond_3
    neg-int v3, v3

    add-int/lit8 v3, v3, -0x2

    .line 33
    iget-boolean v4, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    if-eqz v4, :cond_4

    .line 35
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v3

    add-int/lit8 v3, v2, -0x1

    :cond_4
    if-gez v3, :cond_5

    return v1

    :cond_5
    add-int/lit8 v1, v3, 0x1

    .line 41
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 42
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 43
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 45
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/lu;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/lu;->a()I

    move-result v1

    .line 46
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/lu;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/lu;->a()I

    move-result v5

    sub-int/2addr v1, v5

    .line 48
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/lu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/lu;->a()I

    move-result v0

    sub-int/2addr p1, v0

    sub-int/2addr v4, v2

    int-to-float v0, v4

    int-to-float v1, v1

    div-float/2addr v0, v1

    int-to-float p1, p1

    mul-float v0, v0, p1

    float-to-int p1, v0

    add-int/2addr v2, p1

    .line 52
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    sub-int/2addr v2, p1

    return v2

    .line 54
    :cond_6
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result p1

    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    goto :goto_0

    :cond_7
    :goto_1
    return v1
.end method

.method public getThumbnailWidth()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iget-boolean v0, v0, Lcom/pspdfkit/internal/bt;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ct;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ct;->a()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/lu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/lu;->c()Lcom/pspdfkit/utils/Size;

    move-result-object v0

    iget v0, v0, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v0, v0

    return v0

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iget v0, v0, Lcom/pspdfkit/internal/bt;->d:I

    return v0
.end method

.method public hide()V
    .locals 0

    return-void
.end method

.method public isBackgroundTransparent()Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2
    instance-of v1, v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 3
    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method public isDisplayed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isRedactionAnnotationPreviewEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->redactionAnnotationPreviewEnabled:Z

    return v0
.end method

.method public isUsingPageAspectRatio()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iget-boolean v0, v0, Lcom/pspdfkit/internal/bt;->g:Z

    return v0
.end method

.method synthetic lambda$addThumbnailImage$1$com-pspdfkit-ui-thumbnail-PdfStaticThumbnailBar(Lcom/pspdfkit/internal/lu;Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->onPageChangedListener:Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

    if-eqz p2, :cond_0

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lu;->a()I

    move-result p1

    invoke-interface {p2, p0, p1}, Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;->onPageChanged(Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;I)V

    :cond_0
    return-void
.end method

.method synthetic lambda$onPageUpdated$0$com-pspdfkit-ui-thumbnail-PdfStaticThumbnailBar()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPages:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->notifyPageUpdated(I)V

    goto :goto_0

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPages:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 8
    invoke-virtual {p0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 10
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method synthetic lambda$renderThumbnailIntoImageView$2$com-pspdfkit-ui-thumbnail-PdfStaticThumbnailBar(Ljava/lang/ref/WeakReference;ZLandroid/graphics/drawable/Drawable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    if-eqz p1, :cond_5

    .line 3
    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    if-eqz p2, :cond_5

    .line 13
    iget-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    if-eqz p2, :cond_0

    .line 14
    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    invoke-virtual {p0, p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getDoublePageThumbnailOffsetX(I)I

    move-result p2

    goto :goto_0

    .line 15
    :cond_0
    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    invoke-virtual {p0, p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getThumbnailOffsetX(I)I

    move-result p2

    :goto_0
    int-to-float p2, p2

    .line 16
    iget-object p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    const/high16 v0, 0x40000000    # 2.0f

    const/4 v1, 0x0

    if-ne p1, p3, :cond_3

    .line 18
    iget p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    .line 20
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailWidth()I

    move-result v2

    .line 21
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailHeight()I

    move-result v3

    .line 22
    invoke-direct {p0, p3, v2, v3}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getRenderedThumbnailImageSize(III)Lcom/pspdfkit/utils/Size;

    move-result-object p3

    .line 26
    iget v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    goto :goto_1

    :cond_1
    if-eqz p3, :cond_2

    .line 31
    iget-boolean v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    if-eqz v2, :cond_2

    .line 32
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailWidth()I

    move-result v1

    int-to-float v1, v1

    iget p3, p3, Lcom/pspdfkit/utils/Size;->width:F

    sub-float/2addr v1, p3

    div-float/2addr v1, v0

    :cond_2
    :goto_1
    add-float/2addr p2, v1

    .line 38
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setTranslationX(F)V

    goto :goto_2

    .line 42
    :cond_3
    iget p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    .line 44
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSiblingSelectedThumbnailWidth()I

    move-result v2

    .line 45
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSiblingSelectedThumbnailHeight()I

    move-result v3

    .line 46
    invoke-direct {p0, p3, v2, v3}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getRenderedThumbnailImageSize(III)Lcom/pspdfkit/utils/Size;

    move-result-object p3

    if-eqz p3, :cond_4

    .line 51
    iget-boolean v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    if-eqz v2, :cond_4

    .line 52
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSiblingSelectedThumbnailWidth()I

    move-result v1

    int-to-float v1, v1

    iget p3, p3, Lcom/pspdfkit/utils/Size;->width:F

    sub-float/2addr v1, p3

    div-float/2addr v1, v0

    :cond_4
    sub-float/2addr p2, v1

    .line 56
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 59
    :goto_2
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    const/high16 p2, 0x3f800000    # 1.0f

    .line 60
    invoke-virtual {p1, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    const-wide/16 p2, 0x64

    .line 61
    invoke-virtual {p1, p2, p3}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    new-instance p2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 62
    invoke-virtual {p1, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    :cond_5
    return-void
.end method

.method synthetic lambda$updateCurrentlySelectedPageView$4$com-pspdfkit-ui-thumbnail-PdfStaticThumbnailBar()Lio/reactivex/rxjava3/core/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    iget-object v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    .line 3
    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 4
    :goto_0
    iget-boolean v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    .line 7
    iget v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    .line 8
    iget v4, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    goto :goto_1

    .line 10
    :cond_1
    iget v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    .line 11
    iget v4, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    .line 14
    :goto_1
    iget-object v5, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    .line 15
    invoke-direct {p0, v5, v3, v2, v1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->renderThumbnailIntoImageView(Landroid/widget/ImageView;IZZ)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v3

    iput-object v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImageRenderDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_2

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    .line 18
    invoke-direct {p0, v0, v4, v2, v1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->renderThumbnailIntoImageView(Landroid/widget/ImageView;IZZ)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImageRenderDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 20
    :cond_2
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailDisposables:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImageRenderDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImageRenderDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->detector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-eqz p1, :cond_9

    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    if-eqz p1, :cond_9

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ct;->a()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_9

    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailCount:I

    if-nez p1, :cond_0

    goto/16 :goto_3

    .line 8
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pinnedModeTopSeparator:Landroid/widget/FrameLayout;

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    :cond_1
    sub-int/2addr p4, p2

    .line 11
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->contentPaddingPx:I

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr p4, p1

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ct;->a()Ljava/util/ArrayList;

    move-result-object p1

    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailCount:I

    const/4 p3, 0x1

    sub-int/2addr p2, p3

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/lu;

    .line 16
    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->contentPaddingPx:I

    .line 17
    iget-object p5, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->layoutStyle:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    sget-object v0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;->PINNED:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    if-ne p5, v0, :cond_2

    iget p5, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->borderSizePx:I

    add-int/2addr p2, p5

    .line 20
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lu;->b()I

    move-result p5

    int-to-float p5, p5

    invoke-virtual {p1}, Lcom/pspdfkit/internal/lu;->c()Lcom/pspdfkit/utils/Size;

    move-result-object p1

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    add-float/2addr p5, p1

    float-to-int p1, p5

    sub-int/2addr p4, p1

    .line 21
    div-int/lit8 p4, p4, 0x2

    const/4 p1, 0x0

    const/4 p5, 0x0

    .line 23
    :goto_0
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailCount:I

    if-ge p5, v0, :cond_4

    .line 24
    invoke-virtual {p0, p5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    if-eq v0, v1, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    if-eq v0, v1, :cond_3

    .line 27
    sget v1, Lcom/pspdfkit/R$id;->pspdf__tag_key_thumbnail_position:I

    .line 28
    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/lu;

    .line 29
    iget v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->contentPaddingPx:I

    add-int/2addr v2, p4

    invoke-virtual {v1}, Lcom/pspdfkit/internal/lu;->b()I

    move-result v3

    add-int/2addr v3, v2

    int-to-float v2, v3

    .line 30
    invoke-virtual {v1}, Lcom/pspdfkit/internal/lu;->c()Lcom/pspdfkit/utils/Size;

    move-result-object v1

    iget v1, v1, Lcom/pspdfkit/utils/Size;->width:F

    add-float/2addr v2, v1

    float-to-int v1, v2

    .line 32
    iget v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailHeight:I

    add-int/2addr v2, p2

    .line 33
    invoke-virtual {v0, v3, p2, v1, v2}, Landroid/view/View;->layout(IIII)V

    :cond_3
    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    .line 38
    :cond_4
    iget-object p4, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    const/4 p5, -0x1

    if-eqz p4, :cond_8

    .line 39
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p4

    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailWidth()I

    move-result v0

    iput v0, p4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 40
    iget-object p4, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p4

    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailHeight()I

    move-result v0

    iput v0, p4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 43
    iget p4, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    sub-int/2addr p2, p4

    .line 44
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailWidth()I

    move-result p4

    add-int/2addr p4, p1

    .line 45
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSelectedThumbnailHeight()I

    move-result v0

    add-int/2addr v0, p2

    .line 46
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    invoke-virtual {v1, p1, p2, p4, v0}, Landroid/view/View;->layout(IIII)V

    .line 48
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->leftSelectedImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    .line 51
    :goto_1
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    if-eqz v2, :cond_6

    .line 52
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSiblingSelectedThumbnailWidth()I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 53
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSiblingSelectedThumbnailHeight()I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 55
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getSiblingSelectedThumbnailWidth()I

    move-result v3

    add-int/2addr v3, p4

    invoke-virtual {v2, p4, p2, v3, v0}, Landroid/view/View;->layout(IIII)V

    .line 57
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->rightSelectedImage:Landroid/widget/ImageView;

    .line 58
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result p2

    if-eqz p2, :cond_6

    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    if-eq p2, p5, :cond_6

    goto :goto_2

    :cond_6
    const/4 p3, 0x0

    :goto_2
    if-nez v1, :cond_7

    if-eqz p3, :cond_8

    .line 62
    :cond_7
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->updateCurrentlySelectedPageView()V

    .line 66
    :cond_8
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->layoutStyle:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    sget-object p3, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;->PINNED:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    if-ne p2, p3, :cond_9

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    if-lez p2, :cond_9

    .line 67
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->getPinnedModeTopSeparator()Landroid/view/View;

    move-result-object p2

    .line 70
    invoke-virtual {p0}, Landroid/view/ViewGroup;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p3

    invoke-virtual {p0, p2, p5, p3}, Landroid/view/ViewGroup;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 71
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    iget p4, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->borderSizePx:I

    invoke-virtual {p2, p1, p1, p3, p4}, Landroid/view/View;->layout(IIII)V

    :cond_9
    :goto_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-nez p2, :cond_0

    const/4 p1, 0x0

    .line 3
    invoke-virtual {p0, p1, p1}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void

    .line 7
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->availableWidthForThumbnailLayout:I

    .line 11
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->layoutStyle:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    sget-object v0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;->FLOATING:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    if-ne p2, v0, :cond_2

    .line 13
    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->contentPaddingPx:I

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    .line 14
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailWidth:I

    iget v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    add-int/2addr v1, v2

    mul-int v1, v1, v0

    add-int/2addr v1, p2

    sub-int/2addr v1, v2

    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->contentPaddingPx:I

    add-int/2addr v1, p2

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    if-eqz v0, :cond_1

    .line 19
    iget v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->availableWidthForThumbnailLayout:I

    mul-int/lit8 p2, p2, 0x2

    sub-int/2addr v2, p2

    .line 20
    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/ct;->b(I)V

    .line 21
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/ct;->a()Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_1

    .line 22
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/ct;->a()Ljava/util/ArrayList;

    move-result-object p2

    .line 23
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/lu;

    .line 24
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->contentPaddingPx:I

    mul-int/lit8 v0, v0, 0x2

    .line 25
    invoke-virtual {p2}, Lcom/pspdfkit/internal/lu;->b()I

    move-result v1

    add-int/2addr v1, v0

    int-to-float v0, v1

    .line 26
    invoke-virtual {p2}, Lcom/pspdfkit/internal/lu;->c()Lcom/pspdfkit/utils/Size;

    move-result-object p2

    iget p2, p2, Lcom/pspdfkit/utils/Size;->width:F

    add-float/2addr v0, p2

    float-to-int v1, v0

    :cond_1
    if-ge v1, p1, :cond_2

    move p1, v1

    .line 35
    :cond_2
    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->contentPaddingPx:I

    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailHeight:I

    add-int/2addr v0, p2

    add-int/2addr v0, p2

    .line 38
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->layoutStyle:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    sget-object v1, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;->PINNED:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    if-ne p2, v1, :cond_3

    .line 39
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result p2

    iget v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->borderSizePx:I

    add-int/2addr p2, v1

    add-int/2addr v0, p2

    .line 42
    :cond_3
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCallQueried:Z

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-eqz v0, :cond_1

    .line 3
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    if-ne p1, p2, :cond_0

    .line 4
    iput-boolean v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCallQueried:Z

    .line 5
    iput v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    :cond_0
    return-void

    .line 12
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageDeferRenderingInitializationDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_2

    .line 13
    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    .line 17
    :cond_2
    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    if-nez p2, :cond_5

    .line 19
    iput v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    .line 20
    iget-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    if-nez p2, :cond_4

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p1

    if-gt p1, v0, :cond_3

    goto :goto_0

    :cond_3
    const/4 v2, 0x1

    :cond_4
    :goto_0
    iput v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    goto :goto_1

    :cond_5
    if-ne p2, v0, :cond_7

    .line 21
    iget-boolean v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    if-nez v3, :cond_7

    .line 22
    iput v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    .line 23
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p1

    if-le p1, v0, :cond_6

    const/4 v2, 0x1

    :cond_6
    iput v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    goto :goto_1

    .line 25
    :cond_7
    iget-boolean v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    xor-int/2addr v3, v0

    .line 26
    rem-int/lit8 v4, p2, 0x2

    if-nez v4, :cond_8

    const/4 v1, 0x1

    :cond_8
    xor-int/2addr v1, v0

    xor-int/2addr v1, v3

    if-eqz v1, :cond_a

    .line 29
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    .line 31
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p1

    sub-int/2addr p1, v0

    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    if-le p1, p2, :cond_9

    add-int/lit8 v2, p2, 0x1

    :cond_9
    iput v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    goto :goto_1

    :cond_a
    add-int/lit8 p1, p2, -0x1

    .line 33
    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    .line 34
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    goto :goto_1

    .line 39
    :cond_b
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    .line 40
    iput v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    .line 43
    :goto_1
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->updateCurrentlySelectedPageView()V

    return-void
.end method

.method public onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPages:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4
    new-instance p1, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)V

    .line 17
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-wide/16 v0, 0x64

    .line 18
    invoke-virtual {p0, p1, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_1

    if-lez p1, :cond_1

    if-lez p2, :cond_1

    if-ne p3, p1, :cond_0

    if-eq p4, p2, :cond_1

    .line 2
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->refresh()V

    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->detector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 0

    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iput p1, v0, Lcom/pspdfkit/internal/bt;->a:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->refresh()V

    return-void
.end method

.method public setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 9

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 110
    :goto_0
    move-object v3, p1

    check-cast v3, Lcom/pspdfkit/internal/zf;

    iput-object v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    .line 111
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/x5;->c(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    move-result-object v3

    iput-object v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->pageRenderingConfiguration:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    .line 112
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPageBinding()Lcom/pspdfkit/document/PageBinding;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/document/PageBinding;->RIGHT_EDGE:Lcom/pspdfkit/document/PageBinding;

    if-ne v3, v4, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    iput-boolean v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    .line 114
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getExcludedAnnotationTypes()Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->excludedAnnotationTypes:Ljava/util/ArrayList;

    .line 115
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isFirstPageAlwaysSingle()Z

    move-result v3

    iput-boolean v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    .line 116
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p2, p1}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)Z

    move-result p2

    iput-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    if-eqz v0, :cond_4

    .line 120
    iput v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    .line 121
    iget-boolean p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    if-nez p2, :cond_3

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p2

    if-le p2, v1, :cond_3

    .line 122
    iput v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    goto :goto_2

    :cond_3
    const/4 p2, -0x1

    .line 124
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageSiblingIndex:I

    .line 128
    :cond_4
    :goto_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 129
    iput v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailCount:I

    .line 133
    new-instance v3, Lcom/pspdfkit/internal/ct;

    invoke-direct {v3, p1}, Lcom/pspdfkit/internal/ct;-><init>(Lcom/pspdfkit/document/PdfDocument;)V

    iput-object v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->staticThumbnailLayout:Lcom/pspdfkit/internal/ct;

    .line 134
    iget v4, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->thumbnailPaddingPx:I

    iget-boolean v5, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->useDoublePageMode:Z

    iget-boolean v6, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->isRTL:Z

    iget-boolean v7, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->firstPageSingle:Z

    iget-object v8, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    invoke-virtual/range {v3 .. v8}, Lcom/pspdfkit/internal/ct;->a(IZZZLcom/pspdfkit/internal/bt;)V

    .line 143
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$1;

    invoke-direct {p2, p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$1;-><init>(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)V

    invoke-virtual {p1, p2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method public setDrawableProviders(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->drawableProviders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->drawableProviders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->refresh()V

    return-void
.end method

.method public setLayoutStyle(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->layoutStyle:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$LayoutStyle;

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->refresh()V

    return-void
.end method

.method public setOnPageChangedListener(Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->onPageChangedListener:Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

    return-void
.end method

.method public setRedactionAnnotationPreviewEnabled(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->redactionAnnotationPreviewEnabled:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->redactionAnnotationPreviewEnabled:Z

    return-void
.end method

.method public setSelectedThumbnailBorderColor(I)V
    .locals 0

    return-void
.end method

.method public setThumbnailBorderColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iput p1, v0, Lcom/pspdfkit/internal/bt;->b:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->refresh()V

    return-void
.end method

.method public setThumbnailHeight(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iput p1, v0, Lcom/pspdfkit/internal/bt;->e:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->refresh()V

    return-void
.end method

.method public setThumbnailWidth(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iput p1, v0, Lcom/pspdfkit/internal/bt;->d:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->refresh()V

    return-void
.end method

.method public setUsePageAspectRatio(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/bt;

    iput-boolean p1, v0, Lcom/pspdfkit/internal/bt;->g:Z

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->refresh()V

    return-void
.end method

.method public show()V
    .locals 0

    return-void
.end method

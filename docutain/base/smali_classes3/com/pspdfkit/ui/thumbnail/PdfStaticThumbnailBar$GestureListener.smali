.class Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;-><init>(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)V

    return-void
.end method

.method private scrollToPage(II)Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;

    invoke-static {v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetdocument(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Lcom/pspdfkit/internal/zf;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;

    invoke-static {v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetstaticThumbnailLayout(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Lcom/pspdfkit/internal/ct;

    move-result-object v1

    if-nez v1, :cond_0

    goto/16 :goto_1

    :cond_0
    const/4 v3, 0x1

    if-ltz p2, :cond_1

    .line 4
    invoke-static {v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetcontentPaddingPx(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    invoke-static {v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetthumbnailHeight(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)I

    move-result v0

    add-int/2addr v4, v0

    if-gt p2, v4, :cond_1

    const/4 p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    if-nez p2, :cond_2

    return v2

    .line 12
    :cond_2
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ct;->a()Ljava/util/ArrayList;

    move-result-object p2

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;

    invoke-static {v0}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetthumbnailCount(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)I

    move-result v0

    sub-int/2addr v0, v3

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/lu;

    .line 14
    invoke-virtual {p2}, Lcom/pspdfkit/internal/lu;->b()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Lcom/pspdfkit/internal/lu;->c()Lcom/pspdfkit/utils/Size;

    move-result-object p2

    iget p2, p2, Lcom/pspdfkit/utils/Size;->width:F

    add-float/2addr v0, p2

    float-to-int p2, v0

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    sub-int/2addr v0, p2

    div-int/lit8 v0, v0, 0x2

    .line 21
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;

    invoke-static {v1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetdocument(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Lcom/pspdfkit/internal/zf;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v1

    int-to-float p2, p2

    int-to-float v4, v1

    div-float/2addr p2, v4

    sub-int/2addr p1, v0

    .line 26
    invoke-static {p1, v2}, Ljava/lang/Math;->max(II)I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p1, p2

    sub-int/2addr v1, v3

    int-to-float p2, v1

    invoke-static {p1, p2}, Ljava/lang/Math;->min(FF)F

    move-result p1

    float-to-int p1, p1

    .line 28
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;

    invoke-static {p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetisRTL(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 30
    invoke-static {p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetdocument(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Lcom/pspdfkit/internal/zf;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result p2

    sub-int/2addr p2, p1

    add-int/lit8 p1, p2, -0x1

    .line 34
    :cond_3
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;

    invoke-static {p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetuseDoublePageMode(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetfirstPageSingle(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Z

    move-result p2

    .line 35
    invoke-static {p1, p2, v2}, Lcom/pspdfkit/internal/vg;->a(IZZ)Z

    move-result p2

    if-nez p2, :cond_4

    if-lez p1, :cond_4

    add-int/lit8 p1, p1, -0x1

    .line 36
    :cond_4
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;

    iget v0, p2, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->currentPageIndex:I

    if-eq p1, v0, :cond_5

    invoke-static {p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetgotoPageCalledQueriedTargetIndex(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)I

    move-result v0

    if-eq v0, p1, :cond_5

    .line 37
    invoke-static {p2, p1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fputgotoPageCalledQueriedTargetIndex(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;I)V

    .line 38
    invoke-static {p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetonPageChangedListener(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 45
    invoke-static {p2, v2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fputgotoPageCallQueried(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;Z)V

    .line 46
    invoke-static {p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetdocument(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Lcom/pspdfkit/internal/zf;

    move-result-object v0

    invoke-virtual {p2, v0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V

    .line 47
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;

    invoke-static {p2, v3}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fputgotoPageCallQueried(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;Z)V

    .line 49
    invoke-static {p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;->-$$Nest$fgetonPageChangedListener(Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar;)Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

    move-result-object v0

    invoke-interface {v0, p2, p1}, Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;->onPageChanged(Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;I)V

    :cond_5
    return v3

    :cond_6
    :goto_1
    return v2
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    float-to-int p1, p1

    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;->scrollToPage(II)Z

    move-result p1

    return p1
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 0

    .line 1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    float-to-int p1, p1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result p2

    float-to-int p2, p2

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/thumbnail/PdfStaticThumbnailBar$GestureListener;->scrollToPage(II)Z

    move-result p1

    return p1
.end method

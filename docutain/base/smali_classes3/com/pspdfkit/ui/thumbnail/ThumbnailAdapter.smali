.class public Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;,
        Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;,
        Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$OnThumbnailClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "PSPDFKit.ThumbnailGrid"

.field private static final RADIUS_SELECTED_BORDER_ROUND_CORNERS_PX:F = 15.0f


# instance fields
.field private borderScaleFactor:F

.field private final context:Landroid/content/Context;

.field private final document:Lcom/pspdfkit/internal/zf;

.field private final drawableProviders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final excludedAnnotationTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private final fillPaint:Landroid/graphics/Paint;

.field private final firstPageSingle:Z

.field private final invertColors:Z

.field private final isRTL:Z

.field private final onThumbnailClickListener:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$OnThumbnailClickListener;

.field private final pageColor:I

.field private final pageRenderConfiguration:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

.field private radiusSelectedBorderRoundCornersScaleFactor:F

.field private redactionAnnotationPreviewEnabled:Z

.field private selectedBorderScaleFactor:F

.field private selectedPage:I

.field private final selectedStrokeWidth:I

.field private final themeConfig:Lcom/pspdfkit/internal/pq;

.field private thumbnailHeight:I

.field private final thumbnailMarginPx:I

.field private final thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

.field private final thumbnailStrokePaint:Landroid/graphics/Paint;

.field private thumbnailWidth:I

.field private final useDoublePageMode:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/zf;ILandroid/graphics/Paint;Landroid/graphics/Paint;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$OnThumbnailClickListener;Lcom/pspdfkit/internal/pq;Ljava/lang/Integer;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->drawableProviders:Ljava/util/List;

    const/4 v0, 0x0

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->redactionAnnotationPreviewEnabled:Z

    .line 18
    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->context:Landroid/content/Context;

    .line 19
    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    .line 20
    iput p3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailMarginPx:I

    .line 21
    invoke-static {p2, p6}, Lcom/pspdfkit/internal/x5;->c(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->pageRenderConfiguration:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    .line 22
    iget v1, p3, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->paperColor:I

    iput v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->pageColor:I

    .line 23
    iget-boolean p3, p3, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->invertColors:Z

    iput-boolean p3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->invertColors:Z

    .line 24
    new-instance p3, Ljava/util/ArrayList;

    invoke-virtual {p6}, Lcom/pspdfkit/configuration/PdfConfiguration;->getExcludedAnnotationTypes()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p3, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->excludedAnnotationTypes:Ljava/util/ArrayList;

    .line 26
    iput-object p4, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailStrokePaint:Landroid/graphics/Paint;

    .line 27
    iput-object p5, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

    .line 28
    new-instance p3, Landroid/graphics/Paint;

    invoke-direct {p3}, Landroid/graphics/Paint;-><init>()V

    iput-object p3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->fillPaint:Landroid/graphics/Paint;

    .line 29
    sget-object p4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, p4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 p4, 0x1

    .line 30
    invoke-virtual {p3, p4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 32
    iput-object p7, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->onThumbnailClickListener:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$OnThumbnailClickListener;

    .line 33
    invoke-virtual {p5}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result p3

    float-to-int p3, p3

    iput p3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedStrokeWidth:I

    .line 34
    invoke-static {p1, p6, p2}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)Z

    move-result p3

    iput-boolean p3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->useDoublePageMode:Z

    .line 35
    invoke-virtual {p6}, Lcom/pspdfkit/configuration/PdfConfiguration;->isFirstPageAlwaysSingle()Z

    move-result p3

    iput-boolean p3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    if-eqz p8, :cond_0

    goto :goto_0

    .line 37
    :cond_0
    new-instance p8, Lcom/pspdfkit/internal/pq;

    invoke-direct {p8, p1}, Lcom/pspdfkit/internal/pq;-><init>(Landroid/content/Context;)V

    :goto_0
    iput-object p8, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->themeConfig:Lcom/pspdfkit/internal/pq;

    if-eqz p9, :cond_1

    .line 39
    invoke-virtual {p9}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    .line 40
    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->getPageBinding()Lcom/pspdfkit/document/PageBinding;

    move-result-object p1

    sget-object p2, Lcom/pspdfkit/document/PageBinding;->RIGHT_EDGE:Lcom/pspdfkit/document/PageBinding;

    if-ne p1, p2, :cond_2

    const/4 v0, 0x1

    :cond_2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->isRTL:Z

    .line 42
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->applyTheme()V

    return-void
.end method

.method private applyTheme()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailStrokePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v1, v1, Lcom/pspdfkit/internal/pq;->a:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v1, v1, Lcom/pspdfkit/internal/pq;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v1, v0, Lcom/pspdfkit/internal/pq;->c:I

    iput v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailWidth:I

    .line 4
    iget v0, v0, Lcom/pspdfkit/internal/pq;->d:I

    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailHeight:I

    mul-int v1, v1, v0

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedBorderScaleFactor:F

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->borderScaleFactor:F

    const/high16 v0, 0x41700000    # 15.0f

    div-float/2addr v0, v1

    .line 10
    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->radiusSelectedBorderRoundCornersScaleFactor:F

    return-void
.end method

.method private getImageView(Landroid/widget/FrameLayout;)Landroid/widget/ImageView;
    .locals 4

    .line 1
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0x11

    .line 4
    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 5
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    .line 9
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 10
    invoke-static {v0}, Lcom/pspdfkit/internal/ov;->c(Landroid/view/View;)V

    .line 11
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private getPageSize(I)Lcom/pspdfkit/utils/Size;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    return-object p1
.end method

.method private getPdfDrawablesForGivenPage(Landroid/content/Context;I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawable;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v1, :cond_1

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->drawableProviders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;

    .line 5
    iget-object v3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    .line 6
    invoke-virtual {v2, p1, v3, p2}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->getDrawablesForPage(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 8
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 9
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private isPageSelected(I)Z
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->useDoublePageMode:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    if-ne p1, v2, :cond_1

    .line 4
    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    if-nez v0, :cond_1

    :goto_0
    const/4 p1, 0x0

    goto :goto_2

    .line 7
    :cond_1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    xor-int/2addr v0, v2

    .line 8
    rem-int/lit8 v3, p1, 0x2

    if-nez v3, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    xor-int/2addr v3, v2

    xor-int/2addr v0, v3

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 p1, p1, -0x1

    .line 19
    :cond_4
    :goto_2
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    if-ne p1, v0, :cond_5

    const/4 v1, 0x1

    :cond_5
    return v1
.end method

.method static synthetic lambda$logError$0(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.ThumbnailGrid"

    const-string v2, "Failed to render thumbnail image!"

    .line 1
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private logError()Lio/reactivex/rxjava3/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/functions/Consumer<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$$ExternalSyntheticLambda1;

    invoke-direct {v0}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$$ExternalSyntheticLambda1;-><init>()V

    return-object v0
.end method

.method private renderThumbnail(Landroid/widget/ImageView;IZ)Lio/reactivex/rxjava3/disposables/Disposable;
    .locals 23

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v15, p2

    .line 1
    iget-object v2, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v2, :cond_1

    iget v2, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailHeight:I

    if-nez v2, :cond_0

    goto/16 :goto_0

    .line 2
    :cond_0
    invoke-direct {v0, v15}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v2

    .line 3
    iget v3, v2, Lcom/pspdfkit/utils/Size;->width:F

    float-to-double v3, v3

    iget v2, v2, Lcom/pspdfkit/utils/Size;->height:F

    float-to-double v5, v2

    div-double/2addr v3, v5

    .line 6
    iget v2, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailHeight:I

    int-to-double v5, v2

    mul-double v5, v5, v3

    double-to-int v3, v5

    const/4 v4, 0x1

    .line 7
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 9
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    .line 10
    invoke-virtual/range {p1 .. p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v18

    .line 12
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v4

    sget v5, Lcom/pspdfkit/R$id;->pspdf__tag_key_bitmap:I

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/n4;->d(Landroid/graphics/Bitmap;)V

    .line 13
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v4

    invoke-virtual {v4, v3, v2}, Lcom/pspdfkit/internal/n4;->a(II)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 14
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v5

    invoke-virtual {v5, v3, v2}, Lcom/pspdfkit/internal/n4;->a(II)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 16
    sget v2, Lcom/pspdfkit/R$id;->pspdf__tag_key_bitmap:I

    invoke-virtual {v1, v2, v4}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 17
    sget v2, Lcom/pspdfkit/R$id;->pspdf__tag_key_page_index:I

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 19
    new-instance v2, Lcom/pspdfkit/internal/rc$a;

    iget-object v3, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    invoke-direct {v2, v3, v15}, Lcom/pspdfkit/internal/rc$a;-><init>(Lcom/pspdfkit/internal/zf;I)V

    const/4 v3, 0x3

    .line 20
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/g4$a;->c(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/rc$a;

    iget-object v3, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->pageRenderConfiguration:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    .line 21
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/rc$a;->b(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/rc$a;

    move-result-object v2

    .line 22
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/g4$a;->b(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/rc$a;

    .line 23
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/g4$a;->a(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/rc$a;

    const/4 v3, 0x0

    .line 24
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/lang/Integer;)Lcom/pspdfkit/internal/g4$a;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/rc$a;

    iget-object v3, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->excludedAnnotationTypes:Ljava/util/ArrayList;

    .line 25
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/g4$a;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/rc$a;

    iget-object v3, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->context:Landroid/content/Context;

    .line 26
    invoke-direct {v0, v3, v15}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->getPdfDrawablesForGivenPage(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/util/List;)Lcom/pspdfkit/internal/g4$a;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/rc$a;

    iget-boolean v3, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->redactionAnnotationPreviewEnabled:Z

    .line 27
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/g4$a;->b(Z)Lcom/pspdfkit/internal/g4$a;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/rc$a;

    .line 28
    invoke-virtual {v2}, Lcom/pspdfkit/internal/rc$a;->b()Lcom/pspdfkit/internal/rc;

    move-result-object v2

    .line 30
    new-instance v14, Ljava/lang/ref/WeakReference;

    invoke-direct {v14, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 31
    invoke-static {v2}, Lcom/pspdfkit/internal/hm;->a(Lcom/pspdfkit/internal/rc;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    .line 32
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/u;

    const/4 v4, 0x5

    .line 33
    invoke-virtual {v3, v4}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v3

    .line 34
    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v13

    new-instance v12, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;

    iget-object v3, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailStrokePaint:Landroid/graphics/Paint;

    iget-object v4, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

    iget-object v5, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->fillPaint:Landroid/graphics/Paint;

    .line 39
    invoke-direct {v0, v15}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->isPageSelected(I)Z

    move-result v6

    iget v8, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedBorderScaleFactor:F

    iget v9, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->borderScaleFactor:F

    iget v10, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->radiusSelectedBorderRoundCornersScaleFactor:F

    iget-object v11, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    iget-boolean v2, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->useDoublePageMode:Z

    iget-boolean v1, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    iget-boolean v15, v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->isRTL:Z

    move/from16 v19, v2

    move-object v2, v12

    move-object/from16 v20, v11

    move/from16 v11, p2

    move-object v0, v12

    move-object/from16 v12, v20

    move-object/from16 v21, v13

    move/from16 v13, v19

    move-object/from16 v22, v14

    move v14, v1

    move/from16 v1, p2

    invoke-direct/range {v2 .. v15}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;-><init>(Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;ZLandroid/graphics/Bitmap;FFFILcom/pspdfkit/document/PdfDocument;ZZZ)V

    move-object/from16 v2, v21

    .line 40
    invoke-virtual {v2, v0}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v2, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;

    .line 55
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move-object v4, v2

    move/from16 v6, p3

    move-wide/from16 v7, v16

    move-object/from16 v9, v18

    invoke-direct/range {v4 .. v9}, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;-><init>(Landroid/content/res/Resources;ZJLandroid/graphics/drawable/Drawable;)V

    .line 56
    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 58
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    move-object/from16 v2, p0

    move-object/from16 v3, v22

    .line 59
    invoke-direct {v2, v1, v3}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->setImageDrawable(ILjava/lang/ref/WeakReference;)Lio/reactivex/rxjava3/functions/Consumer;

    move-result-object v1

    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->logError()Lio/reactivex/rxjava3/functions/Consumer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    move-object v2, v0

    .line 60
    invoke-static {}, Lio/reactivex/rxjava3/disposables/Disposable$-CC;->disposed()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method private setImageDrawable(ILjava/lang/ref/WeakReference;)Lio/reactivex/rxjava3/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/widget/ImageView;",
            ">;)",
            "Lio/reactivex/rxjava3/functions/Consumer<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p2, p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;Ljava/lang/ref/WeakReference;I)V

    return-object v0
.end method

.method private setMarginsDoublePageMode(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;I)V
    .locals 3

    const/4 v0, 0x0

    if-nez p2, :cond_1

    .line 1
    iget-boolean v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v2

    invoke-static {p2, v2, v1}, Lcom/pspdfkit/internal/vg;->a(IIZ)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetroot(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/FrameLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailMarginPx:I

    invoke-virtual {p1, v0, v0, p2, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 4
    :cond_0
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetroot(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/FrameLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 6
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p2, v1, :cond_3

    .line 7
    iget-boolean v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v2

    invoke-static {p2, v2, v1}, Lcom/pspdfkit/internal/vg;->a(IIZ)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 8
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetroot(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/FrameLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailMarginPx:I

    invoke-virtual {p1, p2, v0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 10
    :cond_2
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetroot(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/FrameLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 12
    :cond_3
    iget-boolean v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    iget-boolean v2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->isRTL:Z

    invoke-static {p2, v1, v2}, Lcom/pspdfkit/internal/vg;->a(IZZ)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 13
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetroot(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/FrameLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailMarginPx:I

    invoke-virtual {p1, p2, v0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 15
    :cond_4
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetroot(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/FrameLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailMarginPx:I

    invoke-virtual {p1, v0, v0, p2, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    :goto_0
    return-void
.end method

.method private setMarginsSinglePageMode(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetroot(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/FrameLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    invoke-virtual {p1, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_0

    :cond_0
    if-nez p2, :cond_1

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetroot(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/FrameLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailMarginPx:I

    invoke-virtual {p1, v2, v2, p2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    sub-int/2addr v0, v1

    if-ne p2, v0, :cond_2

    .line 8
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetroot(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/FrameLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailMarginPx:I

    invoke-virtual {p1, p2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 10
    :cond_2
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetroot(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/FrameLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailMarginPx:I

    .line 11
    invoke-virtual {p1, p2, v2, p2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    :goto_0
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    return v0
.end method

.method getSelectedPage()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    return v0
.end method

.method public getSelectedThumbnailBorderColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v0, v0, Lcom/pspdfkit/internal/pq;->b:I

    return v0
.end method

.method public getThumbnailBorderColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v0, v0, Lcom/pspdfkit/internal/pq;->a:I

    return v0
.end method

.method public getThumbnailHeight()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v0, v0, Lcom/pspdfkit/internal/pq;->d:I

    return v0
.end method

.method public getThumbnailWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v0, v0, Lcom/pspdfkit/internal/pq;->c:I

    return v0
.end method

.method isRedactionAnnotationPreviewEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->redactionAnnotationPreviewEnabled:Z

    return v0
.end method

.method synthetic lambda$setImageDrawable$1$com-pspdfkit-ui-thumbnail-ThumbnailAdapter(Ljava/lang/ref/WeakReference;ILandroid/graphics/drawable/Drawable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    .line 2
    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3
    iget-boolean p3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->useDoublePageMode:Z

    if-eqz p3, :cond_2

    .line 4
    iget-boolean p3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    .line 6
    invoke-static {p2, v0, p3}, Lcom/pspdfkit/internal/vg;->a(IIZ)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 9
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 p2, 0x11

    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0

    .line 10
    :cond_0
    iget-boolean p3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->isRTL:Z

    invoke-static {p2, p3, v0}, Lcom/pspdfkit/internal/vg;->a(IZZ)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 13
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout$LayoutParams;

    const p2, 0x800015

    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0

    .line 18
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout$LayoutParams;

    const p2, 0x800013

    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    :cond_2
    :goto_0
    return-void
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->onBindViewHolder(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;I)V
    .locals 13

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetrenderThumbnail(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 4
    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->useDoublePageMode:Z

    if-eqz v0, :cond_0

    .line 5
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->setMarginsDoublePageMode(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;I)V

    goto :goto_0

    .line 7
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->setMarginsSinglePageMode(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;I)V

    .line 10
    :goto_0
    invoke-direct {p0, p2}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v0

    .line 11
    invoke-direct {p0, p2}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->isPageSelected(I)Z

    move-result v7

    .line 13
    iget-boolean v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->useDoublePageMode:Z

    const/4 v10, 0x1

    const/4 v11, 0x0

    if-eqz v1, :cond_3

    .line 15
    iget-boolean v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v2

    invoke-static {p2, v2, v1}, Lcom/pspdfkit/internal/vg;->a(IIZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x11

    const/4 v2, 0x0

    :goto_1
    const/4 v3, 0x0

    goto :goto_2

    .line 20
    :cond_1
    iget-boolean v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    iget-boolean v2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->isRTL:Z

    invoke-static {p2, v1, v2}, Lcom/pspdfkit/internal/vg;->a(IZZ)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x800015

    const/4 v2, 0x0

    const/4 v3, 0x1

    goto :goto_2

    :cond_2
    const v1, 0x800013

    const/4 v2, 0x1

    goto :goto_1

    .line 32
    :goto_2
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetthumbnail(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout$LayoutParams;

    iput v1, v4, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    move v8, v2

    move v9, v3

    goto :goto_3

    :cond_3
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 35
    :goto_3
    new-instance v12, Lcom/pspdfkit/internal/eq;

    .line 36
    iget-boolean v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->invertColors:Z

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->pageColor:I

    invoke-static {v1}, Lcom/pspdfkit/internal/ga;->c(I)I

    move-result v1

    goto :goto_4

    :cond_4
    iget v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->pageColor:I

    :goto_4
    move v2, v1

    iget v1, v0, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v3, v1

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int v4, v0

    if-eqz v7, :cond_5

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

    goto :goto_5

    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailStrokePaint:Landroid/graphics/Paint;

    :goto_5
    move-object v5, v0

    iget-object v6, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

    move-object v1, v12

    invoke-direct/range {v1 .. v9}, Lcom/pspdfkit/internal/eq;-><init>(IIILandroid/graphics/Paint;Landroid/graphics/Paint;ZZZ)V

    .line 46
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetthumbnail(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 47
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetthumbnail(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->context:Landroid/content/Context;

    .line 48
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__page_with_number:I

    new-array v3, v10, [Ljava/lang/Object;

    add-int/lit8 v4, p2, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v11

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 51
    invoke-static {p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fgetthumbnail(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-direct {p0, v0, p2, v10}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->renderThumbnail(Landroid/widget/ImageView;IZ)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->-$$Nest$fputrenderThumbnail(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;Lio/reactivex/rxjava3/disposables/Disposable;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;
    .locals 3

    .line 2
    new-instance p1, Landroid/widget/FrameLayout;

    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->context:Landroid/content/Context;

    invoke-direct {p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 3
    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedStrokeWidth:I

    mul-int/lit8 p2, p2, 0x2

    .line 4
    new-instance v0, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    iget v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailWidth:I

    add-int/2addr v1, p2

    iget v2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailHeight:I

    add-int/2addr v2, p2

    invoke-direct {v0, v1, v2}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;-><init>(II)V

    .line 6
    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->thumbnailMarginPx:I

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1, p2, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 7
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 8
    new-instance p2, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->getImageView(Landroid/widget/FrameLayout;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->onThumbnailClickListener:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$OnThumbnailClickListener;

    invoke-direct {p2, p1, v0, v1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;-><init>(Landroid/widget/FrameLayout;Landroid/widget/ImageView;Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$OnThumbnailClickListener;)V

    return-object p2
.end method

.method public selectPage(I)V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->useDoublePageMode:Z

    if-eqz v0, :cond_4

    .line 2
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    .line 3
    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    .line 4
    iget-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    .line 5
    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v1

    .line 6
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/internal/vg;->a(IIZ)Z

    move-result p1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 8
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    goto :goto_0

    .line 9
    :cond_0
    iget-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    .line 10
    invoke-static {v0, p1, v1}, Lcom/pspdfkit/internal/vg;->a(IZZ)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 11
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    add-int/lit8 v0, v0, 0x1

    .line 12
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    goto :goto_0

    .line 14
    :cond_1
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    add-int/lit8 v0, v0, -0x1

    .line 15
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 18
    :goto_0
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->document:Lcom/pspdfkit/internal/zf;

    .line 19
    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v2

    .line 20
    invoke-static {p1, v2, v0}, Lcom/pspdfkit/internal/vg;->a(IIZ)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 22
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    goto :goto_1

    .line 23
    :cond_2
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->firstPageSingle:Z

    .line 24
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/vg;->a(IZZ)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 25
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 26
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    goto :goto_1

    .line 28
    :cond_3
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 29
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    goto :goto_1

    .line 33
    :cond_4
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    .line 34
    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    .line 35
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 36
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectedPage:I

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    :goto_1
    return-void
.end method

.method public setDrawableProviders(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;)V"
        }
    .end annotation

    const-string v0, "drawableProviders"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->drawableProviders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->drawableProviders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 56
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method setRedactionAnnotationPreviewEnabled(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->redactionAnnotationPreviewEnabled:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->redactionAnnotationPreviewEnabled:Z

    return-void
.end method

.method public setSelectedThumbnailBorderColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->themeConfig:Lcom/pspdfkit/internal/pq;

    iput p1, v0, Lcom/pspdfkit/internal/pq;->b:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->applyTheme()V

    return-void
.end method

.method public setThumbnailBorderColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->themeConfig:Lcom/pspdfkit/internal/pq;

    iput p1, v0, Lcom/pspdfkit/internal/pq;->a:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->applyTheme()V

    return-void
.end method

.method public setThumbnailHeight(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->themeConfig:Lcom/pspdfkit/internal/pq;

    iput p1, v0, Lcom/pspdfkit/internal/pq;->d:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->applyTheme()V

    return-void
.end method

.method public setThumbnailWidth(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->themeConfig:Lcom/pspdfkit/internal/pq;

    iput p1, v0, Lcom/pspdfkit/internal/pq;->c:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->applyTheme()V

    return-void
.end method

.class Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;

.field final synthetic val$configuration:Lcom/pspdfkit/configuration/PdfConfiguration;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$1;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$1;->val$configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$1;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$1;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;

    invoke-static {v0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->-$$Nest$mapplyTheme(Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$1;->this$0:Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$1;->val$configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-static {v0, v1}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->-$$Nest$mcreateThumbnailsAfterLayout(Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    return-void
.end method

.class public Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;
.super Landroidx/recyclerview/widget/LinearLayoutManager;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;
    }
.end annotation


# instance fields
.field private final smoothScroller:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;

    invoke-direct {v0, p1}, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;->smoothScroller:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 4
    new-instance p2, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;

    invoke-direct {p2, p1}, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;->smoothScroller:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .line 5
    invoke-direct {p0, p1, p2, p3, p4}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 6
    new-instance p2, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;

    invoke-direct {p2, p1}, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;->smoothScroller:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;

    return-void
.end method

.method private isFast(II)Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/LinearLayoutManager;->getWidth()I

    move-result v0

    div-int/2addr v0, p2

    mul-int/lit8 v0, v0, 0x2

    .line 2
    invoke-virtual {p0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result p2

    sub-int/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    if-le p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private smoothScroll(IZ)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;->smoothScroller:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;->setDoublePageSkew(Z)V

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;->smoothScroller:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;

    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/LinearSmoothScroller;->setTargetPosition(I)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;->smoothScroller:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager$CenterSmoothScroller;

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->startSmoothScroll(Landroidx/recyclerview/widget/RecyclerView$SmoothScroller;)V

    return-void
.end method


# virtual methods
.method public smartScrollToPosition(IILandroidx/recyclerview/widget/RecyclerView;Z)V
    .locals 1

    .line 1
    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView;->stopScroll()V

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;->isFast(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4
    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    move-result p3

    const/4 v0, 0x2

    div-int/2addr p3, v0

    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :cond_0
    div-int/2addr p2, v0

    sub-int/2addr p3, p2

    .line 5
    invoke-virtual {p0, p1, p3}, Landroidx/recyclerview/widget/LinearLayoutManager;->scrollToPositionWithOffset(II)V

    goto :goto_0

    .line 8
    :cond_1
    invoke-direct {p0, p1, p4}, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;->smoothScroll(IZ)V

    :goto_0
    return-void
.end method

.method public smoothScrollToPosition(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;I)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/LinearLayoutManager;->isSmoothScrolling()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x0

    .line 5
    invoke-direct {p0, p3, p1}, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;->smoothScroll(IZ)V

    return-void
.end method

.method public supportsPredictiveItemAnimations()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

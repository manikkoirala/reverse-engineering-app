.class Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ThumbnailViewHolder"
.end annotation


# instance fields
.field private final onThumbnailClickListener:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$OnThumbnailClickListener;

.field private renderThumbnail:Lio/reactivex/rxjava3/disposables/Disposable;

.field private final root:Landroid/widget/FrameLayout;

.field private final thumbnail:Landroid/widget/ImageView;


# direct methods
.method static bridge synthetic -$$Nest$fgetrenderThumbnail(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Lio/reactivex/rxjava3/disposables/Disposable;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->renderThumbnail:Lio/reactivex/rxjava3/disposables/Disposable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetroot(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/FrameLayout;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->root:Landroid/widget/FrameLayout;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetthumbnail(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->thumbnail:Landroid/widget/ImageView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputrenderThumbnail(Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->renderThumbnail:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method constructor <init>(Landroid/widget/FrameLayout;Landroid/widget/ImageView;Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$OnThumbnailClickListener;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->root:Landroid/widget/FrameLayout;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->thumbnail:Landroid/widget/ImageView;

    .line 4
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5
    iput-object p3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->onThumbnailClickListener:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$OnThumbnailClickListener;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->thumbnail:Landroid/widget/ImageView;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__tag_key_page_index:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->onThumbnailClickListener:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$OnThumbnailClickListener;

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$ThumbnailViewHolder;->thumbnail:Landroid/widget/ImageView;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__tag_key_page_index:I

    .line 3
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 4
    invoke-interface {v0, p1, v1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$OnThumbnailClickListener;->onThumbnailChanged(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

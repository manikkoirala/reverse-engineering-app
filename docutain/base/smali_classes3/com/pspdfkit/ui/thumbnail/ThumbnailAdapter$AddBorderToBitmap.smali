.class final Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AddBorderToBitmap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Function<",
        "Landroid/graphics/Bitmap;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final borderScaleFactor:F

.field private final document:Lcom/pspdfkit/document/PdfDocument;

.field private final doublePageMode:Z

.field private final fillPaint:Landroid/graphics/Paint;

.field private final firstSinglePage:Z

.field private final isRTL:Z

.field private final output:Landroid/graphics/Bitmap;

.field private final pageIndex:I

.field private final radiusSelectedBorderRoundCornersScaleFactor:F

.field private final selected:Z

.field private final selectedBorderScaleFactor:F

.field private final selectedStrokePaint:Landroid/graphics/Paint;

.field private final strokePaint:Landroid/graphics/Paint;


# direct methods
.method constructor <init>(Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;ZLandroid/graphics/Bitmap;FFFILcom/pspdfkit/document/PdfDocument;ZZZ)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->strokePaint:Landroid/graphics/Paint;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->selectedStrokePaint:Landroid/graphics/Paint;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->fillPaint:Landroid/graphics/Paint;

    .line 5
    iput-boolean p4, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->selected:Z

    .line 6
    iput-object p5, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->output:Landroid/graphics/Bitmap;

    .line 7
    iput p6, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->selectedBorderScaleFactor:F

    .line 8
    iput p7, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->borderScaleFactor:F

    .line 9
    iput p8, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->radiusSelectedBorderRoundCornersScaleFactor:F

    .line 10
    iput p9, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->pageIndex:I

    .line 11
    iput-object p10, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->document:Lcom/pspdfkit/document/PdfDocument;

    .line 12
    iput-boolean p11, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->doublePageMode:Z

    .line 13
    iput-boolean p12, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->firstSinglePage:Z

    .line 14
    iput-boolean p13, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->isRTL:Z

    return-void
.end method


# virtual methods
.method public apply(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 12

    .line 2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int v1, v1, v0

    int-to-float v0, v1

    .line 4
    iget v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->selectedBorderScaleFactor:F

    mul-float v1, v1, v0

    float-to-int v1, v1

    int-to-double v2, v1

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    mul-double v2, v2, v4

    double-to-int v2, v2

    .line 6
    iget v3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->borderScaleFactor:F

    mul-float v3, v3, v0

    float-to-int v3, v3

    .line 7
    iget v4, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->radiusSelectedBorderRoundCornersScaleFactor:F

    mul-float v0, v0, v4

    float-to-int v0, v0

    .line 10
    new-instance v4, Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->output:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 11
    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    const/4 v8, 0x0

    invoke-direct {v5, v8, v8, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 12
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6, v5}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 13
    new-instance v7, Landroid/graphics/Rect;

    .line 16
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    sub-int/2addr v9, v2

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    sub-int/2addr v10, v2

    invoke-direct {v7, v2, v2, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 18
    new-instance v9, Landroid/graphics/RectF;

    invoke-direct {v9, v7}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 21
    iget-boolean v10, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->selected:Z

    if-eqz v10, :cond_0

    int-to-float v10, v0

    .line 22
    iget-object v11, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v6, v10, v10, v11}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 27
    :cond_0
    iget-object v10, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, p1, v5, v7, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 30
    new-instance p1, Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->strokePaint:Landroid/graphics/Paint;

    invoke-direct {p1, v5}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    int-to-float v3, v3

    .line 31
    invoke-virtual {p1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 32
    invoke-virtual {v4, v9, p1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 33
    iget-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->selected:Z

    if-eqz p1, :cond_1

    .line 34
    new-instance p1, Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->selectedStrokePaint:Landroid/graphics/Paint;

    invoke-direct {p1, v3}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    int-to-float v1, v1

    .line 35
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    int-to-float v0, v0

    .line 36
    invoke-virtual {v4, v6, v0, v0, p1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 43
    :cond_1
    iget-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->doublePageMode:Z

    if-eqz p1, :cond_4

    .line 44
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->document:Lcom/pspdfkit/document/PdfDocument;

    if-eqz p1, :cond_2

    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->pageIndex:I

    iget-boolean v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->firstSinglePage:Z

    .line 46
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p1

    .line 47
    invoke-static {v0, p1, v1}, Lcom/pspdfkit/internal/vg;->a(IIZ)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 50
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->output:Landroid/graphics/Bitmap;

    return-object p1

    .line 51
    :cond_2
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->pageIndex:I

    iget-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->firstSinglePage:Z

    iget-boolean v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->isRTL:Z

    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/vg;->a(IZZ)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 53
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->output:Landroid/graphics/Bitmap;

    .line 54
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int/2addr v0, v2

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->output:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 55
    invoke-static {p1, v8, v8, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1

    .line 59
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->output:Landroid/graphics/Bitmap;

    .line 63
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int/2addr v0, v2

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->output:Landroid/graphics/Bitmap;

    .line 64
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 65
    invoke-static {p1, v2, v8, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1

    .line 74
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->output:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$AddBorderToBitmap;->apply(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

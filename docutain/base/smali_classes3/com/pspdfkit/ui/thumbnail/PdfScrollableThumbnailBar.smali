.class public Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;
.super Lcom/pspdfkit/internal/views/utils/a;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;
.implements Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$OnThumbnailClickListener;
.implements Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$SavedState;
    }
.end annotation


# static fields
.field private static final PAGE_INDEX_NONE:I = -0x1

.field private static final THUMBNAIL_MARGIN_DP:I = 0x4

.field private static final THUMBNAIL_RENDERING_DEBOUNCE_MS:I = 0x64

.field private static final THUMBNAIL_SELECTED_BORDER_DP:I = 0x4

.field private static final VIEW_PADDING_DP:I = 0x4


# instance fields
.field private adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

.field private final adapterBehaviorProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/processors/BehaviorProcessor<",
            "Lcom/pspdfkit/internal/jl<",
            "Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;",
            ">;>;"
        }
    .end annotation
.end field

.field private backgroundColor:I

.field private configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

.field currentPageIndex:I

.field private final dirtyPages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final dirtyPagesRunnables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private document:Lcom/pspdfkit/internal/zf;

.field private final drawableProvidersProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/processors/BehaviorProcessor<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;>;"
        }
    .end annotation
.end field

.field private firstPageSingle:Z

.field private gotoPageCallQueried:Z

.field private gotoPageCalledQueriedTargetIndex:I

.field private layoutManager:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;

.field private onPageChangedListener:Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private savedLayoutManagerState:Landroid/os/Parcelable;

.field private scrollingToPage:Ljava/lang/Integer;

.field private selectedStrokeWidth:I

.field private themeConfig:Lcom/pspdfkit/internal/pq;

.field private thumbnailHeight:I

.field private thumbnailMarginPx:I

.field private thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

.field private thumbnailStrokePaint:Landroid/graphics/Paint;

.field private thumbnailWidth:I

.field private useDoublePageMode:Z

.field private viewPaddingPx:I


# direct methods
.method static bridge synthetic -$$Nest$mapplyTheme(Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->applyTheme()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcreateThumbnailsAfterLayout(Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->createThumbnailsAfterLayout(Lcom/pspdfkit/configuration/PdfConfiguration;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__scrollableThumbnailBarStyle:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/pspdfkit/internal/views/utils/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    .line 2
    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    const/4 v1, 0x0

    .line 5
    iput-boolean v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCallQueried:Z

    .line 7
    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    .line 10
    iput-boolean v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->useDoublePageMode:Z

    .line 11
    iput-boolean v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->firstPageSingle:Z

    .line 13
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPages:Ljava/util/Set;

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    .line 33
    iput v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->backgroundColor:I

    .line 35
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->drawableProvidersProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 36
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapterBehaviorProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 40
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 41
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__scrollableThumbnailBarStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/internal/views/utils/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, -0x1

    .line 42
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    const/4 v0, 0x0

    .line 45
    iput-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCallQueried:Z

    .line 47
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    .line 50
    iput-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->useDoublePageMode:Z

    .line 51
    iput-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->firstPageSingle:Z

    .line 53
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPages:Ljava/util/Set;

    .line 56
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    .line 73
    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->backgroundColor:I

    .line 75
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->drawableProvidersProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 76
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapterBehaviorProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 85
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 86
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/views/utils/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, -0x1

    .line 87
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    const/4 p3, 0x0

    .line 90
    iput-boolean p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCallQueried:Z

    .line 92
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    .line 95
    iput-boolean p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->useDoublePageMode:Z

    .line 96
    iput-boolean p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->firstPageSingle:Z

    .line 98
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPages:Ljava/util/Set;

    .line 101
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    .line 118
    iput p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->backgroundColor:I

    .line 120
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->drawableProvidersProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 121
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapterBehaviorProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 136
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .line 137
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/views/utils/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p2, -0x1

    .line 138
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    const/4 p3, 0x0

    .line 141
    iput-boolean p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCallQueried:Z

    .line 143
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    .line 146
    iput-boolean p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->useDoublePageMode:Z

    .line 147
    iput-boolean p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->firstPageSingle:Z

    .line 149
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPages:Ljava/util/Set;

    .line 152
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    .line 169
    iput p3, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->backgroundColor:I

    .line 171
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->drawableProvidersProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 172
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapterBehaviorProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 196
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method private applyTheme()V
    .locals 4

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->setBackgroundColor(I)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailStrokePaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v2, v2, Lcom/pspdfkit/internal/pq;->a:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v2, v2, Lcom/pspdfkit/internal/pq;->b:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v2, v1, Lcom/pspdfkit/internal/pq;->c:I

    iput v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailWidth:I

    .line 5
    iget v2, v1, Lcom/pspdfkit/internal/pq;->d:I

    iput v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailHeight:I

    .line 10
    iget-boolean v1, v1, Lcom/pspdfkit/internal/pq;->e:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v1, :cond_1

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 12
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 13
    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v2

    .line 14
    iget v3, v2, Lcom/pspdfkit/utils/Size;->width:F

    iget v2, v2, Lcom/pspdfkit/utils/Size;->height:F

    div-float/2addr v3, v2

    .line 15
    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 17
    :cond_0
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailHeight:I

    int-to-float v0, v0

    mul-float v0, v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailWidth:I

    .line 18
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iput v0, v1, Lcom/pspdfkit/internal/pq;->c:I

    .line 21
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->createThumbnailsAfterLayout(Lcom/pspdfkit/configuration/PdfConfiguration;)V

    .line 22
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method private createThumbnailsAfterLayout(Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->removeChildViews()V

    .line 4
    new-instance v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    iget v4, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailMarginPx:I

    iget-object v5, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailStrokePaint:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

    iget-object v9, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget-object v10, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->scrollingToPage:Ljava/lang/Integer;

    move-object v1, v0

    move-object v7, p1

    move-object v8, p0

    invoke-direct/range {v1 .. v10}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/zf;ILandroid/graphics/Paint;Landroid/graphics/Paint;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter$OnThumbnailClickListener;Lcom/pspdfkit/internal/pq;Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->layoutManager:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageBinding()Lcom/pspdfkit/document/PageBinding;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/document/PageBinding;->RIGHT_EDGE:Lcom/pspdfkit/document/PageBinding;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->setReverseLayout(Z)V

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapterBehaviorProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    new-instance v0, Lcom/pspdfkit/internal/jl;

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/jl;-><init>(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->onNext(Ljava/lang/Object;)V

    .line 20
    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v0, -0x2

    const/4 v1, -0x1

    invoke-direct {p1, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p0, v0, v2, p1, v2}, Landroid/view/ViewGroup;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 22
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method private getCombiner()Lio/reactivex/rxjava3/functions/BiFunction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/functions/BiFunction<",
            "Lcom/pspdfkit/internal/jl<",
            "Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;",
            "Landroid/util/Pair<",
            "Lcom/pspdfkit/internal/jl<",
            "Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;>;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$$ExternalSyntheticLambda1;

    invoke-direct {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$$ExternalSyntheticLambda1;-><init>()V

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 4

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__static_thumbnail_bar:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    .line 4
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailStrokePaint:Landroid/graphics/Paint;

    .line 5
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailStrokePaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailStrokePaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 10
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

    .line 11
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40800000    # 4.0f

    mul-float p1, p1, v2

    const/high16 v2, 0x40000000    # 2.0f

    mul-float v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    float-to-int p1, p1

    .line 16
    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailMarginPx:I

    .line 17
    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->viewPaddingPx:I

    const/4 p1, 0x0

    .line 19
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 21
    new-instance v0, Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 22
    new-instance v0, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->layoutManager:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;

    .line 23
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 24
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->layoutManager:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 25
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setOverScrollMode(I)V

    .line 28
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailSelectedStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->selectedStrokeWidth:I

    .line 30
    new-instance p1, Lcom/pspdfkit/internal/pq;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/pspdfkit/internal/pq;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    .line 31
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->applyTheme()V

    .line 34
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapterBehaviorProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->drawableProvidersProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->getCombiner()Lio/reactivex/rxjava3/functions/BiFunction;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lio/reactivex/rxjava3/core/Flowable;->combineLatest(Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lio/reactivex/rxjava3/functions/BiFunction;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    .line 35
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->setDrawableProvidersToAdapter()Lio/reactivex/rxjava3/functions/Consumer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Flowable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method static lambda$setDrawableProvidersToAdapter$0(Landroid/util/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/pspdfkit/internal/jl;

    .line 2
    iget-object v0, v0, Lcom/pspdfkit/internal/jl;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    .line 3
    check-cast v0, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    iget-object p0, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p0, Ljava/util/List;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->setDrawableProviders(Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method private notifyPageUpdated(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    :cond_0
    return-void
.end method

.method private removeChildViews()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 3
    iput-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapterBehaviorProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    new-instance v2, Lcom/pspdfkit/internal/jl;

    invoke-direct {v2, v1}, Lcom/pspdfkit/internal/jl;-><init>(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private restoreLayoutManagerPosition()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->savedLayoutManagerState:Landroid/os/Parcelable;

    if-eqz v0, :cond_2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->savedLayoutManagerState:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->savedLayoutManagerState:Landroid/os/Parcelable;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->scrollingToPage:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->layoutManager:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastCompletelyVisibleItemPosition()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->layoutManager:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;

    .line 7
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    if-le v0, v1, :cond_1

    .line 8
    :cond_0
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailWidth:I

    iget v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailMarginPx:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->selectedStrokeWidth:I

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->layoutManager:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;

    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->scrollingToPage:Ljava/lang/Integer;

    .line 10
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-boolean v4, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->useDoublePageMode:Z

    .line 11
    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;->smartScrollToPosition(IILandroidx/recyclerview/widget/RecyclerView;Z)V

    :cond_1
    const/4 v0, 0x1

    return v0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private setDrawableProvidersToAdapter()Lio/reactivex/rxjava3/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/functions/Consumer<",
            "Landroid/util/Pair<",
            "Lcom/pspdfkit/internal/jl<",
            "Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;>;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$$ExternalSyntheticLambda1;

    invoke-direct {v0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$$ExternalSyntheticLambda1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 0

    return-void
.end method

.method public clearDocument()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    .line 2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->fitSystemWindows(Landroid/graphics/Rect;)Z

    const/4 p1, 0x0

    return p1
.end method

.method public getBackgroundColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->backgroundColor:I

    return v0
.end method

.method public getDocumentListener()Lcom/pspdfkit/listeners/DocumentListener;
    .locals 0

    return-object p0
.end method

.method public getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->getItemCount()I

    move-result v0

    return v0
.end method

.method getOnPageChangedListener()Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->onPageChangedListener:Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

    return-object v0
.end method

.method public getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_THUMBNAIL_BAR:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    return-object v0
.end method

.method public getSelectedPage()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->getSelectedPage()I

    move-result v0

    return v0
.end method

.method public getSelectedThumbnailBorderColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v0, v0, Lcom/pspdfkit/internal/pq;->b:I

    return v0
.end method

.method public getThumbnailBorderColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v0, v0, Lcom/pspdfkit/internal/pq;->a:I

    return v0
.end method

.method public getThumbnailHeight()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v0, v0, Lcom/pspdfkit/internal/pq;->d:I

    return v0
.end method

.method public getThumbnailWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget v0, v0, Lcom/pspdfkit/internal/pq;->c:I

    return v0
.end method

.method public hide()V
    .locals 0

    return-void
.end method

.method public isBackgroundTransparent()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isDisplayed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isRedactionAnnotationPreviewEnabled()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->isRedactionAnnotationPreviewEnabled()Z

    move-result v0

    return v0
.end method

.method public isUsingPageAspectRatio()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iget-boolean v0, v0, Lcom/pspdfkit/internal/pq;->e:Z

    return v0
.end method

.method synthetic lambda$onPageUpdated$1$com-pspdfkit-ui-thumbnail-PdfScrollableThumbnailBar()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPages:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->notifyPageUpdated(I)V

    goto :goto_0

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPages:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 8
    invoke-virtual {p0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 10
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-nez p1, :cond_0

    return-void

    :cond_0
    sub-int/2addr p4, p2

    sub-int/2addr p5, p3

    const/4 p1, 0x0

    .line 7
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    if-ge p1, p2, :cond_1

    .line 8
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p2

    .line 9
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    sub-int p3, p4, p3

    .line 10
    div-int/lit8 p3, p3, 0x2

    .line 11
    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->viewPaddingPx:I

    sub-int v1, p4, p3

    sub-int v2, p5, v0

    invoke-virtual {p2, p3, v0, v1, v2}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 3
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 4
    invoke-virtual {p0, v2, p1, p2}, Landroid/view/ViewGroup;->measureChild(Landroid/view/View;II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6
    :cond_0
    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailHeight:I

    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->viewPaddingPx:I

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p2

    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->selectedStrokeWidth:I

    mul-int/lit8 p2, p2, 0x2

    add-int/2addr p2, v0

    const/high16 v0, 0x40000000    # 2.0f

    .line 8
    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 9
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->restoreLayoutManagerPosition()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 6
    :cond_0
    iget-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCallQueried:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    .line 8
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    if-ne p1, p2, :cond_1

    .line 9
    iput-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCallQueried:Z

    const/4 p1, -0x1

    .line 10
    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    :cond_1
    return-void

    .line 16
    :cond_2
    iget-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->useDoublePageMode:Z

    if-eqz p1, :cond_7

    if-nez p2, :cond_3

    .line 18
    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    goto :goto_0

    :cond_3
    const/4 v1, 0x1

    if-ne p2, v1, :cond_4

    .line 19
    iget-boolean v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->firstPageSingle:Z

    if-nez v2, :cond_4

    .line 20
    iput v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    goto :goto_0

    .line 22
    :cond_4
    iget-boolean v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->firstPageSingle:Z

    xor-int/2addr v2, v1

    .line 23
    rem-int/lit8 v3, p2, 0x2

    if-nez v3, :cond_5

    const/4 v0, 0x1

    :cond_5
    xor-int/2addr v0, v1

    xor-int/2addr v0, v2

    if-eqz v0, :cond_6

    .line 26
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    goto :goto_0

    :cond_6
    sub-int/2addr p2, v1

    .line 28
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    goto :goto_0

    .line 33
    :cond_7
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    .line 36
    :goto_0
    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailWidth:I

    iget v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->thumbnailMarginPx:I

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p2

    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->selectedStrokeWidth:I

    mul-int/lit8 p2, p2, 0x2

    add-int/2addr p2, v0

    .line 37
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->layoutManager:Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;

    iget v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    iget-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1, p2, v2, p1}, Lcom/pspdfkit/ui/thumbnail/ScrollableThumbnailBarLayoutManager;->smartScrollToPosition(IILandroidx/recyclerview/widget/RecyclerView;Z)V

    .line 38
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    if-eqz p1, :cond_8

    .line 39
    iget p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    invoke-virtual {p1, p2}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->selectPage(I)V

    goto :goto_1

    .line 41
    :cond_8
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->scrollingToPage:Ljava/lang/Integer;

    :goto_1
    return-void
.end method

.method public onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPages:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4
    new-instance p1, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;)V

    .line 17
    iget-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->dirtyPagesRunnables:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-wide/16 v0, 0x64

    .line 18
    invoke-virtual {p0, p1, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$SavedState;

    if-nez v0, :cond_0

    .line 2
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 6
    :cond_0
    check-cast p1, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$SavedState;

    .line 7
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 8
    iget v0, p1, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$SavedState;->selectedPosition:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->scrollingToPage:Ljava/lang/Integer;

    .line 9
    iget-object p1, p1, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$SavedState;->layoutManager:Landroid/os/Parcelable;

    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->savedLayoutManagerState:Landroid/os/Parcelable;

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 1
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$SavedState;

    invoke-direct {v1, v0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->getSelectedPage()I

    move-result v0

    iput v0, v1, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$SavedState;->selectedPosition:I

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, v1, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$SavedState;->layoutManager:Landroid/os/Parcelable;

    :cond_1
    return-object v1
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    if-eqz v0, :cond_1

    if-lez p1, :cond_1

    if-lez p2, :cond_1

    if-ne p3, p1, :cond_0

    if-eq p4, p2, :cond_1

    .line 2
    :cond_0
    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->createThumbnailsAfterLayout(Lcom/pspdfkit/configuration/PdfConfiguration;)V

    :cond_1
    return-void
.end method

.method public onThumbnailChanged(Landroid/view/View;I)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->isAnimating()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    iget-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->useDoublePageMode:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->firstPageSingle:Z

    .line 6
    invoke-static {p2, p1, v0}, Lcom/pspdfkit/internal/vg;->a(IZZ)Z

    move-result p1

    if-nez p1, :cond_1

    if-lez p2, :cond_1

    add-int/lit8 p2, p2, -0x1

    .line 7
    :cond_1
    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    if-eq p2, p1, :cond_2

    iget p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    if-eq p1, p2, :cond_2

    .line 8
    iput p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCalledQueriedTargetIndex:I

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->onPageChangedListener:Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

    if-eqz p1, :cond_2

    .line 15
    iput-boolean v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCallQueried:Z

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V

    const/4 p1, 0x1

    .line 17
    iput-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->gotoPageCallQueried:Z

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->onPageChangedListener:Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

    invoke-interface {p1, p0, p2}, Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;->onPageChanged(Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;I)V

    :cond_2
    :goto_0
    return-void
.end method

.method public removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 0

    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->backgroundColor:I

    .line 2
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    return-void
.end method

.method public setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 3

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x0

    if-eq v0, p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 110
    :goto_0
    move-object v2, p1

    check-cast v2, Lcom/pspdfkit/internal/zf;

    iput-object v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->document:Lcom/pspdfkit/internal/zf;

    .line 111
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isFirstPageAlwaysSingle()Z

    move-result v2

    iput-boolean v2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->firstPageSingle:Z

    .line 112
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p2, p1}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->useDoublePageMode:Z

    .line 113
    iput-object p2, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    if-eqz v0, :cond_2

    .line 117
    iput v1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->currentPageIndex:I

    .line 120
    :cond_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 121
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p1

    if-lez p1, :cond_3

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p1

    if-lez p1, :cond_3

    .line 122
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->applyTheme()V

    .line 123
    invoke-direct {p0, p2}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->createThumbnailsAfterLayout(Lcom/pspdfkit/configuration/PdfConfiguration;)V

    goto :goto_1

    .line 125
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$1;

    invoke-direct {v0, p0, p2}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar$1;-><init>(Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :goto_1
    return-void
.end method

.method public setDrawableProviders(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->drawableProvidersProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public setOnPageChangedListener(Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->onPageChangedListener:Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;

    return-void
.end method

.method public setRedactionAnnotationPreviewEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->setRedactionAnnotationPreviewEnabled(Z)V

    :cond_0
    return-void
.end method

.method public setSelectedThumbnailBorderColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iput p1, v0, Lcom/pspdfkit/internal/pq;->b:I

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->setSelectedThumbnailBorderColor(I)V

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->applyTheme()V

    return-void
.end method

.method public setThumbnailBorderColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iput p1, v0, Lcom/pspdfkit/internal/pq;->a:I

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->setThumbnailBorderColor(I)V

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->applyTheme()V

    return-void
.end method

.method public setThumbnailHeight(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iput p1, v0, Lcom/pspdfkit/internal/pq;->d:I

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->setThumbnailHeight(I)V

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->applyTheme()V

    return-void
.end method

.method public setThumbnailWidth(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iput p1, v0, Lcom/pspdfkit/internal/pq;->c:I

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->adapter:Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/thumbnail/ThumbnailAdapter;->setThumbnailWidth(I)V

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->applyTheme()V

    return-void
.end method

.method public setUsePageAspectRatio(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->themeConfig:Lcom/pspdfkit/internal/pq;

    iput-boolean p1, v0, Lcom/pspdfkit/internal/pq;->e:Z

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/thumbnail/PdfScrollableThumbnailBar;->applyTheme()V

    return-void
.end method

.method public show()V
    .locals 0

    return-void
.end method

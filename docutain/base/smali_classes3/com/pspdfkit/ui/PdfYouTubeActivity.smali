.class public Lcom/pspdfkit/ui/PdfYouTubeActivity;
.super Lcom/google/android/youtube/player/YouTubeBaseActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/player/YouTubePlayer$OnInitializedListener;


# static fields
.field public static final ARG_MEDIA_URI:Ljava/lang/String; = "PSPDFKit.MediaURI"


# instance fields
.field private mediaUri:Lcom/pspdfkit/media/MediaUri;

.field playerProgressBar:Landroid/widget/ProgressBar;

.field playerView:Lcom/google/android/youtube/player/YouTubePlayerView;

.field videoSettings:Lcom/pspdfkit/media/MediaLinkUtils$VideoSettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/android/youtube/player/YouTubeBaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/google/android/youtube/player/YouTubeBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 2
    sget p1, Lcom/pspdfkit/R$layout;->pspdf__you_tube_activity:I

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setContentView(I)V

    .line 4
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "PSPDFKit.MediaURI"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/media/MediaUri;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfYouTubeActivity;->mediaUri:Lcom/pspdfkit/media/MediaUri;

    .line 6
    sget p1, Lcom/pspdfkit/R$id;->pspdf__youtube_progressbar:I

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfYouTubeActivity;->playerProgressBar:Landroid/widget/ProgressBar;

    .line 7
    sget p1, Lcom/pspdfkit/R$id;->pspdf__youtube_player:I

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/youtube/player/YouTubePlayerView;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfYouTubeActivity;->playerView:Lcom/google/android/youtube/player/YouTubePlayerView;

    .line 9
    invoke-static {}, Lcom/pspdfkit/internal/gj;->m()Lcom/pspdfkit/internal/dj;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/dj;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfYouTubeActivity;->playerView:Lcom/google/android/youtube/player/YouTubePlayerView;

    invoke-virtual {v0, p1, p0}, Lcom/google/android/youtube/player/YouTubePlayerView;->initialize(Ljava/lang/String;Lcom/google/android/youtube/player/YouTubePlayer$OnInitializedListener;)V

    goto :goto_0

    .line 13
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void
.end method

.method public onInitializationFailure(Lcom/google/android/youtube/player/YouTubePlayer$Provider;Lcom/google/android/youtube/player/YouTubeInitializationResult;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "YouTubeAPI Initialization Failed."

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    .line 2
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public onInitializationSuccess(Lcom/google/android/youtube/player/YouTubePlayer$Provider;Lcom/google/android/youtube/player/YouTubePlayer;Z)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfYouTubeActivity;->playerProgressBar:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    if-nez p3, :cond_2

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfYouTubeActivity;->mediaUri:Lcom/pspdfkit/media/MediaUri;

    invoke-virtual {p1}, Lcom/pspdfkit/media/MediaUri;->getVideoSettingsFromOptions()Lcom/pspdfkit/media/MediaLinkUtils$VideoSettings;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfYouTubeActivity;->videoSettings:Lcom/pspdfkit/media/MediaLinkUtils$VideoSettings;

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfYouTubeActivity;->mediaUri:Lcom/pspdfkit/media/MediaUri;

    invoke-virtual {p1}, Lcom/pspdfkit/media/MediaUri;->getParsedUri()Landroid/net/Uri;

    move-result-object p1

    const-string p3, "v"

    invoke-virtual {p1, p3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 5
    iget-object p3, p0, Lcom/pspdfkit/ui/PdfYouTubeActivity;->videoSettings:Lcom/pspdfkit/media/MediaLinkUtils$VideoSettings;

    iget-boolean v0, p3, Lcom/pspdfkit/media/MediaLinkUtils$VideoSettings;->autoplay:Z

    if-eqz v0, :cond_1

    .line 6
    iget p3, p3, Lcom/pspdfkit/media/MediaLinkUtils$VideoSettings;->offset:I

    mul-int/lit16 p3, p3, 0x3e8

    invoke-interface {p2, p1, p3}, Lcom/google/android/youtube/player/YouTubePlayer;->loadVideo(Ljava/lang/String;I)V

    goto :goto_0

    .line 8
    :cond_1
    iget p3, p3, Lcom/pspdfkit/media/MediaLinkUtils$VideoSettings;->offset:I

    mul-int/lit16 p3, p3, 0x3e8

    invoke-interface {p2, p1, p3}, Lcom/google/android/youtube/player/YouTubePlayer;->cueVideo(Ljava/lang/String;I)V

    :cond_2
    :goto_0
    return-void
.end method

.class Lcom/pspdfkit/ui/PdfFragment$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ag;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/PdfFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/PdfFragment;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfFragment$3;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addUserInterfaceListener(Lcom/pspdfkit/internal/ev;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment$3;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fgetuserInterfaceListeners(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public getDocumentListeners()Lcom/pspdfkit/internal/nh;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/listeners/DocumentListener;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment$3;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fgetdocumentListeners(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    return-object v0
.end method

.method public getPasteManager()Lcom/pspdfkit/internal/h7;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment$3;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fgetpasteManager(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/um;

    move-result-object v0

    return-object v0
.end method

.method public getViewCoordinator()Lcom/pspdfkit/internal/xm;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment$3;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fgetviewCoordinator(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/xm;

    move-result-object v0

    return-object v0
.end method

.method public isLastViewedPageRestorationActiveAndIsConfigChange()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment$3;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fgetlastViewedPageRestorationDisposable(Lcom/pspdfkit/ui/PdfFragment;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public removeUserInterfaceListener(Lcom/pspdfkit/internal/ev;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment$3;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fgetuserInterfaceListeners(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public setDocument(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment$3;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$mresetDocument(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfFragment$3;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    check-cast p1, Lcom/pspdfkit/internal/zf;

    invoke-static {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fputdocument(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/zf;)V

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fgetinternalDocumentListener(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/ui/PdfFragment$InternalDocumentListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/zf;->a(Lcom/pspdfkit/internal/zf$f;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment$3;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {p1}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fgetviewCoordinator(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/xm;

    move-result-object p1

    const/4 v0, 0x0

    .line 6
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/xm;->a(Z)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfFragment$3;->this$0:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {p1}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$fgetdocument(Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/zf;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->-$$Nest$mdisplayDocument(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/zf;)V

    :cond_0
    return-void
.end method

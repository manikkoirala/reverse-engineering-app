.class public interface abstract Lcom/pspdfkit/ui/PdfUi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/listeners/PdfActivityListener;
.implements Lcom/pspdfkit/internal/pm$b;
.implements Lcom/pspdfkit/internal/pm$c;
.implements Lcom/pspdfkit/ui/PdfActivityComponentsApi;


# static fields
.field public static final TIMEOUT_DEFAULT:J = 0x0L

.field public static final TIMEOUT_INFINITE:J = 0x7fffffffffffffffL


# virtual methods
.method public abstract addPropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
.end method

.method public abstract getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
.end method

.method public abstract getDocument()Lcom/pspdfkit/document/PdfDocument;
.end method

.method public abstract getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;
.end method

.method public abstract getImplementation()Lcom/pspdfkit/internal/ui/f;
.end method

.method public abstract getPSPDFKitViews()Lcom/pspdfkit/ui/PSPDFKitViews;
.end method

.method public abstract getPageIndex()I
.end method

.method public abstract getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;
.end method

.method public abstract getPropertyInspectorCoordinator()Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;
.end method

.method public abstract getScreenTimeout()J
.end method

.method public abstract getSiblingPageIndex(I)I
.end method

.method public abstract getUserInterfaceViewMode()Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;
.end method

.method public abstract hideUserInterface()V
.end method

.method public abstract isDocumentInteractionEnabled()Z
.end method

.method public abstract isImageDocument()Z
.end method

.method public abstract isUserInterfaceEnabled()Z
.end method

.method public abstract isUserInterfaceVisible()Z
.end method

.method public abstract synthetic onGenerateMenuItemIds(Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract synthetic onGetShowAsAction(II)I
.end method

.method public abstract removePropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
.end method

.method public abstract requirePdfFragment()Lcom/pspdfkit/ui/PdfFragment;
.end method

.method public abstract setAnnotationCreationInspectorController(Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;)V
.end method

.method public abstract setAnnotationEditingInspectorController(Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;)V
.end method

.method public abstract setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
.end method

.method public abstract setDocumentFromDataProvider(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)V
.end method

.method public abstract setDocumentFromDataProviders(Ljava/util/List;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/providers/DataProvider;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setDocumentFromUri(Landroid/net/Uri;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract setDocumentFromUris(Ljava/util/List;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setDocumentInteractionEnabled(Z)V
.end method

.method public abstract setDocumentPrintDialogFactory(Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;)V
.end method

.method public abstract setDocumentSharingDialogFactory(Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;)V
.end method

.method public abstract setOnContextualToolbarLifecycleListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarLifecycleListener;)V
.end method

.method public abstract setOnContextualToolbarMovementListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarMovementListener;)V
.end method

.method public abstract setOnContextualToolbarPositionListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;)V
.end method

.method public abstract setPageIndex(I)V
.end method

.method public abstract setPageIndex(IZ)V
.end method

.method public abstract setPrintOptionsProvider(Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V
.end method

.method public abstract setScreenTimeout(J)V
.end method

.method public abstract setSharingActionMenuListener(Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;)V
.end method

.method public abstract setSharingOptionsProvider(Lcom/pspdfkit/document/sharing/SharingOptionsProvider;)V
.end method

.method public abstract setUserInterfaceEnabled(Z)V
.end method

.method public abstract setUserInterfaceViewMode(Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;)V
.end method

.method public abstract setUserInterfaceVisible(ZZ)V
.end method

.method public abstract showUserInterface()V
.end method

.method public abstract toggleUserInterface()V
.end method

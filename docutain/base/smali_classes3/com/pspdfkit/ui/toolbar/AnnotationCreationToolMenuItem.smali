.class final enum Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum ARROW_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum CAMERA_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum CIRCLE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum CLOUDY_CIRCLE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum CLOUDY_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum CLOUDY_POLYGON_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum CLOUDY_SQUARE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum DASHED_CIRCLE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum DASHED_POLYGON_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum DASHED_SQUARE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum ERASER_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum FREETEXT_CALLOUT_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum FREETEXT_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum HIGHLIGHTER:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum HIGHLIGHT_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum IMAGE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum INSTANT_COMMENT_MARKER_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum INSTANT_HIGHLIGHT_COMMENT_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum LINE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum MAGIC_INK_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum MEASUREMENT_AREA_ELLIPSE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum MEASUREMENT_AREA_POLYGON_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum MEASUREMENT_AREA_RECT_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum MEASUREMENT_DISTANCE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum MEASUREMENT_PERIMETER_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum NOTE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum PEN_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum POLYGON_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum POLYLINE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum REDACTION_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum SIGNATURE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum SOUND_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum SQUARE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum SQUIGGLY_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum STAMP_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum STRIKEOUT_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

.field public static final enum UNDERLINE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;


# instance fields
.field final annotationTool:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field final annotationToolVariant:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

.field final drawableId:I

.field final id:I

.field final isStyleIndicatorEnabled:Z

.field final stringId:I

.field final styleableId:I


# direct methods
.method static constructor <clinit>()V
    .locals 52

    .line 1
    new-instance v8, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->HIGHLIGHT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_highlight:I

    sget v5, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__highlightIcon:I

    sget v6, Lcom/pspdfkit/R$drawable;->pspdf__ic_highlight:I

    sget v7, Lcom/pspdfkit/R$string;->pspdf__edit_menu_highlight:I

    const-string v1, "HIGHLIGHT_ITEM"

    const/4 v2, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v8, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->HIGHLIGHT_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 7
    new-instance v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v12, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SQUIGGLY:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v13, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_squiggly:I

    sget v14, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__squigglyIcon:I

    sget v15, Lcom/pspdfkit/R$drawable;->pspdf__ic_squiggly:I

    sget v16, Lcom/pspdfkit/R$string;->pspdf__edit_menu_squiggly:I

    const-string v10, "SQUIGGLY_ITEM"

    const/4 v11, 0x1

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->SQUIGGLY_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 13
    new-instance v1, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v20, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->STRIKEOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v21, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_strikeout:I

    sget v22, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__strikeoutIcon:I

    sget v23, Lcom/pspdfkit/R$drawable;->pspdf__ic_strikeout:I

    sget v24, Lcom/pspdfkit/R$string;->pspdf__edit_menu_strikeout:I

    const-string v18, "STRIKEOUT_ITEM"

    const/16 v19, 0x2

    move-object/from16 v17, v1

    invoke-direct/range {v17 .. v24}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v1, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->STRIKEOUT_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 19
    new-instance v2, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v12, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->UNDERLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v13, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_underline:I

    sget v14, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__underlineIcon:I

    sget v15, Lcom/pspdfkit/R$drawable;->pspdf__ic_underline:I

    sget v16, Lcom/pspdfkit/R$string;->pspdf__edit_menu_underline:I

    const-string v10, "UNDERLINE_ITEM"

    const/4 v11, 0x3

    move-object v9, v2

    invoke-direct/range {v9 .. v16}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v2, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->UNDERLINE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 25
    new-instance v3, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v20, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v21, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_note:I

    sget v22, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__noteIcon:I

    sget v23, Lcom/pspdfkit/R$drawable;->pspdf__ic_note:I

    sget v24, Lcom/pspdfkit/R$string;->pspdf__edit_menu_note:I

    const-string v18, "NOTE_ITEM"

    const/16 v19, 0x4

    move-object/from16 v17, v3

    invoke-direct/range {v17 .. v24}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v3, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->NOTE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 31
    new-instance v4, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v12, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->FREETEXT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v13, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_freetext:I

    sget v14, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__freeTextIcon:I

    sget v15, Lcom/pspdfkit/R$drawable;->pspdf__ic_freetext:I

    sget v16, Lcom/pspdfkit/R$string;->pspdf__edit_menu_freetext:I

    const-string v10, "FREETEXT_ITEM"

    const/4 v11, 0x5

    move-object v9, v4

    invoke-direct/range {v9 .. v16}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v4, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->FREETEXT_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 37
    new-instance v5, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v20, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->FREETEXT_CALLOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v21, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_freetext_callout:I

    sget v22, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__freeTextCalloutIcon:I

    sget v23, Lcom/pspdfkit/R$drawable;->pspdf__ic_freetext_callout:I

    sget v24, Lcom/pspdfkit/R$string;->pspdf__edit_menu_callout:I

    sget-object v6, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->CALLOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 43
    invoke-static {v6}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v25

    const-string v18, "FREETEXT_CALLOUT_ITEM"

    const/16 v19, 0x6

    const/16 v26, 0x0

    move-object/from16 v17, v5

    invoke-direct/range {v17 .. v26}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    sput-object v5, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->FREETEXT_CALLOUT_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 45
    new-instance v6, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v12, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SIGNATURE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v13, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_signature:I

    sget v14, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__signatureIcon:I

    sget v15, Lcom/pspdfkit/R$drawable;->pspdf__ic_signature:I

    sget v16, Lcom/pspdfkit/R$string;->pspdf__signature:I

    const-string v10, "SIGNATURE_ITEM"

    const/4 v11, 0x7

    move-object v9, v6

    invoke-direct/range {v9 .. v16}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v6, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->SIGNATURE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 51
    new-instance v7, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v12, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v21, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_ink_pen:I

    sget v22, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__inkPenIcon:I

    sget v23, Lcom/pspdfkit/R$drawable;->pspdf__ic_stylus:I

    sget v24, Lcom/pspdfkit/R$string;->pspdf__edit_menu_ink_pen:I

    sget-object v9, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->PEN:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 57
    invoke-static {v9}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v25

    const-string v18, "PEN_ITEM"

    const/16 v19, 0x8

    const/16 v26, 0x1

    move-object/from16 v17, v7

    move-object/from16 v20, v12

    invoke-direct/range {v17 .. v26}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    sput-object v7, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->PEN_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 59
    new-instance v19, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget v13, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_ink_highlighter:I

    sget v14, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__inkHighlighterIcon:I

    sget v15, Lcom/pspdfkit/R$drawable;->pspdf__ic_ink_highlighter:I

    sget v16, Lcom/pspdfkit/R$string;->pspdf__edit_menu_ink_highlighter:I

    sget-object v9, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->HIGHLIGHTER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 65
    invoke-static {v9}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v17

    const-string v10, "HIGHLIGHTER"

    const/16 v11, 0x9

    const/16 v18, 0x1

    move-object/from16 v9, v19

    invoke-direct/range {v9 .. v18}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    sput-object v19, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->HIGHLIGHTER:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 67
    new-instance v9, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v23, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MAGIC_INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v24, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_magic_ink:I

    sget v25, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__magicInkIcon:I

    sget v26, Lcom/pspdfkit/R$drawable;->pspdf__ic_magic_ink:I

    sget v27, Lcom/pspdfkit/R$string;->pspdf__edit_menu_magic_ink:I

    sget-object v10, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->MAGIC:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 73
    invoke-static {v10}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v28

    const-string v21, "MAGIC_INK_ITEM"

    const/16 v22, 0xa

    const/16 v29, 0x0

    move-object/from16 v20, v9

    invoke-direct/range {v20 .. v29}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    sput-object v9, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->MAGIC_INK_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 75
    new-instance v18, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v23, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->LINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v14, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_line:I

    sget v15, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__lineIcon:I

    sget v16, Lcom/pspdfkit/R$drawable;->pspdf__ic_line:I

    sget v17, Lcom/pspdfkit/R$string;->pspdf__annotation_type_line:I

    const-string v11, "LINE_ITEM"

    const/16 v12, 0xb

    move-object/from16 v10, v18

    move-object/from16 v13, v23

    invoke-direct/range {v10 .. v17}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v18, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->LINE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 81
    new-instance v10, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget v24, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_line_arrow:I

    sget v25, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__lineArrowIcon:I

    sget v26, Lcom/pspdfkit/R$drawable;->pspdf__ic_line_arrow:I

    sget v27, Lcom/pspdfkit/R$string;->pspdf__edit_menu_line_arrow:I

    sget-object v11, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->ARROW:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 87
    invoke-static {v11}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v28

    const-string v21, "ARROW_ITEM"

    const/16 v22, 0xc

    move-object/from16 v20, v10

    invoke-direct/range {v20 .. v29}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    sput-object v10, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->ARROW_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 89
    new-instance v11, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v12, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SQUARE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v34, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_square:I

    sget v35, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__squareIcon:I

    sget v36, Lcom/pspdfkit/R$drawable;->pspdf__ic_square:I

    sget v37, Lcom/pspdfkit/R$string;->pspdf__annotation_type_square:I

    const-string v31, "SQUARE_ITEM"

    const/16 v32, 0xd

    move-object/from16 v30, v11

    move-object/from16 v33, v12

    invoke-direct/range {v30 .. v37}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v11, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->SQUARE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 95
    new-instance v13, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget v24, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_dashed_square:I

    sget v25, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__dashedSquareIcon:I

    sget v26, Lcom/pspdfkit/R$drawable;->pspdf__ic_dashed_square:I

    sget v27, Lcom/pspdfkit/R$string;->pspdf__annotation_type_dashed_rectangle:I

    sget-object v14, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->DASHED:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 101
    invoke-static {v14}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v28

    const-string v21, "DASHED_SQUARE_ITEM"

    const/16 v22, 0xe

    move-object/from16 v20, v13

    move-object/from16 v23, v12

    invoke-direct/range {v20 .. v29}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    sput-object v13, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->DASHED_SQUARE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 103
    new-instance v15, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget v24, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_cloudy_square:I

    sget v25, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__cloudySquareIcon:I

    sget v26, Lcom/pspdfkit/R$drawable;->pspdf__ic_cloudy_square:I

    sget v27, Lcom/pspdfkit/R$string;->pspdf__annotation_type_cloudy_rectangle:I

    sget-object v16, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->CLOUDY:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    .line 109
    invoke-static/range {v16 .. v16}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v28

    const-string v21, "CLOUDY_SQUARE_ITEM"

    const/16 v22, 0xf

    move-object/from16 v20, v15

    invoke-direct/range {v20 .. v29}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    sput-object v15, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->CLOUDY_SQUARE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 111
    new-instance v12, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v17, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->CIRCLE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v34, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_circle:I

    sget v35, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__circleIcon:I

    sget v36, Lcom/pspdfkit/R$drawable;->pspdf__ic_circle:I

    sget v37, Lcom/pspdfkit/R$string;->pspdf__annotation_type_circle:I

    const-string v31, "CIRCLE_ITEM"

    const/16 v32, 0x10

    move-object/from16 v30, v12

    move-object/from16 v33, v17

    invoke-direct/range {v30 .. v37}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v12, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->CIRCLE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 117
    new-instance v30, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget v24, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_dashed_circle:I

    sget v25, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__dashedCircleIcon:I

    sget v26, Lcom/pspdfkit/R$drawable;->pspdf__ic_dashed_circle:I

    sget v27, Lcom/pspdfkit/R$string;->pspdf__annotation_type_dashed_ellipse:I

    .line 123
    invoke-static {v14}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v28

    const-string v21, "DASHED_CIRCLE_ITEM"

    const/16 v22, 0x11

    move-object/from16 v20, v30

    move-object/from16 v23, v17

    invoke-direct/range {v20 .. v29}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    sput-object v30, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->DASHED_CIRCLE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 125
    new-instance v31, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget v24, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_cloudy_circle:I

    sget v25, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__cloudyCircleIcon:I

    sget v26, Lcom/pspdfkit/R$drawable;->pspdf__ic_cloudy_circle:I

    sget v27, Lcom/pspdfkit/R$string;->pspdf__annotation_type_cloudy_ellipse:I

    .line 131
    invoke-static/range {v16 .. v16}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v28

    const-string v21, "CLOUDY_CIRCLE_ITEM"

    const/16 v22, 0x12

    move-object/from16 v20, v31

    invoke-direct/range {v20 .. v29}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    sput-object v31, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->CLOUDY_CIRCLE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 133
    new-instance v17, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v40, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->POLYGON:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v36, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_polygon:I

    sget v37, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__polygonIcon:I

    sget v38, Lcom/pspdfkit/R$drawable;->pspdf__ic_polygon:I

    sget v39, Lcom/pspdfkit/R$string;->pspdf__annotation_type_polygon:I

    const-string v33, "POLYGON_ITEM"

    const/16 v34, 0x13

    move-object/from16 v32, v17

    move-object/from16 v35, v40

    invoke-direct/range {v32 .. v39}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v17, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->POLYGON_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 139
    new-instance v32, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget v24, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_dashed_polygon:I

    sget v25, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__dashedPolygonIcon:I

    sget v26, Lcom/pspdfkit/R$drawable;->pspdf__ic_dashed_polygon:I

    sget v27, Lcom/pspdfkit/R$string;->pspdf__annotation_type_dashed_polygon:I

    .line 145
    invoke-static {v14}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v28

    const-string v21, "DASHED_POLYGON_ITEM"

    const/16 v22, 0x14

    move-object/from16 v20, v32

    move-object/from16 v23, v40

    invoke-direct/range {v20 .. v29}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    sput-object v32, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->DASHED_POLYGON_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 147
    new-instance v14, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget v24, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_cloudy_polygon:I

    sget v25, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__cloudyPolygonIcon:I

    sget v26, Lcom/pspdfkit/R$drawable;->pspdf__ic_cloudy_polygon:I

    sget v27, Lcom/pspdfkit/R$string;->pspdf__annotation_type_cloudy_polygon:I

    .line 153
    invoke-static/range {v16 .. v16}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v28

    const-string v21, "CLOUDY_POLYGON_ITEM"

    const/16 v22, 0x15

    move-object/from16 v20, v14

    invoke-direct/range {v20 .. v29}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    sput-object v14, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->CLOUDY_POLYGON_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 158
    new-instance v33, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget v24, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_cloudy:I

    sget v25, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__cloudyPolygonIcon:I

    sget v26, Lcom/pspdfkit/R$drawable;->pspdf__ic_cloudy_polygon:I

    sget v27, Lcom/pspdfkit/R$string;->pspdf__annotation_type_cloudy:I

    .line 164
    invoke-static/range {v16 .. v16}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v28

    const-string v21, "CLOUDY_ITEM"

    const/16 v22, 0x16

    move-object/from16 v20, v33

    invoke-direct/range {v20 .. v29}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    sput-object v33, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->CLOUDY_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 166
    new-instance v16, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v44, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->POLYLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v45, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_polyline:I

    sget v46, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__polylineIcon:I

    sget v47, Lcom/pspdfkit/R$drawable;->pspdf__ic_polyline:I

    sget v48, Lcom/pspdfkit/R$string;->pspdf__annotation_type_polyline:I

    const-string v42, "POLYLINE_ITEM"

    const/16 v43, 0x17

    move-object/from16 v41, v16

    invoke-direct/range {v41 .. v48}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v16, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->POLYLINE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 172
    new-instance v28, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v23, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->IMAGE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v24, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_image:I

    sget v25, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__imageIcon:I

    sget v26, Lcom/pspdfkit/R$drawable;->pspdf__ic_image:I

    sget v27, Lcom/pspdfkit/R$string;->pspdf__gallery_item_img_desc:I

    const-string v21, "IMAGE_ITEM"

    const/16 v22, 0x18

    move-object/from16 v20, v28

    invoke-direct/range {v20 .. v27}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v28, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->IMAGE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 178
    new-instance v20, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v37, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->CAMERA:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v38, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_camera:I

    sget v39, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__cameraIcon:I

    sget v40, Lcom/pspdfkit/R$drawable;->pspdf__ic_camera:I

    sget v41, Lcom/pspdfkit/R$string;->pspdf__annotation_type_camera:I

    const-string v35, "CAMERA_ITEM"

    const/16 v36, 0x19

    move-object/from16 v34, v20

    invoke-direct/range {v34 .. v41}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v20, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->CAMERA_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 184
    new-instance v21, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v45, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->STAMP:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v46, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_stamp:I

    sget v47, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__stampIcon:I

    sget v48, Lcom/pspdfkit/R$drawable;->pspdf__ic_stamp:I

    sget v49, Lcom/pspdfkit/R$string;->pspdf__annotation_type_stamp:I

    const-string v43, "STAMP_ITEM"

    const/16 v44, 0x1a

    move-object/from16 v42, v21

    invoke-direct/range {v42 .. v49}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v21, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->STAMP_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 190
    new-instance v22, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v37, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->ERASER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v38, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_eraser:I

    sget v39, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__eraserIcon:I

    sget v40, Lcom/pspdfkit/R$drawable;->pspdf__ic_eraser:I

    sget v41, Lcom/pspdfkit/R$string;->pspdf__annotation_type_eraser:I

    const-string v35, "ERASER_ITEM"

    const/16 v36, 0x1b

    move-object/from16 v34, v22

    invoke-direct/range {v34 .. v41}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v22, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->ERASER_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 196
    new-instance v23, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v45, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->REDACTION:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v46, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_redaction:I

    sget v47, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__redactionIcon:I

    sget v48, Lcom/pspdfkit/R$drawable;->pspdf__ic_redaction:I

    sget v49, Lcom/pspdfkit/R$string;->pspdf__annotation_type_redaction:I

    const-string v43, "REDACTION_ITEM"

    const/16 v44, 0x1c

    move-object/from16 v42, v23

    invoke-direct/range {v42 .. v49}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v23, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->REDACTION_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 202
    new-instance v24, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v37, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SOUND:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v38, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_sound:I

    sget v39, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__soundIcon:I

    sget v40, Lcom/pspdfkit/R$drawable;->pspdf__ic_sound:I

    sget v41, Lcom/pspdfkit/R$string;->pspdf__annotation_type_sound:I

    const-string v35, "SOUND_ITEM"

    const/16 v36, 0x1d

    move-object/from16 v34, v24

    invoke-direct/range {v34 .. v41}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v24, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->SOUND_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 208
    new-instance v25, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v45, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INSTANT_COMMENT_MARKER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v46, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_instant_comment_marker:I

    sget v47, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__instantCommentIcon:I

    sget v48, Lcom/pspdfkit/R$drawable;->pspdf__ic_instant_comment:I

    sget v49, Lcom/pspdfkit/R$string;->pspdf__annotation_type_instantComments:I

    const-string v43, "INSTANT_COMMENT_MARKER_ITEM"

    const/16 v44, 0x1e

    move-object/from16 v42, v25

    invoke-direct/range {v42 .. v49}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v25, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->INSTANT_COMMENT_MARKER_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 214
    new-instance v26, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v37, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INSTANT_HIGHLIGHT_COMMENT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v38, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_instant_highlight_comment:I

    sget v39, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__instantCommentIcon:I

    sget v40, Lcom/pspdfkit/R$drawable;->pspdf__ic_instant_comment:I

    sget v41, Lcom/pspdfkit/R$string;->pspdf__annotation_type_instantComments:I

    const-string v35, "INSTANT_HIGHLIGHT_COMMENT_ITEM"

    const/16 v36, 0x1f

    move-object/from16 v34, v26

    invoke-direct/range {v34 .. v41}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v26, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->INSTANT_HIGHLIGHT_COMMENT_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 221
    new-instance v27, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v45, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_DISTANCE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v46, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_distance:I

    sget v47, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__measurementDistanceIcon:I

    sget v48, Lcom/pspdfkit/R$drawable;->pspdf__ic_measurement_distance:I

    sget v49, Lcom/pspdfkit/R$string;->pspdf__annotation_type_measure_distance:I

    const-string v43, "MEASUREMENT_DISTANCE_ITEM"

    const/16 v44, 0x20

    move-object/from16 v42, v27

    invoke-direct/range {v42 .. v49}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v27, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->MEASUREMENT_DISTANCE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 227
    new-instance v29, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v37, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_PERIMETER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v38, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_perimeter:I

    sget v39, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__measurementPerimeterIcon:I

    sget v40, Lcom/pspdfkit/R$drawable;->pspdf__ic_measurement_perimeter:I

    sget v41, Lcom/pspdfkit/R$string;->pspdf__annotation_type_measure_perimeter:I

    const-string v35, "MEASUREMENT_PERIMETER_ITEM"

    const/16 v36, 0x21

    move-object/from16 v34, v29

    invoke-direct/range {v34 .. v41}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v29, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->MEASUREMENT_PERIMETER_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 233
    new-instance v34, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v45, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_POLYGON:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v46, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_area_polygon:I

    sget v47, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__measurementAreaPolygonIcon:I

    sget v48, Lcom/pspdfkit/R$drawable;->pspdf__ic_measurement_area_polygon:I

    sget v49, Lcom/pspdfkit/R$string;->pspdf__annotation_type_measure_polygonal_area:I

    const-string v43, "MEASUREMENT_AREA_POLYGON_ITEM"

    const/16 v44, 0x22

    move-object/from16 v42, v34

    invoke-direct/range {v42 .. v49}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v34, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->MEASUREMENT_AREA_POLYGON_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 239
    new-instance v43, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v38, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_ELLIPSE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v39, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_area_ellipse:I

    sget v40, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__measurementAreaEllipseIcon:I

    sget v41, Lcom/pspdfkit/R$drawable;->pspdf__ic_measurement_area_ellipse:I

    sget v42, Lcom/pspdfkit/R$string;->pspdf__annotation_type_measure_elliptical_area:I

    const-string v36, "MEASUREMENT_AREA_ELLIPSE_ITEM"

    const/16 v37, 0x23

    move-object/from16 v35, v43

    invoke-direct/range {v35 .. v42}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v43, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->MEASUREMENT_AREA_ELLIPSE_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    .line 245
    new-instance v35, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    sget-object v47, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_RECT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v48, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_area_rect:I

    sget v49, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__measurementAreaRectIcon:I

    sget v50, Lcom/pspdfkit/R$drawable;->pspdf__ic_measurement_area_rectangle:I

    sget v51, Lcom/pspdfkit/R$string;->pspdf__annotation_type_measure_rectangular_area:I

    const-string v45, "MEASUREMENT_AREA_RECT_ITEM"

    const/16 v46, 0x24

    move-object/from16 v44, v35

    invoke-direct/range {v44 .. v51}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V

    sput-object v35, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->MEASUREMENT_AREA_RECT_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    move-object/from16 v36, v14

    const/16 v14, 0x25

    new-array v14, v14, [Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    const/16 v37, 0x0

    aput-object v8, v14, v37

    const/4 v8, 0x1

    aput-object v0, v14, v8

    const/4 v0, 0x2

    aput-object v1, v14, v0

    const/4 v0, 0x3

    aput-object v2, v14, v0

    const/4 v0, 0x4

    aput-object v3, v14, v0

    const/4 v0, 0x5

    aput-object v4, v14, v0

    const/4 v0, 0x6

    aput-object v5, v14, v0

    const/4 v0, 0x7

    aput-object v6, v14, v0

    const/16 v0, 0x8

    aput-object v7, v14, v0

    const/16 v0, 0x9

    aput-object v19, v14, v0

    const/16 v0, 0xa

    aput-object v9, v14, v0

    const/16 v0, 0xb

    aput-object v18, v14, v0

    const/16 v0, 0xc

    aput-object v10, v14, v0

    const/16 v0, 0xd

    aput-object v11, v14, v0

    const/16 v0, 0xe

    aput-object v13, v14, v0

    const/16 v0, 0xf

    aput-object v15, v14, v0

    const/16 v0, 0x10

    aput-object v12, v14, v0

    const/16 v0, 0x11

    aput-object v30, v14, v0

    const/16 v0, 0x12

    aput-object v31, v14, v0

    const/16 v0, 0x13

    aput-object v17, v14, v0

    const/16 v0, 0x14

    aput-object v32, v14, v0

    const/16 v0, 0x15

    aput-object v36, v14, v0

    const/16 v0, 0x16

    aput-object v33, v14, v0

    const/16 v0, 0x17

    aput-object v16, v14, v0

    const/16 v0, 0x18

    aput-object v28, v14, v0

    const/16 v0, 0x19

    aput-object v20, v14, v0

    const/16 v0, 0x1a

    aput-object v21, v14, v0

    const/16 v0, 0x1b

    aput-object v22, v14, v0

    const/16 v0, 0x1c

    aput-object v23, v14, v0

    const/16 v0, 0x1d

    aput-object v24, v14, v0

    const/16 v0, 0x1e

    aput-object v25, v14, v0

    const/16 v0, 0x1f

    aput-object v26, v14, v0

    const/16 v0, 0x20

    aput-object v27, v14, v0

    const/16 v0, 0x21

    aput-object v29, v14, v0

    const/16 v0, 0x22

    aput-object v34, v14, v0

    const/16 v0, 0x23

    aput-object v43, v14, v0

    const/16 v0, 0x24

    aput-object v35, v14, v0

    .line 246
    sput-object v14, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->$VALUES:[Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIII)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            "IIII)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v8

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v9}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;-><init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/pspdfkit/ui/special_mode/controller/AnnotationTool;IIIILcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            "IIII",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;",
            "Z)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-object p3, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->annotationTool:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 4
    iput p4, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->id:I

    .line 5
    iput p5, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->styleableId:I

    .line 6
    iput p6, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->drawableId:I

    .line 7
    iput p7, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->stringId:I

    .line 8
    iput-object p8, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->annotationToolVariant:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 9
    iput-boolean p9, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->isStyleIndicatorEnabled:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->$VALUES:[Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    invoke-virtual {v0}, [Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    return-object v0
.end method

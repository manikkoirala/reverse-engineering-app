.class public Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarGroupingRule;
.super Lcom/pspdfkit/ui/toolbar/grouping/presets/PresetMenuItemGroupingRule;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/PresetMenuItemGroupingRule;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public getGroupPreset(II)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation

    const/4 p2, 0x4

    if-ne p1, p2, :cond_0

    .line 1
    sget-object p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;->FOUR_ITEMS_GROUPING:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 p2, 0x5

    if-ne p1, p2, :cond_1

    .line 3
    sget-object p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;->FIVE_ITEMS_GROUPING:Ljava/util/List;

    goto :goto_0

    :cond_1
    const/4 p2, 0x6

    if-ne p1, p2, :cond_2

    .line 5
    sget-object p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;->SIX_ITEMS_GROUPING:Ljava/util/List;

    goto :goto_0

    :cond_2
    const/4 p2, 0x7

    if-lt p1, p2, :cond_3

    const/16 p2, 0x9

    if-ge p1, p2, :cond_3

    .line 7
    sget-object p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;->SEVEN_ITEMS_GROUPING:Ljava/util/List;

    goto :goto_0

    .line 9
    :cond_3
    sget-object p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;->ALL_ITEMS_GROUPING:Ljava/util/List;

    :goto_0
    return-object p1
.end method

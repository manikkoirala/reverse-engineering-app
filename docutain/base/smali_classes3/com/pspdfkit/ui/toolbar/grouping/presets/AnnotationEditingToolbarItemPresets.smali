.class public Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final ALL_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final COPY_CUT_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

.field public static final FIVE_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final FOUR_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final INSPECTOR_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

.field public static final SEVEN_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final SIX_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final UNDO_REDO_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;->FOUR_ITEMS_GROUPING:Ljava/util/List;

    .line 3
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v2, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;->FIVE_ITEMS_GROUPING:Ljava/util/List;

    .line 5
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x6

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v3, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;->SIX_ITEMS_GROUPING:Ljava/util/List;

    .line 7
    new-instance v4, Ljava/util/ArrayList;

    const/4 v5, 0x7

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v4, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;->SEVEN_ITEMS_GROUPING:Ljava/util/List;

    .line 9
    new-instance v5, Ljava/util/ArrayList;

    const/16 v6, 0x9

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v5, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;->ALL_ITEMS_GROUPING:Ljava/util/List;

    .line 12
    new-instance v6, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v7, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_undo_redo:I

    const/4 v8, 0x2

    new-array v9, v8, [I

    sget v10, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_undo:I

    const/4 v11, 0x0

    aput v10, v9, v11

    sget v10, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_redo:I

    const/4 v12, 0x1

    aput v10, v9, v12

    invoke-direct {v6, v7, v9}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v6, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;->UNDO_REDO_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 16
    new-instance v7, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v9, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_copy_cut:I

    new-array v10, v8, [I

    sget v13, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_copy:I

    aput v13, v10, v11

    sget v13, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_cut:I

    aput v13, v10, v12

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v7, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;->COPY_CUT_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 20
    new-instance v9, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v10, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_inspector:I

    const/4 v13, 0x3

    new-array v14, v13, [I

    sget v15, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_picker:I

    aput v15, v14, v11

    sget v15, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_play:I

    aput v15, v14, v12

    sget v15, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_record:I

    aput v15, v14, v8

    invoke-direct {v9, v10, v14}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v9, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarItemPresets;->INSPECTOR_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 28
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    new-instance v10, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v14, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_edit_share:I

    new-array v1, v1, [I

    sget v15, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_delete:I

    aput v15, v1, v11

    sget v15, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_annotation_note:I

    aput v15, v1, v12

    sget v15, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_edit:I

    aput v15, v1, v8

    sget v15, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_share:I

    aput v15, v1, v13

    invoke-direct {v10, v14, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_edit_share:I

    new-array v10, v13, [I

    sget v13, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_annotation_note:I

    aput v13, v10, v11

    sget v13, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_edit:I

    aput v13, v10, v12

    sget v13, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_share:I

    aput v13, v10, v8

    invoke-direct {v0, v1, v10}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_delete:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_edit:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_edit_share:I

    new-array v2, v8, [I

    sget v8, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_annotation_note:I

    aput v8, v2, v11

    sget v8, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_share:I

    aput v8, v2, v12

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_delete:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_annotation_note:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_edit:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_share:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_delete:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_copy:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_cut:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_picker:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_play:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_record:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_annotation_note:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_edit:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_undo:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_redo:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_share:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_delete:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

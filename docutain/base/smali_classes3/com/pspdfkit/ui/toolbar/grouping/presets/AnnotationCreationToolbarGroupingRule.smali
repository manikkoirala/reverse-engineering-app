.class public Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarGroupingRule;
.super Lcom/pspdfkit/ui/toolbar/grouping/presets/PresetMenuItemGroupingRule;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/PresetMenuItemGroupingRule;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public areGeneratedGroupItemsSelectable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getGroupPreset(II)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 1
    sget-object p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemPresets;->FOUR_ITEMS_GROUPING:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v0, 0x5

    if-ne p1, v0, :cond_1

    .line 3
    sget-object p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemPresets;->FIVE_ITEMS_GROUPING:Ljava/util/List;

    goto :goto_0

    :cond_1
    const/4 v0, 0x6

    const/16 v1, 0x8

    if-lt p1, v0, :cond_2

    if-ge p1, v1, :cond_2

    .line 5
    sget-object p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemPresets;->SIX_ITEMS_GROUPING:Ljava/util/List;

    goto :goto_0

    :cond_2
    if-lt p1, v1, :cond_3

    if-ge p1, p2, :cond_3

    .line 7
    sget-object p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemPresets;->EIGHT_ITEMS_GROUPING:Ljava/util/List;

    goto :goto_0

    .line 9
    :cond_3
    sget-object p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemPresets;->EIGHT_ITEMS_GROUPING:Ljava/util/List;

    :goto_0
    return-object p1
.end method

.class public Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemPresets;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final ALL_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final EIGHT_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final FIVE_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final FOUR_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final SIX_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemPresets;->FOUR_ITEMS_GROUPING:Ljava/util/List;

    .line 3
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemPresets;->FIVE_ITEMS_GROUPING:Ljava/util/List;

    .line 5
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x6

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v2, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemPresets;->SIX_ITEMS_GROUPING:Ljava/util/List;

    .line 7
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0x8

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v3, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemPresets;->EIGHT_ITEMS_GROUPING:Ljava/util/List;

    .line 9
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0x20

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v4, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemPresets;->ALL_ITEMS_GROUPING:Ljava/util/List;

    .line 12
    sget-object v5, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->MARKUP_GROUP_EXTRA:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 13
    sget-object v5, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->DRAWING_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 14
    new-instance v6, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v7, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    invoke-direct {v6, v7}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 15
    sget-object v6, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->UNDO_REDO_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19
    sget-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->MARKUP_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    sget-object v7, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->WRITING_AND_MULTIMEDIA_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    new-instance v7, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v8, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    invoke-direct {v7, v8}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    sget-object v1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->WRITING_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    sget-object v5, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->MULTIMEDIA_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    new-instance v7, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v8, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    invoke-direct {v7, v8}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    sget-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->DRAWING_GROUP_NO_ERASER:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_eraser:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_undo:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_redo:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_highlight:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_squiggly:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_strikeout:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_underline:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_redaction:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_note:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_instant_comment_marker:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_freetext:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_freetext_callout:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_signature:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_ink_pen:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_ink_highlighter:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_magic_ink:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_line:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_line_arrow:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_square:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_circle:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_polygon:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_cloudy:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_polyline:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_image:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_stamp:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_camera:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_sound:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_eraser:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_undo:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_redo:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/pspdfkit/ui/toolbar/grouping/presets/DocumentEditingToolbarItemPresets;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final ALL_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final FIVE_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final FOUR_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final SIX_ITEMS_GROUPING:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/DocumentEditingToolbarItemPresets;->FOUR_ITEMS_GROUPING:Ljava/util/List;

    .line 2
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v2, Lcom/pspdfkit/ui/toolbar/grouping/presets/DocumentEditingToolbarItemPresets;->FIVE_ITEMS_GROUPING:Ljava/util/List;

    .line 3
    new-instance v4, Ljava/util/ArrayList;

    const/4 v5, 0x6

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v4, Lcom/pspdfkit/ui/toolbar/grouping/presets/DocumentEditingToolbarItemPresets;->SIX_ITEMS_GROUPING:Ljava/util/List;

    .line 4
    new-instance v5, Ljava/util/ArrayList;

    const/4 v6, 0x7

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v5, Lcom/pspdfkit/ui/toolbar/grouping/presets/DocumentEditingToolbarItemPresets;->ALL_ITEMS_GROUPING:Ljava/util/List;

    .line 7
    new-instance v6, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v7, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_remove_pages:I

    invoke-direct {v6, v7}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 8
    new-instance v6, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v7, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_duplicate_pages:I

    invoke-direct {v6, v7}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 9
    new-instance v6, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v7, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_group_more:I

    new-array v3, v3, [I

    sget v8, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_rotate_pages:I

    const/4 v9, 0x0

    aput v8, v3, v9

    sget v8, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_import_document:I

    const/4 v10, 0x1

    aput v8, v3, v10

    sget v8, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_export_pages:I

    const/4 v11, 0x2

    aput v8, v3, v11

    sget v8, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_undo:I

    const/4 v12, 0x3

    aput v8, v3, v12

    sget v8, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_redo:I

    aput v8, v3, v1

    invoke-direct {v6, v7, v3}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16
    new-instance v3, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v6, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_done:I

    invoke-direct {v3, v6}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v3, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_remove_pages:I

    invoke-direct {v0, v3}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v3, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_duplicate_pages:I

    invoke-direct {v0, v3}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v3, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_rotate_pages:I

    invoke-direct {v0, v3}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v3, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_group_more:I

    new-array v1, v1, [I

    sget v6, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_import_document:I

    aput v6, v1, v9

    sget v6, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_export_pages:I

    aput v6, v1, v10

    sget v6, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_undo:I

    aput v6, v1, v11

    sget v6, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_redo:I

    aput v6, v1, v12

    invoke-direct {v0, v3, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_done:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_remove_pages:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_duplicate_pages:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_rotate_pages:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_import_document:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_group_more:I

    new-array v2, v12, [I

    sget v3, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_export_pages:I

    aput v3, v2, v9

    sget v3, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_undo:I

    aput v3, v2, v10

    sget v3, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_redo:I

    aput v3, v2, v11

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_done:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_remove_pages:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_duplicate_pages:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_rotate_pages:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_import_document:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_export_pages:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_undo:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_redo:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_done:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

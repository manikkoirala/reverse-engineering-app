.class public final Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;",
        "",
        "()V",
        "DRAWING_GROUP",
        "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
        "DRAWING_GROUP_NO_ERASER",
        "DRAWING_GROUP_PLUS_MEASUREMENTS",
        "MARKUP_GROUP",
        "MARKUP_GROUP_CALLOUT",
        "MARKUP_GROUP_EXTRA",
        "MEASUREMENTS_GROUP",
        "MULTIMEDIA_GROUP",
        "UNDO_REDO_GROUP",
        "WRITING_AND_MULTIMEDIA_GROUP",
        "WRITING_GROUP",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
    xi = 0x30
.end annotation


# static fields
.field public static final $stable:I

.field public static final DRAWING_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

.field public static final DRAWING_GROUP_NO_ERASER:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

.field public static final DRAWING_GROUP_PLUS_MEASUREMENTS:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

.field public static final INSTANCE:Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;

.field public static final MARKUP_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

.field public static final MARKUP_GROUP_CALLOUT:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

.field public static final MARKUP_GROUP_EXTRA:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

.field public static final MEASUREMENTS_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

.field public static final MULTIMEDIA_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

.field public static final UNDO_REDO_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

.field public static final WRITING_AND_MULTIMEDIA_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

.field public static final WRITING_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;

    invoke-direct {v0}, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;-><init>()V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->INSTANCE:Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 2
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_markup:I

    const/4 v2, 0x5

    new-array v3, v2, [I

    .line 4
    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_highlight:I

    const/4 v5, 0x0

    aput v4, v3, v5

    .line 5
    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_squiggly:I

    const/4 v6, 0x1

    aput v4, v3, v6

    .line 6
    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_strikeout:I

    const/4 v7, 0x2

    aput v4, v3, v7

    .line 7
    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_underline:I

    const/4 v8, 0x3

    aput v4, v3, v8

    .line 8
    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_redaction:I

    const/4 v9, 0x4

    aput v4, v3, v9

    .line 9
    invoke-direct {v0, v1, v3}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->MARKUP_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 21
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 22
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_markup:I

    const/4 v3, 0x6

    new-array v4, v3, [I

    .line 24
    sget v10, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_highlight:I

    aput v10, v4, v5

    .line 25
    sget v10, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_squiggly:I

    aput v10, v4, v6

    .line 26
    sget v10, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_strikeout:I

    aput v10, v4, v7

    .line 27
    sget v10, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_underline:I

    aput v10, v4, v8

    .line 28
    sget v10, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_redaction:I

    aput v10, v4, v9

    .line 29
    sget v10, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_freetext_callout:I

    aput v10, v4, v2

    .line 30
    invoke-direct {v0, v1, v4}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->MARKUP_GROUP_CALLOUT:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 43
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 44
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_markup:I

    const/16 v4, 0xe

    new-array v10, v4, [I

    .line 46
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_highlight:I

    aput v11, v10, v5

    .line 47
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_squiggly:I

    aput v11, v10, v6

    .line 48
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_strikeout:I

    aput v11, v10, v7

    .line 49
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_underline:I

    aput v11, v10, v8

    .line 50
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_redaction:I

    aput v11, v10, v9

    .line 51
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_note:I

    aput v11, v10, v2

    .line 52
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_instant_comment_marker:I

    aput v11, v10, v3

    .line 53
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_freetext:I

    const/4 v12, 0x7

    aput v11, v10, v12

    .line 54
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_freetext_callout:I

    const/16 v13, 0x8

    aput v11, v10, v13

    .line 55
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_signature:I

    const/16 v14, 0x9

    aput v11, v10, v14

    .line 56
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_image:I

    const/16 v15, 0xa

    aput v11, v10, v15

    .line 57
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_stamp:I

    const/16 v4, 0xb

    aput v11, v10, v4

    .line 58
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_camera:I

    const/16 v16, 0xc

    aput v11, v10, v16

    .line 59
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_sound:I

    const/16 v17, 0xd

    aput v11, v10, v17

    .line 60
    invoke-direct {v0, v1, v10}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->MARKUP_GROUP_EXTRA:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 81
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 82
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_writing:I

    new-array v10, v2, [I

    .line 84
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_note:I

    aput v11, v10, v5

    .line 85
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_instant_comment_marker:I

    aput v11, v10, v6

    .line 86
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_freetext:I

    aput v11, v10, v7

    .line 87
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_freetext_callout:I

    aput v11, v10, v8

    .line 88
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_signature:I

    aput v11, v10, v9

    .line 89
    invoke-direct {v0, v1, v10}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->WRITING_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 101
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 102
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_writing:I

    new-array v10, v14, [I

    .line 104
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_note:I

    aput v11, v10, v5

    .line 105
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_instant_comment_marker:I

    aput v11, v10, v6

    .line 106
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_freetext:I

    aput v11, v10, v7

    .line 107
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_freetext_callout:I

    aput v11, v10, v8

    .line 108
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_signature:I

    aput v11, v10, v9

    .line 109
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_image:I

    aput v11, v10, v2

    .line 110
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_stamp:I

    aput v11, v10, v3

    .line 111
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_camera:I

    aput v11, v10, v12

    .line 112
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_sound:I

    aput v11, v10, v13

    .line 113
    invoke-direct {v0, v1, v10}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->WRITING_AND_MULTIMEDIA_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 129
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 130
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_drawing:I

    new-array v10, v4, [I

    .line 132
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_ink_pen:I

    aput v11, v10, v5

    .line 133
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_ink_highlighter:I

    aput v11, v10, v6

    .line 134
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_magic_ink:I

    aput v11, v10, v7

    .line 135
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_line:I

    aput v11, v10, v8

    .line 136
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_line_arrow:I

    aput v11, v10, v9

    .line 137
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_square:I

    aput v11, v10, v2

    .line 138
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_circle:I

    aput v11, v10, v3

    .line 139
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_polygon:I

    aput v11, v10, v12

    .line 140
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_cloudy:I

    aput v11, v10, v13

    .line 141
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_polyline:I

    aput v11, v10, v14

    .line 142
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_eraser:I

    aput v11, v10, v15

    .line 143
    invoke-direct {v0, v1, v10}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->DRAWING_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 161
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 162
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_drawing:I

    new-array v10, v15, [I

    .line 164
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_ink_pen:I

    aput v11, v10, v5

    .line 165
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_ink_highlighter:I

    aput v11, v10, v6

    .line 166
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_magic_ink:I

    aput v11, v10, v7

    .line 167
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_line:I

    aput v11, v10, v8

    .line 168
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_line_arrow:I

    aput v11, v10, v9

    .line 169
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_square:I

    aput v11, v10, v2

    .line 170
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_circle:I

    aput v11, v10, v3

    .line 171
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_polygon:I

    aput v11, v10, v12

    .line 172
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_cloudy:I

    aput v11, v10, v13

    .line 173
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_polyline:I

    aput v11, v10, v14

    .line 174
    invoke-direct {v0, v1, v10}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->DRAWING_GROUP_NO_ERASER:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 191
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 192
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_drawing:I

    const/16 v10, 0x10

    new-array v10, v10, [I

    .line 194
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_ink_pen:I

    aput v11, v10, v5

    .line 195
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_ink_highlighter:I

    aput v11, v10, v6

    .line 196
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_magic_ink:I

    aput v11, v10, v7

    .line 197
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_line:I

    aput v11, v10, v8

    .line 198
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_line_arrow:I

    aput v11, v10, v9

    .line 199
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_square:I

    aput v11, v10, v2

    .line 200
    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_circle:I

    aput v11, v10, v3

    .line 201
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_polygon:I

    aput v3, v10, v12

    .line 202
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_cloudy:I

    aput v3, v10, v13

    .line 203
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_polyline:I

    aput v3, v10, v14

    .line 204
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_distance:I

    aput v3, v10, v15

    .line 205
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_perimeter:I

    aput v3, v10, v4

    .line 206
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_area_polygon:I

    aput v3, v10, v16

    .line 207
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_area_ellipse:I

    aput v3, v10, v17

    .line 208
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_area_rect:I

    const/16 v4, 0xe

    aput v3, v10, v4

    .line 209
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_eraser:I

    const/16 v4, 0xf

    aput v3, v10, v4

    .line 210
    invoke-direct {v0, v1, v10}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->DRAWING_GROUP_PLUS_MEASUREMENTS:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 233
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 234
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_multimedia:I

    new-array v3, v9, [I

    .line 236
    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_image:I

    aput v4, v3, v5

    .line 237
    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_stamp:I

    aput v4, v3, v6

    .line 238
    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_camera:I

    aput v4, v3, v7

    .line 239
    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_sound:I

    aput v4, v3, v8

    .line 240
    invoke-direct {v0, v1, v3}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->MULTIMEDIA_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 251
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 252
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_undo_redo:I

    new-array v3, v7, [I

    .line 254
    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_undo:I

    aput v4, v3, v5

    .line 255
    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_redo:I

    aput v4, v3, v6

    .line 256
    invoke-direct {v0, v1, v3}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->UNDO_REDO_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 265
    new-instance v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    .line 266
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_measurement:I

    new-array v2, v2, [I

    .line 268
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_distance:I

    aput v3, v2, v5

    .line 269
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_perimeter:I

    aput v3, v2, v6

    .line 270
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_area_polygon:I

    aput v3, v2, v7

    .line 271
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_area_ellipse:I

    aput v3, v2, v8

    .line 272
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_measurement_area_rect:I

    aput v3, v2, v9

    .line 273
    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->MEASUREMENTS_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sput v13, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->$stable:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final VALID_GROUP_IDS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final id:I

.field public final submenuIds:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Integer;

    .line 1
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_markup:I

    .line 2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_measurement:I

    .line 3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_drawing:I

    .line 4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_writing:I

    .line 5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_multimedia:I

    .line 6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_undo_redo:I

    .line 7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_undo_redo:I

    .line 8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_copy_cut:I

    .line 9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_inspector:I

    .line 10
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_edit_share:I

    .line 11
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_group_more:I

    .line 12
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xa

    aput-object v1, v0, v2

    .line 13
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;->VALID_GROUP_IDS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I[I)V

    return-void
.end method

.method public constructor <init>(I[I)V
    .locals 5

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    sget-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;->VALID_GROUP_IDS:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz p2, :cond_3

    .line 8
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 10
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v2

    .line 11
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "are "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/vp;->a(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v2, "unknown"

    const-string v0, "start with pspdf__annotation_creation_toolbar_group or pspdf__annotation_editing_toolbar_group"

    :goto_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v0, v1, v2

    const-string v0, "Illegal id (%s) was passed in for group MenuItem. Valid ids %s. This will throw an exception starting with PSPDFKit for Android 7.0"

    .line 13
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 18
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    const-string v2, "PSPDFKit"

    .line 19
    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 26
    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_undo_redo:I

    if-ne p1, v0, :cond_1

    .line 27
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_undo_redo:I

    goto :goto_1

    .line 28
    :cond_1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_undo_redo:I

    if-ne p1, v0, :cond_2

    .line 29
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_undo_redo:I

    goto :goto_1

    .line 30
    :cond_2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_more:I

    if-ne p1, v0, :cond_3

    .line 31
    sget p1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_group_more:I

    .line 35
    :cond_3
    :goto_1
    iput p1, p0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;->id:I

    .line 36
    iput-object p2, p0, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;->submenuIds:[I

    return-void
.end method

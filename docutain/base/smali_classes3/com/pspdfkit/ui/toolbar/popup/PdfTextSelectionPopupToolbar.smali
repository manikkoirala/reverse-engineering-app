.class public Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;
.super Lcom/pspdfkit/ui/PopupToolbar;
.source "SourceFile"


# instance fields
.field private controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PopupToolbar;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V
    .locals 7

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/ui/PopupToolbar;->pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isCopyPasteEnabled()Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 8
    new-instance v2, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;

    sget v5, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_copy:I

    sget v6, Lcom/pspdfkit/R$string;->pspdf__action_menu_copy:I

    invoke-direct {v2, v5, v6}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;-><init>(II)V

    .line 11
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextExtractionEnabledByDocumentPermissions()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 12
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->getApplicationPolicy()Lcom/pspdfkit/configuration/policy/ApplicationPolicy;

    move-result-object v5

    sget-object v6, Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;->TEXT_COPY_PASTE:Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;

    .line 13
    invoke-virtual {v5, v6}, Lcom/pspdfkit/configuration/policy/ApplicationPolicy;->hasPermissionForEvent(Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    .line 14
    :goto_0
    invoke-virtual {v2, v5}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->setEnabled(Z)V

    .line 18
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    :cond_1
    new-instance v2, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;

    sget v5, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_highlight:I

    sget v6, Lcom/pspdfkit/R$string;->pspdf__edit_menu_highlight:I

    invoke-direct {v2, v5, v6}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;-><init>(II)V

    .line 23
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextHighlightingEnabledByConfiguration()Z

    move-result v5

    invoke-virtual {v2, v5}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->setEnabled(Z)V

    .line 24
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isInstantHighlightCommentingEnabledByConfiguration()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 27
    new-instance v2, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;

    sget v5, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_instantHighlightComment:I

    sget v6, Lcom/pspdfkit/R$string;->pspdf__annotation_type_instantComments:I

    invoke-direct {v2, v5, v6}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;-><init>(II)V

    .line 30
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    :cond_2
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextHighlightingEnabledByConfiguration()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 35
    new-instance v2, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;

    sget v5, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_strikeout:I

    sget v6, Lcom/pspdfkit/R$string;->pspdf__edit_menu_strikeout:I

    invoke-direct {v2, v5, v6}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;-><init>(II)V

    .line 37
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextHighlightingEnabledByConfiguration()Z

    move-result v5

    invoke-virtual {v2, v5}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->setEnabled(Z)V

    .line 38
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    new-instance v2, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;

    sget v5, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_underline:I

    sget v6, Lcom/pspdfkit/R$string;->pspdf__edit_menu_underline:I

    invoke-direct {v2, v5, v6}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;-><init>(II)V

    .line 42
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextHighlightingEnabledByConfiguration()Z

    move-result v5

    invoke-virtual {v2, v5}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->setEnabled(Z)V

    .line 43
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    :cond_3
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isRedactionEnabledByConfiguration()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 47
    new-instance v2, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;

    sget v5, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_redact:I

    sget v6, Lcom/pspdfkit/R$string;->pspdf__redaction_redact:I

    invoke-direct {v2, v5, v6}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;-><init>(II)V

    .line 49
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_4
    iget-object v2, p0, Lcom/pspdfkit/ui/PopupToolbar;->pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/ag;->getPasteManager()Lcom/pspdfkit/internal/h7;

    move-result-object v2

    .line 53
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isCopyPasteEnabled()Z

    move-result v1

    if-eqz v1, :cond_5

    if-eqz v2, :cond_5

    invoke-interface {v2}, Lcom/pspdfkit/internal/h7;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 54
    new-instance v1, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_paste_annotation:I

    sget v5, Lcom/pspdfkit/R$string;->pspdf__paste:I

    invoke-direct {v1, v2, v5}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;-><init>(II)V

    .line 56
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_5
    new-instance v1, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_speak:I

    sget v5, Lcom/pspdfkit/R$string;->pspdf__action_menu_speak:I

    invoke-direct {v1, v2, v5}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;-><init>(II)V

    .line 61
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextExtractionEnabledByDocumentPermissions()Z

    move-result v2

    if-nez v2, :cond_7

    .line 62
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextSpeakEnabledByDocumentPermissions()Z

    move-result v2

    if-eqz v2, :cond_6

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    :cond_7
    :goto_1
    const/4 v2, 0x1

    .line 63
    :goto_2
    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->setEnabled(Z)V

    .line 65
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    new-instance v1, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_search:I

    sget v5, Lcom/pspdfkit/R$string;->pspdf__activity_menu_search:I

    invoke-direct {v1, v2, v5}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;-><init>(II)V

    .line 69
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextSharingEnabledByConfiguration()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 72
    new-instance v1, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_share:I

    sget v5, Lcom/pspdfkit/R$string;->pspdf__share:I

    invoke-direct {v1, v2, v5}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;-><init>(II)V

    .line 74
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextExtractionEnabledByDocumentPermissions()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 75
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->getApplicationPolicy()Lcom/pspdfkit/configuration/policy/ApplicationPolicy;

    move-result-object v2

    sget-object v5, Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;->TEXT_COPY_PASTE:Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;

    .line 76
    invoke-virtual {v2, v5}, Lcom/pspdfkit/configuration/policy/ApplicationPolicy;->hasPermissionForEvent(Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;)Z

    move-result v2

    if-eqz v2, :cond_8

    goto :goto_3

    :cond_8
    const/4 v3, 0x0

    .line 77
    :goto_3
    invoke-virtual {v1, v3}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->setEnabled(Z)V

    .line 80
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    :cond_9
    new-instance v1, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_link:I

    sget v3, Lcom/pspdfkit/R$string;->pspdf__create_link:I

    invoke-direct {v1, v2, v3}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;-><init>(II)V

    .line 85
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isLinkCreationEnabledByConfiguration()Z

    move-result p1

    invoke-virtual {v1, p1}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->setEnabled(Z)V

    .line 86
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/PopupToolbar;->setMenuItems(Ljava/util/List;)V

    return-void
.end method

.method public dismiss()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/PopupToolbar;->dismiss()V

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/bu;->a()V

    return-void
.end method

.method public getController()Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    return-object v0
.end method

.method public getViewId()I
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar:I

    return v0
.end method

.method public onItemClicked(Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)Z
    .locals 9

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/PopupToolbar;->onItemClicked(Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 4
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    if-eqz v2, :cond_d

    if-eqz v0, :cond_d

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->getId()I

    move-result p1

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/ui/PopupToolbar;->pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    if-nez v2, :cond_2

    const/4 p1, 0x0

    return p1

    .line 11
    :cond_2
    sget v3, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_share:I

    const-string v4, "page_index"

    const-string v5, "action"

    const-string v6, "perform_text_selection_action"

    if-ne p1, v3, :cond_3

    .line 12
    iget-object p1, v0, Lcom/pspdfkit/datastructures/TextSelection;->text:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_d

    .line 13
    iget-object p1, v0, Lcom/pspdfkit/datastructures/TextSelection;->text:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingManager;->shareText(Landroid/content/Context;Ljava/lang/String;)V

    .line 15
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    .line 16
    invoke-virtual {p1, v6}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    const-string v2, "share"

    .line 17
    invoke-virtual {p1, v5, v2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    iget v0, v0, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    .line 18
    invoke-virtual {p1, v0, v4}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 19
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    goto/16 :goto_1

    .line 21
    :cond_3
    sget v3, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_copy:I

    if-ne p1, v3, :cond_4

    .line 23
    iget-object p1, v0, Lcom/pspdfkit/datastructures/TextSelection;->text:Ljava/lang/String;

    sget v3, Lcom/pspdfkit/R$string;->pspdf__text_copied_to_clipboard:I

    const/16 v7, 0x30

    const-string v8, ""

    .line 24
    invoke-static {p1, v8, v2, v3, v7}, Lcom/pspdfkit/internal/j5;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;II)Z

    .line 25
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/SpecialModeController;->exitActiveMode()V

    .line 27
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    .line 28
    invoke-virtual {p1, v6}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    const-string v2, "clipboard"

    .line 29
    invoke-virtual {p1, v5, v2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    iget v0, v0, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    .line 30
    invoke-virtual {p1, v0, v4}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 31
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    goto/16 :goto_1

    .line 32
    :cond_4
    sget v3, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_highlight:I

    if-ne p1, v3, :cond_5

    .line 33
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->highlightSelectedText()V

    goto/16 :goto_1

    .line 34
    :cond_5
    sget v3, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_instantHighlightComment:I

    if-ne p1, v3, :cond_6

    .line 35
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->highlightSelectedTextAndBeginCommenting()V

    goto/16 :goto_1

    .line 36
    :cond_6
    sget v3, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_redact:I

    if-ne p1, v3, :cond_7

    .line 37
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->redactSelectedText()V

    goto/16 :goto_1

    .line 38
    :cond_7
    sget v3, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_strikeout:I

    if-ne p1, v3, :cond_8

    .line 39
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->strikeoutSelectedText()V

    goto/16 :goto_1

    .line 40
    :cond_8
    sget v3, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_underline:I

    if-ne p1, v3, :cond_9

    .line 41
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->underlineSelectedText()V

    goto/16 :goto_1

    .line 42
    :cond_9
    sget v3, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_speak:I

    if-ne p1, v3, :cond_a

    .line 43
    iget-object p1, v0, Lcom/pspdfkit/datastructures/TextSelection;->text:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/pspdfkit/internal/bu;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 45
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    .line 46
    invoke-virtual {p1, v6}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    const-string v2, "tts"

    .line 47
    invoke-virtual {p1, v5, v2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    iget v0, v0, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    .line 48
    invoke-virtual {p1, v0, v4}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    goto :goto_1

    .line 50
    :cond_a
    sget v2, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_search:I

    if-ne p1, v2, :cond_b

    .line 51
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    if-eqz p1, :cond_d

    .line 52
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->searchSelectedText()V

    goto :goto_1

    .line 54
    :cond_b
    sget v2, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_link:I

    if-ne p1, v2, :cond_c

    .line 55
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    if-eqz p1, :cond_d

    .line 56
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->createLinkAboveSelectedText()V

    goto :goto_1

    .line 58
    :cond_c
    sget v2, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_paste_annotation:I

    if-ne p1, v2, :cond_d

    .line 59
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    if-eqz p1, :cond_d

    .line 60
    iget-object p1, p0, Lcom/pspdfkit/ui/PopupToolbar;->pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

    .line 61
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/ag;->getPasteManager()Lcom/pspdfkit/internal/h7;

    move-result-object p1

    if-eqz p1, :cond_d

    .line 62
    invoke-interface {p1}, Lcom/pspdfkit/internal/h7;->a()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 63
    iget-object v2, v0, Lcom/pspdfkit/datastructures/TextSelection;->textBlocks:Ljava/util/List;

    invoke-static {v2}, Lcom/pspdfkit/utils/PdfUtils;->createPdfRectUnion(Ljava/util/List;)Landroid/graphics/RectF;

    move-result-object v2

    .line 64
    iget v0, v0, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    new-instance v3, Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/RectF;->right:F

    iget v5, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-direct {v3, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 65
    invoke-interface {p1, v0, v3}, Lcom/pspdfkit/internal/h7;->a(ILandroid/graphics/PointF;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Maybe;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    .line 71
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->dismiss()V

    :cond_d
    :goto_1
    return v1
.end method

.method public showForSelectedText()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3
    iget-object v1, v0, Lcom/pspdfkit/datastructures/TextSelection;->textBlocks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 4
    iget-object v1, v0, Lcom/pspdfkit/datastructures/TextSelection;->textBlocks:Ljava/util/List;

    invoke-static {v1}, Lcom/pspdfkit/utils/PdfUtils;->createPdfRectUnion(Ljava/util/List;)Landroid/graphics/RectF;

    move-result-object v1

    .line 6
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 7
    iget-object v3, p0, Lcom/pspdfkit/ui/PopupToolbar;->pdfFragment:Lcom/pspdfkit/ui/PdfFragment;

    iget v4, v0, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    invoke-virtual {v3, v2, v4}, Lcom/pspdfkit/ui/PdfFragment;->getVisiblePdfRect(Landroid/graphics/RectF;I)Z

    .line 9
    new-instance v3, Landroid/graphics/RectF;

    iget v4, v2, Landroid/graphics/RectF;->left:F

    iget v5, v2, Landroid/graphics/RectF;->bottom:F

    iget v6, v2, Landroid/graphics/RectF;->right:F

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-direct {v3, v4, v5, v6, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 11
    new-instance v2, Landroid/graphics/RectF;

    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    iget v6, v1, Landroid/graphics/RectF;->right:F

    iget v7, v1, Landroid/graphics/RectF;->top:F

    invoke-direct {v2, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 17
    invoke-virtual {v3, v2}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    .line 19
    :cond_0
    iget v0, v0, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v3, v1, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    iget v1, v1, Landroid/graphics/RectF;->top:F

    .line 22
    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 23
    invoke-virtual {p0, v0, v2, v1}, Lcom/pspdfkit/ui/PopupToolbar;->show(IFF)V

    :cond_1
    return-void
.end method

.method public unbindController()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/bu;->a()V

    return-void
.end method

.class public Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final id:I

.field private isEnabled:Z

.field private final title:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->isEnabled:Z

    .line 11
    iput p1, p0, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->id:I

    .line 12
    iput p2, p0, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->title:I

    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput p1, p0, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->id:I

    .line 15
    iput p2, p0, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->title:I

    .line 16
    iput-boolean p3, p0, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->isEnabled:Z

    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->id:I

    return v0
.end method

.method public getTitle()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->title:I

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->isEnabled:Z

    return v0
.end method

.method public setEnabled(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->isEnabled:Z

    return-void
.end method

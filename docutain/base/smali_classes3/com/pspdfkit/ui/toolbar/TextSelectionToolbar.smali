.class public Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;
.super Lcom/pspdfkit/ui/toolbar/ContextualToolbar;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/ui/toolbar/ContextualToolbar<",
        "Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;",
        ">;"
    }
.end annotation


# static fields
.field private static final ATTRS:[I

.field private static final DEF_STYLE_ATTR:I


# instance fields
.field private areItemsSet:Z

.field controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

.field private copyIcon:I

.field private highlightIcon:I

.field private iconColor:I

.field private iconColorActivated:I

.field private linkIcon:I

.field private searchIcon:I

.field private shareIcon:I

.field private speakIcon:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__TextSelectionToolbarIcons:[I

    sput-object v0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->ATTRS:[I

    .line 7
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__textSelectionToolbarIconsStyle:I

    sput v0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->DEF_STYLE_ATTR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->areItemsSet:Z

    .line 6
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 7
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 8
    iput-boolean p2, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->areItemsSet:Z

    .line 17
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    .line 19
    iput-boolean p2, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->areItemsSet:Z

    .line 33
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->init(Landroid/content/Context;)V

    return-void
.end method

.method private applyAnnotationControllerChanges()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-boolean v1, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->areItemsSet:Z

    if-nez v1, :cond_1

    .line 4
    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->generateMenuItems(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItems(Ljava/util/List;)V

    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->areItemsSet:Z

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->notifyToolbarChanged()V

    :cond_1
    return-void
.end method

.method private generateMenuItems(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 2
    new-instance v9, Ljava/util/ArrayList;

    const/4 v0, 0x5

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 4
    sget v1, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_copy:I

    iget v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->copyIcon:I

    .line 7
    invoke-static {v8, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget v0, Lcom/pspdfkit/R$string;->pspdf__action_menu_copy:I

    const/4 v10, 0x0

    .line 8
    invoke-static {v8, v0, v10}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 9
    iget v4, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColor:I

    iget v5, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColorActivated:I

    sget-object v11, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->END:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/4 v7, 0x0

    move-object v0, v8

    move-object v6, v11

    .line 10
    invoke-static/range {v0 .. v7}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    const/4 v12, 0x1

    const/4 v13, 0x0

    if-eqz p1, :cond_0

    .line 19
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextExtractionEnabledByDocumentPermissions()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 20
    :cond_0
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->getApplicationPolicy()Lcom/pspdfkit/configuration/policy/ApplicationPolicy;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;->TEXT_COPY_PASTE:Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;

    .line 21
    invoke-virtual {v1, v2}, Lcom/pspdfkit/configuration/policy/ApplicationPolicy;->hasPermissionForEvent(Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 22
    :goto_0
    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setEnabled(Z)V

    .line 25
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    sget v1, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_highlight:I

    iget v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->highlightIcon:I

    .line 30
    invoke-static {v8, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget v0, Lcom/pspdfkit/R$string;->pspdf__edit_menu_highlight:I

    .line 31
    invoke-static {v8, v0, v10}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 32
    iget v4, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColor:I

    iget v5, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColorActivated:I

    const/4 v7, 0x0

    move-object v0, v8

    move-object v6, v11

    .line 33
    invoke-static/range {v0 .. v7}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-eqz p1, :cond_3

    .line 42
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextHighlightingEnabledByConfiguration()Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setEnabled(Z)V

    .line 43
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    sget v1, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_speak:I

    iget v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->speakIcon:I

    .line 48
    invoke-static {v8, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget v0, Lcom/pspdfkit/R$string;->pspdf__action_menu_speak:I

    .line 49
    invoke-static {v8, v0, v10}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 50
    iget v4, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColor:I

    iget v5, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColorActivated:I

    const/4 v7, 0x0

    move-object v0, v8

    move-object v6, v11

    .line 51
    invoke-static/range {v0 .. v7}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-eqz p1, :cond_5

    .line 61
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextExtractionEnabledByDocumentPermissions()Z

    move-result v1

    if-nez v1, :cond_5

    .line 62
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextSpeakEnabledByDocumentPermissions()Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v1, 0x1

    .line 63
    :goto_4
    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setEnabled(Z)V

    .line 66
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    sget v1, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_search:I

    iget v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->searchIcon:I

    .line 71
    invoke-static {v8, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget v0, Lcom/pspdfkit/R$string;->pspdf__activity_menu_search:I

    .line 72
    invoke-static {v8, v0, v10}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 73
    iget v4, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColor:I

    iget v5, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColorActivated:I

    const/4 v7, 0x0

    move-object v0, v8

    move-object v6, v11

    .line 74
    invoke-static/range {v0 .. v7}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    .line 83
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p1, :cond_7

    .line 85
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextSharingEnabledByConfiguration()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 86
    sget v1, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_share:I

    iget v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->shareIcon:I

    .line 89
    invoke-static {v8, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget v0, Lcom/pspdfkit/R$string;->pspdf__share:I

    .line 90
    invoke-static {v8, v0, v10}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 91
    iget v4, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColor:I

    iget v5, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColorActivated:I

    const/4 v7, 0x0

    move-object v0, v8

    move-object v6, v11

    .line 92
    invoke-static/range {v0 .. v7}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    .line 101
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isTextExtractionEnabledByDocumentPermissions()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 102
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->getApplicationPolicy()Lcom/pspdfkit/configuration/policy/ApplicationPolicy;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;->TEXT_COPY_PASTE:Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;

    .line 103
    invoke-virtual {v1, v2}, Lcom/pspdfkit/configuration/policy/ApplicationPolicy;->hasPermissionForEvent(Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    goto :goto_5

    :cond_6
    const/4 v1, 0x0

    .line 104
    :goto_5
    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setEnabled(Z)V

    .line 107
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_7
    sget v1, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_link:I

    iget v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->linkIcon:I

    .line 113
    invoke-static {v8, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget v0, Lcom/pspdfkit/R$string;->pspdf__create_link:I

    .line 114
    invoke-static {v8, v0, v10}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 115
    iget v4, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColor:I

    iget v5, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColorActivated:I

    const/4 v7, 0x0

    move-object v0, v8

    move-object v6, v11

    .line 116
    invoke-static/range {v0 .. v7}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-eqz p1, :cond_8

    .line 125
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->isLinkCreationEnabledByConfiguration()Z

    move-result p1

    if-eqz p1, :cond_8

    goto :goto_6

    :cond_8
    const/4 v12, 0x0

    :goto_6
    invoke-virtual {v0, v12}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setEnabled(Z)V

    .line 126
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v9
.end method

.method private init(Landroid/content/Context;)V
    .locals 4

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    .line 4
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->ATTRS:[I

    sget v1, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->DEF_STYLE_ATTR:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 5
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__TextSelectionToolbarIcons_pspdf__iconsColor:I

    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getDefaultIconsColor()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColor:I

    .line 6
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__TextSelectionToolbarIcons_pspdf__iconsColorActivated:I

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getDefaultIconsColorActivated()I

    move-result v1

    .line 9
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColorActivated:I

    .line 12
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__TextSelectionToolbarIcons_pspdf__shareIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_share:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->shareIcon:I

    .line 14
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__TextSelectionToolbarIcons_pspdf__copyIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_content_copy:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->copyIcon:I

    .line 16
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__TextSelectionToolbarIcons_pspdf__speakIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_hearing:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->speakIcon:I

    .line 18
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__TextSelectionToolbarIcons_pspdf__highlightIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_highlight:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->highlightIcon:I

    .line 20
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__TextSelectionToolbarIcons_pspdf__searchIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_search:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->searchIcon:I

    .line 22
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__TextSelectionToolbarIcons_pspdf__linkIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_link:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->linkIcon:I

    .line 24
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->closeButton:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    iget v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColor:I

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setIconColor(I)V

    .line 27
    iget p1, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->iconColor:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setDragButtonColor(I)V

    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V
    .locals 0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->unbindController()V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->applyAnnotationControllerChanges()V

    return-void
.end method

.method public bridge synthetic bindController(Lcom/pspdfkit/ui/special_mode/controller/base/SpecialModeController;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->bindController(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V

    return-void
.end method

.method protected handleMenuItemClick(Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 2
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    if-eqz v1, :cond_7

    if-eqz v0, :cond_7

    .line 3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->closeButton:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    if-ne p1, v2, :cond_1

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/SpecialModeController;->exitActiveMode()V

    goto/16 :goto_1

    .line 6
    :cond_1
    sget p1, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_share:I

    const-string v2, "page_index"

    const-string v3, "action"

    const-string v4, "perform_text_selection_action"

    if-ne v1, p1, :cond_2

    .line 7
    iget-object p1, v0, Lcom/pspdfkit/datastructures/TextSelection;->text:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_7

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v1, v0, Lcom/pspdfkit/datastructures/TextSelection;->text:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/pspdfkit/document/sharing/DocumentSharingManager;->shareText(Landroid/content/Context;Ljava/lang/String;)V

    .line 10
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    .line 11
    invoke-virtual {p1, v4}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    const-string v1, "share"

    .line 12
    invoke-virtual {p1, v3, v1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    iget v0, v0, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    .line 13
    invoke-virtual {p1, v0, v2}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    goto/16 :goto_1

    .line 16
    :cond_2
    sget p1, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_copy:I

    if-ne v1, p1, :cond_3

    .line 18
    iget-object p1, v0, Lcom/pspdfkit/datastructures/TextSelection;->text:Ljava/lang/String;

    .line 19
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v5, Lcom/pspdfkit/R$string;->pspdf__text_copied_to_clipboard:I

    const/16 v6, 0x30

    const-string v7, ""

    .line 21
    invoke-static {p1, v7, v1, v5, v6}, Lcom/pspdfkit/internal/j5;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;II)Z

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/SpecialModeController;->exitActiveMode()V

    .line 24
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    .line 25
    invoke-virtual {p1, v4}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    const-string v1, "clipboard"

    .line 26
    invoke-virtual {p1, v3, v1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    iget v0, v0, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    .line 27
    invoke-virtual {p1, v0, v2}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 28
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    goto :goto_1

    .line 29
    :cond_3
    sget p1, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_highlight:I

    if-ne v1, p1, :cond_4

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->highlightSelectedText()V

    goto :goto_1

    .line 31
    :cond_4
    sget p1, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_speak:I

    if-ne v1, p1, :cond_5

    .line 32
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v1, v0, Lcom/pspdfkit/datastructures/TextSelection;->text:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/pspdfkit/internal/bu;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 34
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    .line 35
    invoke-virtual {p1, v4}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    const-string v1, "tts"

    .line 36
    invoke-virtual {p1, v3, v1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    iget v0, v0, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    .line 37
    invoke-virtual {p1, v0, v2}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 38
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    goto :goto_1

    .line 39
    :cond_5
    sget p1, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_search:I

    if-ne v1, p1, :cond_6

    .line 40
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    if-eqz p1, :cond_7

    .line 41
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->searchSelectedText()V

    goto :goto_1

    .line 43
    :cond_6
    sget p1, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_link:I

    if-ne v1, p1, :cond_7

    .line 44
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    if-eqz p1, :cond_7

    .line 45
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->createLinkAboveSelectedText()V

    :cond_7
    :goto_1
    return-void
.end method

.method public isControllerBound()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public unbindController()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/bu;->a()V

    :cond_0
    const/4 v0, 0x0

    .line 6
    iput-boolean v0, p0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->areItemsSet:Z

    return-void
.end method

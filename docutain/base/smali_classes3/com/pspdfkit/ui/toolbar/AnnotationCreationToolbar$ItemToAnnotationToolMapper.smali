.class public interface abstract Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar$ItemToAnnotationToolMapper;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ItemToAnnotationToolMapper"
.end annotation


# virtual methods
.method public abstract getItemToAnnotationToolMapping()Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray<",
            "Landroid/util/Pair<",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract isStyleIndicatorCircleEnabled(I)Z
.end method

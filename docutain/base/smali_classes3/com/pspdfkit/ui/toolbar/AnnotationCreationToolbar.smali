.class public Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;
.super Lcom/pspdfkit/ui/toolbar/ContextualToolbar;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;
.implements Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar$ItemToAnnotationToolMapper;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/ui/toolbar/ContextualToolbar<",
        "Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;",
        ">;",
        "Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;",
        "Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;",
        "Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;"
    }
.end annotation


# static fields
.field private static final ATTRS:[I

.field private static final DEF_STYLE_ATTR:I


# instance fields
.field controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

.field defaultItemToAnnotationToolMappings:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/util/Pair<",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;",
            ">;>;"
        }
    .end annotation
.end field

.field private iconColor:I

.field private iconColorActivated:I

.field public itemToAnnotationToolMapper:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar$ItemToAnnotationToolMapper;

.field menuItemsWithStyleIndicators:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private redoIcon:I

.field private undoIcon:I

.field private undoManager:Lcom/pspdfkit/undo/UndoManager;

.field private undoRedoDrawable:Lcom/pspdfkit/internal/wu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons:[I

    sput-object v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->ATTRS:[I

    .line 7
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__annotationCreationToolbarIconsStyle:I

    sput v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->DEF_STYLE_ATTR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->defaultItemToAnnotationToolMappings:Landroid/util/SparseArray;

    .line 6
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->menuItemsWithStyleIndicators:Ljava/util/Set;

    .line 18
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    new-instance p2, Landroid/util/SparseArray;

    invoke-direct {p2}, Landroid/util/SparseArray;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->defaultItemToAnnotationToolMappings:Landroid/util/SparseArray;

    .line 24
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->menuItemsWithStyleIndicators:Ljava/util/Set;

    .line 41
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    new-instance p2, Landroid/util/SparseArray;

    invoke-direct {p2}, Landroid/util/SparseArray;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->defaultItemToAnnotationToolMappings:Landroid/util/SparseArray;

    .line 47
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->menuItemsWithStyleIndicators:Ljava/util/Set;

    .line 69
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->init(Landroid/content/Context;)V

    return-void
.end method

.method private addColorPickerMenuItem(Landroid/content/Context;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    iget v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColor:I

    iget v2, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColorActivated:I

    .line 4
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/o5;->a(Landroid/content/Context;II)Lcom/pspdfkit/internal/o5;

    move-result-object v2

    sget v0, Lcom/pspdfkit/R$string;->pspdf__edit_menu_color:I

    const/4 v3, 0x0

    .line 5
    invoke-static {p1, v0, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 6
    iget v4, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColor:I

    iget v5, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColorActivated:I

    sget-object v6, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->END:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/4 v7, 0x0

    move-object v0, p1

    .line 7
    invoke-static/range {v0 .. v7}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object p1

    const/4 v0, 0x0

    .line 16
    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setTintingEnabled(Z)V

    const/4 v0, 0x4

    .line 17
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 18
    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addUndoRedoMenuItem(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/configuration/PdfConfiguration;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    move-object v9, p1

    move-object/from16 v10, p3

    if-eqz p2, :cond_0

    .line 1
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isUndoEnabled()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2
    :cond_0
    sget v2, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_undo:I

    iget v1, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoIcon:I

    .line 5
    invoke-static {p1, v1}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget v1, Lcom/pspdfkit/R$string;->pspdf__undo:I

    const/4 v11, 0x0

    .line 6
    invoke-static {p1, v1, v11}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v4

    .line 7
    iget v5, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColor:I

    iget v6, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColorActivated:I

    sget-object v12, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->END:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/4 v8, 0x0

    move-object v1, p1

    move-object v7, v12

    .line 8
    invoke-static/range {v1 .. v8}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v1

    .line 17
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->getUseAlternateBackground()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setUseAlternateBackground(Z)V

    .line 18
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_1

    .line 20
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isRedoEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 21
    :cond_1
    sget v2, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_redo:I

    iget v1, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->redoIcon:I

    .line 24
    invoke-static {p1, v1}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget v1, Lcom/pspdfkit/R$string;->pspdf__redo:I

    .line 25
    invoke-static {p1, v1, v11}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v4

    .line 26
    iget v5, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColor:I

    iget v6, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColorActivated:I

    const/4 v8, 0x0

    move-object v1, p1

    move-object v7, v12

    .line 27
    invoke-static/range {v1 .. v8}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v1

    .line 36
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    :cond_2
    new-instance v7, Lcom/pspdfkit/internal/wu;

    const/4 v1, 0x1

    const/4 v13, 0x0

    if-eqz p2, :cond_4

    .line 41
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isUndoEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v3, 0x1

    :goto_1
    if-eqz p2, :cond_6

    .line 42
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isRedoEnabled()Z

    move-result v2

    if-eqz v2, :cond_5

    goto :goto_2

    :cond_5
    const/4 v4, 0x0

    goto :goto_3

    :cond_6
    :goto_2
    const/4 v4, 0x1

    :goto_3
    iget v5, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoIcon:I

    iget v6, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->redoIcon:I

    move-object v1, v7

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/wu;-><init>(Landroid/content/Context;ZZII)V

    iput-object v7, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoRedoDrawable:Lcom/pspdfkit/internal/wu;

    .line 45
    sget v2, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_undo:I

    sget v1, Lcom/pspdfkit/R$string;->pspdf__undo:I

    .line 46
    invoke-static {p1, v1, v11}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v4

    .line 47
    iget v5, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColor:I

    iget v6, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColorActivated:I

    const/4 v8, 0x0

    move-object v1, p1

    move-object v3, v7

    move-object v7, v12

    .line 48
    invoke-static/range {v1 .. v8}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v1

    .line 57
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->getUseAlternateBackground()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setUseAlternateBackground(Z)V

    .line 59
    sget v2, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_undo_redo:I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v2, v12, v13, v3, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createGroupItem(ILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;ZLjava/util/List;Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v1

    .line 65
    invoke-virtual {v1, v13}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setOpenSubmenuOnClick(Z)V

    .line 66
    invoke-virtual {v1, v13}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setCloseSubmenuOnItemClick(Z)V

    .line 67
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->getUseAlternateBackground()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setUseAlternateBackground(Z)V

    .line 68
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->updateUndoRedoButtons()V

    :cond_7
    return-void
.end method

.method private applyAnnotationControllerChanges(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->updateUndoRedoButtons()V

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->updateColorPickerIcons()V

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->updateActiveAnnotationTool()V

    if-eqz p1, :cond_1

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->notifyToolbarChanged()V

    .line 11
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getGroupedMenuItems()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->initializeStyleIndicatorCircleIcons(Ljava/util/List;)V

    return-void
.end method

.method private bindUndoManager(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 1

    .line 1
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isUndoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    .line 4
    invoke-interface {p1, p0}, Lcom/pspdfkit/undo/UndoManager;->addOnUndoHistoryChangeListener(Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;)V

    :cond_0
    return-void
.end method

.method private generateMenuItems()Ljava/util/List;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 1
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v9

    .line 2
    iget-object v1, v0, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->closeButton:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    iget v2, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColor:I

    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setIconColor(I)V

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v10

    .line 5
    iget-object v1, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    move-object v11, v1

    goto :goto_0

    :cond_0
    move-object v11, v2

    .line 6
    :goto_0
    new-instance v12, Ljava/util/ArrayList;

    const/16 v1, 0x14

    invoke-direct {v12, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 7
    invoke-virtual {v9}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v3, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->ATTRS:[I

    sget v4, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->DEF_STYLE_ATTR:I

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v13

    .line 9
    invoke-static {}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->values()[Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    move-result-object v14

    array-length v15, v14

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v15, :cond_3

    aget-object v7, v14, v8

    .line 10
    invoke-direct {v0, v11, v10, v7}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->shouldToolMenuItemBeShown(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/hb;Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 11
    iget v1, v7, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->styleableId:I

    iget v2, v7, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->drawableId:I

    .line 12
    invoke-virtual {v13, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 13
    invoke-static {v9, v1}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 18
    iget v2, v7, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->id:I

    .line 22
    invoke-direct {v0, v9, v7}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->getToolTitle(Landroid/content/Context;Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;)Ljava/lang/String;

    move-result-object v4

    iget v5, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColor:I

    iget v6, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColorActivated:I

    sget-object v16, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->START:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/16 v17, 0x1

    move-object v1, v9

    move-object/from16 v18, v10

    move-object v10, v7

    move-object/from16 v7, v16

    move/from16 v16, v8

    move/from16 v8, v17

    .line 23
    invoke-static/range {v1 .. v8}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    iget-object v1, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->defaultItemToAnnotationToolMappings:Landroid/util/SparseArray;

    iget v2, v10, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->id:I

    new-instance v3, Landroid/util/Pair;

    iget-object v4, v10, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->annotationTool:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    iget-object v5, v10, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->annotationToolVariant:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 36
    iget-boolean v1, v10, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->isStyleIndicatorEnabled:Z

    if-eqz v1, :cond_2

    .line 37
    iget-object v1, v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->menuItemsWithStyleIndicators:Ljava/util/Set;

    iget v2, v10, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->id:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    move/from16 v16, v8

    move-object/from16 v18, v10

    :cond_2
    :goto_2
    add-int/lit8 v8, v16, 0x1

    move-object/from16 v10, v18

    goto :goto_1

    .line 43
    :cond_3
    invoke-virtual {v13}, Landroid/content/res/TypedArray;->recycle()V

    .line 45
    invoke-direct {v0, v9, v11, v12}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->addUndoRedoMenuItem(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Ljava/util/List;)V

    .line 46
    invoke-direct {v0, v9, v12}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->addColorPickerMenuItem(Landroid/content/Context;Ljava/util/List;)V

    return-object v12
.end method

.method private getAnnotationToolForItemId(I)Landroid/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair<",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->itemToAnnotationToolMapper:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar$ItemToAnnotationToolMapper;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar$ItemToAnnotationToolMapper;->getItemToAnnotationToolMapping()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->defaultItemToAnnotationToolMappings:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Landroid/util/Pair;

    :cond_1
    return-object v0
.end method

.method private getItemIdForAnnotationTool(Landroid/util/Pair;)Ljava/lang/Integer;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair<",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->itemToAnnotationToolMapper:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar$ItemToAnnotationToolMapper;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar$ItemToAnnotationToolMapper;->getItemToAnnotationToolMapping()Landroid/util/SparseArray;

    move-result-object v0

    const/4 v2, 0x0

    .line 4
    :goto_0
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 5
    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    .line 6
    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    invoke-virtual {v4, p1}, Landroid/util/Pair;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 7
    invoke-virtual {p0, v3}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->findItemById(I)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 13
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->defaultItemToAnnotationToolMappings:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->defaultItemToAnnotationToolMappings:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 15
    iget-object v2, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->defaultItemToAnnotationToolMappings:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    invoke-virtual {v2, p1}, Landroid/util/Pair;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 16
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->findItemById(I)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method

.method private getToolTitle(Landroid/content/Context;Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;)Ljava/lang/String;
    .locals 1

    .line 1
    iget p2, p2, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->stringId:I

    const/4 v0, 0x0

    .line 2
    invoke-static {p1, p2, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private init(Landroid/content/Context;)V
    .locals 4

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    .line 4
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->ATTRS:[I

    sget v1, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->DEF_STYLE_ATTR:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 5
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__iconsColor:I

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getDefaultIconsColor()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColor:I

    .line 7
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__iconsColorActivated:I

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getDefaultIconsColorActivated()I

    move-result v1

    .line 10
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColorActivated:I

    .line 13
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__undoIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_undo:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoIcon:I

    .line 15
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationCreationToolbarIcons_pspdf__redoIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_redo:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->redoIcon:I

    .line 17
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->closeButton:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    iget v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColor:I

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setIconColor(I)V

    .line 21
    iget p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColor:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setDragButtonColor(I)V

    const/4 p1, 0x1

    .line 22
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setDraggable(Z)V

    .line 25
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/16 v0, 0x21c

    .line 26
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;I)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 27
    sget-object p1, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;->LEFT:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;->TOP:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    .line 29
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->get(Landroid/content/Context;)Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->getLastToolbarPosition(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;)Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    move-result-object p1

    .line 30
    new-instance v0, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams;

    const-class v1, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    invoke-static {v1}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams;-><init>(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;Ljava/util/EnumSet;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 32
    new-instance p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarGroupingRule;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarGroupingRule;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItemGroupingRule(Lcom/pspdfkit/ui/toolbar/grouping/MenuItemGroupingRule;)V

    .line 33
    invoke-virtual {p0, v3}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setUseBackButtonForCloseWhenHorizontal(Z)V

    return-void
.end method

.method private initializeStyleIndicatorCircleIcons(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->hasSubmenu()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getSubMenuItems()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->initializeStyleIndicatorCircleIcons(Ljava/util/List;)V

    :cond_1
    const/4 v1, 0x0

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->hasSubmenu()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getDefaultSelectedMenuItem()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 11
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->isStyleIndicatorCircleEnabled(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 12
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->getAnnotationToolForItemId(I)Landroid/util/Pair;

    move-result-object v1

    goto :goto_1

    .line 14
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->isStyleIndicatorCircleEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 15
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->getAnnotationToolForItemId(I)Landroid/util/Pair;

    move-result-object v1

    :cond_3
    :goto_1
    if-eqz v1, :cond_4

    .line 19
    iget-object v2, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v2

    .line 20
    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    iget-object v4, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    invoke-interface {v2, v3, v4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result v3

    .line 21
    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 22
    invoke-interface {v2, v4, v1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getThickness(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)F

    move-result v1

    .line 23
    invoke-virtual {v0, v3, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->showColorIndicatorCircle(IF)V

    goto :goto_0

    .line 25
    :cond_4
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->hideColorIndicatorCircle()V

    goto :goto_0

    :cond_5
    return-void
.end method

.method private isAnnotationMenuItem(ILjava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;",
            ">;)Z"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->getAnnotationToolForItemId(I)Landroid/util/Pair;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    .line 4
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->findItemById(ILjava/util/List;)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->hasSubmenu()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getSubMenuItems()Ljava/util/List;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getSubMenuItems()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    .line 8
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getSubMenuItems()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->isAnnotationMenuItem(ILjava/util/List;)Z

    move-result v0

    goto :goto_0

    :cond_1
    return v0
.end method

.method private isStyleIndicatorCircleEnabled(I)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->itemToAnnotationToolMapper:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar$ItemToAnnotationToolMapper;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar$ItemToAnnotationToolMapper;->isStyleIndicatorCircleEnabled(I)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->menuItemsWithStyleIndicators:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    :cond_1
    return v0
.end method

.method private setLatestUsedToolAsDefault(Ljava/util/List;Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/util/Pair<",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;",
            ">;>;",
            "Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getSubMenuItems()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 3
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x0

    const v2, 0x7fffffff

    .line 7
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    .line 9
    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->getAnnotationToolForItemId(I)Landroid/util/Pair;

    move-result-object v5

    invoke-interface {p1, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    if-ge v5, v2, :cond_0

    move-object v1, v4

    move v2, v5

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    .line 17
    invoke-virtual {p2, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setDefaultSelectedMenuItem(Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;)V

    goto :goto_1

    .line 18
    :cond_2
    invoke-virtual {p2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getDefaultSelectedMenuItem()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object p1

    if-nez p1, :cond_3

    const/4 p1, 0x0

    .line 19
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setDefaultSelectedMenuItem(Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;)V

    :cond_3
    :goto_1
    return-void
.end method

.method private shouldToolMenuItemBeShown(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/hb;Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;)Z
    .locals 3

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->ERASER_ITEM:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne p3, v0, :cond_2

    if-eqz p1, :cond_0

    .line 2
    sget-object p3, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    .line 3
    invoke-virtual {p2, p1, p3}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/AnnotationType;)Z

    move-result p3

    if-eqz p3, :cond_1

    sget-object p3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->ERASER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 4
    invoke-virtual {p2, p1, p3}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1

    :cond_2
    if-eqz p1, :cond_3

    .line 6
    iget-object p3, p3, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolMenuItem;->annotationTool:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-virtual {p2, p1, p3}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Z

    move-result p1

    if-eqz p1, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    return v1
.end method

.method private unbindUndoManager()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p0}, Lcom/pspdfkit/undo/UndoManager;->removeOnUndoHistoryChangeListener(Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    :cond_0
    return-void
.end method

.method private updateActiveAnnotationTool()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v1

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 5
    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NONE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-ne v0, v2, :cond_1

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->deselectCurrentMenuItem()V

    goto :goto_0

    .line 8
    :cond_1
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 9
    invoke-direct {p0, v2}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->getItemIdForAnnotationTool(Landroid/util/Pair;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 11
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->findItemById(I)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 13
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->selectMenuItem(Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;)Z

    :cond_2
    :goto_0
    return-void
.end method

.method private updateColorPickerIcons()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->shouldDisplayPicker()Z

    move-result v0

    .line 5
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    invoke-virtual {p0, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->findItemById(I)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->iconColor:I

    iget-object v4, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getColor()I

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/pspdfkit/internal/o5;->a(Landroid/content/Context;II)Lcom/pspdfkit/internal/o5;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 10
    :cond_1
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItemVisibility(II)Z

    return-void
.end method

.method private updateStyleIndicatorCircleIcons()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v1

    if-eqz v0, :cond_8

    if-eqz v1, :cond_8

    .line 7
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 8
    invoke-direct {p0, v2}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->getItemIdForAnnotationTool(Landroid/util/Pair;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 12
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getGroupedMenuItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    move-object v3, v2

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    .line 13
    invoke-virtual {v4}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getDefaultSelectedMenuItem()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 14
    invoke-virtual {v4}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getDefaultSelectedMenuItem()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ne v5, v6, :cond_2

    .line 15
    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    .line 19
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->findItemById(I)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v1

    if-eqz v3, :cond_4

    .line 20
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->findItemById(I)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v2

    .line 22
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->isStyleIndicatorCircleEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getColor()I

    move-result v0

    .line 24
    iget-object v3, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getThickness()F

    move-result v3

    if-eqz v1, :cond_5

    .line 25
    invoke-virtual {v1, v0, v3}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->showColorIndicatorCircle(IF)V

    :cond_5
    if-eqz v2, :cond_8

    .line 26
    invoke-virtual {v2, v0, v3}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->showColorIndicatorCircle(IF)V

    goto :goto_1

    :cond_6
    if-eqz v1, :cond_7

    .line 28
    invoke-virtual {v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->hideColorIndicatorCircle()V

    :cond_7
    if-eqz v2, :cond_8

    .line 29
    invoke-virtual {v2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->hideColorIndicatorCircle()V

    :cond_8
    :goto_1
    return-void
.end method

.method private updateUndoRedoButtons()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_3

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isUndoEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v3, 0x1

    :goto_2
    if-eqz v0, :cond_5

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isRedoEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v0, 0x1

    .line 7
    :goto_4
    iget-object v4, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    invoke-interface {v4}, Lcom/pspdfkit/undo/UndoManager;->canUndo()Z

    move-result v4

    .line 8
    iget-object v5, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    invoke-interface {v5}, Lcom/pspdfkit/undo/UndoManager;->canRedo()Z

    move-result v5

    .line 10
    sget v6, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_undo_redo:I

    if-eqz v3, :cond_6

    if-nez v4, :cond_7

    :cond_6
    if-eqz v0, :cond_8

    if-eqz v5, :cond_8

    :cond_7
    const/4 v1, 0x1

    :cond_8
    invoke-virtual {p0, v6, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItemEnabled(IZ)Z

    .line 14
    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_undo:I

    invoke-virtual {p0, v0, v4}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItemEnabled(IZ)Z

    .line 15
    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_redo:I

    invoke-virtual {p0, v0, v5}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItemEnabled(IZ)Z

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoRedoDrawable:Lcom/pspdfkit/internal/wu;

    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/wu;->b(Z)V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoRedoDrawable:Lcom/pspdfkit/internal/wu;

    invoke-virtual {v0, v5}, Lcom/pspdfkit/internal/wu;->a(Z)V

    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 3

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->unbindController()V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 5
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->addOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->addOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->bindUndoManager(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    .line 12
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    monitor-enter v0

    :try_start_0
    const-string v1, "configuration"

    .line 13
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->MEASUREMENT_TOOLS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isMeasurementsEnabled()Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    monitor-exit v0

    if-eqz p1, :cond_1

    .line 262
    new-instance p1, Lcom/pspdfkit/internal/n0;

    .line 263
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/n;->a:Lcom/pspdfkit/internal/n;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/pspdfkit/internal/n0;-><init>(Landroid/content/Context;Ljava/util/EnumSet;)V

    .line 264
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItemGroupingRule(Lcom/pspdfkit/ui/toolbar/grouping/MenuItemGroupingRule;)V

    .line 268
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->generateMenuItems()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItems(Ljava/util/List;)V

    .line 269
    invoke-direct {p0, v2}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->applyAnnotationControllerChanges(Z)V

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public bridge synthetic bindController(Lcom/pspdfkit/ui/special_mode/controller/base/SpecialModeController;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->bindController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    return-void
.end method

.method protected getUseAlternateBackground()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected handleMenuItemClick(Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v3, "Controller must be bind to the AnnotationCreationToolbar before menu clicks can be handled."

    invoke-static {v3, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->isSelectable()Z

    move-result v0

    xor-int/2addr v0, v2

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getDefaultSelectedMenuItem()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getDefaultSelectedMenuItem()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object p1

    .line 12
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_2

    return-void

    .line 15
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    if-ne v3, v4, :cond_3

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->toggleAnnotationInspector()V

    goto/16 :goto_4

    .line 17
    :cond_3
    iget-object v3, p0, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->closeButton:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    if-ne p1, v3, :cond_4

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/SpecialModeController;->exitActiveMode()V

    goto/16 :goto_4

    .line 19
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_undo:I

    if-eq v3, v4, :cond_c

    .line 20
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_group_undo_redo:I

    if-ne v3, v4, :cond_5

    goto :goto_3

    .line 24
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_redo:I

    if-ne v3, v4, :cond_6

    .line 25
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    if-eqz p1, :cond_d

    invoke-interface {p1}, Lcom/pspdfkit/undo/UndoManager;->canRedo()Z

    move-result p1

    if-eqz p1, :cond_d

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    invoke-interface {p1}, Lcom/pspdfkit/undo/UndoManager;->redo()V

    goto :goto_4

    .line 30
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->getAnnotationToolForItemId(I)Landroid/util/Pair;

    move-result-object p1

    if-eqz p1, :cond_d

    .line 32
    iget-object v3, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 33
    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 35
    iget-object v4, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 36
    invoke-interface {v4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NONE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-ne v4, v5, :cond_8

    if-eq v3, v5, :cond_7

    goto :goto_1

    :cond_7
    const/4 v4, 0x0

    goto :goto_2

    :cond_8
    :goto_1
    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_d

    .line 38
    iget-object v4, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 39
    invoke-interface {v4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v4

    if-ne v3, v4, :cond_9

    iget-object v4, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 40
    invoke-interface {v4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    const/4 v1, 0x1

    :cond_9
    if-eqz v1, :cond_a

    if-nez v0, :cond_a

    move-object v3, v5

    :cond_a
    if-ne v3, v5, :cond_b

    .line 44
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object p1

    .line 46
    :cond_b
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0, v3, p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->changeAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    goto :goto_4

    .line 47
    :cond_c
    :goto_3
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    if-eqz p1, :cond_d

    invoke-interface {p1}, Lcom/pspdfkit/undo/UndoManager;->canUndo()Z

    move-result p1

    if-eqz p1, :cond_d

    .line 48
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    invoke-interface {p1}, Lcom/pspdfkit/undo/UndoManager;->undo()V

    :cond_d
    :goto_4
    return-void
.end method

.method public isControllerBound()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method synthetic lambda$onMenuItemsGrouped$0$com-pspdfkit-ui-toolbar-AnnotationCreationToolbar()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getGroupedMenuItems()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->initializeStyleIndicatorCircleIcons(Ljava/util/List;)V

    return-void
.end method

.method public onAnnotationCreationModeSettingsChange(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->updateColorPickerIcons()V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->updateStyleIndicatorCircleIcons()V

    return-void
.end method

.method public onChangeAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 0

    const/4 p1, 0x0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->applyAnnotationControllerChanges(Z)V

    return-void
.end method

.method public onEnterAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 0

    return-void
.end method

.method public onExitAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 0

    return-void
.end method

.method public onMenuItemsGrouped(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->get(Landroid/content/Context;)Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->getLastAnnotationTools()Ljava/util/List;

    move-result-object v0

    .line 2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    .line 3
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v3

    invoke-direct {p0, v3, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->isAnnotationMenuItem(ILjava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    invoke-direct {p0, v0, v2}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->setLatestUsedToolAsDefault(Ljava/util/List;Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;)V

    goto :goto_0

    .line 8
    :cond_1
    new-instance v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->postOnAnimation(Ljava/lang/Runnable;)V

    return-object p1
.end method

.method public onUndoHistoryChanged(Lcom/pspdfkit/undo/UndoManager;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->updateUndoRedoButtons()V

    return-void
.end method

.method public setItemToAnnotationToolMapper(Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar$ItemToAnnotationToolMapper;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->itemToAnnotationToolMapper:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar$ItemToAnnotationToolMapper;

    return-void
.end method

.method public unbindController()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->removeOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->removeOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->unbindUndoManager()V

    :cond_0
    return-void
.end method

.class public interface abstract Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarLifecycleListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnContextualToolbarLifecycleListener"
.end annotation


# virtual methods
.method public abstract onDisplayContextualToolbar(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;)V
.end method

.method public abstract onPrepareContextualToolbar(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;)V
.end method

.method public abstract onRemoveContextualToolbar(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;)V
.end method

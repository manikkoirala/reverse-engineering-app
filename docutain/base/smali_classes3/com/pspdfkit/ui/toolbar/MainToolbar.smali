.class public Lcom/pspdfkit/ui/toolbar/MainToolbar;
.super Landroidx/appcompat/widget/Toolbar;
.source "SourceFile"


# instance fields
.field private mainToolbarStyle:Lcom/pspdfkit/internal/wh;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/toolbar/MainToolbar;->wrapThemedContext(Landroid/content/Context;)Landroid/view/ContextThemeWrapper;

    move-result-object p1

    invoke-direct {p0, p1}, Landroidx/appcompat/widget/Toolbar;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/MainToolbar;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/ui/toolbar/MainToolbar;->wrapThemedContext(Landroid/content/Context;)Landroid/view/ContextThemeWrapper;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Landroidx/appcompat/widget/Toolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/MainToolbar;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/ui/toolbar/MainToolbar;->wrapThemedContext(Landroid/content/Context;)Landroid/view/ContextThemeWrapper;

    move-result-object p1

    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/Toolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/MainToolbar;->init()V

    return-void
.end method

.method private init()V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/l;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/l;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/pspdfkit/internal/l;->c()Lcom/pspdfkit/internal/wh;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/MainToolbar;->mainToolbarStyle:Lcom/pspdfkit/internal/wh;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/wh;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/MainToolbar;->mainToolbarStyle:Lcom/pspdfkit/internal/wh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wh;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Landroidx/appcompat/widget/Toolbar;->setPopupTheme(I)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/MainToolbar;->mainToolbarStyle:Lcom/pspdfkit/internal/wh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wh;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Landroidx/appcompat/widget/Toolbar;->setTitleTextColor(I)V

    return-void
.end method

.method private static wrapThemedContext(Landroid/content/Context;)Landroid/view/ContextThemeWrapper;
    .locals 2

    .line 1
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-static {p0}, Lcom/pspdfkit/internal/xh;->b(Landroid/content/Context;)I

    move-result v1

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

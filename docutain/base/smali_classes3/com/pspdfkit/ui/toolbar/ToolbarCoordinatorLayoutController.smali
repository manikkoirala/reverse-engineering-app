.class public interface abstract Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayoutController;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract attachContextualToolbar()V
.end method

.method public abstract detachContextualToolbar()V
.end method

.method public abstract displayContextualToolbar(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;Z)V
.end method

.method public abstract onContextualToolbarChanged(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;)V
.end method

.method public abstract onContextualToolbarPositionChanged(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;)V
.end method

.method public abstract removeContextualToolbar(Z)V
.end method

.class public Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;
.super Lcom/pspdfkit/ui/toolbar/ContextualToolbar;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;
.implements Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/ui/toolbar/ContextualToolbar<",
        "Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;",
        ">;",
        "Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;",
        "Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;"
    }
.end annotation


# static fields
.field private static final SAVE_TEXT_SIZE_SP:F = 14.0f


# instance fields
.field controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

.field private iconColorActivated:Ljava/lang/Integer;

.field private iconColorInactive:Ljava/lang/Integer;

.field private redoIcon:I

.field private redoItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

.field private saveButtonFontSize:I

.field private saveButtonItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

.field private textIconColor:Ljava/lang/Integer;

.field private undoIcon:I

.field private undoItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 30
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 32
    iput-object p2, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 65
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 66
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    .line 67
    iput-object p2, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 105
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->init(Landroid/content/Context;)V

    return-void
.end method

.method private generateMenuItems()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 2
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 4
    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_undo:I

    iget v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->undoIcon:I

    .line 7
    invoke-static {v8, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget v0, Lcom/pspdfkit/R$string;->pspdf__undo:I

    const/4 v10, 0x0

    .line 8
    invoke-static {v8, v0, v10}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->iconColorInactive:Ljava/lang/Integer;

    .line 10
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->iconColorActivated:Ljava/lang/Integer;

    .line 11
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sget-object v11, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->END:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/4 v7, 0x0

    move-object v0, v8

    move-object v6, v11

    .line 12
    invoke-static/range {v0 .. v7}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->undoItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    .line 21
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    const/4 v12, 0x0

    const/4 v13, 0x1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->isUndoEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setEnabled(Z)V

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->undoItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_redo:I

    iget v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->redoIcon:I

    .line 27
    invoke-static {v8, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget v0, Lcom/pspdfkit/R$string;->pspdf__redo:I

    .line 28
    invoke-static {v8, v0, v10}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->iconColorInactive:Ljava/lang/Integer;

    .line 30
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->iconColorActivated:Ljava/lang/Integer;

    .line 31
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v7, 0x0

    move-object v0, v8

    move-object v6, v11

    .line 32
    invoke-static/range {v0 .. v7}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->redoItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    .line 41
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->isRedoEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v12, 0x1

    :cond_1
    invoke-virtual {v0, v12}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setEnabled(Z)V

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->redoItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    sget v0, Lcom/pspdfkit/R$id;->pspdf__document_editing_toolbar_item_save:I

    invoke-static {v8, v0, v11}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleTextItem(Landroid/content/Context;ILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->saveButtonItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    .line 47
    sget v0, Lcom/pspdfkit/R$string;->pspdf__save:I

    .line 48
    invoke-static {v8, v0, v10}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 49
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x41600000    # 14.0f

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;F)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->saveButtonFontSize:I

    .line 50
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->saveButtonItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setTitle(Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->updateSaveButtonEnabledState()V

    .line 52
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->saveButtonItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v0, v13}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setTextItemFirstFromEnd(Z)V

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->saveButtonItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v9
.end method

.method private init(Landroid/content/Context;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    .line 1
    :cond_0
    sget v0, Lcom/pspdfkit/R$id;->pspdf__content_editing_toolbar:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    .line 3
    sget v0, Lcom/pspdfkit/R$color;->pspdf__color_dark_blue:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 4
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    sget-object v1, Lcom/pspdfkit/R$styleable;->pspdf__ContentEditingToolbar:[I

    sget v2, Lcom/pspdfkit/R$attr;->pspdf__contentEditingToolbarStyle:I

    sget v3, Lcom/pspdfkit/R$style;->PSPDFKit_ContentEditingToolbar:I

    const/4 v4, 0x0

    .line 5
    invoke-virtual {p1, v4, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->iconColorInactive:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 12
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    .line 13
    :cond_1
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ContentEditingToolbar_pspdf__contentEditingToolbarIconColorInactive:I

    invoke-virtual {p1, v1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 14
    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->iconColorInactive:Ljava/lang/Integer;

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->iconColorActivated:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 21
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1

    .line 22
    :cond_2
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ContentEditingToolbar_pspdf__contentEditingToolbarIconColorActivated:I

    invoke-virtual {p1, v1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 23
    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->iconColorActivated:Ljava/lang/Integer;

    .line 29
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->textIconColor:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 30
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_2

    .line 31
    :cond_3
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ContentEditingToolbar_pspdf__contentEditingToolbarTextIconColor:I

    invoke-virtual {p1, v1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 32
    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->textIconColor:Ljava/lang/Integer;

    .line 38
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 40
    sget p1, Lcom/pspdfkit/R$drawable;->pspdf__ic_undo:I

    iput p1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->undoIcon:I

    .line 41
    sget p1, Lcom/pspdfkit/R$drawable;->pspdf__ic_redo:I

    iput p1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->redoIcon:I

    .line 43
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->closeButton:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->iconColorActivated:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setIconColor(I)V

    .line 44
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->closeButton:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    sget-object v0, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->START:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setPosition(Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;)V

    .line 46
    invoke-virtual {p0, v4}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItemGroupingRule(Lcom/pspdfkit/ui/toolbar/grouping/MenuItemGroupingRule;)V

    .line 47
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->generateMenuItems()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItems(Ljava/util/List;)V

    return-void
.end method

.method private shouldEnableSaveButton()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->isSaveEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private updateSaveButtonEnabledState()V
    .locals 14

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->shouldEnableSaveButton()Z

    move-result v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->saveButtonItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    .line 4
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->saveButtonItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    iget v3, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->saveButtonFontSize:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->textIconColor:Ljava/lang/Integer;

    .line 5
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sget-object v5, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    const/high16 v6, 0x3f000000    # 0.5f

    if-eqz v0, :cond_1

    const/high16 v7, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_1
    const/high16 v7, 0x3f000000    # 0.5f

    :goto_0
    const-string v8, "text"

    .line 6
    invoke-static {v1, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "font"

    invoke-static {v5, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    float-to-double v8, v7

    const-wide/16 v10, 0x0

    const/4 v12, 0x1

    cmpg-double v13, v10, v8

    if-gtz v13, :cond_2

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    cmpg-double v13, v8, v10

    if-gtz v13, :cond_2

    const/4 v8, 0x1

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    :goto_1
    if-eqz v8, :cond_3

    .line 34
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8, v12}, Landroid/graphics/Paint;-><init>(I)V

    .line 35
    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 36
    invoke-virtual {v8, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 37
    invoke-virtual {v8, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    const/16 v3, 0xff

    int-to-float v3, v3

    mul-float v7, v7, v3

    float-to-int v3, v7

    .line 38
    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 41
    invoke-virtual {v8}, Landroid/graphics/Paint;->ascent()F

    move-result v3

    neg-float v3, v3

    .line 42
    invoke-virtual {v8, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    add-float/2addr v4, v6

    float-to-int v4, v4

    .line 43
    invoke-virtual {v8}, Landroid/graphics/Paint;->descent()F

    move-result v5

    add-float/2addr v5, v3

    add-float/2addr v5, v6

    float-to-int v5, v5

    .line 45
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 46
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v6, 0x0

    .line 47
    invoke-virtual {v5, v1, v6, v3, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    const-string v1, "bitmap"

    .line 50
    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 53
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->saveButtonItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setEnabled(Z)V

    return-void

    .line 54
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "TextToBitmapUtils: Text alpha must be between 0 and 1."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V
    .locals 0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->unbindController()V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 4
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getContentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;->addOnContentEditingContentChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/undo/UndoManager;->addOnUndoHistoryChangeListener(Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->onUndoHistoryChanged(Lcom/pspdfkit/undo/UndoManager;)V

    return-void
.end method

.method public bridge synthetic bindController(Lcom/pspdfkit/ui/special_mode/controller/base/SpecialModeController;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->bindController(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V

    return-void
.end method

.method protected handleMenuItemClick(Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getDefaultSelectedMenuItem()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getDefaultSelectedMenuItem()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object p1

    :cond_0
    if-eqz p1, :cond_5

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 7
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->closeButton:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->finishContentEditingSession()V

    goto :goto_0

    .line 9
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->saveButtonItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->finishContentEditingSession(Z)V

    goto :goto_0

    .line 11
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->undoItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/undo/UndoManager;->undo()V

    goto :goto_0

    .line 13
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->redoItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    if-ne p1, v0, :cond_5

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/undo/UndoManager;->redo()V

    :cond_5
    :goto_0
    return-void
.end method

.method public isControllerBound()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isDraggable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public synthetic onContentChange(Ljava/util/UUID;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener$-CC;->$default$onContentChange(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;Ljava/util/UUID;)V

    return-void
.end method

.method public synthetic onContentSelectionChange(Ljava/util/UUID;IILcom/pspdfkit/internal/jt;Z)V
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener$-CC;->$default$onContentSelectionChange(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;Ljava/util/UUID;IILcom/pspdfkit/internal/jt;Z)V

    return-void
.end method

.method public synthetic onFinishEditingContentBlock(Ljava/util/UUID;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener$-CC;->$default$onFinishEditingContentBlock(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;Ljava/util/UUID;)V

    return-void
.end method

.method public synthetic onStartEditingContentBlock(Ljava/util/UUID;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener$-CC;->$default$onStartEditingContentBlock(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;Ljava/util/UUID;)V

    return-void
.end method

.method public onUndoHistoryChanged(Lcom/pspdfkit/undo/UndoManager;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->undoItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-interface {p1}, Lcom/pspdfkit/undo/UndoManager;->canUndo()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setEnabled(Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->redoItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-interface {p1}, Lcom/pspdfkit/undo/UndoManager;->canRedo()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setEnabled(Z)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->saveButtonItem:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result p1

    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->shouldEnableSaveButton()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->updateSaveButtonEnabledState()V

    :cond_0
    return-void
.end method

.method public unbindController()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getContentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;->removeOnContentEditingContentChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/undo/UndoManager;->removeOnUndoHistoryChangeListener(Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    :cond_0
    return-void
.end method

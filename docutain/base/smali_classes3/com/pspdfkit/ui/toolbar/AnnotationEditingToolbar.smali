.class public Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;
.super Lcom/pspdfkit/ui/toolbar/ContextualToolbar;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;
.implements Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/ui/toolbar/ContextualToolbar<",
        "Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;",
        ">;",
        "Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;",
        "Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;",
        "Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;"
    }
.end annotation


# static fields
.field private static final ATTRS:[I

.field private static final DEF_STYLE_ATTR:I


# instance fields
.field private annotationInstantCommentsIcon:I

.field private annotationNoteIcon:I

.field private annotationSharingMenuFragment:Lcom/pspdfkit/internal/c2;

.field controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

.field private copyIcon:I

.field private cutIcon:I

.field private deleteIcon:I

.field private editIcon:I

.field private iconColor:I

.field private iconColorActivated:I

.field private playIcon:I

.field private recordIcon:I

.field private redoIcon:I

.field private shareIcon:I

.field private undoIcon:I

.field private undoManager:Lcom/pspdfkit/undo/UndoManager;

.field private undoRedoDrawable:Lcom/pspdfkit/internal/wu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons:[I

    sput-object v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->ATTRS:[I

    .line 7
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__annotationEditingToolbarIconsStyle:I

    sput v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->DEF_STYLE_ATTR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 4
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 5
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 6
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->init(Landroid/content/Context;)V

    return-void
.end method

.method private addUndoRedoMenuItem(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/configuration/PdfConfiguration;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    move-object v9, p1

    move-object/from16 v10, p3

    if-eqz p2, :cond_0

    .line 1
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isUndoEnabled()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2
    :cond_0
    sget v2, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_undo:I

    iget v1, v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoIcon:I

    .line 5
    invoke-static {p1, v1}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget v1, Lcom/pspdfkit/R$string;->pspdf__undo:I

    const/4 v11, 0x0

    .line 6
    invoke-static {p1, v1, v11}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v4

    .line 7
    iget v5, v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v6, v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    sget-object v12, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->END:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/4 v8, 0x0

    move-object v1, p1

    move-object v7, v12

    .line 8
    invoke-static/range {v1 .. v8}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v1

    .line 17
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_1

    .line 19
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isRedoEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 20
    :cond_1
    sget v2, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_redo:I

    iget v1, v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->redoIcon:I

    .line 23
    invoke-static {p1, v1}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget v1, Lcom/pspdfkit/R$string;->pspdf__redo:I

    .line 24
    invoke-static {p1, v1, v11}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v4

    .line 25
    iget v5, v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v6, v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    const/4 v8, 0x0

    move-object v1, p1

    move-object v7, v12

    .line 26
    invoke-static/range {v1 .. v8}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v1

    .line 35
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    :cond_2
    new-instance v7, Lcom/pspdfkit/internal/wu;

    const/4 v1, 0x1

    const/4 v13, 0x0

    if-eqz p2, :cond_4

    .line 40
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isUndoEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v3, 0x1

    :goto_1
    if-eqz p2, :cond_6

    .line 41
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isRedoEnabled()Z

    move-result v2

    if-eqz v2, :cond_5

    goto :goto_2

    :cond_5
    const/4 v4, 0x0

    goto :goto_3

    :cond_6
    :goto_2
    const/4 v4, 0x1

    :goto_3
    iget v5, v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoIcon:I

    iget v6, v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->redoIcon:I

    move-object v1, v7

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/wu;-><init>(Landroid/content/Context;ZZII)V

    iput-object v7, v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoRedoDrawable:Lcom/pspdfkit/internal/wu;

    .line 44
    sget v2, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_undo:I

    sget v1, Lcom/pspdfkit/R$string;->pspdf__undo:I

    .line 45
    invoke-static {p1, v1, v11}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v4

    .line 46
    iget v5, v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v6, v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    const/4 v8, 0x0

    move-object v1, p1

    move-object v3, v7

    move-object v7, v12

    .line 47
    invoke-static/range {v1 .. v8}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v1

    .line 56
    sget v2, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_undo_redo:I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v2, v12, v13, v3, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createGroupItem(ILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;ZLjava/util/List;Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v1

    .line 62
    invoke-virtual {v1, v13}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setOpenSubmenuOnClick(Z)V

    .line 63
    invoke-virtual {v1, v13}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setCloseSubmenuOnItemClick(Z)V

    .line 64
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->updateUndoRedoButtons()V

    :cond_7
    return-void
.end method

.method private applyControllerChanges()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->generateMenuItems(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItems(Ljava/util/List;)V

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->updateIcons()V

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->updateUndoRedoButtons()V

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->notifyToolbarChanged()V

    return-void
.end method

.method private bindUndoManager()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-nez v0, :cond_0

    return-void

    .line 5
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isUndoEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    .line 9
    invoke-interface {v0, p0}, Lcom/pspdfkit/undo/UndoManager;->addOnUndoHistoryChangeListener(Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;)V

    :cond_1
    return-void
.end method

.method private editCurrentlySelectedAnnotation(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 6
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v1, v2, :cond_2

    .line 7
    check-cast v0, Lcom/pspdfkit/annotations/NoteAnnotation;

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->showAnnotationEditor(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->showAnnotationEditor(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    .line 12
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->toggleAnnotationInspector()V

    :goto_0
    return-void
.end method

.method private generateMenuItems(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    .line 1
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2
    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v10

    .line 3
    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v2

    .line 4
    new-instance v11, Ljava/util/ArrayList;

    const/4 v3, 0x4

    invoke-direct {v11, v3}, Ljava/util/ArrayList;-><init>(I)V

    if-nez v10, :cond_0

    return-object v11

    .line 10
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v3

    monitor-enter v3

    .line 11
    :try_start_0
    sget-object v4, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v3, v4}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v4

    const/4 v12, 0x0

    const/4 v13, 0x1

    if-nez v4, :cond_1

    invoke-virtual {v3}, Lcom/pspdfkit/internal/hb;->d()Z

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    monitor-exit v3

    const/4 v14, 0x0

    if-eqz v4, :cond_2

    .line 12
    invoke-virtual {v10}, Lcom/pspdfkit/annotations/Annotation;->isSignature()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 13
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_delete:I

    iget v2, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->deleteIcon:I

    .line 16
    invoke-static {v0, v2}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sget v2, Lcom/pspdfkit/R$string;->pspdf__delete:I

    .line 17
    invoke-static {v0, v2, v14}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v5

    .line 18
    iget v6, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v7, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    sget-object v8, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->END:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/4 v9, 0x0

    move-object v2, v0

    .line 19
    invoke-static/range {v2 .. v9}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v11

    .line 31
    :cond_2
    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v3

    .line 32
    invoke-virtual {v10}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v4

    invoke-interface {v4}, Lcom/pspdfkit/internal/pf;->hasInstantComments()Z

    move-result v15

    .line 34
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isUndoRedoEnabled()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 35
    invoke-direct {v1, v0, v3, v11}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->addUndoRedoMenuItem(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Ljava/util/List;)V

    .line 38
    :cond_3
    invoke-direct {v1, v2, v10}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isNoteItemEnabled(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    if-eqz v2, :cond_5

    if-eqz v15, :cond_4

    .line 42
    iget v2, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->annotationInstantCommentsIcon:I

    .line 43
    sget v3, Lcom/pspdfkit/R$string;->pspdf__note_icon_comment:I

    goto :goto_1

    .line 45
    :cond_4
    iget v2, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->annotationNoteIcon:I

    .line 46
    sget v3, Lcom/pspdfkit/R$string;->pspdf__edit_menu_note:I

    .line 48
    :goto_1
    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_annotation_note:I

    .line 51
    invoke-static {v0, v2}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 52
    invoke-static {v0, v3, v14}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v6

    .line 53
    iget v7, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v8, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    sget-object v9, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->END:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/16 v16, 0x0

    move-object v2, v0

    move v3, v4

    move-object v4, v5

    move-object v5, v6

    move v6, v7

    move v7, v8

    move-object v8, v9

    move/from16 v9, v16

    .line 54
    invoke-static/range {v2 .. v9}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v2

    .line 63
    invoke-virtual {v2, v13}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setDisplayOutsideOfSubmenuIfPossible(Z)V

    .line 64
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    if-nez v15, :cond_6

    .line 69
    invoke-virtual {v10}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v2, v3, :cond_6

    .line 70
    invoke-virtual {v10}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->SOUND:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v2, v3, :cond_6

    .line 71
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_picker:I

    iget v2, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v4, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    .line 74
    invoke-static {v0, v2, v4}, Lcom/pspdfkit/internal/o5;->a(Landroid/content/Context;II)Lcom/pspdfkit/internal/o5;

    move-result-object v4

    sget v2, Lcom/pspdfkit/R$string;->pspdf__edit_menu_color:I

    .line 75
    invoke-static {v0, v2, v14}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v5

    .line 76
    iget v6, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v7, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    sget-object v8, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->END:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/4 v9, 0x0

    move-object v2, v0

    .line 77
    invoke-static/range {v2 .. v9}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v2

    .line 86
    invoke-virtual {v2, v12}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setTintingEnabled(Z)V

    .line 87
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_6
    invoke-virtual {v10}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v2, v3, :cond_8

    if-eqz v15, :cond_7

    .line 94
    iget v2, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->annotationInstantCommentsIcon:I

    .line 95
    sget v3, Lcom/pspdfkit/R$string;->pspdf__note_icon_comment:I

    goto :goto_2

    .line 97
    :cond_7
    iget v2, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->editIcon:I

    .line 98
    sget v3, Lcom/pspdfkit/R$string;->pspdf__edit:I

    .line 100
    :goto_2
    sget v4, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_edit:I

    .line 103
    invoke-static {v0, v2}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 104
    invoke-static {v0, v3, v14}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v6

    .line 105
    iget v7, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v8, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    sget-object v9, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->END:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/4 v12, 0x0

    move-object v2, v0

    move v3, v4

    move-object v4, v5

    move-object v5, v6

    move v6, v7

    move v7, v8

    move-object v8, v9

    move v9, v12

    .line 106
    invoke-static/range {v2 .. v9}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    :cond_8
    invoke-virtual {v10}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->SOUND:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v2, v3, :cond_9

    .line 118
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_play:I

    iget v2, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->playIcon:I

    .line 121
    invoke-static {v0, v2}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sget v2, Lcom/pspdfkit/R$string;->pspdf__audio_play:I

    .line 122
    invoke-static {v0, v2, v14}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v5

    .line 123
    iget v6, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v7, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    sget-object v10, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->END:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/4 v9, 0x0

    move-object v2, v0

    move-object v8, v10

    .line 124
    invoke-static/range {v2 .. v9}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v2

    .line 133
    invoke-virtual {v2, v13}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setDisplayOutsideOfSubmenuIfPossible(Z)V

    .line 134
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_record:I

    iget v2, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->recordIcon:I

    .line 139
    invoke-static {v0, v2}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sget v2, Lcom/pspdfkit/R$string;->pspdf__audio_record:I

    .line 140
    invoke-static {v0, v2, v14}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v5

    .line 141
    iget v6, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v7, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    move-object v2, v0

    .line 142
    invoke-static/range {v2 .. v9}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v2

    .line 151
    invoke-virtual {v2, v13}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setDisplayOutsideOfSubmenuIfPossible(Z)V

    .line 152
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    if-nez v15, :cond_a

    .line 156
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isSharingVisible()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 157
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_share:I

    iget v2, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->shareIcon:I

    .line 160
    invoke-static {v0, v2}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sget v2, Lcom/pspdfkit/R$string;->pspdf__share:I

    .line 161
    invoke-static {v0, v2, v14}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v5

    .line 162
    iget v6, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v7, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    sget-object v8, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->END:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/4 v9, 0x0

    move-object v2, v0

    .line 163
    invoke-static/range {v2 .. v9}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v2

    .line 172
    invoke-virtual {v2, v13}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setDisplayOutsideOfSubmenuIfPossible(Z)V

    .line 173
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    if-nez v15, :cond_b

    .line 178
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isDeleteEnabled()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 179
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_delete:I

    iget v2, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->deleteIcon:I

    .line 182
    invoke-static {v0, v2}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sget v2, Lcom/pspdfkit/R$string;->pspdf__delete:I

    .line 183
    invoke-static {v0, v2, v14}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v5

    .line 184
    iget v6, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v7, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    sget-object v8, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->END:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/4 v9, 0x0

    move-object v2, v0

    .line 185
    invoke-static/range {v2 .. v9}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_b
    if-nez v15, :cond_c

    .line 197
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isCopyEnabled()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 198
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_copy:I

    iget v2, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->copyIcon:I

    .line 201
    invoke-static {v0, v2}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sget v2, Lcom/pspdfkit/R$string;->pspdf__copy:I

    .line 202
    invoke-static {v0, v2, v14}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v5

    .line 203
    iget v6, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v7, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    sget-object v10, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->START:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/4 v9, 0x0

    move-object v2, v0

    move-object v8, v10

    .line 204
    invoke-static/range {v2 .. v9}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isCutEnabled()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 214
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_cut:I

    iget v2, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->cutIcon:I

    .line 217
    invoke-static {v0, v2}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sget v2, Lcom/pspdfkit/R$string;->pspdf__cut:I

    .line 218
    invoke-static {v0, v2, v14}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v5

    .line 219
    iget v6, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    iget v7, v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    const/4 v9, 0x0

    move-object v2, v0

    move-object v8, v10

    .line 220
    invoke-static/range {v2 .. v9}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    return-object v11

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method private hasExtractPermission()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getPermissions()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/document/DocumentPermissions;->EXTRACT:Lcom/pspdfkit/document/DocumentPermissions;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private hasShareFeature(Lcom/pspdfkit/configuration/sharing/ShareFeatures;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledShareFeatures()Ljava/util/EnumSet;

    move-result-object v0

    .line 6
    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private init(Landroid/content/Context;)V
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->initIcons(Landroid/content/Context;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->closeButton:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    iget v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setIconColor(I)V

    .line 4
    iget p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setDragButtonColor(I)V

    .line 6
    new-instance p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarGroupingRule;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationEditingToolbarGroupingRule;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItemGroupingRule(Lcom/pspdfkit/ui/toolbar/grouping/MenuItemGroupingRule;)V

    return-void
.end method

.method private initIcons(Landroid/content/Context;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->ATTRS:[I

    sget v1, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->DEF_STYLE_ATTR:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 2
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__iconsColor:I

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getDefaultIconsColor()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    .line 4
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__iconsColorActivated:I

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getDefaultIconsColorActivated()I

    move-result v1

    .line 7
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColorActivated:I

    .line 10
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__editIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_edit:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->editIcon:I

    .line 12
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__deleteIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_delete:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->deleteIcon:I

    .line 14
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__shareIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_share:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->shareIcon:I

    .line 16
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__annotationNoteIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_replies:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->annotationNoteIcon:I

    .line 19
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__annotationInstantCommentIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_instant_comment:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->annotationInstantCommentsIcon:I

    .line 22
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__copyIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_content_copy:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->copyIcon:I

    .line 24
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__cutIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_content_cut:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->cutIcon:I

    .line 26
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__undoIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_undo:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoIcon:I

    .line 28
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__redoIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_redo:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->redoIcon:I

    .line 30
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__playIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_play:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->playIcon:I

    .line 32
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__recordIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_record:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->recordIcon:I

    .line 34
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private isCopyEnabled()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isCopyPasteEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->d()Lcom/pspdfkit/internal/b0;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lcom/pspdfkit/internal/b0;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isCutEnabled()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isLocked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isDeleteEnabled()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->FILE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isFileSharingEnabled()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isFileSharingVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->hasExtractPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->FILE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 6
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/FileAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FileAnnotation;->getFile()Lcom/pspdfkit/document/files/EmbeddedFile;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isFileSharingVisible()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->EMBEDDED_FILE_SHARING:Lcom/pspdfkit/configuration/sharing/ShareFeatures;

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->hasShareFeature(Lcom/pspdfkit/configuration/sharing/ShareFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->FILE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isImageSharingEnabled()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isImageSharingVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->hasExtractPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isImageSharingVisible()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->IMAGE_SHARING:Lcom/pspdfkit/configuration/sharing/ShareFeatures;

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->hasShareFeature(Lcom/pspdfkit/configuration/sharing/ShareFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/StampAnnotation;->hasBitmap()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isNoteItemEnabled(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationConfiguration()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v0

    .line 2
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->ANNOTATION_NOTE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v0, v1, v2}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/annotations/AnnotationType;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v0

    .line 6
    invoke-static {p2}, Lcom/pspdfkit/internal/ao;->e(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    if-nez v0, :cond_3

    .line 10
    :cond_0
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p2

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne p2, v1, :cond_2

    if-eqz v0, :cond_2

    .line 12
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object p2

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    monitor-enter p2

    :try_start_0
    const-string v0, "configuration"

    .line 13
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_REPLIES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 229
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getAnnotationReplyFeatures()Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;->DISABLED:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p1, v0, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    monitor-exit p2

    if-eqz p1, :cond_2

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit p2

    throw p1

    :cond_2
    const/4 v2, 0x0

    :cond_3
    :goto_1
    return v2
.end method

.method private isSharingEnabled()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isTextSharingEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isFileSharingEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isImageSharingEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isSoundSharingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isSharingVisible()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isTextSharingVisible()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isFileSharingVisible()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isImageSharingVisible()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isSoundSharingVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isSoundSharingEnabled()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isSoundSharingVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->hasExtractPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->SOUND:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 7
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/SoundAnnotation;

    .line 8
    invoke-static {v0}, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor;->soundAnnotationSupportsSharing(Lcom/pspdfkit/annotations/SoundAnnotation;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isSoundSharingVisible()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->SOUND_SHARING:Lcom/pspdfkit/configuration/sharing/ShareFeatures;

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->hasShareFeature(Lcom/pspdfkit/configuration/sharing/ShareFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->SOUND:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isTextSharingEnabled()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isTextSharingVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->hasExtractPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 6
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v0

    .line 7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isTextSharingVisible()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_1

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v1, v2, :cond_0

    .line 5
    sget-object v0, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->FREE_TEXT_ANNOTATION_SHARING:Lcom/pspdfkit/configuration/sharing/ShareFeatures;

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->hasShareFeature(Lcom/pspdfkit/configuration/sharing/ShareFeatures;)Z

    move-result v0

    return v0

    .line 6
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_1

    .line 7
    sget-object v0, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->NOTE_ANNOTATION_SHARING:Lcom/pspdfkit/configuration/sharing/ShareFeatures;

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->hasShareFeature(Lcom/pspdfkit/configuration/sharing/ShareFeatures;)Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private isUndoRedoEnabled()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->FILE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private shareCurrentlySelectedAnnotationContent()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 6
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isSharingEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 8
    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/c2;->a(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/c2;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->annotationSharingMenuFragment:Lcom/pspdfkit/internal/c2;

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/c2;->b()V

    :cond_2
    return-void
.end method

.method private unbindUndoManager()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p0}, Lcom/pspdfkit/undo/UndoManager;->removeOnUndoHistoryChangeListener(Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    :cond_0
    return-void
.end method

.method private updateIcons()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_9

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_3

    .line 3
    :cond_0
    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_picker:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->findItemById(I)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz v0, :cond_3

    .line 5
    iget-object v3, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->shouldDisplayPicker()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 6
    iget-object v3, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 7
    invoke-interface {v3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    .line 8
    iget-object v4, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 9
    invoke-interface {v4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v4

    sget v5, Lcom/pspdfkit/internal/ao;->a:I

    const-string v5, "annotation"

    .line 10
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 705
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v5

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v5, v6, :cond_1

    .line 706
    check-cast v4, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-static {v4}, Lcom/pspdfkit/internal/ws;->a(Lcom/pspdfkit/annotations/StampAnnotation;)I

    move-result v4

    goto :goto_0

    .line 708
    :cond_1
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v4

    .line 709
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    iget v6, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->iconColor:I

    invoke-static {v5, v6, v4}, Lcom/pspdfkit/internal/o5;->a(Landroid/content/Context;II)Lcom/pspdfkit/internal/o5;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 710
    invoke-virtual {v0, v3}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setEnabled(Z)V

    .line 711
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 713
    :cond_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 717
    :cond_3
    :goto_1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_delete:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->findItemById(I)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 719
    iget-object v3, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->isLocked()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setEnabled(Z)V

    .line 722
    :cond_4
    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_share:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->findItemById(I)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 724
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->isSharingEnabled()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setEnabled(Z)V

    .line 727
    :cond_5
    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_play:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->findItemById(I)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 729
    iget-object v3, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->shouldDisplayPlayAudioButton()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 730
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 732
    :cond_6
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 736
    :cond_7
    :goto_2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_record:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->findItemById(I)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 738
    iget-object v3, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->shouldDisplayRecordAudioButton()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 739
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 741
    :cond_8
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_9
    :goto_3
    return-void
.end method

.method private updateUndoRedoButtons()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_3

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isUndoEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v3, 0x1

    :goto_2
    if-eqz v0, :cond_5

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isRedoEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v0, 0x1

    .line 6
    :goto_4
    iget-object v4, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    invoke-interface {v4}, Lcom/pspdfkit/undo/UndoManager;->canUndo()Z

    move-result v4

    .line 7
    iget-object v5, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    invoke-interface {v5}, Lcom/pspdfkit/undo/UndoManager;->canRedo()Z

    move-result v5

    .line 8
    sget v6, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_undo_redo:I

    if-eqz v3, :cond_6

    if-nez v4, :cond_7

    :cond_6
    if-eqz v0, :cond_8

    if-eqz v5, :cond_8

    :cond_7
    const/4 v1, 0x1

    :cond_8
    invoke-virtual {p0, v6, v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItemEnabled(IZ)Z

    .line 11
    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_undo:I

    invoke-virtual {p0, v0, v4}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItemEnabled(IZ)Z

    .line 12
    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_redo:I

    invoke-virtual {p0, v0, v5}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->setMenuItemEnabled(IZ)Z

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoRedoDrawable:Lcom/pspdfkit/internal/wu;

    if-eqz v0, :cond_9

    .line 16
    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/wu;->b(Z)V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoRedoDrawable:Lcom/pspdfkit/internal/wu;

    invoke-virtual {v0, v5}, Lcom/pspdfkit/internal/wu;->a(Z)V

    :cond_9
    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 1

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 3
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 4
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->addOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->bindUndoManager()V

    .line 8
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->applyControllerChanges()V

    return-void
.end method

.method public bridge synthetic bindController(Lcom/pspdfkit/ui/special_mode/controller/base/SpecialModeController;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->bindController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V

    return-void
.end method

.method protected handleMenuItemClick(Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getDefaultSelectedMenuItem()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->getDefaultSelectedMenuItem()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object p1

    :cond_0
    if-eqz p1, :cond_e

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_2

    .line 8
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->closeButton:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/SpecialModeController;->exitActiveMode()V

    goto/16 :goto_2

    .line 11
    :cond_2
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_edit:I

    if-eq v0, v1, :cond_d

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_picker:I

    if-ne v0, v1, :cond_3

    goto/16 :goto_1

    .line 14
    :cond_3
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_annotation_note:I

    if-ne v0, v1, :cond_4

    const/4 p1, 0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->editCurrentlySelectedAnnotation(Z)V

    goto/16 :goto_2

    .line 16
    :cond_4
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_share:I

    if-ne v0, v1, :cond_5

    .line 17
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->shareCurrentlySelectedAnnotationContent()V

    goto/16 :goto_2

    .line 18
    :cond_5
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_delete:I

    if-ne v0, v1, :cond_6

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->deleteCurrentlySelectedAnnotation()V

    goto/16 :goto_2

    .line 20
    :cond_6
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_undo:I

    if-eq v0, v1, :cond_c

    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_group_undo_redo:I

    if-ne v0, v1, :cond_7

    goto :goto_0

    .line 25
    :cond_7
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_redo:I

    if-ne v0, v1, :cond_8

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    if-eqz p1, :cond_e

    invoke-interface {p1}, Lcom/pspdfkit/undo/UndoManager;->canRedo()Z

    move-result p1

    if-eqz p1, :cond_e

    .line 27
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    invoke-interface {p1}, Lcom/pspdfkit/undo/UndoManager;->redo()V

    goto :goto_2

    .line 29
    :cond_8
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_play:I

    if-ne v0, v1, :cond_9

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->enterAudioPlaybackMode()V

    goto :goto_2

    .line 31
    :cond_9
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_record:I

    if-ne v0, v1, :cond_a

    .line 32
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->enterAudioRecordingMode()V

    goto :goto_2

    .line 34
    :cond_a
    iget-object v1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    .line 35
    iget-object v2, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 36
    invoke-interface {v2}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/ag;->getPasteManager()Lcom/pspdfkit/internal/h7;

    move-result-object v2

    if-eqz v1, :cond_e

    if-eqz v2, :cond_e

    .line 38
    sget v3, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_copy:I

    if-ne v0, v3, :cond_b

    .line 40
    invoke-interface {v2, v1}, Lcom/pspdfkit/internal/h7;->a(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;)V

    .line 41
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_2

    .line 46
    :cond_b
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_toolbar_item_cut:I

    if-ne p1, v0, :cond_e

    .line 48
    invoke-interface {v2, v1}, Lcom/pspdfkit/internal/h7;->b(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_2

    .line 50
    :cond_c
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    if-eqz p1, :cond_e

    invoke-interface {p1}, Lcom/pspdfkit/undo/UndoManager;->canUndo()Z

    move-result p1

    if-eqz p1, :cond_e

    .line 51
    iget-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->undoManager:Lcom/pspdfkit/undo/UndoManager;

    invoke-interface {p1}, Lcom/pspdfkit/undo/UndoManager;->undo()V

    goto :goto_2

    :cond_d
    :goto_1
    const/4 p1, 0x0

    .line 52
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->editCurrentlySelectedAnnotation(Z)V

    :cond_e
    :goto_2
    return-void
.end method

.method public isControllerBound()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method synthetic lambda$handleMenuItemClick$0$com-pspdfkit-ui-toolbar-AnnotationEditingToolbar()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/base/SpecialModeController;->exitActiveMode()V

    :cond_0
    return-void
.end method

.method public onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->updateIcons()V

    return-void
.end method

.method public onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onChangeAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->applyControllerChanges()V

    return-void
.end method

.method public onEnterAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 0

    return-void
.end method

.method public onExitAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 0

    return-void
.end method

.method public onUndoHistoryChanged(Lcom/pspdfkit/undo/UndoManager;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->updateUndoRedoButtons()V

    return-void
.end method

.method public unbindController()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->removeOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V

    .line 4
    iput-object v1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->unbindUndoManager()V

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->annotationSharingMenuFragment:Lcom/pspdfkit/internal/c2;

    if-eqz v0, :cond_1

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/internal/c2;->a()V

    .line 12
    iput-object v1, p0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->annotationSharingMenuFragment:Lcom/pspdfkit/internal/c2;

    :cond_1
    return-void
.end method

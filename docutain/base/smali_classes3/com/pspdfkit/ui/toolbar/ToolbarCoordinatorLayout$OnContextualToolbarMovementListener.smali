.class public interface abstract Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarMovementListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnContextualToolbarMovementListener"
.end annotation


# virtual methods
.method public abstract onAttachContextualToolbar(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;)V
.end method

.method public abstract onDetachContextualToolbar(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;)V
.end method

.method public abstract onDragContextualToolbar(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;II)V
.end method

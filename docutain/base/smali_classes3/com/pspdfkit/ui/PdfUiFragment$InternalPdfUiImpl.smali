.class Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/bg;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/PdfUiFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InternalPdfUiImpl"
.end annotation


# instance fields
.field private final fragment:Lcom/pspdfkit/ui/PdfUiFragment;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/PdfUiFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;->fragment:Lcom/pspdfkit/ui/PdfUiFragment;

    return-void
.end method


# virtual methods
.method public getFragmentManager()Landroidx/fragment/app/FragmentManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;->fragment:Lcom/pspdfkit/ui/PdfUiFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    return-object v0
.end method

.method public getPdfParameters()Landroid/os/Bundle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;->fragment:Lcom/pspdfkit/ui/PdfUiFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public performApplyConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;->fragment:Lcom/pspdfkit/ui/PdfUiFragment;

    invoke-static {p1}, Lcom/pspdfkit/ui/PdfUiFragment;->-$$Nest$mapplyConfiguration(Lcom/pspdfkit/ui/PdfUiFragment;)V

    return-void
.end method

.method public setPdfView(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;->fragment:Lcom/pspdfkit/ui/PdfUiFragment;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfUiFragment;->-$$Nest$fgetviewContainer(Lcom/pspdfkit/ui/PdfUiFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfUiFragment$InternalPdfUiImpl;->fragment:Lcom/pspdfkit/ui/PdfUiFragment;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfUiFragment;->-$$Nest$fgetviewContainer(Lcom/pspdfkit/ui/PdfUiFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.class public final Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/PdfThumbnailBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConvertToDrawable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Function<",
        "Landroid/graphics/Bitmap;",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field private final crossFade:Z

.field private final placeholder:Landroid/graphics/drawable/Drawable;

.field private final renderStartTime:J

.field private final resources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;ZJLandroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "res"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;->resources:Landroid/content/res/Resources;

    .line 4
    iput-boolean p2, p0, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;->crossFade:Z

    .line 5
    iput-wide p3, p0, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;->renderStartTime:J

    .line 6
    iput-object p5, p0, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;->placeholder:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public apply(Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;
    .locals 5

    const-string v0, "bitmap"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;->crossFade:Z

    if-eqz v0, :cond_1

    .line 56
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 57
    iget-wide v2, p0, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;->renderStartTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x96

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 58
    :goto_0
    new-instance v1, Lcom/pspdfkit/internal/gb;

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;->resources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;->placeholder:Landroid/graphics/drawable/Drawable;

    invoke-direct {v1, v2, p1, v3, v0}, Lcom/pspdfkit/internal/gb;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;Z)V

    return-object v1

    .line 60
    :cond_1
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;->resources:Landroid/content/res/Resources;

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/PdfThumbnailBar$ConvertToDrawable;->apply(Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

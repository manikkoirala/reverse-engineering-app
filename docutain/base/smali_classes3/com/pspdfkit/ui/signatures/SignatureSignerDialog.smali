.class public Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;
.super Landroidx/fragment/app/DialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;
    }
.end annotation


# static fields
.field private static final FRAGMENT_TAG:Ljava/lang/String; = "com.pspdfkit.ui.dialog.SignatureSignerDialog.FRAGMENT_TAG"

.field private static final PARAM_BIOMETRIC_SIGNATURE_DATA:Ljava/lang/String; = "PSPDFKit.BiometricSignatureData"

.field private static final PARAM_FORMFIELD:Ljava/lang/String; = "PSPDFKit.FormField"

.field private static final PARAM_SAVE_BEFORE_SIGNING:Ljava/lang/String; = "PSPDFKit.SaveDocumentBeforeSigning"

.field private static final PARAM_SIGNATURE_APPEARANCE:Ljava/lang/String; = "PSPDFKit.SignatureAppearance"

.field private static final PARAM_SIGNATURE_METADATA:Ljava/lang/String; = "PSPDFKit.SignatureMetadata"

.field private static final PARAM_SIGNER_IDENTIFIER:Ljava/lang/String; = "PSPDFKit.SignerIdentifier"


# instance fields
.field private biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

.field private document:Lcom/pspdfkit/internal/zf;

.field private field:Lcom/pspdfkit/forms/SignatureFormField;

.field private listener:Lcom/pspdfkit/listeners/DocumentSigningListener;

.field private parceledAnnotation:Lcom/pspdfkit/internal/nm;

.field private saveDocumentBeforeSigning:Z

.field private shouldCallDismissListener:Z

.field private signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

.field private signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

.field private signer:Lcom/pspdfkit/signatures/signers/Signer;

.field private signingProgressDialog:Lcom/pspdfkit/internal/wr;

.field private signingSubscription:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method static bridge synthetic -$$Nest$fgetsigningProgressDialog(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;)Lcom/pspdfkit/internal/wr;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signingProgressDialog:Lcom/pspdfkit/internal/wr;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$minitiateSigning(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->initiateSigning()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/DialogFragment;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->shouldCallDismissListener:Z

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signingSubscription:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private initiateSigning()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signingProgressDialog:Lcom/pspdfkit/internal/wr;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signer:Lcom/pspdfkit/signatures/signers/Signer;

    if-nez v1, :cond_0

    goto/16 :goto_2

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->field:Lcom/pspdfkit/forms/SignatureFormField;

    if-nez v1, :cond_2

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->parceledAnnotation:Lcom/pspdfkit/internal/nm;

    .line 8
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/nm;->a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Maybe;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/WidgetAnnotation;

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getFormField()Lcom/pspdfkit/forms/FormField;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/forms/SignatureFormField;

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->field:Lcom/pspdfkit/forms/SignatureFormField;

    goto :goto_0

    .line 14
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismissAllowingStateLoss()V

    return-void

    .line 20
    :cond_2
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "pdf"

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/kb;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 22
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 23
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 26
    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->setupSignerUserInteractions()V

    .line 29
    iget-boolean v2, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->saveDocumentBeforeSigning:Z

    if-eqz v2, :cond_3

    .line 30
    iget-object v2, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->saveIfModifiedAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    goto :goto_1

    .line 33
    :cond_3
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v2}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    .line 36
    :goto_1
    new-instance v3, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;

    iget-object v4, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->field:Lcom/pspdfkit/forms/SignatureFormField;

    invoke-direct {v3, v4, v0}, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;-><init>(Lcom/pspdfkit/forms/SignatureFormField;Ljava/io/OutputStream;)V

    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    .line 37
    invoke-virtual {v3, v0}, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->biometricSignatureData(Lcom/pspdfkit/signatures/BiometricSignatureData;)Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

    .line 38
    invoke-virtual {v0, v3}, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureAppearance(Lcom/pspdfkit/signatures/SignatureAppearance;)Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    .line 39
    invoke-virtual {v0, v3}, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureMetadata(Lcom/pspdfkit/signatures/SignatureMetadata;)Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->build()Lcom/pspdfkit/signatures/signers/SignerOptions;

    move-result-object v0

    .line 42
    new-instance v3, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0, v0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;Lcom/pspdfkit/signatures/signers/SignerOptions;)V

    .line 43
    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Single;->flatMapCompletable(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->document:Lcom/pspdfkit/internal/zf;

    const/4 v3, 0x3

    .line 45
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 46
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v2, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0, v1}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;Ljava/io/File;)V

    new-instance v1, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;)V

    .line 47
    invoke-virtual {v0, v2, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signingSubscription:Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_2

    .line 48
    :cond_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Could not create temporary file for document signing."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 88
    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->listener:Lcom/pspdfkit/listeners/DocumentSigningListener;

    if-eqz v1, :cond_5

    invoke-interface {v1, v0}, Lcom/pspdfkit/listeners/DocumentSigningListener;->onDocumentSigningError(Ljava/lang/Throwable;)V

    :cond_5
    :goto_2
    return-void
.end method

.method private injectDocument(Lcom/pspdfkit/internal/zf;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->document:Lcom/pspdfkit/internal/zf;

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->initiateSigning()V

    return-void
.end method

.method public static restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/document/PdfDocument;)V
    .locals 1

    const-string v0, "com.pspdfkit.ui.dialog.SignatureSignerDialog.FRAGMENT_TAG"

    .line 1
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;

    if-eqz p0, :cond_0

    .line 3
    check-cast p1, Lcom/pspdfkit/internal/zf;

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->injectDocument(Lcom/pspdfkit/internal/zf;)V

    :cond_0
    return-void
.end method

.method private setDocumentSigningListener(Lcom/pspdfkit/listeners/DocumentSigningListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->listener:Lcom/pspdfkit/listeners/DocumentSigningListener;

    return-void
.end method

.method public static setListener(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/listeners/DocumentSigningListener;)V
    .locals 1

    const-string v0, "com.pspdfkit.ui.dialog.SignatureSignerDialog.FRAGMENT_TAG"

    .line 1
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;

    if-eqz p0, :cond_0

    .line 3
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->setDocumentSigningListener(Lcom/pspdfkit/listeners/DocumentSigningListener;)V

    :cond_0
    return-void
.end method

.method private setupSignerUserInteractions()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signer:Lcom/pspdfkit/signatures/signers/Signer;

    instance-of v1, v0, Lcom/pspdfkit/signatures/signers/InteractiveSigner;

    if-eqz v1, :cond_0

    .line 2
    check-cast v0, Lcom/pspdfkit/signatures/signers/InteractiveSigner;

    .line 3
    new-instance v1, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2;-><init>(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;)V

    invoke-interface {v0, v1}, Lcom/pspdfkit/signatures/signers/InteractiveSigner;->setLoadingFeedbackListener(Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;)V

    .line 35
    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signingProgressDialog:Lcom/pspdfkit/internal/wr;

    new-instance v2, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$3;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$3;-><init>(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;Lcom/pspdfkit/signatures/signers/InteractiveSigner;)V

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/wr;->setListener(Lcom/pspdfkit/internal/wr$b;)V

    :cond_0
    return-void
.end method

.method public static show(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;Lcom/pspdfkit/listeners/DocumentSigningListener;)V
    .locals 5

    const-string v0, "com.pspdfkit.ui.dialog.SignatureSignerDialog.FRAGMENT_TAG"

    .line 1
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;

    if-nez v1, :cond_0

    .line 3
    new-instance v1, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;

    invoke-direct {v1}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;-><init>()V

    .line 4
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 5
    new-instance v3, Lcom/pspdfkit/internal/nm;

    iget-object v4, p1, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->formField:Lcom/pspdfkit/forms/SignatureFormField;

    .line 7
    invoke-virtual {v4}, Lcom/pspdfkit/forms/FormField;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/pspdfkit/internal/nm;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    const-string v4, "PSPDFKit.FormField"

    .line 8
    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 11
    iget-object v3, p1, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->signerIdentifier:Ljava/lang/String;

    const-string v4, "PSPDFKit.SignerIdentifier"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    iget-object v3, p1, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    const-string v4, "PSPDFKit.BiometricSignatureData"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 13
    iget-object v3, p1, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

    const-string v4, "PSPDFKit.SignatureAppearance"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 14
    iget-object v3, p1, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    const-string v4, "PSPDFKit.SignatureMetadata"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 15
    iget-boolean v3, p1, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->saveDocumentBeforeSigning:Z

    const-string v4, "PSPDFKit.SaveDocumentBeforeSigning"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 16
    invoke-virtual {v1, v2}, Landroidx/fragment/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 17
    invoke-direct {v1, p2}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->setDocumentSigningListener(Lcom/pspdfkit/listeners/DocumentSigningListener;)V

    .line 18
    iget-object p1, p1, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->document:Lcom/pspdfkit/document/PdfDocument;

    check-cast p1, Lcom/pspdfkit/internal/zf;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->injectDocument(Lcom/pspdfkit/internal/zf;)V

    .line 21
    :cond_0
    invoke-virtual {v1}, Landroidx/fragment/app/DialogFragment;->isAdded()Z

    move-result p1

    if-nez p1, :cond_1

    .line 22
    invoke-virtual {v1, p0, v0}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method synthetic lambda$initiateSigning$0$com-pspdfkit-ui-signatures-SignatureSignerDialog(Lcom/pspdfkit/signatures/signers/SignerOptions;Ljava/lang/Boolean;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signer:Lcom/pspdfkit/signatures/signers/Signer;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/signatures/signers/Signer;->signFormFieldAsync(Lcom/pspdfkit/signatures/signers/SignerOptions;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method synthetic lambda$initiateSigning$1$com-pspdfkit-ui-signatures-SignatureSignerDialog(Ljava/io/File;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismiss()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->listener:Lcom/pspdfkit/listeners/DocumentSigningListener;

    if-eqz v0, :cond_0

    .line 3
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/listeners/DocumentSigningListener;->onDocumentSigned(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method synthetic lambda$initiateSigning$2$com-pspdfkit-ui-signatures-SignatureSignerDialog(Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismiss()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->listener:Lcom/pspdfkit/listeners/DocumentSigningListener;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0, p1}, Lcom/pspdfkit/listeners/DocumentSigningListener;->onDocumentSigningError(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "PSPDFKit.FormField"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/nm;

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->parceledAnnotation:Lcom/pspdfkit/internal/nm;

    const-string v0, "parceledAnnotation"

    const-string v1, "Make sure to only start this dialog using SignatureSignerDialog#show(FragmentManager, Options, DocumentSigningListener)"

    .line 3
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 7
    invoke-static {}, Lcom/pspdfkit/signatures/SignatureManager;->getSigners()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PSPDFKit.SignerIdentifier"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/signatures/signers/Signer;

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signer:Lcom/pspdfkit/signatures/signers/Signer;

    .line 8
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "PSPDFKit.BiometricSignatureData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/signatures/BiometricSignatureData;

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    .line 9
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "PSPDFKit.SignatureAppearance"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/signatures/SignatureAppearance;

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

    .line 10
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "PSPDFKit.SignatureMetadata"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/signatures/SignatureMetadata;

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signer:Lcom/pspdfkit/signatures/signers/Signer;

    if-nez p1, :cond_0

    .line 14
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 16
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "PSPDFKit.SaveDocumentBeforeSigning"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->saveDocumentBeforeSigning:Z

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .line 1
    new-instance p1, Lcom/pspdfkit/internal/wr;

    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/pspdfkit/internal/wr;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signingProgressDialog:Lcom/pspdfkit/internal/wr;

    .line 2
    new-instance v0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$1;-><init>(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;)V

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/wr;->setListener(Lcom/pspdfkit/internal/wr$b;)V

    .line 15
    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->initiateSigning()V

    .line 17
    new-instance p1, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 18
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/pspdfkit/R$string;->pspdf__certificate:I

    .line 19
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setTitle(I)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signingProgressDialog:Lcom/pspdfkit/internal/wr;

    .line 20
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    .line 21
    invoke-virtual {p1}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public onDestroy()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signingSubscription:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signingSubscription:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2
    iget-boolean p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->shouldCallDismissListener:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->signingSubscription:Lio/reactivex/rxjava3/disposables/Disposable;

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->listener:Lcom/pspdfkit/listeners/DocumentSigningListener;

    if-eqz p1, :cond_0

    .line 3
    invoke-interface {p1}, Lcom/pspdfkit/listeners/DocumentSigningListener;->onSigningCancelled()V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->shouldCallDismissListener:Z

    return-void
.end method

.method public onStop()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStop()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->shouldCallDismissListener:Z

    return-void
.end method

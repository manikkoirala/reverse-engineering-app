.class public Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;
.super Landroidx/fragment/app/Fragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$InternalListener;
    }
.end annotation


# static fields
.field private static final FRAGMENT_TAG:Ljava/lang/String; = "com.pspdfkit.ui.signatures.ElectronicSignatureFragment.FRAGMENT_TAG"

.field private static final LOG_TAG:Ljava/lang/String; = "PSPDFKit.ElectronicSignatureFragment"

.field private static final STATE_SIGNATURE_OPTIONS:Ljava/lang/String; = "STATE_SIGNATURE_OPTIONS"

.field private static final STATE_SIGNATURE_STORAGE_AVAILABILITY:Ljava/lang/String; = "STATE_SIGNATURE_STORAGE_AVAILABILITY"

.field private static final STATE_WAITING_FOR_SIGNATURE_PICKER_DIALOG:Ljava/lang/String; = "STATE_WAITING_FOR_SIGNATURE_PICKER_DIALOG"


# instance fields
.field private deletingSignaturesDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

.field private electronicSignatureDialog:Lcom/pspdfkit/internal/oa;

.field private isSignatureStorageAvailable:Z

.field private listener:Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;

.field private final signatureDialogListener:Lcom/pspdfkit/internal/or;

.field private signatureOptions:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

.field private signatureRetrievalDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

.field private signatureStorage:Lcom/pspdfkit/signatures/storage/SignatureStorage;

.field private storingSignaturesDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

.field private waitingForSignatureToBePicked:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetlistener(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;)Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->listener:Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetsignatureOptions(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;)Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetsignatureRetrievalDisposable(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;)Lio/reactivex/rxjava3/disposables/Disposable;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureRetrievalDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetwaitingForSignatureToBePicked(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->waitingForSignatureToBePicked:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputdeletingSignaturesDisposable(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->deletingSignaturesDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputelectronicSignatureDialog(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;Lcom/pspdfkit/internal/oa;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->electronicSignatureDialog:Lcom/pspdfkit/internal/oa;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputlistener(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->listener:Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputsignatureRetrievalDisposable(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureRetrievalDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputstoringSignaturesDisposable(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->storingSignaturesDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputwaitingForSignatureToBePicked(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->waitingForSignatureToBePicked:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcreateAndEvaluateSignatureOptions(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;)Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->createAndEvaluateSignatureOptions()Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetSignatureStorage(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;)Lcom/pspdfkit/signatures/storage/SignatureStorage;
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->getSignatureStorage()Lcom/pspdfkit/signatures/storage/SignatureStorage;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 125
    new-instance v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$InternalListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$InternalListener;-><init>(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$InternalListener-IA;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureDialogListener:Lcom/pspdfkit/internal/or;

    const/4 v0, 0x0

    .line 134
    iput-boolean v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->isSignatureStorageAvailable:Z

    const/4 v0, 0x1

    .line 140
    iput-boolean v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->waitingForSignatureToBePicked:Z

    return-void
.end method

.method private createAndEvaluateSignatureOptions()Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;

    invoke-direct {v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->build()Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureStorage:Lcom/pspdfkit/signatures/storage/SignatureStorage;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->isSignatureStorageAvailable:Z

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object v0

    sget-object v2, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;->NEVER_SAVE:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    if-eq v0, v2, :cond_2

    iget-boolean v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->isSignatureStorageAvailable:Z

    if-nez v0, :cond_2

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.ElectronicSignatureFragment"

    const-string v3, "`SignatureSavingStrategy` set to save signatures, but there is no `SignatureStorage` available. Please create one if you wish to save signatures."

    .line 13
    invoke-static {v1, v3, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    new-instance v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;-><init>(Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V

    .line 18
    invoke-virtual {v0, v2}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureSavingStrategy(Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;)Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->build()Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    .line 21
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    return-object v0
.end method

.method public static dismiss(Landroidx/fragment/app/FragmentManager;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->findFragment(Landroidx/fragment/app/FragmentManager;)Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->finish()V

    :cond_0
    return-void
.end method

.method private static findFragment(Landroidx/fragment/app/FragmentManager;)Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;
    .locals 1

    const-string v0, "com.pspdfkit.ui.signatures.ElectronicSignatureFragment.FRAGMENT_TAG"

    .line 1
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;

    return-object p0
.end method

.method private getSignatureStorage()Lcom/pspdfkit/signatures/storage/SignatureStorage;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureStorage:Lcom/pspdfkit/signatures/storage/SignatureStorage;

    return-object v0
.end method

.method static synthetic lambda$showSignatureEditorFragment$2(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.ElectronicSignatureFragment"

    const-string v2, "Failed to retrieve signatures from the signature storage."

    .line 1
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "STATE_WAITING_FOR_SIGNATURE_PICKER_DIALOG"

    const/4 v1, 0x0

    .line 1
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->waitingForSignatureToBePicked:Z

    const-string v0, "STATE_SIGNATURE_OPTIONS"

    .line 2
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    if-eqz v0, :cond_0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    :cond_0
    const-string v0, "STATE_SIGNATURE_STORAGE_AVAILABILITY"

    .line 4
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->isSignatureStorageAvailable:Z

    return-void
.end method

.method public static restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-static {p0, p1, v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    return-void
.end method

.method public static restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V
    .locals 2

    const-string v0, "fragmentManager"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    if-nez p1, :cond_0

    return-void

    .line 55
    :cond_0
    invoke-static {p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->findFragment(Landroidx/fragment/app/FragmentManager;)Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 57
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->setOnSignaturePickedListener(Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V

    .line 58
    invoke-direct {p0, p2}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->setSignatureStorage(Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    :cond_1
    return-void
.end method

.method private setSignatureStorage(Lcom/pspdfkit/signatures/storage/SignatureStorage;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureStorage:Lcom/pspdfkit/signatures/storage/SignatureStorage;

    return-void
.end method

.method public static show(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 66
    invoke-static {p0, p1, v0, v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->show(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    return-void
.end method

.method public static show(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V
    .locals 2

    const-string v0, "fragmentManager"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    invoke-static {p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->findFragment(Landroidx/fragment/app/FragmentManager;)Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;

    invoke-direct {v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;-><init>()V

    .line 57
    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->setOnSignaturePickedListener(Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V

    .line 58
    invoke-direct {v0, p3}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->setSignatureStorage(Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    .line 60
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    const-string p3, "STATE_SIGNATURE_OPTIONS"

    .line 61
    invoke-virtual {p1, p3, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 62
    invoke-virtual {v0, p1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "com.pspdfkit.ui.signatures.ElectronicSignatureFragment.FRAGMENT_TAG"

    .line 65
    invoke-static {p0, v0, p1}, Lcom/pspdfkit/internal/kc;->a(Landroidx/fragment/app/FragmentManager;Landroidx/fragment/app/Fragment;Ljava/lang/String;)Z

    :cond_1
    return-void
.end method

.method private showSignatureEditorFragment()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureDialogListener:Lcom/pspdfkit/internal/or;

    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->createAndEvaluateSignatureOptions()Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    move-result-object v2

    .line 2
    sget v3, Lcom/pspdfkit/internal/oa;->f:I

    const-string v3, "fragmentManager"

    .line 3
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "listener"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "signatureOptions"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "argumentName"

    .line 4
    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    .line 55
    invoke-static {v0, v3, v7}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 56
    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {v1, v4, v7}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 108
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-static {v2, v5, v7}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v3, "com.pspdfkit.ui.dialog.signatures.ElectronicSignatureDialog.FRAGMENT_TAG"

    .line 160
    invoke-virtual {v0, v3}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/oa;

    if-nez v4, :cond_0

    .line 161
    new-instance v4, Lcom/pspdfkit/internal/oa;

    invoke-direct {v4}, Lcom/pspdfkit/internal/oa;-><init>()V

    .line 162
    :cond_0
    invoke-static {v4, v1}, Lcom/pspdfkit/internal/oa;->a(Lcom/pspdfkit/internal/oa;Lcom/pspdfkit/internal/or;)V

    .line 163
    invoke-static {v4, v2}, Lcom/pspdfkit/internal/oa;->a(Lcom/pspdfkit/internal/oa;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V

    .line 164
    invoke-virtual {v4}, Landroidx/appcompat/app/AppCompatDialogFragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_1

    .line 165
    invoke-virtual {v4, v0, v3}, Landroidx/appcompat/app/AppCompatDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 166
    :cond_1
    iput-object v4, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->electronicSignatureDialog:Lcom/pspdfkit/internal/oa;

    const/4 v0, 0x1

    .line 168
    iput-boolean v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->waitingForSignatureToBePicked:Z

    .line 169
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureRetrievalDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 170
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 171
    iput-object v7, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureRetrievalDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 179
    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->getSignatureStorage()Lcom/pspdfkit/signatures/storage/SignatureStorage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 180
    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    .line 181
    invoke-virtual {v1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;->NEVER_SAVE:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    if-ne v1, v2, :cond_2

    goto :goto_0

    .line 186
    :cond_2
    new-instance v1, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$$ExternalSyntheticLambda1;

    invoke-direct {v1, v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 187
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 188
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;)V

    new-instance v2, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$$ExternalSyntheticLambda3;

    invoke-direct {v2}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$$ExternalSyntheticLambda3;-><init>()V

    .line 189
    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureRetrievalDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_1

    .line 190
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->electronicSignatureDialog:Lcom/pspdfkit/internal/oa;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/oa;->a(Ljava/util/List;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->electronicSignatureDialog:Lcom/pspdfkit/internal/oa;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatDialogFragment;->dismiss()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->electronicSignatureDialog:Lcom/pspdfkit/internal/oa;

    .line 6
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;)V

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method synthetic lambda$finish$0$com-pspdfkit-ui-signatures-ElectronicSignatureFragment()V
    .locals 3

    .line 1
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    .line 2
    invoke-static {v0, p0}, Lcom/pspdfkit/internal/kc;->a(Landroidx/fragment/app/FragmentManager;Landroidx/fragment/app/Fragment;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v0, "PSPDFKit.ElectronicSignatureFragment"

    const-string v2, "Dodged IllegalstateException in finish()"

    .line 8
    invoke-static {v0, v2, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method synthetic lambda$showSignatureEditorFragment$1$com-pspdfkit-ui-signatures-ElectronicSignatureFragment(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->electronicSignatureDialog:Lcom/pspdfkit/internal/oa;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/oa;->a(Ljava/util/List;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "STATE_SIGNATURE_OPTIONS"

    .line 5
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    if-eqz v0, :cond_0

    .line 7
    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    :cond_0
    if-eqz p1, :cond_1

    .line 13
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 18
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureDialogListener:Lcom/pspdfkit/internal/or;

    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->createAndEvaluateSignatureOptions()Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    move-result-object v1

    .line 19
    sget v2, Lcom/pspdfkit/internal/oa;->f:I

    const-string v2, "fragmentManager"

    .line 20
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "listener"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "signatureOptions"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "argumentName"

    .line 21
    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    .line 72
    invoke-static {p1, v2, v6}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 73
    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-static {v0, v3, v6}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 125
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    invoke-static {v1, v4, v6}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v2, "com.pspdfkit.ui.dialog.signatures.ElectronicSignatureDialog.FRAGMENT_TAG"

    .line 177
    invoke-virtual {p1, v2}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/oa;

    if-eqz p1, :cond_2

    .line 178
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/oa;->a(Lcom/pspdfkit/internal/oa;Lcom/pspdfkit/internal/or;)V

    .line 179
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/oa;->a(Lcom/pspdfkit/internal/oa;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V

    .line 180
    :cond_2
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->electronicSignatureDialog:Lcom/pspdfkit/internal/oa;

    if-nez p1, :cond_3

    .line 184
    iget-boolean p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->waitingForSignatureToBePicked:Z

    if-eqz p1, :cond_3

    .line 185
    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->showSignatureEditorFragment()V

    :cond_3
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->storingSignaturesDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->deletingSignaturesDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 5
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->waitingForSignatureToBePicked:Z

    const-string v1, "STATE_WAITING_FOR_SIGNATURE_PICKER_DIALOG"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    const-string v1, "STATE_SIGNATURE_OPTIONS"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 4
    iget-boolean v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->isSignatureStorageAvailable:Z

    const-string v1, "STATE_SIGNATURE_STORAGE_AVAILABILITY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public setOnSignaturePickedListener(Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->listener:Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;

    return-void
.end method

.class public Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

.field private final document:Lcom/pspdfkit/document/PdfDocument;

.field private final formField:Lcom/pspdfkit/forms/SignatureFormField;

.field private saveDocumentBeforeSigning:Z

.field private signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

.field private signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

.field private final signerIdentifier:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/forms/SignatureFormField;Lcom/pspdfkit/signatures/signers/Signer;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->saveDocumentBeforeSigning:Z

    const-string v0, "document"

    .line 17
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formField"

    .line 18
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signer"

    .line 19
    invoke-static {p3, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-static {p3}, Lcom/pspdfkit/internal/vr;->a(Lcom/pspdfkit/signatures/signers/Signer;)Ljava/lang/String;

    move-result-object p3

    const-string v0, "signerIdentifier"

    const-string v1, "The provided signer must be registered with the SignatureManager using SignatureManager#addSigner()."

    .line 23
    invoke-static {p3, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->document:Lcom/pspdfkit/document/PdfDocument;

    .line 28
    iput-object p2, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->formField:Lcom/pspdfkit/forms/SignatureFormField;

    .line 29
    iput-object p3, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->signerIdentifier:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public biometricSignatureData(Lcom/pspdfkit/signatures/BiometricSignatureData;)Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    return-object p0
.end method

.method public build()Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;
    .locals 10

    .line 1
    new-instance v9, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->document:Lcom/pspdfkit/document/PdfDocument;

    iget-object v2, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->formField:Lcom/pspdfkit/forms/SignatureFormField;

    iget-object v3, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->signerIdentifier:Ljava/lang/String;

    iget-object v4, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    iget-object v5, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

    iget-object v6, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    iget-boolean v7, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->saveDocumentBeforeSigning:Z

    const/4 v8, 0x0

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;-><init>(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/forms/SignatureFormField;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;Lcom/pspdfkit/signatures/SignatureAppearance;Lcom/pspdfkit/signatures/SignatureMetadata;ZLcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options-IA;)V

    return-object v9
.end method

.method public saveDocumentBeforeSigning(Z)Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->saveDocumentBeforeSigning:Z

    return-object p0
.end method

.method public signatureAppearance(Lcom/pspdfkit/signatures/SignatureAppearance;)Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

    return-object p0
.end method

.method public signatureMetadata(Lcom/pspdfkit/signatures/SignatureMetadata;)Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    return-object p0
.end method

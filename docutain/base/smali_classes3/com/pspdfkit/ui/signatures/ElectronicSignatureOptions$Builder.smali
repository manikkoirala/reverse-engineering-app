.class public final Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private signatureColorOptions:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

.field private signatureCreationModes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;",
            ">;"
        }
    .end annotation
.end field

.field private signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;->SAVE_IF_SELECTED:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 5
    invoke-static {}, Lcom/pspdfkit/configuration/signatures/SignatureColorOptions$-CC;->fromDefaults()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureColorOptions:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    .line 7
    sget-object v1, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;->DRAW:Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;->IMAGE:Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;->TYPE:Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 10
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureCreationModes:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V
    .locals 3

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    sget-object v0, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;->SAVE_IF_SELECTED:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 15
    invoke-static {}, Lcom/pspdfkit/configuration/signatures/SignatureColorOptions$-CC;->fromDefaults()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureColorOptions:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    .line 17
    sget-object v1, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;->DRAW:Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;->IMAGE:Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;->TYPE:Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 20
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureCreationModes:Ljava/util/List;

    .line 33
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 34
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureColorOptions:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    .line 35
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureCreationModes()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureCreationModes:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;
    .locals 4

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions;

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    iget-object v2, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureColorOptions:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    iget-object v3, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureCreationModes:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions;-><init>(Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;Ljava/util/List;)V

    return-object v0
.end method

.method public signatureColorOptions(Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;)Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;
    .locals 2

    const-string v0, "signatureColorOptions"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureColorOptions:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    return-object p0
.end method

.method public signatureCreationModes(Ljava/util/List;)Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;",
            ">;)",
            "Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;"
        }
    .end annotation

    const-string v0, "signatureCreationModes"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "signatureCreationModes must contain no null items."

    .line 54
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Ljava/util/Collection;)V

    .line 56
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_1

    .line 60
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 61
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 64
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureCreationModes:Ljava/util/List;

    return-object p0

    .line 65
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "`signatureCreationModes` must not have duplicates."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 66
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "`signatureCreationModes` must have 1 to 3 elements. Found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 67
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public signatureSavingStrategy(Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;)Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    return-object p0
.end method

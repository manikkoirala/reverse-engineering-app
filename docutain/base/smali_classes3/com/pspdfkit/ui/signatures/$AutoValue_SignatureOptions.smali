.class abstract Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;
.super Lcom/pspdfkit/ui/signatures/SignatureOptions;
.source "SourceFile"


# instance fields
.field private final defaultSigner:Ljava/lang/String;

.field private final signatureCertificateSelectionMode:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

.field private final signaturePickerOrientation:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

.field private final signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;


# direct methods
.method constructor <init>(Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/SignatureOptions;-><init>()V

    if-eqz p1, :cond_2

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signaturePickerOrientation:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    if-eqz p2, :cond_1

    .line 9
    iput-object p2, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signatureCertificateSelectionMode:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    if-eqz p3, :cond_0

    .line 13
    iput-object p3, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 14
    iput-object p4, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->defaultSigner:Ljava/lang/String;

    return-void

    .line 15
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null signatureSavingStrategy"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 16
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null signatureCertificateSelectionMode"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 17
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null signaturePickerOrientation"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/ui/signatures/SignatureOptions;

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    .line 2
    check-cast p1, Lcom/pspdfkit/ui/signatures/SignatureOptions;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signaturePickerOrientation:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getSignaturePickerOrientation()Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signatureCertificateSelectionMode:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getSignatureCertificateSelectionMode()Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->defaultSigner:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getDefaultSigner()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getDefaultSigner()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    return v2
.end method

.method public getDefaultSigner()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->defaultSigner:Ljava/lang/String;

    return-object v0
.end method

.method public getSignatureCertificateSelectionMode()Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signatureCertificateSelectionMode:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    return-object v0
.end method

.method public getSignaturePickerOrientation()Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signaturePickerOrientation:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    return-object v0
.end method

.method public getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signaturePickerOrientation:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0xf4243

    xor-int/2addr v0, v1

    mul-int v0, v0, v1

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signatureCertificateSelectionMode:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->defaultSigner:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_0
    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SignatureOptions{signaturePickerOrientation="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signaturePickerOrientation:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signatureCertificateSelectionMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signatureCertificateSelectionMode:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signatureSavingStrategy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", defaultSigner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_SignatureOptions;->defaultSigner:Ljava/lang/String;

    const-string v2, "}"

    .line 2
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/rg;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

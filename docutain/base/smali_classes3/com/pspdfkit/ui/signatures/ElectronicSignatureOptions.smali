.class public abstract Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;
    }
.end annotation


# static fields
.field private static final CAVEAT_FONT_RES:I

.field private static final MARCK_SCRIPT_FONT_RES:I

.field private static final MEDDON_FONT_RES:I

.field private static final PACIFICO_FONT_RES:I

.field private static final customFonts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/pspdfkit/ui/fonts/Font;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/R$font;->pspdf__caveat_bold:I

    sput v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->CAVEAT_FONT_RES:I

    .line 4
    sget v0, Lcom/pspdfkit/R$font;->pspdf__pacifico_regular:I

    sput v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->PACIFICO_FONT_RES:I

    .line 7
    sget v0, Lcom/pspdfkit/R$font;->pspdf__marck_script_regular:I

    sput v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->MARCK_SCRIPT_FONT_RES:I

    .line 10
    sget v0, Lcom/pspdfkit/R$font;->pspdf__meddon_regular:I

    sput v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->MEDDON_FONT_RES:I

    .line 16
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    sput-object v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->customFonts:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAvailableFonts(Landroid/content/Context;)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set<",
            "Lcom/pspdfkit/ui/fonts/Font;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->customFonts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    new-instance v0, Ljava/util/LinkedHashSet;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/pspdfkit/ui/fonts/Font;

    new-instance v2, Lcom/pspdfkit/ui/fonts/Font;

    sget v3, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->CAVEAT_FONT_RES:I

    .line 57
    invoke-static {p0, v3}, Landroidx/core/content/res/ResourcesCompat;->getFont(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Typeface;

    const-string v4, "Caveat"

    invoke-direct {v2, v4, v3}, Lcom/pspdfkit/ui/fonts/Font;-><init>(Ljava/lang/String;Landroid/graphics/Typeface;)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lcom/pspdfkit/ui/fonts/Font;

    sget v3, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->PACIFICO_FONT_RES:I

    .line 58
    invoke-static {p0, v3}, Landroidx/core/content/res/ResourcesCompat;->getFont(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Typeface;

    const-string v4, "Pacifico"

    invoke-direct {v2, v4, v3}, Lcom/pspdfkit/ui/fonts/Font;-><init>(Ljava/lang/String;Landroid/graphics/Typeface;)V

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v2, Lcom/pspdfkit/ui/fonts/Font;

    sget v3, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->MARCK_SCRIPT_FONT_RES:I

    .line 61
    invoke-static {p0, v3}, Landroidx/core/content/res/ResourcesCompat;->getFont(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Typeface;

    const-string v4, "Marck Script"

    invoke-direct {v2, v4, v3}, Lcom/pspdfkit/ui/fonts/Font;-><init>(Ljava/lang/String;Landroid/graphics/Typeface;)V

    const/4 v3, 0x2

    aput-object v2, v1, v3

    new-instance v2, Lcom/pspdfkit/ui/fonts/Font;

    sget v3, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->MEDDON_FONT_RES:I

    .line 62
    invoke-static {p0, v3}, Landroidx/core/content/res/ResourcesCompat;->getFont(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/graphics/Typeface;

    const-string v3, "Meddon"

    invoke-direct {v2, v3, p0}, Lcom/pspdfkit/ui/fonts/Font;-><init>(Ljava/lang/String;Landroid/graphics/Typeface;)V

    const/4 p0, 0x3

    aput-object v2, v1, p0

    .line 63
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    :cond_0
    return-object v0
.end method

.method public static setAvailableFonts(Ljava/util/LinkedHashSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashSet<",
            "Lcom/pspdfkit/ui/fonts/Font;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->customFonts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    if-eqz p0, :cond_0

    .line 2
    invoke-virtual {p0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3
    invoke-interface {v0, p0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public abstract getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;
.end method

.method public abstract getSignatureCreationModes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;
.end method

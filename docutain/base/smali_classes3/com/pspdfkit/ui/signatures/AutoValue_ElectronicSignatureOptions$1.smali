.class Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions;
    .locals 4

    .line 2
    new-instance v0, Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions;

    .line 3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    invoke-static {v2, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 4
    const-class v2, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    .line 5
    const-class v3, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions;-><init>(Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions$1;->createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions;
    .locals 0

    .line 2
    new-array p1, p1, [Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions$1;->newArray(I)[Lcom/pspdfkit/ui/signatures/AutoValue_ElectronicSignatureOptions;

    move-result-object p1

    return-object p1
.end method

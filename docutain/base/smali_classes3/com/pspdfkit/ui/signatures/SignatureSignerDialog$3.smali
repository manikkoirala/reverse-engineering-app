.class Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/wr$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->setupSignerUserInteractions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;

.field final synthetic val$interactiveSigner:Lcom/pspdfkit/signatures/signers/InteractiveSigner;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;Lcom/pspdfkit/signatures/signers/InteractiveSigner;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$3;->this$0:Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;

    iput-object p2, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$3;->val$interactiveSigner:Lcom/pspdfkit/signatures/signers/InteractiveSigner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPasswordCanceled()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$3;->this$0:Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;

    invoke-virtual {v0}, Landroidx/fragment/app/DialogFragment;->dismiss()V

    return-void
.end method

.method public onPasswordEntered(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$3;->val$interactiveSigner:Lcom/pspdfkit/signatures/signers/InteractiveSigner;

    invoke-interface {v0, p1}, Lcom/pspdfkit/signatures/signers/InteractiveSigner;->unlockPrivateKeyWithPassword(Ljava/lang/String;)V

    return-void
.end method

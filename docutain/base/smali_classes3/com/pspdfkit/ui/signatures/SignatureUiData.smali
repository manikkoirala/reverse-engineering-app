.class public Lcom/pspdfkit/ui/signatures/SignatureUiData;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final inputMethod:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

.field private final pointSequences:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation
.end field

.field private final pressureList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final timePoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final touchRadii:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;",
            "Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureUiData;->pointSequences:Ljava/util/List;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/ui/signatures/SignatureUiData;->pressureList:Ljava/util/List;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/ui/signatures/SignatureUiData;->timePoints:Ljava/util/List;

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/ui/signatures/SignatureUiData;->touchRadii:Ljava/util/List;

    .line 6
    iput-object p5, p0, Lcom/pspdfkit/ui/signatures/SignatureUiData;->inputMethod:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    return-void
.end method


# virtual methods
.method public getInputMethod()Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureUiData;->inputMethod:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    return-object v0
.end method

.method public getPointSequences()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureUiData;->pointSequences:Ljava/util/List;

    return-object v0
.end method

.method public getPressureList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureUiData;->pressureList:Ljava/util/List;

    return-object v0
.end method

.method public getTimePoints()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureUiData;->timePoints:Ljava/util/List;

    return-object v0
.end method

.method public getTouchRadii()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureUiData;->touchRadii:Ljava/util/List;

    return-object v0
.end method

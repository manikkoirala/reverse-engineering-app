.class public Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Options"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;
    }
.end annotation


# instance fields
.field public final biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

.field public final document:Lcom/pspdfkit/document/PdfDocument;

.field public final formField:Lcom/pspdfkit/forms/SignatureFormField;

.field public final saveDocumentBeforeSigning:Z

.field public final signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

.field public final signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

.field public final signerIdentifier:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/forms/SignatureFormField;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;Lcom/pspdfkit/signatures/SignatureAppearance;Lcom/pspdfkit/signatures/SignatureMetadata;Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "document"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formField"

    .line 3
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signerIdentifier"

    .line 4
    invoke-static {p3, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->document:Lcom/pspdfkit/document/PdfDocument;

    .line 6
    iput-object p2, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->formField:Lcom/pspdfkit/forms/SignatureFormField;

    .line 7
    iput-object p3, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->signerIdentifier:Ljava/lang/String;

    .line 8
    iput-object p4, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    .line 9
    iput-object p5, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

    .line 10
    iput-object p6, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    .line 11
    iput-boolean p7, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;->saveDocumentBeforeSigning:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/forms/SignatureFormField;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;Lcom/pspdfkit/signatures/SignatureAppearance;Lcom/pspdfkit/signatures/SignatureMetadata;ZLcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;-><init>(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/forms/SignatureFormField;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;Lcom/pspdfkit/signatures/SignatureAppearance;Lcom/pspdfkit/signatures/SignatureMetadata;Z)V

    return-void
.end method

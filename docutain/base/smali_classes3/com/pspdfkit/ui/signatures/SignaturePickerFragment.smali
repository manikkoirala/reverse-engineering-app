.class public final Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;
.super Landroidx/fragment/app/Fragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/signatures/SignaturePickerFragment$InternalListener;
    }
.end annotation


# static fields
.field private static final FRAGMENT_TAG:Ljava/lang/String; = "com.pspdfkit.ui.signatures.SignaturePickerFragment.FRAGMENT_TAG"

.field private static final LOG_TAG:Ljava/lang/String; = "PSPDFKit.SignaturePickerFragment"

.field private static final STATE_SIGNATURE_OPTIONS:Ljava/lang/String; = "STATE_SIGNATURE_OPTIONS"

.field private static final STATE_WAITING_FOR_SIGNATURE_PICKER_DIALOG:Ljava/lang/String; = "STATE_WAITING_FOR_SIGNATURE_PICKER_DIALOG"


# instance fields
.field private listener:Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;

.field private final signatureDialogListener:Lcom/pspdfkit/internal/or;

.field private signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

.field private signaturePickerDialog:Lcom/pspdfkit/internal/ui/dialog/signatures/j;

.field private signatureRetrievalDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

.field private signatureStorage:Lcom/pspdfkit/signatures/storage/SignatureStorage;

.field private waitingForSignatureToBePicked:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetlistener(Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;)Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->listener:Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetsignatureOptions(Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;)Lcom/pspdfkit/ui/signatures/SignatureOptions;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetsignatureRetrievalDisposable(Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;)Lio/reactivex/rxjava3/disposables/Disposable;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureRetrievalDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetwaitingForSignatureToBePicked(Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->waitingForSignatureToBePicked:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputlistener(Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->listener:Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputsignaturePickerDialog(Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;Lcom/pspdfkit/internal/ui/dialog/signatures/j;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signaturePickerDialog:Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputsignatureRetrievalDisposable(Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureRetrievalDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputwaitingForSignatureToBePicked(Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->waitingForSignatureToBePicked:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetSignatureStorage(Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;)Lcom/pspdfkit/signatures/storage/SignatureStorage;
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->getSignatureStorage()Lcom/pspdfkit/signatures/storage/SignatureStorage;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 119
    new-instance v0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment$InternalListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment$InternalListener;-><init>(Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;Lcom/pspdfkit/ui/signatures/SignaturePickerFragment$InternalListener-IA;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureDialogListener:Lcom/pspdfkit/internal/or;

    const/4 v0, 0x1

    .line 125
    iput-boolean v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->waitingForSignatureToBePicked:Z

    .line 135
    new-instance v0, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;

    invoke-direct {v0}, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;-><init>()V

    .line 136
    invoke-virtual {v0}, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->build()Lcom/pspdfkit/ui/signatures/SignatureOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    return-void
.end method

.method public static dismiss(Landroidx/fragment/app/FragmentManager;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->findFragment(Landroidx/fragment/app/FragmentManager;)Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->finish()V

    :cond_0
    return-void
.end method

.method private static findFragment(Landroidx/fragment/app/FragmentManager;)Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;
    .locals 1

    const-string v0, "com.pspdfkit.ui.signatures.SignaturePickerFragment.FRAGMENT_TAG"

    .line 1
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;

    return-object p0
.end method

.method private getSignatureStorage()Lcom/pspdfkit/signatures/storage/SignatureStorage;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureStorage:Lcom/pspdfkit/signatures/storage/SignatureStorage;

    if-nez v0, :cond_1

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/hb;->b()Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    move-result-object v0

    .line 4
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;->LEGACYSIGNATURES:Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "pspdfkit_db"

    invoke-static {v0, v1}, Lcom/pspdfkit/signatures/storage/DatabaseSignatureStorage;->withName(Landroid/content/Context;Ljava/lang/String;)Lcom/pspdfkit/signatures/storage/DatabaseSignatureStorage;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureStorage:Lcom/pspdfkit/signatures/storage/SignatureStorage;

    .line 8
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureStorage:Lcom/pspdfkit/signatures/storage/SignatureStorage;

    return-object v0
.end method

.method static synthetic lambda$showSignatureEditorFragment$2(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.SignaturePickerFragment"

    const-string v2, "Failed to retrieve signatures from the signature storage."

    .line 1
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "STATE_WAITING_FOR_SIGNATURE_PICKER_DIALOG"

    const/4 v1, 0x0

    .line 1
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->waitingForSignatureToBePicked:Z

    const-string v0, "STATE_SIGNATURE_OPTIONS"

    .line 2
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/signatures/SignatureOptions;

    if-eqz p1, :cond_0

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    :cond_0
    return-void
.end method

.method public static restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-static {p0, p1, v0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    return-void
.end method

.method public static restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V
    .locals 2

    const-string v0, "fragmentManager"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    if-nez p1, :cond_0

    return-void

    .line 55
    :cond_0
    invoke-static {p0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->findFragment(Landroidx/fragment/app/FragmentManager;)Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 57
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->setOnSignaturePickedListener(Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V

    .line 58
    invoke-direct {p0, p2}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->setSignatureStorage(Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    :cond_1
    return-void
.end method

.method private setSignatureStorage(Lcom/pspdfkit/signatures/storage/SignatureStorage;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureStorage:Lcom/pspdfkit/signatures/storage/SignatureStorage;

    return-void
.end method

.method public static show(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 66
    invoke-static {p0, p1, v0, v0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->show(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/ui/signatures/SignatureOptions;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    return-void
.end method

.method public static show(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/ui/signatures/SignatureOptions;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V
    .locals 2

    const-string v0, "fragmentManager"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    invoke-static {p0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->findFragment(Landroidx/fragment/app/FragmentManager;)Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;

    invoke-direct {v0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;-><init>()V

    .line 57
    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->setOnSignaturePickedListener(Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V

    .line 58
    invoke-direct {v0, p3}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->setSignatureStorage(Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    .line 60
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    const-string p3, "STATE_SIGNATURE_OPTIONS"

    .line 61
    invoke-virtual {p1, p3, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 62
    invoke-virtual {v0, p1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "com.pspdfkit.ui.signatures.SignaturePickerFragment.FRAGMENT_TAG"

    .line 65
    invoke-static {p0, v0, p1}, Lcom/pspdfkit/internal/kc;->a(Landroidx/fragment/app/FragmentManager;Landroidx/fragment/app/Fragment;Ljava/lang/String;)Z

    :cond_1
    return-void
.end method

.method private showSignatureEditorFragment()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureDialogListener:Lcom/pspdfkit/internal/or;

    iget-object v2, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    .line 3
    invoke-virtual {v2}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getSignaturePickerOrientation()Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    .line 4
    invoke-virtual {v3}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object v3

    iget-object v4, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    .line 5
    invoke-virtual {v4}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getSignatureCertificateSelectionMode()Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    move-result-object v4

    iget-object v5, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    .line 6
    invoke-virtual {v5}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getDefaultSigner()Ljava/lang/String;

    move-result-object v5

    .line 7
    invoke-static/range {v0 .. v5}, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->b(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/internal/or;Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Ljava/lang/String;)Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signaturePickerDialog:Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    const/4 v0, 0x1

    .line 14
    iput-boolean v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->waitingForSignatureToBePicked:Z

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureRetrievalDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 16
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureRetrievalDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 25
    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->getSignatureStorage()Lcom/pspdfkit/signatures/storage/SignatureStorage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 26
    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    .line 27
    invoke-virtual {v1}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;->NEVER_SAVE:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 32
    :cond_0
    new-instance v1, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$$ExternalSyntheticLambda1;

    invoke-direct {v1, v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 33
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 34
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;)V

    new-instance v2, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment$$ExternalSyntheticLambda2;

    invoke-direct {v2}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment$$ExternalSyntheticLambda2;-><init>()V

    .line 35
    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureRetrievalDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_1

    .line 36
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signaturePickerDialog:Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->a(Ljava/util/List;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signaturePickerDialog:Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatDialogFragment;->dismiss()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signaturePickerDialog:Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    .line 6
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;)V

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method synthetic lambda$finish$0$com-pspdfkit-ui-signatures-SignaturePickerFragment()V
    .locals 3

    .line 1
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    .line 2
    invoke-static {v0, p0}, Lcom/pspdfkit/internal/kc;->a(Landroidx/fragment/app/FragmentManager;Landroidx/fragment/app/Fragment;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v0, "PSPDFKit.SignaturePickerFragment"

    const-string v2, "Dodged IllegalstateException in finish()"

    .line 8
    invoke-static {v0, v2, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method synthetic lambda$showSignatureEditorFragment$1$com-pspdfkit-ui-signatures-SignaturePickerFragment(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signaturePickerDialog:Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->a(Ljava/util/List;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "STATE_SIGNATURE_OPTIONS"

    .line 5
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/signatures/SignatureOptions;

    if-eqz v0, :cond_0

    .line 7
    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    :cond_0
    if-eqz p1, :cond_1

    .line 13
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 18
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureDialogListener:Lcom/pspdfkit/internal/or;

    iget-object p1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    .line 20
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getSignaturePickerOrientation()Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    move-result-object v3

    iget-object p1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    .line 21
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object v4

    iget-object p1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    .line 22
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getSignatureCertificateSelectionMode()Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    move-result-object v5

    iget-object p1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    .line 23
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/SignatureOptions;->getDefaultSigner()Ljava/lang/String;

    move-result-object v6

    .line 24
    invoke-static/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->a(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/internal/or;Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Ljava/lang/String;)Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signaturePickerDialog:Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    if-nez p1, :cond_2

    .line 33
    iget-boolean p1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->waitingForSignatureToBePicked:Z

    if-eqz p1, :cond_2

    .line 34
    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->showSignatureEditorFragment()V

    :cond_2
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->waitingForSignatureToBePicked:Z

    const-string v1, "STATE_WAITING_FOR_SIGNATURE_PICKER_DIALOG"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->signatureOptions:Lcom/pspdfkit/ui/signatures/SignatureOptions;

    const-string v1, "STATE_SIGNATURE_OPTIONS"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public setOnSignaturePickedListener(Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->listener:Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;

    return-void
.end method

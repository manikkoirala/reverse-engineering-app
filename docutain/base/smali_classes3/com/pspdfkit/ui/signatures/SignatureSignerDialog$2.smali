.class Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->setupSignerUserInteractions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2;->this$0:Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method synthetic lambda$onError$1$com-pspdfkit-ui-signatures-SignatureSignerDialog$2(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2;->this$0:Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;

    invoke-static {v0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->-$$Nest$fgetsigningProgressDialog(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;)Lcom/pspdfkit/internal/wr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/wr;->a(Ljava/lang/String;)V

    return-void
.end method

.method synthetic lambda$onInteractionRequired$0$com-pspdfkit-ui-signatures-SignatureSignerDialog$2(Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$4;->$SwitchMap$com$pspdfkit$signatures$signers$InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 6
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2;->this$0:Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;

    invoke-static {p1}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->-$$Nest$fgetsigningProgressDialog(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;)Lcom/pspdfkit/internal/wr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/wr;->b()V

    goto :goto_0

    .line 7
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2;->this$0:Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;

    invoke-static {p1}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->-$$Nest$fgetsigningProgressDialog(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;)Lcom/pspdfkit/internal/wr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/wr;->a()V

    :goto_0
    return-void
.end method

.method synthetic lambda$onSuccess$2$com-pspdfkit-ui-signatures-SignatureSignerDialog$2()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2;->this$0:Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;

    invoke-static {v0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->-$$Nest$fgetsigningProgressDialog(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;)Lcom/pspdfkit/internal/wr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wr;->c()V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 1
    new-instance p2, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2$$ExternalSyntheticLambda2;

    invoke-direct {p2, p0, p1}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2;Ljava/lang/String;)V

    invoke-static {p2}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 2
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 3
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public onInteractionRequired(Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2;Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 11
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 12
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public onSuccess()V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$2;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 2
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

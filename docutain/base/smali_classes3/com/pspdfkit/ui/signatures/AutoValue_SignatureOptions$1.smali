.class Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions;
    .locals 5

    .line 2
    new-instance v0, Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions;

    .line 3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    invoke-static {v2, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    .line 4
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    invoke-static {v3, v2}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    .line 5
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    invoke-static {v4, v3}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 6
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions;-><init>(Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions$1;->createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions;
    .locals 0

    .line 2
    new-array p1, p1, [Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions$1;->newArray(I)[Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions;

    move-result-object p1

    return-object p1
.end method

.class public final Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/signatures/SignatureOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private defaultSigner:Ljava/lang/String;

.field private signatureCertificateSelectionMode:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

.field private signaturePickerOrientation:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

.field private signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;->AUTOMATIC:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->signaturePickerOrientation:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    .line 3
    sget-object v0, Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;->IF_AVAILABLE:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->signatureCertificateSelectionMode:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    .line 5
    sget-object v0, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;->SAVE_IF_SELECTED:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    const/4 v0, 0x0

    .line 6
    iput-object v0, p0, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->defaultSigner:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public build()Lcom/pspdfkit/ui/signatures/SignatureOptions;
    .locals 5

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions;

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->signaturePickerOrientation:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    iget-object v2, p0, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->signatureCertificateSelectionMode:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    iget-object v3, p0, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    iget-object v4, p0, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->defaultSigner:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/pspdfkit/ui/signatures/AutoValue_SignatureOptions;-><init>(Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Ljava/lang/String;)V

    return-object v0
.end method

.method public defaultSigner(Ljava/lang/String;)Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->defaultSigner:Ljava/lang/String;

    return-object p0
.end method

.method public signatureCertificateSelectionMode(Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;)Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->signatureCertificateSelectionMode:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    return-object p0
.end method

.method public signaturePickerOrientation(Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;)Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->signaturePickerOrientation:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    return-object p0
.end method

.method public signatureSavingStrategy(Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;)Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    return-object p0
.end method

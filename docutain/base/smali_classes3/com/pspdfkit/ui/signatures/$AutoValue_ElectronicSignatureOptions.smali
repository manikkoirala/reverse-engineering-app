.class abstract Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;
.super Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;
.source "SourceFile"


# instance fields
.field private final signatureColorOptions:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

.field private final signatureCreationModes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;",
            ">;"
        }
    .end annotation
.end field

.field private final signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;


# direct methods
.method constructor <init>(Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;",
            "Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;-><init>()V

    if-eqz p1, :cond_2

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    if-eqz p2, :cond_1

    .line 9
    iput-object p2, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureColorOptions:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    if-eqz p3, :cond_0

    .line 13
    iput-object p3, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureCreationModes:Ljava/util/List;

    return-void

    .line 14
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null signatureCreationModes"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 15
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null signatureColorOptions"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 16
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null signatureSavingStrategy"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 2
    check-cast p1, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureColorOptions:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureCreationModes:Ljava/util/List;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureCreationModes()Ljava/util/List;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    return v2
.end method

.method public getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureColorOptions:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    return-object v0
.end method

.method public getSignatureCreationModes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureCreationModes:Ljava/util/List;

    return-object v0
.end method

.method public getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0xf4243

    xor-int/2addr v0, v1

    mul-int v0, v0, v1

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureColorOptions:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureCreationModes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ElectronicSignatureOptions{signatureSavingStrategy="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureSavingStrategy:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signatureColorOptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureColorOptions:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signatureCreationModes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/ui/signatures/$AutoValue_ElectronicSignatureOptions;->signatureCreationModes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

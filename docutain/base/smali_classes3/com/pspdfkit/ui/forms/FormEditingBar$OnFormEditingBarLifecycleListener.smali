.class public interface abstract Lcom/pspdfkit/ui/forms/FormEditingBar$OnFormEditingBarLifecycleListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/forms/FormEditingBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnFormEditingBarLifecycleListener"
.end annotation


# virtual methods
.method public abstract onDisplayFormEditingBar(Lcom/pspdfkit/ui/forms/FormEditingBar;)V
.end method

.method public abstract onPrepareFormEditingBar(Lcom/pspdfkit/ui/forms/FormEditingBar;)V
.end method

.method public abstract onRemoveFormEditingBar(Lcom/pspdfkit/ui/forms/FormEditingBar;)V
.end method

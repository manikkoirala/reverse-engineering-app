.class Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/bg;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/PdfActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InternalPdfUiImpl"
.end annotation


# instance fields
.field private final activity:Lcom/pspdfkit/ui/PdfActivity;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/PdfActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;->activity:Lcom/pspdfkit/ui/PdfActivity;

    return-void
.end method


# virtual methods
.method public getFragmentManager()Landroidx/fragment/app/FragmentManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;->activity:Lcom/pspdfkit/ui/PdfActivity;

    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    return-object v0
.end method

.method public getPdfParameters()Landroid/os/Bundle;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;->activity:Lcom/pspdfkit/ui/PdfActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PSPDF.InternalExtras"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public performApplyConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;->activity:Lcom/pspdfkit/ui/PdfActivity;

    invoke-static {p1}, Lcom/pspdfkit/ui/PdfActivity;->-$$Nest$mapplyConfiguration(Lcom/pspdfkit/ui/PdfActivity;)V

    return-void
.end method

.method public setPdfView(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;->activity:Lcom/pspdfkit/ui/PdfActivity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.class Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentVisibleListener;
.implements Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/tabs/PdfTabBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PdfDocumentManagerListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/ui/tabs/PdfTabBar;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/ui/tabs/PdfTabBar;Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;-><init>(Lcom/pspdfkit/ui/tabs/PdfTabBar;)V

    return-void
.end method


# virtual methods
.method public onDocumentAdded(Lcom/pspdfkit/ui/DocumentDescriptor;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->getTabBarItem(Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    iget-object v1, v0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-static {v0, p1}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->-$$Nest$mcreateTabBarItem(Lcom/pspdfkit/ui/tabs/PdfTabBar;Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/cn;->a(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V

    :cond_0
    return-void
.end method

.method public onDocumentMoved(Lcom/pspdfkit/ui/DocumentDescriptor;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->getTabBarItem(Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    iget-object v0, v0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/cn;->a(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;I)V

    :cond_0
    return-void
.end method

.method public onDocumentRemoved(Lcom/pspdfkit/ui/DocumentDescriptor;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->getTabBarItem(Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    iget-object v0, v0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/cn;->c(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V

    :cond_0
    return-void
.end method

.method public onDocumentReplaced(Lcom/pspdfkit/ui/DocumentDescriptor;Lcom/pspdfkit/ui/DocumentDescriptor;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->getTabBarItem(Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    iget-object v0, v0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/cn;->b(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)I

    move-result p1

    if-ltz p1, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    iget-object v1, v0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-static {v0, p2}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->-$$Nest$mcreateTabBarItem(Lcom/pspdfkit/ui/tabs/PdfTabBar;Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object p2

    invoke-virtual {v1, p2, p1}, Lcom/pspdfkit/internal/cn;->b(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;I)V

    :cond_0
    return-void
.end method

.method public onDocumentUpdated(Lcom/pspdfkit/ui/DocumentDescriptor;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->getTabBarItem(Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    iget-object p1, p1, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/cn;->b()V

    :cond_0
    return-void
.end method

.method public onDocumentVisible(Lcom/pspdfkit/ui/DocumentDescriptor;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->getTabBarItem(Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    invoke-static {v0, p1}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->-$$Nest$mcreateTabBarItem(Lcom/pspdfkit/ui/tabs/PdfTabBar;Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object v0

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;->this$0:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    iget-object p1, p1, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/cn;->setSelectedTab(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V

    return-void
.end method

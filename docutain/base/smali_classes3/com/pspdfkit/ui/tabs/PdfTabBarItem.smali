.class public Lcom/pspdfkit/ui/tabs/PdfTabBarItem;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final documentDescriptor:Lcom/pspdfkit/ui/DocumentDescriptor;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/ui/DocumentDescriptor;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "documentDescriptor"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBarItem;->documentDescriptor:Lcom/pspdfkit/ui/DocumentDescriptor;

    return-void
.end method


# virtual methods
.method public getDocumentDescriptor()Lcom/pspdfkit/ui/DocumentDescriptor;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBarItem;->documentDescriptor:Lcom/pspdfkit/ui/DocumentDescriptor;

    return-object v0
.end method

.class public Lcom/pspdfkit/ui/tabs/PdfTabBar;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;,
        Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate;,
        Lcom/pspdfkit/ui/tabs/PdfTabBar$OnTabsChangedListener;,
        Lcom/pspdfkit/ui/tabs/PdfTabBar$OnTabClickedListener;
    }
.end annotation


# instance fields
.field private documentCoordinator:Lcom/pspdfkit/ui/DocumentCoordinator;

.field private final documentManagerListener:Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;

.field tabBarLayout:Lcom/pspdfkit/internal/cn;

.field private final tabBarLayoutDelegate:Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate;

.field private final tabClickedListeners:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/tabs/PdfTabBar$OnTabClickedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final tabsChangedListeners:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/tabs/PdfTabBar$OnTabsChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private themeConfiguration:Lcom/pspdfkit/internal/dn;


# direct methods
.method static bridge synthetic -$$Nest$fgettabClickedListeners(Lcom/pspdfkit/ui/tabs/PdfTabBar;)Lcom/pspdfkit/internal/nh;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabClickedListeners:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgettabsChangedListeners(Lcom/pspdfkit/ui/tabs/PdfTabBar;)Lcom/pspdfkit/internal/nh;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabsChangedListeners:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mcreateTabBarItem(Lcom/pspdfkit/ui/tabs/PdfTabBar;Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->createTabBarItem(Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetDocumentCoordinator(Lcom/pspdfkit/ui/tabs/PdfTabBar;)Lcom/pspdfkit/ui/DocumentCoordinator;
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabsChangedListeners:Lcom/pspdfkit/internal/nh;

    .line 5
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabClickedListeners:Lcom/pspdfkit/internal/nh;

    .line 8
    new-instance p1, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;-><init>(Lcom/pspdfkit/ui/tabs/PdfTabBar;Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener-IA;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->documentManagerListener:Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;

    .line 11
    new-instance p1, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate;

    invoke-direct {p1, p0, v0}, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate;-><init>(Lcom/pspdfkit/ui/tabs/PdfTabBar;Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate-IA;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayoutDelegate:Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate;

    .line 19
    invoke-direct {p0}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->initialize()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabsChangedListeners:Lcom/pspdfkit/internal/nh;

    .line 24
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabClickedListeners:Lcom/pspdfkit/internal/nh;

    .line 27
    new-instance p1, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;-><init>(Lcom/pspdfkit/ui/tabs/PdfTabBar;Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener-IA;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->documentManagerListener:Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;

    .line 30
    new-instance p1, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate;

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate;-><init>(Lcom/pspdfkit/ui/tabs/PdfTabBar;Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate-IA;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayoutDelegate:Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate;

    .line 43
    invoke-direct {p0}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->initialize()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabsChangedListeners:Lcom/pspdfkit/internal/nh;

    .line 48
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabClickedListeners:Lcom/pspdfkit/internal/nh;

    .line 51
    new-instance p1, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;-><init>(Lcom/pspdfkit/ui/tabs/PdfTabBar;Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener-IA;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->documentManagerListener:Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;

    .line 54
    new-instance p1, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate;

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate;-><init>(Lcom/pspdfkit/ui/tabs/PdfTabBar;Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate-IA;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayoutDelegate:Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate;

    .line 72
    invoke-direct {p0}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->initialize()V

    return-void
.end method

.method private createTabBarItem(Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    invoke-direct {v0, p1}, Lcom/pspdfkit/ui/tabs/PdfTabBarItem;-><init>(Lcom/pspdfkit/ui/DocumentDescriptor;)V

    return-object v0
.end method

.method private getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->documentCoordinator:Lcom/pspdfkit/ui/DocumentCoordinator;

    const-string v1, "DocumentCoordinator must be bound to PdfTabBar before using tabs."

    const-string v2, "message"

    .line 2
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->documentCoordinator:Lcom/pspdfkit/ui/DocumentCoordinator;

    return-object v0

    .line 4
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private initialize()V
    .locals 4

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/dn;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/dn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->themeConfiguration:Lcom/pspdfkit/internal/dn;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dn;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/cn;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->themeConfiguration:Lcom/pspdfkit/internal/dn;

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/cn;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/dn;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    .line 6
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->themeConfiguration:Lcom/pspdfkit/internal/dn;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dn;->b()I

    move-result v2

    const/4 v3, -0x2

    invoke-direct {v1, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 9
    new-instance v0, Lcom/pspdfkit/ui/tabs/PdfTabBar$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/tabs/PdfTabBar$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/tabs/PdfTabBar;)V

    invoke-static {p0, v0}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    return-void
.end method


# virtual methods
.method public addOnTabClickedListener(Lcom/pspdfkit/ui/tabs/PdfTabBar$OnTabClickedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabClickedListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public addOnTabsChangedListener(Lcom/pspdfkit/ui/tabs/PdfTabBar$OnTabsChangedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabsChangedListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public bindToDocumentCoordinator(Lcom/pspdfkit/ui/DocumentCoordinator;)V
    .locals 3

    const-string v0, "documentCoordinator"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->documentCoordinator:Lcom/pspdfkit/ui/DocumentCoordinator;

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->documentManagerListener:Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;

    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/DocumentCoordinator;->addOnDocumentVisibleListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentVisibleListener;)V

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->documentManagerListener:Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;

    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/DocumentCoordinator;->addOnDocumentsChangedListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;)V

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    iget-object v1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayoutDelegate:Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfTabBarLayoutDelegate;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/cn;->setDelegate(Lcom/pspdfkit/internal/cn$c;)V

    .line 62
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cn;->c()V

    .line 63
    invoke-interface {p1}, Lcom/pspdfkit/ui/DocumentCoordinator;->getDocuments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/DocumentDescriptor;

    .line 64
    iget-object v2, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->createTabBarItem(Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/cn;->a(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V

    goto :goto_0

    .line 68
    :cond_0
    invoke-interface {p1}, Lcom/pspdfkit/ui/DocumentCoordinator;->getVisibleDocument()Lcom/pspdfkit/ui/DocumentDescriptor;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 70
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->getTabBarItem(Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 72
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/cn;->setSelectedTab(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V

    :cond_1
    return-void
.end method

.method public getSize()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cn;->getTabs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getTabBarItem(Lcom/pspdfkit/ui/DocumentDescriptor;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 1
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/cn;->getTabs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    .line 2
    invoke-virtual {v2}, Lcom/pspdfkit/ui/tabs/PdfTabBarItem;->getDocumentDescriptor()Lcom/pspdfkit/ui/DocumentDescriptor;

    move-result-object v3

    if-ne v3, p1, :cond_1

    return-object v2

    :cond_2
    return-object v0
.end method

.method public getTabs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/tabs/PdfTabBarItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cn;->getTabs()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method synthetic lambda$initialize$0$com-pspdfkit-ui-tabs-PdfTabBar(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 2

    .line 1
    invoke-virtual {p2}, Landroidx/core/view/WindowInsetsCompat;->getSystemWindowInsetLeft()I

    move-result p1

    invoke-virtual {p2}, Landroidx/core/view/WindowInsetsCompat;->getSystemWindowInsetRight()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 2
    invoke-virtual {p2}, Landroidx/core/view/WindowInsetsCompat;->consumeStableInsets()Landroidx/core/view/WindowInsetsCompat;

    move-result-object p1

    return-object p1
.end method

.method public removeOnTabClickedListener(Lcom/pspdfkit/ui/tabs/PdfTabBar$OnTabClickedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabClickedListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public removeOnTabsChangedListener(Lcom/pspdfkit/ui/tabs/PdfTabBar$OnTabsChangedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabsChangedListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public setCloseMode(Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;)V
    .locals 2

    const-string v0, "closeMode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/cn;->setCloseMode(Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;)V

    return-void
.end method

.method public unbindDocumentCoordinator()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->documentCoordinator:Lcom/pspdfkit/ui/DocumentCoordinator;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->documentManagerListener:Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;

    invoke-interface {v0, v2}, Lcom/pspdfkit/ui/DocumentCoordinator;->removeOnDocumentsChangedListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->documentCoordinator:Lcom/pspdfkit/ui/DocumentCoordinator;

    iget-object v2, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->documentManagerListener:Lcom/pspdfkit/ui/tabs/PdfTabBar$PdfDocumentManagerListener;

    invoke-interface {v0, v2}, Lcom/pspdfkit/ui/DocumentCoordinator;->removeOnDocumentVisibleListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentVisibleListener;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cn;->c()V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->tabBarLayout:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/cn;->setDelegate(Lcom/pspdfkit/internal/cn$c;)V

    .line 9
    :cond_0
    iput-object v1, p0, Lcom/pspdfkit/ui/tabs/PdfTabBar;->documentCoordinator:Lcom/pspdfkit/ui/DocumentCoordinator;

    return-void
.end method

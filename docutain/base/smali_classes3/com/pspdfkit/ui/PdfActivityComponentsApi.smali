.class interface abstract Lcom/pspdfkit/ui/PdfActivityComponentsApi;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract addPropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
.end method

.method public abstract getPropertyInspectorCoordinator()Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;
.end method

.method public abstract removePropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
.end method

.method public abstract setAnnotationCreationInspectorController(Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;)V
.end method

.method public abstract setAnnotationEditingInspectorController(Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;)V
.end method

.method public abstract setDocumentPrintDialogFactory(Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;)V
.end method

.method public abstract setDocumentSharingDialogFactory(Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;)V
.end method

.method public abstract setOnContextualToolbarLifecycleListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarLifecycleListener;)V
.end method

.method public abstract setOnContextualToolbarMovementListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarMovementListener;)V
.end method

.method public abstract setOnContextualToolbarPositionListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;)V
.end method

.method public abstract setPrintOptionsProvider(Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V
.end method

.method public abstract setSharingActionMenuListener(Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;)V
.end method

.method public abstract setSharingOptionsProvider(Lcom/pspdfkit/document/sharing/SharingOptionsProvider;)V
.end method

.class public interface abstract Lcom/pspdfkit/ui/fonts/FontManager;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getAvailableFonts()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/fonts/Font;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFontByName(Ljava/lang/String;)Lcom/pspdfkit/ui/fonts/Font;
.end method

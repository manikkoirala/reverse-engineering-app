.class public Lcom/pspdfkit/ui/PdfOutlineView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ql$a;
.implements Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;
.implements Lcom/pspdfkit/ui/drawable/PdfDrawableManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;,
        Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;,
        Lcom/pspdfkit/ui/PdfOutlineView$OnOutlineElementTapListener;,
        Lcom/pspdfkit/ui/PdfOutlineView$OnAnnotationTapListener;
    }
.end annotation


# static fields
.field private static final OUTLINE_VIEW_WIDTH_DP:I = 0x1e0

.field private static final bottomShadow:Landroid/graphics/drawable/GradientDrawable;

.field private static final leftShadow:Landroid/graphics/drawable/GradientDrawable;


# instance fields
.field private displayAnnotationListView:Z

.field private displayBookmarkListView:Z

.field private displayInfoListView:Z

.field private displayOutlineView:Z

.field private final documentListener:Lcom/pspdfkit/listeners/DocumentListener;

.field private isDisplayed:Z

.field private final listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

.field private mayContainDocumentInfoView:Z

.field private onAnnotationTapListener:Lcom/pspdfkit/ui/PdfOutlineView$OnAnnotationTapListener;

.field private onEditRecordedListener:Lcom/pspdfkit/internal/fl;

.field private onOutlineElementTapListener:Lcom/pspdfkit/ui/PdfOutlineView$OnOutlineElementTapListener;

.field private pager:Landroidx/viewpager/widget/ViewPager;

.field private final pagerAdapter:Lcom/pspdfkit/internal/zg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/zg<",
            "Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private pagerTabs:Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;

.field private shadowHeightPx:I

.field private themeConfiguration:Lcom/pspdfkit/internal/rl;


# direct methods
.method static bridge synthetic -$$Nest$fgetisDisplayed(Lcom/pspdfkit/ui/PdfOutlineView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->isDisplayed:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetonAnnotationTapListener(Lcom/pspdfkit/ui/PdfOutlineView;)Lcom/pspdfkit/ui/PdfOutlineView$OnAnnotationTapListener;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->onAnnotationTapListener:Lcom/pspdfkit/ui/PdfOutlineView$OnAnnotationTapListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetonOutlineElementTapListener(Lcom/pspdfkit/ui/PdfOutlineView;)Lcom/pspdfkit/ui/PdfOutlineView$OnOutlineElementTapListener;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->onOutlineElementTapListener:Lcom/pspdfkit/ui/PdfOutlineView$OnOutlineElementTapListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetpager(Lcom/pspdfkit/ui/PdfOutlineView;)Landroidx/viewpager/widget/ViewPager;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pager:Landroidx/viewpager/widget/ViewPager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetpagerAdapter(Lcom/pspdfkit/ui/PdfOutlineView;)Lcom/pspdfkit/internal/zg;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetpagerTabs(Lcom/pspdfkit/ui/PdfOutlineView;)Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerTabs:Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mcreateDocumentInfoListView(Lcom/pspdfkit/ui/PdfOutlineView;Landroid/content/Context;)Lcom/pspdfkit/internal/y8;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfOutlineView;->createDocumentInfoListView(Landroid/content/Context;)Lcom/pspdfkit/internal/y8;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Landroid/graphics/drawable/GradientDrawable$Orientation;->RIGHT_LEFT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/graphics/drawable/GradientDrawable$Orientation;)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/ui/PdfOutlineView;->leftShadow:Landroid/graphics/drawable/GradientDrawable;

    .line 6
    sget-object v0, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 7
    invoke-static {v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/graphics/drawable/GradientDrawable$Orientation;)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/ui/PdfOutlineView;->bottomShadow:Landroid/graphics/drawable/GradientDrawable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-direct {p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    const/4 p1, 0x1

    .line 5
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->mayContainDocumentInfoView:Z

    .line 22
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayOutlineView:Z

    .line 30
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayAnnotationListView:Z

    .line 37
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayBookmarkListView:Z

    const/4 p1, 0x0

    .line 44
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayInfoListView:Z

    .line 68
    new-instance p1, Lcom/pspdfkit/internal/zg;

    invoke-direct {p1}, Lcom/pspdfkit/internal/zg;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    .line 598
    new-instance p1, Lcom/pspdfkit/ui/PdfOutlineView$3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/ui/PdfOutlineView$3;-><init>(Lcom/pspdfkit/ui/PdfOutlineView;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->documentListener:Lcom/pspdfkit/listeners/DocumentListener;

    .line 599
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 600
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 601
    new-instance p1, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-direct {p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    const/4 p1, 0x1

    .line 604
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->mayContainDocumentInfoView:Z

    .line 621
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayOutlineView:Z

    .line 629
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayAnnotationListView:Z

    .line 636
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayBookmarkListView:Z

    const/4 p1, 0x0

    .line 643
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayInfoListView:Z

    .line 667
    new-instance p1, Lcom/pspdfkit/internal/zg;

    invoke-direct {p1}, Lcom/pspdfkit/internal/zg;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    .line 1197
    new-instance p1, Lcom/pspdfkit/ui/PdfOutlineView$3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/ui/PdfOutlineView$3;-><init>(Lcom/pspdfkit/ui/PdfOutlineView;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->documentListener:Lcom/pspdfkit/listeners/DocumentListener;

    .line 1198
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 1199
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200
    new-instance p1, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-direct {p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    const/4 p1, 0x1

    .line 1203
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->mayContainDocumentInfoView:Z

    .line 1220
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayOutlineView:Z

    .line 1228
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayAnnotationListView:Z

    .line 1235
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayBookmarkListView:Z

    const/4 p1, 0x0

    .line 1242
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayInfoListView:Z

    .line 1266
    new-instance p1, Lcom/pspdfkit/internal/zg;

    invoke-direct {p1}, Lcom/pspdfkit/internal/zg;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    .line 1796
    new-instance p1, Lcom/pspdfkit/ui/PdfOutlineView$3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/ui/PdfOutlineView$3;-><init>(Lcom/pspdfkit/ui/PdfOutlineView;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->documentListener:Lcom/pspdfkit/listeners/DocumentListener;

    .line 1797
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .line 1798
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 1799
    new-instance p1, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-direct {p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    const/4 p1, 0x1

    .line 1802
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->mayContainDocumentInfoView:Z

    .line 1819
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayOutlineView:Z

    .line 1827
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayAnnotationListView:Z

    .line 1834
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayBookmarkListView:Z

    const/4 p1, 0x0

    .line 1841
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayInfoListView:Z

    .line 1865
    new-instance p1, Lcom/pspdfkit/internal/zg;

    invoke-direct {p1}, Lcom/pspdfkit/internal/zg;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    .line 2395
    new-instance p1, Lcom/pspdfkit/ui/PdfOutlineView$3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/ui/PdfOutlineView$3;-><init>(Lcom/pspdfkit/ui/PdfOutlineView;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->documentListener:Lcom/pspdfkit/listeners/DocumentListener;

    .line 2396
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->init()V

    return-void
.end method

.method private createDocumentInfoListView(Landroid/content/Context;)Lcom/pspdfkit/internal/y8;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->mayContainDocumentInfoView:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/pspdfkit/internal/y8;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/y8;-><init>(Landroid/content/Context;)V

    return-object v0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private init()V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/rl;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/rl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->themeConfiguration:Lcom/pspdfkit/internal/rl;

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/PdfOutlineView$1;-><init>(Lcom/pspdfkit/ui/PdfOutlineView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method static synthetic lambda$addDrawableProvider$14(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetbookmarkListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/t4;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/t4;->addDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    return-void
.end method

.method static synthetic lambda$addOnDocumentInfoViewModeChangeListener$10(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetdocumentInfoListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/y8;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/y8;->a(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$addOnDocumentInfoViewSaveListener$12(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetdocumentInfoListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/y8;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/y8;->a(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$removeDrawableProvider$15(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetbookmarkListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/t4;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/t4;->removeDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    return-void
.end method

.method static synthetic lambda$removeOnDocumentInfoViewModeChangeListener$11(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetdocumentInfoListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/y8;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/y8;->b(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$removeOnDocumentInfoViewSaveListener$13(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetdocumentInfoListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/y8;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/y8;->b(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$setAnnotationEditingEnabled$3(ZLcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetannotationListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/h1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/h1;->setAnnotationEditingEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$setAnnotationListReorderingEnabled$9(ZLcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetannotationListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/h1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/h1;->setAnnotationListReorderingEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$setBookmarkAdapter$6(Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetbookmarkListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/t4;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/t4;->setBookmarkViewAdapter(Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;)V

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->refreshItemsVisibility()V

    return-void
.end method

.method static synthetic lambda$setBookmarkEditingEnabled$4(ZLcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetbookmarkListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/t4;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/t4;->setBookmarkEditingEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$setBookmarkRenamingEnabled$5(ZLcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetbookmarkListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/t4;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/t4;->setBookmarkRenamingEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$setListedAnnotationTypes$8(Ljava/util/EnumSet;Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetannotationListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/h1;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/h1;->setListedAnnotationTypes(Ljava/util/EnumSet;)V

    return-void
.end method

.method static synthetic lambda$setRedactionAnnotationPreviewEnabled$2(ZLcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetbookmarkListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/t4;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/t4;->setRedactionAnnotationPreviewEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$setShowPageLabels$7(ZLcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetoutlineListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/pl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/pl;->setShowPageLabels(Z)V

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetbookmarkListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/t4;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/t4;->setShowPageLabels(Z)V

    return-void
.end method


# virtual methods
.method public addDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda11;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda11;-><init>(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public addOnDocumentInfoViewModeChangeListener(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public addOnDocumentInfoViewSaveListener(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda10;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    return-void
.end method

.method public clearDocument()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->hide()V

    return-void
.end method

.method ensureInitialized()Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    if-eqz v0, :cond_0

    return-object v0

    .line 4
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__outline_view:I

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    .line 5
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfOutlineView;->themeConfiguration:Lcom/pspdfkit/internal/rl;

    iget v2, v2, Lcom/pspdfkit/internal/rl;->a:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 7
    sget v1, Lcom/pspdfkit/R$id;->pspdf__outline_pager:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/viewpager/widget/ViewPager;

    iput-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pager:Landroidx/viewpager/widget/ViewPager;

    .line 8
    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfOutlineView;->onEditRecordedListener:Lcom/pspdfkit/internal/fl;

    invoke-direct {v1, p0, v2}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;-><init>(Lcom/pspdfkit/ui/PdfOutlineView;Lcom/pspdfkit/internal/fl;)V

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/ui/PdfOutlineView;->themeConfiguration:Lcom/pspdfkit/internal/rl;

    invoke-static {v1, v2}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$mapplyTheme(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;Lcom/pspdfkit/internal/rl;)V

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v2, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 12
    sget v2, Lcom/pspdfkit/R$id;->pspdf__view_pager_tab_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerTabs:Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;

    .line 13
    iget-object v2, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->a(Landroidx/viewpager/widget/ViewPager;)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerTabs:Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;

    iget-object v2, p0, Lcom/pspdfkit/ui/PdfOutlineView;->themeConfiguration:Lcom/pspdfkit/internal/rl;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->a(Lcom/pspdfkit/internal/rl;)V

    .line 17
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x43f00000    # 480.0f

    mul-float v0, v0, v2

    .line 19
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 20
    iget v3, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 21
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    const v5, 0x3f99999a    # 1.2f

    mul-float v5, v5, v0

    cmpl-float v3, v3, v5

    if-lez v3, :cond_1

    .line 24
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    float-to-int v0, v0

    invoke-direct {v2, v0, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    :cond_1
    const v0, 0x800005

    .line 26
    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 27
    invoke-virtual {p0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Ljava/lang/Object;)V

    .line 31
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->refreshViewPager()V

    return-object v1
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 3

    .line 1
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0, v1, v2, p1}, Landroid/view/View;->setPadding(IIII)V

    const/4 p1, 0x0

    return p1
.end method

.method public getDocumentListener()Lcom/pspdfkit/listeners/DocumentListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->documentListener:Lcom/pspdfkit/listeners/DocumentListener;

    return-object v0
.end method

.method public getMayContainDocumentInfoView()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->mayContainDocumentInfoView:Z

    return v0
.end method

.method public getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_OUTLINE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    return-object v0
.end method

.method public hide()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->isDisplayed:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->isDisplayed:Z

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->onHide(Landroid/view/View;)V

    .line 4
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    .line 6
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/PdfOutlineView$2;-><init>(Lcom/pspdfkit/ui/PdfOutlineView;)V

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->onHide()V

    :cond_1
    :goto_0
    return-void
.end method

.method public isDisplayed()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->isDisplayed:Z

    return v0
.end method

.method synthetic lambda$setDocument$0$com-pspdfkit-ui-PdfOutlineView(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-virtual {p3, p1, p2, p0}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/ql$a;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->refreshViewPager()V

    return-void
.end method

.method synthetic lambda$setDocumentOutlineProvider$1$com-pspdfkit-ui-PdfOutlineView(Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V
    .locals 0

    .line 1
    invoke-static {p2}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->-$$Nest$fgetoutlineListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/pl;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/pl;->setDocumentOutlineProvider(Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->refreshViewPager()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 5
    invoke-static {p0}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/zj;->a(Landroid/app/Activity;)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->shadowHeightPx:I

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->a()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    iget v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->shadowHeightPx:I

    add-int/2addr p2, v0

    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    cmpg-float p1, p1, v0

    if-gez p1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->hide()V

    :cond_0
    return v1
.end method

.method public refreshViewPager()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda3;

    invoke-direct {v1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda3;-><init>()V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public removeDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda1;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public removeOnDocumentInfoViewModeChangeListener(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda12;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda12;-><init>(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public removeOnDocumentInfoViewSaveListener(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda2;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    return-void
.end method

.method public setAnnotationEditingEnabled(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda9;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda9;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public setAnnotationListReorderingEnabled(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda5;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda5;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public setAnnotationListViewEnabled(Z)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/ui/PdfOutlineView;->setAnnotationListViewEnabled(ZZ)V

    return-void
.end method

.method public setAnnotationListViewEnabled(ZZ)V
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayAnnotationListView:Z

    if-eqz p2, :cond_0

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->refreshViewPager()V

    :cond_0
    return-void
.end method

.method public setBookmarkAdapter(Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda6;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public setBookmarkEditingEnabled(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda4;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda4;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public setBookmarkRenamingEnabled(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda13;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda13;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public setBookmarkViewEnabled(Z)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/ui/PdfOutlineView;->setBookmarkViewEnabled(ZZ)V

    return-void
.end method

.method public setBookmarkViewEnabled(ZZ)V
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayBookmarkListView:Z

    if-eqz p2, :cond_0

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->refreshViewPager()V

    :cond_0
    return-void
.end method

.method public setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 3

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda8;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/ui/PdfOutlineView;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public setDocumentInfoViewEnabled(Z)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/ui/PdfOutlineView;->setDocumentInfoViewEnabled(ZZ)V

    return-void
.end method

.method public setDocumentInfoViewEnabled(ZZ)V
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->getMayContainDocumentInfoView()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayInfoListView:Z

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 3
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayInfoListView:Z

    :goto_0
    if-eqz p2, :cond_1

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->refreshViewPager()V

    :cond_1
    return-void
.end method

.method public setDocumentOutlineProvider(Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/ui/PdfOutlineView;Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public setListedAnnotationTypes(Ljava/util/EnumSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda14;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda14;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public setMayContainDocumentInfoView(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->mayContainDocumentInfoView:Z

    return-void
.end method

.method public setOnAnnotationTapListener(Lcom/pspdfkit/ui/PdfOutlineView$OnAnnotationTapListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->onAnnotationTapListener:Lcom/pspdfkit/ui/PdfOutlineView$OnAnnotationTapListener;

    return-void
.end method

.method public setOnOutlineElementTapListener(Lcom/pspdfkit/ui/PdfOutlineView$OnOutlineElementTapListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->onOutlineElementTapListener:Lcom/pspdfkit/ui/PdfOutlineView$OnOutlineElementTapListener;

    return-void
.end method

.method setOutlinePagerTabView(Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerTabs:Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;

    return-void
.end method

.method public setOutlineViewEnabled(Z)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/ui/PdfOutlineView;->setOutlineViewEnabled(ZZ)V

    return-void
.end method

.method public setOutlineViewEnabled(ZZ)V
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayOutlineView:Z

    if-eqz p2, :cond_0

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->refreshViewPager()V

    :cond_0
    return-void
.end method

.method public setRedactionAnnotationPreviewEnabled(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda16;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda16;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public setShowPageLabels(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda15;

    invoke-direct {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$$ExternalSyntheticLambda15;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public setUndoManager(Lcom/pspdfkit/undo/UndoManager;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/internal/fl;

    if-eqz v0, :cond_0

    .line 2
    check-cast p1, Lcom/pspdfkit/internal/fl;

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->onEditRecordedListener:Lcom/pspdfkit/internal/fl;

    :cond_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 0

    if-nez p1, :cond_0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->ensureInitialized()Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    .line 3
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    return-void
.end method

.method protected shouldDisplayAnnotationListView()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayAnnotationListView:Z

    return v0
.end method

.method protected shouldDisplayBookmarkListView()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayBookmarkListView:Z

    return v0
.end method

.method protected shouldDisplayDocumentInfoListView()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    .line 2
    iget-boolean v1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayInfoListView:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->isDocumentInfoListViewAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected shouldDisplayOutlineView()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerAdapter:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    .line 2
    iget-boolean v1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->displayOutlineView:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->isOutlineListViewAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public show()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->isDisplayed:Z

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView;->ensureInitialized()Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    move-result-object v0

    const/4 v1, 0x1

    .line 3
    iput-boolean v1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->isDisplayed:Z

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v1, p0}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->onShow(Landroid/view/View;)V

    const/4 v1, 0x0

    .line 6
    invoke-virtual {p0, v1}, Lcom/pspdfkit/ui/PdfOutlineView;->setVisibility(I)V

    .line 7
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->onShow()V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView;->pagerTabs:Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->a()V

    .line 16
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "open_outline_view"

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.class public Lcom/pspdfkit/ui/search/SearchResultHighlighter;
.super Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;
.source "SourceFile"


# instance fields
.field private final noteAnnotationSizePx:I

.field private final searchResultDrawableCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            "Lcom/pspdfkit/ui/search/SearchResultDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private final searchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;"
        }
    .end annotation
.end field

.field private selectedSearchResult:Lcom/pspdfkit/document/search/SearchResult;

.field private final themeConfiguration:Lcom/pspdfkit/internal/rq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResultDrawableCache:Ljava/util/HashMap;

    const-string v1, "context"

    .line 19
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResults:Ljava/util/List;

    const/4 v1, 0x0

    .line 21
    iput-object v1, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->selectedSearchResult:Lcom/pspdfkit/document/search/SearchResult;

    .line 22
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 23
    new-instance v0, Lcom/pspdfkit/internal/rq;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/rq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/pspdfkit/R$dimen;->pspdf__view_annotation_size:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->noteAnnotationSizePx:I

    return-void
.end method


# virtual methods
.method public declared-synchronized addSearchResults(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    .line 1
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    .line 3
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResults:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->notifyDrawablesChanged()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized clearSearchResults()V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    .line 3
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->selectedSearchResult:Lcom/pspdfkit/document/search/SearchResult;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResultDrawableCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->notifyDrawablesChanged()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDrawablesForPage(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/document/PdfDocument;",
            "I)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawable;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 1
    :try_start_0
    new-instance p1, Ljava/util/ArrayList;

    iget-object p2, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResults:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResults:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/document/search/SearchResult;

    .line 3
    iget v1, v0, Lcom/pspdfkit/document/search/SearchResult;->pageIndex:I

    if-ne v1, p3, :cond_0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResultDrawableCache:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/search/SearchResultDrawable;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    .line 7
    invoke-virtual {v1}, Lcom/pspdfkit/ui/search/SearchResultDrawable;->isSelected()Z

    move-result v1

    iget-object v4, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->selectedSearchResult:Lcom/pspdfkit/document/search/SearchResult;

    if-ne v0, v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-ne v1, v4, :cond_2

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResultDrawableCache:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/drawable/PdfDrawable;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 10
    :cond_2
    new-instance v1, Lcom/pspdfkit/ui/search/SearchResultDrawable;

    iget-object v4, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->selectedSearchResult:Lcom/pspdfkit/document/search/SearchResult;

    if-ne v0, v4, :cond_3

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    invoke-direct {v1, v0, v2}, Lcom/pspdfkit/ui/search/SearchResultDrawable;-><init>(Lcom/pspdfkit/document/search/SearchResult;Z)V

    .line 12
    iget-object v2, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iget v3, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->noteAnnotationSizePx:I

    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/ui/search/SearchResultDrawable;->applyTheme(Lcom/pspdfkit/internal/rq;I)V

    .line 13
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 14
    iget-object v2, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResultDrawableCache:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_4
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getSearchResultAnimationPadding()I
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iget v0, v0, Lcom/pspdfkit/internal/rq;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSearchResultAnnotationPadding()I
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iget v0, v0, Lcom/pspdfkit/internal/rq;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSearchResultBackgroundColor()I
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iget v0, v0, Lcom/pspdfkit/internal/rq;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSearchResultBorderColor()I
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iget v0, v0, Lcom/pspdfkit/internal/rq;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSearchResultBorderWidth()I
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iget v0, v0, Lcom/pspdfkit/internal/rq;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSearchResultCornerRadiusToHeightRatio()F
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iget v0, v0, Lcom/pspdfkit/internal/rq;->g:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSearchResultMaxCornerRadius()I
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iget v0, v0, Lcom/pspdfkit/internal/rq;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSearchResultMinCornerRadius()I
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iget v0, v0, Lcom/pspdfkit/internal/rq;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSearchResultPadding()I
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iget v0, v0, Lcom/pspdfkit/internal/rq;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setSearchResultAnimationPadding(I)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iput p1, v0, Lcom/pspdfkit/internal/rq;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setSearchResultAnnotationPadding(I)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iput p1, v0, Lcom/pspdfkit/internal/rq;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setSearchResultBackgroundColor(I)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iput p1, v0, Lcom/pspdfkit/internal/rq;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setSearchResultBorderColor(I)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iput p1, v0, Lcom/pspdfkit/internal/rq;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setSearchResultBorderWidth(I)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iput p1, v0, Lcom/pspdfkit/internal/rq;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setSearchResultCornerRadiusToHeightRatio(F)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iput p1, v0, Lcom/pspdfkit/internal/rq;->g:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setSearchResultMaxCornerRadius(I)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iput p1, v0, Lcom/pspdfkit/internal/rq;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setSearchResultMinCornerRadius(I)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iput p1, v0, Lcom/pspdfkit/internal/rq;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setSearchResultPadding(I)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->themeConfiguration:Lcom/pspdfkit/internal/rq;

    iput p1, v0, Lcom/pspdfkit/internal/rq;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setSearchResults(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "results"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResults:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    .line 56
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 57
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResults:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 58
    iput-object v1, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->selectedSearchResult:Lcom/pspdfkit/document/search/SearchResult;

    .line 59
    iget-object p1, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResultDrawableCache:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/HashMap;->clear()V

    .line 60
    invoke-virtual {p0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->notifyDrawablesChanged()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setSelectedSearchResult(Lcom/pspdfkit/document/search/SearchResult;)V
    .locals 2

    monitor-enter p0

    if-eqz p1, :cond_1

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->searchResults:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Can\'t select a SearchResult that wasn\'t previously provided using SearchResultHighlighter#setSearchResults."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 6
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->selectedSearchResult:Lcom/pspdfkit/document/search/SearchResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_2

    monitor-exit p0

    return-void

    .line 9
    :cond_2
    :try_start_1
    iput-object p1, p0, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->selectedSearchResult:Lcom/pspdfkit/document/search/SearchResult;

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    .line 14
    iget v0, v0, Lcom/pspdfkit/document/search/SearchResult;->pageIndex:I

    iget v1, p1, Lcom/pspdfkit/document/search/SearchResult;->pageIndex:I

    if-ne v0, v1, :cond_3

    .line 15
    invoke-virtual {p0, v1}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->notifyDrawablesChanged(I)V

    goto :goto_1

    .line 17
    :cond_3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->notifyDrawablesChanged(I)V

    .line 18
    iget p1, p1, Lcom/pspdfkit/document/search/SearchResult;->pageIndex:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->notifyDrawablesChanged(I)V

    goto :goto_1

    :cond_4
    if-eqz v0, :cond_5

    .line 21
    iget p1, v0, Lcom/pspdfkit/document/search/SearchResult;->pageIndex:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->notifyDrawablesChanged(I)V

    goto :goto_1

    :cond_5
    if-eqz p1, :cond_6

    .line 23
    iget p1, p1, Lcom/pspdfkit/document/search/SearchResult;->pageIndex:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->notifyDrawablesChanged(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.class public interface abstract Lcom/pspdfkit/ui/search/PdfSearchView;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/search/PdfSearchView$Listener;
    }
.end annotation


# virtual methods
.method public abstract clearSearch()V
.end method

.method public abstract isShown()Z
.end method

.method public abstract setInputFieldText(Ljava/lang/String;Z)V
.end method

.method public abstract setSearchConfiguration(Lcom/pspdfkit/configuration/search/SearchConfiguration;)V
.end method

.method public abstract setSearchViewListener(Lcom/pspdfkit/ui/search/PdfSearchView$Listener;)V
.end method

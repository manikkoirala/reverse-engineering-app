.class public final Lcom/pspdfkit/ui/search/PdfSearchViewModular;
.super Lcom/pspdfkit/ui/search/AbstractPdfSearchView;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/search/PdfSearchViewModular$InteractionHandler;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final SEARCH_VIEW_WIDTH_DP:I = 0x1e0

.field private static final bottomShadow:Landroid/graphics/drawable/GradientDrawable;

.field private static final leftShadow:Landroid/graphics/drawable/GradientDrawable;


# instance fields
.field private adapter:Lcom/pspdfkit/internal/sq;

.field private animationRunning:Z

.field private backgroundColor:I

.field private footer:Landroid/widget/TextView;

.field private highlightBackgroundColor:I

.field private highlightBorderColor:I

.field private highlightTextColor:I

.field private inputFieldBackgroundColor:I

.field private inputFieldHintColor:I

.field private inputFieldTextColor:I

.field private itemTheme:Lcom/pspdfkit/internal/sq$a;

.field private listItemBackgroundColor:I

.field private listItemSubtitleColor:I

.field private listItemTitleColor:I

.field private listSelector:I

.field resultList:Landroid/widget/ListView;

.field private rootView:Landroid/view/View;

.field private separatorColor:I

.field private showPageLabels:Z

.field private throbber:Landroid/widget/ProgressBar;


# direct methods
.method static bridge synthetic -$$Nest$fputanimationRunning(Lcom/pspdfkit/ui/search/PdfSearchViewModular;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->animationRunning:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 8
    sget-object v0, Landroid/graphics/drawable/GradientDrawable$Orientation;->RIGHT_LEFT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 9
    invoke-static {v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/graphics/drawable/GradientDrawable$Orientation;)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->leftShadow:Landroid/graphics/drawable/GradientDrawable;

    .line 13
    sget-object v0, Landroid/graphics/drawable/GradientDrawable$Orientation;->BOTTOM_TOP:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 14
    invoke-static {v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/graphics/drawable/GradientDrawable$Orientation;)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->bottomShadow:Landroid/graphics/drawable/GradientDrawable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 2
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__modularSearchStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    .line 3
    iput-boolean p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->animationRunning:Z

    const/4 p1, 0x1

    .line 434
    iput-boolean p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->showPageLabels:Z

    return-void
.end method


# virtual methods
.method public bridge synthetic addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    return-void
.end method

.method protected applyTheme()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->rootView:Landroid/view/View;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__separator:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 4
    new-instance v1, Lcom/pspdfkit/internal/sq$a;

    invoke-direct {v1}, Lcom/pspdfkit/internal/sq$a;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->itemTheme:Lcom/pspdfkit/internal/sq$a;

    .line 5
    iget v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->highlightBackgroundColor:I

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/sq$a;->a(I)V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->itemTheme:Lcom/pspdfkit/internal/sq$a;

    iget v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->highlightTextColor:I

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/sq$a;->b(I)V

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->itemTheme:Lcom/pspdfkit/internal/sq$a;

    iget v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemBackgroundColor:I

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/sq$a;->c(I)V

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->itemTheme:Lcom/pspdfkit/internal/sq$a;

    iget v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemTitleColor:I

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/sq$a;->e(I)V

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->itemTheme:Lcom/pspdfkit/internal/sq$a;

    iget v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemSubtitleColor:I

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/sq$a;->d(I)V

    const/4 v1, 0x0

    .line 12
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->backgroundColor:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 13
    iget v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->separatorColor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    iget v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->inputFieldBackgroundColor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    iget v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->inputFieldTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    iget v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->inputFieldHintColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHintTextColor(I)V

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    new-instance v1, Lcom/pspdfkit/ui/search/PdfSearchViewModular$2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular$2;-><init>(Lcom/pspdfkit/ui/search/PdfSearchViewModular;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    new-instance v1, Lcom/pspdfkit/ui/search/PdfSearchViewModular$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/search/PdfSearchViewModular;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 37
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listSelector:I

    if-eqz v0, :cond_0

    .line 38
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->resultList:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->setSelector(I)V

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->footer:Landroid/widget/TextView;

    iget v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemBackgroundColor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->footer:Landroid/widget/TextView;

    iget v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemTitleColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public bridge synthetic clearDocument()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->clearDocument()V

    return-void
.end method

.method public bridge synthetic clearOnVisibilityChangedListeners()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->clearOnVisibilityChangedListeners()V

    return-void
.end method

.method protected clearSearchResults()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->resultList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 3

    .line 1
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0, v1, v2, p1}, Landroid/view/View;->setPadding(IIII)V

    const/4 p1, 0x0

    return p1
.end method

.method public getBackgroundColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->backgroundColor:I

    return v0
.end method

.method public getHighlightBackgroundColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->highlightBackgroundColor:I

    return v0
.end method

.method public getHighlightBorderColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->highlightBorderColor:I

    return v0
.end method

.method public getHighlightTextColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->highlightTextColor:I

    return v0
.end method

.method public getInputFieldBackgroundColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->inputFieldBackgroundColor:I

    return v0
.end method

.method public getInputFieldHintColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->inputFieldHintColor:I

    return v0
.end method

.method public getInputFieldTextColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->inputFieldTextColor:I

    return v0
.end method

.method public getListItemBackgroundColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemBackgroundColor:I

    return v0
.end method

.method public getListItemBackgroundColor(I)V
    .locals 0

    .line 2
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemBackgroundColor:I

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->applyTheme()V

    return-void
.end method

.method public getListItemSubtitleColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemSubtitleColor:I

    return v0
.end method

.method public getListItemTitleColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemTitleColor:I

    return v0
.end method

.method public getListSelector()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listSelector:I

    return v0
.end method

.method public bridge synthetic getMaxSearchResults()Ljava/lang/Integer;
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->getMaxSearchResults()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    return-object v0
.end method

.method public getSeparatorColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->separatorColor:I

    return v0
.end method

.method public bridge synthetic getSnippetLength()I
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->getSnippetLength()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getStartSearchChars()I
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->getStartSearchChars()I

    move-result v0

    return v0
.end method

.method public hide()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed:Z

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->onHide(Landroid/view/View;)V

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->hideKeyboard()V

    .line 5
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/search/PdfSearchViewModular$4;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular$4;-><init>(Lcom/pspdfkit/ui/search/PdfSearchViewModular;)V

    .line 8
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method protected init()V
    .locals 7

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewModular:[I

    sget v3, Lcom/pspdfkit/R$attr;->pspdf__modularSearchStyle:I

    sget v4, Lcom/pspdfkit/R$style;->PSPDFKit_SearchViewModular:I

    const/4 v5, 0x0

    .line 4
    invoke-virtual {v1, v5, v2, v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 9
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewModular_pspdf__backgroundColor:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 11
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 12
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->backgroundColor:I

    .line 15
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewModular_pspdf__inputFieldTextColor:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_gray_dark:I

    .line 17
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 18
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->inputFieldTextColor:I

    .line 21
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewModular_pspdf__inputFieldHintColor:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_gray:I

    .line 23
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 24
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->inputFieldHintColor:I

    .line 27
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewModular_pspdf__inputFieldBackgroundColor:I

    const v3, 0x106000d

    .line 29
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    .line 30
    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->inputFieldBackgroundColor:I

    .line 33
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewModular_pspdf__separatorColor:I

    sget v4, Lcom/pspdfkit/R$color;->pspdf__color_gray_light:I

    .line 35
    invoke-static {v0, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    .line 36
    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->separatorColor:I

    .line 39
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewModular_pspdf__listItemSelector:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listSelector:I

    .line 40
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewModular_pspdf__listItemBackgroundColor:I

    .line 42
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 43
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemBackgroundColor:I

    .line 46
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewModular_pspdf__listItemTitleColor:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_gray:I

    .line 48
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 49
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemTitleColor:I

    .line 52
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewModular_pspdf__listItemSubtitleColor:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_gray_dark:I

    .line 54
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 55
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemSubtitleColor:I

    .line 58
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewModular_pspdf__highlightBackgroundColor:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_highlight:I

    .line 60
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 61
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->highlightBackgroundColor:I

    .line 64
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewModular_pspdf__highlightTextColor:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_black:I

    .line 66
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 67
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->highlightTextColor:I

    .line 70
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewModular_pspdf__highlightBorderColor:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_black:I

    .line 72
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 73
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->highlightBorderColor:I

    .line 76
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 78
    sget v1, Lcom/pspdfkit/R$layout;->pspdf__search_view_modular:I

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->rootView:Landroid/view/View;

    .line 80
    sget v2, Lcom/pspdfkit/R$id;->pspdf__search_edit_text_modular:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    .line 81
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->rootView:Landroid/view/View;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__search_resultlist:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->resultList:Landroid/widget/ListView;

    .line 82
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->rootView:Landroid/view/View;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__search_progress_modular:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->throbber:Landroid/widget/ProgressBar;

    const-string v1, "layout_inflater"

    .line 85
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 86
    sget v1, Lcom/pspdfkit/R$layout;->pspdf__search_footer:I

    iget-object v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->resultList:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->footer:Landroid/widget/TextView;

    .line 89
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/search/PdfSearchViewModular$1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular$1;-><init>(Lcom/pspdfkit/ui/search/PdfSearchViewModular;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 98
    new-instance v0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$InteractionHandler;

    invoke-direct {v0, p0, v5}, Lcom/pspdfkit/ui/search/PdfSearchViewModular$InteractionHandler;-><init>(Lcom/pspdfkit/ui/search/PdfSearchViewModular;Lcom/pspdfkit/ui/search/PdfSearchViewModular$InteractionHandler-IA;)V

    .line 99
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->resultList:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 100
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->resultList:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 101
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->resultList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->footer:Landroid/widget/TextView;

    .line 102
    new-instance v2, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 103
    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    const/4 v6, -0x1

    invoke-direct {v3, v6, v4}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v3, 0x1

    .line 104
    invoke-virtual {v0, v2, v5, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 106
    invoke-virtual {v0, v1, v5, v4}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 107
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x43f00000    # 480.0f

    mul-float v0, v0, v1

    .line 113
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    const v2, 0x3f99999a    # 1.2f

    mul-float v2, v2, v0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 114
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    float-to-int v0, v0

    invoke-direct {v1, v0, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const v0, 0x800005

    .line 115
    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 116
    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    const/4 v0, 0x4

    .line 119
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public bridge synthetic isDisplayed()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed()Z

    move-result v0

    return v0
.end method

.method isIdle()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->animationRunning:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchInProgress:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->hasTransientState()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public bridge synthetic isStartSearchOnCurrentPage()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isStartSearchOnCurrentPage()Z

    move-result v0

    return v0
.end method

.method synthetic lambda$applyTheme$0$com-pspdfkit-ui-search-PdfSearchViewModular(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/16 p1, 0x42

    if-ne p2, p1, :cond_0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->hideKeyboard()V

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method protected onMeasure(II)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    const/high16 p2, 0x42c80000    # 100.0f

    mul-float p1, p1, p2

    float-to-int p1, p1

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p2

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0, p2, v0}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public onMoreSearchResults(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->adapter:Lcom/pspdfkit/internal/sq;

    .line 2
    iget-object v1, v0, Lcom/pspdfkit/internal/sq;->d:Ljava/util/ArrayList;

    .line 3
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 4
    iget-object p1, v0, Lcom/pspdfkit/internal/sq;->d:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 5
    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public bridge synthetic onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V

    return-void
.end method

.method public onSearchCanceled()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->throbber:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->footer:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onSearchComplete()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__search_complete:I

    invoke-static {v0, v1, p0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$plurals;->pspdf__search_results_found:I

    iget-object v3, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->adapter:Lcom/pspdfkit/internal/sq;

    .line 3
    invoke-virtual {v3}, Lcom/pspdfkit/internal/sq;->getCount()I

    move-result v3

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->adapter:Lcom/pspdfkit/internal/sq;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/sq;->getCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-virtual {v1, v2, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 4
    new-instance v2, Landroid/text/SpannableString;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\n"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 5
    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v3, 0x12

    invoke-virtual {v2, v1, v7, v0, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->footer:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->footer:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->throbber:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onSearchError(Ljava/lang/Throwable;)V
    .locals 2

    const-string v0, "View"

    const-string v1, "Failed to retrieve search results."

    .line 1
    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void
.end method

.method public onSearchStarted(Ljava/lang/String;)V
    .locals 3

    .line 1
    new-instance p1, Lcom/pspdfkit/internal/sq;

    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->itemTheme:Lcom/pspdfkit/internal/sq$a;

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__search_item:I

    iget-boolean v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->showPageLabels:Z

    invoke-direct {p1, p0, v0, v1, v2}, Lcom/pspdfkit/internal/sq;-><init>(Landroid/view/View;Lcom/pspdfkit/internal/sq$a;IZ)V

    iput-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->adapter:Lcom/pspdfkit/internal/sq;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->resultList:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->footer:Landroid/widget/TextView;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->throbber:Landroid/widget/ProgressBar;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    cmpg-float p1, p1, v0

    if-gez p1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->hide()V

    :cond_0
    return v1
.end method

.method public bridge synthetic removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->backgroundColor:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->applyTheme()V

    return-void
.end method

.method public bridge synthetic setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    return-void
.end method

.method public setHighlightBackgroundColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->highlightBackgroundColor:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->applyTheme()V

    return-void
.end method

.method public setHighlightBorderColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->highlightBorderColor:I

    return-void
.end method

.method public setHighlightTextColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->highlightTextColor:I

    return-void
.end method

.method public setInputFieldBackgroundColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->inputFieldBackgroundColor:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->applyTheme()V

    return-void
.end method

.method public setInputFieldHintColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->inputFieldHintColor:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->applyTheme()V

    return-void
.end method

.method public bridge synthetic setInputFieldText(Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setInputFieldText(Ljava/lang/String;Z)V

    return-void
.end method

.method public setInputFieldTextColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->inputFieldTextColor:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->applyTheme()V

    return-void
.end method

.method public setListItemSubtitleColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemSubtitleColor:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->applyTheme()V

    return-void
.end method

.method public setListItemTitleColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listItemTitleColor:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->applyTheme()V

    return-void
.end method

.method public setListSelector(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->listSelector:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->applyTheme()V

    return-void
.end method

.method public bridge synthetic setMaxSearchResults(Ljava/lang/Integer;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setMaxSearchResults(Ljava/lang/Integer;)V

    return-void
.end method

.method public bridge synthetic setSearchConfiguration(Lcom/pspdfkit/configuration/search/SearchConfiguration;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setSearchConfiguration(Lcom/pspdfkit/configuration/search/SearchConfiguration;)V

    return-void
.end method

.method public setSeparatorColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->separatorColor:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->applyTheme()V

    return-void
.end method

.method public setShowPageLabels(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->showPageLabels:Z

    return-void
.end method

.method public bridge synthetic setSnippetLength(I)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setSnippetLength(I)V

    return-void
.end method

.method public bridge synthetic setStartSearchChars(I)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setStartSearchChars(I)V

    return-void
.end method

.method public bridge synthetic setStartSearchOnCurrentPage(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setStartSearchOnCurrentPage(Z)V

    return-void
.end method

.method public show()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->show()V

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed:Z

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->onShow(Landroid/view/View;)V

    const/4 v0, 0x0

    .line 5
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 6
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 8
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;-><init>(Lcom/pspdfkit/ui/search/PdfSearchViewModular;)V

    .line 9
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

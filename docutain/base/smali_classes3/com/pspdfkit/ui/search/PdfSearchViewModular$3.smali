.class Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/ui/search/PdfSearchViewModular;->show()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/search/PdfSearchViewModular;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->-$$Nest$fputanimationRunning(Lcom/pspdfkit/ui/search/PdfSearchViewModular;Z)V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->-$$Nest$fputanimationRunning(Lcom/pspdfkit/ui/search/PdfSearchViewModular;Z)V

    .line 2
    iget-boolean v0, p1, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed:Z

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {p1}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    iget-object p1, p1, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_1

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->showKeyboard()V

    goto :goto_0

    .line 9
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    iget-object p1, p1, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->resultList:Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    iget-object p1, p1, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->resultList:Landroid/widget/ListView;

    .line 10
    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object p1

    invoke-interface {p1}, Landroid/widget/Adapter;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    iget-object p1, p1, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    .line 11
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->getStartSearchChars()I

    move-result v0

    if-lt p1, v0, :cond_3

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->clearSearch()V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    iget-object v0, p1, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->performSearch(Ljava/lang/String;)V

    .line 17
    :cond_3
    :goto_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    const-string v0, "start_search"

    .line 18
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    const-string v0, "search_type"

    const-string v1, "SEARCH_MODULAR"

    .line 19
    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 20
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$3;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->-$$Nest$fputanimationRunning(Lcom/pspdfkit/ui/search/PdfSearchViewModular;Z)V

    return-void
.end method

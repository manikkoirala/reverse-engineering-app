.class Lcom/pspdfkit/ui/search/PdfSearchViewInline$1;
.super Lcom/pspdfkit/internal/fs;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/ui/search/PdfSearchViewInline;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/search/PdfSearchViewInline;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/search/PdfSearchViewInline;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline$1;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewInline;

    invoke-direct {p0}, Lcom/pspdfkit/internal/fs;-><init>()V

    return-void
.end method


# virtual methods
.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline$1;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewInline;

    invoke-virtual {p2}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->clearSearch()V

    .line 3
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p2

    iget-object p3, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline$1;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewInline;

    invoke-virtual {p3}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->getStartSearchChars()I

    move-result p3

    if-lt p2, p3, :cond_0

    .line 4
    iget-object p2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline$1;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewInline;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->performSearch(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

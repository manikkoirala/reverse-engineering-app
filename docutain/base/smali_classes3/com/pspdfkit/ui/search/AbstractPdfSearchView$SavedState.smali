.class Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/search/AbstractPdfSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private isDisplayingSearchResults:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetisDisplayingSearchResults(Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;->isDisplayingSearchResults:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputisDisplayingSearchResults(Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;->isDisplayingSearchResults:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState$1;

    invoke-direct {v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState$1;-><init>()V

    sput-object v0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 2
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 3
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;->isDisplayingSearchResults:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2
    iget-boolean p2, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;->isDisplayingSearchResults:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

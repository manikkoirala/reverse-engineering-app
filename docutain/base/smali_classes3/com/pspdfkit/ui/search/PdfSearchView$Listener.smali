.class public interface abstract Lcom/pspdfkit/ui/search/PdfSearchView$Listener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/search/PdfSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onMoreSearchResults(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onSearchCleared()V
.end method

.method public abstract onSearchCompleted()V
.end method

.method public abstract onSearchError(Ljava/lang/Throwable;)V
.end method

.method public abstract onSearchResultSelected(Lcom/pspdfkit/document/search/SearchResult;)V
.end method

.method public abstract onSearchStarted(Ljava/lang/String;)V
.end method

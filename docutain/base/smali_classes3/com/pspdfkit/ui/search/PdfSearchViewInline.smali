.class public final Lcom/pspdfkit/ui/search/PdfSearchViewInline;
.super Lcom/pspdfkit/ui/search/AbstractPdfSearchView;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/search/PdfSearchViewInline$ClickListener;,
        Lcom/pspdfkit/ui/search/PdfSearchViewInline$SavedState;
    }
.end annotation


# static fields
.field public static final INLINE_SEARCH_SHOW_DELAY:I = 0x12c

.field private static final NO_SEARCH_RESULT_SELECTED:I = -0x1


# instance fields
.field private backIconColorTint:I

.field private btnBack:Landroid/widget/ImageButton;

.field private btnNextResult:Landroid/widget/ImageButton;

.field private btnPreviousResult:Landroid/widget/ImageButton;

.field private comingFromSavedState:Z

.field private currentResultTextView:Landroid/widget/TextView;

.field private inputFieldTextAppearance:I

.field private nextIcon:I

.field private nextIconColorTint:I

.field private noResultsFound:Landroid/widget/TextView;

.field private prevIcon:I

.field private prevIconColorTint:I

.field private resultTextAppearance:I

.field private final results:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;"
        }
    .end annotation
.end field

.field selectedResultIndex:I

.field private throbber:Landroid/widget/ProgressBar;

.field private throbberColor:I


# direct methods
.method static bridge synthetic -$$Nest$fgetresults(Lcom/pspdfkit/ui/search/PdfSearchViewInline;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mselectSearchResult(Lcom/pspdfkit/ui/search/PdfSearchViewInline;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->selectSearchResult(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 2
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__inlineSearchStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 3
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    const/4 p1, -0x1

    .line 10
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->selectedResultIndex:I

    const/4 p1, 0x0

    .line 13
    iput-boolean p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->comingFromSavedState:Z

    return-void
.end method

.method private fadeInView(Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 3
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    .line 4
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 5
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 6
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const/4 v0, 0x0

    .line 8
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method private fadeOutView(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->fadeOutView(Landroid/view/View;Z)V

    return-void
.end method

.method private fadeOutView(Landroid/view/View;Z)V
    .locals 3

    .line 2
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 4
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 5
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x10e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/search/PdfSearchViewInline$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/ui/search/PdfSearchViewInline$2;-><init>(Lcom/pspdfkit/ui/search/PdfSearchViewInline;Landroid/view/View;Z)V

    .line 8
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    :cond_1
    :goto_0
    return-void
.end method

.method private hideSearchResultsNavigation()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnNextResult:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnPreviousResult:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->currentResultTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->noResultsFound:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private isRtl()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private selectSearchResult(I)V
    .locals 3

    if-ltz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt p1, v0, :cond_0

    .line 5
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->selectedResultIndex:I

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/document/search/SearchResult;

    .line 7
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->dispatchSearchResultSelected(Lcom/pspdfkit/document/search/SearchResult;)V

    .line 8
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->selectedResultIndex:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->showSearchResultsNavigation(II)V

    .line 10
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "select_search_result"

    .line 11
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    iget p1, p1, Lcom/pspdfkit/document/search/SearchResult;->pageIndex:I

    const-string v1, "page_index"

    .line 12
    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->selectedResultIndex:I

    .line 13
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "sort"

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void

    .line 15
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Search result number "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " doesn\'t exist"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private showSearchResultsNavigation(II)V
    .locals 4

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->noResultsFound:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->fadeInView(Landroid/view/View;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->currentResultTextView:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->fadeOutView(Landroid/view/View;)V

    goto :goto_0

    .line 5
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__search_result_of:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v2, p2

    .line 6
    invoke-static {v0, v1, p0, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->currentResultTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnNextResult:Landroid/widget/ImageButton;

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->fadeInView(Landroid/view/View;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnPreviousResult:Landroid/widget/ImageButton;

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->fadeInView(Landroid/view/View;)V

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->currentResultTextView:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->fadeInView(Landroid/view/View;)V

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->noResultsFound:Landroid/widget/TextView;

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->fadeOutView(Landroid/view/View;Z)V

    :goto_0
    return-void
.end method

.method private showSearchResultsNavigationWhileSearching(I)V
    .locals 6

    if-nez p1, :cond_0

    return-void

    .line 1
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__search_result_of:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->selectedResultIndex:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    .line 4
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v5, 0x0

    aput-object v3, v2, v5

    .line 5
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v4

    .line 6
    invoke-static {v0, v1, p0, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->currentResultTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->currentResultTextView:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->fadeInView(Landroid/view/View;)V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnNextResult:Landroid/widget/ImageButton;

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->fadeInView(Landroid/view/View;)V

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnPreviousResult:Landroid/widget/ImageButton;

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->fadeInView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    return-void
.end method

.method protected applyTheme()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    iget v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->inputFieldTextAppearance:I

    invoke-static {v1, v2}, Landroidx/core/widget/TextViewCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->currentResultTextView:Landroid/widget/TextView;

    iget v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->resultTextAppearance:I

    invoke-static {v1, v2}, Landroidx/core/widget/TextViewCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->noResultsFound:Landroid/widget/TextView;

    iget v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->resultTextAppearance:I

    invoke-static {v1, v2}, Landroidx/core/widget/TextViewCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    .line 10
    invoke-direct {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->isRtl()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11
    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__arrow_right:I

    invoke-static {v0, v1}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0

    .line 13
    :cond_0
    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_arrow_back:I

    invoke-static {v0, v1}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    .line 16
    iget v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->backIconColorTint:I

    invoke-static {v1, v2}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 18
    :cond_1
    iget-object v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnBack:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 21
    iget v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->prevIcon:I

    invoke-static {v0, v1}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 23
    iget v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->prevIconColorTint:I

    .line 24
    invoke-static {v1}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 25
    invoke-static {v1, v2}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 26
    :cond_2
    iget-object v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnPreviousResult:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 29
    iget v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->nextIcon:I

    invoke-static {v0, v1}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 31
    iget v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->nextIconColorTint:I

    .line 32
    invoke-static {v0}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 33
    invoke-static {v0, v1}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 34
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnNextResult:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->throbber:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 38
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->throbber:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->throbberColor:I

    .line 39
    invoke-static {v0}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 40
    invoke-static {v0, v1}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 41
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->throbber:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    return-void
.end method

.method public bridge synthetic clearDocument()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->clearDocument()V

    return-void
.end method

.method public bridge synthetic clearOnVisibilityChangedListeners()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->clearOnVisibilityChangedListeners()V

    return-void
.end method

.method protected clearSearchResults()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnNextResult:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnPreviousResult:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->currentResultTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->noResultsFound:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public getBackIconColorTint()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->backIconColorTint:I

    return v0
.end method

.method public getHintTextColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCurrentHintTextColor()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getMaxSearchResults()Ljava/lang/Integer;
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->getMaxSearchResults()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getNavigationTextColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->noResultsFound:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v0

    return v0
.end method

.method public getNextIcon()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->nextIcon:I

    return v0
.end method

.method public getNextIconColorTint()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->nextIconColorTint:I

    return v0
.end method

.method public bridge synthetic getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    return-object v0
.end method

.method public getPrevIcon()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->prevIcon:I

    return v0
.end method

.method public getPrevIconColorTint()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->prevIconColorTint:I

    return v0
.end method

.method public bridge synthetic getSnippetLength()I
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->getSnippetLength()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getStartSearchChars()I
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->getStartSearchChars()I

    move-result v0

    return v0
.end method

.method public getTextColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v0

    return v0
.end method

.method public hide()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed:Z

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->hideKeyboard()V

    const/4 v0, 0x4

    .line 11
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->onHide(Landroid/view/View;)V

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 17
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "exit_search"

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method protected init()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewInline:[I

    sget v3, Lcom/pspdfkit/R$attr;->pspdf__inlineSearchStyle:I

    sget v4, Lcom/pspdfkit/R$style;->PSPDFKit_SearchViewInline:I

    const/4 v5, 0x0

    .line 4
    invoke-virtual {v1, v5, v2, v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 9
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewInline_pspdf__prevIconColorTint:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 11
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 12
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->prevIconColorTint:I

    .line 15
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewInline_pspdf__nextIconColorTint:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 17
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 18
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->nextIconColorTint:I

    .line 21
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewInline_pspdf__backIconColorTint:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 23
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 24
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->backIconColorTint:I

    .line 27
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewInline_pspdf__throbberColor:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 29
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 30
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->throbberColor:I

    .line 33
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewInline_pspdf__prevIconDrawable:I

    sget v3, Lcom/pspdfkit/R$drawable;->pspdf__ic_chevron_left:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->prevIcon:I

    .line 35
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewInline_pspdf__nextIconDrawable:I

    sget v3, Lcom/pspdfkit/R$drawable;->pspdf__ic_chevron_right:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->nextIcon:I

    .line 37
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewInline_pspdf__inputFieldTextAppearance:I

    sget v3, Lcom/pspdfkit/R$style;->PSPDFKit_SearchViewInline_InputFieldTextAppearance:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->inputFieldTextAppearance:I

    .line 40
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SearchViewInline_pspdf__resultTextAppearance:I

    sget v3, Lcom/pspdfkit/R$style;->PSPDFKit_SearchViewInline_ResultTextAppearance:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->resultTextAppearance:I

    .line 43
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 46
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__search_view_inline:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 48
    sget v1, Lcom/pspdfkit/R$id;->pspdf__search_edit_text_inline:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    .line 49
    sget v1, Lcom/pspdfkit/R$id;->pspdf__search_progress_inline:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->throbber:Landroid/widget/ProgressBar;

    .line 50
    sget v1, Lcom/pspdfkit/R$id;->pspdf__search_btn_back:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnBack:Landroid/widget/ImageButton;

    .line 51
    sget v1, Lcom/pspdfkit/R$id;->pspdf__search_btn_prev:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnPreviousResult:Landroid/widget/ImageButton;

    .line 52
    sget v1, Lcom/pspdfkit/R$id;->pspdf__search_btn_next:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnNextResult:Landroid/widget/ImageButton;

    .line 53
    sget v1, Lcom/pspdfkit/R$id;->pspdf__search_tv_current_result:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->currentResultTextView:Landroid/widget/TextView;

    .line 54
    sget v1, Lcom/pspdfkit/R$id;->pspdf__search_tv_no_matches_found:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->noResultsFound:Landroid/widget/TextView;

    .line 57
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    new-instance v1, Lcom/pspdfkit/ui/search/PdfSearchViewInline$1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline$1;-><init>(Lcom/pspdfkit/ui/search/PdfSearchViewInline;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 68
    new-instance v0, Lcom/pspdfkit/ui/search/PdfSearchViewInline$ClickListener;

    invoke-direct {v0, p0, v5}, Lcom/pspdfkit/ui/search/PdfSearchViewInline$ClickListener;-><init>(Lcom/pspdfkit/ui/search/PdfSearchViewInline;Lcom/pspdfkit/ui/search/PdfSearchViewInline$ClickListener-IA;)V

    .line 69
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnBack:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnPreviousResult:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->btnNextResult:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    .line 75
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->setSnippetLength(I)V

    return-void
.end method

.method public bridge synthetic isDisplayed()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed()Z

    move-result v0

    return v0
.end method

.method isIdle()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchInProgress:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public bridge synthetic isStartSearchOnCurrentPage()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isStartSearchOnCurrentPage()Z

    move-result v0

    return v0
.end method

.method public onMoreSearchResults(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->showSearchResultsNavigationWhileSearching(I)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/ui/search/PdfSearchViewInline$SavedState;

    if-nez v0, :cond_0

    .line 2
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 6
    :cond_0
    check-cast p1, Lcom/pspdfkit/ui/search/PdfSearchViewInline$SavedState;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 8
    invoke-static {p1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline$SavedState;->-$$Nest$fgetcurrentHighlightedResult(Lcom/pspdfkit/ui/search/PdfSearchViewInline$SavedState;)I

    move-result p1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 9
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->selectedResultIndex:I

    const/4 p1, 0x1

    .line 10
    iput-boolean p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->comingFromSavedState:Z

    :cond_1
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/ui/search/PdfSearchViewInline$SavedState;

    invoke-direct {v1, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 3
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->selectedResultIndex:I

    invoke-static {v1, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline$SavedState;->-$$Nest$fputcurrentHighlightedResult(Lcom/pspdfkit/ui/search/PdfSearchViewInline$SavedState;I)V

    return-object v1
.end method

.method public onSearchCanceled()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->throbber:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->hideSearchResultsNavigation()V

    return-void
.end method

.method public onSearchComplete()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->throbber:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_3

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->comingFromSavedState:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->selectedResultIndex:I

    const/4 v2, -0x1

    if-le v0, v2, :cond_0

    iget-object v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    .line 5
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 7
    iget v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->selectedResultIndex:I

    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->selectSearchResult(I)V

    .line 8
    iput-boolean v1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->comingFromSavedState:Z

    goto :goto_2

    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 13
    iget-object v2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/document/search/SearchResult;

    iget v2, v2, Lcom/pspdfkit/document/search/SearchResult;->pageIndex:I

    iget v3, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->currentPage:I

    if-lt v2, v3, :cond_1

    move v1, v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 18
    :cond_2
    :goto_1
    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->selectSearchResult(I)V

    goto :goto_2

    .line 21
    :cond_3
    invoke-direct {p0, v1, v1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->showSearchResultsNavigation(II)V

    :goto_2
    return-void
.end method

.method public onSearchError(Ljava/lang/Throwable;)V
    .locals 2

    const-string v0, "View"

    const-string v1, "Failed to retrieve search results."

    .line 1
    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void
.end method

.method public onSearchStarted(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->hideSearchResultsNavigation()V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->throbber:Landroid/widget/ProgressBar;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v0

    int-to-float v0, v0

    cmpl-float p1, p1, v0

    if-lez p1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->hide()V

    :cond_0
    return v1
.end method

.method public bridge synthetic removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    return-void
.end method

.method public setBackIconColorTint(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->backIconColorTint:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->applyTheme()V

    return-void
.end method

.method public bridge synthetic setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    return-void
.end method

.method public setHintTextColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setHintTextColor(I)V

    return-void
.end method

.method public bridge synthetic setInputFieldText(Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setInputFieldText(Ljava/lang/String;Z)V

    return-void
.end method

.method public bridge synthetic setMaxSearchResults(Ljava/lang/Integer;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setMaxSearchResults(Ljava/lang/Integer;)V

    return-void
.end method

.method public setNavigationTextColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->noResultsFound:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->currentResultTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setNextIcon(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->nextIcon:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->applyTheme()V

    return-void
.end method

.method public setNextIconColorTint(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->nextIconColorTint:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->applyTheme()V

    return-void
.end method

.method public setPrevIcon(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->prevIcon:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->applyTheme()V

    return-void
.end method

.method public setPrevIconColorTint(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->prevIconColorTint:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->applyTheme()V

    return-void
.end method

.method public bridge synthetic setSearchConfiguration(Lcom/pspdfkit/configuration/search/SearchConfiguration;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setSearchConfiguration(Lcom/pspdfkit/configuration/search/SearchConfiguration;)V

    return-void
.end method

.method public bridge synthetic setSnippetLength(I)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setSnippetLength(I)V

    return-void
.end method

.method public bridge synthetic setStartSearchChars(I)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setStartSearchChars(I)V

    return-void
.end method

.method public bridge synthetic setStartSearchOnCurrentPage(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->setStartSearchOnCurrentPage(Z)V

    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public show()V
    .locals 3

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->show()V

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed:Z

    const/4 v0, 0x0

    .line 6
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->onShow(Landroid/view/View;)V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->results:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;->getStartSearchChars()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->clearSearch()V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->performSearch(Ljava/lang/String;)V

    goto :goto_0

    .line 15
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->showKeyboard()V

    .line 18
    :goto_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "start_search"

    .line 19
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    const-string v1, "search_type"

    const-string v2, "SEARCH_INLINE"

    .line 20
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

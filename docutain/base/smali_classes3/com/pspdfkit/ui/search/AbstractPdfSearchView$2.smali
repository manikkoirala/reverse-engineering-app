.class Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;
.super Lio/reactivex/rxjava3/subscribers/DisposableSubscriber;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->performSearch(Ljava/lang/String;Ljava/util/EnumSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/rxjava3/subscribers/DisposableSubscriber<",
        "Ljava/util/List<",
        "Lcom/pspdfkit/document/search/SearchResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/search/AbstractPdfSearchView;

.field final synthetic val$searchQuery:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;->this$0:Lcom/pspdfkit/ui/search/AbstractPdfSearchView;

    iput-object p2, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;->val$searchQuery:Ljava/lang/String;

    invoke-direct {p0}, Lio/reactivex/rxjava3/subscribers/DisposableSubscriber;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;->this$0:Lcom/pspdfkit/ui/search/AbstractPdfSearchView;

    invoke-static {v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->-$$Nest$fgetsearchResults(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;)Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-static {v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->-$$Nest$fgetsearchViewListener(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;)Lcom/pspdfkit/ui/search/PdfSearchView$Listener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/ui/search/PdfSearchView$Listener;->onSearchCompleted()V

    .line 10
    :cond_1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "perform_search"

    .line 11
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;->val$searchQuery:Ljava/lang/String;

    .line 12
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const-string v2, "length"

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;->this$0:Lcom/pspdfkit/ui/search/AbstractPdfSearchView;

    invoke-static {v1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->-$$Nest$fgetsearchResults(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;)Ljava/util/List;

    move-result-object v1

    .line 13
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const-string v2, "count"

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 19
    invoke-virtual {p0}, Lio/reactivex/rxjava3/subscribers/DisposableSubscriber;->dispose()V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;->this$0:Lcom/pspdfkit/ui/search/AbstractPdfSearchView;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->onSearchComplete()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;->this$0:Lcom/pspdfkit/ui/search/AbstractPdfSearchView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->-$$Nest$fputsearchResults(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;Ljava/util/List;)V

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->onSearchError(Ljava/lang/Throwable;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;->this$0:Lcom/pspdfkit/ui/search/AbstractPdfSearchView;

    invoke-static {v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->-$$Nest$fgetsearchViewListener(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;)Lcom/pspdfkit/ui/search/PdfSearchView$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/search/PdfSearchView$Listener;->onSearchError(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;->onNext(Ljava/util/List;)V

    return-void
.end method

.method public onNext(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;)V"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;->this$0:Lcom/pspdfkit/ui/search/AbstractPdfSearchView;

    invoke-static {v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->-$$Nest$fgetsearchResults(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 6
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;->this$0:Lcom/pspdfkit/ui/search/AbstractPdfSearchView;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->onMoreSearchResults(Ljava/util/List;)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;->this$0:Lcom/pspdfkit/ui/search/AbstractPdfSearchView;

    invoke-static {v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->-$$Nest$fgetsearchViewListener(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;)Lcom/pspdfkit/ui/search/PdfSearchView$Listener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 10
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/search/PdfSearchView$Listener;->onMoreSearchResults(Ljava/util/List;)V

    :cond_1
    return-void
.end method

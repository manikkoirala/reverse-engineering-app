.class abstract Lcom/pspdfkit/ui/search/AbstractPdfSearchView;
.super Lcom/pspdfkit/internal/d9;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/search/PdfSearchView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;
    }
.end annotation


# static fields
.field private static final CACHE_PRELOAD_KEY:Ljava/lang/String; = "#-CACHE-#"

.field protected static final LOG_TAG:Ljava/lang/String; = "PSPDFKit.SearchView"

.field private static final SEARCH_BUFFER_DURATION:I = 0x12c

.field public static final SEARCH_DELAY:I = 0x12c


# instance fields
.field protected currentPage:I

.field protected document:Lcom/pspdfkit/document/PdfDocument;

.field protected inputField:Landroid/widget/EditText;

.field protected isDisplayed:Z

.field protected final listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

.field private maxSearchResults:Ljava/lang/Integer;

.field protected searchInProgress:Lio/reactivex/rxjava3/disposables/Disposable;

.field private searchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;"
        }
    .end annotation
.end field

.field private searchViewListener:Lcom/pspdfkit/ui/search/PdfSearchView$Listener;

.field private snippetLength:I

.field private startSearchChars:I

.field private startSearchEvenWhenNotDisplayed:Z

.field private startSearchOnCurrentPage:Z

.field private textSearch:Lcom/pspdfkit/document/search/TextSearch;


# direct methods
.method static bridge synthetic -$$Nest$fgetsearchResults(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchResults:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetsearchViewListener(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;)Lcom/pspdfkit/ui/search/PdfSearchView$Listener;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchViewListener:Lcom/pspdfkit/ui/search/PdfSearchView$Listener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputsearchResults(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchResults:Ljava/util/List;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/d9;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2
    new-instance p1, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-direct {p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    const/4 p1, -0x1

    .line 19
    iput p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->currentPage:I

    const/4 p1, 0x2

    .line 27
    iput p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->startSearchChars:I

    const/16 p1, 0x50

    .line 29
    iput p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->snippetLength:I

    const/4 p1, 0x0

    .line 31
    iput-boolean p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->startSearchOnCurrentPage:Z

    .line 51
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->init()V

    .line 52
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->applyTheme()V

    .line 53
    invoke-direct {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->applyPolicy()V

    return-void
.end method

.method private applyPolicy()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->getApplicationPolicy()Lcom/pspdfkit/configuration/policy/ApplicationPolicy;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;->TEXT_COPY_PASTE:Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/policy/ApplicationPolicy;->hasPermissionForEvent(Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    new-instance v1, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$1;-><init>(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$show$0()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    return-void
.end method

.method static synthetic lambda$show$1(Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    return-void
.end method


# virtual methods
.method public addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    return-void
.end method

.method protected abstract applyTheme()V
.end method

.method public clearDocument()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchInProgress:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    .line 2
    :cond_0
    invoke-interface {p0}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->hide()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->document:Lcom/pspdfkit/document/PdfDocument;

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->textSearch:Lcom/pspdfkit/document/search/TextSearch;

    return-void
.end method

.method public clearOnVisibilityChangedListeners()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->clear()V

    return-void
.end method

.method public final clearSearch()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchInProgress:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchInProgress:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchResults:Ljava/util/List;

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->onSearchCanceled()V

    .line 8
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->clearSearchResults()V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchViewListener:Lcom/pspdfkit/ui/search/PdfSearchView$Listener;

    if-eqz v0, :cond_1

    .line 11
    invoke-interface {v0}, Lcom/pspdfkit/ui/search/PdfSearchView$Listener;->onSearchCleared()V

    :cond_1
    return-void
.end method

.method protected abstract clearSearchResults()V
.end method

.method protected final dispatchSearchResultSelected(Lcom/pspdfkit/document/search/SearchResult;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchViewListener:Lcom/pspdfkit/ui/search/PdfSearchView$Listener;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/search/PdfSearchView$Listener;->onSearchResultSelected(Lcom/pspdfkit/document/search/SearchResult;)V

    :cond_0
    return-void
.end method

.method public getMaxSearchResults()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->maxSearchResults:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_SEARCH:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    return-object v0
.end method

.method public final getSearchResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchResults:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSnippetLength()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->snippetLength:I

    return v0
.end method

.method public getStartSearchChars()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->startSearchChars:I

    return v0
.end method

.method protected hideKeyboard()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    return-void
.end method

.method protected abstract init()V
.end method

.method public isDisplayed()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed:Z

    return v0
.end method

.method isIdle()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isStartSearchOnCurrentPage()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->startSearchOnCurrentPage:Z

    return v0
.end method

.method synthetic lambda$performSearch$2$com-pspdfkit-ui-search-AbstractPdfSearchView(Ljava/lang/String;Lorg/reactivestreams/Subscription;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchResults:Ljava/util/List;

    if-nez p2, :cond_0

    .line 2
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchResults:Ljava/util/List;

    goto :goto_0

    .line 4
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 7
    :goto_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->onSearchStarted(Ljava/lang/String;)V

    .line 9
    iget-object p2, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchViewListener:Lcom/pspdfkit/ui/search/PdfSearchView$Listener;

    if-eqz p2, :cond_1

    .line 10
    invoke-interface {p2, p1}, Lcom/pspdfkit/ui/search/PdfSearchView$Listener;->onSearchStarted(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->clearSearch()V

    return-void
.end method

.method protected abstract onMoreSearchResults(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    .line 1
    iput p2, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->currentPage:I

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;

    if-nez v0, :cond_0

    .line 2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 6
    :cond_0
    check-cast p1, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;

    .line 7
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 8
    invoke-static {p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;->-$$Nest$fgetisDisplayingSearchResults(Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    .line 9
    iput-boolean p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->startSearchEvenWhenNotDisplayed:Z

    :cond_1
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 1
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;

    invoke-direct {v1, v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchResults:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;->-$$Nest$fputisDisplayingSearchResults(Lcom/pspdfkit/ui/search/AbstractPdfSearchView$SavedState;Z)V

    return-object v1
.end method

.method protected abstract onSearchCanceled()V
.end method

.method protected abstract onSearchComplete()V
.end method

.method protected abstract onSearchError(Ljava/lang/Throwable;)V
.end method

.method protected abstract onSearchStarted(Ljava/lang/String;)V
.end method

.method public final performSearch(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 49
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->performSearch(Ljava/lang/String;Ljava/util/EnumSet;)V

    return-void
.end method

.method public final performSearch(Ljava/lang/String;Ljava/util/EnumSet;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/document/search/CompareOptions;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->clearSearch()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->document:Lcom/pspdfkit/document/PdfDocument;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "PSPDFKit.SearchView"

    const-string v0, "setDocumentFromUri() has to be called before search can be performed."

    .line 4
    invoke-static {p2, v0, p1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 8
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 9
    iget-boolean v1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->startSearchOnCurrentPage:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->currentPage:I

    const/4 v2, -0x1

    if-le v1, v2, :cond_1

    .line 11
    new-instance v1, Lcom/pspdfkit/datastructures/Range;

    iget v2, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->currentPage:I

    iget-object v3, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->document:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {v3}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v3

    iget v4, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->currentPage:I

    sub-int/2addr v3, v4

    invoke-direct {v1, v2, v3}, Lcom/pspdfkit/datastructures/Range;-><init>(II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 14
    :cond_1
    new-instance v1, Lcom/pspdfkit/document/search/SearchOptions$Builder;

    invoke-direct {v1}, Lcom/pspdfkit/document/search/SearchOptions$Builder;-><init>()V

    .line 15
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->getSnippetLength()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/document/search/SearchOptions$Builder;->snippetLength(I)Lcom/pspdfkit/document/search/SearchOptions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/document/search/SearchOptions$Builder;->priorityPages(Ljava/util/List;)Lcom/pspdfkit/document/search/SearchOptions$Builder;

    move-result-object v0

    if-eqz p2, :cond_2

    .line 18
    invoke-virtual {v0, p2}, Lcom/pspdfkit/document/search/SearchOptions$Builder;->compareOptions(Ljava/util/EnumSet;)Lcom/pspdfkit/document/search/SearchOptions$Builder;

    .line 21
    :cond_2
    iget-object p2, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->maxSearchResults:Ljava/lang/Integer;

    if-eqz p2, :cond_3

    .line 23
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {v0, p2}, Lcom/pspdfkit/document/search/SearchOptions$Builder;->maxSearchResults(I)Lcom/pspdfkit/document/search/SearchOptions$Builder;

    .line 26
    :cond_3
    invoke-virtual {v0}, Lcom/pspdfkit/document/search/SearchOptions$Builder;->build()Lcom/pspdfkit/document/search/SearchOptions;

    move-result-object p2

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->textSearch:Lcom/pspdfkit/document/search/TextSearch;

    .line 29
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/document/search/TextSearch;->performSearchAsync(Ljava/lang/String;Lcom/pspdfkit/document/search/SearchOptions;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x12c

    .line 30
    invoke-virtual {p2, v1, v2, v0}, Lio/reactivex/rxjava3/core/Flowable;->buffer(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p2

    .line 31
    invoke-virtual {p2, v1, v2, v0}, Lio/reactivex/rxjava3/core/Flowable;->delaySubscription(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p2

    .line 32
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Flowable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Flowable;->doOnSubscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$2;-><init>(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Flowable;->subscribeWith(Lorg/reactivestreams/Subscriber;)Lorg/reactivestreams/Subscriber;

    move-result-object p1

    check-cast p1, Lio/reactivex/rxjava3/disposables/Disposable;

    iput-object p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchInProgress:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->listeners:Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    return-void
.end method

.method public setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 3

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    iput-object p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->document:Lcom/pspdfkit/document/PdfDocument;

    .line 108
    new-instance v0, Lcom/pspdfkit/document/search/TextSearch;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/document/search/TextSearch;-><init>(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->textSearch:Lcom/pspdfkit/document/search/TextSearch;

    .line 111
    iget-boolean p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed:Z

    if-nez p1, :cond_0

    iget-boolean p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->startSearchEvenWhenNotDisplayed:Z

    if-eqz p1, :cond_1

    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 112
    iget-object p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->performSearch(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public setInputFieldText(Ljava/lang/String;Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    if-eqz p2, :cond_0

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->clearSearch()V

    .line 8
    new-instance p2, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$$ExternalSyntheticLambda3;

    invoke-direct {p2, p0}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/ui/search/AbstractPdfSearchView;)V

    invoke-virtual {p0, p2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 10
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->performSearch(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setMaxSearchResults(Ljava/lang/Integer;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->maxSearchResults:Ljava/lang/Integer;

    return-void
.end method

.method public setSearchConfiguration(Lcom/pspdfkit/configuration/search/SearchConfiguration;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/search/SearchConfiguration;->getStartSearchChars()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->startSearchChars:I

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/search/SearchConfiguration;->getSnippetLength()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->snippetLength:I

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/search/SearchConfiguration;->isStartSearchOnCurrentPage()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->startSearchOnCurrentPage:Z

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/search/SearchConfiguration;->getMaxSearchResults()Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->maxSearchResults:Ljava/lang/Integer;

    return-void
.end method

.method public final setSearchViewListener(Lcom/pspdfkit/ui/search/PdfSearchView$Listener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchViewListener:Lcom/pspdfkit/ui/search/PdfSearchView$Listener;

    return-void
.end method

.method public setSnippetLength(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->snippetLength:I

    return-void
.end method

.method public setStartSearchChars(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->startSearchChars:I

    return-void
.end method

.method public setStartSearchOnCurrentPage(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->startSearchOnCurrentPage:Z

    return-void
.end method

.method public show()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed:Z

    if-eqz v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->textSearch:Lcom/pspdfkit/document/search/TextSearch;

    if-eqz v0, :cond_1

    const-string v1, "#-CACHE-#"

    .line 7
    invoke-virtual {v0, v1}, Lcom/pspdfkit/document/search/TextSearch;->performSearchAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    .line 8
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Flowable;->ignoreElements()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$$ExternalSyntheticLambda0;-><init>()V

    new-instance v2, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$$ExternalSyntheticLambda1;

    invoke-direct {v2}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView$$ExternalSyntheticLambda1;-><init>()V

    .line 9
    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->searchInProgress:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_1
    return-void
.end method

.method protected showKeyboard()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->inputField:Landroid/widget/EditText;

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 3
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;I)V

    return-void
.end method

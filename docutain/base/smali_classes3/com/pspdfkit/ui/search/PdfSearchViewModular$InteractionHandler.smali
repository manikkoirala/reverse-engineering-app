.class Lcom/pspdfkit/ui/search/PdfSearchViewModular$InteractionHandler;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/search/PdfSearchViewModular;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InteractionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/ui/search/PdfSearchViewModular;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$InteractionHandler;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/ui/search/PdfSearchViewModular;Lcom/pspdfkit/ui/search/PdfSearchViewModular$InteractionHandler-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/search/PdfSearchViewModular$InteractionHandler;-><init>(Lcom/pspdfkit/ui/search/PdfSearchViewModular;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$InteractionHandler;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    iget-object p1, p1, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->resultList:Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$InteractionHandler;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    iget-object p1, p1, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->resultList:Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object p1

    invoke-interface {p1, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/document/search/SearchResult;

    .line 4
    iget-object p2, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$InteractionHandler;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->dispatchSearchResultSelected(Lcom/pspdfkit/document/search/SearchResult;)V

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p2

    const-string p4, "select_search_result"

    .line 7
    invoke-virtual {p2, p4}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p2

    iget p1, p1, Lcom/pspdfkit/document/search/SearchResult;->pageIndex:I

    const-string p4, "page_index"

    .line 8
    invoke-virtual {p2, p1, p4}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 9
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    const-string p3, "sort"

    invoke-virtual {p1, p3, p2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$InteractionHandler;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->hide()V

    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    if-eqz p2, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$InteractionHandler;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->hideKeyboard()V

    :cond_0
    return-void
.end method

.class Lcom/pspdfkit/ui/search/PdfSearchViewModular$4;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/ui/search/PdfSearchViewModular;->hide()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/search/PdfSearchViewModular;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$4;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$4;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->-$$Nest$fputanimationRunning(Lcom/pspdfkit/ui/search/PdfSearchViewModular;Z)V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$4;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->-$$Nest$fputanimationRunning(Lcom/pspdfkit/ui/search/PdfSearchViewModular;Z)V

    .line 2
    iget-boolean v0, p1, Lcom/pspdfkit/ui/search/AbstractPdfSearchView;->isDisplayed:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x4

    .line 3
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 5
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    const-string v0, "exit_search"

    .line 6
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/search/PdfSearchViewModular$4;->this$0:Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->-$$Nest$fputanimationRunning(Lcom/pspdfkit/ui/search/PdfSearchViewModular;Z)V

    return-void
.end method

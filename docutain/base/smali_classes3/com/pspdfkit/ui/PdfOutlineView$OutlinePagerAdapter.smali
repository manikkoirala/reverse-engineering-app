.class public final Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;
.super Lcom/pspdfkit/ui/ViewStatePagerAdapter;
.source "SourceFile"

# interfaces
.implements Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/PdfOutlineView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "OutlinePagerAdapter"
.end annotation


# static fields
.field private static final MAX_NUMBER_OF_ITEMS:I = 0x4


# instance fields
.field private final annotationListView:Lcom/pspdfkit/internal/h1;

.field private final bookmarkListView:Lcom/pspdfkit/internal/t4;

.field private document:Lcom/pspdfkit/document/PdfDocument;

.field private final documentInfoListView:Lcom/pspdfkit/internal/y8;

.field private final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ql<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final outlineListView:Lcom/pspdfkit/internal/pl;

.field final synthetic this$0:Lcom/pspdfkit/ui/PdfOutlineView;

.field private final visibleItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ql<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetannotationListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/h1;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->annotationListView:Lcom/pspdfkit/internal/h1;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetbookmarkListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/t4;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->bookmarkListView:Lcom/pspdfkit/internal/t4;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetdocumentInfoListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/y8;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->documentInfoListView:Lcom/pspdfkit/internal/y8;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetoutlineListView(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)Lcom/pspdfkit/internal/pl;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->outlineListView:Lcom/pspdfkit/internal/pl;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mapplyTheme(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;Lcom/pspdfkit/internal/rl;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->applyTheme(Lcom/pspdfkit/internal/rl;)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/ui/PdfOutlineView;Lcom/pspdfkit/internal/fl;)V
    .locals 5

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    const/4 v0, 0x4

    .line 2
    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/ViewStatePagerAdapter;-><init>(I)V

    .line 3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->items:Ljava/util/List;

    .line 6
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    .line 14
    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView;->-$$Nest$fgetpager(Lcom/pspdfkit/ui/PdfOutlineView;)Landroidx/viewpager/widget/ViewPager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 16
    new-instance v0, Lcom/pspdfkit/internal/pl;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V

    invoke-direct {v0, v2, v3}, Lcom/pspdfkit/internal/pl;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ql$b;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->outlineListView:Lcom/pspdfkit/internal/pl;

    .line 20
    new-instance v2, Lcom/pspdfkit/internal/h1;

    .line 21
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter$$ExternalSyntheticLambda1;

    invoke-direct {v4, p0}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;)V

    invoke-direct {v2, v3, v4, p2}, Lcom/pspdfkit/internal/h1;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ql$b;Lcom/pspdfkit/internal/fl;)V

    iput-object v2, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->annotationListView:Lcom/pspdfkit/internal/h1;

    .line 30
    new-instance p2, Lcom/pspdfkit/internal/t4;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p2, v3}, Lcom/pspdfkit/internal/t4;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->bookmarkListView:Lcom/pspdfkit/internal/t4;

    .line 31
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/pspdfkit/ui/PdfOutlineView;->-$$Nest$mcreateDocumentInfoListView(Lcom/pspdfkit/ui/PdfOutlineView;Landroid/content/Context;)Lcom/pspdfkit/internal/y8;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->documentInfoListView:Lcom/pspdfkit/internal/y8;

    .line 33
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz p1, :cond_0

    .line 36
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->refreshItemsVisibility()V

    return-void
.end method

.method private applyTheme(Lcom/pspdfkit/internal/rl;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ql;

    .line 2
    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/ql;->a(Lcom/pspdfkit/internal/rl;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private notifyDataSetChangedRetainingCurrentItem()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfOutlineView;->-$$Nest$fgetpager(Lcom/pspdfkit/ui/PdfOutlineView;)Landroidx/viewpager/widget/ViewPager;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    goto :goto_2

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfOutlineView;->-$$Nest$fgetpager(Lcom/pspdfkit/ui/PdfOutlineView;)Landroidx/viewpager/widget/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->getItemTabButtonId(I)I

    move-result v0

    .line 7
    invoke-virtual {p0}, Landroidx/viewpager/widget/PagerAdapter;->notifyDataSetChanged()V

    const/4 v1, 0x0

    .line 8
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/ql;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/ql;->getTabButtonId()I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfOutlineView;->-$$Nest$fgetpager(Lcom/pspdfkit/ui/PdfOutlineView;)Landroidx/viewpager/widget/ViewPager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfOutlineView;->-$$Nest$fgetpager(Lcom/pspdfkit/ui/PdfOutlineView;)Landroidx/viewpager/widget/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    if-ne v1, v0, :cond_2

    .line 13
    invoke-virtual {p0, v1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->onPageSelected(I)V

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-void

    .line 14
    :cond_3
    :goto_2
    invoke-virtual {p0}, Landroidx/viewpager/widget/PagerAdapter;->notifyDataSetChanged()V

    return-void
.end method


# virtual methods
.method protected createView(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/ql;

    .line 2
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-object p2
.end method

.method protected destroyView(Landroid/view/ViewGroup;ILandroid/view/View;)V
    .locals 0

    .line 1
    instance-of p2, p1, Lcom/pspdfkit/internal/ql;

    if-eqz p2, :cond_0

    .line 2
    invoke-virtual {p1, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/internal/ql;

    const/4 v1, -0x2

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    :cond_0
    return v1
.end method

.method public getItemTabButtonId(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/ql;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ql;->getTabButtonId()I

    move-result p1

    return p1
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/ql;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ql;->getTitle()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getPositionOfItemWithTabButtonId(I)I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ql;

    .line 2
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ql;->getTabButtonId()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    return p1

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public isDocumentInfoListViewAvailable()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->documentInfoListView:Lcom/pspdfkit/internal/y8;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOutlineListViewAvailable()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->outlineListView:Lcom/pspdfkit/internal/pl;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pl;->getDocumentOutlineProvider()Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->document:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v0, :cond_1

    .line 9
    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->hasOutline()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method synthetic lambda$new$0$com-pspdfkit-ui-PdfOutlineView$OutlinePagerAdapter(Lcom/pspdfkit/internal/ql;Lcom/pspdfkit/document/OutlineElement;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView;->-$$Nest$fgetonOutlineElementTapListener(Lcom/pspdfkit/ui/PdfOutlineView;)Lcom/pspdfkit/ui/PdfOutlineView$OnOutlineElementTapListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/ui/PdfOutlineView$OnOutlineElementTapListener;->onOutlineElementTap(Lcom/pspdfkit/ui/PdfOutlineView;Lcom/pspdfkit/document/OutlineElement;)V

    :cond_0
    return-void
.end method

.method synthetic lambda$new$1$com-pspdfkit-ui-PdfOutlineView$OutlinePagerAdapter(Lcom/pspdfkit/internal/ql;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-static {p1}, Lcom/pspdfkit/ui/PdfOutlineView;->-$$Nest$fgetonAnnotationTapListener(Lcom/pspdfkit/ui/PdfOutlineView;)Lcom/pspdfkit/ui/PdfOutlineView$OnAnnotationTapListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/ui/PdfOutlineView$OnAnnotationTapListener;->onAnnotationTap(Lcom/pspdfkit/ui/PdfOutlineView;Lcom/pspdfkit/annotations/Annotation;)V

    :cond_0
    return-void
.end method

.method public onHide()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ql;

    .line 2
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ql;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    return-void
.end method

.method public onPageSelected(I)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/ql;

    if-ne p1, v1, :cond_0

    const/4 v3, 0x1

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/ql;->setPageSelected(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onShow()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ql;

    .line 2
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ql;->b()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public refreshItemsVisibility()V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfOutlineView;->shouldDisplayOutlineView()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->outlineListView:Lcom/pspdfkit/internal/pl;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 8
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfOutlineView;->shouldDisplayBookmarkListView()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->bookmarkListView:Lcom/pspdfkit/internal/t4;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 12
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfOutlineView;->shouldDisplayAnnotationListView()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->annotationListView:Lcom/pspdfkit/internal/h1;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 16
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfOutlineView;->shouldDisplayDocumentInfoListView()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 17
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->documentInfoListView:Lcom/pspdfkit/internal/y8;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 22
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 23
    iget-object v1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->visibleItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 24
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->notifyDataSetChangedRetainingCurrentItem()V

    .line 36
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->this$0:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-static {v0}, Lcom/pspdfkit/ui/PdfOutlineView;->-$$Nest$fgetisDisplayed(Lcom/pspdfkit/ui/PdfOutlineView;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 37
    invoke-static {v0}, Lcom/pspdfkit/ui/PdfOutlineView;->-$$Nest$fgetpagerTabs(Lcom/pspdfkit/ui/PdfOutlineView;)Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->a()V

    :cond_5
    return-void
.end method

.method public setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/ql$a;)V
    .locals 3

    const-string v0, "onHideListener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p3, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->document:Lcom/pspdfkit/document/PdfDocument;

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ql;

    .line 56
    move-object v2, p1

    check-cast v2, Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1, v2, p2}, Lcom/pspdfkit/internal/ql;->a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    .line 57
    invoke-virtual {v1, p3}, Lcom/pspdfkit/internal/ql;->setOnHideListener(Lcom/pspdfkit/internal/ql$a;)V

    goto :goto_0

    :cond_0
    return-void
.end method

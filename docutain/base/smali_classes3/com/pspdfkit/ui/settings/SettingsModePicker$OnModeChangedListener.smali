.class public interface abstract Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/settings/SettingsModePicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnModeChangedListener"
.end annotation


# virtual methods
.method public abstract OnPageLayoutChange(Lcom/pspdfkit/configuration/page/PageLayoutMode;)V
.end method

.method public abstract OnScreenTimeoutChange(J)V
.end method

.method public abstract OnScrollDirectionChange(Lcom/pspdfkit/configuration/page/PageScrollDirection;)V
.end method

.method public abstract OnScrollModeChange(Lcom/pspdfkit/configuration/page/PageScrollMode;)V
.end method

.method public abstract OnThemeChange(Lcom/pspdfkit/configuration/theming/ThemeMode;)V
.end method

.class public Lcom/pspdfkit/ui/settings/SettingsModePicker;
.super Landroidx/cardview/widget/CardView;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "PSPDFKit.SettingsModePicker"


# instance fields
.field private layoutAutoButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

.field private layoutDoubleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

.field private layoutMode:Lcom/pspdfkit/configuration/page/PageLayoutMode;

.field private layoutSingleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

.field private onModeChangedListener:Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;

.field private screenAwakeContainer:Landroid/widget/LinearLayout;

.field private screenAwakeSeparator:Landroid/view/View;

.field private screenAwakeSwitch:Lcom/pspdfkit/ui/LocalizedSwitch;

.field private screenTimeoutMillis:J

.field private scrollHorizontalButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

.field private scrollMode:Lcom/pspdfkit/configuration/page/PageScrollDirection;

.field private scrollVerticalButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

.field private themeDefaultButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

.field private themeMode:Lcom/pspdfkit/configuration/theming/ThemeMode;

.field private themeNightButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

.field private transitionContinuousButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

.field private transitionJumpButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

.field private transitionMode:Lcom/pspdfkit/configuration/page/PageScrollMode;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->getThemedContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1}, Landroidx/cardview/widget/CardView;-><init>(Landroid/content/Context;)V

    const-wide/16 v0, 0x0

    .line 2
    iput-wide v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenTimeoutMillis:J

    .line 13
    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 14
    invoke-static {p1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->getThemedContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Landroidx/cardview/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-wide/16 p1, 0x0

    .line 15
    iput-wide p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenTimeoutMillis:J

    .line 31
    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 32
    invoke-static {p1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->getThemedContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1, p2, p3}, Landroidx/cardview/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-wide/16 p1, 0x0

    .line 33
    iput-wide p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenTimeoutMillis:J

    .line 55
    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->init()V

    return-void
.end method

.method private generateThemeListDrawable(Lcom/pspdfkit/configuration/theming/ThemeMode;)Landroid/graphics/drawable/StateListDrawable;
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$color;->pspdf__settings_mode_picker_item_activated:I

    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$color;->pspdf__settings_mode_picker_item_default:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 6
    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 7
    sget-object v3, Lcom/pspdfkit/ui/settings/SettingsModePicker$1;->$SwitchMap$com$pspdfkit$configuration$theming$ThemeMode:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget v3, v3, v4

    const v4, 0x10102fe

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eq v3, v5, :cond_1

    const/4 v7, 0x2

    if-eq v3, v7, :cond_0

    new-array v0, v5, [Ljava/lang/Object;

    aput-object p1, v0, v6

    const-string p1, "PSPDFKit.SettingsModePicker"

    const-string v1, "Theme %s mode not handled"

    .line 29
    invoke-static {p1, v1, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-array p1, v5, [I

    aput v4, p1, v6

    .line 32
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, -0x1000000

    invoke-static {v3, v0, v4}, Lcom/pspdfkit/internal/o5;->a(Landroid/content/Context;II)Lcom/pspdfkit/internal/o5;

    move-result-object v0

    .line 33
    invoke-virtual {v2, p1, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-array p1, v6, [I

    .line 40
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1, v4}, Lcom/pspdfkit/internal/o5;->a(Landroid/content/Context;II)Lcom/pspdfkit/internal/o5;

    move-result-object v0

    .line 41
    invoke-virtual {v2, p1, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    new-array p1, v5, [I

    aput v4, p1, v6

    .line 44
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, -0x1

    invoke-static {v3, v0, v4}, Lcom/pspdfkit/internal/o5;->a(Landroid/content/Context;II)Lcom/pspdfkit/internal/o5;

    move-result-object v0

    .line 45
    invoke-virtual {v2, p1, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-array p1, v6, [I

    .line 52
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1, v4}, Lcom/pspdfkit/internal/o5;->a(Landroid/content/Context;II)Lcom/pspdfkit/internal/o5;

    move-result-object v0

    .line 53
    invoke-virtual {v2, p1, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :goto_0
    return-object v2
.end method

.method private getOnScreenTimeoutChangeListener()Landroid/widget/CompoundButton$OnCheckedChangeListener;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/settings/SettingsModePicker$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/settings/SettingsModePicker;)V

    return-object v0
.end method

.method private static getThemedContext(Landroid/content/Context;)Landroid/content/Context;
    .locals 3

    .line 1
    new-instance v0, Landroid/view/ContextThemeWrapper;

    sget v1, Lcom/pspdfkit/R$attr;->pspdf__settingsDialogStyle:I

    sget v2, Lcom/pspdfkit/R$style;->PSPDFKit_SettingsDialog:I

    .line 3
    invoke-static {p0, v1, v2}, Lcom/pspdfkit/internal/cu;->b(Landroid/content/Context;II)I

    move-result v1

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method private init()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__view_settings_mode_picker:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 3
    sget v0, Lcom/pspdfkit/R$id;->pspdf__transition_jump_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionJumpButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    .line 4
    sget v0, Lcom/pspdfkit/R$id;->pspdf__transition_continuous_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionContinuousButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    .line 5
    sget v0, Lcom/pspdfkit/R$id;->pspdf__layout_single_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutSingleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    .line 6
    sget v0, Lcom/pspdfkit/R$id;->pspdf__layout_double_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutDoubleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    .line 7
    sget v0, Lcom/pspdfkit/R$id;->pspdf__layout_auto_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutAutoButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    .line 8
    sget v0, Lcom/pspdfkit/R$id;->pspdf__scroll_horizontal_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->scrollHorizontalButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    .line 9
    sget v0, Lcom/pspdfkit/R$id;->pspdf__scroll_vertical_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->scrollVerticalButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    .line 10
    sget v0, Lcom/pspdfkit/R$id;->pspdf__theme_default_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeDefaultButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    .line 11
    sget v0, Lcom/pspdfkit/R$id;->pspdf__theme_night_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeNightButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    .line 12
    sget v0, Lcom/pspdfkit/R$id;->pspdf__screen_awake_separator:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSeparator:Landroid/view/View;

    .line 13
    sget v0, Lcom/pspdfkit/R$id;->pspdf__screen_awake_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeContainer:Landroid/widget/LinearLayout;

    .line 14
    sget v0, Lcom/pspdfkit/R$id;->pspdf__screen_awake_switch:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/LocalizedSwitch;

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSwitch:Lcom/pspdfkit/ui/LocalizedSwitch;

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionJumpButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    sget-object v1, Lcom/pspdfkit/configuration/page/PageScrollMode;->PER_PAGE:Lcom/pspdfkit/configuration/page/PageScrollMode;

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setTransitionModeAndReturnListener(Lcom/pspdfkit/configuration/page/PageScrollMode;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionContinuousButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    sget-object v1, Lcom/pspdfkit/configuration/page/PageScrollMode;->CONTINUOUS:Lcom/pspdfkit/configuration/page/PageScrollMode;

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setTransitionModeAndReturnListener(Lcom/pspdfkit/configuration/page/PageScrollMode;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutSingleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    sget-object v1, Lcom/pspdfkit/configuration/page/PageLayoutMode;->SINGLE:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setLayoutModeAndReturnListener(Lcom/pspdfkit/configuration/page/PageLayoutMode;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutDoubleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    sget-object v1, Lcom/pspdfkit/configuration/page/PageLayoutMode;->DOUBLE:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setLayoutModeAndReturnListener(Lcom/pspdfkit/configuration/page/PageLayoutMode;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutAutoButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    sget-object v1, Lcom/pspdfkit/configuration/page/PageLayoutMode;->AUTO:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setLayoutModeAndReturnListener(Lcom/pspdfkit/configuration/page/PageLayoutMode;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->scrollHorizontalButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    sget-object v1, Lcom/pspdfkit/configuration/page/PageScrollDirection;->HORIZONTAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setScrollModeAndReturnListener(Lcom/pspdfkit/configuration/page/PageScrollDirection;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->scrollVerticalButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    sget-object v1, Lcom/pspdfkit/configuration/page/PageScrollDirection;->VERTICAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setScrollModeAndReturnListener(Lcom/pspdfkit/configuration/page/PageScrollDirection;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeDefaultButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    sget-object v1, Lcom/pspdfkit/configuration/theming/ThemeMode;->DEFAULT:Lcom/pspdfkit/configuration/theming/ThemeMode;

    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setThemeModeAndReturnListener(Lcom/pspdfkit/configuration/theming/ThemeMode;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeNightButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    sget-object v2, Lcom/pspdfkit/configuration/theming/ThemeMode;->NIGHT:Lcom/pspdfkit/configuration/theming/ThemeMode;

    invoke-direct {p0, v2}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setThemeModeAndReturnListener(Lcom/pspdfkit/configuration/theming/ThemeMode;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeDefaultButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->getIcon()Landroid/widget/ImageView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 31
    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->generateThemeListDrawable(Lcom/pspdfkit/configuration/theming/ThemeMode;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    .line 32
    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeDefaultButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->getIcon()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeNightButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->getIcon()Landroid/widget/ImageView;

    move-result-object v0

    if-nez v0, :cond_1

    .line 35
    invoke-direct {p0, v2}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->generateThemeListDrawable(Lcom/pspdfkit/configuration/theming/ThemeMode;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeNightButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->getIcon()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSwitch:Lcom/pspdfkit/ui/LocalizedSwitch;

    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->getOnScreenTimeoutChangeListener()Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method private preventUiReflectingIncompatibleModes()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionMode:Lcom/pspdfkit/configuration/page/PageScrollMode;

    sget-object v1, Lcom/pspdfkit/configuration/page/PageScrollMode;->CONTINUOUS:Lcom/pspdfkit/configuration/page/PageScrollMode;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutSingleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutDoubleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutAutoButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutSingleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {v0, v3}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutDoubleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {v0, v3}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutAutoButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {v0, v3}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method private setLayoutModeAndReturnListener(Lcom/pspdfkit/configuration/page/PageLayoutMode;)Landroid/view/View$OnClickListener;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/settings/SettingsModePicker$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/ui/settings/SettingsModePicker$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/settings/SettingsModePicker;Lcom/pspdfkit/configuration/page/PageLayoutMode;)V

    return-object v0
.end method

.method private setPageLayoutModeAndNotifyListener(Lcom/pspdfkit/configuration/page/PageLayoutMode;Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutMode:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    if-eq v0, p1, :cond_0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutMode:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->updateLayoutButtons()V

    if-eqz p2, :cond_0

    .line 4
    iget-object p2, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->onModeChangedListener:Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;

    if-eqz p2, :cond_0

    .line 5
    invoke-interface {p2, p1}, Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;->OnPageLayoutChange(Lcom/pspdfkit/configuration/page/PageLayoutMode;)V

    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->preventUiReflectingIncompatibleModes()Z

    return-void
.end method

.method private setScrollModeAndNotifyListener(Lcom/pspdfkit/configuration/page/PageScrollDirection;Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->scrollMode:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    if-eq v0, p1, :cond_1

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->scrollMode:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    .line 3
    sget-object v0, Lcom/pspdfkit/configuration/page/PageScrollDirection;->HORIZONTAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    if-ne p1, v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionContinuousButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_settings_continuous_horizontal:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 6
    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionContinuousButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_settings_continuous_vertical:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 11
    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 14
    :goto_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->updateScrollButtons()V

    if-eqz p2, :cond_1

    .line 15
    iget-object p2, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->onModeChangedListener:Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;

    if-eqz p2, :cond_1

    .line 16
    invoke-interface {p2, p1}, Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;->OnScrollDirectionChange(Lcom/pspdfkit/configuration/page/PageScrollDirection;)V

    .line 19
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->preventUiReflectingIncompatibleModes()Z

    return-void
.end method

.method private setScrollModeAndReturnListener(Lcom/pspdfkit/configuration/page/PageScrollDirection;)Landroid/view/View$OnClickListener;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/settings/SettingsModePicker$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/ui/settings/SettingsModePicker$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/settings/SettingsModePicker;Lcom/pspdfkit/configuration/page/PageScrollDirection;)V

    return-object v0
.end method

.method private setThemeModeAndNotifyListener(Lcom/pspdfkit/configuration/theming/ThemeMode;Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeMode:Lcom/pspdfkit/configuration/theming/ThemeMode;

    if-eq v0, p1, :cond_0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeMode:Lcom/pspdfkit/configuration/theming/ThemeMode;

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->updateThemeButtons()V

    if-eqz p2, :cond_0

    .line 4
    iget-object p2, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->onModeChangedListener:Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;

    if-eqz p2, :cond_0

    .line 5
    invoke-interface {p2, p1}, Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;->OnThemeChange(Lcom/pspdfkit/configuration/theming/ThemeMode;)V

    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->preventUiReflectingIncompatibleModes()Z

    return-void
.end method

.method private setThemeModeAndReturnListener(Lcom/pspdfkit/configuration/theming/ThemeMode;)Landroid/view/View$OnClickListener;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/settings/SettingsModePicker$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/ui/settings/SettingsModePicker$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/ui/settings/SettingsModePicker;Lcom/pspdfkit/configuration/theming/ThemeMode;)V

    return-object v0
.end method

.method private setTransitionModeAndNotifyListener(Lcom/pspdfkit/configuration/page/PageScrollMode;Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionMode:Lcom/pspdfkit/configuration/page/PageScrollMode;

    if-eq v0, p1, :cond_0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionMode:Lcom/pspdfkit/configuration/page/PageScrollMode;

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->updateTransitionButtons()V

    if-eqz p2, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->onModeChangedListener:Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;

    if-eqz p1, :cond_0

    .line 5
    iget-object p2, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionMode:Lcom/pspdfkit/configuration/page/PageScrollMode;

    invoke-interface {p1, p2}, Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;->OnScrollModeChange(Lcom/pspdfkit/configuration/page/PageScrollMode;)V

    .line 10
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->preventUiReflectingIncompatibleModes()Z

    move-result p1

    if-nez p1, :cond_1

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutSingleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutDoubleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutAutoButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method private setTransitionModeAndReturnListener(Lcom/pspdfkit/configuration/page/PageScrollMode;)Landroid/view/View$OnClickListener;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/settings/SettingsModePicker$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/ui/settings/SettingsModePicker$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/ui/settings/SettingsModePicker;Lcom/pspdfkit/configuration/page/PageScrollMode;)V

    return-object v0
.end method

.method private updateLayoutButtons()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutSingleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutMode:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    sget-object v2, Lcom/pspdfkit/configuration/page/PageLayoutMode;->SINGLE:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutDoubleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutMode:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    sget-object v2, Lcom/pspdfkit/configuration/page/PageLayoutMode;->DOUBLE:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutAutoButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutMode:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    sget-object v2, Lcom/pspdfkit/configuration/page/PageLayoutMode;->AUTO:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    if-ne v1, v2, :cond_2

    const/4 v3, 0x1

    :cond_2
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutSingleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {v0, v4}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutDoubleButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {v0, v4}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->layoutAutoButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    invoke-virtual {v0, v4}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    return-void
.end method

.method private updateScrollButtons()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->scrollHorizontalButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->scrollMode:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    sget-object v2, Lcom/pspdfkit/configuration/page/PageScrollDirection;->HORIZONTAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->scrollVerticalButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->scrollMode:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    sget-object v2, Lcom/pspdfkit/configuration/page/PageScrollDirection;->VERTICAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    if-ne v1, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    return-void
.end method

.method private updateThemeButtons()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeDefaultButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeMode:Lcom/pspdfkit/configuration/theming/ThemeMode;

    sget-object v2, Lcom/pspdfkit/configuration/theming/ThemeMode;->DEFAULT:Lcom/pspdfkit/configuration/theming/ThemeMode;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeNightButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->themeMode:Lcom/pspdfkit/configuration/theming/ThemeMode;

    sget-object v2, Lcom/pspdfkit/configuration/theming/ThemeMode;->NIGHT:Lcom/pspdfkit/configuration/theming/ThemeMode;

    if-ne v1, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    return-void
.end method

.method private updateTransitionButtons()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionJumpButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionMode:Lcom/pspdfkit/configuration/page/PageScrollMode;

    sget-object v2, Lcom/pspdfkit/configuration/page/PageScrollMode;->PER_PAGE:Lcom/pspdfkit/configuration/page/PageScrollMode;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionContinuousButton:Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->transitionMode:Lcom/pspdfkit/configuration/page/PageScrollMode;

    sget-object v2, Lcom/pspdfkit/configuration/page/PageScrollMode;->CONTINUOUS:Lcom/pspdfkit/configuration/page/PageScrollMode;

    if-ne v1, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    return-void
.end method


# virtual methods
.method synthetic lambda$getOnScreenTimeoutChangeListener$4$com-pspdfkit-ui-settings-SettingsModePicker(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->onModeChangedListener:Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    .line 2
    :goto_0
    invoke-interface {p1, v0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;->OnScreenTimeoutChange(J)V

    :cond_1
    return-void
.end method

.method synthetic lambda$setLayoutModeAndReturnListener$2$com-pspdfkit-ui-settings-SettingsModePicker(Lcom/pspdfkit/configuration/page/PageLayoutMode;Landroid/view/View;)V
    .locals 0

    const/4 p2, 0x1

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setPageLayoutModeAndNotifyListener(Lcom/pspdfkit/configuration/page/PageLayoutMode;Z)V

    return-void
.end method

.method synthetic lambda$setScrollModeAndReturnListener$1$com-pspdfkit-ui-settings-SettingsModePicker(Lcom/pspdfkit/configuration/page/PageScrollDirection;Landroid/view/View;)V
    .locals 0

    const/4 p2, 0x1

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setScrollModeAndNotifyListener(Lcom/pspdfkit/configuration/page/PageScrollDirection;Z)V

    return-void
.end method

.method synthetic lambda$setThemeModeAndReturnListener$0$com-pspdfkit-ui-settings-SettingsModePicker(Lcom/pspdfkit/configuration/theming/ThemeMode;Landroid/view/View;)V
    .locals 0

    const/4 p2, 0x1

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setThemeModeAndNotifyListener(Lcom/pspdfkit/configuration/theming/ThemeMode;Z)V

    return-void
.end method

.method synthetic lambda$setTransitionModeAndReturnListener$3$com-pspdfkit-ui-settings-SettingsModePicker(Lcom/pspdfkit/configuration/page/PageScrollMode;Landroid/view/View;)V
    .locals 0

    const/4 p2, 0x1

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setTransitionModeAndNotifyListener(Lcom/pspdfkit/configuration/page/PageScrollMode;Z)V

    return-void
.end method

.method public setItemsVisibility(Ljava/util/EnumSet;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;->PAGE_TRANSITION:Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    if-eqz v1, :cond_0

    .line 2
    sget v1, Lcom/pspdfkit/R$id;->pspdf__transition_header:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3
    sget v1, Lcom/pspdfkit/R$id;->pspdf__transition_container:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4
    sget v1, Lcom/pspdfkit/R$id;->pspdf__layout_separator:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 6
    :cond_0
    sget v1, Lcom/pspdfkit/R$id;->pspdf__transition_header:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 7
    sget v1, Lcom/pspdfkit/R$id;->pspdf__transition_container:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 8
    sget v1, Lcom/pspdfkit/R$id;->pspdf__layout_separator:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 12
    :goto_0
    sget-object v1, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;->PAGE_LAYOUT:Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    invoke-virtual {p1, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 13
    sget v1, Lcom/pspdfkit/R$id;->pspdf__layout_header:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 14
    sget v1, Lcom/pspdfkit/R$id;->pspdf__layout_container:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 15
    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16
    sget v0, Lcom/pspdfkit/R$id;->pspdf__layout_separator:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 19
    :cond_1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__layout_header:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 20
    sget v0, Lcom/pspdfkit/R$id;->pspdf__layout_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 21
    sget v0, Lcom/pspdfkit/R$id;->pspdf__layout_separator:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 25
    :cond_2
    :goto_1
    sget-object v0, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;->SCROLL_DIRECTION:Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 26
    sget v0, Lcom/pspdfkit/R$id;->pspdf__scroll_header:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 27
    sget v0, Lcom/pspdfkit/R$id;->pspdf__scroll_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 28
    sget v0, Lcom/pspdfkit/R$id;->pspdf__scroll_separator:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 30
    :cond_3
    sget v0, Lcom/pspdfkit/R$id;->pspdf__scroll_header:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 31
    sget v0, Lcom/pspdfkit/R$id;->pspdf__scroll_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 32
    sget v0, Lcom/pspdfkit/R$id;->pspdf__scroll_separator:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 36
    :goto_2
    sget-object v0, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;->THEME:Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 37
    sget v0, Lcom/pspdfkit/R$id;->pspdf__theme_header:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 38
    sget v0, Lcom/pspdfkit/R$id;->pspdf__theme_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 39
    sget v0, Lcom/pspdfkit/R$id;->pspdf__theme_separator:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 41
    :cond_4
    sget v0, Lcom/pspdfkit/R$id;->pspdf__theme_header:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 42
    sget v0, Lcom/pspdfkit/R$id;->pspdf__theme_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 43
    sget v0, Lcom/pspdfkit/R$id;->pspdf__theme_separator:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 47
    :goto_3
    sget-object v0, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;->SCREEN_AWAKE:Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    iget-wide v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenTimeoutMillis:J

    const-wide/16 v4, 0x0

    cmp-long p1, v0, v4

    if-eqz p1, :cond_5

    const-wide v4, 0x7fffffffffffffffL

    cmp-long p1, v0, v4

    if-nez p1, :cond_6

    .line 50
    :cond_5
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 51
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSeparator:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 53
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 54
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSeparator:Landroid/view/View;

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    return-void
.end method

.method public setOnModeChangedListener(Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->onModeChangedListener:Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;

    return-void
.end method

.method public setPageLayoutMode(Lcom/pspdfkit/configuration/page/PageLayoutMode;)V
    .locals 2

    const-string v0, "layout"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setPageLayoutModeAndNotifyListener(Lcom/pspdfkit/configuration/page/PageLayoutMode;Z)V

    return-void
.end method

.method public setScreenTimeoutMode(J)V
    .locals 5

    .line 1
    iput-wide p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenTimeoutMillis:J

    const/4 v0, 0x0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    cmp-long v4, p1, v2

    if-nez v4, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSeparator:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSwitch:Lcom/pspdfkit/ui/LocalizedSwitch;

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSwitch:Lcom/pspdfkit/ui/LocalizedSwitch;

    invoke-virtual {p1, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSwitch:Lcom/pspdfkit/ui/LocalizedSwitch;

    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->getOnScreenTimeoutChangeListener()Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    :cond_0
    const-wide v2, 0x7fffffffffffffffL

    cmp-long v4, p1, v2

    if-nez v4, :cond_1

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSeparator:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSwitch:Lcom/pspdfkit/ui/LocalizedSwitch;

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSwitch:Lcom/pspdfkit/ui/LocalizedSwitch;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSwitch:Lcom/pspdfkit/ui/LocalizedSwitch;

    invoke-direct {p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->getOnScreenTimeoutChangeListener()Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .line 18
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSeparator:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/ui/settings/SettingsModePicker;->screenAwakeSwitch:Lcom/pspdfkit/ui/LocalizedSwitch;

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :goto_0
    return-void
.end method

.method public setScrollMode(Lcom/pspdfkit/configuration/page/PageScrollDirection;)V
    .locals 2

    const-string v0, "scroll"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setScrollModeAndNotifyListener(Lcom/pspdfkit/configuration/page/PageScrollDirection;Z)V

    return-void
.end method

.method public setThemeMode(Lcom/pspdfkit/configuration/theming/ThemeMode;)V
    .locals 2

    const-string v0, "theme"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setThemeModeAndNotifyListener(Lcom/pspdfkit/configuration/theming/ThemeMode;Z)V

    return-void
.end method

.method public setTransitionMode(Lcom/pspdfkit/configuration/page/PageScrollMode;)V
    .locals 2

    const-string v0, "transition"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setTransitionModeAndNotifyListener(Lcom/pspdfkit/configuration/page/PageScrollMode;Z)V

    return-void
.end method

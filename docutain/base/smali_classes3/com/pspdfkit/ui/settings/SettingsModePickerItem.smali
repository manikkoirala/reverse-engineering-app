.class public Lcom/pspdfkit/ui/settings/SettingsModePickerItem;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private colorStateList:Landroid/content/res/ColorStateList;

.field private icon:Landroid/widget/ImageView;

.field private label:Lcom/pspdfkit/ui/LocalizedTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__settingsModePickerItemStyle:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__SettingsModePickerItem:[I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->init(Landroid/content/res/TypedArray;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .line 3
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__settingsModePickerItemStyle:I

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__SettingsModePickerItem:[I

    sget v1, Lcom/pspdfkit/R$attr;->pspdf__settingsModePickerItemStyle:I

    const/4 v2, 0x0

    .line 5
    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 6
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->init(Landroid/content/res/TypedArray;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 7
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 8
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__SettingsModePickerItem:[I

    const/4 v1, 0x0

    .line 9
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 10
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->init(Landroid/content/res/TypedArray;)V

    return-void
.end method

.method private init(Landroid/content/res/TypedArray;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__view_settings_mode_picker_item:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__icon:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->icon:Landroid/widget/ImageView;

    .line 3
    sget v0, Lcom/pspdfkit/R$id;->pspdf__label:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/LocalizedTextView;

    iput-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->label:Lcom/pspdfkit/ui/LocalizedTextView;

    .line 6
    invoke-virtual {p0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 8
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__SettingsModePickerItem_pspdf__ignoreTint:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 10
    sget v3, Lcom/pspdfkit/R$styleable;->pspdf__SettingsModePickerItem_pspdf__activated:I

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 12
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SettingsModePickerItem_pspdf__itemTint:I

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 14
    iput-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->colorStateList:Landroid/content/res/ColorStateList;

    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v3, Lcom/pspdfkit/R$color;->pspdf__settings_mode_picker_item:I

    invoke-static {v1, v3}, Landroidx/core/content/ContextCompat;->getColorStateList(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->colorStateList:Landroid/content/res/ColorStateList;

    .line 19
    :goto_0
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SettingsModePickerItem_pspdf__buttonIcon:I

    const/4 v3, -0x1

    invoke-virtual {p1, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    if-eq v1, v3, :cond_2

    .line 21
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_1

    .line 24
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 25
    invoke-static {v1}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->colorStateList:Landroid/content/res/ColorStateList;

    invoke-static {v0, v1}, Landroidx/core/graphics/drawable/DrawableCompat;->setTintList(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 27
    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->icon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 29
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 33
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->label:Lcom/pspdfkit/ui/LocalizedTextView;

    iget-object v1, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->colorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 34
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__SettingsModePickerItem_pspdf__label:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 35
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SettingsModePickerItem_pspdf__showLabel:I

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v0, :cond_3

    .line 36
    iget-object v2, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->label:Lcom/pspdfkit/ui/LocalizedTextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    if-nez v1, :cond_4

    .line 37
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->label:Lcom/pspdfkit/ui/LocalizedTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 38
    :cond_4
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public getIcon()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->icon:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getLabelText()Ljava/lang/CharSequence;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->label:Lcom/pspdfkit/ui/LocalizedTextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->label:Lcom/pspdfkit/ui/LocalizedTextView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 3
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 2
    invoke-static {p1}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->colorStateList:Landroid/content/res/ColorStateList;

    invoke-static {p1, v0}, Landroidx/core/graphics/drawable/DrawableCompat;->setTintList(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

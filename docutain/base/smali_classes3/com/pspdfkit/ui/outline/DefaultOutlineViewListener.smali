.class public Lcom/pspdfkit/ui/outline/DefaultOutlineViewListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/PdfOutlineView$OnOutlineElementTapListener;
.implements Lcom/pspdfkit/ui/PdfOutlineView$OnAnnotationTapListener;


# instance fields
.field private final fragment:Lcom/pspdfkit/ui/PdfFragment;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "fragment"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultOutlineViewListener;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    return-void
.end method


# virtual methods
.method public onAnnotationTap(Lcom/pspdfkit/ui/PdfOutlineView;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 1
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result p1

    if-gez p1, :cond_0

    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/outline/DefaultOutlineViewListener;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->beginNavigation()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/outline/DefaultOutlineViewListener;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(IZ)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultOutlineViewListener;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/ui/PdfFragment;->setSelectedAnnotation(Lcom/pspdfkit/annotations/Annotation;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultOutlineViewListener;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->endNavigation()V

    return-void
.end method

.method public onOutlineElementTap(Lcom/pspdfkit/ui/PdfOutlineView;Lcom/pspdfkit/document/OutlineElement;)V
    .locals 0

    .line 1
    invoke-virtual {p2}, Lcom/pspdfkit/document/OutlineElement;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/ui/outline/DefaultOutlineViewListener;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/ui/PdfFragment;->executeAction(Lcom/pspdfkit/annotations/actions/Action;)V

    :cond_0
    return-void
.end method

.class public Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;
.implements Lcom/pspdfkit/listeners/DocumentListener;
.implements Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;


# instance fields
.field private bookmarkViewListener:Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;

.field private document:Lcom/pspdfkit/document/PdfDocument;

.field private final fragment:Lcom/pspdfkit/ui/PdfFragment;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->document:Lcom/pspdfkit/document/PdfDocument;

    .line 5
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->document:Lcom/pspdfkit/document/PdfDocument;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getBookmarkProvider()Lcom/pspdfkit/bookmarks/BookmarkProvider;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/bookmarks/BookmarkProvider;->addBookmarkListener(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public addBookmarkListener(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->bookmarkViewListener:Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;

    .line 55
    iget-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->document:Lcom/pspdfkit/document/PdfDocument;

    if-eqz p1, :cond_0

    .line 56
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getBookmarkProvider()Lcom/pspdfkit/bookmarks/BookmarkProvider;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/bookmarks/BookmarkProvider;->addBookmarkListener(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;)V

    .line 58
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    return-void
.end method

.method public getBookmarks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/bookmarks/Bookmark;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->document:Lcom/pspdfkit/document/PdfDocument;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 2
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getBookmarkProvider()Lcom/pspdfkit/bookmarks/BookmarkProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/bookmarks/BookmarkProvider;->getBookmarks()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isBookmarkAddButtonEnabled()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v1

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    if-gez v1, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->allowMultipleBookmarksPerPage()Z

    move-result v0

    if-nez v0, :cond_2

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->getBookmarks()Ljava/util/List;

    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/bookmarks/Bookmark;

    .line 8
    invoke-virtual {v3}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v1, :cond_1

    return v2

    :cond_2
    const/4 v0, 0x1

    return v0

    :cond_3
    :goto_0
    return v2
.end method

.method synthetic lambda$onBookmarkAdd$0$com-pspdfkit-ui-outline-DefaultBookmarkAdapter(Lcom/pspdfkit/bookmarks/Bookmark;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "add_bookmark"

    .line 2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/bookmarks/Bookmark;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 5
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->onBookmarkAdded(Lcom/pspdfkit/bookmarks/Bookmark;)V

    return-void
.end method

.method public onBookmarkAdd()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->document:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v1, :cond_1

    if-gez v0, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    new-instance v1, Lcom/pspdfkit/bookmarks/Bookmark;

    invoke-direct {v1, v0}, Lcom/pspdfkit/bookmarks/Bookmark;-><init>(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->document:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getBookmarkProvider()Lcom/pspdfkit/bookmarks/BookmarkProvider;

    move-result-object v0

    .line 5
    invoke-interface {v0, v1}, Lcom/pspdfkit/bookmarks/BookmarkProvider;->addBookmarkAsync(Lcom/pspdfkit/bookmarks/Bookmark;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 6
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v2, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v1}, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;Lcom/pspdfkit/bookmarks/Bookmark;)V

    .line 7
    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_1
    :goto_0
    return-void
.end method

.method public onBookmarkAdded(Lcom/pspdfkit/bookmarks/Bookmark;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->bookmarkViewListener:Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;->onBookmarkAdded(Lcom/pspdfkit/bookmarks/Bookmark;)V

    :cond_0
    return-void
.end method

.method public onBookmarkClicked(Lcom/pspdfkit/bookmarks/Bookmark;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    const-string v2, "tap_bookmark_in_bookmark_list"

    .line 4
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    .line 5
    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/bookmarks/Bookmark;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->beginNavigation()V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(IZ)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->endNavigation()V

    return-void
.end method

.method public onBookmarkNameSet(Lcom/pspdfkit/bookmarks/Bookmark;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-virtual {p1, p2}, Lcom/pspdfkit/bookmarks/Bookmark;->setName(Ljava/lang/String;)V

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p2

    const-string v0, "rename_bookmark"

    .line 3
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p2

    .line 4
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/bookmarks/Bookmark;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method public onBookmarkPositionSet(Lcom/pspdfkit/bookmarks/Bookmark;I)V
    .locals 1

    .line 1
    invoke-virtual {p1, p2}, Lcom/pspdfkit/bookmarks/Bookmark;->setSortKey(I)V

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p2

    const-string v0, "sort_bookmark"

    .line 3
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p2

    .line 4
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/bookmarks/Bookmark;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method public onBookmarkRemove(Lcom/pspdfkit/bookmarks/Bookmark;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->document:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getBookmarkProvider()Lcom/pspdfkit/bookmarks/BookmarkProvider;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/bookmarks/BookmarkProvider;->removeBookmark(Lcom/pspdfkit/bookmarks/Bookmark;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    const-string v2, "remove_bookmark"

    .line 5
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    .line 6
    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/bookmarks/Bookmark;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    :cond_1
    return v0
.end method

.method public onBookmarksChanged(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/bookmarks/Bookmark;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->bookmarkViewListener:Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;->onBookmarksChanged(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public onDocumentClick()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method public onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->document:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getBookmarkProvider()Lcom/pspdfkit/bookmarks/BookmarkProvider;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/bookmarks/BookmarkProvider;->removeBookmarkListener(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;)V

    .line 2
    :cond_0
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getBookmarkProvider()Lcom/pspdfkit/bookmarks/BookmarkProvider;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/bookmarks/BookmarkProvider;->addBookmarkListener(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->document:Lcom/pspdfkit/document/PdfDocument;

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->getBookmarks()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->onBookmarksChanged(Ljava/util/List;)V

    return-void
.end method

.method public onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method public onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V
    .locals 0

    return-void
.end method

.method public onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    return-void
.end method

.method public onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    return-void
.end method

.method public removeBookmarkListener(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object v1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->bookmarkViewListener:Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;

    .line 55
    iget-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->document:Lcom/pspdfkit/document/PdfDocument;

    if-eqz p1, :cond_0

    .line 56
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getBookmarkProvider()Lcom/pspdfkit/bookmarks/BookmarkProvider;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/bookmarks/BookmarkProvider;->removeBookmarkListener(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;)V

    .line 58
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    return-void
.end method

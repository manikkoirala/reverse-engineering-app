.class Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private systemUiVisibleLock:Lcom/pspdfkit/internal/ui/a$b;

.field final synthetic this$0:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->this$0:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHide(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->this$0:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->-$$Nest$fgetactivePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->-$$Nest$fgetlifecycleListeners(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)Lcom/pspdfkit/internal/nh;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->this$0:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-static {v1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->-$$Nest$fgetactivePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;->onRemovePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    goto :goto_0

    .line 8
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->this$0:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->-$$Nest$fgetactivePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->reset()V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->systemUiVisibleLock:Lcom/pspdfkit/internal/ui/a$b;

    if-eqz p1, :cond_2

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->this$0:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->systemUiVisibleLock:Lcom/pspdfkit/internal/ui/a$b;

    .line 12
    invoke-static {p1}, Lcom/pspdfkit/internal/ce;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/ui/a;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 14
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/a;->a(Lcom/pspdfkit/internal/ui/a$b;)V

    :cond_1
    const/4 p1, 0x0

    .line 15
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->systemUiVisibleLock:Lcom/pspdfkit/internal/ui/a$b;

    .line 18
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->this$0:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->-$$Nest$mreset(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)V

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->this$0:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-static {p1}, Lcom/pspdfkit/internal/pg;->e(Landroid/view/View;)V

    return-void
.end method

.method public onShow(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->this$0:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->-$$Nest$fgetactivePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->-$$Nest$fgetlifecycleListeners(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)Lcom/pspdfkit/internal/nh;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->this$0:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-static {v1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->-$$Nest$fgetactivePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;->onDisplayPropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    goto :goto_0

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->this$0:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->systemUiVisibleLock:Lcom/pspdfkit/internal/ui/a$b;

    .line 6
    invoke-static {p1}, Lcom/pspdfkit/internal/ce;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/ui/a;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/a;->e()Lcom/pspdfkit/internal/ui/a$b;

    move-result-object v1

    if-eqz v0, :cond_2

    .line 10
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/a;->a(Lcom/pspdfkit/internal/ui/a$b;)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 11
    :cond_2
    :goto_1
    iput-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->systemUiVisibleLock:Lcom/pspdfkit/internal/ui/a$b;

    .line 13
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;->this$0:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->-$$Nest$fgetactivePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 14
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    :cond_4
    return-void
.end method

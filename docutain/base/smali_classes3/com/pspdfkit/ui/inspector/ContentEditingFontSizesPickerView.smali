.class public Lcom/pspdfkit/ui/inspector/ContentEditingFontSizesPickerView;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;


# instance fields
.field private final availableFontSizes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final listener:Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontSizePickerListener;

.field unmatchedCurrentSize:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontSizePickerListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontSizePickerListener;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/ContentEditingFontSizesPickerView;->unmatchedCurrentSize:Ljava/lang/String;

    .line 12
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/ContentEditingFontSizesPickerView;->availableFontSizes:Ljava/util/List;

    .line 13
    iput-object p5, p0, Lcom/pspdfkit/ui/inspector/ContentEditingFontSizesPickerView;->listener:Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontSizePickerListener;

    .line 14
    invoke-direct {p0, p3, p4}, Lcom/pspdfkit/ui/inspector/ContentEditingFontSizesPickerView;->init(Ljava/lang/Integer;Ljava/lang/String;)V

    return-void
.end method

.method private init(Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 8

    .line 1
    new-instance v7, Lcom/pspdfkit/internal/qb;

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/pspdfkit/ui/inspector/ContentEditingFontSizesPickerView;->availableFontSizes:Ljava/util/List;

    iget-object v6, p0, Lcom/pspdfkit/ui/inspector/ContentEditingFontSizesPickerView;->listener:Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontSizePickerListener;

    move-object v0, v7

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/qb;-><init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontSizePickerListener;)V

    .line 3
    invoke-virtual {p0, v7}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 4
    new-instance p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p1, p2, v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 5
    new-instance p1, Lcom/pspdfkit/internal/oo;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/oo;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 0

    return-void
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getMinimumHeight()I

    move-result v0

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic isViewStateRestorationEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$isViewStateRestorationEnabled(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)Z

    move-result v0

    return v0
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public unbindController()V
    .locals 0

    return-void
.end method

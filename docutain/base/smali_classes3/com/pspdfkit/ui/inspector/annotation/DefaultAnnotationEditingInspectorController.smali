.class public Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;
.super Lcom/pspdfkit/ui/inspector/annotation/AbstractAnnotationInspectorController;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;


# instance fields
.field private annotationInspectorFactory:Lcom/pspdfkit/internal/q0;

.field private controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

.field private final onAnnotationEditingModeChangeListener:Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;


# direct methods
.method static bridge synthetic -$$Nest$mapplyControllerChanges(Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->applyControllerChanges()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/inspector/annotation/AbstractAnnotationInspectorController;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController;)V

    .line 2
    new-instance p1, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController$1;

    invoke-direct {p1, p0}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController$1;-><init>(Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->onAnnotationEditingModeChangeListener:Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;

    .line 26
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object p1

    sget p2, Lcom/pspdfkit/R$id;->pspdf__annotation_editing_inspector:I

    invoke-virtual {p1, p2}, Landroid/view/View;->setId(I)V

    .line 27
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->setCancelOnTouchOutside(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->cancel()V

    return-void
.end method

.method private applyControllerChanges()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->isAnnotationInspectorVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_2

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->annotationInspectorFactory:Lcom/pspdfkit/internal/q0;

    if-nez v0, :cond_0

    goto :goto_0

    .line 9
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->reset()V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->annotationInspectorFactory:Lcom/pspdfkit/internal/q0;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 11
    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q0;->b(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 13
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->cancel()V

    return-void

    .line 16
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->setInspectorViews(Ljava/util/List;Z)V

    .line 17
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 19
    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object v1

    iget-object v1, v1, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 20
    invoke-static {v1}, Lcom/pspdfkit/internal/ao;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->setTitle(I)V

    return-void

    .line 21
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->cancel()V

    return-void
.end method


# virtual methods
.method public bindAnnotationEditingController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->unbindAnnotationEditingController()V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/q0;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/q0;-><init>(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->annotationInspectorFactory:Lcom/pspdfkit/internal/q0;

    .line 6
    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->bindAnnotationInspectorController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;)V

    .line 8
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->onAnnotationEditingModeChangeListener:Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;

    .line 9
    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->addOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V

    .line 11
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->applyControllerChanges()V

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->onRestoreState()Z

    return-void
.end method

.method public getAnnotationEditingController()Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    return-object v0
.end method

.method public hasAnnotationInspector()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->annotationInspectorFactory:Lcom/pspdfkit/internal/q0;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->annotationInspectorFactory:Lcom/pspdfkit/internal/q0;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 4
    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q0;->c(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public bridge synthetic hideAnnotationInspector(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/inspector/annotation/AbstractAnnotationInspectorController;->hideAnnotationInspector(Z)V

    return-void
.end method

.method public bridge synthetic isAnnotationInspectorVisible()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/inspector/annotation/AbstractAnnotationInspectorController;->isAnnotationInspectorVisible()Z

    move-result v0

    return v0
.end method

.method protected isBoundToController()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onPreparePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->onPreparePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->applyControllerChanges()V

    return-void
.end method

.method public bridge synthetic showAnnotationInspector(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/inspector/annotation/AbstractAnnotationInspectorController;->showAnnotationInspector(Z)V

    return-void
.end method

.method public bridge synthetic toggleAnnotationInspector(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/inspector/annotation/AbstractAnnotationInspectorController;->toggleAnnotationInspector(Z)V

    return-void
.end method

.method public unbindAnnotationEditingController()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->onAnnotationEditingModeChangeListener:Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;

    .line 4
    invoke-interface {v0, v1}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->removeOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->unbindAnnotationInspectorController()V

    const/4 v0, 0x0

    .line 6
    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    .line 8
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->cancel()V

    return-void
.end method

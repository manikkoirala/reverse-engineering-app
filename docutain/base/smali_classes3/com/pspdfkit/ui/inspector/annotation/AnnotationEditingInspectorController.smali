.class public interface abstract Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;


# virtual methods
.method public abstract bindAnnotationEditingController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
.end method

.method public abstract onRestoreInstanceState(Landroid/os/Bundle;)V
.end method

.method public abstract onSaveInstanceState(Landroid/os/Bundle;)V
.end method

.method public abstract unbindAnnotationEditingController()V
.end method

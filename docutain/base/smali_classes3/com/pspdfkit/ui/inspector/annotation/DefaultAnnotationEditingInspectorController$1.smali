.class Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController$1;->this$0:Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChangeAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController$1;->this$0:Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;

    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->-$$Nest$mapplyControllerChanges(Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;)V

    return-void
.end method

.method public onEnterAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 0

    return-void
.end method

.method public onExitAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController$1;->this$0:Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;

    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;->access$000(Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;)V

    return-void
.end method

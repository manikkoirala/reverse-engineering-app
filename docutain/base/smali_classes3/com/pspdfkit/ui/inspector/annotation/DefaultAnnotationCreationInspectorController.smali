.class public Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;
.super Lcom/pspdfkit/ui/inspector/annotation/AbstractAnnotationInspectorController;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;


# instance fields
.field private annotationInspectorFactory:Lcom/pspdfkit/internal/m0;

.field private controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

.field private final onAnnotationCreationModeChangeListener:Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;

.field private final previewDivider:Lcom/pspdfkit/ui/inspector/PropertyInspector$ItemDecoration;


# direct methods
.method static bridge synthetic -$$Nest$mapplyControllerChanges(Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->applyControllerChanges()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/inspector/annotation/AbstractAnnotationInspectorController;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController;)V

    .line 2
    new-instance p2, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController$1;

    invoke-direct {p2, p0}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController$1;-><init>(Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;)V

    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->onAnnotationCreationModeChangeListener:Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;

    .line 26
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object p2

    sget v0, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_inspector:I

    invoke-virtual {p2, v0}, Landroid/view/View;->setId(I)V

    .line 27
    new-instance p2, Lcom/pspdfkit/internal/l0;

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/l0;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->previewDivider:Lcom/pspdfkit/ui/inspector/PropertyInspector$ItemDecoration;

    return-void
.end method

.method static synthetic access$000(Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->cancel()V

    return-void
.end method

.method private applyControllerChanges()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->isAnnotationInspectorVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    if-eqz v0, :cond_2

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->annotationInspectorFactory:Lcom/pspdfkit/internal/m0;

    if-nez v0, :cond_0

    goto :goto_0

    .line 10
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 11
    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v2

    .line 12
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/m0;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/util/List;

    move-result-object v0

    .line 14
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 15
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->cancel()V

    return-void

    .line 18
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->previewDivider:Lcom/pspdfkit/ui/inspector/PropertyInspector$ItemDecoration;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->addItemDecoration(Lcom/pspdfkit/ui/inspector/PropertyInspector$ItemDecoration;)V

    .line 19
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->setInspectorViews(Ljava/util/List;Z)V

    .line 20
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 21
    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/ao;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->setTitle(I)V

    return-void

    .line 22
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->cancel()V

    return-void
.end method


# virtual methods
.method public bindAnnotationCreationController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->unbindAnnotationCreationController()V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/m0;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/m0;-><init>(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->annotationInspectorFactory:Lcom/pspdfkit/internal/m0;

    .line 6
    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->bindAnnotationInspectorController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;)V

    .line 8
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->onAnnotationCreationModeChangeListener:Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;

    .line 9
    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->addOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V

    .line 11
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->applyControllerChanges()V

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->onRestoreState()Z

    return-void
.end method

.method public getAnnotationCreationController()Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    return-object v0
.end method

.method public hasAnnotationInspector()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->annotationInspectorFactory:Lcom/pspdfkit/internal/m0;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->annotationInspectorFactory:Lcom/pspdfkit/internal/m0;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 5
    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v2

    .line 6
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/m0;->b(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public bridge synthetic hideAnnotationInspector(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/inspector/annotation/AbstractAnnotationInspectorController;->hideAnnotationInspector(Z)V

    return-void
.end method

.method public bridge synthetic isAnnotationInspectorVisible()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/inspector/annotation/AbstractAnnotationInspectorController;->isAnnotationInspectorVisible()Z

    move-result v0

    return v0
.end method

.method protected isBoundToController()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onPreparePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->onPreparePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->applyControllerChanges()V

    return-void
.end method

.method public bridge synthetic showAnnotationInspector(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/inspector/annotation/AbstractAnnotationInspectorController;->showAnnotationInspector(Z)V

    return-void
.end method

.method public bridge synthetic toggleAnnotationInspector(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/inspector/annotation/AbstractAnnotationInspectorController;->toggleAnnotationInspector(Z)V

    return-void
.end method

.method public unbindAnnotationCreationController()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->onAnnotationCreationModeChangeListener:Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;

    .line 4
    invoke-interface {v0, v2}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->removeOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->unbindAnnotationInspectorController()V

    .line 6
    iput-object v1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 8
    :cond_0
    iput-object v1, p0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;->annotationInspectorFactory:Lcom/pspdfkit/internal/m0;

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->cancel()V

    return-void
.end method

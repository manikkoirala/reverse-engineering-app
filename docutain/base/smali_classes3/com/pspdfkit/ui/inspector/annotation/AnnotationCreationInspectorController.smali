.class public interface abstract Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;


# virtual methods
.method public abstract bindAnnotationCreationController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
.end method

.method public abstract onRestoreInstanceState(Landroid/os/Bundle;)V
.end method

.method public abstract onSaveInstanceState(Landroid/os/Bundle;)V
.end method

.method public abstract unbindAnnotationCreationController()V
.end method

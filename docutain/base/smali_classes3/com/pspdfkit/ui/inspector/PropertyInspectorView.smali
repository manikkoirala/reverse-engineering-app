.class public interface abstract Lcom/pspdfkit/ui/inspector/PropertyInspectorView;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
.end method

.method public abstract getPropertyInspectorMaxHeight()I
.end method

.method public abstract getPropertyInspectorMinHeight()I
.end method

.method public abstract getSuggestedHeight()I
.end method

.method public abstract getView()Landroid/view/View;
.end method

.method public abstract isViewStateRestorationEnabled()Z
.end method

.method public abstract onHidden()V
.end method

.method public abstract onShown()V
.end method

.method public abstract unbindController()V
.end method

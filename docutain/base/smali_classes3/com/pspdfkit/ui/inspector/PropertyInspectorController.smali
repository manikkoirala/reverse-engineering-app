.class public interface abstract Lcom/pspdfkit/ui/inspector/PropertyInspectorController;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract ensureFullyVisible(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V
.end method

.method public abstract getVisibleDetailView()Landroid/view/View;
.end method

.method public abstract hideDetailView(Z)V
.end method

.method public abstract showDetailView(Landroid/view/View;Ljava/lang/String;Z)V
.end method

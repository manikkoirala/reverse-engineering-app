.class public Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;
.super Landroidx/coordinatorlayout/widget/CoordinatorLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController;


# instance fields
.field private activePropertyInspector:Lcom/pspdfkit/ui/inspector/PropertyInspector;

.field private bottomInset:I

.field private bottomSheetLayout:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/views/inspector/bottomsheet/c<",
            "Lcom/pspdfkit/ui/inspector/PropertyInspector;",
            ">;"
        }
    .end annotation
.end field

.field private customBottomInset:I

.field private keyboardObserver:Lcom/pspdfkit/internal/pg$c;

.field private final lifecycleListeners:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;",
            ">;"
        }
    .end annotation
.end field

.field private showInspectorViewsUnderBottomInset:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetactivePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)Lcom/pspdfkit/ui/inspector/PropertyInspector;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->activePropertyInspector:Lcom/pspdfkit/ui/inspector/PropertyInspector;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetlifecycleListeners(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)Lcom/pspdfkit/internal/nh;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mreset(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->reset()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    const/4 v0, 0x0

    .line 17
    iput-boolean v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->showInspectorViewsUnderBottomInset:Z

    const/4 v1, 0x0

    .line 21
    invoke-direct {p0, p1, v1, v0, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 22
    invoke-direct {p0, p1, p2}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    const/4 v0, 0x0

    .line 38
    iput-boolean v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->showInspectorViewsUnderBottomInset:Z

    .line 47
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    const/4 v0, 0x0

    .line 64
    iput-boolean v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->showInspectorViewsUnderBottomInset:Z

    .line 79
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3

    .line 1
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x1010440

    const/4 v2, 0x0

    aput v1, v0, v2

    .line 6
    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p3, Lcom/pspdfkit/R$dimen;->pspdf__inspector_elevation:I

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    .line 9
    invoke-virtual {p2, v2, p1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result p1

    .line 11
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    int-to-float p1, p1

    .line 12
    invoke-static {p0, p1}, Landroidx/core/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    .line 14
    new-instance p1, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->bottomSheetLayout:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    .line 15
    new-instance p2, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;

    invoke-direct {p2, p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$1;-><init>(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)V

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->setCallback(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$a;)V

    .line 53
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->bottomSheetLayout:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 54
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->bottomSheetLayout:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method private refreshBottomInset()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->activePropertyInspector:Lcom/pspdfkit/ui/inspector/PropertyInspector;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->bottomSheetLayout:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    iget v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->bottomInset:I

    iget v2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->customBottomInset:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->setBottomInset(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->activePropertyInspector:Lcom/pspdfkit/ui/inspector/PropertyInspector;

    iget v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->bottomInset:I

    iget v2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->customBottomInset:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->setBottomInset(I)V

    .line 7
    iget-boolean v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->showInspectorViewsUnderBottomInset:Z

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 8
    invoke-static {p0}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/ce;->a(Landroid/app/Activity;)I

    move-result v0

    .line 11
    iget v2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->bottomInset:I

    if-lt v0, v2, :cond_1

    move v1, v2

    .line 13
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0, v0, v2, v3, v1}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method private reset()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->keyboardObserver:Lcom/pspdfkit/internal/pg$c;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/pg$c;->d()V

    .line 4
    :cond_0
    invoke-static {p0}, Lcom/pspdfkit/internal/pg;->e(Landroid/view/View;)V

    .line 6
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-gt v0, v1, :cond_1

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->bottomSheetLayout:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    if-eq v0, v1, :cond_2

    .line 7
    :cond_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->bottomSheetLayout:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 10
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->activePropertyInspector:Lcom/pspdfkit/ui/inspector/PropertyInspector;

    if-eqz v0, :cond_3

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->reset()V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->activePropertyInspector:Lcom/pspdfkit/ui/inspector/PropertyInspector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->setCancelListener(Lcom/pspdfkit/ui/inspector/PropertyInspector$OnCancelListener;)V

    .line 13
    iput-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->activePropertyInspector:Lcom/pspdfkit/ui/inspector/PropertyInspector;

    .line 15
    :cond_3
    iput v2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->customBottomInset:I

    return-void
.end method


# virtual methods
.method public addPropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2
    instance-of v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v1, :cond_0

    .line 7
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 8
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 9
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 10
    iget v1, p1, Landroid/graphics/Rect;->right:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 11
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 16
    :cond_0
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    iput p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->bottomInset:I

    .line 17
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->refreshBottomInset()V

    const/4 p1, 0x0

    return p1
.end method

.method getActiveInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->activePropertyInspector:Lcom/pspdfkit/ui/inspector/PropertyInspector;

    return-object v0
.end method

.method public hideInspector(Z)Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->getActiveInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 4
    :cond_0
    invoke-static {p0}, Lcom/pspdfkit/internal/pg;->d(Landroid/view/View;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->bottomSheetLayout:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->a(Z)V

    const/4 p1, 0x1

    return p1
.end method

.method public isInspectorVisible()Z
    .locals 1

    .line 55
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->getActiveInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isInspectorVisible(Lcom/pspdfkit/ui/inspector/PropertyInspector;)Z
    .locals 2

    const-string v0, "inspector"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->getActiveInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method synthetic lambda$showInspector$0$com-pspdfkit-ui-inspector-PropertyInspectorCoordinatorLayout(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 0

    const/4 p1, 0x1

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->hideInspector(Z)Z

    return-void
.end method

.method synthetic lambda$showInspector$1$com-pspdfkit-ui-inspector-PropertyInspectorCoordinatorLayout(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p1, 0x1

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->hideInspector(Z)Z

    const/4 p1, 0x0

    return p1
.end method

.method synthetic lambda$showInspector$2$com-pspdfkit-ui-inspector-PropertyInspectorCoordinatorLayout(Lcom/pspdfkit/ui/inspector/PropertyInspector;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->hideInspector(Z)Z

    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;->onDetachedFromWindow()V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->reset()V

    return-void
.end method

.method public removePropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public setBottomInset(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->customBottomInset:I

    if-ne v0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    iput p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->customBottomInset:I

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->refreshBottomInset()V

    return-void
.end method

.method public setDrawUnderBottomInset(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->showInspectorViewsUnderBottomInset:Z

    if-eq v0, p1, :cond_0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->showInspectorViewsUnderBottomInset:Z

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->refreshBottomInset()V

    :cond_0
    return-void
.end method

.method public setFitsSystemWindows(Z)V
    .locals 0

    const/4 p1, 0x0

    .line 1
    invoke-super {p0, p1}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;->setFitsSystemWindows(Z)V

    return-void
.end method

.method public showInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;Z)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->activePropertyInspector:Lcom/pspdfkit/ui/inspector/PropertyInspector;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    if-ne v0, p1, :cond_0

    return v1

    .line 4
    :cond_0
    invoke-virtual {p0, v1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->hideInspector(Z)Z

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->activePropertyInspector:Lcom/pspdfkit/ui/inspector/PropertyInspector;

    .line 7
    new-instance v0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)V

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->setCancelListener(Lcom/pspdfkit/ui/inspector/PropertyInspector$OnCancelListener;)V

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->isCancelOnTouchOutside()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 10
    new-instance v2, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 14
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 15
    invoke-virtual {v0, v1}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 16
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 22
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->refreshBottomInset()V

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->bottomSheetLayout:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->setContentView(Landroid/view/View;)V

    .line 26
    new-instance v0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/pg;->a(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;)Lcom/pspdfkit/internal/pg$c;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->keyboardObserver:Lcom/pspdfkit/internal/pg$c;

    .line 32
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->lifecycleListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;

    .line 33
    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->activePropertyInspector:Lcom/pspdfkit/ui/inspector/PropertyInspector;

    invoke-interface {v0, v2}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;->onPreparePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    goto :goto_0

    .line 38
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->bottomSheetLayout:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    const/4 v0, 0x1

    if-eqz p2, :cond_3

    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->keyboardObserver:Lcom/pspdfkit/internal/pg$c;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/pg$c;->b()Z

    move-result p2

    if-nez p2, :cond_3

    const/4 v1, 0x1

    :cond_3
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->b(Z)V

    return v0
.end method

.class public interface abstract Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;
    }
.end annotation


# virtual methods
.method public abstract addPropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
.end method

.method public abstract hideInspector(Z)Z
.end method

.method public abstract isInspectorVisible()Z
.end method

.method public abstract isInspectorVisible(Lcom/pspdfkit/ui/inspector/PropertyInspector;)Z
.end method

.method public abstract removePropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V
.end method

.method public abstract setBottomInset(I)V
.end method

.method public abstract setDrawUnderBottomInset(Z)V
.end method

.method public abstract showInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;Z)Z
.end method

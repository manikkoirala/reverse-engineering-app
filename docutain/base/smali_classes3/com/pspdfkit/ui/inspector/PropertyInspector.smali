.class public Lcom/pspdfkit/ui/inspector/PropertyInspector;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/b5;
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorController;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/PropertyInspector$SavedState;,
        Lcom/pspdfkit/ui/inspector/PropertyInspector$OnCancelListener;,
        Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;,
        Lcom/pspdfkit/ui/inspector/PropertyInspector$ItemDecoration;
    }
.end annotation


# static fields
.field private static final DETAIL_VIEW_ANIMATION_DURATION_MS:I = 0xfa

.field private static final FADE_ANIMATION_DURATION_MS:I = 0x12c


# instance fields
.field private activeDetailView:Landroid/view/View;

.field private bottomInset:I

.field private cancelListener:Lcom/pspdfkit/ui/inspector/PropertyInspector$OnCancelListener;

.field private cancelOnTouchOutside:Z

.field private final containerSwitcher:Landroid/widget/FrameLayout;

.field private detailScrollView:Landroidx/core/widget/NestedScrollView;

.field private final inspectorScrollView:Landroidx/core/widget/NestedScrollView;

.field private inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

.field private final itemDecorations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/inspector/PropertyInspector$ItemDecoration;",
            ">;"
        }
    .end annotation
.end field

.field private maximumHeight:I

.field private minimumHeight:I

.field private propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

.field private showingDetailView:Z

.field private final style:Lcom/pspdfkit/internal/fo;

.field private suggestedHeight:I

.field private final views:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/inspector/PropertyInspectorView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-static {p1}, Lcom/pspdfkit/internal/go;->b(Landroid/content/Context;)I

    move-result v1

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {p0, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->views:Ljava/util/List;

    .line 5
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->itemDecorations:Ljava/util/List;

    const p1, 0x7fffffff

    .line 12
    iput p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->suggestedHeight:I

    const/4 p1, 0x0

    .line 14
    iput p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->minimumHeight:I

    .line 15
    iput p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->maximumHeight:I

    .line 20
    iput-boolean p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->cancelOnTouchOutside:Z

    .line 35
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->style:Lcom/pspdfkit/internal/fo;

    .line 37
    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p1, v0, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 40
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->createTitleView()V

    .line 43
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->createContainerLayout()Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    .line 47
    new-instance p1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->containerSwitcher:Landroid/widget/FrameLayout;

    .line 48
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 51
    new-instance v0, Landroidx/core/widget/NestedScrollView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/core/widget/NestedScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorScrollView:Landroidx/core/widget/NestedScrollView;

    const/4 v1, 0x1

    .line 52
    invoke-virtual {v0, v1}, Landroidx/core/widget/NestedScrollView;->setFillViewport(Z)V

    .line 53
    invoke-virtual {v0, p1}, Landroidx/core/widget/NestedScrollView;->addView(Landroid/view/View;)V

    .line 54
    invoke-virtual {v0, v1}, Landroidx/core/widget/NestedScrollView;->setNestedScrollingEnabled(Z)V

    .line 55
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 58
    invoke-virtual {p0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 59
    invoke-virtual {p0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private applyEnterAnimation(Landroid/view/View;Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;)V
    .locals 5

    const-string v0, "animationType"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    const/4 v0, 0x0

    .line 55
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 57
    sget-object v0, Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;->NONE:Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    if-ne p2, v0, :cond_0

    .line 58
    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 59
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    return-void

    .line 63
    :cond_0
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 64
    invoke-virtual {v0, v3}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    const-wide/16 v3, 0xfa

    .line 65
    invoke-virtual {v0, v3, v4}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    .line 67
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 70
    sget-object v3, Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;->LEFT_TO_RIGHT:Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;

    if-ne p2, v3, :cond_1

    neg-int p2, v0

    int-to-float p2, p2

    goto :goto_0

    :cond_1
    int-to-float p2, v0

    :goto_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    .line 72
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    invoke-virtual {p2, v2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationX(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    .line 74
    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 75
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method

.method private applyLeaveAnimation(Landroid/view/View;Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;)V
    .locals 4

    const-string v0, "animationType"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 56
    sget-object v0, Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;->NONE:Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;

    if-ne p2, v0, :cond_0

    const/16 p2, 0x8

    .line 57
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void

    .line 61
    :cond_0
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 62
    invoke-virtual {v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    const-wide/16 v1, 0xfa

    .line 63
    invoke-virtual {v0, v1, v2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    .line 65
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    .line 66
    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 68
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    .line 69
    sget-object v3, Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;->LEFT_TO_RIGHT:Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;

    if-ne p2, v3, :cond_1

    int-to-float p2, v0

    goto :goto_0

    :cond_1
    neg-int p2, v0

    int-to-float p2, p2

    :goto_0
    invoke-virtual {v2, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationX(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    const/high16 p2, 0x3f800000    # 1.0f

    .line 71
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 72
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    invoke-virtual {p2, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    .line 74
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/ui/inspector/PropertyInspector$$ExternalSyntheticLambda1;

    invoke-direct {v0, p1}, Lcom/pspdfkit/ui/inspector/PropertyInspector$$ExternalSyntheticLambda1;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method

.method private createContainerLayout()Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->style:Lcom/pspdfkit/internal/fo;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/fo;->g()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 3
    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    return-object v0
.end method

.method private createTitleView()V
    .locals 4

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/ui/dialog/utils/b;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/pspdfkit/internal/ui/dialog/utils/b;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/ui/dialog/utils/a;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ui/dialog/utils/b;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    .line 2
    sget v1, Lcom/pspdfkit/R$id;->pspdf__bottom_sheet_drag_to_resize_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setBackButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setCloseButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setCloseButtonVisible(Z)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic lambda$applyLeaveAnimation$2(Landroid/view/View;)V
    .locals 1

    const/16 v0, 0x8

    .line 1
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2
    instance-of v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    if-eqz v0, :cond_0

    .line 3
    check-cast p0, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    .line 4
    invoke-interface {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->onHidden()V

    :cond_0
    return-void
.end method


# virtual methods
.method public addInspectorView(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->addInspectorView(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    return-void
.end method

.method public addInspectorView(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->views:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 5
    invoke-interface {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    invoke-interface {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_0

    .line 8
    :cond_0
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    invoke-interface {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2, p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 13
    :goto_0
    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V

    .line 14
    invoke-interface {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->onShown()V

    return-void
.end method

.method public addItemDecoration(Lcom/pspdfkit/ui/inspector/PropertyInspector$ItemDecoration;)V
    .locals 1

    const/4 v0, -0x1

    .line 66
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->addItemDecoration(Lcom/pspdfkit/ui/inspector/PropertyInspector$ItemDecoration;I)V

    return-void
.end method

.method public addItemDecoration(Lcom/pspdfkit/ui/inspector/PropertyInspector$ItemDecoration;I)V
    .locals 2

    const-string v0, "decoration"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->itemDecorations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setWillNotDraw(Z)V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->itemDecorations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    if-gez p2, :cond_2

    .line 61
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->itemDecorations:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 63
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->itemDecorations:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 65
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public cancel()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->cancelListener:Lcom/pspdfkit/ui/inspector/PropertyInspector$OnCancelListener;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector$OnCancelListener;->onCancel(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    :cond_0
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->showingDetailView:Z

    .line 2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    const/4 v1, 0x1

    if-nez p1, :cond_1

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {p0, v1}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->hideDetailView(Z)V

    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->cancel()V

    :cond_1
    :goto_0
    return v1

    .line 14
    :cond_2
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    return-void
.end method

.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 3
    instance-of v1, v0, Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 4
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 5
    invoke-virtual {v0, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 7
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 8
    invoke-static {v0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 12
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public ensureFullyVisible(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V
    .locals 1

    .line 1
    invoke-interface {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getView()Landroid/view/View;

    move-result-object p1

    .line 2
    new-instance v0, Lcom/pspdfkit/ui/inspector/PropertyInspector$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/ui/inspector/PropertyInspector$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/inspector/PropertyInspector;Landroid/view/View;)V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method public getInspectorView(I)Lcom/pspdfkit/ui/inspector/PropertyInspectorView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->views:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    return-object p1
.end method

.method public getInspectorViewCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->views:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method getItemDecorations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/inspector/PropertyInspector$ItemDecoration;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->itemDecorations:Ljava/util/List;

    return-object v0
.end method

.method public getMaximumHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->maximumHeight:I

    return v0
.end method

.method public getMinimumHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->minimumHeight:I

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->suggestedHeight:I

    return v0
.end method

.method public getVisibleDetailView()Landroid/view/View;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->showingDetailView:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->activeDetailView:Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public hideDetailView(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->activeDetailView:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->showingDetailView:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->showingDetailView:Z

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorScrollView:Landroidx/core/widget/NestedScrollView;

    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    invoke-virtual {v1, v0}, Landroidx/core/widget/NestedScrollView;->setNestedScrollingEnabled(Z)V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorScrollView:Landroidx/core/widget/NestedScrollView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroidx/core/widget/NestedScrollView;->setNestedScrollingEnabled(Z)V

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    if-eqz p1, :cond_0

    .line 8
    sget-object v2, Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;->LEFT_TO_RIGHT:Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;->NONE:Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;

    .line 9
    :goto_0
    invoke-direct {p0, v1, v2}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->applyLeaveAnimation(Landroid/view/View;Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;)V

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorScrollView:Landroidx/core/widget/NestedScrollView;

    if-eqz p1, :cond_1

    .line 12
    sget-object v2, Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;->LEFT_TO_RIGHT:Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;

    goto :goto_1

    :cond_1
    sget-object v2, Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;->NONE:Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;

    .line 13
    :goto_1
    invoke-direct {p0, v1, v2}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->applyEnterAnimation(Landroid/view/View;Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;)V

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v1, v0, p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b(ZZ)V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b()V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->activeDetailView:Landroid/view/View;

    instance-of v0, p1, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    if-eqz v0, :cond_2

    .line 20
    check-cast p1, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    .line 21
    invoke-interface {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->onHidden()V

    :cond_2
    return-void
.end method

.method public indexOfInspectorView(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->views:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public isCancelOnTouchOutside()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->cancelOnTouchOutside:Z

    return v0
.end method

.method synthetic lambda$ensureFullyVisible$1$com-pspdfkit-ui-inspector-PropertyInspector(Landroid/view/View;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorScrollView:Landroidx/core/widget/NestedScrollView;

    invoke-virtual {v1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v2

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorScrollView:Landroidx/core/widget/NestedScrollView;

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result p1

    float-to-int p1, p1

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroidx/core/widget/NestedScrollView;->smoothScrollTo(II)V

    :cond_1
    return-void
.end method

.method synthetic lambda$setInspectorViews$0$com-pspdfkit-ui-inspector-PropertyInspector(Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->containerSwitcher:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method protected onBackButtonClicked()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->hideDetailView(Z)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->getBackButton()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->onBackButtonClicked()V

    goto :goto_0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->getCloseButton()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->onCloseButtonClicked()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected onCloseButtonClicked()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->cancel()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    const/4 p1, 0x0

    const/4 p2, 0x0

    .line 1
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p3

    if-ge p2, p3, :cond_4

    .line 2
    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p3

    .line 4
    iget-object p4, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorScrollView:Landroidx/core/widget/NestedScrollView;

    if-eq p3, p4, :cond_1

    iget-object p4, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    if-ne p3, p4, :cond_0

    goto :goto_1

    .line 8
    :cond_0
    iget-object p4, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-ne p3, p4, :cond_3

    .line 9
    invoke-virtual {p3}, Landroid/view/View;->getMeasuredWidth()I

    move-result p4

    invoke-virtual {p3}, Landroid/view/View;->getMeasuredHeight()I

    move-result p5

    invoke-virtual {p3, p1, p1, p4, p5}, Landroid/view/View;->layout(IIII)V

    goto :goto_3

    .line 10
    :cond_1
    :goto_1
    iget-object p4, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p4}, Landroid/view/View;->getVisibility()I

    move-result p4

    const/16 p5, 0x8

    if-eq p4, p5, :cond_2

    iget-object p4, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p4}, Landroid/view/View;->getMeasuredHeight()I

    move-result p4

    goto :goto_2

    :cond_2
    const/4 p4, 0x0

    .line 11
    :goto_2
    invoke-virtual {p3}, Landroid/view/View;->getMeasuredWidth()I

    move-result p5

    invoke-virtual {p3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p4

    invoke-virtual {p3, p1, p4, p5, v0}, Landroid/view/View;->layout(IIII)V

    :cond_3
    :goto_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 16

    move-object/from16 v0, p0

    move/from16 v1, p1

    .line 1
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 2
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 5
    iget-object v4, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/4 v5, 0x0

    const/16 v6, 0x8

    if-eq v4, v6, :cond_0

    iget-object v4, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->getTitleHeight()I

    move-result v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    sub-int v6, v2, v4

    .line 6
    iget v7, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->bottomInset:I

    sub-int/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    .line 9
    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 10
    iget-object v9, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v9, v1, v8}, Landroid/view/View;->measure(II)V

    .line 13
    iget-object v8, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorScrollView:Landroidx/core/widget/NestedScrollView;

    invoke-static {v6, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v8, v1, v9}, Landroid/view/View;->measure(II)V

    .line 14
    iget-object v8, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorScrollView:Landroidx/core/widget/NestedScrollView;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 18
    iget-object v9, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    if-eqz v9, :cond_1

    iget-boolean v10, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->showingDetailView:Z

    if-eqz v10, :cond_1

    .line 19
    invoke-static {v6, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v9, v1, v6}, Landroid/view/View;->measure(II)V

    .line 20
    iget-object v6, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    .line 28
    :goto_1
    iget-boolean v9, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->showingDetailView:Z

    if-nez v9, :cond_3

    .line 30
    iget-object v9, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->views:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    .line 31
    invoke-interface {v14}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getPropertyInspectorMinHeight()I

    move-result v15

    invoke-static {v15, v13}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 32
    invoke-interface {v14}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getPropertyInspectorMaxHeight()I

    move-result v15

    invoke-static {v15, v12}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 33
    invoke-interface {v14}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getView()Landroid/view/View;

    move-result-object v15

    invoke-virtual {v15}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    add-int/2addr v11, v15

    .line 34
    invoke-interface {v14}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getSuggestedHeight()I

    move-result v14

    add-int/2addr v10, v14

    goto :goto_2

    .line 37
    :cond_2
    iget-object v9, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorScrollView:Landroidx/core/widget/NestedScrollView;

    .line 38
    invoke-virtual {v9}, Landroidx/core/widget/NestedScrollView;->getScrollBarSize()I

    move-result v9

    iget-object v14, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    invoke-virtual {v14}, Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;->getVerticalInset()I

    move-result v14

    add-int/2addr v9, v14

    add-int/2addr v13, v9

    add-int/2addr v12, v9

    add-int/2addr v11, v9

    add-int/2addr v10, v9

    goto :goto_3

    .line 45
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->getVisibleDetailView()Landroid/view/View;

    move-result-object v9

    .line 46
    instance-of v10, v9, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    if-eqz v10, :cond_4

    .line 47
    check-cast v9, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    .line 48
    invoke-interface {v9}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getPropertyInspectorMinHeight()I

    move-result v13

    .line 49
    invoke-interface {v9}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getPropertyInspectorMaxHeight()I

    move-result v12

    .line 50
    invoke-interface {v9}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getView()Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    .line 51
    invoke-interface {v9}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getSuggestedHeight()I

    move-result v10

    goto :goto_3

    :cond_4
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    :goto_3
    const/4 v9, 0x2

    new-array v14, v9, [I

    mul-int/lit8 v15, v4, 0x2

    aput v15, v14, v5

    add-int/2addr v13, v4

    const/4 v15, 0x1

    aput v13, v14, v15

    .line 56
    invoke-static {v14}, Lcom/pspdfkit/internal/di;->a([I)I

    move-result v13

    iget v14, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->bottomInset:I

    add-int/2addr v13, v14

    iput v13, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->minimumHeight:I

    new-array v7, v9, [I

    aput v13, v7, v5

    add-int/2addr v10, v4

    add-int/2addr v10, v14

    aput v10, v7, v15

    .line 57
    invoke-static {v7}, Lcom/pspdfkit/internal/di;->a([I)I

    move-result v7

    iput v7, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->suggestedHeight:I

    const/4 v7, 0x3

    new-array v7, v7, [I

    .line 58
    iget v10, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->minimumHeight:I

    aput v10, v7, v5

    new-array v10, v9, [I

    aput v11, v10, v5

    aput v12, v10, v15

    .line 60
    invoke-static {v10}, Lcom/pspdfkit/internal/di;->a([I)I

    move-result v5

    add-int/2addr v5, v4

    iget v10, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->bottomInset:I

    add-int/2addr v5, v10

    aput v5, v7, v15

    iget v5, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->suggestedHeight:I

    aput v5, v7, v9

    .line 61
    invoke-static {v7}, Lcom/pspdfkit/internal/di;->a([I)I

    move-result v5

    iput v5, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->maximumHeight:I

    const/high16 v5, 0x40000000    # 2.0f

    if-ne v3, v5, :cond_5

    .line 68
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getSuggestedMinimumWidth()I

    move-result v3

    invoke-static {v3, v1}, Landroid/view/View;->getDefaultSize(II)I

    move-result v1

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setMeasuredDimension(II)V

    goto :goto_4

    .line 71
    :cond_5
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-static {v2, v1}, Landroid/view/View;->getDefaultSize(II)I

    move-result v1

    .line 74
    iget-boolean v2, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->showingDetailView:Z

    if-eqz v2, :cond_6

    move v8, v6

    :cond_6
    add-int/2addr v4, v8

    iget v2, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->bottomInset:I

    add-int/2addr v4, v2

    .line 76
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getSuggestedMinimumHeight()I

    move-result v2

    .line 77
    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 78
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setMeasuredDimension(II)V

    :goto_4
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/ui/inspector/PropertyInspector$SavedState;

    if-nez v0, :cond_0

    .line 2
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 6
    :cond_0
    check-cast p1, Lcom/pspdfkit/ui/inspector/PropertyInspector$SavedState;

    .line 7
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 8
    iget-object v0, p1, Lcom/pspdfkit/ui/inspector/PropertyInspector$SavedState;->inspectorViewsState:Landroid/util/SparseArray;

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 9
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->getInspectorViewCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 11
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->getInspectorView(I)Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->isViewStateRestorationEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 12
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->getInspectorView(I)Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p1, Lcom/pspdfkit/ui/inspector/PropertyInspector$SavedState;->inspectorViewsState:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/inspector/PropertyInspector$SavedState;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/inspector/PropertyInspector$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 2
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector$SavedState;->inspectorViewsState:Landroid/util/SparseArray;

    const/4 v1, 0x0

    .line 3
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->getInspectorViewCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 5
    invoke-virtual {p0, v1}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->getInspectorView(I)Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->isViewStateRestorationEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6
    invoke-virtual {p0, v1}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->getInspectorView(I)Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getView()Landroid/view/View;

    move-result-object v2

    iget-object v3, v0, Lcom/pspdfkit/ui/inspector/PropertyInspector$SavedState;->inspectorViewsState:Landroid/util/SparseArray;

    invoke-virtual {v2, v3}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public removeAllInspectorViews()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->views:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    .line 2
    invoke-interface {v1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->onHidden()V

    .line 3
    invoke-interface {v1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->unbindController()V

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    invoke-interface {v1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->views:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const v0, 0x7fffffff

    .line 7
    iput v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->suggestedHeight:I

    return-void
.end method

.method public removeAllItemDecorations()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->itemDecorations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setWillNotDraw(Z)V

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public removeInspectorView(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V
    .locals 1

    .line 1
    invoke-interface {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->onHidden()V

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->unbindController()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->views:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    invoke-interface {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public removeItemDecoration(Lcom/pspdfkit/ui/inspector/PropertyInspector$ItemDecoration;)V
    .locals 2

    const-string v0, "decoration"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->itemDecorations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 55
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->itemDecorations:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 56
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setWillNotDraw(Z)V

    .line 58
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public reset()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->removeAllInspectorViews()V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->removeAllItemDecorations()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->activeDetailView:Landroid/view/View;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 4
    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->hideDetailView(Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    if-eqz v0, :cond_0

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->activeDetailView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    .line 8
    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->activeDetailView:Landroid/view/View;

    :cond_1
    return-void
.end method

.method setBottomInset(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->bottomInset:I

    if-ne v0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    iput p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->bottomInset:I

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public setCancelListener(Lcom/pspdfkit/ui/inspector/PropertyInspector$OnCancelListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->cancelListener:Lcom/pspdfkit/ui/inspector/PropertyInspector$OnCancelListener;

    return-void
.end method

.method public setCancelOnTouchOutside(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->cancelOnTouchOutside:Z

    return-void
.end method

.method public setInspectorViews(Ljava/util/List;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/inspector/PropertyInspectorView;",
            ">;Z)V"
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->showingDetailView:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->activeDetailView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->removeAllInspectorViews()V

    .line 3
    invoke-virtual {p0, p2}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->hideDetailView(Z)V

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->getInspectorViewCount()I

    move-result p2

    if-lez p2, :cond_1

    .line 5
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->createContainerLayout()Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->containerSwitcher:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 10
    invoke-static {p2}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    const/4 v1, 0x0

    .line 11
    invoke-virtual {v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    const-wide/16 v2, 0x12c

    .line 12
    invoke-virtual {v0, v2, v3}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 13
    invoke-virtual {v0, v4}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    new-instance v4, Lcom/pspdfkit/ui/inspector/PropertyInspector$$ExternalSyntheticLambda2;

    invoke-direct {v4, p0, p2}, Lcom/pspdfkit/ui/inspector/PropertyInspector$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/inspector/PropertyInspector;Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;)V

    .line 14
    invoke-virtual {v0, v4}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    .line 17
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 18
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorViewsContainer:Lcom/pspdfkit/ui/inspector/InspectorViewsContainer;

    invoke-static {p2}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    const/high16 v0, 0x3f800000    # 1.0f

    .line 19
    invoke-virtual {p2, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    .line 20
    invoke-virtual {p2, v2, v3}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 21
    invoke-virtual {p2, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    const p2, 0x7fffffff

    .line 23
    iput p2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->suggestedHeight:I

    .line 24
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->views:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 26
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->removeAllInspectorViews()V

    .line 29
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    .line 30
    invoke-virtual {p0, p2}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->addInspectorView(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    goto :goto_1

    .line 35
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorScrollView:Landroidx/core/widget/NestedScrollView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2, p2}, Landroidx/core/widget/NestedScrollView;->smoothScrollTo(II)V

    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(I)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    const-string v0, "title"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(Ljava/lang/String;)V

    return-void
.end method

.method public setTitleBarVisible(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public showDetailView(Landroid/view/View;Ljava/lang/String;Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->activeDetailView:Landroid/view/View;

    const/4 v1, 0x1

    if-ne v0, p1, :cond_0

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    if-nez v2, :cond_3

    :cond_0
    if-eqz v0, :cond_1

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    if-eqz v2, :cond_1

    .line 3
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    if-nez v0, :cond_2

    .line 8
    new-instance v0, Landroidx/core/widget/NestedScrollView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroidx/core/widget/NestedScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    .line 9
    invoke-virtual {v0, v1}, Landroidx/core/widget/NestedScrollView;->setFillViewport(Z)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 13
    :cond_2
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->activeDetailView:Landroid/view/View;

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    invoke-virtual {v0, p1}, Landroidx/core/widget/NestedScrollView;->addView(Landroid/view/View;)V

    .line 17
    :cond_3
    iput-boolean v1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->showingDetailView:Z

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    invoke-virtual {p1, v1}, Landroidx/core/widget/NestedScrollView;->setNestedScrollingEnabled(Z)V

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorScrollView:Landroidx/core/widget/NestedScrollView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/core/widget/NestedScrollView;->setNestedScrollingEnabled(Z)V

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->inspectorScrollView:Landroidx/core/widget/NestedScrollView;

    if-eqz p3, :cond_4

    .line 23
    sget-object v0, Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;->RIGHT_TO_LEFT:Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;->NONE:Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;

    .line 24
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->applyLeaveAnimation(Landroid/view/View;Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;)V

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->detailScrollView:Landroidx/core/widget/NestedScrollView;

    if-eqz p3, :cond_5

    sget-object v0, Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;->RIGHT_TO_LEFT:Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;

    goto :goto_1

    :cond_5
    sget-object v0, Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;->NONE:Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;

    :goto_1
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->applyEnterAnimation(Landroid/view/View;Lcom/pspdfkit/ui/inspector/PropertyInspector$DetailViewAnimation;)V

    .line 28
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p1, v1, p3}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b(ZZ)V

    if-eqz p2, :cond_6

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->propertyInspectorTitle:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setDetailTitle(Ljava/lang/String;)V

    .line 33
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/PropertyInspector;->activeDetailView:Landroid/view/View;

    instance-of p2, p1, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    if-eqz p2, :cond_7

    .line 34
    check-cast p1, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;

    .line 35
    invoke-interface {p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->onShown()V

    :cond_7
    return-void
.end method

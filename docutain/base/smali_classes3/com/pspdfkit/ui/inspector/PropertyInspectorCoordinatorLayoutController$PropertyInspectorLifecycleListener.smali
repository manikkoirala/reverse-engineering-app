.class public interface abstract Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PropertyInspectorLifecycleListener"
.end annotation


# virtual methods
.method public abstract onDisplayPropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
.end method

.method public abstract onPreparePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
.end method

.method public abstract onRemovePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
.end method

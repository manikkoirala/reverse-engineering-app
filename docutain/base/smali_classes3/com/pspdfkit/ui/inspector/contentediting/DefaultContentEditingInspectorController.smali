.class public Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;
.super Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$ContentEditingPropertyInspector;
    }
.end annotation


# instance fields
.field private contentEditingInspectorFactory:Lcom/pspdfkit/internal/i6;

.field private final context:Landroid/content/Context;

.field public controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

.field private final onContentEditingModeChangeListener:Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController;)V

    .line 7
    new-instance p2, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$1;

    invoke-direct {p2, p0}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$1;-><init>(Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;)V

    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->onContentEditingModeChangeListener:Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$ContentEditingPropertyInspector;

    move-result-object p2

    sget v0, Lcom/pspdfkit/R$id;->pspdf__content_editing_inspector:I

    invoke-virtual {p2, v0}, Landroid/view/View;->setId(I)V

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$ContentEditingPropertyInspector;

    move-result-object p2

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->setCancelOnTouchOutside(Z)V

    .line 10
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->context:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->cancel()V

    return-void
.end method

.method private applyControllerChanges(Ljava/util/List;Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;Lcom/pspdfkit/internal/jt;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/db;",
            ">;",
            "Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;",
            "Lcom/pspdfkit/internal/jt;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->isContentEditingInspectorVisible()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v0, :cond_8

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getActiveContentEditingStylingItem()Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 9
    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getActiveContentEditingStylingItem()Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;

    move-result-object v1

    .line 10
    sget v2, Lcom/pspdfkit/internal/ao;->a:I

    const-string v2, "context"

    .line 11
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "activeContentEditingStylingItem"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 441
    sget-object v2, Lcom/pspdfkit/internal/ao$a;->c:[I

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eq v1, v4, :cond_3

    if-eq v1, v3, :cond_2

    if-ne v1, v2, :cond_1

    .line 444
    sget v1, Lcom/pspdfkit/R$string;->pspdf__size:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.string.pspdf__size)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 445
    :cond_2
    sget v1, Lcom/pspdfkit/R$string;->pspdf__picker_font:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.string.pspdf__picker_font)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 446
    :cond_3
    sget v1, Lcom/pspdfkit/R$string;->pspdf__edit_menu_text_color:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026df__edit_menu_text_color)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 447
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$ContentEditingPropertyInspector;

    move-result-object v1

    .line 448
    iget-object v5, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    invoke-interface {v5}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getCurrentFormatter()Lcom/pspdfkit/internal/h6;

    move-result-object v5

    .line 450
    sget-object v6, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$2;->$SwitchMap$com$pspdfkit$ui$special_mode$controller$ContentEditingStylingBarItem:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v6, p2

    if-eq p2, v4, :cond_6

    if-eq p2, v3, :cond_5

    if-eq p2, v2, :cond_4

    goto :goto_1

    .line 468
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->contentEditingInspectorFactory:Lcom/pspdfkit/internal/i6;

    .line 469
    invoke-virtual {p1, v5, p3}, Lcom/pspdfkit/internal/i6;->a(Lcom/pspdfkit/internal/h6;Lcom/pspdfkit/internal/jt;)Landroid/view/View;

    move-result-object p1

    .line 470
    invoke-virtual {v1, p1, v0, v4}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->showDetailView(Landroid/view/View;Ljava/lang/String;Z)V

    goto :goto_1

    .line 471
    :cond_5
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->contentEditingInspectorFactory:Lcom/pspdfkit/internal/i6;

    .line 472
    invoke-virtual {p1, v5, p3}, Lcom/pspdfkit/internal/i6;->b(Lcom/pspdfkit/internal/h6;Lcom/pspdfkit/internal/jt;)Lcom/pspdfkit/ui/inspector/ContentEditingFontSizesPickerView;

    move-result-object p1

    .line 473
    invoke-virtual {v1, p1, v0, v4}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->showDetailView(Landroid/view/View;Ljava/lang/String;Z)V

    goto :goto_1

    :cond_6
    if-nez p1, :cond_7

    return-void

    .line 474
    :cond_7
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->contentEditingInspectorFactory:Lcom/pspdfkit/internal/i6;

    .line 475
    invoke-virtual {p2, p1, v5, p3}, Lcom/pspdfkit/internal/i6;->a(Ljava/util/List;Lcom/pspdfkit/internal/h6;Lcom/pspdfkit/internal/jt;)Lcom/pspdfkit/ui/inspector/views/ContentEditingFontNamesPickerView;

    move-result-object p1

    .line 476
    invoke-virtual {v1, p1, v0, v4}, Lcom/pspdfkit/ui/inspector/PropertyInspector;->showDetailView(Landroid/view/View;Ljava/lang/String;Z)V

    :goto_1
    return-void

    .line 477
    :cond_8
    :goto_2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->cancel()V

    return-void
.end method

.method private toggleInspectorVisibility(ZLjava/util/List;Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;Lcom/pspdfkit/internal/jt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/db;",
            ">;",
            "Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;",
            "Lcom/pspdfkit/internal/jt;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->isInspectorVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->hideInspector(Z)V

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->showInspector(Z)V

    .line 5
    invoke-direct {p0, p2, p3, p4}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->applyControllerChanges(Ljava/util/List;Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;Lcom/pspdfkit/internal/jt;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public bindContentEditingController(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->unbindContentEditingController()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/i6;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/i6;-><init>(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->contentEditingInspectorFactory:Lcom/pspdfkit/internal/i6;

    .line 5
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getContentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->onContentEditingModeChangeListener:Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;

    .line 6
    invoke-interface {v0, v1}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;->addOnContentEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;)V

    .line 7
    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->bindContentEditingInspectorController(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;)V

    return-void
.end method

.method protected createPropertyInspector(Landroid/content/Context;)Lcom/pspdfkit/ui/inspector/PropertyInspector;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$ContentEditingPropertyInspector;

    invoke-direct {v0, p1}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$ContentEditingPropertyInspector;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public displayColorPicker(ZLcom/pspdfkit/internal/jt;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;->FONT_COLOR:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0, p2}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->toggleInspectorVisibility(ZLjava/util/List;Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;Lcom/pspdfkit/internal/jt;)V

    return-void
.end method

.method public displayFontNamesSheet(ZLjava/util/List;Lcom/pspdfkit/internal/jt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/db;",
            ">;",
            "Lcom/pspdfkit/internal/jt;",
            ")V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;->FONT_NAME:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->toggleInspectorVisibility(ZLjava/util/List;Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;Lcom/pspdfkit/internal/jt;)V

    return-void
.end method

.method public displayFontSizesSheet(ZLcom/pspdfkit/internal/jt;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;->FONT_SIZE:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0, p2}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->toggleInspectorVisibility(ZLjava/util/List;Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;Lcom/pspdfkit/internal/jt;)V

    return-void
.end method

.method protected bridge synthetic getPropertyInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$ContentEditingPropertyInspector;

    move-result-object v0

    return-object v0
.end method

.method protected getPropertyInspector()Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$ContentEditingPropertyInspector;
    .locals 1

    .line 2
    invoke-super {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->getPropertyInspector()Lcom/pspdfkit/ui/inspector/PropertyInspector;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$ContentEditingPropertyInspector;

    return-object v0
.end method

.method protected isBoundToController()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isContentEditingInspectorVisible()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->isInspectorVisible()Z

    move-result v0

    return v0
.end method

.method public onDisplayPropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->onDisplayPropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;->onDisplayPropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    :cond_0
    return-void
.end method

.method public onRemovePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->onRemovePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;->onRemovePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    :cond_0
    return-void
.end method

.method public unbindContentEditingController()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->unbindContentEditingInspectorController()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;->getContentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->onContentEditingModeChangeListener:Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;

    .line 5
    invoke-interface {v0, v1}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;->removeOnContentEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;)V

    const/4 v0, 0x0

    .line 6
    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;->controller:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 8
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->cancel()V

    return-void
.end method

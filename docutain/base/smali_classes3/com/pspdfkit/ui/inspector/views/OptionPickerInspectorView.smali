.class public final Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$ViewHolder;,
        Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OptionsAdapter;,
        Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OnOptionPickedListener;
    }
.end annotation


# static fields
.field private static final CHECKBOX_SIZE_DP:I = 0x18

.field private static final MIN_INSPECTOR_ITEMS_COUNT:I = 0x3

.field private static final SUGGESTED_INSPECTOR_ITEMS_COUNT:I = 0x4


# instance fields
.field private customValue:Ljava/lang/String;

.field private customValueEditText:Lcom/pspdfkit/ui/LocalizedEditText;

.field private customValueEditTextDrawable:Landroid/graphics/drawable/Drawable;

.field private customValueFilters:[Landroid/text/InputFilter;

.field private customValueInputType:I

.field private customValueLayout:Landroid/view/View;

.field private final isEditable:Z

.field private final isMultiSelectEnabled:Z

.field private itemHeight:I

.field private final listener:Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OnOptionPickedListener;

.field private maxHeight:I

.field private final options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private optionsAdapter:Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OptionsAdapter;

.field private optionsContainer:Landroidx/recyclerview/widget/RecyclerView;

.field private originalSoftInputMode:I

.field private searchView:Landroid/widget/EditText;

.field private final selectedOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetcustomValue(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValue:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetcustomValueEditText(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;)Lcom/pspdfkit/ui/LocalizedEditText;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditText:Lcom/pspdfkit/ui/LocalizedEditText;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetcustomValueEditTextDrawable(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;)Landroid/graphics/drawable/Drawable;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditTextDrawable:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetcustomValueFilters(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;)[Landroid/text/InputFilter;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueFilters:[Landroid/text/InputFilter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetcustomValueInputType(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueInputType:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetcustomValueLayout(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueLayout:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetisEditable(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->isEditable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetoptions(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->options:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetoptionsAdapter(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;)Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OptionsAdapter;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->optionsAdapter:Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OptionsAdapter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetselectedOptions(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->selectedOptions:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputcustomValueEditText(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;Lcom/pspdfkit/ui/LocalizedEditText;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditText:Lcom/pspdfkit/ui/LocalizedEditText;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputcustomValueEditTextDrawable(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditTextDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputcustomValueLayout(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueLayout:Landroid/view/View;

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCustomEditTextDrawable(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->updateCustomEditTextDrawable()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;ZZLjava/lang/String;Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OnOptionPickedListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;ZZ",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OnOptionPickedListener;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->selectedOptions:Ljava/util/List;

    const/4 v0, 0x1

    .line 21
    iput v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueInputType:I

    const/4 v0, 0x0

    .line 26
    iput v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->maxHeight:I

    const-string v0, "options"

    .line 49
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultSelectedOptions"

    .line 50
    invoke-static {p3, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->options:Ljava/util/List;

    .line 52
    iput-object p7, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OnOptionPickedListener;

    .line 54
    iput-boolean p4, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->isMultiSelectEnabled:Z

    .line 55
    iput-boolean p5, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->isEditable:Z

    .line 57
    invoke-direct {p0, p1, p3, p6}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->init(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method private clearCustomValueText()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditText:Lcom/pspdfkit/ui/LocalizedEditText;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditText:Lcom/pspdfkit/ui/LocalizedEditText;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    :cond_0
    return-void
.end method

.method private getCustomValueLayoutHeight()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private getSearchViewHeight()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->searchView:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private init(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->selectedOptions:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValue:Ljava/lang/String;

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object p2

    .line 7
    invoke-virtual {p2}, Lcom/pspdfkit/internal/fo;->c()I

    move-result p3

    iput p3, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->itemHeight:I

    .line 9
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p3

    .line 10
    sget v0, Lcom/pspdfkit/R$layout;->pspdf__option_picker_inspector_view:I

    const/4 v1, 0x1

    invoke-virtual {p3, v0, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 11
    sget p3, Lcom/pspdfkit/R$id;->pspdf__options_layout:I

    invoke-virtual {p0, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p3, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->optionsContainer:Landroidx/recyclerview/widget/RecyclerView;

    .line 12
    invoke-virtual {p3, v1}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 13
    new-instance p3, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OptionsAdapter;

    invoke-direct {p3, p0}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OptionsAdapter;-><init>(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;)V

    iput-object p3, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->optionsAdapter:Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OptionsAdapter;

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->optionsContainer:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p3}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 15
    iget-object p3, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->optionsContainer:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {p3, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 17
    invoke-virtual {p2}, Lcom/pspdfkit/internal/fo;->h()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 18
    sget p3, Lcom/pspdfkit/R$id;->pspdf__search_edit_text_inline:I

    invoke-virtual {p0, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/EditText;

    iput-object p3, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->searchView:Landroid/widget/EditText;

    .line 19
    invoke-virtual {p3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 20
    iget-object p3, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->searchView:Landroid/widget/EditText;

    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p3

    check-cast p3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 23
    invoke-virtual {p2}, Lcom/pspdfkit/internal/fo;->b()I

    move-result v0

    const/4 v3, 0x4

    int-to-float v3, v3

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 25
    invoke-static {v1, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v0, v4

    .line 26
    invoke-virtual {p2}, Lcom/pspdfkit/internal/fo;->b()I

    move-result v4

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    .line 28
    invoke-static {v1, v3, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p1

    float-to-int p1, p1

    sub-int/2addr v4, p1

    .line 29
    invoke-virtual {p3, v0, v2, v4, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 34
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->searchView:Landroid/widget/EditText;

    invoke-virtual {p1, p3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 35
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->searchView:Landroid/widget/EditText;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/fo;->c()I

    move-result p3

    invoke-virtual {p1, p3}, Landroid/view/View;->setMinimumHeight(I)V

    .line 36
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->searchView:Landroid/widget/EditText;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/fo;->f()F

    move-result p3

    invoke-virtual {p1, v2, p3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 37
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->searchView:Landroid/widget/EditText;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/fo;->e()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 38
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->searchView:Landroid/widget/EditText;

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 39
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->searchView:Landroid/widget/EditText;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 40
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->searchView:Landroid/widget/EditText;

    const/16 p2, 0xb1

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setInputType(I)V

    .line 41
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->searchView:Landroid/widget/EditText;

    const/4 p2, 0x3

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 42
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->searchView:Landroid/widget/EditText;

    new-instance p2, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$1;

    invoke-direct {p2, p0}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$1;-><init>(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;)V

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_0
    return-void
.end method

.method private isSelected(I)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->options:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->selectedOptions:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private onSelectedOptionsChanged()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OnOptionPickedListener;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->getSelectedOptions()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OnOptionPickedListener;->onOptionsSelected(Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method private setSelectedOption(IZZ)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->options:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-ge p1, v0, :cond_4

    if-gez p1, :cond_0

    goto :goto_1

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->selectedOptions:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eq v0, p2, :cond_1

    const/4 v1, 0x1

    :cond_1
    if-eqz v1, :cond_2

    if-eqz p2, :cond_2

    .line 7
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->selectedOptions:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    if-nez p2, :cond_3

    .line 9
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->selectedOptions:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 12
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->optionsAdapter:Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OptionsAdapter;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    if-eqz v1, :cond_4

    if-eqz p3, :cond_4

    .line 15
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->onSelectedOptionsChanged()V

    :cond_4
    :goto_1
    return v1
.end method

.method private updateCustomEditTextDrawable()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditText:Lcom/pspdfkit/ui/LocalizedEditText;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditTextDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 2
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditText:Lcom/pspdfkit/ui/LocalizedEditText;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditText:Lcom/pspdfkit/ui/LocalizedEditText;

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditTextDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v1, v2, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 0

    return-void
.end method

.method public getCustomValue()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditText:Lcom/pspdfkit/ui/LocalizedEditText;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->maxHeight:I

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->maxHeight:I

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->itemHeight:I

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x3

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    mul-int v1, v1, v0

    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->getSearchViewHeight()I

    move-result v0

    add-int/2addr v1, v0

    return v1
.end method

.method public getSelectedOptions()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->selectedOptions:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getSuggestedHeight()I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->optionsContainer:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->itemHeight:I

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->options:Ljava/util/List;

    .line 3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    mul-int v2, v2, v1

    .line 4
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->getCustomValueLayoutHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 8
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->getSearchViewHeight()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic isViewStateRestorationEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$isViewStateRestorationEnabled(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->optionsAdapter:Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OptionsAdapter;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->optionsContainer:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->getPosition(Landroid/view/View;)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OptionsAdapter;->getItemId(I)J

    move-result-wide v0

    long-to-int p1, v0

    if-gez p1, :cond_0

    return-void

    .line 4
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->isMultiSelectEnabled:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 5
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->isSelected(I)Z

    move-result v0

    xor-int/2addr v0, v1

    invoke-direct {p0, p1, v0, v1}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->setSelectedOption(IZZ)Z

    goto :goto_0

    .line 7
    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1, v1}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->setSelectedOptions(Ljava/util/List;Z)V

    :goto_0
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1

    if-eqz p2, :cond_0

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/16 p2, 0x10

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/pg;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->originalSoftInputMode:I

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    iget v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->originalSoftInputMode:I

    invoke-static {p2, v0}, Lcom/pspdfkit/internal/pg;->a(Landroid/content/Context;I)I

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    :goto_0
    return-void
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .line 1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    if-lez v0, :cond_0

    const/high16 p2, -0x80000000

    .line 6
    invoke-static {v0, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 8
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValue:Ljava/lang/String;

    .line 3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    const/4 p4, 0x1

    if-eqz p3, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    :cond_1
    const/4 p2, 0x1

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_3

    return-void

    .line 4
    :cond_3
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValue:Ljava/lang/String;

    .line 6
    iget-boolean p2, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->isMultiSelectEnabled:Z

    if-nez p2, :cond_4

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_4

    .line 7
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p0, p2, p4}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->setSelectedOptions(Ljava/util/List;Z)V

    .line 9
    :cond_4
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->updateCustomEditTextDrawable()V

    .line 11
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OnOptionPickedListener;

    if-eqz p2, :cond_5

    .line 12
    invoke-interface {p2, p1}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView$OnOptionPickedListener;->onCustomValueChanged(Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method public setCustomValue(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditText:Lcom/pspdfkit/ui/LocalizedEditText;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValue:Ljava/lang/String;

    .line 2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditText:Lcom/pspdfkit/ui/LocalizedEditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method

.method public setFilters([Landroid/text/InputFilter;)V
    .locals 2

    const-string v0, "filters"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueFilters:[Landroid/text/InputFilter;

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditText:Lcom/pspdfkit/ui/LocalizedEditText;

    if-nez v0, :cond_0

    return-void

    .line 57
    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method

.method public setInputType(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueInputType:I

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->customValueEditText:Lcom/pspdfkit/ui/LocalizedEditText;

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->setInputType(I)V

    return-void
.end method

.method public setSelectedOptions(Ljava/util/List;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "selectedOptions"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-boolean v0, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->isMultiSelectEnabled:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 55
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->options:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 56
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 57
    invoke-direct {p0, v0, v3, v1}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->setSelectedOption(IZZ)Z

    move-result v3

    or-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    goto :goto_1

    :cond_1
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 61
    :goto_2
    iget-object v4, p0, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->options:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_3

    if-ne v3, v0, :cond_2

    const/4 v4, 0x1

    goto :goto_3

    :cond_2
    const/4 v4, 0x0

    .line 62
    :goto_3
    invoke-direct {p0, v3, v4, v1}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->setSelectedOption(IZZ)Z

    move-result v4

    or-int/2addr v2, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 64
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_4

    .line 65
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->clearCustomValueText()V

    :cond_4
    if-eqz v2, :cond_5

    if-eqz p2, :cond_5

    .line 69
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/OptionPickerInspectorView;->onSelectedOptionsChanged()V

    :cond_5
    return-void
.end method

.method public unbindController()V
    .locals 0

    return-void
.end method

.class public Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;
.super Landroid/view/View;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;


# instance fields
.field protected final annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

.field private final annotationType:Lcom/pspdfkit/annotations/AnnotationType;

.field private final drawnShape:Lcom/pspdfkit/internal/y4;

.field private final fillPaint:Landroid/graphics/Paint;

.field private final inspectorStyle:Lcom/pspdfkit/internal/fo;

.field private final paint:Landroid/graphics/Paint;

.field private final unscaledPageToViewTransformation:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/annotations/AnnotationType;Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->unscaledPageToViewTransformation:Landroid/graphics/Matrix;

    const-string v0, "annotationType"

    .line 22
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationCreationController"

    .line 23
    invoke-static {p3, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iput-object p3, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 25
    invoke-static {p1}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->inspectorStyle:Lcom/pspdfkit/internal/fo;

    .line 26
    invoke-static {}, Lcom/pspdfkit/internal/h4;->i()Landroid/graphics/Paint;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->paint:Landroid/graphics/Paint;

    .line 27
    invoke-static {}, Lcom/pspdfkit/internal/h4;->h()Landroid/graphics/Paint;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->fillPaint:Landroid/graphics/Paint;

    .line 28
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationType:Lcom/pspdfkit/annotations/AnnotationType;

    .line 30
    sget-object p3, Lcom/pspdfkit/annotations/AnnotationType;->LINE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne p2, p3, :cond_0

    .line 31
    new-instance p2, Lcom/pspdfkit/internal/ih;

    invoke-direct {p2}, Lcom/pspdfkit/internal/ih;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    goto :goto_2

    .line 32
    :cond_0
    sget-object p3, Lcom/pspdfkit/annotations/AnnotationType;->CIRCLE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq p2, p3, :cond_4

    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->SQUARE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne p2, v0, :cond_1

    goto :goto_0

    .line 35
    :cond_1
    sget-object p3, Lcom/pspdfkit/annotations/AnnotationType;->POLYGON:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne p2, p3, :cond_2

    .line 36
    new-instance p2, Lcom/pspdfkit/internal/pn;

    invoke-direct {p2}, Lcom/pspdfkit/internal/pn;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    goto :goto_2

    .line 37
    :cond_2
    sget-object p3, Lcom/pspdfkit/annotations/AnnotationType;->POLYLINE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne p2, p3, :cond_3

    .line 38
    new-instance p2, Lcom/pspdfkit/internal/sn;

    invoke-direct {p2}, Lcom/pspdfkit/internal/sn;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    goto :goto_2

    .line 40
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    const-string v0, "Unsupported annotation type for preview: "

    invoke-direct {p3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 41
    :cond_4
    :goto_0
    new-instance v0, Lcom/pspdfkit/internal/ds;

    if-ne p2, p3, :cond_5

    .line 42
    sget-object p2, Lcom/pspdfkit/internal/ds$a;->b:Lcom/pspdfkit/internal/ds$a;

    goto :goto_1

    :cond_5
    sget-object p2, Lcom/pspdfkit/internal/ds$a;->a:Lcom/pspdfkit/internal/ds$a;

    :goto_1
    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/ds;-><init>(Lcom/pspdfkit/internal/ds$a;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    .line 51
    :goto_2
    new-instance p2, Landroid/view/ViewGroup$LayoutParams;

    .line 52
    invoke-virtual {p1}, Lcom/pspdfkit/internal/fo;->d()I

    move-result p1

    const/4 p3, -0x2

    invoke-direct {p2, p3, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 53
    invoke-virtual {p0, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private refreshAnnotationCreationParams()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h4;->a(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getThickness()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h4;->b(F)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getBorderStylePreset()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/y4;->a(Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getFillColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h4;->b(I)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAlpha()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h4;->a(F)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationType:Lcom/pspdfkit/annotations/AnnotationType;

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->LINE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->POLYLINE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_1

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    check-cast v0, Lcom/pspdfkit/internal/sn;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/sn;->a(Landroidx/core/util/Pair;)V

    .line 12
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 13
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getThickness()F

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->unscaledPageToViewTransformation:Landroid/graphics/Matrix;

    .line 14
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v0

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->inspectorStyle:Lcom/pspdfkit/internal/fo;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/fo;->b()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    add-float/2addr v1, v0

    float-to-int v1, v1

    .line 17
    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->inspectorStyle:Lcom/pspdfkit/internal/fo;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/fo;->g()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v0

    float-to-int v0, v2

    .line 18
    invoke-virtual {p0, v1, v0, v1, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->unscaledPageToViewTransformation:Landroid/graphics/Matrix;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/h4;->a(FLandroid/graphics/Matrix;)Z

    .line 21
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->unscaledPageToViewTransformation:Landroid/graphics/Matrix;

    .line 3
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/go;->a(Lcom/pspdfkit/ui/PdfFragment;Landroid/graphics/Matrix;)V

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->refreshAnnotationCreationParams()V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->addOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    return-void
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic isViewStateRestorationEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$isViewStateRestorationEnabled(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)Z

    move-result v0

    return v0
.end method

.method public onAnnotationCreationModeSettingsChange(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->refreshAnnotationCreationParams()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->paint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1, v2}, Lcom/pspdfkit/internal/h4;->b(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    return-void
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 11

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getSuggestedMinimumWidth()I

    move-result p2

    invoke-static {p2, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result p1

    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->inspectorStyle:Lcom/pspdfkit/internal/fo;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/fo;->d()I

    move-result p2

    .line 2
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    .line 8
    sget-object v0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView$1;->$SwitchMap$com$pspdfkit$annotations$AnnotationType:[I

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationType:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-eq v0, v2, :cond_3

    if-eq v0, v3, :cond_2

    const/4 v4, 0x3

    if-eq v0, v4, :cond_2

    const/4 v5, 0x4

    if-eq v0, v5, :cond_1

    const/4 v6, 0x5

    if-eq v0, v6, :cond_0

    goto/16 :goto_0

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    check-cast v0, Lcom/pspdfkit/internal/sn;

    new-array v5, v5, [Landroid/graphics/PointF;

    new-instance v7, Landroid/graphics/PointF;

    .line 31
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v8

    int-to-float v8, v8

    div-int/lit8 v9, p2, 0x2

    int-to-float v9, v9

    invoke-direct {v7, v8, v9}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v7, v5, v1

    new-instance v1, Landroid/graphics/PointF;

    div-int/lit8 v7, p1, 0x3

    int-to-float v7, v7

    mul-int/lit8 v8, p2, 0x4

    div-int/2addr v8, v6

    int-to-float v8, v8

    invoke-direct {v1, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v5, v2

    new-instance v1, Landroid/graphics/PointF;

    mul-int/lit8 v2, p1, 0x2

    div-int/2addr v2, v4

    int-to-float v2, v2

    div-int/2addr p2, v6

    int-to-float p2, p2

    invoke-direct {v1, v2, p2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v5, v3

    new-instance p2, Landroid/graphics/PointF;

    .line 34
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    sub-int/2addr p1, v1

    int-to-float p1, p1

    invoke-direct {p2, p1, v9}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object p2, v5, v4

    .line 35
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 36
    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/d4;->b(Ljava/util/List;)V

    goto/16 :goto_0

    .line 37
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    check-cast v0, Lcom/pspdfkit/internal/pn;

    new-array v6, v5, [Landroid/graphics/PointF;

    new-instance v7, Landroid/graphics/PointF;

    .line 39
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v8

    int-to-float v8, v8

    int-to-float v9, p2

    const/high16 v10, 0x3fc00000    # 1.5f

    mul-float v9, v9, v10

    invoke-direct {v7, v8, v9}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v7, v6, v1

    new-instance v1, Landroid/graphics/PointF;

    .line 40
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v7

    div-int/lit8 v8, p1, 0x6

    add-int/2addr v7, v8

    int-to-float v7, v7

    div-int/2addr p2, v5

    int-to-float p2, p2

    invoke-direct {v1, v7, p2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v6, v2

    new-instance v1, Landroid/graphics/PointF;

    .line 41
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    sub-int v2, p1, v2

    sub-int/2addr v2, v8

    int-to-float v2, v2

    invoke-direct {v1, v2, p2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v6, v3

    new-instance p2, Landroid/graphics/PointF;

    .line 42
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    sub-int/2addr p1, v1

    int-to-float p1, p1

    invoke-direct {p2, p1, v9}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object p2, v6, v4

    .line 43
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 44
    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/d4;->b(Ljava/util/List;)V

    goto :goto_0

    .line 45
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    check-cast v0, Lcom/pspdfkit/internal/ds;

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    div-int/lit8 v2, p2, 0x4

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    sub-int/2addr p1, v4

    int-to-float p1, p1

    mul-int/lit8 p2, p2, 0x2

    int-to-float p2, p2

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/pspdfkit/internal/ds;->a(FFFF)V

    goto :goto_0

    .line 46
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->drawnShape:Lcom/pspdfkit/internal/y4;

    check-cast v0, Lcom/pspdfkit/internal/ih;

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    int-to-float v4, v4

    div-int/2addr p2, v3

    int-to-float p2, p2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    sub-int/2addr p1, v5

    int-to-float p1, p1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-array v3, v3, [Landroid/graphics/PointF;

    .line 47
    new-instance v5, Landroid/graphics/PointF;

    invoke-direct {v5, v4, p2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v5, v3, v1

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v3, v2

    .line 48
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/d4;->b(Ljava/util/List;)V

    :goto_0
    return-void
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public unbindController()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->removeOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    return-void
.end method

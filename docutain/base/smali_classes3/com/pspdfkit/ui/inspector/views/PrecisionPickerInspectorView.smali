.class public Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$PrecisionPickerListener;
    }
.end annotation


# instance fields
.field private final label:Ljava/lang/String;

.field listener:Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$PrecisionPickerListener;

.field private spinner:Landroid/widget/Spinner;

.field private spinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private spinnerText:Landroid/widget/TextView;


# direct methods
.method static bridge synthetic -$$Nest$fgetspinnerAdapter(Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;)Landroid/widget/ArrayAdapter;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinnerAdapter:Landroid/widget/ArrayAdapter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetspinnerText(Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinnerText:Landroid/widget/TextView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$smstringFromPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->stringFromPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/pspdfkit/annotations/measurements/FloatPrecision;Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$PrecisionPickerListener;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const-string p1, "label"

    .line 2
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "defaultValue"

    .line 3
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->label:Ljava/lang/String;

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$PrecisionPickerListener;

    .line 7
    invoke-direct {p0, p3}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->init(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    return-void
.end method

.method private init(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object v0

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__view_inspector_precision_picker:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setMinimumHeight(I)V

    .line 7
    sget v2, Lcom/pspdfkit/R$id;->pspdf__label:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 8
    iget-object v3, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->label:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v0

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 12
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v4, -0x2

    invoke-direct {v0, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 16
    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->prepareSpinner(Lcom/pspdfkit/annotations/measurements/FloatPrecision;Landroid/view/View;)V

    .line 18
    invoke-virtual {p0, p1, v3}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->setPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;Z)V

    return-void
.end method

.method private prepareSpinner(Lcom/pspdfkit/annotations/measurements/FloatPrecision;Landroid/view/View;)V
    .locals 5

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__precision_spinner:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinner:Landroid/widget/Spinner;

    .line 2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__precision_spinner_text:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinnerText:Landroid/widget/TextView;

    .line 3
    new-instance p2, Landroid/widget/ArrayAdapter;

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__inspector_precision_spinner_item:I

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, Lcom/pspdfkit/annotations/measurements/FloatPrecision;->WHOLE:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    .line 5
    invoke-static {v3}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->stringFromPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    sget-object v3, Lcom/pspdfkit/annotations/measurements/FloatPrecision;->ONE_DP:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    .line 6
    invoke-static {v3}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->stringFromPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    sget-object v3, Lcom/pspdfkit/annotations/measurements/FloatPrecision;->TWO_DP:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    .line 7
    invoke-static {v3}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->stringFromPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    sget-object v3, Lcom/pspdfkit/annotations/measurements/FloatPrecision;->THREE_DP:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    .line 8
    invoke-static {v3}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->stringFromPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v2, v4

    sget-object v3, Lcom/pspdfkit/annotations/measurements/FloatPrecision;->FOUR_DP:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    .line 9
    invoke-static {v3}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->stringFromPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    aput-object v3, v2, v4

    invoke-direct {p2, v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinnerAdapter:Landroid/widget/ArrayAdapter;

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 12
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinner:Landroid/widget/Spinner;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 13
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__measurement_precision_spinner_dropdown_horizontal_offset:I

    .line 14
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 15
    invoke-virtual {p2, v0}, Landroid/widget/Spinner;->setDropDownHorizontalOffset(I)V

    .line 20
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->stringFromPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result p1

    .line 21
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinner:Landroid/widget/Spinner;

    invoke-virtual {p2, p1}, Landroid/widget/AdapterView;->setSelection(I)V

    .line 23
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinner:Landroid/widget/Spinner;

    new-instance p2, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$1;

    invoke-direct {p2, p0}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$1;-><init>(Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;)V

    invoke-virtual {p1, p2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 37
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinnerText:Landroid/widget/TextView;

    new-instance p2, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private static stringFromPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$2;->$SwitchMap$com$pspdfkit$annotations$measurements$FloatPrecision:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const-string p0, ""

    return-object p0

    :cond_0
    const-string p0, "0.0001"

    return-object p0

    :cond_1
    const-string p0, "0.001"

    return-object p0

    :cond_2
    const-string p0, "0.01"

    return-object p0

    :cond_3
    const-string p0, "0.1"

    return-object p0

    :cond_4
    const-string p0, "1"

    return-object p0
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 0

    return-void
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic isViewStateRestorationEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$isViewStateRestorationEnabled(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)Z

    move-result v0

    return v0
.end method

.method synthetic lambda$prepareSpinner$0$com-pspdfkit-ui-inspector-views-PrecisionPickerInspectorView(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinner:Landroid/widget/Spinner;

    invoke-virtual {p1}, Landroid/widget/Spinner;->performClick()Z

    return-void
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public setPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->stringFromPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->spinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/AdapterView;->setSelection(I)V

    if-eqz p2, :cond_0

    .line 3
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$PrecisionPickerListener;

    if-eqz p2, :cond_0

    .line 4
    invoke-interface {p2, p1}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$PrecisionPickerListener;->onPrecisionPicked(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    :cond_0
    return-void
.end method

.method public unbindController()V
    .locals 0

    return-void
.end method

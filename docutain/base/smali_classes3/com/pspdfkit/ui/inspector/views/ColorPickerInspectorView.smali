.class public Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$SavedState;,
        Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;,
        Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;
    }
.end annotation


# static fields
.field private static final COLOR_CIRCLE_BORDER_WIDTH_DP:I = 0x1

.field private static final COLOR_CIRCLE_RADIUS_DP:I = 0x8


# instance fields
.field private final availableDrawingColors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private colorView:Landroid/widget/ImageView;

.field private controller:Lcom/pspdfkit/ui/inspector/PropertyInspectorController;

.field private detailView:Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;

.field private final label:Ljava/lang/String;

.field listener:Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;I",
            "Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;",
            "Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;",
            ")V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const-string p1, "label"

    .line 4
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "colors"

    .line 5
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    iput-object p6, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;

    .line 8
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->availableDrawingColors:Ljava/util/List;

    .line 9
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->label:Ljava/lang/String;

    .line 11
    invoke-direct {p0, p4}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->init(I)V

    if-nez p5, :cond_0

    .line 15
    new-instance p5, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;

    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    const/4 p3, 0x0

    invoke-direct {p5, p2, p1, p4, p3}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;IZ)V

    .line 18
    :cond_0
    invoke-virtual {p0, p5}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->setColorPickerDetailView(Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;I",
            "Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;",
            ")V"
        }
    .end annotation

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p5

    .line 2
    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;[IILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V
    .locals 6

    .line 1
    invoke-static {p3}, Lcom/pspdfkit/internal/fv;->a([I)Ljava/util/ArrayList;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V

    return-void
.end method

.method private init(I)V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object v0

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__view_inspector_color_picker:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setMinimumHeight(I)V

    .line 7
    sget v2, Lcom/pspdfkit/R$id;->pspdf__label:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 8
    sget v3, Lcom/pspdfkit/R$id;->pspdf__color:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->colorView:Landroid/widget/ImageView;

    .line 9
    iget-object v3, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->label:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 12
    invoke-virtual {p0, p1, v4}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->setColor(IZ)V

    .line 14
    sget p1, Lcom/pspdfkit/R$id;->pspdf__expand_icon:I

    invoke-virtual {v1, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$drawable;->pspdf__ic_chevron_right:I

    invoke-static {v2, v3}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 17
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v0

    invoke-static {v2, v0}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 18
    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 20
    new-instance p1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v2, -0x2

    invoke-direct {p1, v0, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 25
    new-instance p1, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$$ExternalSyntheticLambda2;

    invoke-direct {p1, p0}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;)V

    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private showDetailView(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->controller:Lcom/pspdfkit/ui/inspector/PropertyInspectorController;

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->detailView:Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;

    invoke-interface {v1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->label:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorController;->showDetailView(Landroid/view/View;Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->controller:Lcom/pspdfkit/ui/inspector/PropertyInspectorController;

    return-void
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public isViewStateRestorationEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method synthetic lambda$init$0$com-pspdfkit-ui-inspector-views-ColorPickerInspectorView(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->showDetailView(Z)V

    return-void
.end method

.method synthetic lambda$onRestoreInstanceState$2$com-pspdfkit-ui-inspector-views-ColorPickerInspectorView()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->showDetailView(Z)V

    return-void
.end method

.method synthetic lambda$setColorPickerDetailView$1$com-pspdfkit-ui-inspector-views-ColorPickerInspectorView(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 0

    const/4 p1, 0x1

    .line 1
    invoke-virtual {p0, p2, p1}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->setColor(IZ)V

    return-void
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->detailView:Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;

    invoke-interface {v0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 2
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$SavedState;

    if-nez v0, :cond_0

    .line 2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 6
    :cond_0
    check-cast p1, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$SavedState;

    .line 7
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 8
    iget-boolean v0, p1, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$SavedState;->isDetailViewVisible:Z

    if-eqz v0, :cond_1

    .line 9
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;)V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 11
    :cond_1
    iget-object p1, p1, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$SavedState;->detailViewState:Landroid/os/Parcelable;

    if-eqz p1, :cond_2

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->detailView:Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;->setState(Landroid/os/Parcelable;)V

    :cond_2
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$SavedState;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->controller:Lcom/pspdfkit/ui/inspector/PropertyInspectorController;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorController;->getVisibleDetailView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->detailView:Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, v0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$SavedState;->isDetailViewVisible:Z

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->detailView:Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;

    invoke-interface {v1}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;->getState()Landroid/os/Parcelable;

    move-result-object v1

    iput-object v1, v0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$SavedState;->detailViewState:Landroid/os/Parcelable;

    return-object v0
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public setColor(IZ)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->colorView:Landroid/widget/ImageView;

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/s5;->b(I)I

    move-result v2

    const/high16 v4, 0x41000000    # 8.0f

    const/high16 v5, 0x41000000    # 8.0f

    const/high16 v6, 0x3f800000    # 1.0f

    move v3, p1

    .line 4
    invoke-static/range {v1 .. v6}, Lcom/pspdfkit/internal/o5;->a(Landroid/content/Context;IIFFF)Lcom/pspdfkit/internal/o5;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    if-eqz p2, :cond_0

    .line 11
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;

    if-eqz p2, :cond_0

    .line 12
    invoke-interface {p2, p0, p1}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;->onColorPicked(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    :cond_0
    return-void
.end method

.method public setColorPickerDetailView(Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->detailView:Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;

    .line 2
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;)V

    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;->setOnColorPickedListener(Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V

    return-void
.end method

.method public unbindController()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;->controller:Lcom/pspdfkit/ui/inspector/PropertyInspectorController;

    return-void
.end method

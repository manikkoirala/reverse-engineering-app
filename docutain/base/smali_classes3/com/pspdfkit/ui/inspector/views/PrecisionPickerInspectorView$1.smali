.class Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->prepareSpinner(Lcom/pspdfkit/annotations/measurements/FloatPrecision;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$1;->this$0:Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$1;->this$0:Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;

    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->-$$Nest$fgetspinnerAdapter(Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;)Landroid/widget/ArrayAdapter;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result p1

    if-lt p3, p1, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-static {}, Lcom/pspdfkit/annotations/measurements/FloatPrecision;->values()[Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object p1

    aget-object p1, p1, p3

    .line 3
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$1;->this$0:Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;

    invoke-static {p2}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->-$$Nest$fgetspinnerText(Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;)Landroid/widget/TextView;

    move-result-object p2

    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->-$$Nest$smstringFromPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$1;->this$0:Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;

    const/4 p3, 0x1

    invoke-virtual {p2, p1, p3}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;->setPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;Z)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;)V"
        }
    .end annotation

    return-void
.end method

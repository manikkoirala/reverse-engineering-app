.class public Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;
    }
.end annotation


# instance fields
.field private final moveBackward:Landroid/widget/ImageButton;

.field private final moveForward:Landroid/widget/ImageButton;

.field private final moveToBack:Landroid/widget/ImageButton;

.field private final moveToFront:Landroid/widget/ImageButton;

.field private final zIndexChangeListener:Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 3
    iput-object p3, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->zIndexChangeListener:Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object p1

    .line 9
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    sget v0, Lcom/pspdfkit/R$layout;->pspdf__view_inspector_z_index_picker:I

    invoke-static {p3, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/internal/fo;->c()I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/view/View;->setMinimumHeight(I)V

    .line 12
    sget p3, Lcom/pspdfkit/R$id;->pspdf__label:I

    invoke-virtual {p0, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/internal/fo;->e()I

    move-result p1

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 15
    invoke-virtual {p3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 17
    sget p1, Lcom/pspdfkit/R$id;->pspdf__move_to_front:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageButton;

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveToFront:Landroid/widget/ImageButton;

    .line 18
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    sget p1, Lcom/pspdfkit/R$id;->pspdf__move_forward:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageButton;

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveForward:Landroid/widget/ImageButton;

    .line 21
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    sget p1, Lcom/pspdfkit/R$id;->pspdf__move_backward:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageButton;

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveBackward:Landroid/widget/ImageButton;

    .line 24
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    sget p1, Lcom/pspdfkit/R$id;->pspdf__move_to_back:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageButton;

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveToBack:Landroid/widget/ImageButton;

    .line 27
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 0

    return-void
.end method

.method public disableBackwardMovements()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveToBack:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveToBack:Landroid/widget/ImageButton;

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveBackward:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveBackward:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    return-void
.end method

.method public disableForwardMovements()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveToFront:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveToFront:Landroid/widget/ImageButton;

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveForward:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveForward:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    return-void
.end method

.method public enableAllMovements()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveToFront:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveToFront:Landroid/widget/ImageButton;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveForward:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveForward:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveToBack:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveToBack:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveBackward:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveBackward:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    return-void
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic isViewStateRestorationEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$isViewStateRestorationEnabled(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->zIndexChangeListener:Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveToFront:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->zIndexChangeListener:Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;

    sget-object v0, Lcom/pspdfkit/annotations/AnnotationZIndexMove;->MOVE_TO_FRONT:Lcom/pspdfkit/annotations/AnnotationZIndexMove;

    invoke-interface {p1, p0, v0}, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;->onMoveExecuted(Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V

    goto :goto_0

    .line 4
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveForward:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->zIndexChangeListener:Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;

    sget-object v0, Lcom/pspdfkit/annotations/AnnotationZIndexMove;->MOVE_FORWARD:Lcom/pspdfkit/annotations/AnnotationZIndexMove;

    invoke-interface {p1, p0, v0}, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;->onMoveExecuted(Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V

    goto :goto_0

    .line 6
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveBackward:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->zIndexChangeListener:Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;

    sget-object v0, Lcom/pspdfkit/annotations/AnnotationZIndexMove;->MOVE_BACKWARD:Lcom/pspdfkit/annotations/AnnotationZIndexMove;

    invoke-interface {p1, p0, v0}, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;->onMoveExecuted(Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V

    goto :goto_0

    .line 8
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->moveToBack:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->zIndexChangeListener:Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;

    sget-object v0, Lcom/pspdfkit/annotations/AnnotationZIndexMove;->MOVE_TO_BACK:Lcom/pspdfkit/annotations/AnnotationZIndexMove;

    invoke-interface {p1, p0, v0}, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;->onMoveExecuted(Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V

    :cond_4
    :goto_0
    return-void
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public unbindController()V
    .locals 0

    return-void
.end method

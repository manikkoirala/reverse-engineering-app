.class public Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$CalibrationPickerListener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "CALIBRATION_PICKER"


# instance fields
.field private currentUnit:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

.field private currentValue:Ljava/lang/Float;

.field private final label:Ljava/lang/String;

.field private lineAnnotation:Lcom/pspdfkit/annotations/LineAnnotation;

.field private listener:Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$CalibrationPickerListener;

.field private spinner:Landroid/widget/Spinner;

.field private spinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private spinnerText:Landroid/widget/TextView;

.field private valueText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;


# direct methods
.method static bridge synthetic -$$Nest$fgetcurrentValue(Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;)Ljava/lang/Float;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->currentValue:Ljava/lang/Float;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetspinnerText(Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinnerText:Landroid/widget/TextView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mupdateValueFromEditTextView(Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->updateValueFromEditTextView()V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/annotations/LineAnnotation;Landroid/content/Context;Ljava/lang/String;Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$CalibrationPickerListener;)V
    .locals 0

    .line 1
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 p2, 0x0

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->currentValue:Ljava/lang/Float;

    const-string p2, "label"

    .line 44
    invoke-static {p3, p2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "defaultUnit"

    .line 45
    invoke-static {p4, p2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "lineAnnotation"

    .line 46
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->lineAnnotation:Lcom/pspdfkit/annotations/LineAnnotation;

    .line 48
    iput-object p3, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->label:Ljava/lang/String;

    .line 49
    iput-object p5, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$CalibrationPickerListener;

    .line 50
    iput-object p4, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->currentUnit:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 51
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->init()V

    return-void
.end method

.method private init()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object v0

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__view_inspector_scale_calibration_picker:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setMinimumHeight(I)V

    .line 7
    sget v2, Lcom/pspdfkit/R$id;->pspdf__label:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 8
    iget-object v4, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->label:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v0

    const/4 v4, 0x0

    invoke-virtual {v2, v4, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 12
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v4, -0x2

    invoke-direct {v0, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 16
    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->initSpinner(Landroid/view/View;)V

    .line 17
    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->initValueText(Landroid/view/View;)V

    .line 18
    invoke-virtual {p0, v3}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->updateCalibration(Lcom/pspdfkit/annotations/measurements/Scale;)V

    return-void
.end method

.method private initSpinner(Landroid/view/View;)V
    .locals 4

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__calibrate_unit_spinner:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinner:Landroid/widget/Spinner;

    .line 2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__calibrate_unit_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinnerText:Landroid/widget/TextView;

    .line 4
    new-instance p1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/String;

    sget-object v2, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->IN:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 5
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->MM:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 6
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->CM:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 7
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    sget-object v2, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->PT:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 8
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    sget-object v2, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->FT:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 9
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    aput-object v2, v1, v3

    sget-object v2, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->M:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 10
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    aput-object v2, v1, v3

    sget-object v2, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->YD:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 11
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    aput-object v2, v1, v3

    sget-object v2, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->KM:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 12
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    aput-object v2, v1, v3

    sget-object v2, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->MI:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 13
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x8

    aput-object v2, v1, v3

    const v2, 0x1090008

    invoke-direct {p1, v0, v2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinnerAdapter:Landroid/widget/ArrayAdapter;

    .line 15
    sget v0, Lcom/pspdfkit/R$layout;->pspdf__inspector_scale_unit_spinner_item:I

    invoke-virtual {p1, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinner:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinner:Landroid/widget/Spinner;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 18
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__measurement_unit_spinner_dropdown_horizontal_offset:I

    .line 19
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 20
    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setDropDownHorizontalOffset(I)V

    .line 25
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinnerAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->currentUnit:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result p1

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/AdapterView;->setSelection(I)V

    .line 28
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinner:Landroid/widget/Spinner;

    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$1;-><init>(Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;)V

    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 46
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinnerText:Landroid/widget/TextView;

    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private initValueText(Landroid/view/View;)V
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__calibrate_value_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->valueText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    const/4 p1, 0x2

    new-array p1, p1, [Landroid/text/InputFilter;

    .line 4
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v1, 0x0

    aput-object v0, p1, v1

    new-instance v0, Lcom/pspdfkit/internal/v7;

    invoke-direct {v0}, Lcom/pspdfkit/internal/v7;-><init>()V

    const/4 v1, 0x1

    aput-object v0, p1, v1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->valueText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->valueText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatEditText;->setImeOptions(I)V

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->valueText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;)V

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->valueText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$2;-><init>(Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method private updateValueFromEditTextView()V
    .locals 3

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->valueText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    const/4 v0, 0x0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->valueText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->valueText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->valueText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const v2, 0x3727c5ac    # 1.0E-5f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    return-void

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->currentValue:Ljava/lang/Float;

    invoke-static {v1, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->currentUnit:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->setCalibration(Ljava/lang/Float;Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;Z)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CALIBRATION_PICKER"

    const-string v2, "Calibration value should be a float or empty."

    .line 11
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 0

    return-void
.end method

.method public getLineAnnotation()Lcom/pspdfkit/annotations/LineAnnotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->lineAnnotation:Lcom/pspdfkit/annotations/LineAnnotation;

    return-object v0
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic isViewStateRestorationEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$isViewStateRestorationEnabled(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)Z

    move-result v0

    return v0
.end method

.method synthetic lambda$initSpinner$0$com-pspdfkit-ui-inspector-views-ScaleCalibrationPickerInspectorView(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinner:Landroid/widget/Spinner;

    invoke-virtual {p1}, Landroid/widget/Spinner;->performClick()Z

    return-void
.end method

.method synthetic lambda$initValueText$1$com-pspdfkit-ui-inspector-views-ScaleCalibrationPickerInspectorView(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p3, 0x6

    if-ne p2, p3, :cond_0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->updateValueFromEditTextView()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public setCalibration(Ljava/lang/Float;Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->currentValue:Ljava/lang/Float;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->currentUnit:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    if-eq v0, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 3
    :goto_1
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->currentValue:Ljava/lang/Float;

    .line 4
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->currentUnit:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinner:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {p2}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setSelection(I)V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->spinnerText:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_2

    .line 10
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {v1}, Lcom/pspdfkit/internal/ui;->a(F)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_2
    const-string v1, ""

    .line 12
    :goto_2
    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->valueText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->valueText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 13
    :cond_3
    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->valueText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    if-eqz p3, :cond_5

    .line 20
    iget-object p3, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$CalibrationPickerListener;

    if-eqz p3, :cond_5

    if-eqz v0, :cond_5

    .line 21
    invoke-interface {p3, p1, p2}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$CalibrationPickerListener;->onScaleCalibrationPicked(Ljava/lang/Float;Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;)V

    :cond_5
    return-void
.end method

.method public unbindController()V
    .locals 0

    return-void
.end method

.method public updateCalibration(Lcom/pspdfkit/annotations/measurements/Scale;)V
    .locals 2

    if-eqz p1, :cond_0

    goto :goto_0

    .line 1
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->lineAnnotation:Lcom/pspdfkit/annotations/LineAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object p1

    :goto_0
    if-nez p1, :cond_1

    return-void

    .line 4
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->lineAnnotation:Lcom/pspdfkit/annotations/LineAnnotation;

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/LineAnnotation;->getPoints()Landroidx/core/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->lineAnnotation:Lcom/pspdfkit/annotations/LineAnnotation;

    .line 7
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/LineAnnotation;->getPoints()Landroidx/core/util/Pair;

    move-result-object v1

    iget-object v1, v1, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/PointF;

    .line 8
    invoke-static {p1}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/annotations/measurements/Scale;)Lcom/pspdfkit/internal/jni/NativeMeasurementScale;

    move-result-object p1

    .line 9
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/internal/jni/NativeMeasurementCalculator;->getMeasurementCalibrationFromScale(Landroid/graphics/PointF;Landroid/graphics/PointF;Lcom/pspdfkit/internal/jni/NativeMeasurementScale;)Lcom/pspdfkit/internal/jni/NativeMeasurementCalibration;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeMeasurementCalibration;->getValue()D

    move-result-wide v0

    double-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeMeasurementCalibration;->getUnitTo()Lcom/pspdfkit/internal/jni/NativeUnitTo;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/internal/jni/NativeUnitTo;)Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    move-result-object p1

    const/4 v1, 0x0

    .line 16
    invoke-virtual {p0, v0, p1, v1}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->setCalibration(Ljava/lang/Float;Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;Z)V

    :cond_2
    return-void
.end method

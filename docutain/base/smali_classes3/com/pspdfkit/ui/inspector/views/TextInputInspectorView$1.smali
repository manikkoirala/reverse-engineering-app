.class Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->init(Ljava/lang/String;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$TextInputListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private originalSoftInputMode:I

.field final synthetic this$0:Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;


# direct methods
.method constructor <init>(Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$1;->this$0:Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$1;->this$0:Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;

    .line 2
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/16 p2, 0x10

    .line 3
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/pg;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$1;->originalSoftInputMode:I

    goto :goto_0

    .line 7
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$1;->this$0:Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    iget p2, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$1;->originalSoftInputMode:I

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/pg;->a(Landroid/content/Context;I)I

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$1;->this$0:Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;

    invoke-static {p1}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->-$$Nest$fgeteditText(Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;)Landroid/widget/EditText;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    :goto_0
    return-void
.end method

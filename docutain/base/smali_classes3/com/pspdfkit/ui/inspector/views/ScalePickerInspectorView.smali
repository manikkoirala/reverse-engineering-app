.class public Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$ScalePickerListener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "SCALE_PICKER"

.field private static spinnerUnitDropdownHorizontalOffset:I


# instance fields
.field private calibrationPickerInspectorView:Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;

.field private currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

.field private final label:Ljava/lang/String;

.field listener:Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$ScalePickerListener;

.field private spinnerUnitFrom:Landroid/widget/Spinner;

.field private spinnerUnitFromAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private spinnerUnitFromText:Landroid/widget/TextView;

.field private spinnerUnitTo:Landroid/widget/Spinner;

.field private spinnerUnitToAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private spinnerUnitToText:Landroid/widget/TextView;

.field private valueFromText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

.field private valueToText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;


# direct methods
.method static bridge synthetic -$$Nest$fgetcurrentScaleValue(Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;)Lcom/pspdfkit/annotations/measurements/Scale;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetspinnerUnitFromText(Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFromText:Landroid/widget/TextView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetspinnerUnitToText(Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitToText:Landroid/widget/TextView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$monValueEditTextViewUpdated(Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->onValueEditTextViewUpdated(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$ScalePickerListener;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const-string p1, "label"

    .line 2
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "defaultValue"

    .line 3
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->label:Ljava/lang/String;

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$ScalePickerListener;

    .line 6
    iput-object p3, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

    .line 8
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->init()V

    return-void
.end method

.method private init()V
    .locals 5

    .line 1
    sget v0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitDropdownHorizontalOffset:I

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__measurement_unit_spinner_dropdown_horizontal_offset:I

    .line 4
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitDropdownHorizontalOffset:I

    .line 8
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object v0

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__view_inspector_scale_picker:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setMinimumHeight(I)V

    .line 13
    sget v2, Lcom/pspdfkit/R$id;->pspdf__label:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 14
    iget-object v3, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->label:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 15
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 16
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v0

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 18
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v4, -0x2

    invoke-direct {v0, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 22
    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->prepareSpinners(Landroid/view/View;)V

    .line 23
    invoke-direct {p0, v1}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->prepareValueTexts(Landroid/view/View;)V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

    invoke-virtual {p0, v0, v3, v3}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->setScale(Lcom/pspdfkit/annotations/measurements/Scale;ZZ)V

    return-void
.end method

.method private onValueEditTextViewUpdated(Z)V
    .locals 6

    if-eqz p1, :cond_0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueFromText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueToText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    :goto_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    const v1, 0x3727c5ac    # 1.0E-5f

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    .line 2
    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    if-eqz p1, :cond_1

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

    iget v1, v1, Lcom/pspdfkit/annotations/measurements/Scale;->valueFrom:F

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

    iget v1, v1, Lcom/pspdfkit/annotations/measurements/Scale;->valueTo:F

    :goto_1
    const/4 v2, 0x5

    .line 4
    new-instance v3, Ljava/math/BigDecimal;

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 5
    sget-object v1, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v2, v1}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 6
    invoke-virtual {v1}, Ljava/math/BigDecimal;->floatValue()F

    move-result v1

    .line 7
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/di;->a(FF)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_3

    const-string v1, "sourceScale"

    const/4 v2, 0x1

    if-eqz p1, :cond_2

    .line 9
    :try_start_1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

    .line 10
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 280
    new-instance v1, Lcom/pspdfkit/annotations/measurements/Scale;

    iget-object v3, p1, Lcom/pspdfkit/annotations/measurements/Scale;->unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    iget v4, p1, Lcom/pspdfkit/annotations/measurements/Scale;->valueTo:F

    iget-object p1, p1, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    invoke-direct {v1, v0, v3, v4, p1}, Lcom/pspdfkit/annotations/measurements/Scale;-><init>(FLcom/pspdfkit/annotations/measurements/Scale$UnitFrom;FLcom/pspdfkit/annotations/measurements/Scale$UnitTo;)V

    .line 281
    invoke-virtual {p0, v1, v2, v2}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->setScale(Lcom/pspdfkit/annotations/measurements/Scale;ZZ)V

    goto :goto_2

    .line 283
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

    .line 284
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 557
    new-instance v1, Lcom/pspdfkit/annotations/measurements/Scale;

    iget v3, p1, Lcom/pspdfkit/annotations/measurements/Scale;->valueFrom:F

    iget-object v4, p1, Lcom/pspdfkit/annotations/measurements/Scale;->unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    iget-object p1, p1, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    invoke-direct {v1, v3, v4, v0, p1}, Lcom/pspdfkit/annotations/measurements/Scale;-><init>(FLcom/pspdfkit/annotations/measurements/Scale$UnitFrom;FLcom/pspdfkit/annotations/measurements/Scale$UnitTo;)V

    .line 558
    invoke-virtual {p0, v1, v2, v2}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->setScale(Lcom/pspdfkit/annotations/measurements/Scale;ZZ)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    const/4 p1, 0x0

    new-array v0, p1, [Ljava/lang/Object;

    const-string v1, "SCALE_PICKER"

    const-string v2, "Scale from or to value is not a float."

    .line 562
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 564
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

    invoke-virtual {p0, v0, p1, p1}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->setScale(Lcom/pspdfkit/annotations/measurements/Scale;ZZ)V

    :cond_3
    :goto_2
    return-void
.end method

.method private prepareSpinners(Landroid/view/View;)V
    .locals 9

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__unit_from_spinner:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFrom:Landroid/widget/Spinner;

    .line 2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__unit_to_spinner:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitTo:Landroid/widget/Spinner;

    .line 3
    sget v0, Lcom/pspdfkit/R$id;->pspdf__unit_from_spinner_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFromText:Landroid/widget/TextView;

    .line 4
    sget v0, Lcom/pspdfkit/R$id;->pspdf__unit_to_spinner_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitToText:Landroid/widget/TextView;

    .line 6
    new-instance p1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x4

    new-array v2, v1, [Ljava/lang/String;

    sget-object v3, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->IN:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 7
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    sget-object v3, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->MM:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 8
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    aput-object v3, v2, v5

    sget-object v3, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->CM:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 9
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x2

    aput-object v3, v2, v6

    sget-object v3, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->PT:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 10
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x3

    aput-object v3, v2, v7

    const v3, 0x1090008

    invoke-direct {p1, v0, v3, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFromAdapter:Landroid/widget/ArrayAdapter;

    .line 12
    sget v0, Lcom/pspdfkit/R$layout;->pspdf__inspector_scale_unit_spinner_item:I

    invoke-virtual {p1, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFrom:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFromAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFrom:Landroid/widget/Spinner;

    sget v0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitDropdownHorizontalOffset:I

    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setDropDownHorizontalOffset(I)V

    .line 16
    new-instance p1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/String;

    sget-object v8, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->IN:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 17
    invoke-virtual {v8}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v4

    sget-object v4, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->MM:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 18
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v5

    sget-object v4, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->CM:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 19
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v6

    sget-object v4, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->PT:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 20
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v7

    sget-object v4, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->FT:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 21
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    sget-object v1, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->M:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 22
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x5

    aput-object v1, v2, v4

    sget-object v1, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->YD:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 23
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x6

    aput-object v1, v2, v4

    sget-object v1, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->KM:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 24
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x7

    aput-object v1, v2, v4

    sget-object v1, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->MI:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 25
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v4, 0x8

    aput-object v1, v2, v4

    invoke-direct {p1, v0, v3, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitToAdapter:Landroid/widget/ArrayAdapter;

    .line 27
    sget v0, Lcom/pspdfkit/R$layout;->pspdf__inspector_scale_unit_spinner_item:I

    invoke-virtual {p1, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 28
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitTo:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitToAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 29
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitTo:Landroid/widget/Spinner;

    sget v0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitDropdownHorizontalOffset:I

    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setDropDownHorizontalOffset(I)V

    .line 32
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFromAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

    iget-object v0, v0, Lcom/pspdfkit/annotations/measurements/Scale;->unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result p1

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFrom:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/AdapterView;->setSelection(I)V

    .line 34
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitToAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

    iget-object v0, v0, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result p1

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitTo:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/AdapterView;->setSelection(I)V

    .line 37
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFrom:Landroid/widget/Spinner;

    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$1;-><init>(Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;)V

    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 53
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitTo:Landroid/widget/Spinner;

    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$2;-><init>(Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;)V

    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 71
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFromText:Landroid/widget/TextView;

    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitToText:Landroid/widget/TextView;

    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private prepareValueTexts(Landroid/view/View;)V
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__value_from_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueFromText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    .line 2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__value_to_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueToText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    const/4 p1, 0x2

    new-array p1, p1, [Landroid/text/InputFilter;

    .line 5
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v1, 0x0

    aput-object v0, p1, v1

    new-instance v0, Lcom/pspdfkit/internal/v7;

    invoke-direct {v0}, Lcom/pspdfkit/internal/v7;-><init>()V

    const/4 v1, 0x1

    aput-object v0, p1, v1

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueFromText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueToText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueFromText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;)V

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueToText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;)V

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueFromText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatEditText;->setImeOptions(I)V

    .line 31
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueToText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatEditText;->setImeOptions(I)V

    .line 33
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueFromText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$3;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$3;-><init>(Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 45
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueToText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$4;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$4;-><init>(Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 0

    return-void
.end method

.method public getCurrentScaleValue()Lcom/pspdfkit/annotations/measurements/Scale;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

    return-object v0
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic isViewStateRestorationEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$isViewStateRestorationEnabled(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)Z

    move-result v0

    return v0
.end method

.method synthetic lambda$prepareSpinners$0$com-pspdfkit-ui-inspector-views-ScalePickerInspectorView(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFrom:Landroid/widget/Spinner;

    invoke-virtual {p1}, Landroid/widget/Spinner;->performClick()Z

    return-void
.end method

.method synthetic lambda$prepareSpinners$1$com-pspdfkit-ui-inspector-views-ScalePickerInspectorView(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitTo:Landroid/widget/Spinner;

    invoke-virtual {p1}, Landroid/widget/Spinner;->performClick()Z

    return-void
.end method

.method synthetic lambda$prepareValueTexts$2$com-pspdfkit-ui-inspector-views-ScalePickerInspectorView(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p3, 0x6

    if-ne p2, p3, :cond_0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    const/4 p1, 0x1

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->onValueEditTextViewUpdated(Z)V

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method synthetic lambda$prepareValueTexts$3$com-pspdfkit-ui-inspector-views-ScalePickerInspectorView(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 p3, 0x0

    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 2
    invoke-direct {p0, p3}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->onValueEditTextViewUpdated(Z)V

    const/4 p1, 0x1

    return p1

    :cond_0
    return p3
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public setCalibrationPicker(Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->calibrationPickerInspectorView:Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;

    return-void
.end method

.method public setScale(Lcom/pspdfkit/annotations/measurements/Scale;ZZ)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/measurements/Scale;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->currentScaleValue:Lcom/pspdfkit/annotations/measurements/Scale;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFrom:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFromAdapter:Landroid/widget/ArrayAdapter;

    iget-object v3, p1, Lcom/pspdfkit/annotations/measurements/Scale;->unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setSelection(I)V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitTo:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitToAdapter:Landroid/widget/ArrayAdapter;

    iget-object v3, p1, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setSelection(I)V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitFromText:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/pspdfkit/annotations/measurements/Scale;->unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->spinnerUnitToText:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9
    iget v1, p1, Lcom/pspdfkit/annotations/measurements/Scale;->valueFrom:F

    invoke-static {v1}, Lcom/pspdfkit/internal/ui;->a(F)Ljava/lang/String;

    move-result-object v1

    .line 10
    iget v2, p1, Lcom/pspdfkit/annotations/measurements/Scale;->valueTo:F

    invoke-static {v2}, Lcom/pspdfkit/internal/ui;->a(F)Ljava/lang/String;

    move-result-object v2

    .line 11
    iget-object v3, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueFromText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueFromText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    .line 12
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 13
    :cond_0
    iget-object v3, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueFromText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 15
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueToText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueToText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    .line 16
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 17
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->valueToText:Lcom/pspdfkit/ui/editor/ScreenAdjustingEditText;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    if-eqz p2, :cond_4

    .line 20
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$ScalePickerListener;

    if-eqz p2, :cond_4

    if-eqz v0, :cond_4

    .line 21
    invoke-interface {p2, p1}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$ScalePickerListener;->onScalePicked(Lcom/pspdfkit/annotations/measurements/Scale;)V

    .line 23
    :cond_4
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->calibrationPickerInspectorView:Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;

    if-eqz p2, :cond_5

    if-eqz p3, :cond_5

    .line 24
    invoke-virtual {p2, p1}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;->updateCalibration(Lcom/pspdfkit/annotations/measurements/Scale;)V

    :cond_5
    return-void
.end method

.method public unbindController()V
    .locals 0

    return-void
.end method

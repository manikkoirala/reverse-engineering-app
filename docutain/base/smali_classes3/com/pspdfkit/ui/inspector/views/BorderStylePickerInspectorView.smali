.class public Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;
.super Lcom/pspdfkit/internal/views/inspector/views/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView$BorderStylePickerListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/views/inspector/views/a<",
        "Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;",
        ">;"
    }
.end annotation


# instance fields
.field listener:Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView$BorderStylePickerListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView$BorderStylePickerListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;",
            ">;",
            "Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;",
            "Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView$BorderStylePickerListener;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-static {p1, p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;->getPickerItems(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 2
    invoke-static {p3, p4}, Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;->getDefaultPreset(Ljava/util/List;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object p3

    .line 3
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/pspdfkit/internal/views/inspector/views/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/Object;)V

    .line 8
    iput-object p5, p0, Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView$BorderStylePickerListener;

    return-void
.end method

.method private static getDefaultPreset(Ljava/util/List;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;",
            ">;",
            "Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;",
            ")",
            "Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;"
        }
    .end annotation

    .line 1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    .line 2
    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    .line 5
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v2

    invoke-virtual {v1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v3

    if-ne v2, v3, :cond_2

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v2

    invoke-virtual {v1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v3

    if-ne v2, v3, :cond_2

    return-object v1

    :cond_3
    const/4 p1, 0x0

    .line 13
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    return-object p0
.end method

.method private static getPickerItems(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/views/inspector/views/a$a<",
            "Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;",
            ">;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    invoke-static {p0}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object v1

    const/4 v2, 0x1

    int-to-float v3, v2

    .line 4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 5
    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/internal/fo;->a()I

    move-result v1

    .line 8
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v10, v3

    check-cast v10, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    .line 9
    new-instance v11, Lcom/pspdfkit/internal/eh;

    int-to-float v6, v2

    .line 10
    sget-object v9, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    move-object v3, v11

    move-object v4, p0

    move v5, v1

    move-object v7, v10

    move-object v8, v9

    invoke-direct/range {v3 .. v9}, Lcom/pspdfkit/internal/eh;-><init>(Landroid/content/Context;IFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    .line 11
    new-instance v3, Lcom/pspdfkit/internal/views/inspector/views/a$a;

    invoke-direct {v3, v11, v10}, Lcom/pspdfkit/internal/views/inspector/views/a$a;-><init>(Lcom/pspdfkit/internal/eh;Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method protected onItemPicked(Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView$BorderStylePickerListener;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0, p0, p1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView$BorderStylePickerListener;->onBorderStylePicked(Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onItemPicked(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;->onItemPicked(Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    return-void
.end method

.class public Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$SavedState;,
        Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$TextInputListener;
    }
.end annotation


# instance fields
.field private controller:Lcom/pspdfkit/ui/inspector/PropertyInspectorController;

.field private editText:Landroid/widget/EditText;

.field private final label:Ljava/lang/String;

.field private labelView:Landroid/widget/TextView;

.field private listener:Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$TextInputListener;

.field private textInputContainer:Landroid/widget/FrameLayout;

.field private textView:Lcom/pspdfkit/internal/ui/views/UnderlinedTextView;


# direct methods
.method static bridge synthetic -$$Nest$fgeteditText(Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;)Landroid/widget/EditText;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->editText:Landroid/widget/EditText;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$msetValue(Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->setValue(Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$TextInputListener;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const-string p1, "label"

    .line 2
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "defaultValue"

    .line 3
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->label:Ljava/lang/String;

    .line 5
    invoke-direct {p0, p3, p4}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->init(Ljava/lang/String;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$TextInputListener;)V

    return-void
.end method

.method private init(Ljava/lang/String;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$TextInputListener;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object v0

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 4
    sget v2, Lcom/pspdfkit/R$layout;->pspdf__view_inspector_text:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 6
    sget v1, Lcom/pspdfkit/R$id;->pspdf__label:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->labelView:Landroid/widget/TextView;

    .line 7
    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->label:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->labelView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->labelView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 11
    sget v1, Lcom/pspdfkit/R$id;->pspdf__text:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ui/views/UnderlinedTextView;

    iput-object v1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->textView:Lcom/pspdfkit/internal/ui/views/UnderlinedTextView;

    .line 12
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->textView:Lcom/pspdfkit/internal/ui/views/UnderlinedTextView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->textView:Lcom/pspdfkit/internal/ui/views/UnderlinedTextView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/ui/views/UnderlinedTextView;->setUnderLineColor(I)V

    .line 16
    sget v1, Lcom/pspdfkit/R$id;->pspdf__text_input_container:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->textInputContainer:Landroid/widget/FrameLayout;

    .line 18
    sget v1, Lcom/pspdfkit/R$id;->pspdf__text_input:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->editText:Landroid/widget/EditText;

    .line 19
    iget-object v2, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->label:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 21
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 22
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->editText:Landroid/widget/EditText;

    new-instance v2, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$1;

    invoke-direct {v2, p0}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$1;-><init>(Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 39
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->editText:Landroid/widget/EditText;

    new-instance v2, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$2;

    invoke-direct {v2, p0}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$2;-><init>(Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 46
    sget v1, Lcom/pspdfkit/R$id;->pspdf__text_picker_title_row:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 47
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->c()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setMinimumHeight(I)V

    .line 48
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    invoke-direct {p0, p1, v3}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->setValue(Ljava/lang/String;Z)V

    .line 51
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$TextInputListener;

    return-void
.end method

.method private setValue(Ljava/lang/String;Z)V
    .locals 1

    if-nez p2, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->textView:Lcom/pspdfkit/internal/ui/views/UnderlinedTextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$TextInputListener;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 5
    invoke-interface {v0, p0, p1}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$TextInputListener;->onValuePicked(Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private showDetailPicker(ZZ)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->textInputContainer:Landroid/widget/FrameLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    if-nez p2, :cond_1

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->editText:Landroid/widget/EditText;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setAlpha(F)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->editText:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;)V

    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->controller:Lcom/pspdfkit/ui/inspector/PropertyInspectorController;

    if-eqz p1, :cond_1

    .line 6
    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorController;->ensureFullyVisible(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    goto :goto_0

    .line 10
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->textInputContainer:Landroid/widget/FrameLayout;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->controller:Lcom/pspdfkit/ui/inspector/PropertyInspectorController;

    return-void
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public isViewStateRestorationEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method synthetic lambda$init$0$com-pspdfkit-ui-inspector-views-TextInputInspectorView(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->textInputContainer:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->showDetailPicker(ZZ)V

    return-void
.end method

.method synthetic lambda$onRestoreInstanceState$2$com-pspdfkit-ui-inspector-views-TextInputInspectorView(Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$SavedState;)V
    .locals 1

    .line 1
    iget-boolean p1, p1, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$SavedState;->isDetailPickerVisible:Z

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->showDetailPicker(ZZ)V

    return-void
.end method

.method synthetic lambda$showDetailPicker$1$com-pspdfkit-ui-inspector-views-TextInputInspectorView()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    return-void
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$SavedState;

    if-nez v0, :cond_0

    .line 2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 6
    :cond_0
    check-cast p1, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$SavedState;

    .line 7
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 8
    iget-boolean v0, p1, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$SavedState;->isDetailPickerVisible:Z

    if-eqz v0, :cond_1

    .line 9
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$SavedState;)V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_1
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$SavedState;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->editText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, v0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$SavedState;->isDetailPickerVisible:Z

    return-object v0
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public unbindController()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;->controller:Lcom/pspdfkit/ui/inspector/PropertyInspectorController;

    return-void
.end method

.class public Lcom/pspdfkit/ui/inspector/views/ContentEditingFontNamesPickerView;
.super Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorDetailView;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/fonts/Font;",
            ">;",
            "Lcom/pspdfkit/ui/fonts/Font;",
            "Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;)V

    return-void
.end method


# virtual methods
.method protected createAdapter(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView;Lcom/pspdfkit/ui/fonts/Font;Ljava/util/List;Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;)Lcom/pspdfkit/internal/pb;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroidx/recyclerview/widget/RecyclerView;",
            "Lcom/pspdfkit/ui/fonts/Font;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/fonts/Font;",
            ">;",
            "Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;",
            ")",
            "Lcom/pspdfkit/internal/pb;"
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/pspdfkit/internal/g6;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v0, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/g6;-><init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView;Lcom/pspdfkit/ui/fonts/Font;Ljava/util/List;Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;)V

    return-object p1
.end method

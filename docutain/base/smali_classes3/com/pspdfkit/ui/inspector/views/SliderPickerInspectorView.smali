.class public Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;
    }
.end annotation


# instance fields
.field private final label:Ljava/lang/String;

.field listener:Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;

.field private final maximumValue:I

.field private final minimumValue:I

.field private seekBarView:Landroid/widget/SeekBar;

.field private sliderLabelView:Landroid/widget/TextView;

.field private unitEditText:Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

.field private value:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmaximumValue(Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->maximumValue:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetminimumValue(Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->minimumValue:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetunitEditText(Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;)Lcom/pspdfkit/ui/editor/UnitSelectionEditText;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->unitEditText:Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$msetValue(Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->setValue(IZ)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIILcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/high16 p1, -0x80000000

    .line 2
    iput p1, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->value:I

    const-string p1, "label"

    .line 26
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "unitLabel"

    .line 27
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->label:Ljava/lang/String;

    .line 29
    iput p4, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->minimumValue:I

    .line 30
    iput p5, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->maximumValue:I

    .line 31
    invoke-direct {p0, p6, p7, p3}, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->init(ILcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;Ljava/lang/String;)V

    return-void
.end method

.method private init(ILcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;Ljava/lang/String;)V
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object v0

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__view_inspector_slider_picker:I

    invoke-static {v1, v2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setMinimumHeight(I)V

    .line 6
    sget v1, Lcom/pspdfkit/R$id;->pspdf__sliderLabel:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->sliderLabelView:Landroid/widget/TextView;

    .line 7
    sget v1, Lcom/pspdfkit/R$id;->pspdf__sliderSeekBar:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->seekBarView:Landroid/widget/SeekBar;

    .line 8
    sget v1, Lcom/pspdfkit/R$id;->pspdf__sliderUnitEditText:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

    iput-object v2, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->unitEditText:Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

    .line 9
    iget v5, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->minimumValue:I

    iget v6, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->maximumValue:I

    new-instance v7, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$$ExternalSyntheticLambda0;

    invoke-direct {v7, p0}, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;)V

    move-object v3, p3

    move v4, p1

    invoke-virtual/range {v2 .. v7}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->setUnitLabel(Ljava/lang/String;IIILcom/pspdfkit/ui/editor/UnitSelectionEditText$UnitSelectionListener;)V

    .line 12
    iget-object p3, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->sliderLabelView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 13
    iget-object p3, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->sliderLabelView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p3, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 14
    iget-object p3, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->unitEditText:Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 15
    iget-object p3, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->unitEditText:Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v0

    invoke-virtual {p3, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 17
    iget-object p3, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->sliderLabelView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->label:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 18
    iget-object p3, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->seekBarView:Landroid/widget/SeekBar;

    iget v0, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->maximumValue:I

    iget v1, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->minimumValue:I

    sub-int/2addr v0, v1

    invoke-virtual {p3, v0}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 19
    iget-object p3, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->seekBarView:Landroid/widget/SeekBar;

    new-instance v0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$1;-><init>(Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;)V

    invoke-virtual {p3, v0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 35
    invoke-direct {p0, p1, v2}, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->setValue(IZ)V

    .line 36
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;

    return-void
.end method

.method private setValue(IZ)V
    .locals 2

    .line 2
    iget v0, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->value:I

    if-ne v0, p1, :cond_0

    return-void

    .line 3
    :cond_0
    iput p1, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->value:I

    if-eqz p2, :cond_1

    .line 5
    iget v0, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->minimumValue:I

    iget v1, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->maximumValue:I

    .line 6
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 7
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->seekBarView:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->minimumValue:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->unitEditText:Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->setTextToFormat(I)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    .line 10
    invoke-interface {v0, p0, p1}, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;->onValuePicked(Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V

    :cond_2
    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 0

    return-void
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic isViewStateRestorationEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$isViewStateRestorationEnabled(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)Z

    move-result v0

    return v0
.end method

.method synthetic lambda$init$0$com-pspdfkit-ui-inspector-views-SliderPickerInspectorView(Lcom/pspdfkit/ui/editor/UnitSelectionEditText;I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p2}, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->setValue(I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->unitEditText:Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->getValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->setValue(I)V

    return-void
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public setValue(I)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;->setValue(IZ)V

    return-void
.end method

.method public unbindController()V
    .locals 0

    return-void
.end method

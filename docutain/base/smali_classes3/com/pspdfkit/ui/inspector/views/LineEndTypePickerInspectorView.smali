.class public Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;
.super Lcom/pspdfkit/internal/views/inspector/views/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView$LineEndTypePickerListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/views/inspector/views/a<",
        "Lcom/pspdfkit/annotations/LineEndType;",
        ">;"
    }
.end annotation


# instance fields
.field listener:Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView$LineEndTypePickerListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Lcom/pspdfkit/annotations/LineEndType;ZLcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView$LineEndTypePickerListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Z",
            "Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView$LineEndTypePickerListener;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-static {p1, p3, p5}, Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;->getPickerItems(Landroid/content/Context;Ljava/util/List;Z)Ljava/util/List;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/views/inspector/views/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/Object;)V

    .line 2
    iput-object p6, p0, Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView$LineEndTypePickerListener;

    return-void
.end method

.method private static getPickerItems(Landroid/content/Context;Ljava/util/List;Z)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;Z)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/views/inspector/views/a$a<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    invoke-static {p0}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object v1

    const/4 v2, 0x2

    int-to-float v2, v2

    .line 4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    const/4 v4, 0x1

    .line 5
    invoke-static {v4, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/internal/fo;->a()I

    move-result v1

    .line 8
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v10, v3

    check-cast v10, Lcom/pspdfkit/annotations/LineEndType;

    .line 9
    new-instance v11, Lcom/pspdfkit/internal/eh;

    int-to-float v6, v2

    if-eqz p2, :cond_0

    move-object v8, v10

    goto :goto_1

    .line 13
    :cond_0
    sget-object v3, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    move-object v8, v3

    :goto_1
    if-nez p2, :cond_1

    move-object v9, v10

    goto :goto_2

    .line 14
    :cond_1
    sget-object v3, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    move-object v9, v3

    .line 15
    :goto_2
    sget-object v7, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->SOLID:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-object v3, v11

    move-object v4, p0

    move v5, v1

    invoke-direct/range {v3 .. v9}, Lcom/pspdfkit/internal/eh;-><init>(Landroid/content/Context;IFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    .line 16
    new-instance v3, Lcom/pspdfkit/internal/views/inspector/views/a$a;

    invoke-direct {v3, v11, v10}, Lcom/pspdfkit/internal/views/inspector/views/a$a;-><init>(Lcom/pspdfkit/internal/eh;Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method


# virtual methods
.method protected onItemPicked(Lcom/pspdfkit/annotations/LineEndType;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView$LineEndTypePickerListener;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0, p0, p1}, Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView$LineEndTypePickerListener;->onLineEndTypePicked(Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;Lcom/pspdfkit/annotations/LineEndType;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onItemPicked(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;->onItemPicked(Lcom/pspdfkit/annotations/LineEndType;)V

    return-void
.end method

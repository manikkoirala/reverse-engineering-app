.class public Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView$SnappingPickerListener;
    }
.end annotation


# instance fields
.field private isSnappingEnabled:Z

.field private final label:Ljava/lang/String;

.field listener:Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView$SnappingPickerListener;

.field private snappingSwitch:Lcom/pspdfkit/ui/LocalizedSwitch;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZLcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView$SnappingPickerListener;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const-string p1, "label"

    .line 2
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const-string v0, "defaultValue"

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;->label:Ljava/lang/String;

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView$SnappingPickerListener;

    .line 6
    iput-boolean p3, p0, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;->isSnappingEnabled:Z

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;->init()V

    return-void
.end method

.method private init()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object v0

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__view_inspector_snapping_picker:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setMinimumHeight(I)V

    .line 7
    sget v2, Lcom/pspdfkit/R$id;->pspdf__label:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 8
    iget-object v3, p0, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;->label:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v0

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 12
    sget v0, Lcom/pspdfkit/R$id;->pspdf__snapping_switch:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/LocalizedSwitch;

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;->snappingSwitch:Lcom/pspdfkit/ui/LocalizedSwitch;

    .line 13
    new-instance v2, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;)V

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 15
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 19
    iget-boolean v0, p0, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;->isSnappingEnabled:Z

    invoke-virtual {p0, v0}, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;->setSnappingEnabled(Z)V

    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 0

    return-void
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic isViewStateRestorationEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$isViewStateRestorationEnabled(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)Z

    move-result v0

    return v0
.end method

.method synthetic lambda$init$0$com-pspdfkit-ui-inspector-views-SnappingPickerInspectorView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p2}, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;->setSnappingEnabled(Z)V

    return-void
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public setSnappingEnabled(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;->isSnappingEnabled:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;->snappingSwitch:Lcom/pspdfkit/ui/LocalizedSwitch;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView$SnappingPickerListener;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView$SnappingPickerListener;->onSnappingPicked(Z)V

    :cond_0
    return-void
.end method

.method public unbindController()V
    .locals 0

    return-void
.end method

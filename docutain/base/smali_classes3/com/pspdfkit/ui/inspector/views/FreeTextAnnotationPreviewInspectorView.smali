.class public Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;


# instance fields
.field private final annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

.field private final textView:Landroid/widget/TextView;

.field private final unscaledPageToViewTransformation:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->unscaledPageToViewTransformation:Landroid/graphics/Matrix;

    const-string v0, "annotationCreationController"

    .line 18
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 21
    invoke-static {p1}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object p2

    .line 24
    invoke-virtual {p2}, Lcom/pspdfkit/internal/fo;->b()I

    move-result v0

    .line 25
    invoke-virtual {p2}, Lcom/pspdfkit/internal/fo;->g()I

    move-result v1

    .line 26
    invoke-virtual {p0, v0, v1, v0, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 28
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->textView:Landroid/widget/TextView;

    const/4 p1, 0x0

    .line 29
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 30
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSingleLine(Z)V

    const p1, 0x800013

    .line 31
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setGravity(I)V

    .line 32
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object p1

    .line 33
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mt;->a()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 34
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/fonts/Font;

    .line 35
    invoke-virtual {p1}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object p1

    .line 36
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 40
    invoke-virtual {p2}, Lcom/pspdfkit/internal/fo;->d()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setHeight(I)V

    .line 41
    sget p1, Lcom/pspdfkit/R$string;->pspdf__sample:I

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    const/4 p1, -0x1

    const/4 p2, -0x2

    .line 43
    invoke-virtual {p0, v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    return-void
.end method

.method private refreshAnnotationCreationParams()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->textView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->textView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAlpha()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getTextSize()F

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->unscaledPageToViewTransformation:Landroid/graphics/Matrix;

    .line 5
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v0

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->textView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->textView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getFillColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->textView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getFont()Lcom/pspdfkit/ui/fonts/Font;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->unscaledPageToViewTransformation:Landroid/graphics/Matrix;

    .line 3
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/go;->a(Lcom/pspdfkit/ui/PdfFragment;Landroid/graphics/Matrix;)V

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->refreshAnnotationCreationParams()V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->addOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    return-void
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic isViewStateRestorationEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$isViewStateRestorationEnabled(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)Z

    move-result v0

    return v0
.end method

.method public onAnnotationCreationModeSettingsChange(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->refreshAnnotationCreationParams()V

    return-void
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public unbindController()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;->annotationCreationController:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;->removeOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    return-void
.end method

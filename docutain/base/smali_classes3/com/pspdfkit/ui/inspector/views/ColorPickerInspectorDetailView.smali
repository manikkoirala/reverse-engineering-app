.class public Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;
.super Landroid/widget/ScrollView;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;


# static fields
.field private static final DEFAULT_MIN_ROWS_COUNT:D = 1.5


# instance fields
.field private colorPickerView:Lcom/pspdfkit/internal/views/picker/a;

.field private final colors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;IZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;IZ)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    const-string v0, "colors"

    .line 3
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->colors:Ljava/util/List;

    .line 7
    invoke-direct {p0, p1, p3, p4}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->init(Landroid/content/Context;IZ)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[IIZ)V
    .locals 0

    .line 1
    invoke-static {p2}, Lcom/pspdfkit/internal/fv;->a([I)Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;IZ)V

    return-void
.end method

.method private init(Landroid/content/Context;IZ)V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/views/picker/a;

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->colors:Ljava/util/List;

    invoke-direct {v0, p1, v1, p3}, Lcom/pspdfkit/internal/views/picker/a;-><init>(Landroid/content/Context;Ljava/util/List;Z)V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->colorPickerView:Lcom/pspdfkit/internal/views/picker/a;

    const/4 p1, 0x1

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/picker/a;->setShowSelectionIndicator(Z)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->colorPickerView:Lcom/pspdfkit/internal/views/picker/a;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/views/picker/a;->b(I)Z

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->colorPickerView:Lcom/pspdfkit/internal/views/picker/a;

    new-instance p2, Landroid/view/ViewGroup$LayoutParams;

    const/4 p3, -0x1

    invoke-direct {p2, p3, p3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 0

    return-void
.end method

.method public getMaximumHeight()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->colorPickerView:Lcom/pspdfkit/internal/views/picker/a;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getMinimumHeight()I
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->colorPickerView:Lcom/pspdfkit/internal/views/picker/a;

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/picker/a;->a(I)I

    move-result v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->colors:Ljava/util/List;

    .line 3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40a00000    # 5.0f

    div-float/2addr v1, v2

    float-to-double v1, v1

    const-wide/high16 v3, 0x3ff8000000000000L    # 1.5

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(DD)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v0, v0, 0xa

    mul-int v0, v0, v1

    return v0
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->getMaximumHeight()I

    move-result v0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->getMinimumHeight()I

    move-result v0

    return v0
.end method

.method public synthetic getState()Landroid/os/Parcelable;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView$-CC;->$default$getState(Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;)Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic isViewStateRestorationEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$isViewStateRestorationEnabled(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)Z

    move-result v0

    return v0
.end method

.method synthetic lambda$setOnColorPickedListener$0$com-pspdfkit-ui-inspector-views-ColorPickerInspectorDetailView(Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;Lcom/pspdfkit/internal/views/picker/a;I)V
    .locals 0

    .line 1
    invoke-interface {p1, p0, p3}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;->onColorPicked(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    return-void
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public setOnColorPickedListener(Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->colorPickerView:Lcom/pspdfkit/internal/views/picker/a;

    new-instance v1, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/picker/a;->setOnColorPickedListener(Lcom/pspdfkit/internal/views/picker/a$a;)V

    goto :goto_0

    .line 4
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->colorPickerView:Lcom/pspdfkit/internal/views/picker/a;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/views/picker/a;->setOnColorPickedListener(Lcom/pspdfkit/internal/views/picker/a$a;)V

    :goto_0
    return-void
.end method

.method public setShowSelectionIndicator(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->colorPickerView:Lcom/pspdfkit/internal/views/picker/a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/picker/a;->setShowSelectionIndicator(Z)V

    return-void
.end method

.method public synthetic setState(Landroid/os/Parcelable;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView$-CC;->$default$setState(Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;Landroid/os/Parcelable;)V

    return-void
.end method

.method public unbindController()V
    .locals 0

    return-void
.end method

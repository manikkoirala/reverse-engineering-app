.class public Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$TogglePickerListener;
    }
.end annotation


# instance fields
.field private checkBoxView:Landroid/widget/CheckBox;

.field private currentValue:Z

.field private final listener:Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$TogglePickerListener;

.field private final offValue:Ljava/lang/String;

.field private final onValue:Ljava/lang/String;

.field private selectedValueView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$TogglePickerListener;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->currentValue:Z

    const-string p1, "label"

    .line 21
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "onValue"

    .line 22
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "offValue"

    .line 23
    invoke-static {p4, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iput-object p3, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->onValue:Ljava/lang/String;

    .line 25
    iput-object p4, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->offValue:Ljava/lang/String;

    .line 26
    iput-object p6, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$TogglePickerListener;

    .line 27
    invoke-direct {p0, p2, p5}, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->init(Ljava/lang/String;Z)V

    return-void
.end method

.method private init(Ljava/lang/String;Z)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object v0

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__view_inspector_toggle_picker:I

    invoke-static {v1, v2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setMinimumHeight(I)V

    .line 6
    new-instance v2, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 8
    sget v1, Lcom/pspdfkit/R$id;->pspdf__label:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 11
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 16
    sget p1, Lcom/pspdfkit/R$id;->pspdf__value:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->selectedValueView:Landroid/widget/TextView;

    .line 17
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v1

    invoke-virtual {p1, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->selectedValueView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 20
    sget p1, Lcom/pspdfkit/R$id;->pspdf__toggle:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CheckBox;

    iput-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->checkBoxView:Landroid/widget/CheckBox;

    .line 21
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;)V

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 25
    iput-boolean p2, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->currentValue:Z

    .line 26
    invoke-direct {p0, p2, v3}, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->setValue(ZZ)V

    return-void
.end method

.method private setValue(ZZ)V
    .locals 0

    if-eqz p2, :cond_0

    .line 1
    iget-boolean p2, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->currentValue:Z

    if-eq p2, p1, :cond_0

    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->listener:Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$TogglePickerListener;

    if-eqz p2, :cond_0

    .line 2
    invoke-interface {p2, p0, p1}, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$TogglePickerListener;->onSelectionChanged(Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;Z)V

    .line 4
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->currentValue:Z

    .line 5
    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->checkBoxView:Landroid/widget/CheckBox;

    invoke-virtual {p2, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    if-eqz p1, :cond_1

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->selectedValueView:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->onValue:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 9
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->selectedValueView:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->offValue:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 0

    return-void
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic isViewStateRestorationEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$isViewStateRestorationEnabled(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)Z

    move-result v0

    return v0
.end method

.method synthetic lambda$init$0$com-pspdfkit-ui-inspector-views-TogglePickerInspectorView(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->checkBoxView:Landroid/widget/CheckBox;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    const/4 v0, 0x1

    xor-int/2addr p1, v0

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->setValue(ZZ)V

    return-void
.end method

.method synthetic lambda$init$1$com-pspdfkit-ui-inspector-views-TogglePickerInspectorView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    const/4 p1, 0x1

    .line 1
    invoke-direct {p0, p2, p1}, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;->setValue(ZZ)V

    return-void
.end method

.method public synthetic onHidden()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public synthetic onShown()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    return-void
.end method

.method public unbindController()V
    .locals 0

    return-void
.end method

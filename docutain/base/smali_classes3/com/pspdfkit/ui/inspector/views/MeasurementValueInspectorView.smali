.class public Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorView;


# instance fields
.field private final annotation:Lcom/pspdfkit/annotations/Annotation;

.field private final label:Ljava/lang/String;

.field private measurementValueView:Landroid/widget/TextView;

.field private onAnnotationPropertyChangeListener:Lcom/pspdfkit/internal/el;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const-string p1, "label"

    .line 2
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "measurementValue"

    .line 3
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "annotation"

    .line 4
    invoke-static {p4, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    iput-object p2, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->label:Ljava/lang/String;

    .line 6
    iput-object p4, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->annotation:Lcom/pspdfkit/annotations/Annotation;

    .line 8
    invoke-direct {p0, p3}, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->init(Ljava/lang/String;)V

    return-void
.end method

.method private init(Ljava/lang/String;)V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/fo;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/fo;

    move-result-object v0

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__view_inspector_measurement_value:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setMinimumHeight(I)V

    .line 7
    sget v2, Lcom/pspdfkit/R$id;->pspdf__label:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 8
    iget-object v3, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->label:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 12
    sget v2, Lcom/pspdfkit/R$id;->pspdf__value_text:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->measurementValueView:Landroid/widget/TextView;

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fo;->f()F

    move-result v0

    invoke-virtual {v2, v4, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 15
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 19
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->setMeasurementValue(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bindController(Lcom/pspdfkit/ui/inspector/PropertyInspectorController;)V
    .locals 0

    return-void
.end method

.method public getPropertyInspectorMaxHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getPropertyInspectorMinHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getSuggestedHeight()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic isViewStateRestorationEnabled()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$isViewStateRestorationEnabled(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)Z

    move-result v0

    return v0
.end method

.method synthetic lambda$onShown$0$com-pspdfkit-ui-inspector-views-MeasurementValueInspectorView(Lcom/pspdfkit/annotations/Annotation;ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->annotation:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->setMeasurementValue(Ljava/lang/String;)V

    return-void
.end method

.method public onHidden()V
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onHidden(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->onAnnotationPropertyChangeListener:Lcom/pspdfkit/internal/el;

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->annotation:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->onAnnotationPropertyChangeListener:Lcom/pspdfkit/internal/el;

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    :cond_0
    return-void
.end method

.method public onShown()V
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView$-CC;->$default$onShown(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->annotation:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->annotation:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->onAnnotationPropertyChangeListener:Lcom/pspdfkit/internal/el;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->annotation:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->onAnnotationPropertyChangeListener:Lcom/pspdfkit/internal/el;

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->addOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    :cond_0
    return-void
.end method

.method public setMeasurementValue(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;->measurementValueView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public unbindController()V
    .locals 0

    return-void
.end method

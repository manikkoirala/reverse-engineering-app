.class public Lcom/pspdfkit/ui/redaction/RedactionView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;,
        Lcom/pspdfkit/ui/redaction/RedactionView$RedactionViewListener;
    }
.end annotation


# static fields
.field public static final REDACTION_BUTTON_ICON_WIDTH_DP:I = 0x30


# instance fields
.field private isExpanded:Z

.field private isVisible:Z

.field private onRedactionButtonVisibilityChangedListener:Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;

.field private openRedactButton:Landroid/view/View;

.field private redactionActionsContainer:Landroid/view/View;

.field private redactionContainer:Landroid/widget/LinearLayout;

.field private redactionPreviewButton:Landroid/widget/Button;

.field private redactionPreviewEnabled:Z

.field private redactionViewListener:Lcom/pspdfkit/ui/redaction/RedactionView$RedactionViewListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isVisible:Z

    .line 3
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isExpanded:Z

    .line 4
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionPreviewEnabled:Z

    .line 8
    invoke-direct {p0}, Lcom/pspdfkit/ui/redaction/RedactionView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 10
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isVisible:Z

    .line 11
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isExpanded:Z

    .line 12
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionPreviewEnabled:Z

    .line 21
    invoke-direct {p0}, Lcom/pspdfkit/ui/redaction/RedactionView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    .line 23
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isVisible:Z

    .line 24
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isExpanded:Z

    .line 25
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionPreviewEnabled:Z

    .line 39
    invoke-direct {p0}, Lcom/pspdfkit/ui/redaction/RedactionView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p1, 0x0

    .line 41
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isVisible:Z

    .line 42
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isExpanded:Z

    .line 43
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionPreviewEnabled:Z

    .line 66
    invoke-direct {p0}, Lcom/pspdfkit/ui/redaction/RedactionView;->init()V

    return-void
.end method

.method private expandRedactionOptions()V
    .locals 3

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isExpanded:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionContainer:Landroid/widget/LinearLayout;

    .line 3
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0xfa

    .line 4
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 5
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/redaction/RedactionView;)V

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method private init()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__redaction_view:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__redaction_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionContainer:Landroid/widget/LinearLayout;

    .line 3
    sget v0, Lcom/pspdfkit/R$id;->pspdf__open_redact_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->openRedactButton:Landroid/view/View;

    .line 4
    new-instance v1, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/ui/redaction/RedactionView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 18
    sget v0, Lcom/pspdfkit/R$id;->pspdf__redaction_actions_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionActionsContainer:Landroid/view/View;

    .line 20
    sget v0, Lcom/pspdfkit/R$id;->pspdf__apply_redactions_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 21
    new-instance v1, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/redaction/RedactionView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    sget v0, Lcom/pspdfkit/R$id;->pspdf__clear_redactions_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 27
    new-instance v1, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/ui/redaction/RedactionView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    sget v0, Lcom/pspdfkit/R$id;->pspdf__redaction_preview_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionPreviewButton:Landroid/widget/Button;

    .line 33
    new-instance v1, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/ui/redaction/RedactionView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private updatePreviewText()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionPreviewEnabled:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionPreviewButton:Landroid/widget/Button;

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$string;->pspdf__redaction_disable_preview:I

    .line 4
    invoke-static {v2, v3, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    .line 5
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionPreviewButton:Landroid/widget/Button;

    .line 9
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$string;->pspdf__redaction_enable_preview:I

    .line 10
    invoke-static {v2, v3, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    .line 11
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public collapseRedactionOptions(Z)V
    .locals 5

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isExpanded:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionActionsContainer:Landroid/view/View;

    .line 3
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0xfa

    .line 4
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v3, 0x0

    .line 5
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v4, 0x3f000000    # 0.5f

    .line 6
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v4, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->openRedactButton:Landroid/view/View;

    .line 7
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 8
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v3, 0x0

    .line 9
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v4, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda9;

    invoke-direct {v4, p0}, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/ui/redaction/RedactionView;)V

    .line 10
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    if-eqz p1, :cond_0

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionContainer:Landroid/widget/LinearLayout;

    .line 15
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 16
    invoke-virtual {p1, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->openRedactButton:Landroid/view/View;

    .line 17
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x30

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    .line 19
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 21
    invoke-virtual {p1, v3}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 22
    invoke-virtual {p1, v3}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    return-void
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    const/4 p1, 0x0

    return p1
.end method

.method public getRedactionButtonWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->openRedactButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    return v0
.end method

.method public isButtonRedactionButtonVisible()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isVisible:Z

    return v0
.end method

.method public isRedactionAnnotationPreviewEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionPreviewEnabled:Z

    return v0
.end method

.method public isRedactionButtonExpanded()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isVisible:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isExpanded:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method synthetic lambda$collapseRedactionOptions$5$com-pspdfkit-ui-redaction-RedactionView()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionActionsContainer:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method synthetic lambda$expandRedactionOptions$4$com-pspdfkit-ui-redaction-RedactionView()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionActionsContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionActionsContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionActionsContainer:Landroid/view/View;

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionActionsContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionActionsContainer:Landroid/view/View;

    iget-object v2, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->openRedactButton:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionActionsContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setPivotY(F)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionActionsContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setPivotX(F)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionActionsContainer:Landroid/view/View;

    .line 10
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    .line 11
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 12
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 13
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 14
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 15
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 16
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 17
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method synthetic lambda$init$0$com-pspdfkit-ui-redaction-RedactionView(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isExpanded:Z

    if-nez p1, :cond_1

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->onRedactionButtonVisibilityChangedListener:Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;

    if-eqz p1, :cond_0

    .line 3
    invoke-interface {p1}, Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;->onRedactionButtonExpanding()V

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/ui/redaction/RedactionView;->expandRedactionOptions()V

    goto :goto_0

    .line 7
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->onRedactionButtonVisibilityChangedListener:Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;

    if-eqz p1, :cond_2

    .line 8
    invoke-interface {p1}, Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;->onRedactionButtonCollapsing()V

    :cond_2
    const/4 p1, 0x1

    .line 10
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/redaction/RedactionView;->collapseRedactionOptions(Z)V

    :goto_0
    return-void
.end method

.method synthetic lambda$init$1$com-pspdfkit-ui-redaction-RedactionView(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionViewListener:Lcom/pspdfkit/ui/redaction/RedactionView$RedactionViewListener;

    if-eqz p1, :cond_0

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/ui/redaction/RedactionView$RedactionViewListener;->onRedactionsApplied()V

    :cond_0
    return-void
.end method

.method synthetic lambda$init$2$com-pspdfkit-ui-redaction-RedactionView(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionViewListener:Lcom/pspdfkit/ui/redaction/RedactionView$RedactionViewListener;

    if-eqz p1, :cond_0

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/ui/redaction/RedactionView$RedactionViewListener;->onRedactionsCleared()V

    :cond_0
    return-void
.end method

.method synthetic lambda$init$3$com-pspdfkit-ui-redaction-RedactionView(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionPreviewEnabled:Z

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionPreviewEnabled:Z

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/redaction/RedactionView;->updatePreviewText()V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionViewListener:Lcom/pspdfkit/ui/redaction/RedactionView$RedactionViewListener;

    if-eqz p1, :cond_0

    .line 4
    iget-boolean v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionPreviewEnabled:Z

    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/redaction/RedactionView$RedactionViewListener;->onPreviewModeChanged(Z)V

    :cond_0
    return-void
.end method

.method synthetic lambda$setRedactionButtonVisible$6$com-pspdfkit-ui-redaction-RedactionView(ZZ)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/ui/redaction/RedactionView;->setRedactionButtonVisible(ZZ)V

    return-void
.end method

.method synthetic lambda$setRedactionButtonVisible$7$com-pspdfkit-ui-redaction-RedactionView()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->onRedactionButtonVisibilityChangedListener:Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;->onRedactionButtonSlidingInside()V

    :cond_0
    return-void
.end method

.method synthetic lambda$setRedactionButtonVisible$8$com-pspdfkit-ui-redaction-RedactionView()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->onRedactionButtonVisibilityChangedListener:Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;->onRedactionButtonSlidingOutside()V

    :cond_0
    return-void
.end method

.method synthetic lambda$setRedactionButtonVisible$9$com-pspdfkit-ui-redaction-RedactionView()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionContainer:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setBottomOffset(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    neg-int p1, p1

    int-to-float p1, p1

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method public setListener(Lcom/pspdfkit/ui/redaction/RedactionView$RedactionViewListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionViewListener:Lcom/pspdfkit/ui/redaction/RedactionView$RedactionViewListener;

    return-void
.end method

.method public setOnRedactionButtonVisibilityChangedListener(Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->onRedactionButtonVisibilityChangedListener:Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;

    return-void
.end method

.method public setRedactionAnnotationPreviewEnabled(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionPreviewEnabled:Z

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/ui/redaction/RedactionView;->updatePreviewText()V

    return-void
.end method

.method public setRedactionButtonVisible(ZZ)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/ui/redaction/RedactionView;ZZ)V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void

    :cond_0
    const-wide/16 v0, 0xfa

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    if-eqz p1, :cond_2

    .line 8
    iget-boolean v5, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isVisible:Z

    if-nez v5, :cond_2

    const/4 p1, 0x1

    .line 9
    iput-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isVisible:Z

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1, v5}, Landroid/widget/LinearLayout;->setTranslationX(F)V

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionContainer:Landroid/widget/LinearLayout;

    .line 13
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    if-eqz p2, :cond_1

    goto :goto_0

    :cond_1
    move-wide v0, v2

    .line 14
    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v0, 0x3fc00000    # 1.5f

    invoke-direct {p2, v0}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    .line 15
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->openRedactButton:Landroid/view/View;

    .line 16
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result p2

    .line 17
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x30

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v0

    sub-int/2addr p2, v0

    int-to-float p2, p2

    .line 18
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda6;

    invoke-direct {p2, p0}, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/ui/redaction/RedactionView;)V

    .line 20
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const/4 p2, 0x0

    .line 25
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    goto :goto_2

    :cond_2
    if-nez p1, :cond_4

    .line 26
    iget-boolean p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isVisible:Z

    if-eqz p1, :cond_4

    .line 27
    iput-boolean v4, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->isVisible:Z

    .line 28
    iget-object p1, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionContainer:Landroid/widget/LinearLayout;

    .line 29
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    if-eqz p2, :cond_3

    goto :goto_1

    :cond_3
    move-wide v0, v2

    .line 30
    :goto_1
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance p2, Landroid/view/animation/AccelerateInterpolator;

    const v0, 0x3fb33333    # 1.4f

    invoke-direct {p2, v0}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    .line 31
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/ui/redaction/RedactionView;->redactionContainer:Landroid/widget/LinearLayout;

    .line 32
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result p2

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda7;

    invoke-direct {p2, p0}, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/ui/redaction/RedactionView;)V

    .line 33
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda8;

    invoke-direct {p2, p0}, Lcom/pspdfkit/ui/redaction/RedactionView$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/ui/redaction/RedactionView;)V

    .line 38
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 39
    invoke-virtual {p0, v4}, Lcom/pspdfkit/ui/redaction/RedactionView;->collapseRedactionOptions(Z)V

    :cond_4
    :goto_2
    return-void
.end method

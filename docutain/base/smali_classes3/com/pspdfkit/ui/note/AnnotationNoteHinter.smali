.class public Lcom/pspdfkit/ui/note/AnnotationNoteHinter;
.super Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# instance fields
.field private final annotationNoteHinterThemeConfiguration:Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;

.field private final commentThreadIcon:Landroid/graphics/drawable/Drawable;

.field private final drawableCache:Landroidx/collection/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/SparseArrayCompat<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/note/NoteHinterDrawable;",
            ">;>;"
        }
    .end annotation
.end field

.field private final noteIcon:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;-><init>()V

    .line 2
    new-instance v0, Landroidx/collection/SparseArrayCompat;

    invoke-direct {v0}, Landroidx/collection/SparseArrayCompat;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->drawableCache:Landroidx/collection/SparseArrayCompat;

    .line 6
    new-instance v0, Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;

    invoke-direct {v0, p1}, Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->annotationNoteHinterThemeConfiguration:Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;

    .line 7
    iget v1, v0, Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;->noteHinterDrawable:I

    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->noteIcon:Landroid/graphics/drawable/Drawable;

    .line 8
    iget v0, v0, Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;->instantCommentHinterDrawable:I

    .line 9
    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->commentThreadIcon:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method private findCachedDrawableForAnnotation(Ljava/util/List;Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/ui/note/NoteHinterDrawable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/note/NoteHinterDrawable;",
            ">;",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Lcom/pspdfkit/ui/note/NoteHinterDrawable;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 1
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/note/NoteHinterDrawable;

    .line 2
    iget-object v2, v1, Lcom/pspdfkit/ui/note/NoteHinterDrawable;->annotation:Lcom/pspdfkit/annotations/Annotation;

    if-ne v2, p2, :cond_1

    return-object v1

    :cond_2
    return-object v0
.end method

.method private notifyDrawablesChangedIfSupported(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->e(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->notifyDrawablesChanged()V

    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized getDrawablesForPage(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/document/PdfDocument;",
            "I)",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/ui/drawable/PdfDrawable;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 1
    :try_start_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->noteIcon:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-object p1

    .line 4
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->drawableCache:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0, p3}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 5
    invoke-interface {p2}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p2

    invoke-interface {p2, p3}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAnnotations(I)Ljava/util/List;

    move-result-object p2

    .line 6
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 7
    invoke-static {v1}, Lcom/pspdfkit/internal/ao;->e(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 8
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->findCachedDrawableForAnnotation(Ljava/util/List;Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/ui/note/NoteHinterDrawable;

    move-result-object v2

    if-nez v2, :cond_9

    .line 10
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->hasInstantComments()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 11
    new-instance v2, Lcom/pspdfkit/ui/note/NoteTextMarkupAnnotationHinterDrawable;

    iget-object v3, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->commentThreadIcon:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->annotationNoteHinterThemeConfiguration:Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;

    invoke-direct {v2, v3, v1, v4}, Lcom/pspdfkit/ui/note/NoteTextMarkupAnnotationHinterDrawable;-><init>(Landroid/graphics/drawable/Drawable;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;)V

    goto/16 :goto_3

    .line 13
    :cond_2
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v3, v4, :cond_8

    .line 14
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->POLYGON:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v3, v4, :cond_8

    .line 15
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->POLYLINE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v3, v4, :cond_3

    goto :goto_2

    .line 18
    :cond_3
    instance-of v3, v1, Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    if-eqz v3, :cond_4

    .line 19
    new-instance v2, Lcom/pspdfkit/ui/note/NoteTextMarkupAnnotationHinterDrawable;

    iget-object v3, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->noteIcon:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->annotationNoteHinterThemeConfiguration:Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;

    invoke-direct {v2, v3, v1, v4}, Lcom/pspdfkit/ui/note/NoteTextMarkupAnnotationHinterDrawable;-><init>(Landroid/graphics/drawable/Drawable;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;)V

    goto :goto_3

    .line 21
    :cond_4
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->LINE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v3, v4, :cond_5

    .line 22
    new-instance v2, Lcom/pspdfkit/ui/note/NoteLineAnnotationHinterDrawable;

    iget-object v3, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->noteIcon:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->annotationNoteHinterThemeConfiguration:Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;

    invoke-direct {v2, v3, v1, v4}, Lcom/pspdfkit/ui/note/NoteLineAnnotationHinterDrawable;-><init>(Landroid/graphics/drawable/Drawable;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;)V

    goto :goto_3

    .line 24
    :cond_5
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->SQUARE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v3, v4, :cond_7

    .line 25
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->CIRCLE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v3, v4, :cond_6

    goto :goto_1

    .line 28
    :cond_6
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v3, v4, :cond_a

    .line 29
    new-instance v2, Lcom/pspdfkit/ui/note/NoteStampAnnotationHinterDrawable;

    iget-object v3, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->noteIcon:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->annotationNoteHinterThemeConfiguration:Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;

    invoke-direct {v2, v3, v1, v4}, Lcom/pspdfkit/ui/note/NoteStampAnnotationHinterDrawable;-><init>(Landroid/graphics/drawable/Drawable;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;)V

    goto :goto_3

    .line 30
    :cond_7
    :goto_1
    new-instance v2, Lcom/pspdfkit/ui/note/NoteShapeAnnotationHinterDrawable;

    iget-object v3, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->noteIcon:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->annotationNoteHinterThemeConfiguration:Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;

    invoke-direct {v2, v3, v1, v4}, Lcom/pspdfkit/ui/note/NoteShapeAnnotationHinterDrawable;-><init>(Landroid/graphics/drawable/Drawable;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;)V

    goto :goto_3

    .line 31
    :cond_8
    :goto_2
    new-instance v2, Lcom/pspdfkit/ui/note/NoteMultilineAnnotationHinterDrawable;

    iget-object v3, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->noteIcon:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->annotationNoteHinterThemeConfiguration:Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;

    invoke-direct {v2, v3, v1, v4}, Lcom/pspdfkit/ui/note/NoteMultilineAnnotationHinterDrawable;-><init>(Landroid/graphics/drawable/Drawable;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/note/AnnotationNoteHinterThemeConfiguration;)V

    goto :goto_3

    .line 49
    :cond_9
    invoke-virtual {v2}, Lcom/pspdfkit/ui/note/NoteHinterDrawable;->refresh()V

    :cond_a
    :goto_3
    if-eqz v2, :cond_1

    .line 53
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_b
    if-eqz v0, :cond_c

    .line 60
    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 61
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_4
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/note/NoteHinterDrawable;

    .line 62
    invoke-virtual {v0}, Lcom/pspdfkit/ui/note/NoteHinterDrawable;->dispose()V

    goto :goto_4

    .line 67
    :cond_c
    iget-object p2, p0, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->drawableCache:Landroidx/collection/SparseArrayCompat;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p2, p3, v0}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->notifyDrawablesChangedIfSupported(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;->notifyDrawablesChangedIfSupported(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->notifyDrawablesChanged()V

    return-void
.end method

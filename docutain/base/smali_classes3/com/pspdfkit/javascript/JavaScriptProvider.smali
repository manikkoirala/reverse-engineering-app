.class public interface abstract Lcom/pspdfkit/javascript/JavaScriptProvider;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract executeDocumentLevelScripts()V
.end method

.method public abstract executeDocumentLevelScriptsAsync()Lio/reactivex/rxjava3/core/Completable;
.end method

.method public abstract isJavaScriptEnabled()Z
.end method

.method public abstract setJavaScriptEnabled(Z)V
.end method

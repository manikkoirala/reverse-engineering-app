.class public Lcom/pspdfkit/utils/FreeTextAnnotationUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static placeCallOutPoints(Lcom/pspdfkit/annotations/FreeTextAnnotation;)V
    .locals 2

    const-string v0, "freeTextAnnotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result v0

    .line 55
    invoke-static {p0, v0}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;I)V

    return-void
.end method

.method public static resizeToFitText(Lcom/pspdfkit/annotations/FreeTextAnnotation;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;)V
    .locals 3

    const-string v0, "freeTextAnnotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "document"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "widthScaleMode"

    .line 108
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "heightScaleMode"

    .line 161
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    invoke-static {p3, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 213
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/pspdfkit/document/PdfDocument;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    .line 214
    invoke-static {p0, p1, p2, p3, v2}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;Landroid/text/TextPaint;)V

    return-void
.end method

.class final Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->g()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$c;->a:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 4

    .line 1
    check-cast p1, Ljava/lang/Throwable;

    const-string v0, "it"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v2, "PSPDFKit.ContentEditing"

    const-string v3, "Saving content to document failed."

    .line 354
    invoke-static {v2, p1, v3, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 355
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$c;->a:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    sget v0, Lcom/pspdfkit/R$string;->pspdf__document_could_not_be_saved:I

    .line 356
    iget-object v2, p1, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    .line 357
    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 358
    iget-object p1, p1, Lcom/pspdfkit/internal/specialMode/handler/d;->c:Lcom/pspdfkit/internal/ms;

    invoke-interface {p1}, Lcom/pspdfkit/internal/ms;->exitCurrentlyActiveMode()V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;
.super Landroidx/lifecycle/ViewModel;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lcom/pspdfkit/internal/jni/NativeContentEditor;

.field private final b:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->b:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->a:Lcom/pspdfkit/internal/jni/NativeContentEditor;

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/jni/NativeContentEditor;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->a:Lcom/pspdfkit/internal/jni/NativeContentEditor;

    return-void
.end method

.method public final b()Lcom/pspdfkit/internal/jni/NativeContentEditor;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->a:Lcom/pspdfkit/internal/jni/NativeContentEditor;

    return-object v0
.end method

.method public final c()Ljava/util/HashMap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final onCleared()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->a()V

    return-void
.end method

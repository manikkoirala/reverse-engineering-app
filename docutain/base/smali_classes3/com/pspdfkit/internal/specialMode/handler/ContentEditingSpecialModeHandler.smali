.class public final Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;
.super Lcom/pspdfkit/internal/specialMode/handler/d;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;
.implements Lcom/pspdfkit/internal/a7;
.implements Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;
.implements Landroidx/lifecycle/DefaultLifecycleObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;,
        Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$b;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u0005:\u0001\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;",
        "Lcom/pspdfkit/internal/specialMode/handler/d;",
        "Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;",
        "Lcom/pspdfkit/internal/a7;",
        "Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;",
        "Landroidx/lifecycle/DefaultLifecycleObserver;",
        "a",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# instance fields
.field private final e:Lcom/pspdfkit/internal/f6;

.field private final f:Lcom/pspdfkit/ui/PdfFragment;

.field private final g:Lkotlin/Lazy;

.field private final h:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

.field private final i:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

.field private final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/internal/k6;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

.field private l:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;

.field private m:Ljava/util/UUID;

.field private n:Lkotlin/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private o:Landroidx/appcompat/app/AlertDialog;

.field private final p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/internal/db;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$0KNU9Iuw_-K9LbMWtT0iXNaaUqU(Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$0LDHXml2sSboCNTYdLKSYXbZHkM(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$Iecc3oLlZkSEuedh3Khv_HS9vUc(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->b(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$V1ruCZ5Dv_Sx25rkN2GwLcLaY1I(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V

    return-void
.end method

.method public static synthetic $r8$lambda$arRqAw-uBeuMN4KsUQz_LlgY3X8(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->b(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V

    return-void
.end method

.method public static synthetic $r8$lambda$wGuZKUxv_n0RqOyJjPbnD3i5CZM(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;Landroid/content/DialogInterface;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/b7;Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 4

    const-string v0, "contentEditingEventDispatcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fragment"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p2}, Lcom/pspdfkit/ui/PdfFragment;->getContentEditingUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type com.pspdfkit.internal.undo.annotations.OnEditRecordedListener"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/pspdfkit/internal/fl;

    invoke-direct {p0, v0, p2, v1}, Lcom/pspdfkit/internal/specialMode/handler/d;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ms;Lcom/pspdfkit/internal/fl;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->e:Lcom/pspdfkit/internal/f6;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f:Lcom/pspdfkit/ui/PdfFragment;

    .line 512
    new-instance p1, Lcom/pspdfkit/internal/r6;

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/r6;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 516
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    new-instance v1, Lcom/pspdfkit/internal/s6;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/s6;-><init>(Lcom/pspdfkit/internal/r6;)V

    invoke-static {v0, v1}, Lkotlin/LazyKt;->lazy(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    .line 517
    const-class v0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/t6;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/t6;-><init>(Lkotlin/Lazy;)V

    new-instance v2, Lcom/pspdfkit/internal/u6;

    invoke-direct {v2, p1}, Lcom/pspdfkit/internal/u6;-><init>(Lkotlin/Lazy;)V

    new-instance v3, Lcom/pspdfkit/internal/v6;

    invoke-direct {v3, p2, p1}, Lcom/pspdfkit/internal/v6;-><init>(Landroidx/fragment/app/Fragment;Lkotlin/Lazy;)V

    invoke-static {p2, v0, v1, v2, v3}, Landroidx/fragment/app/FragmentViewModelLazyKt;->createViewModelLazy(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    .line 518
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->g:Lkotlin/Lazy;

    .line 519
    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f()Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->h:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    .line 520
    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f()Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->i:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    .line 521
    new-instance p1, Ljava/util/ArrayList;

    const/4 p2, 0x5

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->j:Ljava/util/ArrayList;

    .line 522
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->p:Ljava/util/ArrayList;

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<InputType:",
            "Ljava/lang/Object;",
            "ResultType:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/pspdfkit/internal/z5<",
            "TInputType;TResultType;>;)",
            "Lcom/pspdfkit/internal/o6<",
            "TResultType;>;"
        }
    .end annotation

    .line 1360
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->h:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->b()Lcom/pspdfkit/internal/jni/NativeContentEditor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1361
    monitor-enter v0

    const/4 v1, 0x0

    .line 1363
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1365
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/z5;->a(Lcom/pspdfkit/internal/jni/NativeContentEditor;)Lcom/pspdfkit/internal/jni/NativeContentEditingResult;

    move-result-object v4

    .line 1366
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v2

    .line 1371
    invoke-virtual {p1, v4}, Lcom/pspdfkit/internal/z5;->a(Lcom/pspdfkit/internal/jni/NativeContentEditingResult;)Ljava/lang/Object;

    move-result-object v7

    .line 1373
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v2

    sub-long v2, v8, v5

    const-string v10, "PSPDFKit.ContentEditing"

    .line 1377
    invoke-virtual {p1}, Lcom/pspdfkit/internal/z5;->d()Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    move-result-object v11

    invoke-virtual {p1}, Lcom/pspdfkit/internal/z5;->a()Ljava/lang/String;

    move-result-object v12

    .line 1378
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v11, " "

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, " executed in "

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v8, " ms (native execution = "

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v5, " ms, conversion = "

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " ms."

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    .line 1379
    invoke-static {v10, v2, v3}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1384
    new-instance v2, Lcom/pspdfkit/internal/o6;

    invoke-direct {v2, v7, v4}, Lcom/pspdfkit/internal/o6;-><init>(Ljava/lang/Object;Lcom/pspdfkit/internal/jni/NativeContentEditingResult;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v2

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_1
    const-string v3, "PSPDFKit.ContentEditing"

    .line 1386
    invoke-virtual {p1}, Lcom/pspdfkit/internal/z5;->d()Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    move-result-object v4

    invoke-virtual {p1}, Lcom/pspdfkit/internal/z5;->a()Ljava/lang/String;

    move-result-object p1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error on executing "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3, v2, p1, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1387
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit v0

    throw p1

    .line 1388
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string v0, "Need to instantiate native content editor first"

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static final a(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1518
    invoke-interface {p0}, Landroid/content/DialogInterface;->cancel()V

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/l6;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1542
    :try_start_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/e6;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/l6;->c(Z)I

    move-result v2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/l6;->b()Lcom/pspdfkit/internal/cb;

    move-result-object v3

    const-string v4, "testBlockId"

    .line 1543
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "externalControlState"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2008
    new-instance v4, Lcom/pspdfkit/internal/wp;

    const/4 v5, 0x0

    invoke-direct {v4, v1, v2, v3, v5}, Lcom/pspdfkit/internal/wp;-><init>(Ljava/util/UUID;ILcom/pspdfkit/internal/cb;I)V

    invoke-direct {p0, v4}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;

    move-result-object v1

    .line 2009
    invoke-virtual {v1}, Lcom/pspdfkit/internal/o6;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/av;

    .line 2010
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->i:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 2011
    iget v3, p1, Lcom/pspdfkit/internal/zl;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lcom/pspdfkit/internal/e6;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/pt;

    if-nez v2, :cond_0

    goto :goto_2

    .line 2012
    :cond_0
    iget-object v3, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->j:Ljava/util/ArrayList;

    .line 2013
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Lcom/pspdfkit/internal/k6;

    .line 2014
    invoke-virtual {v6}, Lcom/pspdfkit/internal/k6;->getPageIndex()I

    move-result v6

    iget v7, p1, Lcom/pspdfkit/internal/zl;->a:I

    if-ne v6, v7, :cond_2

    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    :goto_0
    if-eqz v6, :cond_1

    goto :goto_1

    :cond_3
    move-object v4, v0

    :goto_1
    check-cast v4, Lcom/pspdfkit/internal/k6;

    if-eqz v4, :cond_4

    .line 2017
    invoke-virtual {v4, v2, v1, v5}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/av;Z)V

    .line 2019
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/l6;->b(Z)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/l6;->a(Z)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v4, v1, p1, v5}, Lcom/pspdfkit/internal/k6;->a(Ljava/lang/Integer;Ljava/lang/Integer;Z)Z

    goto :goto_2

    .line 2021
    :cond_4
    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/pt;->a(Lcom/pspdfkit/internal/av;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_2
    return-void

    :catch_0
    move-exception p1

    if-eqz p2, :cond_6

    const-string v0, "undo"

    :cond_6
    if-nez v0, :cond_7

    const-string v0, "redo"

    .line 2024
    :cond_7
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Content Editing "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " operation failed"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;

    invoke-direct {v0, p2, p1}, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static final a(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1492
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f:Lcom/pspdfkit/ui/PdfFragment;

    .line 1493
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1494
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f:Lcom/pspdfkit/ui/PdfFragment;

    .line 1495
    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getDocumentSources()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/ui/PdfFragment;->setCustomPdfSources(Ljava/util/List;)V

    .line 1496
    :cond_0
    iget-object p0, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->c:Lcom/pspdfkit/internal/ms;

    invoke-interface {p0}, Lcom/pspdfkit/internal/ms;->exitCurrentlyActiveMode()V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;Landroid/content/DialogInterface;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1519
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroidx/lifecycle/Lifecycle;->removeObserver(Landroidx/lifecycle/LifecycleObserver;)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;Landroid/content/DialogInterface;I)V
    .locals 0

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x1

    .line 1516
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->finishContentEditingSession(Z)V

    .line 1517
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method private final a(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;Lcom/pspdfkit/internal/jt;)V
    .locals 4

    .line 1520
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->n:Lkotlin/Pair;

    .line 1521
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->k:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

    if-nez v1, :cond_0

    return-void

    .line 1523
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->l:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;

    .line 1525
    sget-object v2, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$b;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v2, p1

    const/4 v2, 0x1

    if-eq p1, v2, :cond_3

    const/4 v3, 0x2

    if-eq p1, v3, :cond_2

    const/4 v3, 0x3

    if-eq p1, v3, :cond_1

    goto :goto_0

    .line 1528
    :cond_1
    invoke-interface {v1, v2, p2}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;->displayColorPicker(ZLcom/pspdfkit/internal/jt;)V

    goto :goto_0

    .line 1529
    :cond_2
    invoke-interface {v1, v2, p2}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;->displayFontSizesSheet(ZLcom/pspdfkit/internal/jt;)V

    goto :goto_0

    .line 1530
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->p:Ljava/util/ArrayList;

    .line 1531
    invoke-interface {v1, v2, p1, p2}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;->displayFontNamesSheet(ZLjava/util/List;Lcom/pspdfkit/internal/jt;)V

    :goto_0
    if-eqz v0, :cond_6

    .line 1532
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->j:Ljava/util/ArrayList;

    .line 1533
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    move-object v1, p2

    check-cast v1, Lcom/pspdfkit/internal/k6;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/k6;->p()Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_1

    :cond_5
    const/4 p2, 0x0

    :goto_1
    check-cast p2, Lcom/pspdfkit/internal/k6;

    if-eqz p2, :cond_6

    .line 1534
    invoke-virtual {v0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {v0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1535
    invoke-virtual {p2, p1, v0, v2}, Lcom/pspdfkit/internal/k6;->a(Ljava/lang/Integer;Ljava/lang/Integer;Z)Z

    :cond_6
    return-void
.end method

.method private static final synthetic b(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V
    .locals 0

    .line 1710
    invoke-virtual {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->h()V

    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;Landroid/content/DialogInterface;I)V
    .locals 0

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1708
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    const/4 p1, 0x0

    .line 1709
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->finishContentEditingSession(Z)V

    return-void
.end method

.method private final e()Lcom/pspdfkit/internal/k6;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->j:Ljava/util/ArrayList;

    .line 2
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/pspdfkit/internal/k6;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/k6;->p()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    check-cast v1, Lcom/pspdfkit/internal/k6;

    return-object v1
.end method

.method private final f()Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->g:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    return-object v0
.end method

.method private final g()V
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 2
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 3
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 4
    new-instance v1, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V

    new-instance v2, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$c;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$c;-><init>(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V

    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private final i()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->p:Ljava/util/ArrayList;

    .line 2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/x3;

    invoke-direct {v0}, Lcom/pspdfkit/internal/x3;-><init>()V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/o6;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->p:Ljava/util/ArrayList;

    .line 5
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->p:Ljava/util/ArrayList;

    .line 8
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->p:Ljava/util/ArrayList;

    const-string v2, "\r\n"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3e

    const/4 v9, 0x0

    .line 10
    invoke-static/range {v1 .. v9}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Available Faces ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "):\r\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.ContentEditing"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/pspdfkit/internal/o6;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/pspdfkit/internal/o6<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/rt;",
            ">;>;"
        }
    .end annotation

    .line 1536
    new-instance v0, Lcom/pspdfkit/internal/ad;

    .line 1537
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1538
    invoke-interface {v1, p1}, Lcom/pspdfkit/document/PdfDocument;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v1

    const-string v2, "requireNotNull(document).getPageSize(pageIndex)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1, v1}, Lcom/pspdfkit/internal/ad;-><init>(ILcom/pspdfkit/utils/Size;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(ILcom/pspdfkit/internal/pt;Landroid/graphics/Matrix;Lcom/pspdfkit/utils/Size;ZLcom/pspdfkit/internal/uq;Lcom/pspdfkit/internal/m7;)Lcom/pspdfkit/internal/o6;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/pspdfkit/internal/pt;",
            "Landroid/graphics/Matrix;",
            "Lcom/pspdfkit/utils/Size;",
            "Z",
            "Lcom/pspdfkit/internal/uq;",
            "Lcom/pspdfkit/internal/m7;",
            ")",
            "Lcom/pspdfkit/internal/o6<",
            "Lcom/pspdfkit/internal/ip;",
            ">;"
        }
    .end annotation

    const-string v0, "textblock"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transformation"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pageSize"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1539
    new-instance v0, Lcom/pspdfkit/internal/gp;

    move-object v1, v0

    move v2, p1

    move v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/pspdfkit/internal/gp;-><init>(ILcom/pspdfkit/internal/pt;Landroid/graphics/Matrix;Lcom/pspdfkit/utils/Size;ZLcom/pspdfkit/internal/uq;Lcom/pspdfkit/internal/m7;)V

    move-object v1, p0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/pspdfkit/internal/pt;II)Lcom/pspdfkit/internal/o6;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/pt;",
            "II)",
            "Lcom/pspdfkit/internal/o6<",
            "Lcom/pspdfkit/internal/av;",
            ">;"
        }
    .end annotation

    const-string v0, "textBlock"

    .line 466
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 528
    new-instance v1, Lcom/pspdfkit/internal/l5;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/pt;->a(I)I

    move-result p2

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/pt;->a(I)I

    move-result p3

    invoke-direct {v1, p2, p3}, Lcom/pspdfkit/internal/l5;-><init>(II)V

    .line 529
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "clusterRange"

    invoke-static {v1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 594
    invoke-virtual {v1}, Lcom/pspdfkit/internal/l5;->a()I

    move-result p2

    invoke-virtual {v1}, Lcom/pspdfkit/internal/l5;->b()I

    move-result p3

    const/4 v2, 0x0

    if-ne p2, p3, :cond_0

    .line 595
    invoke-virtual {v1}, Lcom/pspdfkit/internal/l5;->a()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    .line 596
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 977
    new-instance p3, Lcom/pspdfkit/internal/tg;

    invoke-direct {p3, p1, p2, v2}, Lcom/pspdfkit/internal/tg;-><init>(Lcom/pspdfkit/internal/pt;Ljava/lang/Integer;Lcom/pspdfkit/internal/l5;)V

    invoke-direct {p0, p3}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;

    move-result-object p1

    goto :goto_0

    .line 978
    :cond_0
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1359
    new-instance p2, Lcom/pspdfkit/internal/tg;

    invoke-direct {p2, p1, v2, v1}, Lcom/pspdfkit/internal/tg;-><init>(Lcom/pspdfkit/internal/pt;Ljava/lang/Integer;Lcom/pspdfkit/internal/l5;)V

    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final a(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/jt;)Lcom/pspdfkit/internal/o6;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/pt;",
            "Lcom/pspdfkit/internal/jt;",
            ")",
            "Lcom/pspdfkit/internal/o6<",
            "Lcom/pspdfkit/internal/av;",
            ">;"
        }
    .end annotation

    const-string v0, "textBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "styleInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1540
    new-instance v0, Lcom/pspdfkit/internal/q2;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/internal/q2;-><init>(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/jt;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/internal/pt;Ljava/lang/String;I)Lcom/pspdfkit/internal/o6;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/pt;",
            "Ljava/lang/String;",
            "I)",
            "Lcom/pspdfkit/internal/o6<",
            "Lcom/pspdfkit/internal/av;",
            ">;"
        }
    .end annotation

    const-string v0, "textblock"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "text"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/pt;->a(I)I

    move-result p3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    .line 86
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 461
    new-instance v0, Lcom/pspdfkit/internal/ke;

    invoke-direct {v0, p1, p2, p3}, Lcom/pspdfkit/internal/ke;-><init>(Lcom/pspdfkit/internal/pt;Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/internal/pt;Ljava/lang/String;II)Lcom/pspdfkit/internal/o6;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/pt;",
            "Ljava/lang/String;",
            "II)",
            "Lcom/pspdfkit/internal/o6<",
            "Lcom/pspdfkit/internal/av;",
            ">;"
        }
    .end annotation

    const-string v0, "textBlock"

    .line 462
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 463
    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/pt;->a(I)I

    move-result p3

    .line 464
    invoke-virtual {p1, p4}, Lcom/pspdfkit/internal/pt;->a(I)I

    move-result p4

    .line 465
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->b(Lcom/pspdfkit/internal/pt;Ljava/lang/String;II)Lcom/pspdfkit/internal/o6;

    move-result-object p1

    return-object p1
.end method

.method public final a(ILcom/pspdfkit/internal/pt;)V
    .locals 2

    const-string v0, "textBlock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1389
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->i:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1390
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 1437
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1438
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1488
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1489
    :cond_0
    check-cast v1, Ljava/util/Map;

    .line 1490
    invoke-virtual {p2}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object p1

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1491
    invoke-virtual {p2}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->onContentChange(Ljava/util/UUID;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/k6;)V
    .locals 1

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1507
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1509
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->j:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1514
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->e:Lcom/pspdfkit/internal/f6;

    check-cast p1, Lcom/pspdfkit/internal/b7;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/b7;->a(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V

    .line 1515
    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f()Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->a()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/l6;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;
        }
    .end annotation

    const-string v0, "edit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 1541
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/l6;Z)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/k6;)V
    .locals 2

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1497
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1498
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1499
    iget-object p2, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->h:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->b()Lcom/pspdfkit/internal/jni/NativeContentEditor;

    move-result-object p2

    if-nez p2, :cond_1

    .line 1500
    invoke-virtual {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object p2

    invoke-interface {p2}, Lcom/pspdfkit/undo/UndoManager;->clearHistory()V

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.ContentEditing"

    const-string v1, "Creating native content editor"

    .line 1501
    invoke-static {v0, v1, p2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1502
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/jni/NativeContentEditor;->create(Lcom/pspdfkit/internal/jni/NativeDocument;)Lcom/pspdfkit/internal/jni/NativeContentEditor;

    move-result-object p1

    .line 1503
    iget-object p2, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->h:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->a(Lcom/pspdfkit/internal/jni/NativeContentEditor;)V

    .line 1504
    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->i()V

    .line 1505
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->j:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_2

    .line 1506
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->e:Lcom/pspdfkit/internal/f6;

    check-cast p1, Lcom/pspdfkit/internal/b7;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/b7;->a(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V

    :cond_2
    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/pt;II)Lcom/pspdfkit/internal/o6;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/pt;",
            "II)",
            "Lcom/pspdfkit/internal/o6<",
            "Lcom/pspdfkit/internal/av;",
            ">;"
        }
    .end annotation

    const-string v0, "textblock"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/pt;->a(I)I

    move-result p2

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/pt;->a(I)I

    move-result p3

    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 381
    new-instance v0, Lcom/pspdfkit/internal/c8;

    invoke-direct {v0, p1, p2, p3}, Lcom/pspdfkit/internal/c8;-><init>(Lcom/pspdfkit/internal/pt;II)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;

    move-result-object p1

    return-object p1
.end method

.method public final b(Lcom/pspdfkit/internal/pt;Ljava/lang/String;II)Lcom/pspdfkit/internal/o6;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/pt;",
            "Ljava/lang/String;",
            "II)",
            "Lcom/pspdfkit/internal/o6<",
            "Lcom/pspdfkit/internal/av;",
            ">;"
        }
    .end annotation

    const-string v0, "textBlock"

    .line 382
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "text"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 492
    new-instance v2, Lcom/pspdfkit/internal/l5;

    invoke-direct {v2, p3, p4}, Lcom/pspdfkit/internal/l5;-><init>(II)V

    .line 493
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "clusterRange"

    invoke-static {v2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 558
    invoke-virtual {v2}, Lcom/pspdfkit/internal/l5;->a()I

    move-result p3

    invoke-virtual {v2}, Lcom/pspdfkit/internal/l5;->b()I

    move-result p4

    const/4 v3, 0x0

    if-ne p3, p4, :cond_0

    .line 559
    invoke-virtual {v2}, Lcom/pspdfkit/internal/l5;->a()I

    move-result p3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    .line 560
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 941
    new-instance p4, Lcom/pspdfkit/internal/tg;

    invoke-direct {p4, p1, p3, v3}, Lcom/pspdfkit/internal/tg;-><init>(Lcom/pspdfkit/internal/pt;Ljava/lang/Integer;Lcom/pspdfkit/internal/l5;)V

    invoke-direct {p0, p4}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;

    move-result-object p3

    goto :goto_0

    .line 942
    :cond_0
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1323
    new-instance p3, Lcom/pspdfkit/internal/tg;

    invoke-direct {p3, p1, v3, v2}, Lcom/pspdfkit/internal/tg;-><init>(Lcom/pspdfkit/internal/pt;Ljava/lang/Integer;Lcom/pspdfkit/internal/l5;)V

    invoke-direct {p0, p3}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;

    move-result-object p3

    .line 1324
    :goto_0
    invoke-virtual {p3}, Lcom/pspdfkit/internal/o6;->a()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/pspdfkit/internal/av;

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/pt;->c(Lcom/pspdfkit/internal/av;)Lcom/pspdfkit/internal/jt;

    const-string p3, "textblock"

    .line 1325
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1700
    new-instance p3, Lcom/pspdfkit/internal/ke;

    invoke-direct {p3, p1, p2, v3}, Lcom/pspdfkit/internal/ke;-><init>(Lcom/pspdfkit/internal/pt;Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-direct {p0, p3}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;

    move-result-object p2

    .line 1701
    invoke-virtual {p2}, Lcom/pspdfkit/internal/o6;->a()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/pspdfkit/internal/av;

    invoke-virtual {p1, p3, v3}, Lcom/pspdfkit/internal/pt;->a(Lcom/pspdfkit/internal/av;Lcom/pspdfkit/utils/Size;)V

    return-object p2
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 1

    .line 1704
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final b(I)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Map<",
            "Ljava/util/UUID;",
            "Lcom/pspdfkit/internal/pt;",
            ">;"
        }
    .end annotation

    .line 1702
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->i:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1703
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public final b(Lcom/pspdfkit/internal/k6;)V
    .locals 1

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1705
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1707
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->j:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/l6;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;
        }
    .end annotation

    const-string v0, "edit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1711
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/l6;Z)V

    return-void
.end method

.method public final bindContentEditingInspectorController(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;)V
    .locals 1

    const-string v0, "contentEditingInspectorController"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->k:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

    return-void
.end method

.method public final c()Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->k:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

    return-object v0
.end method

.method public final clearContentEditing()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->e()Lcom/pspdfkit/internal/k6;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k6;->g()V

    :cond_0
    return-void
.end method

.method public final d()Landroid/content/Context;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    const-string v1, "super.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final displayColorPicker(Lcom/pspdfkit/internal/jt;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;->FONT_COLOR:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;

    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;Lcom/pspdfkit/internal/jt;)V

    return-void
.end method

.method public final displayFontNamesSheet(Lcom/pspdfkit/internal/jt;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;->FONT_NAME:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;

    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;Lcom/pspdfkit/internal/jt;)V

    return-void
.end method

.method public final displayFontSizesSheet(Lcom/pspdfkit/internal/jt;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;->FONT_SIZE:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;

    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;Lcom/pspdfkit/internal/jt;)V

    return-void
.end method

.method public final finishContentEditingSession()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->isValidForEditing()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 3
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/undo/UndoManager;->canUndo()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    .line 4
    new-instance v0, Landroidx/appcompat/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    sget v3, Lcom/pspdfkit/R$string;->pspdf__contentediting_confirm_discard_changes:I

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    .line 6
    invoke-static {v1}, Lcom/pspdfkit/internal/r2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 7
    invoke-static {v1}, Lcom/pspdfkit/internal/r2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v7, 0x2e

    .line 8
    invoke-virtual {v1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    const/4 v8, 0x0

    if-ge v7, v4, :cond_2

    goto :goto_1

    .line 9
    :cond_2
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 10
    :goto_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    const/16 v9, 0x5f

    .line 11
    invoke-virtual {v7, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    add-int/2addr v9, v4

    if-lez v9, :cond_3

    .line 12
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    sub-int/2addr v7, v9

    const/16 v10, 0x28

    if-ne v7, v10, :cond_3

    .line 13
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v9

    invoke-virtual {v6, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    :cond_3
    aput-object v6, v5, v8

    .line 14
    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 15
    invoke-virtual {v0, v4}, Landroidx/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 16
    sget v1, Lcom/pspdfkit/R$string;->pspdf__save:I

    new-instance v2, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V

    invoke-virtual {v0, v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 20
    sget v1, Lcom/pspdfkit/R$string;->pspdf__cancel:I

    new-instance v2, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$$ExternalSyntheticLambda1;

    invoke-direct {v2}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$$ExternalSyntheticLambda1;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 23
    sget v1, Lcom/pspdfkit/R$string;->pspdf__discard_changes:I

    new-instance v2, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V

    invoke-virtual {v0, v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Landroidx/appcompat/app/AlertDialog$Builder;->show()Landroidx/appcompat/app/AlertDialog;

    move-result-object v0

    .line 31
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->o:Landroidx/appcompat/app/AlertDialog;

    .line 52
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    goto :goto_2

    .line 53
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->c:Lcom/pspdfkit/internal/ms;

    invoke-interface {v0}, Lcom/pspdfkit/internal/ms;->exitCurrentlyActiveMode()V

    :goto_2
    return-void
.end method

.method public final finishContentEditingSession(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 60
    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->g()V

    goto :goto_0

    .line 61
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->c:Lcom/pspdfkit/internal/ms;

    invoke-interface {p1}, Lcom/pspdfkit/internal/ms;->exitCurrentlyActiveMode()V

    :goto_0
    return-void
.end method

.method public final getActiveContentEditingStylingItem()Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->l:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingStylingBarItem;

    if-nez v0, :cond_0

    const-string v0, "contentEditingStylingBarItem"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public final getContentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->e:Lcom/pspdfkit/internal/f6;

    return-object v0
.end method

.method public final getCurrentFormatter()Lcom/pspdfkit/internal/h6;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->j:Ljava/util/ArrayList;

    .line 2
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/pspdfkit/internal/k6;

    .line 3
    invoke-virtual {v3}, Lcom/pspdfkit/internal/k6;->k()Lcom/pspdfkit/internal/views/contentediting/a;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_0

    goto :goto_1

    :cond_2
    move-object v1, v2

    :goto_1
    check-cast v1, Lcom/pspdfkit/internal/k6;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/pspdfkit/internal/k6;->k()Lcom/pspdfkit/internal/views/contentediting/a;

    move-result-object v2

    :cond_3
    return-object v2
.end method

.method public final getCurrentStyleInfo()Lcom/pspdfkit/internal/jt;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->e()Lcom/pspdfkit/internal/k6;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k6;->l()Lcom/pspdfkit/internal/jt;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getFragment()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f:Lcom/pspdfkit/ui/PdfFragment;

    return-object v0
.end method

.method public final getUndoManager()Lcom/pspdfkit/undo/UndoManager;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getContentEditingUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v0

    const-string v1, "fragment.contentEditingUndoManager"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final h()V
    .locals 8

    const-string v0, "PSPDFKit.ContentEditing"

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v1

    const-string v2, "Required value was null."

    if-eqz v1, :cond_7

    .line 2
    iget-object v3, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    const-string v4, "pdf"

    invoke-static {v3, v4}, Lcom/pspdfkit/internal/kb;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    const-string v2, "requireNotNull(FileUtils\u2026FilePath(context, \"pdf\"))"

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->i:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler$a;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 4
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    .line 515
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 516
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 517
    check-cast v5, Ljava/util/Map;

    .line 518
    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    .line 1032
    invoke-static {v4, v5}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    :cond_0
    const-string v2, "path"

    .line 1033
    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "textBlocks"

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1495
    new-instance v2, Lcom/pspdfkit/internal/kq;

    invoke-direct {v2, v3, v4}, Lcom/pspdfkit/internal/kq;-><init>(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/z5;)Lcom/pspdfkit/internal/o6;

    move-result-object v2

    .line 1496
    invoke-virtual {v2}, Lcom/pspdfkit/internal/o6;->c()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1501
    invoke-interface {v1}, Lcom/pspdfkit/document/PdfDocument;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/document/DocumentSource;->isFileSource()Z

    move-result v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v2, :cond_2

    .line 1502
    invoke-interface {v1}, Lcom/pspdfkit/document/PdfDocument;->isValidForEditing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1503
    invoke-interface {v1}, Lcom/pspdfkit/document/PdfDocument;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "doc.documentSource.fileUri"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1504
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    new-array v2, v5, [Landroid/net/Uri;

    aput-object v0, v2, v6

    invoke-static {v1, v5, v2}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Z[Landroid/net/Uri;)V

    .line 1505
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1506
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/kb;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v0, v4}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1507
    invoke-static {v1, v4}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    goto :goto_2

    :catchall_0
    move-exception v2

    .line 1508
    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-static {v0, v2}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception v0

    .line 1509
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :catchall_3
    move-exception v2

    invoke-static {v1, v0}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v2

    .line 1510
    :cond_1
    new-instance v0, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string v1, "Content Editing - SaveToDocument: document is not valid for editing."

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1511
    :cond_2
    invoke-interface {v1}, Lcom/pspdfkit/document/PdfDocument;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v2

    instance-of v7, v2, Lcom/pspdfkit/document/providers/WritableDataProvider;

    if-eqz v7, :cond_3

    check-cast v2, Lcom/pspdfkit/document/providers/WritableDataProvider;

    invoke-interface {v2}, Lcom/pspdfkit/document/providers/WritableDataProvider;->canWrite()Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_4

    .line 1512
    invoke-interface {v1}, Lcom/pspdfkit/document/PdfDocument;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type com.pspdfkit.document.providers.WritableDataProvider"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/pspdfkit/document/providers/WritableDataProvider;

    .line 1513
    sget-object v2, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;->REWRITE_FILE:Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    invoke-interface {v1, v2}, Lcom/pspdfkit/document/providers/WritableDataProvider;->startWrite(Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;)Z

    .line 1515
    :try_start_6
    new-instance v2, Ljava/io/FileInputStream;

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    :try_start_7
    invoke-static {v2, v1}, Lcom/pspdfkit/internal/kb;->a(Ljava/io/FileInputStream;Lcom/pspdfkit/document/providers/WritableDataProvider;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    :try_start_8
    invoke-static {v2, v4}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_6

    .line 1523
    invoke-interface {v1}, Lcom/pspdfkit/document/providers/WritableDataProvider;->finishWrite()Z

    .line 1535
    :goto_2
    :try_start_9
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    :catch_0
    return-void

    :catchall_4
    move-exception v3

    .line 1536
    :try_start_a
    throw v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    :catchall_5
    move-exception v4

    :try_start_b
    invoke-static {v2, v3}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v4
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    :catchall_6
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v2

    :try_start_c
    const-string v3, "Error while writing."

    new-array v4, v6, [Ljava/lang/Object;

    .line 1541
    invoke-static {v0, v2, v3, v4}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1542
    throw v2

    :catch_2
    move-exception v2

    const-string v3, "Error while opening cached file."

    new-array v4, v6, [Ljava/lang/Object;

    .line 1543
    invoke-static {v0, v2, v3, v4}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1544
    throw v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    .line 1549
    :goto_3
    invoke-interface {v1}, Lcom/pspdfkit/document/providers/WritableDataProvider;->finishWrite()Z

    throw v0

    .line 1554
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Saving content changes in place can be applied only when the source is a file Uri or a data provider that supports saving."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1555
    :cond_5
    new-instance v0, Lcom/pspdfkit/exceptions/PSPDFKitException;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/o6;->b()Lcom/pspdfkit/internal/jni/NativeContentEditingResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeContentEditingResult;->getError()Lcom/pspdfkit/internal/jni/NativeContentEditingError;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not write temporary file "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1556
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1557
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final hasUnsavedChanges()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/undo/UndoManager;->canUndo()Z

    move-result v0

    return v0
.end method

.method public final isBoldStyleButtonEnabled(Lcom/pspdfkit/internal/jt;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 1
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->j()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    move-object v1, p1

    goto :goto_0

    :cond_1
    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_4

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->p:Ljava/util/ArrayList;

    .line 3
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/pspdfkit/internal/db;

    .line 4
    invoke-virtual {v4}, Lcom/pspdfkit/internal/db;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v2, v3

    .line 5
    :cond_3
    check-cast v2, Lcom/pspdfkit/internal/db;

    :cond_4
    if-nez v2, :cond_5

    return v0

    .line 6
    :cond_5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->a()Ljava/lang/Boolean;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->f()Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    invoke-virtual {v2, v0, p1}, Lcom/pspdfkit/internal/db;->a(ZZ)Z

    move-result p1

    return p1
.end method

.method public final isClearContentEditingEnabled()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->e()Lcom/pspdfkit/internal/k6;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isItalicStyleButtonEnabled(Lcom/pspdfkit/internal/jt;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 1
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->j()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    move-object v1, p1

    goto :goto_0

    :cond_1
    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_4

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->p:Ljava/util/ArrayList;

    .line 3
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/pspdfkit/internal/db;

    .line 4
    invoke-virtual {v4}, Lcom/pspdfkit/internal/db;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v2, v3

    .line 5
    :cond_3
    check-cast v2, Lcom/pspdfkit/internal/db;

    :cond_4
    if-nez v2, :cond_5

    return v0

    .line 6
    :cond_5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->f()Ljava/lang/Boolean;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->a()Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, p1, v0}, Lcom/pspdfkit/internal/db;->a(ZZ)Z

    move-result p1

    return p1
.end method

.method public final isRedoEnabled()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/undo/UndoManager;->canRedo()Z

    move-result v0

    return v0
.end method

.method public final isSaveEnabled()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/undo/UndoManager;->canUndo()Z

    move-result v0

    return v0
.end method

.method public final isUndoEnabled()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/undo/UndoManager;->canUndo()Z

    move-result v0

    return v0
.end method

.method public final onContentChange(Ljava/util/UUID;)V
    .locals 1

    const-string v0, "contentID"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->e:Lcom/pspdfkit/internal/f6;

    check-cast v0, Lcom/pspdfkit/internal/b7;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/b7;->a(Ljava/util/UUID;)V

    return-void
.end method

.method public final onContentSelectionChange(Ljava/util/UUID;IILcom/pspdfkit/internal/jt;Z)V
    .locals 7

    const-string v0, "contentId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "styleInfo"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->m:Ljava/util/UUID;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    new-instance v0, Lkotlin/Pair;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->n:Lkotlin/Pair;

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->e:Lcom/pspdfkit/internal/f6;

    move-object v1, v0

    check-cast v1, Lcom/pspdfkit/internal/b7;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/pspdfkit/internal/b7;->a(Ljava/util/UUID;IILcom/pspdfkit/internal/jt;Z)V

    return-void
.end method

.method public synthetic onCreate(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    invoke-static {p0, p1}, Landroidx/lifecycle/DefaultLifecycleObserver$-CC;->$default$onCreate(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    return-void
.end method

.method public synthetic onDestroy(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    invoke-static {p0, p1}, Landroidx/lifecycle/DefaultLifecycleObserver$-CC;->$default$onDestroy(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    return-void
.end method

.method public final onDisplayPropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 1

    const-string v0, "inspector"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->m:Ljava/util/UUID;

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->e()Lcom/pspdfkit/internal/k6;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/k6;->onDisplayPropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    :cond_0
    return-void
.end method

.method public final onFinishEditingContentBlock(Ljava/util/UUID;)V
    .locals 1

    const-string v0, "contentId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->m:Ljava/util/UUID;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->m:Ljava/util/UUID;

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->n:Lkotlin/Pair;

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->e:Lcom/pspdfkit/internal/f6;

    check-cast v0, Lcom/pspdfkit/internal/b7;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/b7;->b(Ljava/util/UUID;)V

    return-void
.end method

.method public synthetic onPause(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    invoke-static {p0, p1}, Landroidx/lifecycle/DefaultLifecycleObserver$-CC;->$default$onPause(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    return-void
.end method

.method public final onPreparePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 1

    const-string v0, "inspector"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final onRemovePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 1

    const-string v0, "inspector"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->m:Ljava/util/UUID;

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->e()Lcom/pspdfkit/internal/k6;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/k6;->onRemovePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V

    :cond_0
    return-void
.end method

.method public synthetic onResume(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    invoke-static {p0, p1}, Landroidx/lifecycle/DefaultLifecycleObserver$-CC;->$default$onResume(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    return-void
.end method

.method public synthetic onStart(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    invoke-static {p0, p1}, Landroidx/lifecycle/DefaultLifecycleObserver$-CC;->$default$onStart(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    return-void
.end method

.method public final onStartEditingContentBlock(Ljava/util/UUID;)V
    .locals 1

    const-string v0, "contentId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->m:Ljava/util/UUID;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->e:Lcom/pspdfkit/internal/f6;

    check-cast v0, Lcom/pspdfkit/internal/b7;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/b7;->c(Ljava/util/UUID;)V

    return-void
.end method

.method public final onStop(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 1

    const-string v0, "owner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->f:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroidx/lifecycle/Lifecycle;->removeObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->o:Landroidx/appcompat/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 3
    :cond_0
    invoke-static {p0, p1}, Landroidx/lifecycle/DefaultLifecycleObserver$-CC;->$default$onStop(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    return-void
.end method

.method public final unbindContentEditingInspectorController()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->k:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

    return-void
.end method

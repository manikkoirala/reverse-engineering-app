.class public final Lcom/pspdfkit/internal/specialMode/handler/c;
.super Lcom/pspdfkit/internal/specialMode/handler/d;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;
.implements Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;


# instance fields
.field private final e:Lcom/pspdfkit/ui/PdfFragment;

.field private final f:Lcom/pspdfkit/internal/yb;

.field private g:Lcom/pspdfkit/forms/FormElement;

.field private final h:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/forms/FormType;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/pspdfkit/ui/special_mode/controller/FormElementViewController;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/ac;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/pspdfkit/internal/specialMode/handler/d;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ms;Lcom/pspdfkit/internal/fl;)V

    .line 2
    sget-object p3, Lcom/pspdfkit/forms/FormType;->CHECKBOX:Lcom/pspdfkit/forms/FormType;

    sget-object v0, Lcom/pspdfkit/forms/FormType;->RADIOBUTTON:Lcom/pspdfkit/forms/FormType;

    sget-object v1, Lcom/pspdfkit/forms/FormType;->TEXT:Lcom/pspdfkit/forms/FormType;

    sget-object v2, Lcom/pspdfkit/forms/FormType;->COMBOBOX:Lcom/pspdfkit/forms/FormType;

    sget-object v3, Lcom/pspdfkit/forms/FormType;->LISTBOX:Lcom/pspdfkit/forms/FormType;

    .line 4
    invoke-static {p3, v0, v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->h:Ljava/util/EnumSet;

    .line 18
    iput-object p2, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->e:Lcom/pspdfkit/ui/PdfFragment;

    .line 19
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->f:Lcom/pspdfkit/internal/yb;

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/forms/FormElement;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->i:Lcom/pspdfkit/ui/special_mode/controller/FormElementViewController;

    if-nez p1, :cond_1

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    if-eqz p1, :cond_0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->f:Lcom/pspdfkit/internal/yb;

    check-cast p1, Lcom/pspdfkit/internal/ac;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->c(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->e:Lcom/pspdfkit/ui/PdfFragment;

    .line 6
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalFocusChangeListener(Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;)V

    :cond_0
    return-void

    .line 11
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    if-nez v0, :cond_2

    .line 12
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->f:Lcom/pspdfkit/internal/yb;

    check-cast p1, Lcom/pspdfkit/internal/ac;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->b(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->e:Lcom/pspdfkit/ui/PdfFragment;

    .line 15
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalFocusChangeListener(Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;)V

    goto :goto_0

    .line 17
    :cond_2
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->f:Lcom/pspdfkit/internal/yb;

    check-cast p1, Lcom/pspdfkit/internal/ac;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->a(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    :goto_0
    return-void
.end method

.method public final bindFormElementViewController(Lcom/pspdfkit/ui/special_mode/controller/FormElementViewController;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->i:Lcom/pspdfkit/ui/special_mode/controller/FormElementViewController;

    return-void
.end method

.method public final canClearFormField()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 2
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->i:Lcom/pspdfkit/ui/special_mode/controller/FormElementViewController;

    if-eqz v2, :cond_1

    .line 3
    invoke-interface {v2}, Lcom/pspdfkit/ui/special_mode/controller/FormElementViewController;->canClearFormField()Z

    move-result v0

    return v0

    .line 5
    :cond_1
    sget-object v2, Lcom/pspdfkit/internal/specialMode/handler/c$a;->a:[I

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v3, 0x2

    if-eq v0, v3, :cond_6

    const/4 v3, 0x3

    if-eq v0, v3, :cond_5

    const/4 v3, 0x4

    if-eq v0, v3, :cond_2

    return v1

    .line 18
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    check-cast v0, Lcom/pspdfkit/forms/ComboBoxFormElement;

    .line 19
    invoke-virtual {v0}, Lcom/pspdfkit/forms/ComboBoxFormElement;->isCustomTextSet()Z

    move-result v3

    if-nez v3, :cond_3

    .line 20
    invoke-virtual {v0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getSelectedIndexes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    return v1

    .line 21
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    check-cast v0, Lcom/pspdfkit/forms/ListBoxFormElement;

    .line 22
    invoke-virtual {v0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getSelectedIndexes()Ljava/util/List;

    move-result-object v0

    .line 23
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v2

    return v0

    .line 24
    :cond_6
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    check-cast v0, Lcom/pspdfkit/forms/TextFormElement;

    invoke-virtual {v0}, Lcom/pspdfkit/forms/TextFormElement;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/2addr v0, v2

    return v0

    .line 25
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    check-cast v0, Lcom/pspdfkit/forms/CheckBoxFormElement;

    invoke-virtual {v0}, Lcom/pspdfkit/forms/EditableButtonFormElement;->isSelected()Z

    move-result v0

    return v0
.end method

.method public final clearFormField()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 2
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->i:Lcom/pspdfkit/ui/special_mode/controller/FormElementViewController;

    if-eqz v2, :cond_1

    .line 3
    invoke-interface {v2}, Lcom/pspdfkit/ui/special_mode/controller/FormElementViewController;->clearFormField()Z

    move-result v0

    return v0

    .line 6
    :cond_1
    sget-object v2, Lcom/pspdfkit/internal/specialMode/handler/c$a;->a:[I

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v3, 0x2

    if-eq v0, v3, :cond_6

    const/4 v3, 0x3

    if-eq v0, v3, :cond_5

    const/4 v3, 0x4

    if-eq v0, v3, :cond_2

    return v1

    .line 23
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    check-cast v0, Lcom/pspdfkit/forms/ComboBoxFormElement;

    .line 24
    invoke-virtual {v0}, Lcom/pspdfkit/forms/ComboBoxFormElement;->isCustomTextSet()Z

    move-result v3

    if-nez v3, :cond_3

    .line 25
    invoke-virtual {v0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getSelectedIndexes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    :cond_3
    const/4 v1, 0x1

    .line 26
    :cond_4
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/forms/ChoiceFormElement;->setSelectedIndexes(Ljava/util/List;)V

    const/4 v2, 0x0

    .line 27
    invoke-virtual {v0, v2}, Lcom/pspdfkit/forms/ComboBoxFormElement;->setCustomText(Ljava/lang/String;)Z

    return v1

    .line 28
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    check-cast v0, Lcom/pspdfkit/forms/ListBoxFormElement;

    .line 30
    invoke-virtual {v0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getSelectedIndexes()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v2

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/forms/ChoiceFormElement;->setSelectedIndexes(Ljava/util/List;)V

    return v1

    .line 32
    :cond_6
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    check-cast v0, Lcom/pspdfkit/forms/TextFormElement;

    invoke-virtual {v0}, Lcom/pspdfkit/forms/TextFormElement;->getText()Ljava/lang/String;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    check-cast v1, Lcom/pspdfkit/forms/TextFormElement;

    const-string v3, ""

    invoke-virtual {v1, v3}, Lcom/pspdfkit/forms/TextFormElement;->setText(Ljava/lang/String;)Z

    .line 34
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/2addr v0, v2

    return v0

    .line 35
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    check-cast v0, Lcom/pspdfkit/forms/CheckBoxFormElement;

    invoke-virtual {v0}, Lcom/pspdfkit/forms/EditableButtonFormElement;->deselect()Z

    move-result v0

    return v0
.end method

.method public final finishEditing()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->e:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    const/4 v0, 0x1

    return v0
.end method

.method public final getCurrentlySelectedFormElement()Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    return-object v0
.end method

.method public final getFormManager()Lcom/pspdfkit/ui/special_mode/manager/FormManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->f:Lcom/pspdfkit/internal/yb;

    return-object v0
.end method

.method public final getFragment()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->e:Lcom/pspdfkit/ui/PdfFragment;

    return-object v0
.end method

.method public final hasNextElement()Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    goto :goto_4

    .line 2
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 3
    iget-object v3, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getNextElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_5

    .line 4
    iget-object v4, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    if-eqz v4, :cond_1

    .line 6
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v4

    iget-object v5, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    .line 7
    invoke-virtual {v5}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v5

    if-ne v4, v5, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_5

    .line 8
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 9
    iget-object v4, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->h:Ljava/util/EnumSet;

    .line 10
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 11
    sget v4, Lcom/pspdfkit/internal/ao;->a:I

    .line 12
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->isReadOnly()Z

    move-result v4

    if-nez v4, :cond_2

    .line 14
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    const/4 v4, 0x1

    goto :goto_3

    :cond_3
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_4

    goto :goto_5

    .line 15
    :cond_4
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 16
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getNextElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v3

    goto :goto_0

    :cond_5
    :goto_4
    const/4 v3, 0x0

    :goto_5
    if-eqz v3, :cond_6

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    return v1
.end method

.method public final hasPreviousElement()Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    goto :goto_4

    .line 2
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 3
    iget-object v3, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getPreviousElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_5

    .line 4
    iget-object v4, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    if-eqz v4, :cond_1

    .line 6
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v4

    iget-object v5, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    .line 7
    invoke-virtual {v5}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v5

    if-ne v4, v5, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_5

    .line 8
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 9
    iget-object v4, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->h:Ljava/util/EnumSet;

    .line 10
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 11
    sget v4, Lcom/pspdfkit/internal/ao;->a:I

    .line 12
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->isReadOnly()Z

    move-result v4

    if-nez v4, :cond_2

    .line 14
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    const/4 v4, 0x1

    goto :goto_3

    :cond_3
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_4

    goto :goto_5

    .line 15
    :cond_4
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 16
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getPreviousElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v3

    goto :goto_0

    :cond_5
    :goto_4
    const/4 v3, 0x0

    :goto_5
    if-eqz v3, :cond_6

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    return v1
.end method

.method public final onGlobalFocusChanged(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .line 1
    instance-of p1, p2, Lcom/pspdfkit/internal/views/annotations/a;

    if-eqz p1, :cond_0

    .line 2
    check-cast p2, Lcom/pspdfkit/internal/views/annotations/a;

    invoke-interface {p2}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    .line 3
    instance-of p2, p1, Lcom/pspdfkit/annotations/WidgetAnnotation;

    if-eqz p2, :cond_1

    .line 4
    check-cast p1, Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 6
    iget-object p2, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->e:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/ui/PdfFragment;->setSelectedFormElement(Lcom/pspdfkit/forms/FormElement;)V

    goto :goto_0

    .line 9
    :cond_0
    instance-of p1, p2, Lcom/pspdfkit/internal/im;

    if-eqz p1, :cond_1

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->e:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    :cond_1
    :goto_0
    return-void
.end method

.method public final selectNextFormElement()Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v2, 0x1

    if-nez v0, :cond_1

    goto :goto_4

    .line 2
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 3
    iget-object v3, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getNextElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_6

    .line 4
    iget-object v4, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    if-eqz v4, :cond_2

    .line 6
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v4

    iget-object v5, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    .line 7
    invoke-virtual {v5}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v5

    if-ne v4, v5, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_6

    .line 8
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 9
    iget-object v4, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->h:Ljava/util/EnumSet;

    .line 10
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 11
    sget v4, Lcom/pspdfkit/internal/ao;->a:I

    .line 12
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->isReadOnly()Z

    move-result v4

    if-nez v4, :cond_3

    .line 14
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x1

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_4

    const/4 v4, 0x1

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_5

    goto :goto_5

    .line 15
    :cond_5
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 16
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getNextElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v3

    goto :goto_0

    :cond_6
    :goto_4
    const/4 v3, 0x0

    :goto_5
    if-eqz v3, :cond_7

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->e:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0, v3}, Lcom/pspdfkit/ui/PdfFragment;->setSelectedFormElement(Lcom/pspdfkit/forms/FormElement;)V

    return v2

    :cond_7
    return v1
.end method

.method public final selectPreviousFormElement()Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v2, 0x1

    if-nez v0, :cond_1

    goto :goto_4

    .line 2
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 3
    iget-object v3, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getPreviousElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_6

    .line 4
    iget-object v4, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    if-eqz v4, :cond_2

    .line 6
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v4

    iget-object v5, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->g:Lcom/pspdfkit/forms/FormElement;

    .line 7
    invoke-virtual {v5}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v5

    if-ne v4, v5, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_6

    .line 8
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 9
    iget-object v4, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->h:Ljava/util/EnumSet;

    .line 10
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 11
    sget v4, Lcom/pspdfkit/internal/ao;->a:I

    .line 12
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->isReadOnly()Z

    move-result v4

    if-nez v4, :cond_3

    .line 14
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x1

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_4

    const/4 v4, 0x1

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_5

    goto :goto_5

    .line 15
    :cond_5
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 16
    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElement;->getPreviousElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v3

    goto :goto_0

    :cond_6
    :goto_4
    const/4 v3, 0x0

    :goto_5
    if-eqz v3, :cond_7

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->e:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0, v3}, Lcom/pspdfkit/ui/PdfFragment;->setSelectedFormElement(Lcom/pspdfkit/forms/FormElement;)V

    return v2

    :cond_7
    return v1
.end method

.method public final unbindFormElementViewController()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/c;->i:Lcom/pspdfkit/ui/special_mode/controller/FormElementViewController;

    return-void
.end method

.class public final Lcom/pspdfkit/internal/specialMode/handler/b;
.super Lcom/pspdfkit/internal/specialMode/handler/d;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;


# instance fields
.field private final e:Lcom/pspdfkit/internal/w0;

.field private final f:Lcom/pspdfkit/ui/PdfFragment;

.field private final g:Lcom/pspdfkit/internal/views/document/DocumentView;

.field private h:Lcom/pspdfkit/annotations/Annotation;

.field private i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;

.field private final j:Lcom/pspdfkit/internal/u0;

.field private final k:Lcom/pspdfkit/ui/audio/AudioModeManager;

.field private l:Z

.field private final m:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private n:Lcom/pspdfkit/internal/o1;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/internal/views/document/a;Lcom/pspdfkit/ui/audio/AudioModeManager;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/fl;)V
    .locals 1

    .line 1
    invoke-virtual {p4}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p4, p6}, Lcom/pspdfkit/internal/specialMode/handler/d;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ms;Lcom/pspdfkit/internal/fl;)V

    .line 2
    iput-object p4, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->f:Lcom/pspdfkit/ui/PdfFragment;

    .line 3
    iput-object p5, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->g:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->e:Lcom/pspdfkit/internal/w0;

    .line 5
    iput-object p2, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->j:Lcom/pspdfkit/internal/u0;

    .line 6
    iput-object p3, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->k:Lcom/pspdfkit/ui/audio/AudioModeManager;

    .line 7
    invoke-virtual {p4}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->m:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V

    const/4 p1, 0x0

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->n:Lcom/pspdfkit/internal/o1;

    return-void

    .line 10
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->d:Lcom/pspdfkit/internal/fl;

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/o1;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/internal/o1;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->n:Lcom/pspdfkit/internal/o1;

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    if-nez v0, :cond_2

    .line 13
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->b(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V

    goto :goto_0

    .line 16
    :cond_2
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V

    :goto_0
    return-void
.end method

.method public final bindAnnotationInspectorController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->l:Z

    .line 4
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;

    .line 5
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->l:Z

    if-eqz p1, :cond_1

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V

    :cond_1
    return-void
.end method

.method public final deleteCurrentlySelectedAnnotation()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_1

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->f:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->f:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 8
    invoke-interface {v1}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v1

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->d:Lcom/pspdfkit/internal/fl;

    .line 10
    invoke-static {v0}, Lcom/pspdfkit/internal/x;->b(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    .line 11
    invoke-interface {v1, v0}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->f:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    .line 13
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    const-string v2, "delete_annotation"

    .line 14
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    .line 15
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    :cond_1
    return-void
.end method

.method public final enterAudioPlaybackMode()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    .line 2
    instance-of v1, v0, Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v1, :cond_0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->k:Lcom/pspdfkit/ui/audio/AudioModeManager;

    check-cast v0, Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-interface {v1, v0}, Lcom/pspdfkit/ui/audio/AudioModeManager;->enterAudioPlaybackMode(Lcom/pspdfkit/annotations/SoundAnnotation;)V

    :cond_0
    return-void
.end method

.method public final enterAudioRecordingMode()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    .line 2
    instance-of v1, v0, Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v1, :cond_0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->k:Lcom/pspdfkit/ui/audio/AudioModeManager;

    check-cast v0, Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-interface {v1, v0}, Lcom/pspdfkit/ui/audio/AudioModeManager;->enterAudioRecordingMode(Lcom/pspdfkit/annotations/SoundAnnotation;)V

    :cond_0
    return-void
.end method

.method public final getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->e:Lcom/pspdfkit/internal/w0;

    return-object v0
.end method

.method public final getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->m:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-object v0
.end method

.method public final getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method public final getFragment()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->f:Lcom/pspdfkit/ui/PdfFragment;

    return-object v0
.end method

.method public final recordAnnotationZIndexEdit(Lcom/pspdfkit/annotations/Annotation;II)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->d:Lcom/pspdfkit/internal/fl;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/k2;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v2

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result p1

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/pspdfkit/internal/k2;-><init>(IIII)V

    .line 5
    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    return-void
.end method

.method public final saveCurrentlySelectedAnnotation()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->f:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->f:Lcom/pspdfkit/ui/PdfFragment;

    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    :cond_1
    return-void
.end method

.method public final shouldDisplayPicker()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->l:Z

    return v1

    .line 8
    :cond_1
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;->hasAnnotationInspector()Z

    move-result v0

    return v0
.end method

.method public final shouldDisplayPlayAudioButton()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    .line 2
    instance-of v1, v0, Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v1, :cond_0

    .line 3
    check-cast v0, Lcom/pspdfkit/annotations/SoundAnnotation;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->k:Lcom/pspdfkit/ui/audio/AudioModeManager;

    invoke-interface {v1, v0}, Lcom/pspdfkit/ui/audio/AudioModeManager;->canPlay(Lcom/pspdfkit/annotations/SoundAnnotation;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final shouldDisplayRecordAudioButton()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->h:Lcom/pspdfkit/annotations/Annotation;

    .line 2
    instance-of v1, v0, Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v1, :cond_0

    .line 3
    check-cast v0, Lcom/pspdfkit/annotations/SoundAnnotation;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->k:Lcom/pspdfkit/ui/audio/AudioModeManager;

    invoke-interface {v1, v0}, Lcom/pspdfkit/ui/audio/AudioModeManager;->canRecord(Lcom/pspdfkit/annotations/SoundAnnotation;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final showAnnotationEditor(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->j:Lcom/pspdfkit/internal/u0;

    check-cast v0, Lcom/pspdfkit/internal/views/document/a;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/views/document/a;->a(Lcom/pspdfkit/annotations/Annotation;Z)V

    return-void
.end method

.method public final showEditedAnnotationPositionOnThePage(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->g:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->k()V

    :cond_0
    return-void
.end method

.method public final startRecording()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->n:Lcom/pspdfkit/internal/o1;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/o1;->a()V

    :cond_0
    return-void
.end method

.method public final stopRecording()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->n:Lcom/pspdfkit/internal/o1;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/o1;->b()V

    :cond_0
    return-void
.end method

.method public final toggleAnnotationInspector()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    .line 2
    invoke-interface {v0, v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;->toggleAnnotationInspector(Z)V

    return-void
.end method

.method public final unbindAnnotationInspectorController()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/b;->i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;

    return-void
.end method

.class final Lcom/pspdfkit/internal/specialMode/handler/a$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/specialMode/handler/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private a:Lcom/pspdfkit/ui/fonts/Font;

.field private b:Lcom/pspdfkit/annotations/measurements/Scale;

.field private c:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

.field private d:I

.field private e:I

.field private f:I

.field private g:F

.field private h:F

.field private i:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

.field private j:Landroidx/core/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;"
        }
    .end annotation
.end field

.field private k:F

.field private l:Ljava/lang/String;

.field private m:Z


# direct methods
.method public constructor <init>(Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 1

    const-string v0, "font"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "measurementScale"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "floatPrecision"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a:Lcom/pspdfkit/ui/fonts/Font;

    .line 6
    iput-object p2, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->b:Lcom/pspdfkit/annotations/measurements/Scale;

    .line 9
    iput-object p3, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->c:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    const/high16 p1, 0x41200000    # 10.0f

    .line 25
    iput p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->g:F

    .line 29
    iput p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->h:F

    .line 32
    new-instance p1, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    sget-object p2, Lcom/pspdfkit/annotations/BorderStyle;->SOLID:Lcom/pspdfkit/annotations/BorderStyle;

    invoke-direct {p1, p2}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;-><init>(Lcom/pspdfkit/annotations/BorderStyle;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->i:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    .line 35
    new-instance p1, Landroidx/core/util/Pair;

    sget-object p2, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    invoke-direct {p1, p2, p2}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->j:Landroidx/core/util/Pair;

    const/high16 p1, 0x3f800000    # 1.0f

    .line 38
    iput p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->k:F

    const-string p1, ""

    .line 41
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->l:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    .line 7
    iget v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->k:F

    return v0
.end method

.method public final a(F)V
    .locals 0

    .line 8
    iput p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->k:F

    return-void
.end method

.method public final a(I)V
    .locals 0

    .line 4
    iput p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->d:I

    return-void
.end method

.method public final a(Landroidx/core/util/Pair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->j:Landroidx/core/util/Pair;

    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->c:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/measurements/Scale;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->b:Lcom/pspdfkit/annotations/measurements/Scale;

    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/fonts/Font;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a:Lcom/pspdfkit/ui/fonts/Font;

    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->i:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->l:Ljava/lang/String;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 10
    iput-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->m:Z

    return-void
.end method

.method public final b()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->i:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    return-object v0
.end method

.method public final b(F)V
    .locals 0

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->h:F

    return-void
.end method

.method public final b(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->e:I

    return-void
.end method

.method public final c()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->d:I

    return v0
.end method

.method public final c(F)V
    .locals 0

    .line 3
    iput p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->g:F

    return-void
.end method

.method public final c(I)V
    .locals 0

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->f:I

    return-void
.end method

.method public final d()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->e:I

    return v0
.end method

.method public final e()Lcom/pspdfkit/annotations/measurements/FloatPrecision;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->c:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    return-object v0
.end method

.method public final f()Lcom/pspdfkit/ui/fonts/Font;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a:Lcom/pspdfkit/ui/fonts/Font;

    return-object v0
.end method

.method public final g()Landroidx/core/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->j:Landroidx/core/util/Pair;

    return-object v0
.end method

.method public final h()Lcom/pspdfkit/annotations/measurements/Scale;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->b:Lcom/pspdfkit/annotations/measurements/Scale;

    return-object v0
.end method

.method public final i()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->f:I

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->m:Z

    return v0
.end method

.method public final l()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->h:F

    return v0
.end method

.method public final m()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a$a;->g:F

    return v0
.end method

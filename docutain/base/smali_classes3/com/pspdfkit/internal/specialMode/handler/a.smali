.class public final Lcom/pspdfkit/internal/specialMode/handler/a;
.super Lcom/pspdfkit/internal/specialMode/handler/d;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/specialMode/handler/a$a;
    }
.end annotation


# instance fields
.field private final e:Lcom/pspdfkit/internal/w0;

.field private final f:Lcom/pspdfkit/internal/u0;

.field private final g:Lcom/pspdfkit/ui/audio/AudioModeManager;

.field private final h:Lcom/pspdfkit/ui/PdfFragment;

.field private final i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

.field private final j:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

.field private final k:Landroid/os/Handler;

.field private final l:Lcom/pspdfkit/internal/uh;

.field private final m:Ljava/util/ArrayList;

.field private final n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

.field private o:I

.field private p:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field private q:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

.field private r:Z

.field private s:Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;

.field private t:Z

.field private final u:Lcom/pspdfkit/configuration/PdfConfiguration;


# direct methods
.method public static synthetic $r8$lambda$UmJlIBIlywOVM43v5BneJhEQECw(Lcom/pspdfkit/internal/specialMode/handler/a;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/internal/specialMode/handler/a;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/internal/views/document/a;Lcom/pspdfkit/ui/audio/AudioModeManager;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/l1;Lcom/pspdfkit/preferences/PSPDFKitPreferences;Lcom/pspdfkit/ui/fonts/Font;Landroid/os/Handler;Lcom/pspdfkit/internal/fl;Lcom/pspdfkit/internal/uh;)V
    .locals 1

    const-string v0, "annotationEventDispatcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationEditorController"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioModeManager"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fragment"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationPreferences"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preferences"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "freeTextAnnotationFont"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEditRecordedListener"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "magnifierManager"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p4}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p4, p9}, Lcom/pspdfkit/internal/specialMode/handler/d;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ms;Lcom/pspdfkit/internal/fl;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    .line 7
    iput-object p2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->f:Lcom/pspdfkit/internal/u0;

    .line 8
    iput-object p3, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->g:Lcom/pspdfkit/ui/audio/AudioModeManager;

    .line 10
    iput-object p4, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->h:Lcom/pspdfkit/ui/PdfFragment;

    .line 12
    iput-object p5, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    .line 14
    iput-object p6, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->j:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    .line 18
    iput-object p8, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->k:Landroid/os/Handler;

    .line 21
    iput-object p10, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->l:Lcom/pspdfkit/internal/uh;

    .line 37
    invoke-virtual {p4}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object p1

    if-nez p1, :cond_1

    :cond_0
    invoke-static {}, Lcom/pspdfkit/annotations/measurements/Scale;->defaultScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object p1

    .line 38
    :cond_1
    invoke-virtual {p4}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    if-eqz p2, :cond_2

    invoke-interface {p2}, Lcom/pspdfkit/document/PdfDocument;->getMeasurementPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object p2

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    :goto_0
    if-nez p2, :cond_3

    sget-object p2, Lcom/pspdfkit/internal/ao;->b:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    .line 40
    :cond_3
    new-instance p3, Lcom/pspdfkit/internal/specialMode/handler/a$a;

    const-string p5, "scale"

    invoke-static {p1, p5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p3, p7, p1, p2}, Lcom/pspdfkit/internal/specialMode/handler/a$a;-><init>(Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    .line 71
    invoke-virtual {p4}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    const-string p2, "fragment.configuration"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->u:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 75
    new-instance p1, Ljava/util/ArrayList;

    const/4 p2, 0x5

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->m:Ljava/util/ArrayList;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/specialMode/handler/a;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 212
    iput v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->o:I

    const/4 v0, 0x0

    .line 213
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->p:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 214
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->q:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 215
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->b(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    const-string v0, "newAnnotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    sget v1, Lcom/pspdfkit/internal/ao;->a:I

    const-string v1, "annotationPreferences"

    .line 217
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "annotation"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1507
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getCreator()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1508
    invoke-interface {v0}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getAnnotationCreator()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1510
    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setCreator(Ljava/lang/String;)V

    .line 1511
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    .line 1512
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->q:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 1513
    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/pf;->setVariant(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/k1;)V
    .locals 7

    const-string v0, "modeHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v1

    const-string v2, "getFeatures()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v0, :cond_0

    .line 146
    invoke-interface {p1}, Lcom/pspdfkit/internal/em;->a()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->o:I

    .line 147
    invoke-interface {p1}, Lcom/pspdfkit/internal/k1;->e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->p:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 148
    invoke-interface {p1}, Lcom/pspdfkit/internal/k1;->c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->q:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 149
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    goto :goto_1

    .line 150
    :cond_0
    invoke-interface {p1}, Lcom/pspdfkit/internal/em;->a()I

    move-result v0

    iget v4, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->o:I

    if-ne v0, v4, :cond_1

    .line 151
    invoke-interface {p1}, Lcom/pspdfkit/internal/k1;->e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    iget-object v4, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->p:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-ne v0, v4, :cond_1

    .line 152
    invoke-interface {p1}, Lcom/pspdfkit/internal/k1;->c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    iget-object v4, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->q:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 153
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 157
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 158
    invoke-interface {p1}, Lcom/pspdfkit/internal/em;->a()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->o:I

    .line 159
    invoke-interface {p1}, Lcom/pspdfkit/internal/k1;->e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->p:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 160
    invoke-interface {p1}, Lcom/pspdfkit/internal/k1;->c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->q:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 161
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    .line 172
    :goto_1
    sget-object v4, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v1, v4}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_3

    .line 175
    :cond_3
    iput-boolean v2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    .line 176
    invoke-interface {p1}, Lcom/pspdfkit/internal/k1;->e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v1

    const-string v2, "modeHandler.annotationTool"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    invoke-interface {p1}, Lcom/pspdfkit/internal/k1;->c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object p1

    const-string v2, "modeHandler.annotationToolVariant"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->j:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    invoke-virtual {v2, v1, p1}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->setLastAnnotationTool(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 181
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v2, v1, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->setColor(I)V

    .line 182
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v2, v1, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getFillColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->setFillColor(I)V

    .line 183
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v2, v1, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getOutlineColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->setOutlineColor(I)V

    .line 186
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v2, v1, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getThickness(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->setThickness(F)V

    .line 187
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v2, v1, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getTextSize(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->setTextSize(F)V

    .line 190
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v2, v1, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getBorderStylePreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object v2

    const-string v4, "annotationPreferences.ge\u2026l, annotationToolVariant)"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->setBorderStylePreset(Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    .line 193
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v2, v1, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getLineEnds(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Landroidx/core/util/Pair;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    iget-object v5, v2, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    const-string v6, "lineEnds.first"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Lcom/pspdfkit/annotations/LineEndType;

    iget-object v2, v2, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    const-string v6, "lineEnds.second"

    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {p0, v5, v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->setLineEnds(Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    .line 197
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v2, v1, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getAlpha(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->setAlpha(F)V

    .line 200
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v2, v1, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getFont(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Lcom/pspdfkit/ui/fonts/Font;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->setFont(Lcom/pspdfkit/ui/fonts/Font;)V

    .line 203
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v2, v1, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->setOverlayText(Ljava/lang/String;)V

    .line 204
    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v2, v1, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getRepeatOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->setRepeatOverlayText(Z)V

    if-eqz v0, :cond_4

    .line 206
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    goto :goto_2

    .line 208
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->a(Lcom/pspdfkit/internal/specialMode/handler/a;)V

    .line 210
    :goto_2
    iput-boolean v3, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    :goto_3
    return-void
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final b(Lcom/pspdfkit/internal/k1;)V
    .locals 1

    const-string v0, "modeHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->m:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 3
    iput p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->o:I

    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->p:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->q:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->b(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_0
    return-void
.end method

.method public final bindAnnotationInspectorController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;)V
    .locals 1

    const-string v0, "annotationInspectorController"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->s:Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->t:Z

    .line 4
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->s:Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;

    .line 5
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->t:Z

    if-eqz p1, :cond_1

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_1
    return-void
.end method

.method public final c()Lcom/pspdfkit/internal/w0;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    return-object v0
.end method

.method public final c(Lcom/pspdfkit/internal/k1;)V
    .locals 1

    const-string v0, "modeHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->m:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-nez p1, :cond_0

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->k:Landroid/os/Handler;

    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/a$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public final changeAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 1

    const-string v0, "annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->c:Lcom/pspdfkit/internal/ms;

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/internal/ms;->enterAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    return-void
.end method

.method public final d()Lcom/pspdfkit/ui/audio/AudioModeManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->g:Lcom/pspdfkit/ui/audio/AudioModeManager;

    return-object v0
.end method

.method public final e()Landroid/content/Context;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    const-string v1, "super.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final f()Lcom/pspdfkit/internal/uh;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->l:Lcom/pspdfkit/internal/uh;

    return-object v0
.end method

.method public final getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->p:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method public final getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->q:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    return-object v0
.end method

.method public final getAlpha()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a()F

    move-result v0

    return v0
.end method

.method public final getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    return-object v0
.end method

.method public final getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    return-object v0
.end method

.method public final getBorderStylePreset()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->b()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object v0

    return-object v0
.end method

.method public final getColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->c()I

    move-result v0

    return v0
.end method

.method public final getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->u:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-object v0
.end method

.method public final getFillColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->d()I

    move-result v0

    return v0
.end method

.method public final getFloatPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->h:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getMeasurementPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->e()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public final getFont()Lcom/pspdfkit/ui/fonts/Font;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->f()Lcom/pspdfkit/ui/fonts/Font;

    move-result-object v0

    return-object v0
.end method

.method public final getFragment()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->h:Lcom/pspdfkit/ui/PdfFragment;

    return-object v0
.end method

.method public final getLineEnds()Landroidx/core/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->g()Landroidx/core/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public final getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->h:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->h()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public final getOutlineColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->i()I

    move-result v0

    return v0
.end method

.method public final getOverlayText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRepeatOverlayText()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->k()Z

    move-result v0

    return v0
.end method

.method public final getTextSize()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->l()F

    move-result v0

    return v0
.end method

.method public final getThickness()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->m()F

    move-result v0

    return v0
.end method

.method public final isMeasurementSnappingEnabled()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->j:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    invoke-virtual {v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->isMeasurementSnappingEnabled()Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "preferences.isMeasurementSnappingEnabled"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final setAlpha(F)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a()F

    move-result v0

    cmpg-float v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a(F)V

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    if-nez p1, :cond_1

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_1
    return-void
.end method

.method public final setBorderStylePreset(Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
    .locals 1

    const-string v0, "borderPreset"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->b()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a(Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    if-nez p1, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_0
    return-void
.end method

.method public final setColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->c()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a(I)V

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    if-nez p1, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_0
    return-void
.end method

.method public final setFillColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->d()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->b(I)V

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    if-nez p1, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_0
    return-void
.end method

.method public final setFloatPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 1

    const-string v0, "precision"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->e()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v0

    if-eq v0, p1, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->h:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {v0, p1}, Lcom/pspdfkit/document/PdfDocument;->setMeasurementPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    .line 5
    :goto_0
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    if-nez p1, :cond_1

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_1
    return-void
.end method

.method public final setFont(Lcom/pspdfkit/ui/fonts/Font;)V
    .locals 1

    const-string v0, "font"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->f()Lcom/pspdfkit/ui/fonts/Font;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a(Lcom/pspdfkit/ui/fonts/Font;)V

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    if-nez p1, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_0
    return-void
.end method

.method public final setLineEnds(Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V
    .locals 2

    const-string v0, "lineEnd1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lineEnd2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->g()Landroidx/core/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->g()Landroidx/core/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    if-eq v0, p2, :cond_1

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    new-instance v1, Landroidx/core/util/Pair;

    invoke-direct {v1, p1, p2}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a(Landroidx/core/util/Pair;)V

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    if-nez p1, :cond_1

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_1
    return-void
.end method

.method public final setMeasurementScale(Lcom/pspdfkit/annotations/measurements/Scale;)V
    .locals 1

    const-string v0, "measurementScale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->h()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a(Lcom/pspdfkit/annotations/measurements/Scale;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->h:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {v0, p1}, Lcom/pspdfkit/document/PdfDocument;->setMeasurementScale(Lcom/pspdfkit/annotations/measurements/Scale;)V

    .line 5
    :goto_0
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    if-nez p1, :cond_1

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_1
    return-void
.end method

.method public final setMeasurementSnappingEnabled(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->j:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    invoke-virtual {v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->isMeasurementSnappingEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->j:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->setMeasurementSnappingEnabled(Z)V

    :cond_0
    return-void
.end method

.method public final setOutlineColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->i()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->c(I)V

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    if-nez p1, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_0
    return-void
.end method

.method public final setOverlayText(Ljava/lang/String;)V
    .locals 1

    const-string v0, "overlayText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a(Ljava/lang/String;)V

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    if-nez p1, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_0
    return-void
.end method

.method public final setRepeatOverlayText(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->k()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-ne v0, p1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->a(Z)V

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    if-nez p1, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_0
    return-void
.end method

.method public final setTextSize(F)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->l()F

    move-result v0

    cmpg-float v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->b(F)V

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    if-nez p1, :cond_1

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_1
    return-void
.end method

.method public final setThickness(F)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->m()F

    move-result v0

    cmpg-float v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->n:Lcom/pspdfkit/internal/specialMode/handler/a$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a$a;->c(F)V

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->r:Z

    if-nez p1, :cond_1

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->e:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    :cond_1
    return-void
.end method

.method public final shouldDisplayPicker()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->p:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->s:Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->t:Z

    goto :goto_0

    .line 6
    :cond_1
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;->hasAnnotationInspector()Z

    move-result v1

    :goto_0
    return v1
.end method

.method public final showAnnotationEditor(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->f:Lcom/pspdfkit/internal/u0;

    check-cast v0, Lcom/pspdfkit/internal/views/document/a;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/views/document/a;->a(Lcom/pspdfkit/annotations/Annotation;Z)V

    return-void
.end method

.method public final toggleAnnotationInspector()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->s:Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;->toggleAnnotationInspector(Z)V

    return-void
.end method

.method public final unbindAnnotationInspectorController()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/a;->s:Lcom/pspdfkit/ui/special_mode/controller/AnnotationInspectorController;

    return-void
.end method

.class public abstract Lcom/pspdfkit/internal/specialMode/handler/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected b:Landroid/content/Context;

.field protected c:Lcom/pspdfkit/internal/ms;

.field protected d:Lcom/pspdfkit/internal/fl;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/ms;Lcom/pspdfkit/internal/fl;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->c:Lcom/pspdfkit/internal/ms;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->d:Lcom/pspdfkit/internal/fl;

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/fl;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->d:Lcom/pspdfkit/internal/fl;

    return-object v0
.end method

.method public final exitActiveMode()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->c:Lcom/pspdfkit/internal/ms;

    invoke-interface {v0}, Lcom/pspdfkit/internal/ms;->exitCurrentlyActiveMode()V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/specialMode/handler/e;
.super Lcom/pspdfkit/internal/specialMode/handler/d;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;


# instance fields
.field private final e:Lcom/pspdfkit/internal/xt;

.field private final f:Lcom/pspdfkit/internal/views/document/a;

.field private final g:Lcom/pspdfkit/ui/PdfFragment;

.field private final h:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

.field private final i:Lcom/pspdfkit/annotations/links/LinkAnnotationHighlighter;

.field private final j:Lcom/pspdfkit/internal/uh;

.field private k:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/document/DocumentPermissions;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/pspdfkit/internal/zt;

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:Lcom/pspdfkit/internal/zt$d;

.field private p:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController$OnSearchSelectedTextListener;


# direct methods
.method public static synthetic $r8$lambda$-YURVk0-LZ1w2sGteRBbSZg0ia0(Lcom/pspdfkit/internal/specialMode/handler/e;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/e;->e()V

    return-void
.end method

.method public static synthetic $r8$lambda$6zISztjmKXjLvR76w460v2TDhK4(Lcom/pspdfkit/internal/specialMode/handler/e;Lcom/pspdfkit/annotations/LinkAnnotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Lcom/pspdfkit/annotations/LinkAnnotation;)V

    return-void
.end method

.method public static synthetic $r8$lambda$JgxDSwBAZdJ-XvDnnO8MpazpVrM(Lcom/pspdfkit/internal/specialMode/handler/e;Landroid/widget/EditText;Landroidx/appcompat/app/AlertDialog;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Landroid/widget/EditText;Landroidx/appcompat/app/AlertDialog;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$JucngWGpGloyejoFSWU1WP37EIY(Lcom/pspdfkit/internal/specialMode/handler/e;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$M0Gj3hFzy8_F7AfVfLneS9RPlJM(Lcom/pspdfkit/internal/specialMode/handler/e;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/e;->i()V

    return-void
.end method

.method public static synthetic $r8$lambda$P1UrM1xe4Ibpyr-4MQ66Osadl5M(Lcom/pspdfkit/internal/specialMode/handler/e;Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/annotations/AnnotationType;ZLcom/pspdfkit/annotations/BaseRectsAnnotation;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/annotations/AnnotationType;ZLcom/pspdfkit/annotations/BaseRectsAnnotation;)V

    return-void
.end method

.method public static synthetic $r8$lambda$PjqiU08XbofdgEnL6du2ZmmWW-g(Lcom/pspdfkit/internal/specialMode/handler/e;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/e;->h()V

    return-void
.end method

.method public static synthetic $r8$lambda$_kyeEmn6MbwIFlKyCM0VnrPX7uk(Lcom/pspdfkit/internal/specialMode/handler/e;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/e;->f()V

    return-void
.end method

.method public static synthetic $r8$lambda$eDiuf35FXlvW26SKchRI1bltGeQ(Lcom/pspdfkit/internal/specialMode/handler/e;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/specialMode/handler/e;->g()V

    return-void
.end method

.method public static synthetic $r8$lambda$fr0XGHNE7U1W5O1PSdzDsfMZGAw(Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$j00qEfWPCQ8JlnSou5Jqd3VtyeM(Lcom/pspdfkit/internal/specialMode/handler/e;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static synthetic $r8$lambda$sclkNe8QUVDI4TdETpzWDQWuv7Y(Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/e;->b(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetm(Lcom/pspdfkit/internal/specialMode/handler/e;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->m:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputm(Lcom/pspdfkit/internal/specialMode/handler/e;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->m:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$ma(Lcom/pspdfkit/internal/specialMode/handler/e;Landroidx/appcompat/app/AlertDialog;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Landroidx/appcompat/app/AlertDialog;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mb(Lcom/pspdfkit/internal/specialMode/handler/e;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/e;->b(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/yt;Lcom/pspdfkit/internal/views/document/a;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/l1;Lcom/pspdfkit/internal/fl;Lcom/pspdfkit/internal/uh;)V
    .locals 1

    .line 1
    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p3, p5}, Lcom/pspdfkit/internal/specialMode/handler/d;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ms;Lcom/pspdfkit/internal/fl;)V

    .line 2
    const-class p5, Lcom/pspdfkit/document/DocumentPermissions;

    invoke-static {p5}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object p5

    iput-object p5, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->k:Ljava/util/EnumSet;

    .line 26
    iput-object p2, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->f:Lcom/pspdfkit/internal/views/document/a;

    .line 27
    iput-object p3, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    .line 28
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->e:Lcom/pspdfkit/internal/xt;

    .line 29
    iput-object p4, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->h:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    .line 30
    new-instance p1, Lcom/pspdfkit/annotations/links/LinkAnnotationHighlighter;

    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/pspdfkit/annotations/links/LinkAnnotationHighlighter;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->i:Lcom/pspdfkit/annotations/links/LinkAnnotationHighlighter;

    .line 31
    iput-object p6, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->j:Lcom/pspdfkit/internal/uh;

    .line 32
    invoke-virtual {p3, p1}, Lcom/pspdfkit/ui/PdfFragment;->addDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    return-void
.end method

.method private synthetic a(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x0

    .line 71
    iput-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->n:Z

    const/4 p1, 0x0

    .line 72
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->m:Ljava/lang/String;

    return-void
.end method

.method private static synthetic a(Landroid/content/DialogInterface;I)V
    .locals 0

    return-void
.end method

.method private a(Landroid/widget/EditText;Landroidx/appcompat/app/AlertDialog;Landroid/view/View;)V
    .locals 3

    .line 73
    invoke-virtual {p0}, Lcom/pspdfkit/internal/specialMode/handler/e;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->m:Ljava/lang/String;

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    const-string v2, "http://"

    .line 75
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->m:Ljava/lang/String;

    const-string v2, "https://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 80
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->m:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 81
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Lcom/pspdfkit/datastructures/TextSelection;Ljava/lang/Integer;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.TextSelection"

    const-string v2, "Entered text could not be converted to an Integer nor URL."

    .line 83
    invoke-static {v1, p2, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    sget p3, Lcom/pspdfkit/R$string;->pspdf__link_annotation_creation_parsed_text_error:I

    .line 86
    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 87
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    return-void

    .line 88
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->m:Ljava/lang/String;

    .line 89
    iget-object p3, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p3}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p3

    if-nez p3, :cond_2

    goto :goto_1

    .line 92
    :cond_2
    new-instance v1, Lcom/pspdfkit/annotations/actions/UriAction;

    invoke-direct {v1, p1}, Lcom/pspdfkit/annotations/actions/UriAction;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p3, v0, v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/annotations/actions/Action;)V

    .line 93
    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->c:Lcom/pspdfkit/internal/ms;

    invoke-interface {p1}, Lcom/pspdfkit/internal/ms;->exitCurrentlyActiveMode()V

    .line 94
    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method private a(Landroidx/appcompat/app/AlertDialog;)V
    .locals 1

    const/4 v0, -0x1

    .line 95
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/AnnotationType;Z)V
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->l:Lcom/pspdfkit/internal/zt;

    if-nez v0, :cond_0

    return-void

    .line 43
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zt;->f()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v1

    .line 46
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/zt;->a(Lcom/pspdfkit/annotations/AnnotationType;Z)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v2, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda3;

    invoke-direct {v2, p0, v1, p1, p2}, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/specialMode/handler/e;Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/annotations/AnnotationType;Z)V

    .line 47
    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/LinkAnnotation;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.TextSelection"

    const-string v3, "Link annotation successfully created above the selected text."

    .line 134
    invoke-static {v2, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 137
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    .line 138
    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    .line 139
    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__link_annotation_successfully_created:I

    .line 140
    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 145
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "create_annotation"

    .line 146
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 147
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 149
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->i:Lcom/pspdfkit/annotations/links/LinkAnnotationHighlighter;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/links/LinkAnnotationHighlighter;->setLinkAnnotation(Lcom/pspdfkit/annotations/LinkAnnotation;)V

    .line 150
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->d:Lcom/pspdfkit/internal/fl;

    .line 151
    invoke-static {p1}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    return-void
.end method

.method private a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/annotations/AnnotationType;ZLcom/pspdfkit/annotations/BaseRectsAnnotation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    if-eqz p4, :cond_5

    if-eqz p1, :cond_4

    .line 48
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "perform_text_selection_action"

    .line 49
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 50
    sget-object v1, Lcom/pspdfkit/internal/specialMode/handler/e$d;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    const-string p2, "underline"

    goto :goto_0

    .line 60
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    const-string p4, "Invalid type passed: "

    invoke-direct {p3, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const-string p2, "strikeout"

    goto :goto_0

    :cond_2
    const-string p2, "redact"

    goto :goto_0

    :cond_3
    const-string p2, "highlight"

    :goto_0
    const-string v1, "action"

    .line 65
    invoke-virtual {v0, v1, p2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p2

    iget p1, p1, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    const-string v0, "page_index"

    .line 66
    invoke-virtual {p2, p1, v0}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 67
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 68
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->c:Lcom/pspdfkit/internal/ms;

    invoke-interface {p1}, Lcom/pspdfkit/internal/ms;->exitCurrentlyActiveMode()V

    if-eqz p3, :cond_5

    .line 69
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->f:Lcom/pspdfkit/internal/views/document/a;

    const/4 p2, 0x0

    invoke-virtual {p1, p4, p2}, Lcom/pspdfkit/internal/views/document/a;->a(Lcom/pspdfkit/annotations/Annotation;Z)V

    :cond_5
    return-void
.end method

.method private a(Lcom/pspdfkit/datastructures/TextSelection;Ljava/lang/Integer;)V
    .locals 4

    .line 97
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 100
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ltz v1, :cond_2

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v2

    if-lt v1, v2, :cond_1

    goto :goto_0

    .line 114
    :cond_1
    new-instance v1, Lcom/pspdfkit/annotations/actions/GoToAction;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-direct {v1, p2}, Lcom/pspdfkit/annotations/actions/GoToAction;-><init>(I)V

    invoke-direct {p0, v0, p1, v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/annotations/actions/Action;)V

    return-void

    .line 115
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 118
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__link_page_not_found:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 119
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 120
    invoke-static {p1, p2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    .line 125
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    new-array p1, v3, [Ljava/lang/Object;

    const-string p2, "PSPDFKit.TextSelection"

    const-string v0, "Unable to create link annotation with GOTO action pointing to non-existing page in the document."

    .line 126
    invoke-static {p2, v0, p1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private a(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/annotations/actions/Action;)V
    .locals 2

    .line 127
    new-instance v0, Lcom/pspdfkit/annotations/LinkAnnotation;

    iget v1, p2, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    invoke-direct {v0, v1}, Lcom/pspdfkit/annotations/LinkAnnotation;-><init>(I)V

    .line 128
    iget-object p2, p2, Lcom/pspdfkit/datastructures/TextSelection;->textBlocks:Ljava/util/List;

    invoke-static {p2}, Lcom/pspdfkit/internal/di;->a(Ljava/util/List;)Landroid/graphics/RectF;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    .line 129
    invoke-virtual {v0, p3}, Lcom/pspdfkit/annotations/LinkAnnotation;->setAction(Lcom/pspdfkit/annotations/actions/Action;)V

    .line 130
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p1

    .line 131
    invoke-interface {p1, v0}, Lcom/pspdfkit/annotations/AnnotationProvider;->addAnnotationToPageAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 132
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda1;

    invoke-direct {p2, p0, v0}, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/specialMode/handler/e;Lcom/pspdfkit/annotations/LinkAnnotation;)V

    new-instance p3, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda2;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/specialMode/handler/e;)V

    .line 133
    invoke-virtual {p1, p2, p3}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 2

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->h:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v0}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getAnnotationCreator()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    .line 19
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/specialMode/handler/e$a;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/specialMode/handler/e$a;-><init>(Ljava/lang/Runnable;)V

    const/4 p1, 0x0

    .line 20
    invoke-static {v0, p1, v1}, Lcom/pspdfkit/ui/AnnotationCreatorInputDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/pspdfkit/ui/AnnotationCreatorInputDialogFragment$OnAnnotationCreatorSetListener;)V

    .line 33
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    const-string v0, "show_annotation_creator_dialog"

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    goto :goto_0

    .line 35
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.TextSelection"

    const-string v3, "Creating link annotation above the selected text failed."

    .line 152
    invoke-static {v2, p1, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    iget-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    .line 154
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    sget v1, Lcom/pspdfkit/R$string;->pspdf__link_annotation_creation_failed:I

    .line 155
    invoke-static {p1, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    .line 159
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private static synthetic b(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 68
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 5

    .line 3
    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 5
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 7
    iget-object v3, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/pspdfkit/R$dimen;->pspdf__alert_dialog_inset:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 8
    iget-object v3, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/pspdfkit/R$dimen;->pspdf__alert_dialog_inset:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 10
    new-instance v3, Landroid/widget/EditText;

    iget-object v4, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 11
    sget v4, Lcom/pspdfkit/R$id;->pspdf__link_creator_dialog_edit_text:I

    invoke-virtual {v3, v4}, Landroid/view/View;->setId(I)V

    .line 12
    invoke-virtual {v3}, Landroid/widget/TextView;->setSingleLine()V

    if-eqz p1, :cond_0

    .line 14
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 16
    :cond_0
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 17
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 19
    new-instance p1, Landroidx/appcompat/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    invoke-direct {p1, v1}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/pspdfkit/R$string;->pspdf__link_destination:I

    .line 20
    invoke-virtual {p1, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setTitle(I)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/pspdfkit/R$string;->pspdf__link_enter_page_index_or_url:I

    .line 21
    invoke-virtual {p1, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setMessage(I)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    .line 22
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/specialMode/handler/e;)V

    .line 23
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/pspdfkit/R$string;->pspdf__add_link:I

    new-instance v1, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda5;

    invoke-direct {v1}, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda5;-><init>()V

    .line 27
    invoke-virtual {p1, v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->b:Landroid/content/Context;

    sget v1, Lcom/pspdfkit/R$string;->pspdf__cancel:I

    const/4 v4, 0x0

    .line 28
    invoke-static {v0, v1, v4}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 29
    new-instance v1, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda6;

    invoke-direct {v1}, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda6;-><init>()V

    .line 30
    invoke-virtual {p1, v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    .line 33
    invoke-virtual {p1}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object p1

    .line 35
    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/e$c;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/e$c;-><init>(Lcom/pspdfkit/internal/specialMode/handler/e;Landroidx/appcompat/app/AlertDialog;)V

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 43
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 44
    invoke-virtual {p1, v2}, Landroidx/appcompat/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0, v3, p1}, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/specialMode/handler/e;Landroid/widget/EditText;Landroidx/appcompat/app/AlertDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Landroidx/appcompat/app/AlertDialog;)V

    const/4 p1, 0x1

    .line 67
    iput-boolean p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->n:Z

    return-void
.end method

.method private synthetic e()V
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->HIGHLIGHT:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Lcom/pspdfkit/annotations/AnnotationType;Z)V

    return-void
.end method

.method private synthetic f()V
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->HIGHLIGHT:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Lcom/pspdfkit/annotations/AnnotationType;Z)V

    return-void
.end method

.method private synthetic g()V
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->REDACT:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Lcom/pspdfkit/annotations/AnnotationType;Z)V

    return-void
.end method

.method private synthetic h()V
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->STRIKEOUT:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Lcom/pspdfkit/annotations/AnnotationType;Z)V

    return-void
.end method

.method private synthetic i()V
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->UNDERLINE:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Lcom/pspdfkit/annotations/AnnotationType;Z)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/datastructures/TextSelection;)V
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->e:Lcom/pspdfkit/internal/xt;

    check-cast v0, Lcom/pspdfkit/internal/yt;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/yt;->a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/datastructures/TextSelection;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zt$d;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->o:Lcom/pspdfkit/internal/zt$d;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->l:Lcom/pspdfkit/internal/zt;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zt;->a(Lcom/pspdfkit/internal/zt$d;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zt;)V
    .locals 2

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->l:Lcom/pspdfkit/internal/zt;

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->o:Lcom/pspdfkit/internal/zt$d;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/zt;->a(Lcom/pspdfkit/internal/zt$d;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->l:Lcom/pspdfkit/internal/zt;

    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->j:Lcom/pspdfkit/internal/uh;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zt;->a(Lcom/pspdfkit/internal/uh;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->e:Lcom/pspdfkit/internal/xt;

    check-cast v0, Lcom/pspdfkit/internal/yt;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/yt;->a(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zt;->f()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 12
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "select_text"

    .line 13
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    iget p1, p1, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    const-string v1, "page_index"

    .line 14
    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .line 70
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->m:Ljava/lang/String;

    return-void
.end method

.method public final a(Ljava/util/EnumSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/document/DocumentPermissions;",
            ">;)V"
        }
    .end annotation

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->k:Ljava/util/EnumSet;

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public final b(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/datastructures/TextSelection;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->e:Lcom/pspdfkit/internal/xt;

    check-cast v0, Lcom/pspdfkit/internal/yt;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/yt;->b(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/datastructures/TextSelection;)Z

    move-result p1

    return p1
.end method

.method public final c()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->l:Lcom/pspdfkit/internal/zt;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zt;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final createLinkAboveSelectedText()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/specialMode/handler/e;->isLinkCreationEnabledByConfiguration()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->h:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->isAnnotationCreatorSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    .line 6
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/specialMode/handler/e$b;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/specialMode/handler/e$b;-><init>(Lcom/pspdfkit/internal/specialMode/handler/e;)V

    const/4 v2, 0x0

    .line 7
    invoke-static {v0, v2, v1}, Lcom/pspdfkit/ui/AnnotationCreatorInputDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/pspdfkit/ui/AnnotationCreatorInputDialogFragment$OnAnnotationCreatorSetListener;)V

    .line 20
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "show_annotation_creator_dialog"

    .line 21
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    goto :goto_0

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->m:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.TextSelection"

    const-string v2, "Unable to create link above selected text: creating links not enabled by configuration."

    .line 27
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public final d()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->n:Z

    return v0
.end method

.method public final getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->h:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    return-object v0
.end method

.method public final getFragment()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    return-object v0
.end method

.method public final getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->l:Lcom/pspdfkit/internal/zt;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zt;->f()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTextSelectionManager()Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->e:Lcom/pspdfkit/internal/xt;

    return-object v0
.end method

.method public final highlightSelectedText()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda11;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda11;-><init>(Lcom/pspdfkit/internal/specialMode/handler/e;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final highlightSelectedTextAndBeginCommenting()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda10;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/internal/specialMode/handler/e;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final isInstantHighlightCommentingEnabledByConfiguration()Z
    .locals 3

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    .line 2
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INSTANT_HIGHLIGHT_COMMENT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    .line 4
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isLinkCreationEnabledByConfiguration()Z
    .locals 3

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->LINK:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/AnnotationType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isRedactionEnabledByConfiguration()Z
    .locals 3

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->REDACTION:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Z

    move-result v0

    return v0
.end method

.method public final isTextExtractionEnabledByDocumentPermissions()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->k:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/document/DocumentPermissions;->EXTRACT:Lcom/pspdfkit/document/DocumentPermissions;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isTextHighlightingEnabledByConfiguration()Z
    .locals 3

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->HIGHLIGHT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isTextSharingEnabledByConfiguration()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->g:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledShareFeatures()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->TEXT_SELECTION_SHARING:Lcom/pspdfkit/configuration/sharing/ShareFeatures;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isTextSpeakEnabledByDocumentPermissions()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->k:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/document/DocumentPermissions;->EXTRACT_ACCESSIBILITY:Lcom/pspdfkit/document/DocumentPermissions;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final j()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->e:Lcom/pspdfkit/internal/xt;

    check-cast v0, Lcom/pspdfkit/internal/yt;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/yt;->b(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->l:Lcom/pspdfkit/internal/zt;

    return-void
.end method

.method public final k()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->e:Lcom/pspdfkit/internal/xt;

    check-cast v0, Lcom/pspdfkit/internal/yt;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/yt;->b(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->l:Lcom/pspdfkit/internal/zt;

    return-void
.end method

.method public final redactSelectedText()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda8;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/specialMode/handler/e;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final searchSelectedText()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/specialMode/handler/e;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget-object v1, v0, Lcom/pspdfkit/datastructures/TextSelection;->text:Ljava/lang/String;

    .line 5
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v2

    const-string v3, "perform_text_selection_action"

    .line 7
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v2

    const-string v3, "action"

    const-string v4, "search"

    .line 8
    invoke-virtual {v2, v3, v4}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v2

    iget v0, v0, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    const-string v3, "page_index"

    .line 9
    invoke-virtual {v2, v0, v3}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->p:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController$OnSearchSelectedTextListener;

    if-eqz v0, :cond_1

    .line 13
    invoke-interface {v0, v1}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController$OnSearchSelectedTextListener;->onSearchSelectedText(Ljava/lang/String;)V

    .line 14
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/d;->c:Lcom/pspdfkit/internal/ms;

    invoke-interface {v0}, Lcom/pspdfkit/internal/ms;->exitCurrentlyActiveMode()V

    :cond_2
    return-void
.end method

.method public final setOnSearchSelectedTextListener(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController$OnSearchSelectedTextListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->p:Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController$OnSearchSelectedTextListener;

    return-void
.end method

.method public final setTextSelection(Lcom/pspdfkit/datastructures/TextSelection;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/specialMode/handler/e;->l:Lcom/pspdfkit/internal/zt;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zt;->a(Lcom/pspdfkit/datastructures/TextSelection;)V

    :cond_0
    return-void
.end method

.method public final strikeoutSelectedText()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda9;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/internal/specialMode/handler/e;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final underlineSelectedText()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/e$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/specialMode/handler/e;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Ljava/lang/Runnable;)V

    return-void
.end method

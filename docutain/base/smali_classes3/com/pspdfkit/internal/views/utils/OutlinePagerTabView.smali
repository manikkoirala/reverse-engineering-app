.class public Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/material/bottomnavigation/BottomNavigationView$OnNavigationItemSelectedListener;
.implements Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;


# instance fields
.field private final b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

.field private c:Landroidx/viewpager/widget/ViewPager;

.field private d:Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

.field private final e:Ljava/util/ArrayList;


# direct methods
.method static bridge synthetic -$$Nest$mb(Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->e:Ljava/util/ArrayList;

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/pspdfkit/R$layout;->pspdf__view_pager_tab_view:I

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 9
    sget p2, Lcom/pspdfkit/R$id;->pspdf__view_pager_tab_buttons_bar:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    iput-object p1, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    .line 10
    sget p2, Lcom/pspdfkit/R$menu;->pspdf__menu_pdf_outline_view:I

    invoke-virtual {p1, p2}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->inflateMenu(I)V

    .line 11
    invoke-virtual {p1, p0}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->setOnNavigationItemSelectedListener(Lcom/google/android/material/bottomnavigation/BottomNavigationView$OnNavigationItemSelectedListener;)V

    const/4 p2, 0x0

    .line 15
    invoke-static {p1, p2}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    const/4 p1, 0x0

    .line 17
    :goto_0
    iget-object p2, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    invoke-virtual {p2}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->getMenu()Landroid/view/Menu;

    move-result-object p2

    invoke-interface {p2}, Landroid/view/Menu;->size()I

    move-result p2

    if-ge p1, p2, :cond_0

    .line 18
    iget-object p2, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    invoke-virtual {p2}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->getMenu()Landroid/view/Menu;

    move-result-object p2

    invoke-interface {p2, p1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object p2

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 21
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b()V

    return-void
.end method

.method private b()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    invoke-virtual {v0}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    invoke-virtual {v0}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/Menu;->size()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->d:Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    .line 14
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->d:Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_6

    .line 15
    iget-object v2, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->d:Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    invoke-virtual {v2, v0}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->getItemTabButtonId(I)I

    move-result v2

    .line 16
    iget-object v3, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->e:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/MenuItem;

    .line 17
    invoke-interface {v4}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    if-ne v5, v2, :cond_0

    .line 18
    iget-object v5, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    .line 19
    invoke-virtual {v5}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v5

    .line 21
    invoke-interface {v4}, Landroid/view/MenuItem;->getGroupId()I

    move-result v6

    .line 22
    invoke-interface {v4}, Landroid/view/MenuItem;->getItemId()I

    move-result v7

    .line 23
    invoke-interface {v4}, Landroid/view/MenuItem;->getOrder()I

    move-result v8

    .line 24
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v9

    .line 25
    invoke-interface {v4}, Landroid/view/MenuItem;->getItemId()I

    move-result v10

    .line 26
    sget v11, Lcom/pspdfkit/R$id;->pspdf__menu_pdf_outline_view_outline:I

    if-ne v10, v11, :cond_1

    .line 27
    sget v10, Lcom/pspdfkit/R$string;->pspdf__activity_menu_outline:I

    .line 28
    invoke-static {v9, v10, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    .line 29
    :cond_1
    sget v11, Lcom/pspdfkit/R$id;->pspdf__menu_pdf_outline_view_bookmarks:I

    if-ne v10, v11, :cond_2

    .line 30
    sget v10, Lcom/pspdfkit/R$string;->pspdf__bookmarks:I

    .line 31
    invoke-static {v9, v10, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    .line 32
    :cond_2
    sget v11, Lcom/pspdfkit/R$id;->pspdf__menu_pdf_outline_view_document_info:I

    if-ne v10, v11, :cond_3

    .line 33
    sget v10, Lcom/pspdfkit/R$string;->pspdf__document_info:I

    .line 34
    invoke-static {v9, v10, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    .line 35
    :cond_3
    sget v11, Lcom/pspdfkit/R$id;->pspdf__menu_pdf_outline_view_annotations:I

    if-ne v10, v11, :cond_4

    .line 36
    sget v10, Lcom/pspdfkit/R$string;->pspdf__annotations:I

    .line 37
    invoke-static {v9, v10, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    :cond_4
    const-string v9, ""

    .line 39
    :goto_2
    invoke-interface {v5, v6, v7, v8, v9}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v5

    .line 44
    invoke-interface {v4}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_1

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_6
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->d:Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_7

    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->c:Landroidx/viewpager/widget/ViewPager;

    if-eqz v0, :cond_7

    .line 47
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    invoke-virtual {v0, v1}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->setOnNavigationItemSelectedListener(Lcom/google/android/material/bottomnavigation/BottomNavigationView$OnNavigationItemSelectedListener;)V

    .line 49
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->d:Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    iget-object v2, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->c:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v2}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->getItemTabButtonId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->setSelectedItemId(I)V

    .line 51
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    invoke-virtual {v0, p0}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->setOnNavigationItemSelectedListener(Lcom/google/android/material/bottomnavigation/BottomNavigationView$OnNavigationItemSelectedListener;)V

    :cond_7
    return-void
.end method

.method public final a(Landroidx/viewpager/widget/ViewPager;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v0

    .line 2
    instance-of v1, v0, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    if-eqz v1, :cond_0

    .line 6
    move-object v1, v0

    check-cast v1, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    iput-object v1, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->d:Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    .line 7
    iput-object p1, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->c:Landroidx/viewpager/widget/ViewPager;

    .line 8
    invoke-virtual {p1, p0}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 11
    new-instance p1, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView$a;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView$a;-><init>(Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;)V

    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/PagerAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void

    .line 12
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "bindViewPager() was called with ViewPager that does not have an OutlinePagerAdapter set."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Lcom/pspdfkit/internal/rl;)V
    .locals 7

    .line 52
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    iget v1, p1, Lcom/pspdfkit/internal/rl;->D:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 53
    new-instance v0, Landroid/content/res/ColorStateList;

    const/4 v1, 0x2

    new-array v2, v1, [[I

    const/4 v3, 0x1

    new-array v4, v3, [I

    const v5, 0x10100a0

    const/4 v6, 0x0

    aput v5, v4, v6

    aput-object v4, v2, v6

    new-array v4, v6, [I

    aput-object v4, v2, v3

    new-array v1, v1, [I

    iget v4, p1, Lcom/pspdfkit/internal/rl;->C:I

    aput v4, v1, v6

    iget v4, p1, Lcom/pspdfkit/internal/rl;->B:I

    aput v4, v1, v3

    invoke-direct {v0, v2, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 57
    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    invoke-virtual {v1, v0}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->setItemIconTintList(Landroid/content/res/ColorStateList;)V

    .line 58
    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    invoke-virtual {v1, v0}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->setItemTextColor(Landroid/content/res/ColorStateList;)V

    .line 60
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/MenuItem;

    .line 61
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/pspdfkit/R$id;->pspdf__menu_pdf_outline_view_outline:I

    if-ne v2, v3, :cond_1

    .line 62
    iget v2, p1, Lcom/pspdfkit/internal/rl;->x:I

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 63
    :cond_1
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/pspdfkit/R$id;->pspdf__menu_pdf_outline_view_bookmarks:I

    if-ne v2, v3, :cond_2

    .line 64
    iget v2, p1, Lcom/pspdfkit/internal/rl;->y:I

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 65
    :cond_2
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/pspdfkit/R$id;->pspdf__menu_pdf_outline_view_annotations:I

    if-ne v2, v3, :cond_3

    .line 66
    iget v2, p1, Lcom/pspdfkit/internal/rl;->z:I

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 67
    :cond_3
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/pspdfkit/R$id;->pspdf__menu_pdf_outline_view_document_info:I

    if-ne v2, v3, :cond_0

    .line 68
    iget v2, p1, Lcom/pspdfkit/internal/rl;->A:I

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0

    :cond_4
    return-void
.end method

.method public final onNavigationItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->c:Landroidx/viewpager/widget/ViewPager;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->d:Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    if-eqz v1, :cond_0

    .line 2
    invoke-virtual {v0, p0}, Landroidx/viewpager/widget/ViewPager;->removeOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->c:Landroidx/viewpager/widget/ViewPager;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->d:Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    invoke-virtual {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->getPositionOfItemWithTabButtonId(I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->c:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {p1, p0}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final onPageScrollStateChanged(I)V
    .locals 0

    return-void
.end method

.method public final onPageScrolled(IFI)V
    .locals 0

    return-void
.end method

.method public final onPageSelected(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->d:Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->setOnNavigationItemSelectedListener(Lcom/google/android/material/bottomnavigation/BottomNavigationView$OnNavigationItemSelectedListener;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->d:Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView$OutlinePagerAdapter;->getItemTabButtonId(I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->setSelectedItemId(I)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/views/utils/OutlinePagerTabView;->b:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    invoke-virtual {p1, p0}, Lcom/google/android/material/bottomnavigation/BottomNavigationView;->setOnNavigationItemSelectedListener(Lcom/google/android/material/bottomnavigation/BottomNavigationView$OnNavigationItemSelectedListener;)V

    :cond_0
    return-void
.end method

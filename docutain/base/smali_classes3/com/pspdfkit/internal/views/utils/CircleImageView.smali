.class public Lcom/pspdfkit/internal/views/utils/CircleImageView;
.super Landroidx/appcompat/widget/AppCompatImageView;
.source "SourceFile"


# static fields
.field private static final o:Landroid/widget/ImageView$ScaleType;


# instance fields
.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private e:I

.field private f:I

.field private g:I

.field private h:Z

.field private i:Landroid/graphics/Bitmap;

.field private j:Landroid/graphics/BitmapShader;

.field private k:I

.field private l:I

.field private m:F

.field private n:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    sput-object v0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->o:Landroid/widget/ImageView$ScaleType;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->b:Landroid/graphics/Paint;

    .line 3
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->c:Landroid/graphics/Paint;

    .line 4
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->d:Landroid/graphics/Paint;

    const/4 p1, 0x4

    .line 5
    iput p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->e:I

    const p1, -0x777778

    .line 6
    iput p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->f:I

    const/4 p1, -0x1

    .line 7
    iput p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->g:I

    const/4 p1, 0x0

    .line 8
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->h:Z

    .line 20
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/utils/CircleImageView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/internal/views/utils/CircleImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->b:Landroid/graphics/Paint;

    .line 24
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->c:Landroid/graphics/Paint;

    .line 25
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->d:Landroid/graphics/Paint;

    const/4 p1, 0x4

    .line 26
    iput p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->e:I

    const p1, -0x777778

    .line 27
    iput p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->f:I

    const/4 p1, -0x1

    .line 28
    iput p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->g:I

    const/4 p1, 0x0

    .line 29
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->h:Z

    .line 50
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/utils/CircleImageView;->a()V

    return-void
.end method

.method private static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 6

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 5
    :cond_0
    instance-of v1, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    .line 6
    check-cast p0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0

    .line 7
    :cond_1
    instance-of v1, p0, Landroid/graphics/drawable/ColorDrawable;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    .line 13
    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 14
    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 16
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 17
    invoke-virtual {v3}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    invoke-virtual {p0, v1, v1, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 18
    invoke-virtual {p0, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :catch_0
    move-exception p0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit"

    const-string v3, "Can\'t create bitmap in CircleImageView"

    .line 21
    invoke-static {v2, p0, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    .line 22
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "ColorDrawable not supported."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private a()V
    .locals 1

    .line 23
    sget-object v0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->o:Landroid/widget/ImageView$ScaleType;

    invoke-super {p0, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 24
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/utils/CircleImageView;->b()V

    return-void
.end method

.method private b()V
    .locals 5

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->k:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->l:I

    if-nez v0, :cond_0

    return-void

    .line 5
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->e:I

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v0

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->i:Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 8
    new-instance v1, Landroid/graphics/BitmapShader;

    iget-object v3, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->i:Landroid/graphics/Bitmap;

    sget-object v4, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-direct {v1, v3, v4, v4}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->j:Landroid/graphics/BitmapShader;

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->c:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->j:Landroid/graphics/BitmapShader;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 11
    iget-boolean v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->h:Z

    if-eqz v1, :cond_1

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->c:Landroid/graphics/Paint;

    const/16 v3, 0x4b

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 16
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->d:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 17
    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 18
    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->d:Landroid/graphics/Paint;

    iget v3, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->f:I

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 19
    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->d:Landroid/graphics/Paint;

    int-to-float v3, v0

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 21
    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->b:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 22
    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 23
    iget-object v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->b:Landroid/graphics/Paint;

    iget v2, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->g:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 25
    iget v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->l:I

    sub-int/2addr v1, v0

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v3, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->k:I

    sub-int/2addr v3, v0

    int-to-float v0, v3

    div-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->n:F

    .line 27
    iget v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->l:I

    int-to-float v0, v0

    div-float/2addr v0, v2

    iget v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->k:I

    int-to-float v1, v1

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->m:F

    .line 29
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2
    iput-boolean p2, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->h:Z

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/views/utils/CircleImageView;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->i:Landroid/graphics/Bitmap;

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/utils/CircleImageView;->b()V

    return-void
.end method

.method public getScaleType()Landroid/widget/ImageView$ScaleType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->o:Landroid/widget/ImageView$ScaleType;

    return-object v0
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->k:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget v2, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->l:I

    int-to-float v2, v2

    div-float/2addr v2, v1

    iget v3, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->m:F

    iget-object v4, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->i:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 5
    iget v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->k:I

    int-to-float v0, v0

    div-float/2addr v0, v1

    iget v2, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->l:I

    int-to-float v2, v2

    div-float/2addr v2, v1

    iget v3, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->m:F

    iget-object v4, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 9
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->k:I

    int-to-float v0, v0

    div-float/2addr v0, v1

    iget v2, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->l:I

    int-to-float v2, v2

    div-float/2addr v2, v1

    iget v1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->n:F

    iget-object v3, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final onMeasure(II)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p1}, Landroidx/appcompat/widget/AppCompatImageView;->onMeasure(II)V

    return-void
.end method

.method protected final onSizeChanged(IIII)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroidx/appcompat/widget/AppCompatImageView;->onSizeChanged(IIII)V

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->k:I

    .line 3
    iput p2, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->l:I

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/utils/CircleImageView;->b()V

    return-void
.end method

.method public setAdjustViewBounds(Z)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 1
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "adjustViewBounds not supported."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->g:I

    if-ne p1, v0, :cond_0

    return-void

    .line 5
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->g:I

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setBackgroundColorResource(I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/utils/CircleImageView;->setBackgroundColor(I)V

    return-void
.end method

.method public setBorderColor(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->f:I

    if-ne v0, p1, :cond_0

    return-void

    .line 5
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->f:I

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setBorderColorResource(I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/utils/CircleImageView;->setBorderColor(I)V

    return-void
.end method

.method public setBorderWidthDp(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->e:I

    if-ne v0, p1, :cond_0

    return-void

    .line 5
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->e:I

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/utils/CircleImageView;->b()V

    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->i:Landroid/graphics/Bitmap;

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/utils/CircleImageView;->b()V

    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/views/utils/CircleImageView;->a(Landroid/graphics/drawable/Drawable;Z)V

    return-void
.end method

.method public setImageResource(I)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 2
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/views/utils/CircleImageView;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->i:Landroid/graphics/Bitmap;

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/utils/CircleImageView;->b()V

    return-void
.end method

.method public setScaleType(Landroid/widget/ImageView$ScaleType;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/views/utils/CircleImageView;->o:Landroid/widget/ImageView$ScaleType;

    if-ne p1, v0, :cond_0

    return-void

    .line 2
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "ScaleType %s not supported."

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

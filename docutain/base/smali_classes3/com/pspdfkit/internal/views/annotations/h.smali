.class public final Lcom/pspdfkit/internal/views/annotations/h;
.super Landroidx/appcompat/widget/AppCompatImageView;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/views/annotations/a;
.implements Lcom/pspdfkit/internal/mo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/appcompat/widget/AppCompatImageView;",
        "Lcom/pspdfkit/internal/views/annotations/a<",
        "Lcom/pspdfkit/annotations/Annotation;",
        ">;",
        "Lcom/pspdfkit/internal/mo;"
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private c:Lcom/pspdfkit/annotations/Annotation;

.field private final d:Lcom/pspdfkit/internal/views/annotations/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/views/annotations/i<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;I)V
    .locals 1

    const/4 p3, 0x0

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p3, v0}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2
    new-instance p3, Lcom/pspdfkit/internal/views/annotations/i;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/views/annotations/i;-><init>(Lcom/pspdfkit/internal/views/annotations/a;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/views/annotations/h;->d:Lcom/pspdfkit/internal/views/annotations/i;

    .line 23
    iput-object p2, p0, Lcom/pspdfkit/internal/views/annotations/h;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/pspdfkit/R$dimen;->pspdf__view_annotation_size:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/views/annotations/h;->e:I

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic a(FLandroid/graphics/Matrix;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$a(Lcom/pspdfkit/internal/views/annotations/a;FLandroid/graphics/Matrix;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/views/annotations/a$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/annotations/a$a<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/h;->d:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/annotations/i;->a(Lcom/pspdfkit/internal/views/annotations/a$a;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/h;->c:Lcom/pspdfkit/annotations/Annotation;

    if-eqz p1, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/h;->d:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/i;->b()V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/h;->c:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 6
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 7
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/h;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result v0

    invoke-virtual {p0, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setAlpha(F)V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/h;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/internal/ao;->a(Lcom/pspdfkit/annotations/Annotation;)I

    move-result v0

    .line 12
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/h;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->hasInstantComments()Z

    move-result v0

    if-nez v0, :cond_0

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/h;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 18
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/h;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/h;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v2

    .line 19
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ga;->a(IZZ)I

    move-result v0

    .line 22
    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v0, v2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/h;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    .line 26
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot update NoteAnnotationView if no annotation is set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic b(Z)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$b(Lcom/pspdfkit/internal/views/annotations/a;Z)Z

    move-result p1

    return p1
.end method

.method public synthetic e()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$e(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v0

    return v0
.end method

.method public synthetic f()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$f(Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method public synthetic g()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$g(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v0

    return v0
.end method

.method public getAnnotation()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/h;->c:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method public bridge synthetic getApproximateMemoryUsage()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic getContentScaler()Lcom/pspdfkit/internal/k0;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$getContentScaler(Lcom/pspdfkit/internal/views/annotations/a;)Lcom/pspdfkit/internal/k0;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPageRect()Lcom/pspdfkit/utils/PageRect;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$getPageRect(Lcom/pspdfkit/internal/views/annotations/a;)Lcom/pspdfkit/utils/PageRect;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$i(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v0

    return v0
.end method

.method public synthetic k()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$k(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v0

    return v0
.end method

.method public synthetic o()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$o(Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method public final onProvideStructure(Landroid/view/ViewStructure;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->onProvideStructure(Landroid/view/ViewStructure;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/h;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/h;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/h;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ViewStructure;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final p()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/h;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/h;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isNoteAnnotationNoZoomHandlingEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 6
    :goto_0
    invoke-static {p0, v0}, Lcom/pspdfkit/internal/views/annotations/b;->a(Lcom/pspdfkit/internal/views/annotations/a;Z)Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v0

    .line 9
    new-instance v1, Lcom/pspdfkit/utils/Size;

    iget v2, p0, Lcom/pspdfkit/internal/views/annotations/h;->e:I

    int-to-float v2, v2

    invoke-direct {v1, v2, v2}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    iput-object v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->minSize:Lcom/pspdfkit/utils/Size;

    .line 12
    iget-boolean v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->noZoom:Z

    if-eqz v1, :cond_1

    .line 13
    new-instance v1, Lcom/pspdfkit/utils/Size;

    iget v2, p0, Lcom/pspdfkit/internal/views/annotations/h;->e:I

    int-to-float v2, v2

    invoke-direct {v1, v2, v2}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    iput-object v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->fixedScreenSize:Lcom/pspdfkit/utils/Size;

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 15
    iput-object v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->fixedScreenSize:Lcom/pspdfkit/utils/Size;

    .line 17
    :goto_1
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final recycle()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/h;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/h;->d:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/i;->a()V

    return-void
.end method

.method public setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->FILE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Only note and file annotations are supported."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 4
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/h;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    .line 5
    :cond_2
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/h;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/h;->p()V

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/h;->b()V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/h;->d:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/i;->b()V

    return-void
.end method

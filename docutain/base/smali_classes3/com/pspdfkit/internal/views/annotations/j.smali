.class public Lcom/pspdfkit/internal/views/annotations/j;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/views/annotations/a;
.implements Lcom/pspdfkit/internal/mo;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/annotations/j$c;,
        Lcom/pspdfkit/internal/views/annotations/j$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Lcom/pspdfkit/internal/views/annotations/a<",
        "Lcom/pspdfkit/annotations/Annotation;",
        ">;",
        "Lcom/pspdfkit/internal/mo;"
    }
.end annotation


# instance fields
.field protected final b:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private final c:I

.field private final d:I

.field private final e:Ljava/lang/Integer;

.field private final f:Ljava/lang/Integer;

.field private final g:Z

.field private final h:Z

.field private final i:Z

.field private j:Lcom/pspdfkit/annotations/Annotation;

.field private k:Landroid/graphics/Bitmap;

.field private l:I

.field private m:I

.field private n:Lio/reactivex/rxjava3/disposables/Disposable;

.field private o:Z

.field private final p:Lcom/pspdfkit/internal/views/annotations/i;

.field private q:Landroid/graphics/Matrix;

.field private r:Lcom/pspdfkit/internal/views/annotations/j$b;

.field protected final s:Lcom/pspdfkit/internal/views/annotations/j$c;

.field private t:Z

.field private final u:Lcom/pspdfkit/utils/PageRect;


# direct methods
.method public static synthetic $r8$lambda$99DjqPCSRA2Jbdx8eYAZHP1-hRg(Lcom/pspdfkit/internal/views/annotations/j;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/annotations/j;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$KlGSqf1_iVTeDdBHcvZ1w5qgo1U(Lcom/pspdfkit/internal/views/annotations/j;IILandroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/views/annotations/j;->a(IILandroid/graphics/Bitmap;)V

    return-void
.end method

.method public static synthetic $r8$lambda$RbpMi7v8d0_XBRI2t3bAc2bC9kE(Lcom/pspdfkit/internal/views/annotations/j;IILcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)Lio/reactivex/rxjava3/core/SingleSource;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/views/annotations/j;->a(IILcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)Lio/reactivex/rxjava3/core/SingleSource;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/i;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/views/annotations/i;-><init>(Lcom/pspdfkit/internal/views/annotations/a;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->p:Lcom/pspdfkit/internal/views/annotations/i;

    .line 25
    new-instance v0, Lcom/pspdfkit/utils/PageRect;

    invoke-direct {v0}, Lcom/pspdfkit/utils/PageRect;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->u:Lcom/pspdfkit/utils/PageRect;

    .line 33
    iput-object p2, p0, Lcom/pspdfkit/internal/views/annotations/j;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 35
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/j$c;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/views/annotations/j$c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    .line 36
    sget-object p1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 37
    new-instance p1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {p1, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 39
    invoke-static {p3, p2}, Lcom/pspdfkit/internal/x5;->a(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->c:I

    .line 40
    invoke-static {p3, p2}, Lcom/pspdfkit/internal/x5;->b(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->d:I

    .line 41
    invoke-static {}, Lcom/pspdfkit/internal/x5;->b()Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->e:Ljava/lang/Integer;

    .line 43
    invoke-static {p3, p2}, Lcom/pspdfkit/internal/x5;->d(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->f:Ljava/lang/Integer;

    .line 45
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->g:Z

    .line 46
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->h:Z

    .line 47
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->showSignHereOverlay()Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->i:Z

    const/4 p1, 0x0

    .line 49
    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private synthetic a(IILcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)Lio/reactivex/rxjava3/core/SingleSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->j:Lcom/pspdfkit/annotations/Annotation;

    .line 15
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/pspdfkit/internal/n4;->a(II)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 16
    invoke-virtual {v0, p1, p3}, Lcom/pspdfkit/annotations/Annotation;->renderToBitmapAsync(Landroid/graphics/Bitmap;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(IILandroid/graphics/Bitmap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 17
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/n4;->d(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    .line 18
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->n:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 20
    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/views/annotations/j;->a(Landroid/graphics/Bitmap;)V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->p:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/i;->b()V

    .line 25
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne p1, v0, :cond_0

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    if-eq p2, p1, :cond_1

    .line 26
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/j;->b()V

    .line 29
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->r:Lcom/pspdfkit/internal/views/annotations/j$b;

    if-eqz p1, :cond_2

    .line 30
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/j$b;->a()V

    :cond_2
    return-void
.end method

.method private synthetic a(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Could not render annotation: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->j:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.PdfView"

    invoke-static {v2, p1, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private q()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->j:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->q:Landroid/graphics/Matrix;

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->j:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/j;->q:Landroid/graphics/Matrix;

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->l:I

    .line 7
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    neg-float v0, v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->q:Landroid/graphics/Matrix;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->m:I

    .line 12
    iget v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->l:I

    const/4 v2, 0x0

    const/16 v3, 0x800

    if-le v1, v0, :cond_1

    .line 13
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v1, v0

    .line 14
    iget v3, p0, Lcom/pspdfkit/internal/views/annotations/j;->l:I

    int-to-float v3, v3

    add-float/2addr v3, v2

    div-float/2addr v1, v3

    .line 15
    iget v2, p0, Lcom/pspdfkit/internal/views/annotations/j;->m:I

    int-to-float v2, v2

    mul-float v2, v2, v1

    float-to-int v1, v2

    goto :goto_0

    .line 16
    :cond_1
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v0, v1

    .line 17
    iget v3, p0, Lcom/pspdfkit/internal/views/annotations/j;->m:I

    int-to-float v3, v3

    add-float/2addr v3, v2

    div-float/2addr v0, v3

    .line 18
    iget v2, p0, Lcom/pspdfkit/internal/views/annotations/j;->l:I

    int-to-float v2, v2

    mul-float v2, v2, v0

    float-to-int v0, v2

    :goto_0
    if-eqz v0, :cond_3

    if-nez v1, :cond_2

    goto :goto_1

    .line 26
    :cond_2
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/j;->n:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 27
    invoke-static {v2}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 28
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/j;->r()Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;->build()Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;

    move-result-object v2

    .line 29
    new-instance v3, Lcom/pspdfkit/internal/views/annotations/j$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0, v0, v1, v2}, Lcom/pspdfkit/internal/views/annotations/j$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/annotations/j;IILcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)V

    invoke-static {v3}, Lio/reactivex/rxjava3/core/Single;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 34
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/u;

    const/4 v5, 0x5

    .line 35
    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v4

    const-wide/16 v5, 0x14

    .line 36
    invoke-virtual {v2, v5, v6, v3, v4}, Lio/reactivex/rxjava3/core/Single;->delaySubscription(JLjava/util/concurrent/TimeUnit;Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    .line 40
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    new-instance v3, Lcom/pspdfkit/internal/views/annotations/j$$ExternalSyntheticLambda1;

    invoke-direct {v3, p0, v0, v1}, Lcom/pspdfkit/internal/views/annotations/j$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/views/annotations/j;II)V

    new-instance v0, Lcom/pspdfkit/internal/views/annotations/j$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/views/annotations/j$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/views/annotations/j;)V

    .line 41
    invoke-virtual {v2, v3, v0}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->n:Lio/reactivex/rxjava3/disposables/Disposable;

    const/4 v0, 0x0

    .line 62
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->o:Z

    return-void

    .line 63
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->p:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/i;->b()V

    :cond_4
    :goto_2
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final a(FLandroid/graphics/Matrix;)V
    .locals 0

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->q:Landroid/graphics/Matrix;

    if-nez p1, :cond_0

    .line 7
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->q:Landroid/graphics/Matrix;

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->q:Landroid/graphics/Matrix;

    invoke-virtual {p1, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 10
    iget-boolean p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->o:Z

    if-eqz p1, :cond_1

    .line 11
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/j;->q()V

    goto :goto_0

    .line 13
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    :goto_0
    return-void
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 32
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->k:Landroid/graphics/Bitmap;

    .line 33
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/annotations/j;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 35
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/j$c;->b()V

    .line 36
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/j;->s()V

    .line 39
    iget-boolean p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->t:Z

    if-eqz p1, :cond_0

    .line 40
    sget p1, Lcom/pspdfkit/internal/views/annotations/b;->b:I

    .line 41
    invoke-interface {p0}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/views/annotations/b;->a(Lcom/pspdfkit/internal/views/annotations/a;Z)Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 42
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/j$c;->b()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/views/annotations/a$a;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->p:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/annotations/i;->a(Lcom/pspdfkit/internal/views/annotations/a$a;)V

    .line 4
    iget-boolean p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->o:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->n:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->p:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/i;->b()V

    :cond_1
    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->o:Z

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/j;->q()V

    return-void
.end method

.method public synthetic b(Z)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$b(Lcom/pspdfkit/internal/views/annotations/a;Z)Z

    move-result p1

    return p1
.end method

.method public synthetic e()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$e(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v0

    return v0
.end method

.method public synthetic f()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$f(Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method public final g()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->j:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getAnnotation()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->j:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method public getApproximateMemoryUsage()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->k:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v0

    return v0

    .line 4
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    sget v1, Lcom/pspdfkit/internal/ao;->a:I

    const-string v1, "layoutParams"

    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1118
    instance-of v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    if-eqz v1, :cond_1

    .line 1119
    check-cast v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    iget-object v0, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v0

    const-string v1, "layoutParams.pageRect.screenRect"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1121
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    const/16 v2, 0x800

    .line 1122
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1123
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    .line 1124
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    mul-int v0, v0, v1

    mul-int/lit8 v0, v0, 0x4

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-object v0
.end method

.method public bridge synthetic getContentScaler()Lcom/pspdfkit/internal/k0;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$getContentScaler(Lcom/pspdfkit/internal/views/annotations/a;)Lcom/pspdfkit/internal/k0;

    move-result-object v0

    return-object v0
.end method

.method public getPageRect()Lcom/pspdfkit/utils/PageRect;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->t:Z

    if-nez v0, :cond_0

    .line 2
    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$getPageRect(Lcom/pspdfkit/internal/views/annotations/a;)Lcom/pspdfkit/utils/PageRect;

    move-result-object v0

    return-object v0

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->u:Lcom/pspdfkit/utils/PageRect;

    return-object v0
.end method

.method public getRenderedAnnotationBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->k:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final i()Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/j;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getAppearanceStreamGenerator()Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    move-result-object v2

    if-eqz v2, :cond_1

    return v1

    .line 9
    :cond_1
    sget-object v2, Lcom/pspdfkit/internal/views/annotations/j$a;->a:[I

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    return v1

    :pswitch_0
    const/4 v0, 0x0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic k()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public synthetic o()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$o(Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 0

    .line 1
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 2
    iget-boolean p2, p0, Lcom/pspdfkit/internal/views/annotations/j;->o:Z

    if-eqz p2, :cond_0

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/j;->q()V

    :cond_0
    if-eqz p1, :cond_1

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/j$c;->b()V

    :cond_1
    return-void
.end method

.method protected final onSizeChanged(IIII)V
    .locals 0

    .line 1
    iget-boolean p3, p0, Lcom/pspdfkit/internal/views/annotations/j;->o:Z

    if-nez p3, :cond_1

    iget-object p3, p0, Lcom/pspdfkit/internal/views/annotations/j;->k:Landroid/graphics/Bitmap;

    if-eqz p3, :cond_1

    iget p3, p0, Lcom/pspdfkit/internal/views/annotations/j;->l:I

    sub-int/2addr p1, p3

    .line 3
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    const/16 p3, 0xa

    if-gt p1, p3, :cond_0

    iget p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->m:I

    sub-int/2addr p2, p1

    .line 4
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p1

    if-le p1, p3, :cond_1

    :cond_0
    const/4 p1, 0x1

    .line 5
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->o:Z

    .line 7
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/j$c;->b()V

    return-void
.end method

.method public p()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->j:Lcom/pspdfkit/annotations/Annotation;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->t:Z

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 3
    sget v0, Lcom/pspdfkit/internal/views/annotations/b;->b:I

    .line 4
    invoke-interface {p0}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v0

    invoke-static {p0, v1}, Lcom/pspdfkit/internal/views/annotations/b;->a(Lcom/pspdfkit/internal/views/annotations/a;Z)Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/j$c;->b()V

    goto :goto_0

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->u:Lcom/pspdfkit/utils/PageRect;

    .line 7
    invoke-static {p0, v1}, Lcom/pspdfkit/internal/views/annotations/b;->a(Lcom/pspdfkit/internal/views/annotations/a;Z)Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v1

    .line 8
    iget-object v1, v1, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/utils/PageRect;->set(Lcom/pspdfkit/utils/PageRect;)V

    :goto_0
    return-void
.end method

.method protected r()Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;

    invoke-direct {v0}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;-><init>()V

    iget v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->c:I

    .line 2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;->formHighlightColor(Ljava/lang/Integer;)Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->e:Ljava/lang/Integer;

    .line 3
    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;->formItemHighlightColor(Ljava/lang/Integer;)Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;

    move-result-object v0

    iget v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->d:I

    .line 4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;->formRequiredFieldBorderColor(Ljava/lang/Integer;)Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->f:Ljava/lang/Integer;

    .line 5
    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;->signHereOverlayBackgroundColor(Ljava/lang/Integer;)Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->h:Z

    .line 6
    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;->toGrayscale(Z)Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->g:Z

    .line 7
    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;->invertColors(Z)Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->i:Z

    .line 8
    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;->showSignHereOverlay(Z)Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public recycle()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->n:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->n:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/annotations/j$c;->recycle()V

    .line 5
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->j:Lcom/pspdfkit/annotations/Annotation;

    const/4 v1, 0x0

    .line 6
    iput v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->m:I

    iput v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->l:I

    .line 7
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->o:Z

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->k:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 10
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/j;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/n4;->d(Landroid/graphics/Bitmap;)V

    .line 11
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->k:Landroid/graphics/Bitmap;

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->p:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/i;->a()V

    return-void
.end method

.method protected s()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->j:Lcom/pspdfkit/annotations/Annotation;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBlendMode()Lcom/pspdfkit/annotations/BlendMode;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/views/annotations/j$c;->setBlendMode(Lcom/pspdfkit/annotations/BlendMode;)V

    return-void
.end method

.method public setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->j:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/Annotation;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->j:Lcom/pspdfkit/annotations/Annotation;

    const/4 v0, 0x1

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->o:Z

    .line 4
    new-instance v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->j:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;->LAYOUT:Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;-><init>(Landroid/graphics/RectF;Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x0

    .line 5
    invoke-static {p0, v0}, Lcom/pspdfkit/internal/views/annotations/b;->a(Lcom/pspdfkit/internal/views/annotations/a;Z)Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/annotations/j$c;->setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V

    :cond_1
    return-void
.end method

.method protected setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public setOnRenderedListener(Lcom/pspdfkit/internal/views/annotations/j$b;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->r:Lcom/pspdfkit/internal/views/annotations/j$b;

    return-void
.end method

.method public setRefreshBoundingBoxAfterRendering(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->t:Z

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/j;->u:Lcom/pspdfkit/utils/PageRect;

    const/4 v0, 0x0

    .line 5
    invoke-static {p0, v0}, Lcom/pspdfkit/internal/views/annotations/b;->a(Lcom/pspdfkit/internal/views/annotations/a;Z)Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v0

    .line 6
    iget-object v0, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/utils/PageRect;->set(Lcom/pspdfkit/utils/PageRect;)V

    return-void
.end method

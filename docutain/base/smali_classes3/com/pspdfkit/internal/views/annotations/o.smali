.class public final Lcom/pspdfkit/internal/views/annotations/o;
.super Landroidx/appcompat/widget/AppCompatImageView;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/views/annotations/a;
.implements Lcom/pspdfkit/internal/mo;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/annotations/o$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/appcompat/widget/AppCompatImageView;",
        "Lcom/pspdfkit/internal/views/annotations/a<",
        "Lcom/pspdfkit/annotations/SoundAnnotation;",
        ">;",
        "Lcom/pspdfkit/internal/mo;"
    }
.end annotation


# static fields
.field private static final l:[I

.field private static final m:I

.field private static final n:I


# instance fields
.field private b:Lcom/pspdfkit/annotations/SoundAnnotation;

.field private final c:Lcom/pspdfkit/internal/views/annotations/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/views/annotations/i<",
            "Lcom/pspdfkit/annotations/SoundAnnotation;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Lcom/pspdfkit/internal/ks;

.field private j:Lcom/pspdfkit/internal/views/annotations/o$a;

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__SoundAnnotationIcon:[I

    sput-object v0, Lcom/pspdfkit/internal/views/annotations/o;->l:[I

    .line 2
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__soundAnnotationIconStyle:I

    sput v0, Lcom/pspdfkit/internal/views/annotations/o;->m:I

    .line 3
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_SoundAnnotationIcon:I

    sput v0, Lcom/pspdfkit/internal/views/annotations/o;->n:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4

    const/4 p2, 0x0

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/views/annotations/i;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/views/annotations/i;-><init>(Lcom/pspdfkit/internal/views/annotations/a;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/o;->c:Lcom/pspdfkit/internal/views/annotations/i;

    .line 27
    sget-object v1, Lcom/pspdfkit/internal/views/annotations/o$a;->a:Lcom/pspdfkit/internal/views/annotations/o$a;

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/o;->j:Lcom/pspdfkit/internal/views/annotations/o$a;

    .line 30
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->k:Z

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__sound_annotation_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->d:I

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/views/annotations/o;->l:[I

    sget v2, Lcom/pspdfkit/internal/views/annotations/o;->m:I

    sget v3, Lcom/pspdfkit/internal/views/annotations/o;->n:I

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 51
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__SoundAnnotationIcon_pspdf__iconColor:I

    const/high16 v1, -0x1000000

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->e:I

    .line 53
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__SoundAnnotationIcon_pspdf__backgroundColor:I

    const/4 v1, -0x1

    .line 54
    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 56
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SoundAnnotationIcon_pspdf__selectionColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__sound_annotation_selection:I

    .line 58
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 59
    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/views/annotations/o;->f:I

    .line 63
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SoundAnnotationIcon_pspdf__playbackColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__sound_annotation_playback:I

    .line 65
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 66
    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/views/annotations/o;->g:I

    .line 70
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SoundAnnotationIcon_pspdf__recordColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__sound_annotation_record:I

    .line 72
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 73
    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/views/annotations/o;->h:I

    .line 77
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 79
    new-instance p2, Lcom/pspdfkit/internal/ks;

    invoke-direct {p2, p1, v0}, Lcom/pspdfkit/internal/ks;-><init>(Landroid/content/Context;I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/annotations/o;->i:Lcom/pspdfkit/internal/ks;

    .line 80
    invoke-virtual {p0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private setState(Lcom/pspdfkit/internal/views/annotations/o$a;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->j:Lcom/pspdfkit/internal/views/annotations/o$a;

    if-ne v0, p1, :cond_0

    return-void

    .line 3
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/o;->j:Lcom/pspdfkit/internal/views/annotations/o$a;

    .line 4
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_4

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    goto :goto_0

    .line 15
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/o;->i:Lcom/pspdfkit/internal/ks;

    iget v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->h:I

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ks;->a(I)V

    goto :goto_0

    .line 16
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/o;->i:Lcom/pspdfkit/internal/ks;

    iget v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->g:I

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ks;->a(I)V

    goto :goto_0

    .line 17
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/o;->i:Lcom/pspdfkit/internal/ks;

    iget v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->f:I

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ks;->a(I)V

    goto :goto_0

    .line 18
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/o;->i:Lcom/pspdfkit/internal/ks;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ks;->a(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public synthetic a(FLandroid/graphics/Matrix;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$a(Lcom/pspdfkit/internal/views/annotations/a;FLandroid/graphics/Matrix;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/views/annotations/a$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/annotations/a$a<",
            "Lcom/pspdfkit/annotations/SoundAnnotation;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->c:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/annotations/i;->a(Lcom/pspdfkit/internal/views/annotations/a$a;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/o;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz p1, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/o;->c:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/i;->b()V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Cannot update SoundAnnotationView if no annotation is set."

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-static {v0}, Lcom/pspdfkit/internal/ao;->a(Lcom/pspdfkit/annotations/Annotation;)I

    move-result v0

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Lcom/pspdfkit/internal/views/annotations/o;->e:I

    .line 6
    invoke-static {v0}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 7
    invoke-static {v0, v1}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/o;->i:Lcom/pspdfkit/internal/ks;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ks;->a(Landroid/graphics/drawable/Drawable;)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getSoundAnnotationState()Lcom/pspdfkit/internal/ls;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 12
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/annotations/o;->setSoundAnnotationState(Lcom/pspdfkit/internal/ls;)V

    .line 14
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic b(Z)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$b(Lcom/pspdfkit/internal/views/annotations/a;Z)Z

    move-result p1

    return p1
.end method

.method public synthetic e()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$e(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v0

    return v0
.end method

.method public synthetic f()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$f(Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method public synthetic g()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$g(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getAnnotation()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/o;->getAnnotation()Lcom/pspdfkit/annotations/SoundAnnotation;

    move-result-object v0

    return-object v0
.end method

.method public getAnnotation()Lcom/pspdfkit/annotations/SoundAnnotation;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    return-object v0
.end method

.method public bridge synthetic getApproximateMemoryUsage()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic getContentScaler()Lcom/pspdfkit/internal/k0;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$getContentScaler(Lcom/pspdfkit/internal/views/annotations/a;)Lcom/pspdfkit/internal/k0;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPageRect()Lcom/pspdfkit/utils/PageRect;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$getPageRect(Lcom/pspdfkit/internal/views/annotations/a;)Lcom/pspdfkit/utils/PageRect;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$i(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 3

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->k:Z

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/o;->j:Lcom/pspdfkit/internal/views/annotations/o$a;

    sget-object v2, Lcom/pspdfkit/internal/views/annotations/o$a;->c:Lcom/pspdfkit/internal/views/annotations/o$a;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/pspdfkit/internal/views/annotations/o$a;->d:Lcom/pspdfkit/internal/views/annotations/o$a;

    if-eq v1, v2, :cond_0

    .line 3
    sget-object v1, Lcom/pspdfkit/internal/views/annotations/o$a;->a:Lcom/pspdfkit/internal/views/annotations/o$a;

    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/views/annotations/o;->setState(Lcom/pspdfkit/internal/views/annotations/o$a;)V

    :cond_0
    return v0
.end method

.method public final o()V
    .locals 2

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->k:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->j:Lcom/pspdfkit/internal/views/annotations/o$a;

    sget-object v1, Lcom/pspdfkit/internal/views/annotations/o$a;->c:Lcom/pspdfkit/internal/views/annotations/o$a;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/pspdfkit/internal/views/annotations/o$a;->d:Lcom/pspdfkit/internal/views/annotations/o$a;

    if-eq v0, v1, :cond_0

    .line 3
    sget-object v0, Lcom/pspdfkit/internal/views/annotations/o$a;->b:Lcom/pspdfkit/internal/views/annotations/o$a;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/annotations/o;->setState(Lcom/pspdfkit/internal/views/annotations/o$a;)V

    :cond_0
    return-void
.end method

.method public final onProvideStructure(Landroid/view/ViewStructure;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->onProvideStructure(Landroid/view/ViewStructure;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ViewStructure;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final p()V
    .locals 3

    const/4 v0, 0x1

    .line 1
    invoke-static {p0, v0}, Lcom/pspdfkit/internal/views/annotations/b;->a(Lcom/pspdfkit/internal/views/annotations/a;Z)Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/utils/Size;

    iget v2, p0, Lcom/pspdfkit/internal/views/annotations/o;->d:I

    int-to-float v2, v2

    invoke-direct {v1, v2, v2}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    iput-object v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->fixedScreenSize:Lcom/pspdfkit/utils/Size;

    .line 4
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final recycle()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/views/annotations/o$a;->a:Lcom/pspdfkit/internal/views/annotations/o$a;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/annotations/o;->setState(Lcom/pspdfkit/internal/views/annotations/o$a;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->c:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/i;->a()V

    return-void
.end method

.method public bridge synthetic setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/annotations/o;->setAnnotation(Lcom/pspdfkit/annotations/SoundAnnotation;)V

    return-void
.end method

.method public setAnnotation(Lcom/pspdfkit/annotations/SoundAnnotation;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/o;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 3
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/o;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/o;->p()V

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/o;->b()V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/o;->c:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/i;->b()V

    return-void
.end method

.method public setSoundAnnotationState(Lcom/pspdfkit/internal/ls;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_1

    .line 11
    :cond_0
    sget-object p1, Lcom/pspdfkit/internal/views/annotations/o$a;->c:Lcom/pspdfkit/internal/views/annotations/o$a;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/annotations/o;->setState(Lcom/pspdfkit/internal/views/annotations/o$a;)V

    goto :goto_1

    .line 12
    :cond_1
    sget-object p1, Lcom/pspdfkit/internal/views/annotations/o$a;->d:Lcom/pspdfkit/internal/views/annotations/o$a;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/annotations/o;->setState(Lcom/pspdfkit/internal/views/annotations/o$a;)V

    goto :goto_1

    .line 13
    :cond_2
    iget-boolean p1, p0, Lcom/pspdfkit/internal/views/annotations/o;->k:Z

    if-eqz p1, :cond_3

    sget-object p1, Lcom/pspdfkit/internal/views/annotations/o$a;->b:Lcom/pspdfkit/internal/views/annotations/o$a;

    goto :goto_0

    :cond_3
    sget-object p1, Lcom/pspdfkit/internal/views/annotations/o$a;->a:Lcom/pspdfkit/internal/views/annotations/o$a;

    :goto_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/annotations/o;->setState(Lcom/pspdfkit/internal/views/annotations/o$a;)V

    :goto_1
    return-void
.end method

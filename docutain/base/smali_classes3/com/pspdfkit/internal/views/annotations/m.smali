.class public final Lcom/pspdfkit/internal/views/annotations/m;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/views/annotations/a;
.implements Lcom/pspdfkit/internal/mo;
.implements Lcom/pspdfkit/internal/qc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/view/ViewGroup;",
        "Lcom/pspdfkit/internal/views/annotations/a<",
        "Lcom/pspdfkit/annotations/FreeTextAnnotation;",
        ">;",
        "Lcom/pspdfkit/internal/mo;",
        "Lcom/pspdfkit/internal/qc;"
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private final c:Lcom/pspdfkit/internal/views/annotations/e;

.field private final d:Lcom/pspdfkit/internal/views/annotations/n;

.field private final e:Landroid/graphics/Matrix;

.field private f:F

.field private final g:Landroid/graphics/RectF;

.field private final h:Landroid/graphics/RectF;

.field private i:Lcom/pspdfkit/annotations/BlendMode;

.field private j:Landroid/graphics/Paint;

.field private final k:Landroid/graphics/Rect;

.field private l:Lcom/pspdfkit/internal/pn;

.field private final m:Landroid/graphics/Matrix;

.field private final n:Ljava/util/ArrayList;

.field private final o:Landroid/graphics/Paint;

.field private final p:Landroid/graphics/Paint;


# direct methods
.method public static synthetic $r8$lambda$AdZrzlXcFzapLIh-aaLNNRwZmtU(Lcom/pspdfkit/internal/views/annotations/m;Lcom/pspdfkit/internal/views/annotations/a$a;Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/views/annotations/m;->a(Lcom/pspdfkit/internal/views/annotations/a$a;Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;)V
    .locals 4

    .line 1
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->e:Landroid/graphics/Matrix;

    .line 7
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->g:Landroid/graphics/RectF;

    .line 9
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->h:Landroid/graphics/RectF;

    .line 12
    sget-object v1, Lcom/pspdfkit/annotations/BlendMode;->NORMAL:Lcom/pspdfkit/annotations/BlendMode;

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->i:Lcom/pspdfkit/annotations/BlendMode;

    .line 15
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->j:Landroid/graphics/Paint;

    .line 18
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->k:Landroid/graphics/Rect;

    .line 23
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->m:Landroid/graphics/Matrix;

    .line 24
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->n:Ljava/util/ArrayList;

    .line 25
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->o:Landroid/graphics/Paint;

    .line 26
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->p:Landroid/graphics/Paint;

    .line 34
    iput-object p3, p0, Lcom/pspdfkit/internal/views/annotations/m;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 36
    new-instance v3, Lcom/pspdfkit/internal/views/annotations/e;

    invoke-direct {v3, p1, p2, p3, p4}, Lcom/pspdfkit/internal/views/annotations/e;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;)V

    iput-object v3, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    const/high16 p2, 0x3f800000    # 1.0f

    .line 38
    invoke-virtual {v3, p2, v0}, Lcom/pspdfkit/internal/views/annotations/e;->a(FLandroid/graphics/Matrix;)V

    const/4 p2, 0x0

    .line 40
    invoke-virtual {v3, p2}, Lcom/pspdfkit/internal/views/annotations/e;->setApplyAnnotationAlpha(Z)V

    .line 42
    invoke-virtual {v3, p2}, Lcom/pspdfkit/internal/views/annotations/e;->setDrawBackground(Z)V

    .line 44
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 46
    new-instance p4, Lcom/pspdfkit/internal/views/annotations/n;

    invoke-direct {p4, p1, p3}, Lcom/pspdfkit/internal/views/annotations/n;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    iput-object p4, p0, Lcom/pspdfkit/internal/views/annotations/m;->d:Lcom/pspdfkit/internal/views/annotations/n;

    .line 48
    invoke-virtual {p0, p4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 50
    invoke-virtual {p0, p2}, Landroid/view/View;->setWillNotDraw(Z)V

    .line 52
    sget-object p1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, p1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 53
    sget-object p1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    :goto_0
    const/4 p1, 0x4

    if-ge p2, p1, :cond_0

    .line 56
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/m;->n:Ljava/util/ArrayList;

    new-instance p3, Landroid/graphics/PointF;

    invoke-direct {p3}, Landroid/graphics/PointF;-><init>()V

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/FreeTextAnnotation;)F
    .locals 9

    const/4 v0, 0x0

    .line 88
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/views/annotations/m;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Z)Lcom/pspdfkit/utils/Size;

    move-result-object v0

    .line 89
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v1

    int-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v1

    .line 90
    iget v3, v0, Lcom/pspdfkit/utils/Size;->width:F

    float-to-double v3, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double v5, v5, v3

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    move-result-wide v3

    iget v5, v0, Lcom/pspdfkit/utils/Size;->height:F

    float-to-double v5, v5

    .line 91
    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v7

    mul-double v7, v7, v5

    invoke-static {v7, v8}, Ljava/lang/Math;->abs(D)D

    move-result-wide v5

    add-double/2addr v5, v3

    .line 92
    iget v3, v0, Lcom/pspdfkit/utils/Size;->width:F

    float-to-double v3, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v7

    mul-double v7, v7, v3

    invoke-static {v7, v8}, Ljava/lang/Math;->abs(D)D

    move-result-wide v3

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    float-to-double v7, v0

    .line 93
    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    mul-double v0, v0, v7

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    add-double/2addr v0, v3

    .line 95
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->g:Landroid/graphics/RectF;

    invoke-virtual {p1, v2}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p1

    .line 98
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-double v2, v2

    div-double/2addr v2, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result p1

    float-to-double v4, p1

    div-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    double-to-float p1, v0

    return p1
.end method

.method private a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Z)Lcom/pspdfkit/utils/Size;
    .locals 4

    if-eqz p2, :cond_0

    .line 56
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getTextInsets()Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object p2

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result v0

    const/4 v1, 0x0

    .line 57
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/ia;->a(Lcom/pspdfkit/utils/EdgeInsets;II)Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object p2

    goto :goto_0

    .line 60
    :cond_0
    new-instance p2, Lcom/pspdfkit/utils/EdgeInsets;

    invoke-direct {p2}, Lcom/pspdfkit/utils/EdgeInsets;-><init>()V

    .line 63
    :goto_0
    iget v0, p2, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    iget v1, p2, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    add-float/2addr v0, v1

    .line 64
    iget v1, p2, Lcom/pspdfkit/utils/EdgeInsets;->top:F

    iget p2, p2, Lcom/pspdfkit/utils/EdgeInsets;->bottom:F

    add-float/2addr v1, p2

    .line 66
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p2

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->h:Landroid/graphics/RectF;

    invoke-interface {p2, v2}, Lcom/pspdfkit/internal/pf;->getContentSize(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 69
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 70
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->needsFlippedContentSize()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 73
    new-instance p1, Landroid/graphics/RectF;

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result p2

    invoke-direct {p1, v3, v3, v2, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object p2, p1

    .line 75
    :cond_1
    invoke-virtual {p2}, Landroid/graphics/RectF;->sort()V

    .line 76
    new-instance p1, Lcom/pspdfkit/utils/Size;

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    sub-float/2addr v2, v0

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result p2

    sub-float/2addr p2, v1

    invoke-direct {p1, v2, p2}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    return-object p1

    .line 78
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result p2

    .line 80
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->g:Landroid/graphics/RectF;

    invoke-virtual {p1, v2}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p1

    .line 81
    invoke-virtual {p1}, Landroid/graphics/RectF;->sort()V

    const/16 v2, 0x5a

    if-eq p2, v2, :cond_4

    const/16 v2, 0x10e

    if-ne p2, v2, :cond_3

    goto :goto_1

    .line 86
    :cond_3
    new-instance p2, Lcom/pspdfkit/utils/Size;

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v2

    sub-float/2addr v2, v0

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result p1

    sub-float/2addr p1, v1

    invoke-direct {p2, v2, p1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    return-object p2

    .line 87
    :cond_4
    :goto_1
    new-instance p2, Lcom/pspdfkit/utils/Size;

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v2

    sub-float/2addr v2, v0

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result p1

    sub-float/2addr p1, v1

    invoke-direct {p2, v2, p1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    return-object p2
.end method

.method private synthetic a(Lcom/pspdfkit/internal/views/annotations/a$a;Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 0

    .line 39
    invoke-interface {p1, p0}, Lcom/pspdfkit/internal/views/annotations/a$a;->a(Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method private d()V
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/m;->getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 5
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/views/annotations/m;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Z)Lcom/pspdfkit/utils/Size;

    move-result-object v1

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, v1, Lcom/pspdfkit/utils/Size;->width:F

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    iget v1, v1, Lcom/pspdfkit/utils/Size;->height:F

    div-float/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 9
    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    const/4 v2, 0x1

    .line 13
    invoke-direct {p0, v0, v2}, Lcom/pspdfkit/internal/views/annotations/m;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Z)Lcom/pspdfkit/utils/Size;

    move-result-object v2

    .line 14
    new-instance v3, Lcom/pspdfkit/utils/Size;

    iget v4, v2, Lcom/pspdfkit/utils/Size;->width:F

    mul-float v4, v4, v1

    iget v2, v2, Lcom/pspdfkit/utils/Size;->height:F

    mul-float v2, v2, v1

    invoke-direct {v3, v4, v2}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 17
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v1

    int-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v1

    .line 18
    iget v4, v3, Lcom/pspdfkit/utils/Size;->width:F

    float-to-double v4, v4

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double v6, v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    iget v6, v3, Lcom/pspdfkit/utils/Size;->height:F

    float-to-double v6, v6

    .line 19
    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double v8, v8, v6

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    add-double/2addr v6, v4

    .line 20
    iget v4, v3, Lcom/pspdfkit/utils/Size;->width:F

    float-to-double v4, v4

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double v8, v8, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    iget v8, v3, Lcom/pspdfkit/utils/Size;->height:F

    float-to-double v8, v8

    .line 21
    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    mul-double v1, v1, v8

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v1

    add-double/2addr v1, v4

    .line 27
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/annotations/m;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;)F

    move-result v4

    .line 29
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getTextInsets()Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object v5

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v8

    invoke-interface {v8}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result v8

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v0

    .line 30
    invoke-static {v5, v8, v0}, Lcom/pspdfkit/internal/ia;->a(Lcom/pspdfkit/utils/EdgeInsets;II)Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object v0

    .line 32
    iget v5, v0, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    iget v8, v0, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    add-float/2addr v5, v8

    .line 33
    iget v8, v0, Lcom/pspdfkit/utils/EdgeInsets;->top:F

    iget v0, v0, Lcom/pspdfkit/utils/EdgeInsets;->bottom:F

    add-float/2addr v8, v0

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->e:Landroid/graphics/Matrix;

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-static {v9, v0}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v0

    mul-float v0, v0, v4

    .line 39
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    int-to-float v9, v9

    mul-float v5, v5, v0

    sub-float/2addr v9, v5

    float-to-double v9, v9

    div-double/2addr v9, v6

    invoke-static {v9, v10}, Ljava/lang/Math;->abs(D)D

    move-result-wide v5

    .line 40
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    int-to-float v7, v7

    mul-float v8, v8, v0

    sub-float/2addr v7, v8

    float-to-double v7, v7

    div-double/2addr v7, v1

    invoke-static {v7, v8}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    .line 41
    invoke-static {v5, v6, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    double-to-float v0, v0

    div-float/2addr v0, v4

    .line 48
    iget v1, v3, Lcom/pspdfkit/utils/Size;->width:F

    mul-float v1, v1, v0

    float-to-double v1, v1

    .line 49
    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    .line 50
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 52
    iget v3, v3, Lcom/pspdfkit/utils/Size;->height:F

    mul-float v3, v3, v0

    .line 53
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v0, v3

    .line 54
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 56
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v2, v1, v0}, Landroid/view/View;->measure(II)V

    return-void
.end method

.method private h()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/m;->getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/annotations/m;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;)F

    move-result v1

    .line 8
    :try_start_0
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/views/annotations/d;->m()Z

    move-result v2

    if-nez v2, :cond_1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 9
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/m;->e:Landroid/graphics/Matrix;

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v2

    .line 10
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    mul-float v1, v1, v2

    invoke-virtual {v3, v1}, Landroidx/appcompat/widget/AppCompatEditText;->setScaleX(F)V

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v2, v1}, Landroidx/appcompat/widget/AppCompatEditText;->setScaleY(F)V

    goto :goto_0

    .line 13
    :cond_1
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v2, v1}, Landroidx/appcompat/widget/AppCompatEditText;->setScaleX(F)V

    .line 14
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v2, v1}, Landroidx/appcompat/widget/AppCompatEditText;->setScaleY(F)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const-string v1, "PSPDFKit.Annotations"

    const-string v3, "updateDecoratedView "

    .line 18
    invoke-static {v1, v3, v2}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroidx/appcompat/widget/AppCompatEditText;->setRotation(F)V

    return-void
.end method

.method private j()V
    .locals 9

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/m;->getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result v1

    .line 7
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    .line 12
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBlendMode()Lcom/pspdfkit/annotations/BlendMode;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->i:Lcom/pspdfkit/annotations/BlendMode;

    .line 13
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/m;->j:Landroid/graphics/Paint;

    invoke-static {v3, v2}, Lcom/pspdfkit/internal/views/annotations/b;->a(Landroid/graphics/Paint;Lcom/pspdfkit/annotations/BlendMode;)Landroid/graphics/Paint;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->j:Landroid/graphics/Paint;

    goto :goto_0

    .line 15
    :cond_1
    sget-object v2, Lcom/pspdfkit/annotations/BlendMode;->NORMAL:Lcom/pspdfkit/annotations/BlendMode;

    iput-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->i:Lcom/pspdfkit/annotations/BlendMode;

    :goto_0
    const/4 v2, 0x0

    if-nez v1, :cond_4

    .line 22
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBlendMode()Lcom/pspdfkit/annotations/BlendMode;

    move-result-object v1

    sget v3, Lcom/pspdfkit/internal/views/annotations/b;->b:I

    .line 23
    sget-object v3, Lcom/pspdfkit/internal/views/annotations/b$a;->a:[I

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v3, v1

    const/4 v3, 0x1

    if-eq v1, v3, :cond_3

    const/4 v3, 0x2

    if-eq v1, v3, :cond_2

    goto :goto_1

    :cond_2
    const/high16 v2, -0x1000000

    goto :goto_1

    :cond_3
    const/4 v2, -0x1

    .line 24
    :goto_1
    invoke-virtual {p0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_2

    .line 26
    :cond_4
    invoke-virtual {p0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 29
    :goto_2
    new-instance v1, Lcom/pspdfkit/internal/pn;

    .line 31
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBorderColor()I

    move-result v2

    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/m;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v3}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result v3

    iget-object v4, p0, Lcom/pspdfkit/internal/views/annotations/m;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v4}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v4

    .line 32
    invoke-static {v2, v3, v4}, Lcom/pspdfkit/internal/ga;->a(IZZ)I

    move-result v4

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    .line 34
    invoke-virtual {v2}, Lcom/pspdfkit/internal/views/annotations/e;->getAnnotationBackgroundColor()I

    move-result v5

    .line 35
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBorderWidth()F

    move-result v6

    new-instance v8, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    .line 38
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v2

    .line 39
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v3

    .line 40
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffectIntensity()F

    move-result v7

    .line 41
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBorderDashArray()Ljava/util/List;

    move-result-object v0

    invoke-direct {v8, v2, v3, v7, v0}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;-><init>(Lcom/pspdfkit/annotations/BorderStyle;Lcom/pspdfkit/annotations/BorderEffect;FLjava/util/List;)V

    const/high16 v7, 0x3f800000    # 1.0f

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lcom/pspdfkit/internal/pn;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->l:Lcom/pspdfkit/internal/pn;

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final a(FLandroid/graphics/Matrix;)V
    .locals 3

    .line 40
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/m;->h()V

    .line 44
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    .line 45
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    .line 46
    iget-object v2, v1, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v2}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v2

    iget-object v0, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 47
    iget-object v0, v1, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/utils/PageRect;->updatePageRect(Landroid/graphics/Matrix;)V

    .line 49
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/views/annotations/n;->a(FLandroid/graphics/Matrix;)V

    .line 51
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->e:Landroid/graphics/Matrix;

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 52
    iput p1, p0, Lcom/pspdfkit/internal/views/annotations/m;->f:F

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/d;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/views/annotations/e;->a(FLandroid/graphics/Matrix;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/views/annotations/a$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/annotations/a$a<",
            "Lcom/pspdfkit/annotations/FreeTextAnnotation;",
            ">;)V"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    new-instance v1, Lcom/pspdfkit/internal/views/annotations/m$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/views/annotations/m$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/annotations/m;Lcom/pspdfkit/internal/views/annotations/a$a;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/annotations/e;->a(Lcom/pspdfkit/internal/views/annotations/a$a;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ka;Lcom/pspdfkit/configuration/PdfConfiguration;Landroid/view/MotionEvent;)Z
    .locals 3

    const-string v0, "mode"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/m;->getTextForScaling()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    if-eqz p3, :cond_0

    .line 35
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result p3

    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    if-eqz p3, :cond_1

    .line 36
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSelectedAnnotationFontScalingOnResizeEnabled()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 37
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object p1

    sget-object p2, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    if-ne p1, p2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public final b()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/e;->b()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/n;->b()V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/m;->h()V

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/m;->j()V

    return-void
.end method

.method public final b(Z)Z
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/annotations/e;->b(Z)Z

    move-result p1

    return p1
.end method

.method public final c()Lcom/pspdfkit/internal/views/annotations/e;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    return-object v0
.end method

.method protected final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/m;->getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->l:Lcom/pspdfkit/internal/pn;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->e:Landroid/graphics/Matrix;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v3, v2}, Lcom/pspdfkit/internal/h4;->a(FLandroid/graphics/Matrix;)Z

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getTextInsets()Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object v1

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result v2

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v4

    neg-int v4, v4

    .line 9
    invoke-static {v1, v2, v4}, Lcom/pspdfkit/internal/ia;->a(Lcom/pspdfkit/utils/EdgeInsets;II)Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object v1

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->e:Landroid/graphics/Matrix;

    .line 12
    invoke-static {v3, v2}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v2

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/annotations/m;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;)F

    move-result v3

    mul-float v2, v2, v3

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBorderWidth()F

    move-result v3

    mul-float v3, v3, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    .line 15
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v4

    rem-int/lit8 v4, v4, 0x5a

    const/4 v5, 0x3

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x2

    if-eqz v4, :cond_0

    .line 17
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->m:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/2addr v2, v8

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/2addr v3, v8

    int-to-float v3, v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getScaleX()F

    move-result v1

    mul-float v1, v1, v0

    float-to-int v0, v1

    .line 22
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v2}, Landroidx/appcompat/widget/AppCompatEditText;->getScaleX()F

    move-result v2

    mul-float v2, v2, v1

    float-to-int v1, v2

    .line 24
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/2addr v3, v8

    div-int/2addr v0, v8

    sub-int/2addr v3, v0

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    div-int/2addr v4, v8

    div-int/2addr v1, v8

    sub-int/2addr v4, v1

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 25
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/2addr v3, v8

    add-int/2addr v3, v0

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    div-int/2addr v4, v8

    sub-int/2addr v4, v1

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 26
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/2addr v3, v8

    add-int/2addr v3, v0

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    div-int/2addr v4, v8

    add-int/2addr v4, v1

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 27
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/2addr v3, v8

    sub-int/2addr v3, v0

    int-to-float v0, v3

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/2addr v3, v8

    add-int/2addr v3, v1

    int-to-float v1, v3

    invoke-virtual {v2, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->n:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->m:Landroid/graphics/Matrix;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/nu;->a(Ljava/util/ArrayList;Landroid/graphics/Matrix;)V

    goto/16 :goto_0

    .line 33
    :cond_0
    iget-object v4, p0, Lcom/pspdfkit/internal/views/annotations/m;->m:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v9

    div-int/2addr v9, v8

    int-to-float v9, v9

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v10

    div-int/2addr v10, v8

    int-to-float v10, v10

    invoke-virtual {v4, v0, v9, v10}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v4, v1, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    mul-float v4, v4, v2

    add-float/2addr v4, v3

    iget v7, v1, Lcom/pspdfkit/utils/EdgeInsets;->top:F

    mul-float v7, v7, v2

    add-float/2addr v7, v3

    invoke-virtual {v0, v4, v7}, Landroid/graphics/PointF;->set(FF)V

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->n:Ljava/util/ArrayList;

    .line 37
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 38
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget v6, v1, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    mul-float v6, v6, v2

    add-float/2addr v6, v3

    sub-float/2addr v4, v6

    iget v6, v1, Lcom/pspdfkit/utils/EdgeInsets;->top:F

    mul-float v6, v6, v2

    add-float/2addr v6, v3

    invoke-virtual {v0, v4, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->n:Ljava/util/ArrayList;

    .line 40
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 42
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget v6, v1, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    mul-float v6, v6, v2

    add-float/2addr v6, v3

    sub-float/2addr v4, v6

    .line 43
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v6

    int-to-float v6, v6

    iget v7, v1, Lcom/pspdfkit/utils/EdgeInsets;->bottom:F

    mul-float v7, v7, v2

    add-float/2addr v7, v3

    sub-float/2addr v6, v7

    .line 44
    invoke-virtual {v0, v4, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 47
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->n:Ljava/util/ArrayList;

    .line 48
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v4, v1, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    mul-float v4, v4, v2

    add-float/2addr v4, v3

    .line 49
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    iget v1, v1, Lcom/pspdfkit/utils/EdgeInsets;->bottom:F

    mul-float v1, v1, v2

    add-float/2addr v1, v3

    sub-float/2addr v5, v1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 52
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->l:Lcom/pspdfkit/internal/pn;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/d4;->b(Ljava/util/List;)V

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->l:Lcom/pspdfkit/internal/pn;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d4;->v()V

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->l:Lcom/pspdfkit/internal/pn;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->o:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->p:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1, v2}, Lcom/pspdfkit/internal/h4;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    .line 56
    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->i:Lcom/pspdfkit/annotations/BlendMode;

    sget-object v2, Lcom/pspdfkit/annotations/BlendMode;->NORMAL:Lcom/pspdfkit/annotations/BlendMode;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->k:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->k:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    int-to-float v4, v2

    iget v2, v1, Landroid/graphics/Rect;->top:I

    int-to-float v5, v2

    iget v2, v1, Landroid/graphics/Rect;->right:I

    int-to-float v6, v2

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v1

    iget-object v8, p0, Lcom/pspdfkit/internal/views/annotations/m;->j:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;)I

    .line 11
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 12
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return-void
.end method

.method public final e()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/d;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/e;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/m;->e:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/pspdfkit/internal/views/annotations/m;->f:F

    invoke-virtual {v1, v3, v2}, Lcom/pspdfkit/internal/views/annotations/e;->a(FLandroid/graphics/Matrix;)V

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/m;->h()V

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    .line 9
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_1
    return v0
.end method

.method public final f()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/d;->m()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/e;->f()V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/views/annotations/e;->a(FLandroid/graphics/Matrix;)V

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/m;->h()V

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    .line 9
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final g()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/d;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public bridge synthetic getAnnotation()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/m;->getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object v0

    return-object v0
.end method

.method public getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/e;->getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getApproximateMemoryUsage()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getContentScaler()Lcom/pspdfkit/internal/k0;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic getPageRect()Lcom/pspdfkit/utils/PageRect;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$getPageRect(Lcom/pspdfkit/internal/views/annotations/a;)Lcom/pspdfkit/utils/PageRect;

    move-result-object v0

    return-object v0
.end method

.method public getPaintForFontScalingCalculation()Landroid/graphics/Paint;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    return-object v0
.end method

.method public getTextForScaling()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/m;->getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getContents()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v2, 0x1

    :goto_2
    if-nez v2, :cond_3

    move-object v1, v0

    :cond_3
    return-object v1
.end method

.method public synthetic i()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$i(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/e;->k()Z

    move-result v0

    return v0
.end method

.method public synthetic o()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$o(Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/d;->m()Z

    move-result p1

    if-nez p1, :cond_1

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/m;->getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x1

    .line 5
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/views/annotations/m;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Z)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    .line 6
    iget p2, p1, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int p2, p2

    const/high16 p3, 0x40000000    # 2.0f

    invoke-static {p2, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 7
    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    float-to-int p1, p1

    invoke-static {p1, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 8
    iget-object p3, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {p3, p2, p1}, Landroid/view/View;->measure(II)V

    goto :goto_0

    .line 9
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/m;->d()V

    .line 12
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/m;->getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object p1

    if-nez p1, :cond_2

    return-void

    .line 16
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getTextInsets()Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object p2

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p3

    invoke-interface {p3}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result p3

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result p4

    .line 17
    invoke-static {p2, p3, p4}, Lcom/pspdfkit/internal/ia;->a(Lcom/pspdfkit/utils/EdgeInsets;II)Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object p2

    .line 20
    iget p3, p2, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    iget p4, p2, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    sub-float/2addr p3, p4

    .line 21
    iget p4, p2, Lcom/pspdfkit/utils/EdgeInsets;->top:F

    iget p2, p2, Lcom/pspdfkit/utils/EdgeInsets;->bottom:F

    sub-float/2addr p4, p2

    .line 25
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result p2

    const/16 p5, 0x5a

    if-eq p2, p5, :cond_3

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result p2

    const/16 p5, 0x10e

    if-ne p2, p5, :cond_4

    :cond_3
    const/high16 p2, -0x40800000    # -1.0f

    mul-float p3, p3, p2

    mul-float p4, p4, p2

    .line 31
    :cond_4
    iget-object p2, p0, Lcom/pspdfkit/internal/views/annotations/m;->e:Landroid/graphics/Matrix;

    const/high16 p5, 0x3f800000    # 1.0f

    .line 32
    invoke-static {p5, p2}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result p2

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/annotations/m;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;)F

    move-result p1

    mul-float p2, p2, p1

    const/high16 p1, 0x3f000000    # 0.5f

    mul-float p2, p2, p1

    .line 34
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p1

    iget-object p5, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {p5}, Landroid/view/View;->getMeasuredWidth()I

    move-result p5

    sub-int/2addr p1, p5

    div-int/lit8 p1, p1, 0x2

    int-to-float p1, p1

    mul-float p3, p3, p2

    add-float/2addr p3, p1

    float-to-int p1, p3

    .line 35
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p3

    iget-object p5, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {p5}, Landroid/view/View;->getMeasuredHeight()I

    move-result p5

    sub-int/2addr p3, p5

    div-int/lit8 p3, p3, 0x2

    int-to-float p3, p3

    mul-float p4, p4, p2

    add-float/2addr p4, p3

    float-to-int p2, p4

    .line 37
    iget-object p3, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    .line 40
    invoke-virtual {p3}, Landroid/view/View;->getMeasuredWidth()I

    move-result p4

    add-int/2addr p4, p1

    iget-object p5, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    .line 41
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredHeight()I

    move-result p5

    add-int/2addr p5, p2

    .line 42
    invoke-virtual {p3, p1, p2, p4, p5}, Landroid/view/View;->layout(IIII)V

    .line 47
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/m;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p3

    const/4 p4, 0x0

    invoke-virtual {p1, p4, p4, p2, p3}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method protected final onMeasure(II)V
    .locals 2

    .line 1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/d;->m()Z

    move-result p1

    const/high16 p2, 0x40000000    # 2.0f

    if-eqz p1, :cond_0

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/m;->d()V

    goto :goto_0

    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/m;->getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    .line 8
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/views/annotations/m;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Z)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    .line 9
    iget v0, p1, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v0, v0

    invoke-static {v0, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 10
    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    float-to-int p1, p1

    invoke-static {p1, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v1, v0, p1}, Landroid/view/View;->measure(II)V

    .line 12
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/m;->d:Lcom/pspdfkit/internal/views/annotations/n;

    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 14
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 15
    invoke-virtual {p1, v0, p2}, Landroid/view/View;->measure(II)V

    return-void
.end method

.method public final p()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/e;->p()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/n;->p()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final recycle()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/e;->recycle()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/n;->recycle()V

    return-void
.end method

.method public bridge synthetic setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/annotations/m;->setAnnotation(Lcom/pspdfkit/annotations/FreeTextAnnotation;)V

    return-void
.end method

.method public setAnnotation(Lcom/pspdfkit/annotations/FreeTextAnnotation;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/annotations/e;->setAnnotation(Lcom/pspdfkit/annotations/FreeTextAnnotation;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/m;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/annotations/n;->setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/m;->c:Lcom/pspdfkit/internal/views/annotations/e;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/m;->h()V

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/m;->j()V

    return-void
.end method

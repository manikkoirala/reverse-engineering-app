.class public final Lcom/pspdfkit/internal/views/annotations/k;
.super Lcom/pspdfkit/internal/views/annotations/j;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/views/annotations/j;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)V

    const/4 p1, 0x1

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/annotations/j;->setRefreshBoundingBoxAfterRendering(Z)V

    return-void
.end method


# virtual methods
.method protected final s()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/j;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->HIGHLIGHT:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_4

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    .line 7
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/j;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v3}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->ADD:Landroid/graphics/PorterDuff$Mode;

    goto :goto_0

    :cond_1
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    :goto_0
    invoke-direct {v1, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 8
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/j;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v3}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 9
    new-instance v2, Landroid/graphics/ColorMatrixColorFilter;

    invoke-static {}, Lcom/pspdfkit/internal/ga;->a()Landroid/graphics/ColorMatrix;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 10
    :cond_2
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/views/annotations/j$c;->a(Landroid/graphics/PorterDuffXfermode;Landroid/graphics/ColorMatrixColorFilter;)V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/j;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v1

    if-eqz v1, :cond_3

    const/high16 v1, -0x1000000

    goto :goto_1

    :cond_3
    const/4 v1, -0x1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_2

    .line 19
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/j$c;->a()V

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/j;->s:Lcom/pspdfkit/internal/views/annotations/j$c;

    invoke-virtual {v0, v2}, Landroidx/appcompat/widget/AppCompatImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    return-void
.end method

.method public setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/j;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/j;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/Annotation;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/views/annotations/j;->setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/k;->s()V

    return-void
.end method

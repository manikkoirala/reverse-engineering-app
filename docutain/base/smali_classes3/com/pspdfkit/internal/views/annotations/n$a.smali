.class final Lcom/pspdfkit/internal/views/annotations/n$a;
.super Lcom/pspdfkit/internal/yr;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/views/annotations/n;->h()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/views/annotations/n;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/views/annotations/n;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/n$a;->a:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-direct {p0}, Lcom/pspdfkit/internal/yr;-><init>()V

    return-void
.end method


# virtual methods
.method public final onComplete()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n$a;->a:Lcom/pspdfkit/internal/views/annotations/n;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/views/annotations/n;->-$$Nest$fgetc(Lcom/pspdfkit/internal/views/annotations/n;)Lcom/pspdfkit/internal/hr;

    move-result-object v1

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/internal/hr;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/pspdfkit/internal/views/annotations/n;->-$$Nest$fgetc(Lcom/pspdfkit/internal/views/annotations/n;)Lcom/pspdfkit/internal/hr;

    move-result-object v1

    .line 4
    invoke-virtual {v1}, Lcom/pspdfkit/internal/hr;->c()Landroid/graphics/Rect;

    move-result-object v1

    invoke-static {v0}, Lcom/pspdfkit/internal/views/annotations/n;->-$$Nest$fgetj(Lcom/pspdfkit/internal/views/annotations/n;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n$a;->a:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-static {v0}, Lcom/pspdfkit/internal/views/annotations/n;->-$$Nest$fgetu(Lcom/pspdfkit/internal/views/annotations/n;)Lcom/pspdfkit/internal/views/annotations/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/i;->b()V

    goto :goto_1

    .line 7
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n$a;->a:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-static {v0}, Lcom/pspdfkit/internal/views/annotations/n;->-$$Nest$mj(Lcom/pspdfkit/internal/views/annotations/n;)V

    .line 9
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n$a;->a:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    return-void
.end method

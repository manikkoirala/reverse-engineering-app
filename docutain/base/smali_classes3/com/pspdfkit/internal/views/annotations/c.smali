.class public final Lcom/pspdfkit/internal/views/annotations/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/annotations/c$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/pspdfkit/internal/views/annotations/a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/pspdfkit/internal/views/annotations/c$a;


# direct methods
.method public static synthetic $r8$lambda$1K3qwJbELRRWfyR1hRI-muij1z4(Lcom/pspdfkit/internal/views/annotations/c;Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/annotations/c;->b(Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 9
    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/c;->a:Ljava/util/Set;

    return-void
.end method

.method private synthetic b(Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/c;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/c;->b:Lcom/pspdfkit/internal/views/annotations/c$a;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/c;->a:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/c;->b:Lcom/pspdfkit/internal/views/annotations/c$a;

    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/c$a;->a()V

    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/c;->b:Lcom/pspdfkit/internal/views/annotations/c$a;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">(",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/c;->b:Lcom/pspdfkit/internal/views/annotations/c$a;

    if-nez v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/c;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/c$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/views/annotations/c$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/annotations/c;)V

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/views/annotations/a;->a(Lcom/pspdfkit/internal/views/annotations/a$a;)V

    return-void

    .line 6
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Calling add() not allowed after callOnReady()."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Lcom/pspdfkit/internal/views/annotations/c$a;)V
    .locals 0

    .line 7
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/c;->b:Lcom/pspdfkit/internal/views/annotations/c$a;

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/c;->a:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/c;->b:Lcom/pspdfkit/internal/views/annotations/c$a;

    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/c$a;->a()V

    const/4 p1, 0x0

    .line 10
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/c;->b:Lcom/pspdfkit/internal/views/annotations/c$a;

    :cond_0
    return-void
.end method

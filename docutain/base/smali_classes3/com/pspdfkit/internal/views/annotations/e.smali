.class public final Lcom/pspdfkit/internal/views/annotations/e;
.super Lcom/pspdfkit/internal/views/annotations/d;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/views/annotations/a;
.implements Lcom/pspdfkit/internal/el;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/annotations/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/views/annotations/d;",
        "Lcom/pspdfkit/internal/views/annotations/a<",
        "Lcom/pspdfkit/annotations/FreeTextAnnotation;",
        ">;",
        "Lcom/pspdfkit/internal/el;"
    }
.end annotation


# static fields
.field static final synthetic y:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final j:Lcom/pspdfkit/document/PdfDocument;

.field private final k:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private final l:Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

.field private m:Lcom/pspdfkit/internal/fl;

.field private final n:Lcom/pspdfkit/internal/views/annotations/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/views/annotations/i<",
            "Lcom/pspdfkit/annotations/FreeTextAnnotation;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

.field private p:Z

.field private q:Z

.field private r:Lcom/pspdfkit/internal/o1;

.field private s:Lio/reactivex/rxjava3/disposables/Disposable;

.field private t:Ljava/lang/Runnable;

.field private u:Z

.field private final v:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

.field private final w:Lcom/pspdfkit/internal/views/annotations/e$b;

.field private final x:Lcom/pspdfkit/internal/views/annotations/e$c;


# direct methods
.method public static synthetic $r8$lambda$jTDOEDR0CTI36OwdajFiDpRd_18(Lcom/pspdfkit/internal/views/annotations/e;Ljava/lang/Object;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/views/annotations/e;->a(Lcom/pspdfkit/internal/views/annotations/e;Ljava/lang/Object;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/pspdfkit/internal/views/annotations/e;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    .line 1
    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-string v3, "applyAnnotationAlpha"

    const-string v4, "getApplyAnnotationAlpha()Z"

    const/4 v5, 0x0

    invoke-direct {v2, v0, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    aput-object v2, v1, v5

    .line 11
    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-string v3, "drawBackground"

    const-string v4, "getDrawBackground()Z"

    invoke-direct {v2, v0, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v0

    const/4 v2, 0x1

    aput-object v0, v1, v2

    sput-object v1, Lcom/pspdfkit/internal/views/annotations/e;->y:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "document"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationConfigurationRegistry"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/annotations/d;-><init>(Landroid/content/Context;)V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/views/annotations/e;->j:Lcom/pspdfkit/document/PdfDocument;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/views/annotations/e;->k:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 6
    iput-object p4, p0, Lcom/pspdfkit/internal/views/annotations/e;->l:Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    .line 13
    new-instance p1, Lcom/pspdfkit/internal/views/annotations/i;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/views/annotations/i;-><init>(Lcom/pspdfkit/internal/views/annotations/a;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/e;->n:Lcom/pspdfkit/internal/views/annotations/i;

    .line 35
    new-instance p1, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/e;->v:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    const/4 p1, 0x0

    .line 38
    invoke-virtual {p0, p1}, Landroid/view/View;->setWillNotDraw(Z)V

    .line 39
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 40
    new-instance p2, Lcom/pspdfkit/internal/views/annotations/e$b;

    invoke-direct {p2, p0, p1}, Lcom/pspdfkit/internal/views/annotations/e$b;-><init>(Lcom/pspdfkit/internal/views/annotations/e;Ljava/lang/Boolean;)V

    .line 41
    iput-object p2, p0, Lcom/pspdfkit/internal/views/annotations/e;->w:Lcom/pspdfkit/internal/views/annotations/e$b;

    .line 42
    new-instance p2, Lcom/pspdfkit/internal/views/annotations/e$c;

    invoke-direct {p2, p0, p1}, Lcom/pspdfkit/internal/views/annotations/e$c;-><init>(Lcom/pspdfkit/internal/views/annotations/e;Ljava/lang/Boolean;)V

    .line 43
    iput-object p2, p0, Lcom/pspdfkit/internal/views/annotations/e;->x:Lcom/pspdfkit/internal/views/annotations/e$c;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/views/annotations/e;)Lcom/pspdfkit/annotations/FreeTextAnnotation;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    return-object p0
.end method

.method private static final a(Lcom/pspdfkit/internal/views/annotations/e;Ljava/lang/Object;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 69
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->t:Ljava/lang/Runnable;

    .line 70
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/annotations/e;->a(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Landroid/widget/EditText;->setSelection(I)V

    return-void
.end method

.method private final a(Ljava/lang/String;)V
    .locals 13

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->m()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 9
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/annotations/e;->u:Z

    .line 10
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 13
    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-eqz v0, :cond_8

    if-eqz p1, :cond_8

    .line 14
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-lez v2, :cond_8

    .line 15
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/e;->getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getLineHeightFactor()Ljava/lang/Float;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_1
    invoke-virtual {v0}, Landroid/text/Layout;->getSpacingMultiplier()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    :cond_2
    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v8

    .line 17
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 18
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x1c

    const/4 v11, 0x1

    if-lt v3, v4, :cond_3

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_4

    .line 19
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-virtual {v0}, Landroid/text/Layout;->getWidth()I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/text/DynamicLayout$Builder;->obtain(Ljava/lang/CharSequence;Landroid/text/TextPaint;I)Landroid/text/DynamicLayout$Builder;

    move-result-object v3

    .line 20
    invoke-virtual {v0}, Landroid/text/Layout;->getAlignment()Landroid/text/Layout$Alignment;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/text/DynamicLayout$Builder;->setAlignment(Landroid/text/Layout$Alignment;)Landroid/text/DynamicLayout$Builder;

    move-result-object v0

    const/4 v3, 0x0

    .line 21
    invoke-virtual {v0, v3, v8}, Landroid/text/DynamicLayout$Builder;->setLineSpacing(FF)Landroid/text/DynamicLayout$Builder;

    move-result-object v0

    .line 22
    invoke-virtual {v0, v1}, Landroid/text/DynamicLayout$Builder;->setIncludePad(Z)Landroid/text/DynamicLayout$Builder;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Landroid/text/DynamicLayout$Builder;->build()Landroid/text/DynamicLayout;

    move-result-object v0

    const-string v3, "{\n                    Dy\u2026build()\n                }"

    .line 24
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 32
    :cond_4
    new-instance v12, Landroid/text/DynamicLayout;

    .line 34
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    .line 35
    invoke-virtual {v0}, Landroid/text/Layout;->getWidth()I

    move-result v6

    .line 36
    invoke-virtual {v0}, Landroid/text/Layout;->getAlignment()Landroid/text/Layout$Alignment;

    move-result-object v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v3, v12

    move-object v4, v2

    .line 37
    invoke-direct/range {v3 .. v10}, Landroid/text/DynamicLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object v0, v12

    :goto_1
    const/4 v3, 0x0

    .line 49
    :goto_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_5

    const/4 v4, 0x1

    goto :goto_3

    :cond_5
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_7

    .line 51
    invoke-virtual {v0}, Landroid/text/DynamicLayout;->getLineCount()I

    move-result v4

    if-eq v4, v11, :cond_7

    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    if-ge v4, v5, :cond_6

    goto :goto_4

    .line 56
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v3, v11

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 60
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v2, v1, v3, p1}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/4 v3, 0x1

    goto :goto_2

    .line 64
    :cond_7
    :goto_4
    iput-boolean v3, p0, Lcom/pspdfkit/internal/views/annotations/e;->u:Z

    .line 65
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 67
    :cond_8
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/annotations/e;->u:Z

    .line 68
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/views/annotations/e;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->s:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public static final c(Lcom/pspdfkit/internal/views/annotations/e;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->r:Lcom/pspdfkit/internal/o1;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/o1;->b()V

    :cond_0
    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->r:Lcom/pspdfkit/internal/o1;

    return-void
.end method

.method public static final d(Lcom/pspdfkit/internal/views/annotations/e;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/e;->getDrawBackground()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/e;->getAnnotationBackgroundColor()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 5
    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final a(FLandroid/graphics/Matrix;)V
    .locals 1

    const-string v0, "pdfToViewMatrix"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/internal/views/annotations/d;->a(FLandroid/graphics/Matrix;)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/e;->b()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/views/annotations/a$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/annotations/a$a<",
            "Lcom/pspdfkit/annotations/FreeTextAnnotation;",
            ">;)V"
        }
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->n:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/annotations/i;->a(Lcom/pspdfkit/internal/views/annotations/a$a;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz p1, :cond_0

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/e;->n:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/i;->b()V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 11

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-nez v0, :cond_0

    goto/16 :goto_8

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/e;->k:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result v2

    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/e;->k:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v3}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/ga;->a(IZZ)I

    move-result v1

    .line 4
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/e;->getApplyAnnotationAlpha()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result v1

    invoke-virtual {p0, v1}, Landroidx/appcompat/widget/AppCompatEditText;->setAlpha(F)V

    .line 10
    :cond_1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object v1

    const-string v2, "getSystemFontManager()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/nt;->a(Lcom/pspdfkit/internal/mt;Lcom/pspdfkit/annotations/FreeTextAnnotation;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v1

    .line 11
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v1

    .line 12
    new-instance v2, Lcom/pspdfkit/internal/views/annotations/g;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/views/annotations/g;-><init>(Lcom/pspdfkit/internal/views/annotations/e;)V

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v1

    const-string v2, "private fun updateEditTe\u2026hangeListener(this)\n    }"

    .line 13
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/e;->v:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    const-string v3, "<this>"

    .line 19
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "compositeDisposable"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {v2, v1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    .line 57
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/e;->getDrawBackground()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/e;->getAnnotationBackgroundColor()I

    move-result v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 60
    :goto_0
    invoke-virtual {p0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 61
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getTextJustification()Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextTextJustification;

    move-result-object v1

    const-string v3, "boundAnnotation.textJustification"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    sget-object v3, Lcom/pspdfkit/internal/views/annotations/e$a;->a:[I

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v3, v1

    const/4 v3, 0x3

    const/4 v4, 0x1

    if-eq v1, v4, :cond_5

    const/4 v5, 0x2

    if-eq v1, v5, :cond_4

    if-ne v1, v3, :cond_3

    const/4 v3, 0x5

    goto :goto_1

    .line 65
    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :cond_4
    const/4 v3, 0x1

    .line 66
    :cond_5
    :goto_1
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getVerticalTextAlignment()Lcom/pspdfkit/annotations/VerticalTextAlignment;

    move-result-object v1

    const-string v5, "boundAnnotation.verticalTextAlignment"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/pspdfkit/internal/a4;->a(Lcom/pspdfkit/annotations/VerticalTextAlignment;)I

    move-result v1

    or-int/2addr v1, v3

    .line 67
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 69
    invoke-static {v0}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;)F

    move-result v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->getPdfToViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v1

    float-to-double v5, v1

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-int v1, v5

    .line 70
    invoke-virtual {p0, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 71
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-nez v1, :cond_6

    const/4 v1, 0x0

    goto/16 :goto_6

    .line 72
    :cond_6
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getTextSize()F

    move-result v3

    .line 73
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v5

    if-nez v5, :cond_7

    move v1, v3

    goto/16 :goto_6

    .line 79
    :cond_7
    iget-boolean v5, p0, Lcom/pspdfkit/internal/views/annotations/e;->p:Z

    if-eqz v5, :cond_8

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v5

    invoke-interface {v5}, Lcom/pspdfkit/internal/pf;->getTextShouldFit()Z

    move-result v5

    if-eqz v5, :cond_8

    const/4 v5, 0x1

    goto :goto_2

    :cond_8
    const/4 v5, 0x0

    .line 82
    :goto_2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/e;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v6

    .line 83
    iget v7, v6, Landroid/graphics/RectF;->right:F

    iget v8, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v7, v8

    .line 84
    iget v8, v6, Landroid/graphics/RectF;->top:F

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v8, v6

    .line 87
    new-instance v6, Landroid/text/TextPaint;

    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-direct {v6, v9}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    const/high16 v9, 0x3f800000    # 1.0f

    if-eqz v5, :cond_b

    .line 88
    :cond_9
    invoke-virtual {v6, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 89
    invoke-static {v1, v7, v6}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;FLandroid/text/TextPaint;)Lcom/pspdfkit/utils/Size;

    move-result-object v5

    .line 90
    iget v10, v5, Lcom/pspdfkit/utils/Size;->width:F

    cmpg-float v10, v10, v7

    if-gtz v10, :cond_a

    iget v5, v5, Lcom/pspdfkit/utils/Size;->height:F

    cmpg-float v5, v5, v8

    if-gtz v5, :cond_a

    const/4 v5, 0x1

    goto :goto_3

    :cond_a
    const/4 v5, 0x0

    :goto_3
    if-nez v5, :cond_d

    sub-float/2addr v3, v9

    cmpl-float v5, v3, v9

    if-gtz v5, :cond_9

    goto :goto_5

    .line 91
    :cond_b
    invoke-virtual {v6, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 92
    invoke-static {v1, v7, v6}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;FLandroid/text/TextPaint;)Lcom/pspdfkit/utils/Size;

    move-result-object v5

    .line 93
    iget v6, v5, Lcom/pspdfkit/utils/Size;->width:F

    cmpg-float v6, v6, v7

    if-gtz v6, :cond_c

    iget v5, v5, Lcom/pspdfkit/utils/Size;->height:F

    cmpg-float v5, v5, v8

    if-gtz v5, :cond_c

    const/4 v5, 0x1

    goto :goto_4

    :cond_c
    const/4 v5, 0x0

    :goto_4
    if-eqz v5, :cond_d

    .line 94
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1, v4}, Lcom/pspdfkit/internal/pf;->setTextShouldFit(Z)V

    .line 97
    :cond_d
    :goto_5
    invoke-static {v3, v9}, Lkotlin/ranges/RangesKt;->coerceAtLeast(FF)F

    move-result v1

    .line 98
    :goto_6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->getPdfToViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v1

    .line 99
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->m()Z

    move-result v3

    if-nez v3, :cond_e

    float-to-double v3, v1

    .line 104
    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-float v1, v3

    goto :goto_7

    :cond_e
    const v3, 0x3f7851ec    # 0.97f

    mul-float v1, v1, v3

    .line 110
    :goto_7
    invoke-virtual {p0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 111
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/pf;->addOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    :goto_8
    return-void
.end method

.method public final b(Z)Z
    .locals 0

    return p1
.end method

.method public final c()V
    .locals 1

    .line 4
    invoke-super {p0}, Lcom/pspdfkit/internal/views/annotations/d;->c()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-nez v0, :cond_0

    return-void

    .line 8
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getContents()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/annotations/e;->a(Ljava/lang/String;)V

    .line 10
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setSelection(I)V

    return-void
.end method

.method public final e()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/e;->c()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/e;->l()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->r:Lcom/pspdfkit/internal/o1;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/o1;->b()V

    :cond_0
    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->r:Lcom/pspdfkit/internal/o1;

    return-void
.end method

.method public synthetic g()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$g(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getAnnotation()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/e;->getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object v0

    return-object v0
.end method

.method public getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    return-object v0
.end method

.method public final getAnnotationBackgroundColor()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getFillColor()I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/e;->k:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/e;->k:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ga;->a(IZZ)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getApplyAnnotationAlpha()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->w:Lcom/pspdfkit/internal/views/annotations/e$b;

    sget-object v1, Lcom/pspdfkit/internal/views/annotations/e;->y:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/properties/ObservableProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getApproximateMemoryUsage()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected getBoundingBox()Landroid/graphics/RectF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    :cond_1
    return-object v0
.end method

.method public bridge synthetic getContentScaler()Lcom/pspdfkit/internal/k0;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$getContentScaler(Lcom/pspdfkit/internal/views/annotations/a;)Lcom/pspdfkit/internal/k0;

    move-result-object v0

    return-object v0
.end method

.method public final getDrawBackground()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->x:Lcom/pspdfkit/internal/views/annotations/e$c;

    sget-object v1, Lcom/pspdfkit/internal/views/annotations/e;->y:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/properties/ObservableProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final getOnEditRecordedListener()Lcom/pspdfkit/internal/fl;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->m:Lcom/pspdfkit/internal/fl;

    return-object v0
.end method

.method public bridge synthetic getPageRect()Lcom/pspdfkit/utils/PageRect;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$getPageRect(Lcom/pspdfkit/internal/views/annotations/a;)Lcom/pspdfkit/utils/PageRect;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$i(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/e;->l()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 6
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    .line 9
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/e;->t:Ljava/lang/Runnable;

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    .line 10
    :goto_1
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getContents()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    if-nez v3, :cond_2

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->m()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 14
    invoke-virtual {v0, v2}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setContents(Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 17
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    const-string v3, "null cannot be cast to non-null type com.pspdfkit.ui.overlay.OverlayLayoutParams"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    .line 18
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v3

    iget-object v5, v2, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v5}, Lcom/pspdfkit/utils/PageRect;->getPageRect()Landroid/graphics/RectF;

    move-result-object v5

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 19
    iget-object v1, v2, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v1}, Lcom/pspdfkit/utils/PageRect;->getPageRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    const/4 v1, 0x1

    .line 22
    :cond_3
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    :cond_4
    return v1
.end method

.method public final l()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/views/annotations/d;->l()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getContents()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/annotations/e;->a(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic o()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$o(Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method public final declared-synchronized onAnnotationPropertyChange(Lcom/pspdfkit/annotations/Annotation;ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->q:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    .line 3
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    monitor-exit p0

    return-void

    .line 4
    :cond_1
    :try_start_2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez p1, :cond_2

    monitor-exit p0

    return-void

    :cond_2
    const/4 p1, 0x3

    if-eq p2, p1, :cond_5

    const/16 p1, 0x9

    if-eq p2, p1, :cond_3

    goto :goto_0

    :cond_3
    if-eqz p3, :cond_8

    if-eqz p4, :cond_8

    .line 29
    :try_start_3
    check-cast p3, Landroid/graphics/RectF;

    .line 30
    check-cast p4, Landroid/graphics/RectF;

    .line 31
    invoke-virtual {p4}, Landroid/graphics/RectF;->width()F

    move-result p1

    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result p2

    cmpg-float p1, p1, p2

    if-ltz p1, :cond_4

    invoke-virtual {p4}, Landroid/graphics/RectF;->height()F

    move-result p1

    neg-float p1, p1

    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result p2

    neg-float p2, p2

    cmpg-float p1, p1, p2

    if-gez p1, :cond_8

    .line 32
    :cond_4
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->clearTextShouldFit()V

    goto :goto_0

    :cond_5
    if-eqz p4, :cond_8

    .line 33
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_8

    .line 34
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/e;->r:Lcom/pspdfkit/internal/o1;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/pspdfkit/internal/o1;->b()V

    :cond_6
    const/4 p1, 0x0

    .line 35
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/e;->r:Lcom/pspdfkit/internal/o1;

    .line 36
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/e;->t:Ljava/lang/Runnable;

    if-eqz p1, :cond_7

    .line 37
    invoke-virtual {p0, p1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 39
    :cond_7
    new-instance p1, Lcom/pspdfkit/internal/views/annotations/e$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0, p4}, Lcom/pspdfkit/internal/views/annotations/e$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/annotations/e;Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/e;->t:Ljava/lang/Runnable;

    .line 45
    invoke-virtual {p0, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_8
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 1

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->m()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/annotations/d;->setKeyboardVisible(Z)V

    goto :goto_0

    .line 4
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/internal/views/annotations/d;->onFocusChange(Landroid/view/View;Z)V

    :goto_0
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;->onMeasure(II)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-nez p1, :cond_0

    return-void

    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->m()Z

    move-result p2

    if-nez p2, :cond_1

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getContents()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/annotations/e;->a(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/views/annotations/d;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-nez p2, :cond_0

    return-void

    .line 3
    :cond_0
    iget-boolean p3, p0, Lcom/pspdfkit/internal/views/annotations/e;->u:Z

    if-nez p3, :cond_3

    const/4 p3, 0x1

    .line 5
    iput-boolean p3, p0, Lcom/pspdfkit/internal/views/annotations/e;->q:Z

    .line 6
    iget-object p3, p0, Lcom/pspdfkit/internal/views/annotations/e;->m:Lcom/pspdfkit/internal/fl;

    .line 7
    iget-object p4, p0, Lcom/pspdfkit/internal/views/annotations/e;->r:Lcom/pspdfkit/internal/o1;

    if-nez p4, :cond_1

    if-eqz p3, :cond_1

    .line 9
    invoke-static {p2, p3}, Lcom/pspdfkit/internal/o1;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/internal/o1;

    move-result-object p3

    .line 10
    iput-object p3, p0, Lcom/pspdfkit/internal/views/annotations/e;->r:Lcom/pspdfkit/internal/o1;

    .line 11
    invoke-virtual {p3}, Lcom/pspdfkit/internal/o1;->a()V

    .line 13
    :cond_1
    iget-object p3, p0, Lcom/pspdfkit/internal/views/annotations/e;->s:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {p3}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 14
    sget-object p3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v0, 0x12c

    invoke-static {v0, v1, p3}, Lio/reactivex/rxjava3/core/Observable;->timer(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p3

    .line 15
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p4

    invoke-virtual {p3, p4}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p3

    .line 16
    new-instance p4, Lcom/pspdfkit/internal/views/annotations/f;

    invoke-direct {p4, p0}, Lcom/pspdfkit/internal/views/annotations/f;-><init>(Lcom/pspdfkit/internal/views/annotations/e;)V

    invoke-virtual {p3, p4}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p3

    .line 17
    iput-object p3, p0, Lcom/pspdfkit/internal/views/annotations/e;->s:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 18
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getContents()Ljava/lang/String;

    move-result-object p4

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_2

    .line 19
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setContents(Ljava/lang/String;)V

    .line 20
    new-instance p1, Landroid/text/TextPaint;

    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object p3

    invoke-direct {p1, p3}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 21
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    .line 23
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getTextSize()F

    move-result p3

    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 24
    iget-object p3, p0, Lcom/pspdfkit/internal/views/annotations/e;->l:Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    iget-object p4, p0, Lcom/pspdfkit/internal/views/annotations/e;->j:Lcom/pspdfkit/document/PdfDocument;

    invoke-virtual {p2}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-interface {p4, v0}, Lcom/pspdfkit/document/PdfDocument;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p4

    const-string v0, "document.getPageSize(boundAnnotation.pageIndex)"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p3, p4, p1}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;Lcom/pspdfkit/utils/Size;Landroid/text/TextPaint;)V

    :cond_2
    const/4 p1, 0x0

    .line 26
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/annotations/e;->q:Z

    :cond_3
    return-void
.end method

.method public final p()V
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/internal/views/annotations/b;->b:I

    .line 2
    invoke-interface {p0}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/pspdfkit/internal/views/annotations/b;->a(Lcom/pspdfkit/internal/views/annotations/a;Z)Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final recycle()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/views/annotations/d;->recycle()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    :cond_0
    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/e;->v:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    const/4 v1, 0x0

    .line 5
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/annotations/e;->q:Z

    .line 6
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->m:Lcom/pspdfkit/internal/fl;

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/e;->r:Lcom/pspdfkit/internal/o1;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/o1;->b()V

    .line 8
    :cond_1
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->r:Lcom/pspdfkit/internal/o1;

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/e;->s:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->s:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->n:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/i;->a()V

    return-void
.end method

.method public bridge synthetic setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/annotations/e;->setAnnotation(Lcom/pspdfkit/annotations/FreeTextAnnotation;)V

    return-void
.end method

.method public setAnnotation(Lcom/pspdfkit/annotations/FreeTextAnnotation;)V
    .locals 2

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/e;->o:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/e;->v:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    if-eqz v0, :cond_1

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    .line 8
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/pf;->addOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getTextShouldFit()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->p:Z

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getContents()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/annotations/e;->a(Ljava/lang/String;)V

    .line 11
    new-instance v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p1

    sget-object v1, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;->LAYOUT:Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;

    invoke-direct {v0, p1, v1}, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;-><init>(Landroid/graphics/RectF;Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/e;->b()V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/e;->n:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/i;->b()V

    return-void
.end method

.method public final setApplyAnnotationAlpha(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->w:Lcom/pspdfkit/internal/views/annotations/e$b;

    sget-object v1, Lcom/pspdfkit/internal/views/annotations/e;->y:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p0, v1, p1}, Lkotlin/properties/ObservableProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setDrawBackground(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/e;->x:Lcom/pspdfkit/internal/views/annotations/e$c;

    sget-object v1, Lcom/pspdfkit/internal/views/annotations/e;->y:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p0, v1, p1}, Lkotlin/properties/ObservableProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setOnEditRecordedListener(Lcom/pspdfkit/internal/fl;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/e;->m:Lcom/pspdfkit/internal/fl;

    return-void
.end method

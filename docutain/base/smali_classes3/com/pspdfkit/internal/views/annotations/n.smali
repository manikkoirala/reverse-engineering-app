.class public final Lcom/pspdfkit/internal/views/annotations/n;
.super Landroid/view/View;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/h2;
.implements Lcom/pspdfkit/internal/mo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/view/View;",
        "Lcom/pspdfkit/internal/h2<",
        "Lcom/pspdfkit/annotations/Annotation;",
        ">;",
        "Lcom/pspdfkit/internal/mo;"
    }
.end annotation


# instance fields
.field private final b:Landroid/graphics/Matrix;

.field private final c:Lcom/pspdfkit/internal/hr;

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/Paint;

.field private final g:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private h:Lcom/pspdfkit/annotations/BlendMode;

.field private i:Lcom/pspdfkit/internal/views/document/DocumentView;

.field private final j:Landroid/graphics/Rect;

.field private final k:Landroid/graphics/Rect;

.field private final l:Landroid/graphics/Rect;

.field private final m:Landroid/graphics/RectF;

.field private n:F

.field private final o:Ljava/util/ArrayList;

.field private final p:Ljava/util/ArrayList;

.field private q:F

.field private r:F

.field private s:Z

.field private final t:Ljava/lang/Runnable;

.field private final u:Lcom/pspdfkit/internal/views/annotations/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/views/annotations/i<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private final v:Landroid/os/Handler;


# direct methods
.method public static synthetic $r8$lambda$wIzoGU-f7i8biJGsMjx1FzlobFM(Lcom/pspdfkit/internal/views/annotations/n;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/n;->h()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/views/annotations/n;)Lcom/pspdfkit/internal/hr;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/annotations/n;->c:Lcom/pspdfkit/internal/hr;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetj(Lcom/pspdfkit/internal/views/annotations/n;)Landroid/graphics/Rect;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/annotations/n;->j:Landroid/graphics/Rect;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetu(Lcom/pspdfkit/internal/views/annotations/n;)Lcom/pspdfkit/internal/views/annotations/i;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/annotations/n;->u:Lcom/pspdfkit/internal/views/annotations/i;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mj(Lcom/pspdfkit/internal/views/annotations/n;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/n;->j()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 2

    .line 86
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/pspdfkit/internal/views/annotations/n;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/views/document/DocumentView;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Lcom/pspdfkit/configuration/PdfConfiguration;",
            "Lcom/pspdfkit/internal/views/document/DocumentView;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/n;->b:Landroid/graphics/Matrix;

    .line 9
    invoke-static {}, Lcom/pspdfkit/internal/h4;->i()Landroid/graphics/Paint;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/n;->d:Landroid/graphics/Paint;

    .line 12
    invoke-static {}, Lcom/pspdfkit/internal/h4;->h()Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->e:Landroid/graphics/Paint;

    .line 14
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->f:Landroid/graphics/Paint;

    .line 20
    sget-object v1, Lcom/pspdfkit/annotations/BlendMode;->NORMAL:Lcom/pspdfkit/annotations/BlendMode;

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->h:Lcom/pspdfkit/annotations/BlendMode;

    .line 27
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->j:Landroid/graphics/Rect;

    .line 30
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->k:Landroid/graphics/Rect;

    .line 36
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->l:Landroid/graphics/Rect;

    .line 39
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->m:Landroid/graphics/RectF;

    const/4 v1, 0x0

    .line 42
    iput v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->n:F

    .line 44
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    .line 47
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->p:Ljava/util/ArrayList;

    .line 50
    iput v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->q:F

    .line 51
    iput v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->r:F

    const/4 v1, 0x0

    .line 56
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->s:Z

    .line 58
    new-instance v2, Lcom/pspdfkit/internal/views/annotations/n$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/views/annotations/n$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/annotations/n;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->t:Ljava/lang/Runnable;

    .line 61
    new-instance v2, Lcom/pspdfkit/internal/views/annotations/i;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/views/annotations/i;-><init>(Lcom/pspdfkit/internal/views/annotations/a;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->u:Lcom/pspdfkit/internal/views/annotations/i;

    .line 66
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->v:Landroid/os/Handler;

    .line 74
    iput-object p3, p0, Lcom/pspdfkit/internal/views/annotations/n;->g:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 75
    iput-object p4, p0, Lcom/pspdfkit/internal/views/annotations/n;->i:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 77
    invoke-virtual {p3}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result p4

    invoke-virtual {p3}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result p3

    invoke-static {p4, p3}, Lcom/pspdfkit/internal/ga;->a(ZZ)Landroid/graphics/ColorMatrixColorFilter;

    move-result-object p3

    .line 78
    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 79
    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 81
    new-instance p3, Lcom/pspdfkit/internal/hr;

    invoke-direct {p3, p1, v0}, Lcom/pspdfkit/internal/hr;-><init>(Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/views/annotations/n;->c:Lcom/pspdfkit/internal/hr;

    .line 83
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/views/annotations/n;->setAnnotations(Ljava/util/List;)V

    .line 85
    invoke-virtual {p0, v1}, Landroid/view/View;->setWillNotDraw(Z)V

    return-void
.end method

.method private d()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->p:Ljava/util/ArrayList;

    .line 4
    sget-object v3, Lcom/pspdfkit/internal/views/annotations/n$b;->a:[I

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 25
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Shape for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 26
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " annotation type is not implemented."

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :pswitch_0
    new-instance v3, Lcom/pspdfkit/internal/xr;

    sget-object v4, Lcom/pspdfkit/internal/ds$a;->a:Lcom/pspdfkit/internal/ds$a;

    invoke-direct {v3, v4}, Lcom/pspdfkit/internal/xr;-><init>(Lcom/pspdfkit/internal/ds$a;)V

    goto :goto_1

    .line 28
    :pswitch_1
    new-instance v3, Lcom/pspdfkit/internal/xr;

    sget-object v4, Lcom/pspdfkit/internal/ds$a;->b:Lcom/pspdfkit/internal/ds$a;

    invoke-direct {v3, v4}, Lcom/pspdfkit/internal/xr;-><init>(Lcom/pspdfkit/internal/ds$a;)V

    goto :goto_1

    .line 29
    :pswitch_2
    new-instance v3, Lcom/pspdfkit/internal/rn;

    invoke-direct {v3}, Lcom/pspdfkit/internal/rn;-><init>()V

    goto :goto_1

    .line 30
    :pswitch_3
    new-instance v3, Lcom/pspdfkit/internal/on;

    invoke-direct {v3}, Lcom/pspdfkit/internal/on;-><init>()V

    goto :goto_1

    .line 31
    :pswitch_4
    new-instance v3, Lcom/pspdfkit/internal/gh;

    invoke-direct {v3}, Lcom/pspdfkit/internal/gh;-><init>()V

    goto :goto_1

    .line 32
    :pswitch_5
    new-instance v3, Lcom/pspdfkit/internal/ge;

    invoke-direct {v3}, Lcom/pspdfkit/internal/ge;-><init>()V

    .line 57
    :goto_1
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 58
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->MEASUREMENT_TOOLS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 59
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getMeasurementProperties()Lcom/pspdfkit/internal/ri;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 61
    invoke-interface {v3, v1}, Lcom/pspdfkit/internal/b2;->a(Lcom/pspdfkit/internal/ri;)V

    .line 62
    :cond_0
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 65
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/n;->p()V

    .line 66
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/n;->b()V

    .line 68
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 69
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->u:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/i;->b()V

    :cond_2
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private h()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->c:Lcom/pspdfkit/internal/hr;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->j:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->p:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->b:Landroid/graphics/Matrix;

    iget v4, p0, Lcom/pspdfkit/internal/views/annotations/n;->n:F

    const-wide/16 v5, 0x0

    .line 2
    invoke-virtual/range {v0 .. v6}, Lcom/pspdfkit/internal/hr;->a(Landroid/graphics/Rect;Ljava/util/ArrayList;Landroid/graphics/Matrix;FJ)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/views/annotations/n$a;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/views/annotations/n$a;-><init>(Lcom/pspdfkit/internal/views/annotations/n;)V

    .line 3
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/core/CompletableObserver;)V

    return-void
.end method

.method private j()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->s:Z

    if-eqz v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->c:Lcom/pspdfkit/internal/hr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hr;->a()V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->v:Landroid/os/Handler;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->t:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->v:Landroid/os/Handler;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->t:Ljava/lang/Runnable;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private l()Z
    .locals 8

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->n:F

    const/4 v1, 0x0

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    return v1

    .line 3
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    if-nez v0, :cond_1

    return v1

    .line 5
    :cond_1
    iget-object v0, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getPageRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->m:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->b:Landroid/graphics/Matrix;

    .line 7
    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 8
    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->m:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->n:F

    div-float v4, v2, v3

    iput v4, p0, Lcom/pspdfkit/internal/views/annotations/n;->q:F

    .line 10
    iget v4, v0, Landroid/graphics/RectF;->top:F

    div-float v3, v4, v3

    iput v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->r:F

    .line 13
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->k:Landroid/graphics/Rect;

    iget v5, v3, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    add-float/2addr v5, v2

    iget v6, v3, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    add-float/2addr v6, v4

    iget v7, v3, Landroid/graphics/Rect;->right:I

    int-to-float v7, v7

    add-float/2addr v7, v2

    iget v2, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    add-float/2addr v2, v4

    invoke-virtual {v0, v5, v6, v7, v2}, Landroid/graphics/RectF;->intersect(FFFF)Z

    move-result v0

    if-nez v0, :cond_2

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->m:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->setEmpty()V

    .line 22
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->m:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->m:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    .line 23
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->m:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    .line 24
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->m:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    .line 25
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    if-eq v0, v2, :cond_3

    goto :goto_0

    :cond_3
    return v1

    .line 26
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->j:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->m:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    .line 27
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->m:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    .line 28
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->m:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    .line 29
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v4, p0, Lcom/pspdfkit/internal/views/annotations/n;->m:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    .line 30
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 31
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final a(FLandroid/graphics/Matrix;)V
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 13
    iput p1, p0, Lcom/pspdfkit/internal/views/annotations/n;->n:F

    .line 14
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/n;->b()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/views/annotations/a$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/annotations/a$a<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->u:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/annotations/i;->a(Lcom/pspdfkit/internal/views/annotations/a$a;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/n;->u:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/i;->b()V

    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/b2;

    .line 16
    invoke-interface {v2, p1}, Lcom/pspdfkit/internal/b2;->a(Z)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    .line 21
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/n;->c()V

    :cond_2
    return-void
.end method

.method public final varargs a([Lcom/pspdfkit/annotations/Annotation;)V
    .locals 4

    .line 1
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p1, v1

    .line 2
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 3
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/n;->d()V

    return-void
.end method

.method public final b()V
    .locals 7

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->k:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->n:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    goto/16 :goto_5

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/b2;

    .line 10
    iget v4, p0, Lcom/pspdfkit/internal/views/annotations/n;->n:F

    iget-object v5, p0, Lcom/pspdfkit/internal/views/annotations/n;->b:Landroid/graphics/Matrix;

    invoke-interface {v3, v4, v5}, Lcom/pspdfkit/internal/br;->a(FLandroid/graphics/Matrix;)Z

    move-result v3

    or-int/2addr v2, v3

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 14
    :goto_1
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 15
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->p:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/b2;

    iget-object v4, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    .line 16
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/Annotation;

    iget-object v5, p0, Lcom/pspdfkit/internal/views/annotations/n;->b:Landroid/graphics/Matrix;

    iget v6, p0, Lcom/pspdfkit/internal/views/annotations/n;->n:F

    invoke-interface {v3, v4, v5, v6}, Lcom/pspdfkit/internal/b2;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;F)Z

    move-result v3

    or-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 20
    :cond_2
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/n;->l()Z

    move-result v0

    or-int/2addr v0, v2

    .line 21
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    .line 22
    :cond_3
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getBlendMode()Lcom/pspdfkit/annotations/BlendMode;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->h:Lcom/pspdfkit/annotations/BlendMode;

    .line 23
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/Annotation;

    .line 24
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getBlendMode()Lcom/pspdfkit/annotations/BlendMode;

    move-result-object v3

    .line 25
    iget-object v4, p0, Lcom/pspdfkit/internal/views/annotations/n;->h:Lcom/pspdfkit/annotations/BlendMode;

    if-eq v4, v3, :cond_4

    .line 27
    sget-object v2, Lcom/pspdfkit/annotations/BlendMode;->NORMAL:Lcom/pspdfkit/annotations/BlendMode;

    iput-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->h:Lcom/pspdfkit/annotations/BlendMode;

    .line 32
    :cond_5
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->g:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 33
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->h:Lcom/pspdfkit/annotations/BlendMode;

    sget v3, Lcom/pspdfkit/internal/views/annotations/b;->b:I

    .line 34
    sget-object v3, Lcom/pspdfkit/annotations/BlendMode;->MULTIPLY:Lcom/pspdfkit/annotations/BlendMode;

    if-ne v2, v3, :cond_6

    .line 35
    sget-object v2, Lcom/pspdfkit/annotations/BlendMode;->SCREEN:Lcom/pspdfkit/annotations/BlendMode;

    goto :goto_2

    .line 36
    :cond_6
    sget-object v4, Lcom/pspdfkit/annotations/BlendMode;->SCREEN:Lcom/pspdfkit/annotations/BlendMode;

    if-ne v2, v4, :cond_7

    move-object v2, v3

    .line 37
    :cond_7
    :goto_2
    iput-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->h:Lcom/pspdfkit/annotations/BlendMode;

    .line 40
    :cond_8
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->f:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->h:Lcom/pspdfkit/annotations/BlendMode;

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/views/annotations/b;->a(Landroid/graphics/Paint;Lcom/pspdfkit/annotations/BlendMode;)Landroid/graphics/Paint;

    .line 41
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->h:Lcom/pspdfkit/annotations/BlendMode;

    .line 42
    sget-object v3, Lcom/pspdfkit/internal/views/annotations/b$a;->a:[I

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v2, v3, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_a

    const/4 v3, 0x2

    if-eq v2, v3, :cond_9

    goto :goto_3

    :cond_9
    const/high16 v1, -0x1000000

    goto :goto_3

    :cond_a
    const/4 v1, -0x1

    .line 43
    :goto_3
    invoke-virtual {p0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    :goto_4
    if-eqz v0, :cond_b

    .line 44
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/n;->j()V

    .line 45
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_b
    :goto_5
    return-void
.end method

.method public final varargs b([Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/n;->d()V

    return-void
.end method

.method public synthetic b(Z)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$b(Lcom/pspdfkit/internal/views/annotations/a;Z)Z

    move-result p1

    return p1
.end method

.method public final c()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->k:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/n;->l()Z

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/n;->j()V

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_1
    :goto_0
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->h:Lcom/pspdfkit/annotations/BlendMode;

    sget-object v2, Lcom/pspdfkit/annotations/BlendMode;->NORMAL:Lcom/pspdfkit/annotations/BlendMode;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->k:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->k:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    int-to-float v4, v2

    iget v2, v1, Landroid/graphics/Rect;->top:I

    int-to-float v5, v2

    iget v2, v1, Landroid/graphics/Rect;->right:I

    int-to-float v6, v2

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v1

    iget-object v8, p0, Lcom/pspdfkit/internal/views/annotations/n;->f:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;)I

    .line 11
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 12
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return-void
.end method

.method public synthetic e()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$e(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v0

    return v0
.end method

.method public synthetic f()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$f(Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method public final g()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getAnnotation()Lcom/pspdfkit/annotations/Annotation;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    return-object v0

    .line 6
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getAnnotation() can be used only when single annotation is bound to ShapeAnnotationView."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getAnnotations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getApproximateMemoryUsage()I
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    sget v1, Lcom/pspdfkit/internal/ao;->a:I

    const-string v1, "layoutParams"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1115
    instance-of v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    if-eqz v1, :cond_0

    .line 1116
    check-cast v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    iget-object v0, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v0

    const-string v1, "layoutParams.pageRect.screenRect"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1118
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    const/16 v2, 0x800

    .line 1119
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1120
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    .line 1121
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    mul-int v0, v0, v1

    mul-int/lit8 v0, v0, 0x4

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic getContentScaler()Lcom/pspdfkit/internal/k0;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$getContentScaler(Lcom/pspdfkit/internal/views/annotations/a;)Lcom/pspdfkit/internal/k0;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getPageRect()Lcom/pspdfkit/utils/PageRect;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$getPageRect(Lcom/pspdfkit/internal/views/annotations/a;)Lcom/pspdfkit/utils/PageRect;

    move-result-object v0

    return-object v0
.end method

.method public getShapes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/b2;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->p:Ljava/util/ArrayList;

    return-object v0
.end method

.method public synthetic i()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/views/annotations/a$-CC;->$default$i(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->i:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/internal/views/annotations/n;)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->i:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/internal/views/annotations/n;)V

    :cond_0
    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->k:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->n:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    goto/16 :goto_3

    .line 6
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/n;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/n;->j()V

    .line 10
    :cond_1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->s:Z

    if-eqz v0, :cond_3

    .line 11
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->k:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->k:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->j:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v4

    int-to-float v2, v2

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v3

    int-to-float v1, v1

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->p:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/br;

    .line 15
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->d:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/pspdfkit/internal/views/annotations/n;->e:Landroid/graphics/Paint;

    invoke-interface {v2, p1, v3, v4}, Lcom/pspdfkit/internal/br;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 17
    :cond_2
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_3

    .line 18
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->c:Lcom/pspdfkit/internal/hr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hr;->d()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->c:Lcom/pspdfkit/internal/hr;

    .line 19
    invoke-virtual {v0}, Lcom/pspdfkit/internal/hr;->c()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->j:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_5

    .line 20
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 21
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->k:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {p1, v3, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 25
    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->c:Lcom/pspdfkit/internal/hr;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/hr;->c()Landroid/graphics/Rect;

    move-result-object v2

    .line 26
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->l:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v3, v1, v1, v4, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 29
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->c:Lcom/pspdfkit/internal/hr;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/hr;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->l:Landroid/graphics/Rect;

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 31
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_3

    .line 33
    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 34
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->k:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 35
    iget v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->n:F

    invoke-virtual {p1, v1, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 36
    iget v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->q:F

    neg-float v1, v1

    iget v2, p0, Lcom/pspdfkit/internal/views/annotations/n;->r:F

    neg-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 37
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->p:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/br;

    .line 38
    iget-object v3, p0, Lcom/pspdfkit/internal/views/annotations/n;->d:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/pspdfkit/internal/views/annotations/n;->e:Landroid/graphics/Paint;

    invoke-interface {v2, p1, v3, v4}, Lcom/pspdfkit/internal/br;->b(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    goto :goto_2

    .line 40
    :cond_6
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_7
    :goto_3
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 0

    .line 1
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/n;->b()V

    return-void
.end method

.method public final p()V
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/internal/views/annotations/b;->b:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/n;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/pspdfkit/internal/views/annotations/b;->a(Lcom/pspdfkit/internal/views/annotations/a;Z)Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final recycle()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->c:Lcom/pspdfkit/internal/hr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hr;->recycle()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->j:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->k:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->m:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->setEmpty()V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    const/4 v0, 0x0

    .line 8
    iput v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->n:F

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/n;->p:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 12
    iput v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->q:F

    .line 13
    iput v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->r:F

    const/4 v0, 0x0

    .line 14
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->s:Z

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->u:Lcom/pspdfkit/internal/views/annotations/i;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/i;->a()V

    return-void
.end method

.method public setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/annotations/n;->setAnnotations(Ljava/util/List;)V

    return-void
.end method

.method public setAnnotations(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/n;->d()V

    return-void
.end method

.method public setForceHighQualityDrawing(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/annotations/n;->s:Z

    return-void
.end method

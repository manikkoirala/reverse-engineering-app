.class public final synthetic Lcom/pspdfkit/internal/views/annotations/a$-CC;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/pspdfkit/annotations/Annotation;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public static $default$a(Lcom/pspdfkit/internal/views/annotations/a;FLandroid/graphics/Matrix;)V
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/internal/views/annotations/a;

    .line 0
    return-void
.end method

.method public static $default$b(Lcom/pspdfkit/internal/views/annotations/a;Z)Z
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/internal/views/annotations/a;

    .line 0
    const/4 p1, 0x0

    return p1
.end method

.method public static $default$e(Lcom/pspdfkit/internal/views/annotations/a;)Z
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/internal/views/annotations/a;

    .line 0
    const/4 v0, 0x0

    return v0
.end method

.method public static $default$f(Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/internal/views/annotations/a;

    .line 0
    return-void
.end method

.method public static $default$g(Lcom/pspdfkit/internal/views/annotations/a;)Z
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/internal/views/annotations/a;

    .line 0
    const/4 v0, 0x0

    return v0
.end method

.method public static $default$getApproximateMemoryUsage(Lcom/pspdfkit/internal/views/annotations/a;)I
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/internal/views/annotations/a;

    .line 0
    const/4 v0, 0x0

    return v0
.end method

.method public static $default$getContentScaler(Lcom/pspdfkit/internal/views/annotations/a;)Lcom/pspdfkit/internal/k0;
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/internal/views/annotations/a;

    .line 0
    const/4 v0, 0x0

    return-object v0
.end method

.method public static $default$getPageRect(Lcom/pspdfkit/internal/views/annotations/a;)Lcom/pspdfkit/utils/PageRect;
    .locals 4
    .param p0, "_this"    # Lcom/pspdfkit/internal/views/annotations/a;

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2
    instance-of v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    if-eqz v1, :cond_0

    .line 3
    check-cast v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    iget-object v0, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    return-object v0

    .line 5
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Annotation view had unexpected LayoutParams: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static $default$i(Lcom/pspdfkit/internal/views/annotations/a;)Z
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/internal/views/annotations/a;

    .line 0
    const/4 v0, 0x1

    return v0
.end method

.method public static $default$k(Lcom/pspdfkit/internal/views/annotations/a;)Z
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/internal/views/annotations/a;

    .line 0
    const/4 v0, 0x0

    return v0
.end method

.method public static $default$o(Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/internal/views/annotations/a;

    .line 0
    return-void
.end method

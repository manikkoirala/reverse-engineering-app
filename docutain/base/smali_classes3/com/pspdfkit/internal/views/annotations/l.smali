.class public final Lcom/pspdfkit/internal/views/annotations/l;
.super Lcom/pspdfkit/internal/views/annotations/j;
.source "SourceFile"


# instance fields
.field private final v:Lcom/pspdfkit/ui/PdfFragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 1

    .line 1
    invoke-virtual {p3}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/internal/views/annotations/j;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)V

    .line 2
    iput-object p3, p0, Lcom/pspdfkit/internal/views/annotations/l;->v:Lcom/pspdfkit/ui/PdfFragment;

    const/4 p1, 0x1

    .line 3
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/annotations/j;->setRefreshBoundingBoxAfterRendering(Z)V

    return-void
.end method


# virtual methods
.method protected final r()Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/views/annotations/j;->r()Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/l;->v:Lcom/pspdfkit/ui/PdfFragment;

    .line 2
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->isRedactionAnnotationPreviewEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;->redactionAnnotationPreviewEnabled(Z)Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/j;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/j;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/Annotation;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/views/annotations/j;->setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/j;->s()V

    return-void
.end method

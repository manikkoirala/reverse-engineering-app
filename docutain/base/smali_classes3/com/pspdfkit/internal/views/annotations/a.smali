.class public interface abstract Lcom/pspdfkit/internal/views/annotations/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/annotations/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/pspdfkit/annotations/Annotation;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a()Landroid/view/View;
.end method

.method public abstract a(FLandroid/graphics/Matrix;)V
.end method

.method public abstract a(Lcom/pspdfkit/internal/views/annotations/a$a;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/annotations/a$a<",
            "TT;>;)V"
        }
    .end annotation
.end method

.method public abstract b()V
.end method

.method public abstract b(Z)Z
.end method

.method public abstract e()Z
.end method

.method public abstract f()V
.end method

.method public abstract g()Z
.end method

.method public abstract getAnnotation()Lcom/pspdfkit/annotations/Annotation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract getApproximateMemoryUsage()I
.end method

.method public abstract getContentScaler()Lcom/pspdfkit/internal/k0;
.end method

.method public abstract getPageRect()Lcom/pspdfkit/utils/PageRect;
.end method

.method public abstract i()Z
.end method

.method public abstract k()Z
.end method

.method public abstract o()V
.end method

.method public abstract p()V
.end method

.method public abstract setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

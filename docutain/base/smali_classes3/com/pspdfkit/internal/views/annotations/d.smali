.class public abstract Lcom/pspdfkit/internal/views/annotations/d;
.super Landroidx/appcompat/widget/AppCompatEditText;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/pg$d;
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lcom/pspdfkit/internal/mo;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/annotations/d$a;
    }
.end annotation


# instance fields
.field private b:Lcom/pspdfkit/internal/views/annotations/d$a;

.field private final c:Landroid/graphics/Matrix;

.field private final d:Ljava/lang/Runnable;

.field private final e:Landroid/graphics/Matrix;

.field private f:Lcom/pspdfkit/internal/pg$c;

.field private g:I

.field private h:Z

.field private i:Landroid/text/method/KeyListener;


# direct methods
.method public static synthetic $r8$lambda$tiOTObi8RcDMDDKiUbGJGTwzI0Q(Lcom/pspdfkit/internal/views/annotations/d;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/d;->q()V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/d;->c:Landroid/graphics/Matrix;

    .line 5
    new-instance p1, Lcom/pspdfkit/internal/views/annotations/d$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/views/annotations/d$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/annotations/d;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/d;->d:Ljava/lang/Runnable;

    .line 8
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/d;->e:Landroid/graphics/Matrix;

    const/4 p1, 0x0

    .line 28
    invoke-static {p0, p1}, Landroidx/core/view/ViewCompat;->setBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    const/4 p1, 0x0

    .line 29
    invoke-virtual {p0, p1, p1, p1, p1}, Landroid/view/View;->setPadding(IIII)V

    const v0, 0x28001

    .line 30
    invoke-virtual {p0, v0}, Landroidx/appcompat/widget/AppCompatEditText;->setInputType(I)V

    .line 33
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 34
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setSingleLine(Z)V

    const v0, 0x800033

    .line 35
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 36
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/pspdfkit/internal/mt;->a()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/fonts/Font;

    .line 39
    invoke-virtual {v0}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    .line 40
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 44
    invoke-virtual {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 46
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object p1

    const/4 v0, 0x1

    .line 52
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setLinearText(Z)V

    .line 53
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setSubpixelText(Z)V

    return-void
.end method

.method private q()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/d;->b:Lcom/pspdfkit/internal/views/annotations/d$a;

    if-nez v0, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/d;->c:Landroid/graphics/Matrix;

    .line 6
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 7
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 8
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v2

    invoke-virtual {p0, v2, v1}, Landroid/widget/TextView;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 9
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v2

    neg-int v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v3

    neg-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    .line 12
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineHeight()I

    move-result v2

    int-to-float v2, v2

    .line 13
    iget v3, v0, Landroid/graphics/RectF;->top:F

    .line 14
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v3, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v2, v1

    sub-float/2addr v3, v1

    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    .line 15
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    add-float/2addr v2, v1

    .line 16
    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    .line 17
    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 18
    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 19
    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 22
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/d;->c:Landroid/graphics/Matrix;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;)V

    .line 23
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/d;->b:Lcom/pspdfkit/internal/views/annotations/d$a;

    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/views/annotations/d$a;->a(Landroid/graphics/RectF;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private setKeyboardResizeWindow(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/16 v0, 0x10

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/pg;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/views/annotations/d;->g:I

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    iget v0, p0, Lcom/pspdfkit/internal/views/annotations/d;->g:I

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/pg;->a(Landroid/content/Context;I)I

    :goto_0
    return-void
.end method


# virtual methods
.method public a(FLandroid/graphics/Matrix;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {p1, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    if-eqz p1, :cond_1

    .line 2
    invoke-static {p0}, Landroidx/core/view/ViewCompat;->isAttachedToWindow(Landroid/view/View;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/d;->d:Ljava/lang/Runnable;

    invoke-virtual {p0, p1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/views/annotations/d;->d:Ljava/lang/Runnable;

    const-wide/16 v0, 0x64

    invoke-virtual {p0, p1, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public c()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/d;->h:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/d;->h:Z

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/d;->i:Landroid/text/method/KeyListener;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/widget/TextView;->getKeyListener()Landroid/text/method/KeyListener;

    move-result-object v1

    if-nez v1, :cond_1

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/d;->i:Landroid/text/method/KeyListener;

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setKeyListener(Landroid/text/method/KeyListener;)V

    :cond_1
    const/4 v1, 0x0

    .line 7
    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/d;->i:Landroid/text/method/KeyListener;

    .line 9
    invoke-virtual {p0, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 10
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setCursorVisible(Z)V

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    .line 14
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 16
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 19
    :cond_2
    invoke-virtual {p0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 21
    invoke-static {p0, p0}, Lcom/pspdfkit/internal/pg;->a(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;)Lcom/pspdfkit/internal/pg$c;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/d;->f:Lcom/pspdfkit/internal/pg$c;

    .line 22
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/annotations/d;->setKeyboardVisible(Z)V

    .line 23
    invoke-virtual {p0, p0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method protected abstract getBoundingBox()Landroid/graphics/RectF;
.end method

.method protected getPdfToViewMatrix()Landroid/graphics/Matrix;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/d;->c:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public l()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/d;->h:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/d;->h:Z

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/views/annotations/d;->b:Lcom/pspdfkit/internal/views/annotations/d$a;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/d$a;->f()Z

    move-result v1

    if-nez v1, :cond_2

    .line 4
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {p0}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lcom/pspdfkit/internal/views/annotations/d;

    if-eqz v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x0

    if-nez v1, :cond_4

    .line 5
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/annotations/d;->setKeyboardVisible(Z)V

    .line 6
    invoke-virtual {p0, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->clearFocus()V

    goto :goto_1

    .line 10
    :cond_4
    invoke-virtual {p0}, Landroid/widget/TextView;->getKeyListener()Landroid/text/method/KeyListener;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/views/annotations/d;->i:Landroid/text/method/KeyListener;

    .line 11
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 12
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setCursorVisible(Z)V

    .line 15
    :goto_1
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 16
    invoke-virtual {p0, v2}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 17
    invoke-virtual {p0, p0}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/views/annotations/d;->f:Lcom/pspdfkit/internal/pg$c;

    if-eqz v0, :cond_5

    .line 20
    invoke-virtual {v0}, Lcom/pspdfkit/internal/pg$c;->d()V

    :cond_5
    return-void
.end method

.method public final m()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/annotations/d;->h:Z

    return v0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1

    const/4 p1, 0x1

    if-nez p2, :cond_3

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/internal/views/annotations/d;->b:Lcom/pspdfkit/internal/views/annotations/d$a;

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/pspdfkit/internal/views/annotations/d$a;->f()Z

    move-result p2

    if-nez p2, :cond_2

    .line 2
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    move-result p2

    if-nez p2, :cond_1

    invoke-static {p0}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;)Landroid/app/Activity;

    move-result-object p2

    invoke-virtual {p2}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object p2

    instance-of p2, p2, Lcom/pspdfkit/internal/views/annotations/d;

    if-eqz p2, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :cond_2
    :goto_0
    if-nez p1, :cond_4

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/annotations/d;->setKeyboardVisible(Z)V

    goto :goto_1

    .line 6
    :cond_3
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/annotations/d;->setKeyboardVisible(Z)V

    :cond_4
    :goto_1
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 0

    .line 1
    invoke-super/range {p0 .. p5}, Landroidx/appcompat/widget/AppCompatEditText;->onLayout(ZIIII)V

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    instance-of p1, p1, Lcom/pspdfkit/internal/en;

    if-eqz p1, :cond_0

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/en;

    .line 7
    iget-object p2, p0, Lcom/pspdfkit/internal/views/annotations/d;->e:Landroid/graphics/Matrix;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/en;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/en;->getZoomScale()F

    move-result p1

    .line 10
    iget-object p2, p0, Lcom/pspdfkit/internal/views/annotations/d;->c:Landroid/graphics/Matrix;

    iget-object p3, p0, Lcom/pspdfkit/internal/views/annotations/d;->e:Landroid/graphics/Matrix;

    invoke-virtual {p2, p3}, Landroid/graphics/Matrix;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 11
    iget-object p2, p0, Lcom/pspdfkit/internal/views/annotations/d;->e:Landroid/graphics/Matrix;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/views/annotations/d;->a(FLandroid/graphics/Matrix;)V

    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/internal/views/annotations/d;->b:Lcom/pspdfkit/internal/views/annotations/d$a;

    if-eqz p2, :cond_0

    .line 2
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/pspdfkit/internal/views/annotations/d$a;->a(Ljava/lang/String;)V

    .line 4
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/annotations/d;->q()V

    return-void
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->onWindowFocusChanged(Z)V

    .line 9
    invoke-virtual {p0, p0, p1}, Lcom/pspdfkit/internal/views/annotations/d;->onFocusChange(Landroid/view/View;Z)V

    return-void
.end method

.method public recycle()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/views/annotations/d;->b:Lcom/pspdfkit/internal/views/annotations/d$a;

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->l()V

    const/4 v0, 0x0

    .line 4
    iput v0, p0, Lcom/pspdfkit/internal/views/annotations/d;->g:I

    return-void
.end method

.method public setEditTextViewListener(Lcom/pspdfkit/internal/views/annotations/d$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/annotations/d;->b:Lcom/pspdfkit/internal/views/annotations/d$a;

    return-void
.end method

.method protected setKeyboardVisible(Z)V
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/annotations/d;->setKeyboardResizeWindow(Z)V

    .line 2
    invoke-static {p0, p0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 4
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/annotations/d;->setKeyboardResizeWindow(Z)V

    .line 5
    invoke-static {p0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    :goto_0
    return-void
.end method

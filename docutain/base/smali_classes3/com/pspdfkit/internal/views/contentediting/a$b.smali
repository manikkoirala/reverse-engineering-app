.class public final Lcom/pspdfkit/internal/views/contentediting/a$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/views/contentediting/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/views/contentediting/a;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/views/contentediting/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p5

    move/from16 v3, p6

    const-string v4, ""

    const-string v5, "source"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "dest"

    move-object/from16 v6, p4

    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v5, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/pspdfkit/internal/views/contentediting/a;->a(Lcom/pspdfkit/internal/views/contentediting/a;Lcom/pspdfkit/internal/l6;)V

    .line 6
    instance-of v5, v1, Lcom/pspdfkit/internal/e7;

    if-eqz v5, :cond_0

    return-object v1

    .line 339
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 340
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_0
    const/4 v9, 0x1

    if-ge v8, v6, :cond_3

    .line 341
    invoke-interface {v1, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v10

    .line 342
    invoke-static {v10}, Ljava/lang/Character;->getType(C)I

    move-result v11

    const/16 v12, 0x13

    if-eq v11, v12, :cond_1

    const/16 v12, 0x1c

    if-eq v11, v12, :cond_1

    goto :goto_1

    :cond_1
    const/4 v9, 0x0

    :goto_1
    if-eqz v9, :cond_2

    .line 672
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/Appendable;

    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 673
    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    sub-int v6, v3, v2

    .line 674
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-lez v1, :cond_4

    const/4 v8, 0x1

    goto :goto_2

    :cond_4
    const/4 v8, 0x0

    :goto_2
    if-lez v6, :cond_5

    const/4 v6, 0x1

    goto :goto_3

    :cond_5
    const/4 v6, 0x0

    :goto_3
    if-eqz v8, :cond_6

    if-eqz v6, :cond_6

    const/4 v10, 0x1

    goto :goto_4

    :cond_6
    const/4 v10, 0x0

    .line 681
    :goto_4
    :try_start_0
    new-instance v11, Lcom/pspdfkit/internal/e7;

    invoke-direct {v11, v5, v1}, Lcom/pspdfkit/internal/e7;-><init>(Ljava/lang/CharSequence;I)V

    .line 683
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 689
    iget-object v5, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-static {v5}, Lcom/pspdfkit/internal/views/contentediting/a;->b(Lcom/pspdfkit/internal/views/contentediting/a;)Lcom/pspdfkit/internal/su;

    move-result-object v5

    if-nez v5, :cond_7

    iget-object v5, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v5

    invoke-static {v5}, Lcom/pspdfkit/internal/z6;->a(Lcom/pspdfkit/internal/pt;)Lcom/pspdfkit/internal/su;

    move-result-object v5

    .line 691
    :cond_7
    iget-object v14, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v14}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v14

    invoke-virtual {v14}, Lcom/pspdfkit/internal/pt;->f()Lcom/pspdfkit/internal/jt;

    move-result-object v14
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v15, "text.toString()"

    if-eqz v10, :cond_8

    .line 696
    :try_start_1
    iget-object v6, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/views/contentediting/a;->getEditor()Lcom/pspdfkit/internal/a7;

    move-result-object v6

    iget-object v8, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v8}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v8

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v8, v9, v2, v3}, Lcom/pspdfkit/internal/a7;->a(Lcom/pspdfkit/internal/pt;Ljava/lang/String;II)Lcom/pspdfkit/internal/o6;

    move-result-object v2

    goto/16 :goto_7

    :cond_8
    if-eqz v8, :cond_9

    .line 698
    iget-object v3, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/views/contentediting/a;->getEditor()Lcom/pspdfkit/internal/a7;

    move-result-object v3

    iget-object v6, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v6

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v6, v8, v2}, Lcom/pspdfkit/internal/a7;->a(Lcom/pspdfkit/internal/pt;Ljava/lang/String;I)Lcom/pspdfkit/internal/o6;

    move-result-object v2

    goto/16 :goto_7

    :cond_9
    if-eqz v6, :cond_a

    .line 700
    iget-object v6, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/views/contentediting/a;->getEditor()Lcom/pspdfkit/internal/a7;

    move-result-object v6

    iget-object v8, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v8}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v8

    invoke-interface {v6, v8, v2, v3}, Lcom/pspdfkit/internal/a7;->b(Lcom/pspdfkit/internal/pt;II)Lcom/pspdfkit/internal/o6;

    move-result-object v2

    goto :goto_7

    :cond_a
    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Integer;

    .line 704
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v9

    const/4 v8, 0x2

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v8

    const/4 v2, 0x3

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 999
    instance-of v3, v2, Ljava/util/Collection;

    if-eqz v3, :cond_b

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_b

    goto :goto_6

    .line 1000
    :cond_b
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    if-nez v3, :cond_d

    const/4 v3, 0x1

    goto :goto_5

    :cond_d
    const/4 v3, 0x0

    :goto_5
    if-nez v3, :cond_c

    const/4 v9, 0x0

    :cond_e
    :goto_6
    if-eqz v9, :cond_11

    .line 1001
    iget-object v2, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/pt;->j()I

    move-result v2

    if-eqz v2, :cond_11

    .line 1002
    iget-object v2, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/views/contentediting/a;->getEditor()Lcom/pspdfkit/internal/a7;

    move-result-object v2

    iget-object v3, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v3

    const/4 v6, -0x1

    invoke-interface {v2, v3, v7, v6}, Lcom/pspdfkit/internal/a7;->b(Lcom/pspdfkit/internal/pt;II)Lcom/pspdfkit/internal/o6;

    move-result-object v2

    .line 1007
    :goto_7
    invoke-virtual {v2}, Lcom/pspdfkit/internal/o6;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/av;

    .line 1010
    invoke-static {v2}, Lcom/pspdfkit/internal/z6;->a(Lcom/pspdfkit/internal/av;)Lcom/pspdfkit/internal/su;

    move-result-object v3

    .line 1015
    iget-object v6, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    new-instance v8, Lcom/pspdfkit/internal/l6;

    .line 1016
    invoke-virtual {v6}, Lcom/pspdfkit/internal/views/contentediting/a;->getPageIndex()I

    move-result v9

    .line 1017
    iget-object v10, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v10}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextblockId()Ljava/util/UUID;

    move-result-object v10

    .line 1020
    iget-object v15, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v15}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v15

    invoke-virtual {v15}, Lcom/pspdfkit/internal/pt;->g()Lcom/pspdfkit/internal/cb;

    move-result-object v15

    move-object/from16 p1, v8

    move/from16 p2, v9

    move-object/from16 p3, v10

    move-object/from16 p4, v5

    move-object/from16 p5, v3

    move-object/from16 p6, v15

    .line 1021
    invoke-direct/range {p1 .. p6}, Lcom/pspdfkit/internal/l6;-><init>(ILjava/util/UUID;Lcom/pspdfkit/internal/su;Lcom/pspdfkit/internal/su;Lcom/pspdfkit/internal/cb;)V

    invoke-static {v6, v8}, Lcom/pspdfkit/internal/views/contentediting/a;->a(Lcom/pspdfkit/internal/views/contentediting/a;Lcom/pspdfkit/internal/l6;)V

    .line 1028
    iget-object v3, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-static {v3}, Lcom/pspdfkit/internal/views/contentediting/a;->c(Lcom/pspdfkit/internal/views/contentediting/a;)V

    .line 1031
    iget-object v3, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/views/contentediting/a;->getListener()Lcom/pspdfkit/internal/views/contentediting/a$a;

    move-result-object v3

    iget-object v5, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v5

    invoke-static {v3, v5, v2}, Lcom/pspdfkit/internal/views/contentediting/a$a$a;->a(Lcom/pspdfkit/internal/views/contentediting/a$a;Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/av;)V

    .line 1032
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v12

    const-string v5, "PSPDFKit.ContentEditing"

    .line 1033
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "time to process input = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " ms"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v5, v2, v3}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1036
    iget-object v2, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/pt;->j()I

    move-result v2

    if-nez v2, :cond_f

    .line 1037
    iget-object v2, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/pt;->k()Lcom/pspdfkit/internal/av;

    move-result-object v2

    new-instance v3, Lcom/pspdfkit/internal/d8;

    iget-object v5, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v5

    invoke-static {v5, v14}, Lcom/pspdfkit/internal/pt;->a(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/jt;)Lcom/pspdfkit/internal/fj;

    move-result-object v5

    const/4 v6, 0x6

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/d8;-><init>(Lcom/pspdfkit/internal/fj;I)V

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/av;->a(Lcom/pspdfkit/internal/d8;)V

    .line 1041
    iget-object v2, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/pt;->k()Lcom/pspdfkit/internal/av;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/av;->f()Lcom/pspdfkit/internal/d8;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/d8;->a()Lcom/pspdfkit/internal/fj;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Lcom/pspdfkit/internal/fj;->c()Lcom/pspdfkit/internal/y3;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Lcom/pspdfkit/internal/y3;->b()F

    move-result v2

    iget-object v3, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    .line 1042
    invoke-static {v3}, Lcom/pspdfkit/internal/views/contentediting/a;->a(Lcom/pspdfkit/internal/views/contentediting/a;)F

    move-result v5

    mul-float v2, v2, v5

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1047
    :cond_f
    invoke-static {v7, v1}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v1

    iget-object v2, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    .line 1305
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_10

    move-object v3, v1

    check-cast v3, Lkotlin/collections/IntIterator;

    invoke-virtual {v3}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v3

    .line 1306
    new-instance v5, Lcom/pspdfkit/internal/views/contentediting/c;

    invoke-direct {v5, v2}, Lcom/pspdfkit/internal/views/contentediting/c;-><init>(Lcom/pspdfkit/internal/views/contentediting/a;)V

    add-int/lit8 v6, v3, 0x1

    const/16 v7, 0x21

    .line 1307
    invoke-virtual {v11, v5, v3, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_8

    .line 1315
    :cond_10
    iget-object v1, v0, Lcom/pspdfkit/internal/views/contentediting/a$b;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextMetrics()Lcom/pspdfkit/internal/views/contentediting/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/contentediting/d;->c()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-object v11

    :catch_0
    :cond_11
    return-object v4
.end method

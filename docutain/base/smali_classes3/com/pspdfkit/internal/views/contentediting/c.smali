.class public final Lcom/pspdfkit/internal/views/contentediting/c;
.super Landroid/text/style/ReplacementSpan;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/views/contentediting/a;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/views/contentediting/a;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/c;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 0

    const-string p2, "canvas"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "paint"

    invoke-static {p9, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 0

    const-string p2, "paint"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/c;->a:Lcom/pspdfkit/internal/views/contentediting/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextMetrics()Lcom/pspdfkit/internal/views/contentediting/d;

    move-result-object p1

    if-eqz p5, :cond_0

    .line 3
    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/views/contentediting/d;->b(I)Lcom/pspdfkit/internal/views/contentediting/d$a;

    move-result-object p2

    .line 4
    invoke-virtual {p2}, Lcom/pspdfkit/internal/views/contentediting/d$a;->c()I

    move-result p4

    iput p4, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 5
    invoke-virtual {p2}, Lcom/pspdfkit/internal/views/contentediting/d$a;->a()I

    move-result p2

    iput p2, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 6
    iget p4, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iput p4, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 7
    iput p2, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 11
    :cond_0
    :try_start_0
    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/views/contentediting/d;->a(I)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

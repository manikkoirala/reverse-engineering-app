.class public final Lcom/pspdfkit/internal/views/contentediting/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/contentediting/d$a;
    }
.end annotation


# instance fields
.field private a:Lcom/pspdfkit/internal/pt;

.field private b:F

.field private c:[Lcom/pspdfkit/internal/views/contentediting/d$a;

.field private d:[Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/pt;F)V
    .locals 1

    const-string v0, "textBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/d;->a:Lcom/pspdfkit/internal/pt;

    .line 2
    iput p2, p0, Lcom/pspdfkit/internal/views/contentediting/d;->b:F

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/contentediting/d;->a()[Lcom/pspdfkit/internal/views/contentediting/d$a;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/d;->c:[Lcom/pspdfkit/internal/views/contentediting/d$a;

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/contentediting/d;->b()[Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/d;->d:[Ljava/lang/Integer;

    return-void
.end method

.method private final a()[Lcom/pspdfkit/internal/views/contentediting/d$a;
    .locals 4

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/d;->a:Lcom/pspdfkit/internal/pt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pt;->h()I

    move-result v0

    .line 14
    new-array v1, v0, [Lcom/pspdfkit/internal/views/contentediting/d$a;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    const/4 v3, 0x0

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private final b()[Ljava/lang/Integer;
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/d;->a:Lcom/pspdfkit/internal/pt;

    .line 36
    invoke-virtual {v0}, Lcom/pspdfkit/internal/pt;->e()Lcom/pspdfkit/internal/bv;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/pspdfkit/internal/bv;->d()Ljava/lang/String;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 39
    new-array v1, v0, [Ljava/lang/Integer;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    const/4 v3, 0x0

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/d;->d:[Ljava/lang/Integer;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/d;->a:Lcom/pspdfkit/internal/pt;

    sget-object v1, Lcom/pspdfkit/internal/pt;->Companion:Lcom/pspdfkit/internal/pt$b;

    const/4 v1, 0x0

    .line 2
    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/pt;->a(IZ)Lcom/pspdfkit/internal/pt$c;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/pt$c;->a()Lcom/pspdfkit/internal/ra;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ra;->c()Lcom/pspdfkit/internal/iv;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/iv;->a()F

    move-result v1

    iget v2, p0, Lcom/pspdfkit/internal/views/contentediting/d;->b:F

    mul-float v1, v1, v2

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ra;->a()Lcom/pspdfkit/internal/iv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/iv;->a()F

    move-result v0

    iget v2, p0, Lcom/pspdfkit/internal/views/contentediting/d;->b:F

    mul-float v0, v0, v2

    add-float/2addr v0, v1

    invoke-static {v0}, Lkotlin/math/MathKt;->roundToInt(F)I

    move-result v0

    invoke-static {v1}, Lkotlin/math/MathKt;->roundToInt(F)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 12
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/views/contentediting/d;->d:[Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, p1

    move p1, v0

    :goto_1
    return p1
.end method

.method public final a(F)V
    .locals 1

    .line 15
    iget v0, p0, Lcom/pspdfkit/internal/views/contentediting/d;->b:F

    cmpg-float v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    return-void

    .line 16
    :cond_1
    iput p1, p0, Lcom/pspdfkit/internal/views/contentediting/d;->b:F

    .line 17
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/contentediting/d;->c()V

    return-void
.end method

.method public final b(I)Lcom/pspdfkit/internal/views/contentediting/d$a;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/d;->a:Lcom/pspdfkit/internal/pt;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/pt;->b(I)I

    move-result p1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/d;->c:[Lcom/pspdfkit/internal/views/contentediting/d$a;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    new-instance v0, Lcom/pspdfkit/internal/views/contentediting/d$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/contentediting/d;->a:Lcom/pspdfkit/internal/pt;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/pt;->c(I)Lcom/pspdfkit/internal/ah;

    move-result-object v1

    iget v2, p0, Lcom/pspdfkit/internal/views/contentediting/d;->b:F

    const-string v3, "line"

    .line 3
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ah;->c()Lcom/pspdfkit/internal/jh;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/jh;->b()F

    move-result v3

    neg-float v3, v3

    mul-float v3, v3, v2

    invoke-static {v3}, Lkotlin/math/MathKt;->roundToInt(F)I

    move-result v3

    .line 32
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ah;->c()Lcom/pspdfkit/internal/jh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/jh;->a()F

    move-result v1

    mul-float v1, v1, v2

    invoke-static {v1}, Lkotlin/math/MathKt;->roundToInt(F)I

    move-result v1

    .line 33
    invoke-direct {v0, v3, v1}, Lcom/pspdfkit/internal/views/contentediting/d$a;-><init>(II)V

    .line 34
    iget-object v1, p0, Lcom/pspdfkit/internal/views/contentediting/d;->c:[Lcom/pspdfkit/internal/views/contentediting/d$a;

    aput-object v0, v1, p1

    :cond_0
    return-object v0
.end method

.method public final c()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/contentediting/d;->b()[Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/d;->d:[Ljava/lang/Integer;

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/contentediting/d;->a()[Lcom/pspdfkit/internal/views/contentediting/d$a;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/d;->c:[Lcom/pspdfkit/internal/views/contentediting/d$a;

    return-void
.end method

.class public final Lcom/pspdfkit/internal/views/contentediting/a;
.super Lcom/pspdfkit/internal/views/annotations/d;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/h6;
.implements Landroidx/core/view/OnReceiveContentListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/contentediting/a$a;,
        Lcom/pspdfkit/internal/views/contentediting/a$b;
    }
.end annotation


# instance fields
.field private final j:Lcom/pspdfkit/internal/a7;

.field private final k:I

.field private final l:Lcom/pspdfkit/internal/pt;

.field private final m:Lcom/pspdfkit/internal/views/contentediting/a$a;

.field private final n:Lcom/pspdfkit/internal/fl;

.field private o:F

.field private final p:Ljava/util/UUID;

.field private q:Lcom/pspdfkit/internal/l6;

.field private r:Lcom/pspdfkit/internal/su;

.field private final s:I

.field private final t:I

.field private u:Lcom/pspdfkit/internal/views/contentediting/d;

.field private v:Landroid/graphics/RectF;

.field private w:Lcom/pspdfkit/internal/jt;

.field private final x:Lkotlin/Lazy;


# direct methods
.method public static synthetic $r8$lambda$4vhiUkXxKxaGEpbJKv9otekHQn0(Landroid/content/ClipData$Item;)Z
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/views/contentediting/a;->a(Landroid/content/ClipData$Item;)Z

    move-result p0

    return p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;ILcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/views/contentediting/a$a;Lcom/pspdfkit/internal/fl;F)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "textBlock"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/annotations/d;-><init>(Landroid/content/Context;)V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/views/contentediting/a;->j:Lcom/pspdfkit/internal/a7;

    .line 3
    iput p3, p0, Lcom/pspdfkit/internal/views/contentediting/a;->k:I

    .line 4
    iput-object p4, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    .line 5
    iput-object p5, p0, Lcom/pspdfkit/internal/views/contentediting/a;->m:Lcom/pspdfkit/internal/views/contentediting/a$a;

    .line 6
    iput-object p6, p0, Lcom/pspdfkit/internal/views/contentediting/a;->n:Lcom/pspdfkit/internal/fl;

    .line 7
    iput p7, p0, Lcom/pspdfkit/internal/views/contentediting/a;->o:F

    .line 8
    invoke-virtual {p4}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/views/contentediting/a;->p:Ljava/util/UUID;

    const/16 p2, 0xc

    .line 9
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p2

    iput p2, p0, Lcom/pspdfkit/internal/views/contentediting/a;->s:I

    const/16 p2, 0x20

    .line 10
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p2

    iput p2, p0, Lcom/pspdfkit/internal/views/contentediting/a;->t:I

    .line 11
    new-instance p2, Lcom/pspdfkit/internal/views/contentediting/d;

    iget p3, p0, Lcom/pspdfkit/internal/views/contentediting/a;->o:F

    invoke-direct {p2, p4, p3}, Lcom/pspdfkit/internal/views/contentediting/d;-><init>(Lcom/pspdfkit/internal/pt;F)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/contentediting/a;->u:Lcom/pspdfkit/internal/views/contentediting/d;

    .line 12
    new-instance p2, Landroid/graphics/RectF;

    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/contentediting/a;->v:Landroid/graphics/RectF;

    const-string p2, "image/*"

    const-string p3, "image/png"

    const-string p4, "image/gif"

    const-string p5, "image/jpeg"

    .line 28
    filled-new-array {p2, p3, p4, p5}, [Ljava/lang/String;

    move-result-object p2

    .line 29
    invoke-static {p0, p2, p0}, Landroidx/core/view/ViewCompat;->setOnReceiveContentListener(Landroid/view/View;[Ljava/lang/String;Landroidx/core/view/OnReceiveContentListener;)V

    .line 34
    invoke-static {p0}, Lcom/pspdfkit/internal/views/contentediting/a;->d(Lcom/pspdfkit/internal/views/contentediting/a;)V

    const/4 p2, 0x1

    new-array p2, p2, [Lcom/pspdfkit/internal/views/contentediting/a$b;

    .line 35
    new-instance p3, Lcom/pspdfkit/internal/views/contentediting/a$b;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/views/contentediting/a$b;-><init>(Lcom/pspdfkit/internal/views/contentediting/a;)V

    const/4 p4, 0x0

    aput-object p3, p2, p4

    check-cast p2, [Landroid/text/InputFilter;

    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 36
    new-instance p2, Lcom/pspdfkit/internal/views/contentediting/b;

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/views/contentediting/b;-><init>(Landroid/content/Context;)V

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->x:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/views/contentediting/a;)F
    .locals 0

    .line 109
    iget p0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->o:F

    return p0
.end method

.method private final a(Lcom/pspdfkit/internal/pt;II)V
    .locals 8

    if-nez p1, :cond_0

    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->j:Lcom/pspdfkit/internal/a7;

    invoke-interface {v0, p1, p2, p3}, Lcom/pspdfkit/internal/a7;->a(Lcom/pspdfkit/internal/pt;II)Lcom/pspdfkit/internal/o6;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/o6;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/av;

    .line 127
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/pt;->c(Lcom/pspdfkit/internal/av;)Lcom/pspdfkit/internal/jt;

    move-result-object v7

    .line 128
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "active = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.ContentEditing"

    invoke-static {v3, v1, v2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    iget-object v1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->m:Lcom/pspdfkit/internal/views/contentediting/a$a;

    iget-object v2, p0, Lcom/pspdfkit/internal/views/contentediting/a;->w:Lcom/pspdfkit/internal/jt;

    invoke-static {v7, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v6, v2, 0x1

    move-object v2, p1

    move-object v3, v7

    move v4, p2

    move v5, p3

    invoke-interface/range {v1 .. v6}, Lcom/pspdfkit/internal/views/contentediting/a$a;->a(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/jt;IIZ)V

    .line 131
    invoke-static {v0}, Lcom/pspdfkit/internal/pt;->b(Lcom/pspdfkit/internal/av;)V

    .line 132
    iput-object v7, p0, Lcom/pspdfkit/internal/views/contentediting/a;->w:Lcom/pspdfkit/internal/jt;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/views/contentediting/a;Lcom/pspdfkit/internal/l6;)V
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->q:Lcom/pspdfkit/internal/l6;

    return-void
.end method

.method private static final a(Landroid/content/ClipData$Item;)Z
    .locals 0

    .line 125
    invoke-virtual {p0}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/views/contentediting/a;)Lcom/pspdfkit/internal/su;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->r:Lcom/pspdfkit/internal/su;

    return-object p0
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/views/contentediting/a;)V
    .locals 1

    const/4 v0, 0x0

    .line 89
    iput-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->r:Lcom/pspdfkit/internal/su;

    return-void
.end method

.method public static synthetic d(Lcom/pspdfkit/internal/views/contentediting/a;)V
    .locals 1

    const/4 v0, 0x0

    .line 5
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/contentediting/a;->c(Z)V

    return-void
.end method


# virtual methods
.method public final a(FLandroid/graphics/Matrix;)V
    .locals 3

    const-string v0, "pdfToViewMatrix"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/internal/views/annotations/d;->a(FLandroid/graphics/Matrix;)V

    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 113
    invoke-virtual {p2, v0}, Landroid/graphics/Matrix;->getValues([F)V

    const/4 p2, 0x0

    aget v0, v0, p2

    .line 114
    iput v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->o:F

    .line 115
    iget-object v1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->u:Lcom/pspdfkit/internal/views/contentediting/d;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/views/contentediting/d;->a(F)V

    .line 116
    iget v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->o:F

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EditText MatrixScale = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, ", PageScale = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, p2, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.ContentEditing"

    invoke-static {v1, p1, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 117
    iget-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt;->h()I

    move-result p1

    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 120
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/pt;->b(I)I

    move-result p1

    .line 122
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->u:Lcom/pspdfkit/internal/views/contentediting/d;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/views/contentediting/d;->b(I)Lcom/pspdfkit/internal/views/contentediting/d$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/contentediting/d$a;->b()I

    move-result p1

    .line 123
    iget v1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->s:I

    iget v2, p0, Lcom/pspdfkit/internal/views/contentediting/a;->t:I

    if-gt p1, v2, :cond_1

    if-gt v1, p1, :cond_1

    const/4 p2, 0x1

    :cond_1
    if-eqz p2, :cond_2

    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    const/4 p1, 0x1

    .line 124
    :goto_1
    invoke-virtual {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->setRotation(F)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/jt;)V
    .locals 8

    const-string v0, "currentStyleInfo"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->g()Ljava/lang/Float;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    const/4 v0, 0x1

    int-to-float v0, v0

    rem-float v1, p1, v0

    .line 99
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sub-float/2addr p1, v0

    goto :goto_0

    :cond_0
    float-to-double v0, p1

    .line 102
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float p1, v0

    :goto_0
    float-to-int p1, p1

    .line 106
    invoke-interface {p0}, Lcom/pspdfkit/internal/h6;->getAvailableFontSizes()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    if-le v0, p1, :cond_1

    goto :goto_1

    .line 108
    :cond_1
    new-instance v0, Lcom/pspdfkit/internal/jt;

    int-to-float p1, p1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xef

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/internal/jt;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Float;Ljava/lang/Integer;I)V

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/contentediting/a;->e(Lcom/pspdfkit/internal/jt;)V

    :cond_2
    :goto_1
    return-void
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->q:Lcom/pspdfkit/internal/l6;

    if-eqz p1, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->n:Lcom/pspdfkit/internal/fl;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    :cond_0
    const/4 p1, 0x0

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->q:Lcom/pspdfkit/internal/l6;

    :cond_1
    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/jt;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->g()Ljava/lang/Float;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Number;->floatValue()F

    move-result p1

    .line 3
    invoke-interface {p0}, Lcom/pspdfkit/internal/h6;->getAvailableFontSizes()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->lastOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    int-to-float v1, v1

    cmpg-float p1, p1, v1

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 4
    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_2

    .line 5
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :cond_2
    return v0
.end method

.method public final c(Lcom/pspdfkit/internal/jt;)V
    .locals 8

    const-string v0, "currentStyleInfo"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->g()Ljava/lang/Float;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    const/4 v0, 0x1

    int-to-float v0, v0

    rem-float v1, p1, v0

    .line 83
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    add-float/2addr p1, v0

    goto :goto_0

    :cond_0
    float-to-double v0, p1

    .line 86
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float p1, v0

    :goto_0
    float-to-int p1, p1

    .line 87
    invoke-interface {p0}, Lcom/pspdfkit/internal/h6;->getAvailableFontSizes()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->lastOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    if-ge v0, p1, :cond_1

    goto :goto_1

    .line 88
    :cond_1
    new-instance v0, Lcom/pspdfkit/internal/jt;

    int-to-float p1, p1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xef

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/internal/jt;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Float;Ljava/lang/Integer;I)V

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/contentediting/a;->e(Lcom/pspdfkit/internal/jt;)V

    :cond_2
    :goto_1
    return-void
.end method

.method public final c(Z)V
    .locals 8

    .line 90
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pt;->k()Lcom/pspdfkit/internal/av;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->u:Lcom/pspdfkit/internal/views/contentediting/d;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/contentediting/d;->c()V

    .line 92
    invoke-virtual {v0}, Lcom/pspdfkit/internal/bv;->c()Lcom/pspdfkit/utils/PageRect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/utils/PageRect;->getPageRect()Landroid/graphics/RectF;

    move-result-object v1

    const-string v2, "updateInfo.pageRect.pageRect"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->v:Landroid/graphics/RectF;

    .line 93
    invoke-virtual {v0}, Lcom/pspdfkit/internal/av;->d()Ljava/lang/String;

    move-result-object v1

    .line 94
    new-instance v2, Lcom/pspdfkit/internal/e7;

    const-string v3, "text"

    .line 95
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v2, v1, v3}, Lcom/pspdfkit/internal/e7;-><init>(Ljava/lang/CharSequence;I)V

    .line 146
    invoke-virtual {v0}, Lcom/pspdfkit/internal/av;->g()Lcom/pspdfkit/internal/xg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xg;->a()Ljava/util/List;

    move-result-object v0

    .line 331
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/ah;

    .line 332
    invoke-virtual {v4}, Lcom/pspdfkit/internal/ah;->a()Ljava/util/List;

    move-result-object v4

    .line 511
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/ra;

    .line 512
    invoke-virtual {v5}, Lcom/pspdfkit/internal/ra;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    .line 514
    new-instance v6, Lcom/pspdfkit/internal/views/contentediting/c;

    invoke-direct {v6, p0}, Lcom/pspdfkit/internal/views/contentediting/c;-><init>(Lcom/pspdfkit/internal/views/contentediting/a;)V

    add-int/2addr v5, v3

    const/16 v7, 0x21

    .line 515
    invoke-virtual {v2, v6, v3, v5, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move v3, v5

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    .line 526
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setTextKeepState(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 528
    :cond_2
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 529
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt;->h()I

    move-result p1

    const/4 v0, 0x1

    if-gt p1, v0, :cond_3

    const/4 p1, 0x0

    goto :goto_2

    .line 532
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/pt;->b(I)I

    move-result p1

    .line 534
    :goto_2
    iget-object v2, p0, Lcom/pspdfkit/internal/views/contentediting/a;->u:Lcom/pspdfkit/internal/views/contentediting/d;

    invoke-virtual {v2, p1}, Lcom/pspdfkit/internal/views/contentediting/d;->b(I)Lcom/pspdfkit/internal/views/contentediting/d$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/contentediting/d$a;->b()I

    move-result p1

    .line 535
    iget v2, p0, Lcom/pspdfkit/internal/views/contentediting/a;->s:I

    iget v3, p0, Lcom/pspdfkit/internal/views/contentediting/a;->t:I

    if-gt p1, v3, :cond_4

    if-gt v2, p1, :cond_4

    const/4 v1, 0x1

    :cond_4
    if-eqz v1, :cond_5

    const/4 p1, 0x0

    goto :goto_3

    :cond_5
    const/4 p1, 0x1

    .line 536
    :goto_3
    invoke-virtual {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->setRotation(F)V

    return-void
.end method

.method public final d(Lcom/pspdfkit/internal/jt;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->g()Ljava/lang/Float;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Number;->floatValue()F

    move-result p1

    .line 2
    invoke-interface {p0}, Lcom/pspdfkit/internal/h6;->getAvailableFontSizes()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    int-to-float v1, v1

    cmpl-float p1, p1, v1

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 3
    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_2

    .line 4
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :cond_2
    return v0
.end method

.method public final e(Lcom/pspdfkit/internal/jt;)V
    .locals 10

    const-string v0, "styleInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Applying new style "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.ContentEditing"

    invoke-static {v3, v0, v2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pt;->k()Lcom/pspdfkit/internal/av;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/av;->h()Lcom/pspdfkit/internal/vq;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-static {v0}, Lcom/pspdfkit/internal/z6;->a(Lcom/pspdfkit/internal/pt;)Lcom/pspdfkit/internal/su;

    move-result-object v6

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->j:Lcom/pspdfkit/internal/a7;

    iget-object v3, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-interface {v0, v3, p1}, Lcom/pspdfkit/internal/a7;->a(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/jt;)Lcom/pspdfkit/internal/o6;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/o6;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/av;

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->m:Lcom/pspdfkit/internal/views/contentediting/a$a;

    iget-object v3, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-interface {v0, v3, p1, v1}, Lcom/pspdfkit/internal/views/contentediting/a$a;->a(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/av;Z)V

    .line 13
    invoke-static {p1}, Lcom/pspdfkit/internal/z6;->a(Lcom/pspdfkit/internal/av;)Lcom/pspdfkit/internal/su;

    move-result-object v7

    .line 17
    new-instance p1, Lcom/pspdfkit/internal/l6;

    iget v4, p0, Lcom/pspdfkit/internal/views/contentediting/a;->k:I

    iget-object v5, p0, Lcom/pspdfkit/internal/views/contentediting/a;->p:Ljava/util/UUID;

    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pt;->g()Lcom/pspdfkit/internal/cb;

    move-result-object v8

    move-object v3, p1

    invoke-direct/range {v3 .. v8}, Lcom/pspdfkit/internal/l6;-><init>(ILjava/util/UUID;Lcom/pspdfkit/internal/su;Lcom/pspdfkit/internal/su;Lcom/pspdfkit/internal/cb;)V

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->n:Lcom/pspdfkit/internal/fl;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    .line 19
    :cond_0
    iput-object v2, p0, Lcom/pspdfkit/internal/views/contentediting/a;->q:Lcom/pspdfkit/internal/l6;

    goto :goto_0

    .line 20
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pt;->k()Lcom/pspdfkit/internal/av;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-static {v1, p1}, Lcom/pspdfkit/internal/pt;->a(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/jt;)Lcom/pspdfkit/internal/fj;

    move-result-object v1

    .line 22
    iget-object v3, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/pt;->k()Lcom/pspdfkit/internal/av;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/av;->f()Lcom/pspdfkit/internal/d8;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/d8;->b()Lcom/pspdfkit/internal/eb;

    move-result-object v3

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->j()Z

    move-result p1

    if-nez p1, :cond_2

    move-object v2, v3

    .line 23
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt;->k()Lcom/pspdfkit/internal/av;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/av;->f()Lcom/pspdfkit/internal/d8;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/d8;->c()Lcom/pspdfkit/internal/jt;

    move-result-object p1

    .line 24
    new-instance v3, Lcom/pspdfkit/internal/d8;

    invoke-direct {v3, v1, v2, p1}, Lcom/pspdfkit/internal/d8;-><init>(Lcom/pspdfkit/internal/fj;Lcom/pspdfkit/internal/eb;Lcom/pspdfkit/internal/jt;)V

    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/av;->a(Lcom/pspdfkit/internal/d8;)V

    .line 29
    iget-object v4, p0, Lcom/pspdfkit/internal/views/contentediting/a;->m:Lcom/pspdfkit/internal/views/contentediting/a$a;

    iget-object v5, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/pt;->f()Lcom/pspdfkit/internal/jt;

    move-result-object v6

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    move-result v7

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v8

    const/4 v9, 0x1

    invoke-interface/range {v4 .. v9}, Lcom/pspdfkit/internal/views/contentediting/a$a;->a(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/jt;IIZ)V

    :goto_0
    return-void
.end method

.method public getAvailableFontSizes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->x:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method protected getBoundingBox()Landroid/graphics/RectF;
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/contentediting/a;->v:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    return-object v0
.end method

.method public final getEditor()Lcom/pspdfkit/internal/a7;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->j:Lcom/pspdfkit/internal/a7;

    return-object v0
.end method

.method public final getListener()Lcom/pspdfkit/internal/views/contentediting/a$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->m:Lcom/pspdfkit/internal/views/contentediting/a$a;

    return-object v0
.end method

.method public final getPageIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->k:I

    return v0
.end method

.method public final getTextBlock()Lcom/pspdfkit/internal/pt;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    return-object v0
.end method

.method public final getTextMetrics()Lcom/pspdfkit/internal/views/contentediting/d;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->u:Lcom/pspdfkit/internal/views/contentediting/d;

    return-object v0
.end method

.method public final getTextblockId()Ljava/util/UUID;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->p:Ljava/util/UUID;

    return-object v0
.end method

.method public final onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 1

    const-string v0, "outAttrs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/high16 v0, 0x12000000

    .line 1
    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 2
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object p1

    return-object p1
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-static {v0}, Lcom/pspdfkit/internal/z6;->a(Lcom/pspdfkit/internal/pt;)Lcom/pspdfkit/internal/su;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->r:Lcom/pspdfkit/internal/su;

    .line 2
    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public final onReceiveContent(Landroid/view/View;Landroidx/core/view/ContentInfoCompat;)Landroidx/core/view/ContentInfoCompat;
    .locals 6

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "payload"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/views/contentediting/a$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/views/contentediting/a$$ExternalSyntheticLambda0;-><init>()V

    invoke-virtual {p2, v0}, Landroidx/core/view/ContentInfoCompat;->partition(Landroidx/core/util/Predicate;)Landroid/util/Pair;

    move-result-object p2

    const-string v0, "payload.partition { it.uri != null }"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroidx/core/view/ContentInfoCompat;

    .line 3
    iget-object p2, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p2, Landroidx/core/view/ContentInfoCompat;

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {v0}, Landroidx/core/view/ContentInfoCompat;->getClip()Landroid/content/ClipData;

    move-result-object v0

    const-string v1, "uriContent.clip"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v2, v1}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v1

    .line 89
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v3, v1

    check-cast v3, Lkotlin/collections/IntIterator;

    invoke-virtual {v3}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v3

    .line 90
    invoke-virtual {v0, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "clip.getItemAt(i).uri"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 92
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Inserting "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " not supported"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v2, [Ljava/lang/Object;

    const-string v5, "PSPDFKit.ContentEditing"

    invoke-static {v5, v3, v4}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-object p2
.end method

.method protected final onSelectionChanged(II)V
    .locals 4

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;->onSelectionChanged(II)V

    .line 2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-direct {p0, v2, p1, p2}, Lcom/pspdfkit/internal/views/contentediting/a;->a(Lcom/pspdfkit/internal/pt;II)V

    .line 4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "new cursor position = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " - "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " ("

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, "ms)"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.ContentEditing"

    invoke-static {v0, p1, p2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final onTextContextMenuItem(I)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->l:Lcom/pspdfkit/internal/pt;

    invoke-static {v0}, Lcom/pspdfkit/internal/z6;->a(Lcom/pspdfkit/internal/pt;)Lcom/pspdfkit/internal/su;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/views/contentediting/a;->r:Lcom/pspdfkit/internal/su;

    .line 2
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->onTextContextMenuItem(I)Z

    move-result p1

    return p1
.end method

.method public final scrollTo(II)V
    .locals 0

    const/4 p1, 0x0

    .line 1
    invoke-super {p0, p1, p1}, Landroidx/appcompat/widget/AppCompatEditText;->scrollTo(II)V

    return-void
.end method

.method public setBold(Z)V
    .locals 8

    .line 1
    new-instance v7, Lcom/pspdfkit/internal/jt;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xfb

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/jt;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Float;Ljava/lang/Integer;I)V

    invoke-virtual {p0, v7}, Lcom/pspdfkit/internal/views/contentediting/a;->e(Lcom/pspdfkit/internal/jt;)V

    return-void
.end method

.method public setFaceName(Ljava/lang/String;)V
    .locals 8

    const-string v0, "faceName"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance v0, Lcom/pspdfkit/internal/jt;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xfe

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/internal/jt;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Float;Ljava/lang/Integer;I)V

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/contentediting/a;->e(Lcom/pspdfkit/internal/jt;)V

    return-void
.end method

.method public setFontColor(I)V
    .locals 8

    .line 1
    new-instance v7, Lcom/pspdfkit/internal/jt;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xdf

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/jt;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Float;Ljava/lang/Integer;I)V

    invoke-virtual {p0, v7}, Lcom/pspdfkit/internal/views/contentediting/a;->e(Lcom/pspdfkit/internal/jt;)V

    return-void
.end method

.method public setFontSize(F)V
    .locals 8

    .line 1
    new-instance v7, Lcom/pspdfkit/internal/jt;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xef

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/jt;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Float;Ljava/lang/Integer;I)V

    invoke-virtual {p0, v7}, Lcom/pspdfkit/internal/views/contentediting/a;->e(Lcom/pspdfkit/internal/jt;)V

    return-void
.end method

.method public setItalic(Z)V
    .locals 8

    .line 1
    new-instance v7, Lcom/pspdfkit/internal/jt;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xf7

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/jt;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Float;Ljava/lang/Integer;I)V

    invoke-virtual {p0, v7}, Lcom/pspdfkit/internal/views/contentediting/a;->e(Lcom/pspdfkit/internal/jt;)V

    return-void
.end method

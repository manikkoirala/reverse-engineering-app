.class public final Lcom/pspdfkit/internal/views/picker/a;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/picker/a$a;
    }
.end annotation


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:I

.field private e:Lcom/pspdfkit/internal/views/picker/a$a;

.field private f:Z

.field private g:I

.field private h:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static synthetic $r8$lambda$PEphd7Bdfd5YuGCYVK-mB4wZBkQ(Lcom/pspdfkit/internal/views/picker/a;ILandroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/views/picker/a;->a(ILandroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/pspdfkit/internal/views/picker/a;->d:I

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/views/picker/a;->a(Landroid/content/Context;Ljava/util/List;Z)V

    return-void
.end method

.method private a()V
    .locals 6

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/views/picker/a;->h:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 29
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_done:I

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/views/picker/a;->h:Landroid/graphics/drawable/Drawable;

    .line 31
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/picker/a;->f:Z

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    .line 32
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 33
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 34
    instance-of v3, v0, Landroid/widget/ImageView;

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Ljava/lang/Integer;

    if-eqz v3, :cond_2

    .line 35
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lcom/pspdfkit/internal/views/picker/a;->g:I

    if-ne v3, v4, :cond_1

    .line 36
    iget-object v3, p0, Lcom/pspdfkit/internal/views/picker/a;->h:Landroid/graphics/drawable/Drawable;

    .line 38
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v4, v5}, Lcom/pspdfkit/internal/s5;->a(Landroid/content/Context;I)I

    move-result v4

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    .line 39
    invoke-static {v3}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 40
    invoke-static {v3, v4}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 41
    invoke-static {v3, v5}, Landroidx/core/graphics/drawable/DrawableCompat;->setTintMode(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 42
    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 44
    :cond_1
    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 49
    :cond_3
    :goto_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 50
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 51
    instance-of v3, v0, Landroid/widget/ImageView;

    if-eqz v3, :cond_4

    .line 52
    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    return-void
.end method

.method private synthetic a(ILandroid/view/View;)V
    .locals 0

    .line 25
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/picker/a;->b(I)Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/pspdfkit/internal/views/picker/a;->e:Lcom/pspdfkit/internal/views/picker/a$a;

    if-eqz p2, :cond_0

    .line 26
    invoke-interface {p2, p0, p1}, Lcom/pspdfkit/internal/views/picker/a$a;->a(Lcom/pspdfkit/internal/views/picker/a;I)V

    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/util/List;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    .line 1
    iput-boolean p3, p0, Lcom/pspdfkit/internal/views/picker/a;->c:Z

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/views/picker/a;->b:Ljava/util/List;

    .line 3
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    .line 4
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 5
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 6
    new-instance v1, Lcom/pspdfkit/internal/q5;

    const/4 v2, 0x2

    .line 7
    invoke-direct {v1, p1, p3, v2}, Lcom/pspdfkit/internal/q5;-><init>(Landroid/content/Context;II)V

    .line 8
    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 10
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/16 v3, 0x42

    const/16 v4, 0xff

    .line 11
    invoke-static {v3, v4, v4, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v1, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 12
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 15
    new-instance v1, Lcom/pspdfkit/internal/views/picker/a$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p3}, Lcom/pspdfkit/internal/views/picker/a$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/picker/a;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x1

    .line 20
    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 21
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {v0, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 22
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 24
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/picker/a;->a()V

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 4

    .line 53
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/picker/a;->c:Z

    add-int/lit8 p1, p1, -0xa

    if-eqz v0, :cond_0

    int-to-double v0, p1

    const-wide/high16 v2, 0x4016000000000000L    # 5.5

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    sub-double/2addr v0, v2

    double-to-int p1, v0

    goto :goto_0

    .line 55
    :cond_0
    div-int/lit8 p1, p1, 0x5

    add-int/lit8 p1, p1, -0xa

    :goto_0
    return p1
.end method

.method public final b(I)Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/views/picker/a;->g:I

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 2
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/views/picker/a;->g:I

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/picker/a;->a()V

    const/4 p1, 0x1

    return p1
.end method

.method getAvailableColors()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/picker/a;->b:Ljava/util/List;

    return-object v0
.end method

.method protected final onLayout(ZIIII)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    const/4 p2, 0x0

    :goto_0
    if-ge p2, p1, :cond_1

    .line 2
    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p3

    .line 3
    iget-boolean p4, p0, Lcom/pspdfkit/internal/views/picker/a;->c:Z

    const/16 p5, 0xa

    if-eqz p4, :cond_0

    .line 4
    invoke-virtual {p3}, Landroid/view/View;->getMeasuredWidth()I

    move-result p4

    add-int/2addr p4, p5

    mul-int p4, p4, p2

    add-int/2addr p4, p5

    goto :goto_1

    .line 7
    :cond_0
    div-int/lit8 p4, p2, 0x5

    .line 8
    rem-int/lit8 v0, p2, 0x5

    .line 9
    invoke-virtual {p3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, p5

    mul-int v1, v1, v0

    add-int/lit8 v0, v1, 0xa

    .line 10
    invoke-virtual {p3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, p5

    mul-int v1, v1, p4

    add-int/2addr p5, v1

    move p4, v0

    .line 13
    :goto_1
    invoke-virtual {p3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, p4

    invoke-virtual {p3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, p5

    invoke-virtual {p3, p4, p5, v0, v1}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 2

    const/4 p2, 0x0

    .line 1
    invoke-static {p2, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result p1

    .line 2
    iget p2, p0, Lcom/pspdfkit/internal/views/picker/a;->d:I

    if-nez p2, :cond_0

    .line 6
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/picker/a;->a(I)I

    move-result p2

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    .line 9
    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 10
    invoke-virtual {p0, v0, v0}, Landroid/view/ViewGroup;->measureChildren(II)V

    .line 12
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/picker/a;->c:Z

    if-eqz v0, :cond_1

    add-int/lit8 p1, p2, 0xa

    .line 14
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    mul-int v0, v0, p1

    add-int/lit8 v0, v0, 0xa

    add-int/lit8 p2, p2, 0x14

    .line 15
    invoke-virtual {p0, v0, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    goto :goto_0

    .line 19
    :cond_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40a00000    # 5.0f

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    add-int/lit8 p2, p2, 0xa

    mul-int p2, p2, v0

    add-int/lit8 p2, p2, 0xa

    .line 20
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    :goto_0
    return-void
.end method

.method setBlockWidthDimension(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/views/picker/a;->d:I

    return-void
.end method

.method public setOnColorPickedListener(Lcom/pspdfkit/internal/views/picker/a$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/picker/a;->e:Lcom/pspdfkit/internal/views/picker/a$a;

    return-void
.end method

.method public setShowSelectionIndicator(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/picker/a;->f:Z

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/picker/a;->a()V

    return-void
.end method

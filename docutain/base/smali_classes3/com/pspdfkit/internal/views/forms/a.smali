.class public final Lcom/pspdfkit/internal/views/forms/a;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/xb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Lcom/pspdfkit/internal/xb<",
        "Lcom/pspdfkit/forms/FormElement;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/jp;

.field private final c:Lcom/pspdfkit/internal/views/forms/c;

.field private d:Lcom/pspdfkit/forms/FormElement;


# direct methods
.method public static synthetic $r8$lambda$qMcTsdP5Dk9wm7YRXyaG4Y6IX6M(Lcom/pspdfkit/internal/views/forms/a;)Ljava/lang/Boolean;
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/views/forms/a;->b()Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/zf;ILcom/pspdfkit/internal/views/forms/c$a;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/jp;

    invoke-direct {v0, p1, p2, p3}, Lcom/pspdfkit/internal/jp;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/zf;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->b:Lcom/pspdfkit/internal/jp;

    .line 4
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 6
    new-instance p2, Lcom/pspdfkit/internal/views/forms/c;

    invoke-direct {p2, p1, p4, p5}, Lcom/pspdfkit/internal/views/forms/c;-><init>(Landroid/content/Context;ILcom/pspdfkit/internal/views/forms/c$a;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/forms/a;->c:Lcom/pspdfkit/internal/views/forms/c;

    .line 7
    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method private synthetic b()Ljava/lang/Boolean;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/forms/a;->setHighlightEnabled(Z)V

    .line 2
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final d()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->b:Lcom/pspdfkit/internal/jp;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->c:Lcom/pspdfkit/internal/views/forms/c;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/forms/c;->d()V

    const/4 v0, 0x0

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/forms/a;->setHighlightEnabled(Z)V

    return-void
.end method

.method public getFormElement()Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->d:Lcom/pspdfkit/forms/FormElement;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->b:Lcom/pspdfkit/internal/jp;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/j;->b()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->c:Lcom/pspdfkit/internal/views/forms/c;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    return-void
.end method

.method public final j()Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/views/forms/a$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/views/forms/a$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/forms/a;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 5
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public final n()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->b:Lcom/pspdfkit/internal/jp;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->c:Lcom/pspdfkit/internal/views/forms/c;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/forms/c;->n()V

    return-void
.end method

.method public final onChangeFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->b:Lcom/pspdfkit/internal/jp;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->c:Lcom/pspdfkit/internal/views/forms/c;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/c;->onChangeFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    return-void
.end method

.method public final onEnterFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->b:Lcom/pspdfkit/internal/jp;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->c:Lcom/pspdfkit/internal/views/forms/c;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/c;->onEnterFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    return-void
.end method

.method public final onExitFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->b:Lcom/pspdfkit/internal/jp;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->c:Lcom/pspdfkit/internal/views/forms/c;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/c;->onExitFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    return-void
.end method

.method public setFormElement(Lcom/pspdfkit/forms/FormElement;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->d:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/forms/FormElement;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/a;->d:Lcom/pspdfkit/forms/FormElement;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->b:Lcom/pspdfkit/internal/jp;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jp;->setFormElement(Lcom/pspdfkit/forms/FormElement;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->c:Lcom/pspdfkit/internal/views/forms/c;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/c;->setFormElement(Lcom/pspdfkit/forms/FormElement;)V

    .line 7
    new-instance v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p1

    sget-object v1, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;->LAYOUT:Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;

    invoke-direct {v0, p1, v1}, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;-><init>(Landroid/graphics/RectF;Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;)V

    .line 9
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/a;->b:Lcom/pspdfkit/internal/jp;

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/a;->c:Lcom/pspdfkit/internal/views/forms/c;

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setHighlightEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/a;->c:Lcom/pspdfkit/internal/views/forms/c;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

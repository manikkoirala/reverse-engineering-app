.class public final Lcom/pspdfkit/internal/views/forms/e;
.super Lcom/pspdfkit/internal/views/annotations/d;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/xb;
.implements Lcom/pspdfkit/ui/special_mode/controller/FormElementViewController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/forms/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/views/annotations/d;",
        "Lcom/pspdfkit/internal/xb<",
        "Lcom/pspdfkit/forms/TextFormElement;",
        ">;",
        "Lcom/pspdfkit/ui/special_mode/controller/FormElementViewController;"
    }
.end annotation


# instance fields
.field private A:Ljava/lang/String;

.field private B:Lio/reactivex/rxjava3/disposables/Disposable;

.field private C:Z

.field private D:F

.field private E:F

.field private final F:Landroidx/collection/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/LruCache<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final j:I

.field private final k:Z

.field private final l:Z

.field private final m:I

.field private final n:Lcom/pspdfkit/internal/ic;

.field private final o:Lcom/pspdfkit/internal/views/forms/b;

.field private final p:Landroid/graphics/drawable/ShapeDrawable;

.field private q:Lcom/pspdfkit/forms/TextFormElement;

.field private r:Landroid/graphics/drawable/ColorDrawable;

.field private final s:Landroid/graphics/drawable/Drawable;

.field private t:Ljava/lang/Runnable;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

.field private x:F

.field private final y:Lcom/pspdfkit/internal/yb;

.field private final z:Lcom/pspdfkit/internal/views/forms/e$a;


# direct methods
.method public static synthetic $r8$lambda$GEB04NO-ttBYDF83uDvftNpRM4I(Lcom/pspdfkit/internal/views/forms/e;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/forms/e;->a(Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$LC2zYHzUChx34CACcPL8vrK0HAY(Lcom/pspdfkit/internal/views/forms/e;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/views/forms/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method

.method public static synthetic $r8$lambda$ZOJzjYOuaxctCKV004d2J_cNqlA(Lcom/pspdfkit/internal/views/forms/e;Ljava/lang/CharSequence;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/forms/e;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static synthetic $r8$lambda$gwX6gkBaDxFYqyDxF4TFHKNmSw4(Lcom/pspdfkit/internal/views/forms/e;Lcom/pspdfkit/forms/TextFormElement;Lcom/pspdfkit/annotations/Annotation;ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/pspdfkit/internal/views/forms/e;->a(Lcom/pspdfkit/forms/TextFormElement;Lcom/pspdfkit/annotations/Annotation;ILjava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic $r8$lambda$i6cVwoY7KExDeFCbM2MWaLpPqBk(Lcom/pspdfkit/internal/views/forms/e;Ljava/lang/Boolean;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/forms/e;->b(Ljava/lang/Boolean;)V

    return-void
.end method

.method public static synthetic $r8$lambda$v1pPHWcZCt333au-knQ38IJ65ZI(Lcom/pspdfkit/internal/views/forms/e;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/views/forms/e;->r()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/ic;ILcom/pspdfkit/internal/yb;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/annotations/d;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Lcom/pspdfkit/internal/views/forms/b;

    invoke-direct {p1}, Lcom/pspdfkit/internal/views/forms/b;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->o:Lcom/pspdfkit/internal/views/forms/b;

    const/4 p1, 0x0

    .line 30
    iput p1, p0, Lcom/pspdfkit/internal/views/forms/e;->x:F

    .line 36
    new-instance p1, Lcom/pspdfkit/internal/views/forms/e$a;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/pspdfkit/internal/views/forms/e$a;-><init>(Lcom/pspdfkit/internal/views/forms/e;Lcom/pspdfkit/internal/views/forms/e$a-IA;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->z:Lcom/pspdfkit/internal/views/forms/e$a;

    const/4 p1, 0x1

    .line 48
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/forms/e;->C:Z

    .line 55
    new-instance p1, Landroidx/collection/LruCache;

    const/16 v0, 0x19

    invoke-direct {p1, v0}, Landroidx/collection/LruCache;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->F:Landroidx/collection/LruCache;

    .line 75
    iput-object p5, p0, Lcom/pspdfkit/internal/views/forms/e;->y:Lcom/pspdfkit/internal/yb;

    .line 77
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getBackgroundColor()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/views/forms/e;->j:I

    .line 78
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/forms/e;->k:Z

    .line 79
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/forms/e;->l:Z

    .line 80
    iput-object p3, p0, Lcom/pspdfkit/internal/views/forms/e;->n:Lcom/pspdfkit/internal/ic;

    .line 81
    iput p4, p0, Lcom/pspdfkit/internal/views/forms/e;->m:I

    .line 83
    new-instance p1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance p2, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {p2}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {p1, p2}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->p:Landroid/graphics/drawable/ShapeDrawable;

    .line 86
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/pspdfkit/R$drawable;->pspdf__ic_input_error:I

    .line 88
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    sget p4, Lcom/pspdfkit/R$color;->pspdf__color_error:I

    invoke-static {p3, p4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p3

    .line 89
    invoke-static {p1, p2, p3}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->s:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method private a(Lcom/pspdfkit/forms/TextFormElement;Ljava/lang/String;)F
    .locals 9

    .line 30
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFontSize()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    return v0

    .line 38
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    neg-float v1, v1

    const/high16 v2, 0x40800000    # 4.0f

    sub-float v6, v1, v2

    .line 40
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    sub-float v5, v0, v2

    .line 45
    iget v0, p0, Lcom/pspdfkit/internal/views/forms/e;->D:F

    const/4 v1, 0x1

    cmpl-float v0, v0, v5

    if-nez v0, :cond_1

    iget v0, p0, Lcom/pspdfkit/internal/views/forms/e;->E:F

    cmpl-float v0, v0, v6

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 46
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/views/forms/e;->F:Landroidx/collection/LruCache;

    invoke-virtual {v2, p2}, Landroidx/collection/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    if-eqz v2, :cond_2

    .line 48
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-float p1, p1

    return p1

    :cond_2
    if-nez v0, :cond_3

    .line 53
    iput v5, p0, Lcom/pspdfkit/internal/views/forms/e;->D:F

    .line 54
    iput v6, p0, Lcom/pspdfkit/internal/views/forms/e;->E:F

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->F:Landroidx/collection/LruCache;

    invoke-virtual {v0}, Landroidx/collection/LruCache;->evictAll()V

    .line 62
    :cond_3
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    .line 65
    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->isMultiLine()Z

    move-result v7

    .line 70
    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->isScrollEnabled()Z

    move-result p1

    xor-int/lit8 v8, p1, 0x1

    move-object v3, p2

    .line 71
    invoke-static/range {v3 .. v8}, Lcom/pspdfkit/internal/vt;->a(Ljava/lang/String;Landroid/graphics/Paint;FFZZ)F

    move-result p1

    .line 84
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->F:Landroidx/collection/LruCache;

    float-to-int v1, p1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Landroidx/collection/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return p1
.end method

.method private a(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->getText()Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Lcom/pspdfkit/internal/views/forms/e;->v:Ljava/lang/String;

    sget v3, Lcom/pspdfkit/internal/ft;->d:I

    if-nez p1, :cond_0

    if-nez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_1

    :cond_0
    if-eqz p1, :cond_2

    if-nez v2, :cond_1

    goto :goto_0

    .line 12
    :cond_1
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    :goto_1
    if-nez p1, :cond_3

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    .line 13
    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Lcom/pspdfkit/forms/TextFormElement;Lcom/pspdfkit/annotations/Annotation;ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    const/16 p2, 0x3ea

    if-ne p3, p2, :cond_0

    if-eq p5, p4, :cond_0

    if-eqz p5, :cond_0

    .line 1
    move-object p2, p5

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/views/forms/e;->setViewTextSizeFromAnnotationFontSize(F)V

    :cond_0
    const/16 p2, 0x3ee

    if-ne p3, p2, :cond_1

    if-eq p5, p4, :cond_1

    if-eqz p5, :cond_1

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->isMultiLine()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 10
    check-cast p5, Lcom/pspdfkit/annotations/VerticalTextAlignment;

    invoke-static {p5}, Lcom/pspdfkit/internal/a4;->a(Lcom/pspdfkit/annotations/VerticalTextAlignment;)I

    move-result p1

    const p2, 0x800003

    or-int/2addr p1, p2

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setGravity(I)V

    :cond_1
    return-void
.end method

.method private synthetic a(Ljava/lang/CharSequence;)V
    .locals 0

    .line 24
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/forms/e;->b(Ljava/lang/String;)V

    return-void
.end method

.method private synthetic a(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 14
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 15
    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->u:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private b()V
    .locals 7

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    if-nez v0, :cond_0

    return-void

    .line 33
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/views/forms/e;->n:Lcom/pspdfkit/internal/ic;

    iget v1, v1, Lcom/pspdfkit/internal/ic;->d:I

    invoke-static {v1}, Lcom/pspdfkit/internal/s5;->c(I)I

    move-result v1

    .line 34
    iget-boolean v2, p0, Lcom/pspdfkit/internal/views/forms/e;->l:Z

    iget-boolean v3, p0, Lcom/pspdfkit/internal/views/forms/e;->k:Z

    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/ga;->a(IZZ)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 38
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFontSize()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/views/forms/e;->setViewTextSizeFromAnnotationFontSize(F)V

    .line 43
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->getPdfToViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    const/high16 v2, 0x3fc00000    # 1.5f

    .line 44
    invoke-static {v2, v1}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 46
    invoke-virtual {v0}, Lcom/pspdfkit/forms/TextFormElement;->isMultiLine()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 48
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v2

    .line 51
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getVerticalTextAlignment()Lcom/pspdfkit/annotations/VerticalTextAlignment;

    move-result-object v5

    .line 52
    invoke-static {v5}, Lcom/pspdfkit/internal/a4;->a(Lcom/pspdfkit/annotations/VerticalTextAlignment;)I

    move-result v5

    const v6, 0x800003

    or-int/2addr v5, v6

    .line 53
    invoke-virtual {p0, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 56
    invoke-virtual {p0, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    if-eqz v2, :cond_2

    .line 58
    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getLineHeightFactor()Ljava/lang/Float;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 60
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p0, v3, v1}, Landroid/widget/TextView;->setLineSpacing(FF)V

    goto :goto_0

    :cond_1
    const/16 v2, 0x10

    .line 65
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 66
    invoke-virtual {p0, v1, v4, v1, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 69
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/views/forms/e;->p:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 70
    iget-object v1, p0, Lcom/pspdfkit/internal/views/forms/e;->p:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    .line 71
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 72
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->getPdfToViewMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v5, v2}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/views/forms/e;->x:F

    .line 73
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 76
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->isRequired()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 77
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->n:Lcom/pspdfkit/internal/ic;

    iget v4, v0, Lcom/pspdfkit/internal/ic;->f:I

    goto :goto_1

    .line 78
    :cond_3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 79
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->n:Lcom/pspdfkit/internal/ic;

    iget v4, v0, Lcom/pspdfkit/internal/ic;->e:I

    goto :goto_1

    .line 81
    :cond_4
    iput v3, p0, Lcom/pspdfkit/internal/views/forms/e;->x:F

    .line 83
    :goto_1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/forms/e;->l:Z

    iget-boolean v2, p0, Lcom/pspdfkit/internal/views/forms/e;->k:Z

    invoke-static {v4, v0, v2}, Lcom/pspdfkit/internal/ga;->a(IZZ)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    :cond_5
    return-void
.end method

.method private synthetic b(Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->z:Lcom/pspdfkit/internal/views/forms/e$a;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/views/forms/e$a;->-$$Nest$fputa(Lcom/pspdfkit/internal/views/forms/e$a;Ljava/lang/String;)V

    .line 4
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/forms/e;->setErrorMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 7
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->z:Lcom/pspdfkit/internal/views/forms/e$a;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/forms/e$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/views/forms/e$a;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 8
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/forms/e;->setErrorMessage(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->B:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 10
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 11
    iput-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->B:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/pspdfkit/forms/TextFormElement;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 13
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/forms/e;->c(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 14
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/views/forms/e$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/views/forms/e$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/views/forms/e;)V

    .line 15
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->B:Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_0

    .line 28
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->z:Lcom/pspdfkit/internal/views/forms/e$a;

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/views/forms/e$a;->-$$Nest$fputa(Lcom/pspdfkit/internal/views/forms/e$a;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/forms/e;->setErrorMessage(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private c(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/forms/TextFormElement;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/vb;->a(Lcom/pspdfkit/forms/TextFormElement;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/views/forms/e$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/views/forms/e$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/views/forms/e;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    .line 8
    :cond_0
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method private r()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/forms/e;->l()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->B:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->B:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 5
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/forms/e;->setErrorMessage(Ljava/lang/String;)V

    return-void
.end method

.method private setErrorMessage(Ljava/lang/String;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->A:Ljava/lang/String;

    sget v1, Lcom/pspdfkit/internal/ft;->d:I

    const/4 v1, 0x0

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    if-eqz v0, :cond_2

    if-nez p1, :cond_1

    goto :goto_0

    .line 2
    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    return-void

    .line 3
    :cond_3
    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->A:Ljava/lang/String;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->s:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v0, v3

    .line 6
    iget-object v3, p0, Lcom/pspdfkit/internal/views/forms/e;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v1, v1, v0, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v2, v2, v0, v2}, Landroidx/appcompat/widget/AppCompatEditText;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 9
    :cond_4
    invoke-virtual {p0, v2, v2, v2, v2}, Landroidx/appcompat/widget/AppCompatEditText;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 10
    :goto_2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    if-nez v0, :cond_5

    return-void

    :cond_5
    if-eqz p1, :cond_6

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/views/forms/e;->y:Lcom/pspdfkit/internal/yb;

    check-cast v1, Lcom/pspdfkit/internal/ac;

    invoke-virtual {v1, v0, p1}, Lcom/pspdfkit/internal/ac;->a(Lcom/pspdfkit/forms/TextFormElement;Ljava/lang/String;)V

    goto :goto_3

    .line 14
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->y:Lcom/pspdfkit/internal/yb;

    check-cast p1, Lcom/pspdfkit/internal/ac;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ac;->a(Lcom/pspdfkit/forms/TextFormElement;)V

    :goto_3
    return-void
.end method

.method private setSuppressJavaScriptAlerts(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->z:Lcom/pspdfkit/internal/views/forms/e$a;

    check-cast p1, Lcom/pspdfkit/internal/ig;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ig;->a(Lcom/pspdfkit/internal/lg;)V

    goto :goto_0

    .line 8
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->z:Lcom/pspdfkit/internal/views/forms/e$a;

    check-cast p1, Lcom/pspdfkit/internal/ig;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ig;->b(Lcom/pspdfkit/internal/lg;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private setUpWidgetAnnotationObserver(Lcom/pspdfkit/forms/TextFormElement;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/views/forms/e$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/views/forms/e$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/views/forms/e;Lcom/pspdfkit/forms/TextFormElement;)V

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->addOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    return-void
.end method

.method private setViewTextSizeFromAnnotationFontSize(F)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-nez v1, :cond_2

    .line 2
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 3
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const-string p1, ""

    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/views/forms/e;->a(Lcom/pspdfkit/forms/TextFormElement;Ljava/lang/String;)F

    move-result p1

    .line 4
    :cond_2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->getPdfToViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result p1

    const/4 v0, 0x0

    .line 5
    invoke-virtual {p0, v0, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;)F
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    if-eqz v0, :cond_0

    .line 26
    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/views/forms/e;->a(Lcom/pspdfkit/forms/TextFormElement;Ljava/lang/String;)F

    move-result p1

    .line 27
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/d;->getPdfToViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result p1

    return p1

    .line 29
    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getTextSize()F

    move-result p1

    return p1
.end method

.method public final a()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final a(FLandroid/graphics/Matrix;)V
    .locals 2

    .line 16
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/internal/views/annotations/d;->a(FLandroid/graphics/Matrix;)V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->s:Landroid/graphics/drawable/Drawable;

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->A:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    sub-int/2addr p1, v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    sub-int/2addr p1, v0

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->s:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, p1, p1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p2, p2, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 22
    :cond_0
    invoke-virtual {p0, p2, p2, p2, p2}, Landroidx/appcompat/widget/AppCompatEditText;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 23
    :goto_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/forms/e;->b()V

    return-void
.end method

.method public final c()V
    .locals 7

    .line 9
    invoke-super {p0}, Lcom/pspdfkit/internal/views/annotations/d;->c()V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->n:Lcom/pspdfkit/internal/ic;

    iget v0, v0, Lcom/pspdfkit/internal/ic;->d:I

    if-nez v0, :cond_0

    return-void

    .line 17
    :cond_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/forms/e;->n:Lcom/pspdfkit/internal/ic;

    iget v1, v1, Lcom/pspdfkit/internal/ic;->d:I

    iget-boolean v2, p0, Lcom/pspdfkit/internal/views/forms/e;->l:Z

    iget-boolean v3, p0, Lcom/pspdfkit/internal/views/forms/e;->k:Z

    .line 18
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/ga;->a(IZZ)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/internal/views/forms/e;->n:Lcom/pspdfkit/internal/ic;

    iget v1, v1, Lcom/pspdfkit/internal/ic;->d:I

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    const/16 v2, 0xff

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ne v1, v2, :cond_1

    .line 22
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-boolean v2, p0, Lcom/pspdfkit/internal/views/forms/e;->l:Z

    iget-boolean v5, p0, Lcom/pspdfkit/internal/views/forms/e;->k:Z

    const/4 v6, -0x1

    .line 23
    invoke-static {v6, v2, v5}, Lcom/pspdfkit/internal/ga;->a(IZZ)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 24
    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/graphics/drawable/Drawable;

    aput-object v1, v5, v3

    aput-object v0, v5, v4

    invoke-direct {v2, v5}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 31
    :cond_1
    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    new-array v1, v4, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v1, v3

    invoke-direct {v2, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 34
    :goto_0
    invoke-static {p0, v2}, Landroidx/core/view/ViewCompat;->setBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final canClearFormField()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final clearFormField()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/forms/e;->canClearFormField()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    .line 2
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/forms/e;->b(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final d()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/forms/e;->l()V

    const/4 v0, 0x0

    .line 4
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/forms/e;->setSuppressJavaScriptAlerts(Z)V

    return-void
.end method

.method protected getBoundingBox()Landroid/graphics/RectF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    :goto_0
    return-object v0
.end method

.method public bridge synthetic getFormElement()Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/forms/e;->getFormElement()Lcom/pspdfkit/forms/TextFormElement;

    move-result-object v0

    return-object v0
.end method

.method public getFormElement()Lcom/pspdfkit/forms/TextFormElement;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    return-object v0
.end method

.method public final h()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/forms/TextFormElement;->getText()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 5
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/views/forms/e;->u:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 6
    :cond_2
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextKeepState(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method

.method public final j()Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/views/forms/e$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/views/forms/e$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/forms/e;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 9
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 10
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/views/forms/e;->c(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/views/forms/e$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/views/forms/e$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/views/forms/e;)V

    .line 11
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    .line 12
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/SingleSource;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public final l()V
    .locals 4

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/views/annotations/d;->l()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->t:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {p0, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    .line 6
    iput-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->t:Ljava/lang/Runnable;

    .line 10
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/views/forms/e;->j:I

    iget-boolean v1, p0, Lcom/pspdfkit/internal/views/forms/e;->l:Z

    iget-boolean v2, p0, Lcom/pspdfkit/internal/views/forms/e;->k:Z

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ga;->a(IZZ)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 13
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/forms/e;->n:Lcom/pspdfkit/internal/ic;

    iget v1, v1, Lcom/pspdfkit/internal/ic;->a:I

    iget-boolean v2, p0, Lcom/pspdfkit/internal/views/forms/e;->l:Z

    iget-boolean v3, p0, Lcom/pspdfkit/internal/views/forms/e;->k:Z

    .line 14
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/ga;->a(IZZ)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->r:Landroid/graphics/drawable/ColorDrawable;

    .line 16
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/forms/e;->b()V

    return-void
.end method

.method public final n()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/forms/e;->c()V

    const/4 v0, 0x1

    .line 5
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/forms/e;->setSuppressJavaScriptAlerts(Z)V

    return-void
.end method

.method public final onChangeFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->w:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    .line 2
    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->bindFormElementViewController(Lcom/pspdfkit/ui/special_mode/controller/FormElementViewController;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->o:Lcom/pspdfkit/internal/views/forms/b;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/b;->onChangeFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .line 1
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->r:Landroid/graphics/drawable/ColorDrawable;

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v5, v3

    .line 6
    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->r:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 11
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/views/forms/e;->x:F

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 15
    iget-object v1, p0, Lcom/pspdfkit/internal/views/forms/e;->p:Landroid/graphics/drawable/ShapeDrawable;

    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v2

    sub-int/2addr v2, v0

    .line 17
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v3

    sub-int/2addr v3, v0

    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v4

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v5, v4

    add-int/2addr v5, v0

    .line 19
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v4

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v6, v4

    add-int/2addr v6, v0

    .line 20
    invoke-virtual {v1, v2, v3, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->p:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    return-void
.end method

.method public final onEditorAction(I)V
    .locals 1

    const/4 v0, 0x6

    if-ne p1, v0, :cond_1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->w:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    if-eqz v0, :cond_1

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAutoSelectNextFormElementEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->w:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->hasNextElement()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->w:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->selectNextFormElement()Z

    goto :goto_0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->w:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->finishEditing()Z

    .line 9
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->onEditorAction(I)V

    return-void
.end method

.method public final onEnterFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->w:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    .line 2
    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->bindFormElementViewController(Lcom/pspdfkit/ui/special_mode/controller/FormElementViewController;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->o:Lcom/pspdfkit/internal/views/forms/b;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/b;->onEnterFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    return-void
.end method

.method public final onExitFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->w:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->unbindFormElementViewController()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->w:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->o:Lcom/pspdfkit/internal/views/forms/b;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/b;->onExitFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->o:Lcom/pspdfkit/internal/views/forms/b;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/views/forms/b;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->o:Lcom/pspdfkit/internal/views/forms/b;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/views/forms/b;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/views/annotations/d;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    if-eqz p2, :cond_2

    .line 4
    iget-object p3, p0, Lcom/pspdfkit/internal/views/forms/e;->y:Lcom/pspdfkit/internal/yb;

    check-cast p3, Lcom/pspdfkit/internal/ac;

    invoke-virtual {p3, p2}, Lcom/pspdfkit/internal/ac;->b(Lcom/pspdfkit/forms/TextFormElement;)V

    .line 6
    iget-object p3, p0, Lcom/pspdfkit/internal/views/forms/e;->t:Ljava/lang/Runnable;

    if-eqz p3, :cond_0

    .line 7
    invoke-virtual {p0, p3}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 9
    :cond_0
    new-instance p3, Lcom/pspdfkit/internal/views/forms/e$$ExternalSyntheticLambda2;

    invoke-direct {p3, p0, p1}, Lcom/pspdfkit/internal/views/forms/e$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/views/forms/e;Ljava/lang/CharSequence;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/views/forms/e;->t:Ljava/lang/Runnable;

    const-wide/16 v0, 0x1f4

    .line 10
    invoke-virtual {p0, p3, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    if-eqz p1, :cond_1

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFontSize()F

    move-result p1

    const/4 p3, 0x0

    cmpl-float p1, p1, p3

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    .line 13
    invoke-virtual {p2}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFontSize()F

    move-result p1

    .line 14
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/forms/e;->setViewTextSizeFromAnnotationFontSize(F)V

    :cond_2
    return-void
.end method

.method public final scrollTo(II)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/forms/e;->C:Z

    if-nez v0, :cond_0

    const/4 p1, 0x0

    .line 2
    invoke-super {p0, p1, p1}, Landroidx/appcompat/widget/AppCompatEditText;->scrollTo(II)V

    goto :goto_0

    .line 4
    :cond_0
    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;->scrollTo(II)V

    :goto_0
    return-void
.end method

.method public bridge synthetic setFormElement(Lcom/pspdfkit/forms/FormElement;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/forms/TextFormElement;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/forms/e;->setFormElement(Lcom/pspdfkit/forms/TextFormElement;)V

    return-void
.end method

.method public setFormElement(Lcom/pspdfkit/forms/TextFormElement;)V
    .locals 6

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/e;->q:Lcom/pspdfkit/forms/TextFormElement;

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->o:Lcom/pspdfkit/internal/views/forms/b;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/b;->a(Lcom/pspdfkit/forms/FormElement;)V

    .line 6
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/forms/e;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    .line 8
    iget v2, p0, Lcom/pspdfkit/internal/views/forms/e;->m:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 9
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 12
    iget v0, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, v1, Landroid/graphics/RectF;->top:F

    cmpl-float v3, v0, v2

    if-lez v3, :cond_0

    .line 14
    iput v0, v1, Landroid/graphics/RectF;->top:F

    .line 15
    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 16
    :cond_0
    new-instance v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    sget-object v2, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;->LAYOUT:Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;-><init>(Landroid/graphics/RectF;Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 18
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result v0

    .line 19
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 20
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/forms/e;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v3

    int-to-float v0, v0

    .line 21
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-virtual {v1, v0, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 22
    invoke-virtual {v1, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 25
    iget v1, v3, Landroid/graphics/RectF;->bottom:F

    iget v4, v3, Landroid/graphics/RectF;->top:F

    cmpl-float v5, v1, v4

    if-lez v5, :cond_1

    .line 27
    iput v1, v3, Landroid/graphics/RectF;->top:F

    .line 28
    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 29
    :cond_1
    new-instance v1, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    invoke-direct {v1, v3, v2}, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;-><init>(Landroid/graphics/RectF;Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;)V

    invoke-virtual {p0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 32
    invoke-virtual {p0, v0}, Landroidx/appcompat/widget/AppCompatEditText;->setRotation(F)V

    .line 33
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    .line 34
    sget-object v1, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->FIELD_FORMAT:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getAdditionalAction(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 35
    :cond_2
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 38
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object v0

    .line 39
    check-cast v0, Lcom/pspdfkit/internal/ig;

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/ig;->a(Lcom/pspdfkit/forms/FormElement;Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)Z

    .line 40
    :cond_3
    :goto_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/forms/e;->setUpWidgetAnnotationObserver(Lcom/pspdfkit/forms/TextFormElement;)V

    .line 43
    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->getEditingContents()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->getEditingContents()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->getText()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->v:Ljava/lang/String;

    .line 44
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->isScrollEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/forms/e;->C:Z

    const/4 v0, 0x0

    .line 50
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 53
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 54
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wb;->a(Lcom/pspdfkit/forms/TextFormElement;Landroid/content/ContentResolver;)I

    move-result v0

    invoke-virtual {p0, v0}, Landroidx/appcompat/widget/AppCompatEditText;->setInputType(I)V

    .line 56
    invoke-virtual {p0}, Landroidx/appcompat/widget/AppCompatEditText;->getInputType()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    const-string v0, "0123456789,.-"

    .line 60
    invoke-static {v0}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 63
    :cond_5
    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->isMultiLine()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 64
    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->isMultiLine()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x6

    .line 65
    invoke-virtual {p0, v0}, Landroidx/appcompat/widget/AppCompatEditText;->setImeOptions(I)V

    .line 68
    :cond_6
    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->isPassword()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 70
    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 74
    :cond_7
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 75
    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->getMaxLength()I

    move-result v1

    if-eqz v1, :cond_8

    .line 76
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->getMaxLength()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    :cond_8
    iget-boolean v1, p0, Lcom/pspdfkit/internal/views/forms/e;->C:Z

    if-nez v1, :cond_9

    .line 79
    new-instance v1, Lcom/pspdfkit/internal/views/forms/d;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/views/forms/d;-><init>(Lcom/pspdfkit/internal/views/forms/e;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    :cond_9
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 83
    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ig;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ig;->isJavaScriptEnabled()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 84
    new-instance v1, Lcom/pspdfkit/internal/wt;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/wt;-><init>(Lcom/pspdfkit/forms/TextFormElement;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    const/4 p1, 0x0

    new-array p1, p1, [Landroid/text/InputFilter;

    .line 86
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Landroid/text/InputFilter;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 89
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/forms/e;->b()V

    return-void
.end method

.method public final willNotDraw()Z
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/appcompat/widget/AppCompatEditText;->willNotDraw()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/e;->r:Landroid/graphics/drawable/ColorDrawable;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.class final Lcom/pspdfkit/internal/views/forms/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;


# instance fields
.field private b:Lcom/pspdfkit/forms/FormElement;

.field private c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method final a(Lcom/pspdfkit/forms/FormElement;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->b:Lcom/pspdfkit/forms/FormElement;

    return-void
.end method

.method final a(ILandroid/view/KeyEvent;)Z
    .locals 5

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    const/4 v0, 0x0

    if-eqz p2, :cond_5

    iget-object p2, p0, Lcom/pspdfkit/internal/views/forms/b;->b:Lcom/pspdfkit/forms/FormElement;

    if-eqz p2, :cond_5

    const/16 v1, 0x3d

    const/4 v2, 0x1

    if-eq p1, v1, :cond_1

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 5
    :goto_1
    invoke-virtual {p2}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object p2

    sget-object v3, Lcom/pspdfkit/forms/FormType;->TEXT:Lcom/pspdfkit/forms/FormType;

    const/16 v4, 0x42

    if-ne p2, v3, :cond_2

    if-ne p1, v4, :cond_4

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->b:Lcom/pspdfkit/forms/FormElement;

    check-cast p1, Lcom/pspdfkit/forms/TextFormElement;

    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->isMultiLine()Z

    move-result p1

    if-nez p1, :cond_4

    goto :goto_2

    :cond_2
    if-eq p1, v4, :cond_3

    const/16 p2, 0x3e

    if-ne p1, p2, :cond_4

    :cond_3
    :goto_2
    const/4 v0, 0x1

    :cond_4
    or-int/2addr v0, v1

    :cond_5
    return v0
.end method

.method final b(ILandroid/view/KeyEvent;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    const/4 v1, 0x0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/b;->b:Lcom/pspdfkit/forms/FormElement;

    if-nez v0, :cond_0

    goto/16 :goto_2

    :cond_0
    const/16 v0, 0x3d

    if-ne p1, v0, :cond_1

    .line 2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->selectPreviousFormElement()Z

    move-result p1

    return p1

    :cond_1
    if-ne p1, v0, :cond_2

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->selectNextFormElement()Z

    move-result p1

    return p1

    :cond_2
    const/4 p2, 0x4

    if-ne p1, p2, :cond_3

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->finishEditing()Z

    move-result p1

    return p1

    .line 11
    :cond_3
    iget-object p2, p0, Lcom/pspdfkit/internal/views/forms/b;->b:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {p2}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object p2

    sget-object v0, Lcom/pspdfkit/forms/FormType;->TEXT:Lcom/pspdfkit/forms/FormType;

    const/16 v2, 0x42

    if-ne p2, v0, :cond_5

    if-ne p1, v2, :cond_a

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->b:Lcom/pspdfkit/forms/FormElement;

    check-cast p1, Lcom/pspdfkit/forms/TextFormElement;

    invoke-virtual {p1}, Lcom/pspdfkit/forms/TextFormElement;->isMultiLine()Z

    move-result p1

    if-nez p1, :cond_a

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    .line 14
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAutoSelectNextFormElementEnabled()Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    .line 15
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->hasNextElement()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->selectNextFormElement()Z

    move-result p1

    goto :goto_0

    .line 17
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->finishEditing()Z

    move-result p1

    :goto_0
    return p1

    :cond_5
    if-ne p1, v2, :cond_7

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    .line 19
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAutoSelectNextFormElementEnabled()Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    .line 20
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->hasNextElement()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->selectNextFormElement()Z

    move-result p1

    goto :goto_1

    .line 22
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;->finishEditing()Z

    move-result p1

    :goto_1
    return p1

    :cond_7
    const/16 p2, 0x3e

    if-ne p1, p2, :cond_a

    .line 23
    sget-object p1, Lcom/pspdfkit/internal/views/forms/b$a;->a:[I

    iget-object p2, p0, Lcom/pspdfkit/internal/views/forms/b;->b:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {p2}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p1, p1, p2

    const/4 p2, 0x1

    if-eq p1, p2, :cond_9

    const/4 v0, 0x2

    if-eq p1, v0, :cond_8

    goto :goto_2

    .line 28
    :cond_8
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->b:Lcom/pspdfkit/forms/FormElement;

    check-cast p1, Lcom/pspdfkit/forms/CheckBoxFormElement;

    invoke-virtual {p1}, Lcom/pspdfkit/forms/EditableButtonFormElement;->toggleSelection()Z

    return p2

    .line 29
    :cond_9
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->b:Lcom/pspdfkit/forms/FormElement;

    check-cast p1, Lcom/pspdfkit/forms/RadioButtonFormElement;

    invoke-virtual {p1}, Lcom/pspdfkit/forms/EditableButtonFormElement;->toggleSelection()Z

    return p2

    :cond_a
    :goto_2
    return v1
.end method

.method public final onChangeFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    return-void
.end method

.method public final onEnterFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    return-void
.end method

.method public final onExitFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 0

    const/4 p1, 0x0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/b;->c:Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;

    return-void
.end method

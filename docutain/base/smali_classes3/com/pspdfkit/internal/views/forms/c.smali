.class public final Lcom/pspdfkit/internal/views/forms/c;
.super Landroid/view/View;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/xb;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/forms/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/view/View;",
        "Lcom/pspdfkit/internal/xb<",
        "Lcom/pspdfkit/forms/FormElement;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/views/forms/c$a;

.field private c:Lcom/pspdfkit/forms/FormElement;

.field private final d:Lcom/pspdfkit/internal/views/forms/b;

.field private e:Lcom/pspdfkit/internal/pg$c;


# direct methods
.method public static synthetic $r8$lambda$7TI6Mi4lnbu6B6NC-SFzCi1bdv8(Lcom/pspdfkit/internal/views/forms/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/views/forms/c;->b()V

    return-void
.end method

.method public static synthetic $r8$lambda$uTGKunIimddrLh3gfHLL2ii4FdI(Lcom/pspdfkit/internal/views/forms/c;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/forms/c;->a(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/pspdfkit/internal/views/forms/c$a;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Lcom/pspdfkit/internal/views/forms/b;

    invoke-direct {p1}, Lcom/pspdfkit/internal/views/forms/b;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/c;->d:Lcom/pspdfkit/internal/views/forms/b;

    .line 18
    iput-object p3, p0, Lcom/pspdfkit/internal/views/forms/c;->b:Lcom/pspdfkit/internal/views/forms/c$a;

    .line 20
    invoke-virtual {p0, p2}, Landroid/view/View;->setBackgroundColor(I)V

    const/4 p1, 0x1

    .line 21
    invoke-virtual {p0, p1}, Landroid/view/View;->setFocusable(Z)V

    .line 22
    invoke-virtual {p0, p1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private synthetic a(Z)V
    .locals 1

    if-nez p1, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/c;->b:Lcom/pspdfkit/internal/views/forms/c$a;

    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->c:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    check-cast p1, Lcom/pspdfkit/internal/tb;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/tb;->a(Landroid/graphics/RectF;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/views/forms/c;->e:Lcom/pspdfkit/internal/pg$c;

    if-eqz p1, :cond_0

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/pg$c;->d()V

    const/4 p1, 0x0

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/c;->e:Lcom/pspdfkit/internal/pg$c;

    :cond_0
    return-void
.end method

.method private synthetic b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->e:Lcom/pspdfkit/internal/pg$c;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pg$c;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->e:Lcom/pspdfkit/internal/pg$c;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pg$c;->d()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->e:Lcom/pspdfkit/internal/pg$c;

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->b:Lcom/pspdfkit/internal/views/forms/c$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/forms/c;->c:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {v1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    check-cast v0, Lcom/pspdfkit/internal/tb;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/tb;->a(Landroid/graphics/RectF;)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final d()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->e:Lcom/pspdfkit/internal/pg$c;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/pg$c;->d()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->e:Lcom/pspdfkit/internal/pg$c;

    :cond_0
    return-void
.end method

.method public getFormElement()Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->c:Lcom/pspdfkit/forms/FormElement;

    return-object v0
.end method

.method public final h()V
    .locals 0

    return-void
.end method

.method public final j()Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public final n()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->c:Lcom/pspdfkit/forms/FormElement;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->b:Lcom/pspdfkit/internal/views/forms/c$a;

    if-nez v0, :cond_0

    goto :goto_0

    .line 6
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/views/forms/c$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/views/forms/c$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/forms/c;)V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/pg;->a(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;)Lcom/pspdfkit/internal/pg$c;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->e:Lcom/pspdfkit/internal/pg$c;

    .line 17
    new-instance v0, Lcom/pspdfkit/internal/views/forms/c$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/views/forms/c$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/views/forms/c;)V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final onChangeFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->d:Lcom/pspdfkit/internal/views/forms/b;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/b;->onChangeFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    return-void
.end method

.method public final onEnterFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->d:Lcom/pspdfkit/internal/views/forms/b;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/b;->onEnterFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    return-void
.end method

.method public final onExitFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->d:Lcom/pspdfkit/internal/views/forms/b;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/b;->onExitFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->d:Lcom/pspdfkit/internal/views/forms/b;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/views/forms/b;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->d:Lcom/pspdfkit/internal/views/forms/b;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/views/forms/b;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public setFormElement(Lcom/pspdfkit/forms/FormElement;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->c:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/forms/FormElement;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/views/forms/c;->c:Lcom/pspdfkit/forms/FormElement;

    .line 3
    new-instance v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;->LAYOUT:Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;-><init>(Landroid/graphics/RectF;Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;)V

    .line 5
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 8
    invoke-static {p0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 9
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/views/forms/c;->d:Lcom/pspdfkit/internal/views/forms/b;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/b;->a(Lcom/pspdfkit/forms/FormElement;)V

    return-void
.end method

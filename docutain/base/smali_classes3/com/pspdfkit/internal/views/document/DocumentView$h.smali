.class final Lcom/pspdfkit/internal/views/document/DocumentView$h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/dm$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/views/document/DocumentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "h"
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/views/document/DocumentView;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/views/document/DocumentView$h-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView$h;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/dm;)V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgeth0(Lcom/pspdfkit/internal/views/document/DocumentView;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->d()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 28
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p1

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgeth0(Lcom/pspdfkit/internal/views/document/DocumentView;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$mq(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    :goto_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/dm;Landroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 9

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    if-nez p4, :cond_0

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$mc(Lcom/pspdfkit/internal/views/document/DocumentView;Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 4
    :goto_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->d()Z

    move-result v2

    if-nez v2, :cond_1

    return v1

    :cond_1
    const/4 v2, 0x1

    if-nez v1, :cond_3

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget-object v3, v1, Lcom/pspdfkit/internal/views/document/DocumentView;->t:Lcom/pspdfkit/listeners/DocumentListener;

    if-eqz v3, :cond_2

    invoke-static {v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetQ(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/zf;

    move-result-object v4

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v5

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    .line 10
    invoke-interface/range {v3 .. v8}, Lcom/pspdfkit/listeners/DocumentListener;->onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p3

    if-eqz p3, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    .line 15
    :cond_3
    :goto_1
    iget-object p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p3

    :goto_2
    if-ge v0, p3, :cond_5

    .line 16
    iget-object v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v3, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(I)Lcom/pspdfkit/internal/dm;

    move-result-object v3

    .line 17
    invoke-virtual {v3, p1, p2, p4}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/internal/dm;Landroid/view/MotionEvent;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 24
    :cond_5
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetb(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/h9;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/h9;->a()V

    return v1
.end method

.method public final b(Lcom/pspdfkit/internal/dm;Landroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 9

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->d()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget-object v2, v0, Lcom/pspdfkit/internal/views/document/DocumentView;->u:Lcom/pspdfkit/listeners/OnDocumentLongPressListener;

    const/4 v8, 0x1

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetQ(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/zf;

    move-result-object v3

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v4

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    .line 7
    invoke-interface/range {v2 .. v7}, Lcom/pspdfkit/listeners/OnDocumentLongPressListener;->onDocumentLongPress(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    if-nez v1, :cond_2

    if-eqz p2, :cond_2

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    .line 14
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v7

    .line 16
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 17
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 18
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v4, Lcom/pspdfkit/R$dimen;->pspdf__min_selectable_text_size:I

    invoke-virtual {p2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    int-to-float v4, p2

    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetQ(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/zf;

    move-result-object v5

    move v6, v0

    .line 19
    invoke-static/range {v2 .. v7}, Lcom/pspdfkit/internal/zt;->a(FFFLcom/pspdfkit/internal/zf;ILandroid/graphics/Matrix;)Lcom/pspdfkit/datastructures/TextSelectionRectangles;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 27
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v2, v0, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(ILcom/pspdfkit/datastructures/TextSelectionRectangles;)V

    .line 28
    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetC(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/views/document/DocumentView$d;

    move-result-object p2

    sget-object v0, Lcom/pspdfkit/internal/views/document/DocumentView$d;->c:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-ne p2, v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    if-nez v1, :cond_3

    if-nez p4, :cond_3

    .line 36
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    .line 37
    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView$h;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetb(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/h9;

    move-result-object p2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p1

    iget p4, p3, Landroid/graphics/PointF;->x:F

    iget p3, p3, Landroid/graphics/PointF;->y:F

    invoke-virtual {p2, p1, p4, p3}, Lcom/pspdfkit/internal/h9;->a(IFF)V

    goto :goto_0

    :cond_3
    move v8, v1

    :goto_0
    return v8
.end method

.class final Lcom/pspdfkit/internal/views/document/a$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/editor/AnnotationEditor$OnDismissedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/views/document/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/views/document/a;


# direct methods
.method public static synthetic $r8$lambda$g_7kRW17M9h0BOeMDPYauXV2Vpo(Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/views/document/a$a;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$oDwsWrTbXw0L-ImhtS89X9CGlcE(Lcom/pspdfkit/internal/views/document/a$a;ZLcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/views/document/a$a;->a(ZLcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method private constructor <init>(Lcom/pspdfkit/internal/views/document/a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/a$a;->a:Lcom/pspdfkit/internal/views/document/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/views/document/a;Lcom/pspdfkit/internal/views/document/a$a-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/a$a;-><init>(Lcom/pspdfkit/internal/views/document/a;)V

    return-void
.end method

.method private static synthetic a(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.AnnotationEditor"

    const-string v2, "Annotation to remove was not found!"

    .line 27
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private a(ZLcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 1
    invoke-virtual {p3}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne p1, v0, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/a$a;->a:Lcom/pspdfkit/internal/views/document/a;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/views/document/a;)Lcom/pspdfkit/internal/fl;

    move-result-object p1

    .line 3
    invoke-static {p3}, Lcom/pspdfkit/internal/x;->b(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object v0

    .line 4
    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    .line 6
    invoke-interface {p2}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p1

    invoke-interface {p1, p3}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/a$a;->a:Lcom/pspdfkit/internal/views/document/a;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/views/document/a;)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object p1

    .line 10
    invoke-virtual {p3}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->b()Z

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 15
    invoke-virtual {p3, p1}, Lcom/pspdfkit/annotations/Annotation;->setContents(Ljava/lang/String;)V

    .line 20
    :cond_1
    :goto_0
    invoke-virtual {p3}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 21
    invoke-virtual {p3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 25
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/a$a;->a:Lcom/pspdfkit/internal/views/document/a;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/views/document/a;)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 26
    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public final onAnnotationEditorDismissed(Lcom/pspdfkit/ui/editor/AnnotationEditor;Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/a$a;->a:Lcom/pspdfkit/internal/views/document/a;

    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/views/document/a;)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 5
    :cond_0
    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/editor/AnnotationEditor;->getAnnotation(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance v1, Lcom/pspdfkit/internal/views/document/a$a$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p2, v0}, Lcom/pspdfkit/internal/views/document/a$a$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/document/a$a;ZLcom/pspdfkit/document/PdfDocument;)V

    new-instance p2, Lcom/pspdfkit/internal/views/document/a$a$$ExternalSyntheticLambda1;

    invoke-direct {p2}, Lcom/pspdfkit/internal/views/document/a$a$$ExternalSyntheticLambda1;-><init>()V

    .line 6
    invoke-virtual {p1, v1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

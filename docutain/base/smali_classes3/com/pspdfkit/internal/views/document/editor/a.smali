.class public final Lcom/pspdfkit/internal/views/document/editor/a;
.super Landroidx/recyclerview/widget/PdfViewHolderBindDirtyReporter;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field private final b:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

.field private final c:Lcom/pspdfkit/internal/views/document/editor/b;

.field public d:Landroid/graphics/Bitmap;

.field public e:Lio/reactivex/rxjava3/disposables/Disposable;

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/ju;Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;Lcom/pspdfkit/internal/views/document/editor/b;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/PdfViewHolderBindDirtyReporter;-><init>(Landroid/view/View;)V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/document/editor/a;->f:Z

    .line 9
    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/editor/a;->b:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    .line 10
    iput-object p3, p0, Lcom/pspdfkit/internal/views/document/editor/a;->c:Lcom/pspdfkit/internal/views/document/editor/b;

    .line 11
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 12
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .line 5
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getElevation()F

    move-result v0

    .line 6
    iget-object v1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/high16 v2, 0x40c00000    # 6.0f

    add-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/View;->setElevation(F)V

    .line 7
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast v0, Lcom/pspdfkit/internal/ju;

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ju;->a()V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/a;->b:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    if-eqz v0, :cond_0

    .line 11
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;->onStartDraggingPages()V

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/a;->c:Lcom/pspdfkit/internal/views/document/editor/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/editor/b;->a(Z)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/editor/a;->f:Z

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 2
    :goto_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/editor/a;->f:Z

    if-eqz v0, :cond_1

    .line 3
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast v0, Lcom/pspdfkit/internal/ju;

    .line 4
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ju;->a(Z)V

    :cond_1
    return-void
.end method

.method public final b()V
    .locals 3

    .line 1
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getElevation()F

    move-result v0

    .line 2
    iget-object v1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/high16 v2, 0x40c00000    # 6.0f

    sub-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/View;->setElevation(F)V

    .line 3
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast v0, Lcom/pspdfkit/internal/ju;

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ju;->b()V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/a;->b:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    if-eqz v0, :cond_0

    .line 7
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;->onStopDraggingPages()V

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/a;->c:Lcom/pspdfkit/internal/views/document/editor/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/editor/b;->a(Z)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/views/document/editor/a;->f:Z

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/a;->c:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/document/editor/b;->c(Lcom/pspdfkit/internal/views/document/editor/a;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/a;->b:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    if-eqz p1, :cond_1

    .line 4
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;->onPageSelectionStateChanged()V

    goto :goto_0

    .line 7
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/a;->b:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    if-eqz p1, :cond_1

    .line 8
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;->onPageClick(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .locals 1

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/views/document/editor/a;->f:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/a;->b:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;->onPageLongClick(I)V

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method protected final onViewHolderBindDirty()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/a;->c:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/views/document/editor/b;->a(Lcom/pspdfkit/internal/views/document/editor/a;)V

    return-void
.end method

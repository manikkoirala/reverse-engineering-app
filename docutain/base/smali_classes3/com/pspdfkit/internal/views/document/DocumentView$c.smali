.class final Lcom/pspdfkit/internal/views/document/DocumentView$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/views/document/DocumentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field private a:Z

.field private final b:Landroid/util/SparseLongArray;

.field final synthetic c:Lcom/pspdfkit/internal/views/document/DocumentView;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x1

    .line 10
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->a:Z

    .line 16
    new-instance p1, Landroid/util/SparseLongArray;

    invoke-direct {p1}, Landroid/util/SparseLongArray;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->b:Landroid/util/SparseLongArray;

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/views/document/DocumentView$c-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView$c;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetQ(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/zf;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 7
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    const/4 v1, 0x1

    if-nez p1, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result p1

    if-nez p1, :cond_1

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->b:Landroid/util/SparseLongArray;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v2

    invoke-virtual {p1, p2, v2, v3}, Landroid/util/SparseLongArray;->put(IJ)V

    goto :goto_0

    .line 9
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-ne p1, v1, :cond_2

    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->b:Landroid/util/SparseLongArray;

    .line 10
    invoke-virtual {p1, p2}, Landroid/util/SparseLongArray;->get(I)J

    move-result-wide v2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x12c

    cmp-long p1, v2, v4

    if-ltz p1, :cond_2

    return v0

    .line 15
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetR(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isCopyPasteEnabled()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 16
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-nez p1, :cond_8

    .line 17
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result p1

    if-eqz p1, :cond_8

    iget-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->a:Z

    if-eqz p1, :cond_8

    .line 19
    invoke-static {}, Lcom/pspdfkit/internal/gj;->d()Lcom/pspdfkit/internal/b0;

    move-result-object p1

    .line 20
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v2}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetF(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/specialMode/handler/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/b;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v2

    const/16 v3, 0x1f

    if-ne p2, v3, :cond_4

    if-eqz v2, :cond_4

    .line 23
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v2}, Lcom/pspdfkit/internal/b0;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 24
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgeta0(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/h7;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 25
    invoke-interface {p1, v2}, Lcom/pspdfkit/internal/h7;->a(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    .line 27
    :cond_3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->a:Z

    return v1

    :cond_4
    const/16 v3, 0x34

    if-ne p2, v3, :cond_6

    if-eqz v2, :cond_6

    .line 32
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v2}, Lcom/pspdfkit/internal/b0;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 33
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgeta0(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/h7;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 34
    invoke-interface {p1, v2}, Lcom/pspdfkit/internal/h7;->b(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    .line 36
    :cond_5
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->a:Z

    return v1

    :cond_6
    const/16 v2, 0x32

    if-ne p2, v2, :cond_8

    .line 39
    invoke-virtual {p1}, Lcom/pspdfkit/internal/b0;->c()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 40
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgeta0(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/h7;

    move-result-object p2

    if-eqz p2, :cond_7

    .line 41
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result p1

    invoke-interface {p2, p1}, Lcom/pspdfkit/internal/h7;->a(I)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Maybe;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    .line 43
    :cond_7
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->a:Z

    return v1

    :cond_8
    const/16 p1, 0x16

    if-eq p2, p1, :cond_a

    const/16 p1, 0x15

    if-eq p2, p1, :cond_a

    const/16 p1, 0x13

    if-eq p2, p1, :cond_a

    const/16 p1, 0x14

    if-ne p2, p1, :cond_9

    goto :goto_1

    :cond_9
    const/4 p1, 0x0

    goto :goto_2

    :cond_a
    :goto_1
    const/4 p1, 0x1

    .line 55
    :goto_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p3

    const/16 v2, 0x70

    const/16 v3, 0x43

    if-nez p3, :cond_d

    if-nez p1, :cond_b

    .line 56
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetF(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/specialMode/handler/b;

    move-result-object p1

    .line 57
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/b;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-eqz p1, :cond_c

    if-eq p2, v3, :cond_b

    if-ne p2, v2, :cond_c

    :cond_b
    const/4 v0, 0x1

    :cond_c
    return v0

    .line 61
    :cond_d
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->a:Z

    if-eqz p1, :cond_e

    .line 64
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 65
    invoke-static {p1}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;)Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object p1

    instance-of p1, p1, Landroid/widget/EditText;

    if-eqz p1, :cond_e

    return v0

    :cond_e
    if-eq p2, v3, :cond_11

    if-eq p2, v2, :cond_11

    packed-switch p2, :pswitch_data_0

    return v0

    .line 66
    :pswitch_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 67
    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetQ(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/zf;

    move-result-object p2

    .line 68
    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->getPageBinding()Lcom/pspdfkit/document/PageBinding;

    move-result-object p2

    sget-object p3, Lcom/pspdfkit/document/PageBinding;->RIGHT_EDGE:Lcom/pspdfkit/document/PageBinding;

    if-ne p2, p3, :cond_f

    .line 69
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Z)Z

    move-result p1

    goto :goto_3

    .line 71
    :cond_f
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Z)Z

    move-result p1

    :goto_3
    return p1

    .line 72
    :pswitch_1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 73
    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetQ(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/zf;

    move-result-object p2

    .line 74
    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->getPageBinding()Lcom/pspdfkit/document/PageBinding;

    move-result-object p2

    sget-object p3, Lcom/pspdfkit/document/PageBinding;->RIGHT_EDGE:Lcom/pspdfkit/document/PageBinding;

    if-ne p2, p3, :cond_10

    .line 75
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Z)Z

    move-result p1

    goto :goto_4

    .line 77
    :cond_10
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Z)Z

    move-result p1

    :goto_4
    return p1

    .line 78
    :pswitch_2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Z)Z

    move-result p1

    return p1

    .line 82
    :pswitch_3
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Z)Z

    move-result p1

    return p1

    .line 85
    :cond_11
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetF(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/specialMode/handler/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/b;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-eqz p1, :cond_12

    .line 86
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetF(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/specialMode/handler/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/b;->deleteCurrentlySelectedAnnotation()V

    .line 87
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$c;->c:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetF(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/specialMode/handler/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/d;->exitActiveMode()V

    return v1

    :cond_12
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

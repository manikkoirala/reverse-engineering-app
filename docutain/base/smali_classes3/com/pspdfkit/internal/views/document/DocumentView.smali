.class public Lcom/pspdfkit/internal/views/document/DocumentView;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/no$a;
.implements Lcom/pspdfkit/ui/drawable/PdfDrawableManager;
.implements Lcom/pspdfkit/internal/ms;
.implements Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/document/DocumentView$c;,
        Lcom/pspdfkit/internal/views/document/DocumentView$b;,
        Lcom/pspdfkit/internal/views/document/DocumentView$h;,
        Lcom/pspdfkit/internal/views/document/DocumentView$f;,
        Lcom/pspdfkit/internal/views/document/DocumentView$e;,
        Lcom/pspdfkit/internal/views/document/DocumentView$g;,
        Lcom/pspdfkit/internal/views/document/DocumentView$d;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/view/ViewGroup;",
        "Lcom/pspdfkit/internal/no$a<",
        "Lcom/pspdfkit/internal/dm;",
        ">;",
        "Lcom/pspdfkit/ui/drawable/PdfDrawableManager;",
        "Lcom/pspdfkit/internal/ms;",
        "Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionChangeListener;"
    }
.end annotation


# instance fields
.field A:I

.field B:Lcom/pspdfkit/internal/ug;

.field private C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

.field private D:Lcom/pspdfkit/internal/specialMode/handler/a;

.field private E:Lcom/pspdfkit/internal/specialMode/handler/e;

.field private F:Lcom/pspdfkit/internal/specialMode/handler/b;

.field private G:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

.field private H:Lcom/pspdfkit/internal/specialMode/handler/c;

.field private I:I

.field private J:Z

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Z

.field private P:F

.field private Q:Lcom/pspdfkit/internal/zf;

.field private R:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private S:Lcom/pspdfkit/listeners/scrolling/ScrollState;

.field private T:Lcom/pspdfkit/internal/views/document/DocumentView$h;

.field private U:Lcom/pspdfkit/internal/zi;

.field private V:I

.field private W:I

.field private a0:Lcom/pspdfkit/internal/h7;

.field private b:Lcom/pspdfkit/internal/h9;

.field private b0:Lcom/pspdfkit/internal/fl;

.field private c:Z

.field private c0:Lcom/pspdfkit/internal/pr;

.field private d:Lio/reactivex/rxjava3/disposables/Disposable;

.field private final d0:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/pspdfkit/internal/views/document/DocumentView$f;

.field private final e0:Ljava/util/ArrayList;

.field final f:Lcom/pspdfkit/internal/fm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/fm<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;"
        }
    .end annotation
.end field

.field private f0:Lcom/pspdfkit/internal/i2;

.field final g:Lcom/pspdfkit/internal/fm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/fm<",
            "Lcom/pspdfkit/ui/overlay/OverlayViewProvider;",
            ">;"
        }
    .end annotation
.end field

.field private g0:Z

.field private final h:Ljava/util/HashSet;

.field private final h0:Ljava/util/HashSet;

.field private final i:Ljava/util/ArrayList;

.field private final i0:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/internal/views/document/DocumentView$g;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/pspdfkit/internal/i1;

.field private j0:Lcom/pspdfkit/internal/views/document/DocumentView$e;

.field private k:Lcom/pspdfkit/internal/views/document/a;

.field private k0:Z

.field private final l:Lcom/pspdfkit/internal/b7;

.field private l0:Lcom/pspdfkit/internal/uh;

.field private final m:Lcom/pspdfkit/internal/ac;

.field private m0:Lcom/pspdfkit/internal/ni;

.field private final n:Lcom/pspdfkit/internal/yt;

.field private final n0:Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

.field protected o:I

.field private final o0:Ljava/lang/Runnable;

.field protected p:I

.field private p0:Ljava/lang/Integer;

.field protected q:I

.field protected r:F

.field protected s:F

.field protected t:Lcom/pspdfkit/listeners/DocumentListener;

.field protected u:Lcom/pspdfkit/listeners/OnDocumentLongPressListener;

.field protected v:Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;

.field protected w:Lcom/pspdfkit/internal/no;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/no<",
            "Lcom/pspdfkit/internal/dm;",
            ">;"
        }
    .end annotation
.end field

.field protected x:Lcom/pspdfkit/internal/i;

.field y:Lcom/pspdfkit/internal/o7;

.field z:Lcom/pspdfkit/internal/q7;


# direct methods
.method public static synthetic $r8$lambda$45VxqOM343EuM-q0MNICer56-hE(Lcom/pspdfkit/internal/views/document/DocumentView;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$5PEc9OULBXC2V4D-Nq23Hf6VTaY(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/forms/FormField;Ljava/util/Set;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/forms/FormField;Ljava/util/Set;)V

    return-void
.end method

.method public static synthetic $r8$lambda$DF8Vb3_w9kuOTu17bdgzK0n9qww(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/dm;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/internal/dm;)V

    return-void
.end method

.method public static synthetic $r8$lambda$GtL7SDEtYfdzLfhh9L3qpZ82Jrk(Lcom/pspdfkit/internal/views/document/DocumentView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->e(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$VpDcXRpr1ViUjbyKxFUOvVsGgLk(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/ui/PdfFragment;)V

    return-void
.end method

.method public static synthetic $r8$lambda$ZlxByJWRe76FxsidvrIUFddvHA8(Lcom/pspdfkit/internal/views/document/DocumentView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->f(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$l-YH7l3EgSi9rM7Wgvc86fe7mr0(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->v()V

    return-void
.end method

.method public static synthetic $r8$lambda$u4VmBcMg6e0MHVyj-oiScnGUdWM(Lcom/pspdfkit/internal/views/document/DocumentView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->h(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$xbZGsb8dqjO925yXOEFPRrEvlIo(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->f()V

    return-void
.end method

.method public static synthetic $r8$lambda$ytQ6ed3dKHzvkl7WxJou54l6dl4(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/listeners/DocumentListener;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/listeners/DocumentListener;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetC(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/views/document/DocumentView$d;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetD(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/specialMode/handler/a;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->D:Lcom/pspdfkit/internal/specialMode/handler/a;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetE(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/specialMode/handler/e;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetF(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/specialMode/handler/b;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->F:Lcom/pspdfkit/internal/specialMode/handler/b;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetI(Lcom/pspdfkit/internal/views/document/DocumentView;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->I:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetJ(Lcom/pspdfkit/internal/views/document/DocumentView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->J:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetK(Lcom/pspdfkit/internal/views/document/DocumentView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->K:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetL(Lcom/pspdfkit/internal/views/document/DocumentView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->L:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetM(Lcom/pspdfkit/internal/views/document/DocumentView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->M:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetN(Lcom/pspdfkit/internal/views/document/DocumentView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->N:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetQ(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/zf;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetR(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeta0(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/h7;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->a0:Lcom/pspdfkit/internal/h7;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/h9;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->b:Lcom/pspdfkit/internal/h9;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeth0(Lcom/pspdfkit/internal/views/document/DocumentView;)Ljava/util/HashSet;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h0:Ljava/util/HashSet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputI(Lcom/pspdfkit/internal/views/document/DocumentView;I)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->I:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputM(Lcom/pspdfkit/internal/views/document/DocumentView;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->M:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mc(Lcom/pspdfkit/internal/views/document/DocumentView;Landroid/view/MotionEvent;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->c(Landroid/view/MotionEvent;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mq(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->q()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 686
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__documentViewStyle:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    .line 687
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->c:Z

    .line 732
    new-instance v0, Lcom/pspdfkit/internal/fm;

    invoke-direct {v0}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->f:Lcom/pspdfkit/internal/fm;

    .line 738
    new-instance v0, Lcom/pspdfkit/internal/fm;

    invoke-direct {v0}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->g:Lcom/pspdfkit/internal/fm;

    .line 744
    new-instance v0, Ljava/util/HashSet;

    const/4 v2, 0x6

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h:Ljava/util/HashSet;

    .line 748
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i:Ljava/util/ArrayList;

    .line 752
    new-instance v0, Lcom/pspdfkit/internal/i1;

    .line 754
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/pspdfkit/internal/i1;-><init>(Lcom/pspdfkit/internal/du;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->j:Lcom/pspdfkit/internal/i1;

    .line 759
    new-instance v0, Lcom/pspdfkit/internal/b7;

    invoke-direct {v0}, Lcom/pspdfkit/internal/b7;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->l:Lcom/pspdfkit/internal/b7;

    .line 764
    new-instance v0, Lcom/pspdfkit/internal/ac;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ac;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m:Lcom/pspdfkit/internal/ac;

    .line 768
    new-instance v0, Lcom/pspdfkit/internal/yt;

    invoke-direct {v0}, Lcom/pspdfkit/internal/yt;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->n:Lcom/pspdfkit/internal/yt;

    const/4 v0, -0x1

    .line 778
    iput v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->q:I

    .line 822
    sget-object v2, Lcom/pspdfkit/internal/views/document/DocumentView$d;->a:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    iput-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const/4 v2, 0x1

    .line 833
    iput v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->I:I

    .line 840
    iput-boolean v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->J:Z

    .line 845
    iput-boolean v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->K:Z

    .line 848
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->L:Z

    .line 851
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->M:Z

    .line 857
    iput-boolean v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->N:Z

    .line 863
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->O:Z

    const/4 v2, 0x0

    .line 866
    iput v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->P:F

    .line 878
    sget-object v2, Lcom/pspdfkit/listeners/scrolling/ScrollState;->IDLE:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    iput-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->S:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    .line 894
    iput v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->V:I

    .line 917
    const-class v0, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->d0:Ljava/util/EnumSet;

    .line 920
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->e0:Ljava/util/ArrayList;

    .line 948
    new-instance v0, Ljava/util/HashSet;

    const/4 v2, 0x5

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h0:Ljava/util/HashSet;

    .line 952
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i0:Lcom/pspdfkit/internal/nh;

    .line 963
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->k0:Z

    .line 972
    iput-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m0:Lcom/pspdfkit/internal/ni;

    .line 975
    new-instance p1, Lcom/pspdfkit/internal/views/document/DocumentView$a;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/views/document/DocumentView$a;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->n0:Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 1011
    new-instance p1, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda7;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->o0:Ljava/lang/Runnable;

    .line 1020
    iput-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->p0:Ljava/lang/Integer;

    .line 1035
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->h()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 341
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__documentViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    .line 342
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->c:Z

    .line 387
    new-instance p2, Lcom/pspdfkit/internal/fm;

    invoke-direct {p2}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->f:Lcom/pspdfkit/internal/fm;

    .line 393
    new-instance p2, Lcom/pspdfkit/internal/fm;

    invoke-direct {p2}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->g:Lcom/pspdfkit/internal/fm;

    .line 399
    new-instance p2, Ljava/util/HashSet;

    const/4 v0, 0x6

    invoke-direct {p2, v0}, Ljava/util/HashSet;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h:Ljava/util/HashSet;

    .line 403
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i:Ljava/util/ArrayList;

    .line 407
    new-instance p2, Lcom/pspdfkit/internal/i1;

    .line 409
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/pspdfkit/internal/i1;-><init>(Lcom/pspdfkit/internal/du;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->j:Lcom/pspdfkit/internal/i1;

    .line 414
    new-instance p2, Lcom/pspdfkit/internal/b7;

    invoke-direct {p2}, Lcom/pspdfkit/internal/b7;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->l:Lcom/pspdfkit/internal/b7;

    .line 419
    new-instance p2, Lcom/pspdfkit/internal/ac;

    invoke-direct {p2}, Lcom/pspdfkit/internal/ac;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m:Lcom/pspdfkit/internal/ac;

    .line 423
    new-instance p2, Lcom/pspdfkit/internal/yt;

    invoke-direct {p2}, Lcom/pspdfkit/internal/yt;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->n:Lcom/pspdfkit/internal/yt;

    const/4 p2, -0x1

    .line 433
    iput p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->q:I

    .line 477
    sget-object v0, Lcom/pspdfkit/internal/views/document/DocumentView$d;->a:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const/4 v0, 0x1

    .line 488
    iput v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->I:I

    .line 495
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->J:Z

    .line 500
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->K:Z

    .line 503
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->L:Z

    .line 506
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->M:Z

    .line 512
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->N:Z

    .line 518
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->O:Z

    const/4 v0, 0x0

    .line 521
    iput v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->P:F

    .line 533
    sget-object v0, Lcom/pspdfkit/listeners/scrolling/ScrollState;->IDLE:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->S:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    .line 549
    iput p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->V:I

    .line 572
    const-class p2, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {p2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->d0:Ljava/util/EnumSet;

    .line 575
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->e0:Ljava/util/ArrayList;

    .line 603
    new-instance p2, Ljava/util/HashSet;

    const/4 v0, 0x5

    invoke-direct {p2, v0}, Ljava/util/HashSet;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h0:Ljava/util/HashSet;

    .line 607
    new-instance p2, Lcom/pspdfkit/internal/nh;

    invoke-direct {p2}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i0:Lcom/pspdfkit/internal/nh;

    .line 618
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->k0:Z

    const/4 p1, 0x0

    .line 627
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m0:Lcom/pspdfkit/internal/ni;

    .line 630
    new-instance p2, Lcom/pspdfkit/internal/views/document/DocumentView$a;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/views/document/DocumentView$a;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->n0:Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 666
    new-instance p2, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda7;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->o0:Ljava/lang/Runnable;

    .line 675
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->p0:Ljava/lang/Integer;

    .line 685
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->h()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->c:Z

    .line 47
    new-instance p2, Lcom/pspdfkit/internal/fm;

    invoke-direct {p2}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->f:Lcom/pspdfkit/internal/fm;

    .line 53
    new-instance p2, Lcom/pspdfkit/internal/fm;

    invoke-direct {p2}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->g:Lcom/pspdfkit/internal/fm;

    .line 59
    new-instance p2, Ljava/util/HashSet;

    const/4 p3, 0x6

    invoke-direct {p2, p3}, Ljava/util/HashSet;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h:Ljava/util/HashSet;

    .line 63
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i:Ljava/util/ArrayList;

    .line 67
    new-instance p2, Lcom/pspdfkit/internal/i1;

    .line 69
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/pspdfkit/internal/i1;-><init>(Lcom/pspdfkit/internal/du;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->j:Lcom/pspdfkit/internal/i1;

    .line 74
    new-instance p2, Lcom/pspdfkit/internal/b7;

    invoke-direct {p2}, Lcom/pspdfkit/internal/b7;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->l:Lcom/pspdfkit/internal/b7;

    .line 79
    new-instance p2, Lcom/pspdfkit/internal/ac;

    invoke-direct {p2}, Lcom/pspdfkit/internal/ac;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m:Lcom/pspdfkit/internal/ac;

    .line 83
    new-instance p2, Lcom/pspdfkit/internal/yt;

    invoke-direct {p2}, Lcom/pspdfkit/internal/yt;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->n:Lcom/pspdfkit/internal/yt;

    const/4 p2, -0x1

    .line 93
    iput p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->q:I

    .line 137
    sget-object p3, Lcom/pspdfkit/internal/views/document/DocumentView$d;->a:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    iput-object p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const/4 p3, 0x1

    .line 148
    iput p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->I:I

    .line 155
    iput-boolean p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->J:Z

    .line 160
    iput-boolean p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->K:Z

    .line 163
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->L:Z

    .line 166
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->M:Z

    .line 172
    iput-boolean p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->N:Z

    .line 178
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->O:Z

    const/4 p3, 0x0

    .line 181
    iput p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->P:F

    .line 193
    sget-object p3, Lcom/pspdfkit/listeners/scrolling/ScrollState;->IDLE:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    iput-object p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->S:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    .line 209
    iput p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->V:I

    .line 232
    const-class p2, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {p2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->d0:Ljava/util/EnumSet;

    .line 235
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->e0:Ljava/util/ArrayList;

    .line 263
    new-instance p2, Ljava/util/HashSet;

    const/4 p3, 0x5

    invoke-direct {p2, p3}, Ljava/util/HashSet;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h0:Ljava/util/HashSet;

    .line 267
    new-instance p2, Lcom/pspdfkit/internal/nh;

    invoke-direct {p2}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i0:Lcom/pspdfkit/internal/nh;

    .line 278
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->k0:Z

    const/4 p1, 0x0

    .line 287
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m0:Lcom/pspdfkit/internal/ni;

    .line 290
    new-instance p2, Lcom/pspdfkit/internal/views/document/DocumentView$a;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/views/document/DocumentView$a;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->n0:Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 326
    new-instance p2, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda7;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->o0:Ljava/lang/Runnable;

    .line 335
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->p0:Ljava/lang/Integer;

    .line 340
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->h()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .line 1036
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p1, 0x0

    .line 1037
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->c:Z

    .line 1082
    new-instance p2, Lcom/pspdfkit/internal/fm;

    invoke-direct {p2}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->f:Lcom/pspdfkit/internal/fm;

    .line 1088
    new-instance p2, Lcom/pspdfkit/internal/fm;

    invoke-direct {p2}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->g:Lcom/pspdfkit/internal/fm;

    .line 1094
    new-instance p2, Ljava/util/HashSet;

    const/4 p3, 0x6

    invoke-direct {p2, p3}, Ljava/util/HashSet;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h:Ljava/util/HashSet;

    .line 1098
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i:Ljava/util/ArrayList;

    .line 1102
    new-instance p2, Lcom/pspdfkit/internal/i1;

    .line 1104
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/pspdfkit/internal/i1;-><init>(Lcom/pspdfkit/internal/du;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->j:Lcom/pspdfkit/internal/i1;

    .line 1109
    new-instance p2, Lcom/pspdfkit/internal/b7;

    invoke-direct {p2}, Lcom/pspdfkit/internal/b7;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->l:Lcom/pspdfkit/internal/b7;

    .line 1114
    new-instance p2, Lcom/pspdfkit/internal/ac;

    invoke-direct {p2}, Lcom/pspdfkit/internal/ac;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m:Lcom/pspdfkit/internal/ac;

    .line 1118
    new-instance p2, Lcom/pspdfkit/internal/yt;

    invoke-direct {p2}, Lcom/pspdfkit/internal/yt;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->n:Lcom/pspdfkit/internal/yt;

    const/4 p2, -0x1

    .line 1128
    iput p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->q:I

    .line 1172
    sget-object p3, Lcom/pspdfkit/internal/views/document/DocumentView$d;->a:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    iput-object p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const/4 p3, 0x1

    .line 1183
    iput p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->I:I

    .line 1190
    iput-boolean p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->J:Z

    .line 1195
    iput-boolean p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->K:Z

    .line 1198
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->L:Z

    .line 1201
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->M:Z

    .line 1207
    iput-boolean p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->N:Z

    .line 1213
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->O:Z

    const/4 p3, 0x0

    .line 1216
    iput p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->P:F

    .line 1228
    sget-object p3, Lcom/pspdfkit/listeners/scrolling/ScrollState;->IDLE:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    iput-object p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->S:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    .line 1244
    iput p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->V:I

    .line 1267
    const-class p2, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {p2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->d0:Ljava/util/EnumSet;

    .line 1270
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->e0:Ljava/util/ArrayList;

    .line 1298
    new-instance p2, Ljava/util/HashSet;

    const/4 p3, 0x5

    invoke-direct {p2, p3}, Ljava/util/HashSet;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h0:Ljava/util/HashSet;

    .line 1302
    new-instance p2, Lcom/pspdfkit/internal/nh;

    invoke-direct {p2}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i0:Lcom/pspdfkit/internal/nh;

    .line 1313
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->k0:Z

    const/4 p1, 0x0

    .line 1322
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m0:Lcom/pspdfkit/internal/ni;

    .line 1325
    new-instance p2, Lcom/pspdfkit/internal/views/document/DocumentView$a;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/views/document/DocumentView$a;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->n0:Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 1361
    new-instance p2, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda7;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->o0:Ljava/lang/Runnable;

    .line 1370
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->p0:Ljava/lang/Integer;

    .line 1391
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->h()V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/forms/FormField;Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 329
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getFormElements()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/forms/FormElement;

    .line 330
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m:Lcom/pspdfkit/internal/ac;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ac;->c(Lcom/pspdfkit/forms/FormElement;)V

    goto :goto_0

    .line 332
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->t:Lcom/pspdfkit/listeners/DocumentListener;

    if-eqz p1, :cond_1

    .line 333
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    .line 334
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->t:Lcom/pspdfkit/listeners/DocumentListener;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-interface {v0, v1, p2}, Lcom/pspdfkit/listeners/DocumentListener;->onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/dm;)V
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->G:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V

    return-void
.end method

.method private a(Lcom/pspdfkit/internal/dm;Z)V
    .locals 8

    .line 335
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->d()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 337
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    .line 338
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 339
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->e0:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/Annotation;

    .line 340
    iget-boolean v4, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->g0:Z

    if-eqz v4, :cond_4

    sget-object v4, Lcom/pspdfkit/internal/tl;->a:Ljava/util/EnumSet;

    .line 341
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v4

    invoke-static {v4}, Lcom/pspdfkit/internal/tl;->a(Lcom/pspdfkit/annotations/AnnotationType;)Z

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v4, :cond_3

    .line 342
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v4

    .line 343
    sget-object v7, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v4, v7, :cond_2

    sget-object v7, Lcom/pspdfkit/annotations/AnnotationType;->FILE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v4, v7, :cond_2

    sget-object v7, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v4, v7, :cond_2

    sget-object v7, Lcom/pspdfkit/annotations/AnnotationType;->SOUND:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v4, v7, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    if-nez v4, :cond_3

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    if-nez v5, :cond_4

    goto :goto_0

    .line 344
    :cond_4
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v4

    if-ne v4, v0, :cond_1

    .line 345
    :cond_5
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 349
    :cond_6
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object p1

    .line 350
    invoke-virtual {p1, v1, p2}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/ArrayList;Z)V

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 144
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAutomaticLinkGenerationEnabled()Z

    move-result p1

    .line 145
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/zf;->setAutomaticLinkGenerationEnabled(Z)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/listeners/DocumentListener;I)V
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-interface {p1, v0, p2}, Lcom/pspdfkit/listeners/DocumentListener;->onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V

    return-void
.end method

.method private synthetic a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 300
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 301
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 303
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/dm;->onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V

    .line 305
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->t:Lcom/pspdfkit/listeners/DocumentListener;

    if-eqz v1, :cond_0

    .line 306
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-interface {v1, v2, v0}, Lcom/pspdfkit/listeners/DocumentListener;->onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 177
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getVisiblePages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 178
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 180
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    .line 181
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    .line 182
    invoke-virtual {p1, v3, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 183
    invoke-virtual {v2, p1}, Lcom/pspdfkit/internal/dm;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    neg-float v2, v3

    neg-float v3, v4

    .line 184
    invoke-virtual {p1, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    goto :goto_0

    :cond_1
    :goto_1
    return v1
.end method

.method private b(Lcom/pspdfkit/internal/dm;)V
    .locals 4

    .line 140
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->g0:Z

    if-eqz v0, :cond_2

    .line 141
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->d0:Ljava/util/EnumSet;

    .line 142
    sget-object v1, Lcom/pspdfkit/internal/tl;->a:Ljava/util/EnumSet;

    .line 143
    const-class v1, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 144
    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/AnnotationType;

    .line 145
    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->FILE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->SOUND:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v2, v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-nez v3, :cond_0

    .line 146
    invoke-virtual {v1, v2}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 147
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->d0:Ljava/util/EnumSet;

    .line 149
    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/EnumSet;)V

    return-void
.end method

.method private b(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 55
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getVisiblePages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 56
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 58
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    .line 59
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    .line 61
    invoke-virtual {p1, v3, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 62
    invoke-virtual {v2, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    neg-float v2, v3

    neg-float v3, v4

    .line 63
    invoke-virtual {p1, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    goto :goto_0

    :cond_1
    :goto_1
    return v1
.end method

.method private c(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->scrollOnEdgeTapEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 26
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getScrollDirection()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v0

    sget-object v2, Lcom/pspdfkit/configuration/page/PageScrollDirection;->HORIZONTAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    if-ne v0, v2, :cond_3

    .line 27
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_3

    .line 28
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->d(I)F

    move-result v0

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_3

    .line 33
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result p1

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 35
    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    aget v0, v0, v1

    int-to-float v0, v0

    sub-float/2addr p1, v0

    .line 39
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 40
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/PdfConfiguration;->scrollOnEdgeTapMargin()I

    move-result v2

    int-to-float v2, v2

    mul-float v0, v0, v2

    cmpg-float v2, p1, v0

    if-gtz v2, :cond_1

    .line 43
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->animateScrollOnEdgeTaps()Z

    move-result p1

    .line 44
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageBinding()Lcom/pspdfkit/document/PageBinding;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/document/PageBinding;->RIGHT_EDGE:Lcom/pspdfkit/document/PageBinding;

    if-ne v0, v1, :cond_0

    .line 45
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Z)Z

    move-result p1

    goto :goto_0

    .line 47
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Z)Z

    move-result p1

    :goto_0
    return p1

    .line 48
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, v0

    cmpl-float p1, p1, v2

    if-ltz p1, :cond_3

    .line 50
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->animateScrollOnEdgeTaps()Z

    move-result p1

    .line 51
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageBinding()Lcom/pspdfkit/document/PageBinding;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/document/PageBinding;->RIGHT_EDGE:Lcom/pspdfkit/document/PageBinding;

    if-ne v0, v1, :cond_2

    .line 52
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Z)Z

    move-result p1

    goto :goto_1

    .line 54
    :cond_2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Z)Z

    move-result p1

    :goto_1
    return p1

    :cond_3
    return v1
.end method

.method private synthetic e(I)V
    .locals 1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(IZ)V

    :cond_0
    return-void
.end method

.method private f()V
    .locals 11

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-nez v0, :cond_0

    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->c()I

    move-result v1

    if-gez v1, :cond_1

    return-void

    .line 10
    :cond_1
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ug;->i(I)F

    move-result v1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->d()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    return-void

    .line 14
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->p0:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->p0:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 17
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ug;->b(II)I

    move-result v0

    :goto_0
    const/4 v1, 0x0

    .line 20
    iput-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->p0:Ljava/lang/Integer;

    .line 22
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    .line 23
    iget v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->A:I

    mul-int/lit8 v4, v3, 0x2

    add-int/lit8 v5, v4, 0x1

    sub-int v3, v0, v3

    sub-int v4, v1, v4

    .line 26
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/2addr v5, v3

    sub-int/2addr v5, v2

    .line 27
    invoke-static {v5, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 29
    iget-object v5, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->clear()V

    .line 30
    iget-object v5, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 35
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v5, :cond_7

    .line 36
    invoke-super {p0, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/internal/dm;

    .line 37
    invoke-virtual {v7}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v8

    invoke-virtual {v8}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v8

    if-lt v8, v3, :cond_5

    if-le v8, v1, :cond_4

    goto :goto_2

    .line 41
    :cond_4
    iget-object v9, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h:Ljava/util/HashSet;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    if-ne v8, v0, :cond_6

    .line 42
    invoke-virtual {p0}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v8

    instance-of v8, v8, Lcom/pspdfkit/internal/im;

    if-eqz v8, :cond_6

    .line 45
    invoke-virtual {v7}, Landroid/view/View;->requestFocus()Z

    goto :goto_3

    .line 46
    :cond_5
    :goto_2
    iget-object v8, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 58
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_8
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/dm;

    .line 59
    iget-object v6, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->U:Lcom/pspdfkit/internal/zi;

    if-eqz v6, :cond_9

    .line 60
    invoke-virtual {v5}, Lcom/pspdfkit/internal/dm;->getMediaPlayer()Lcom/pspdfkit/internal/yi;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/pspdfkit/internal/zi;->b(Lcom/pspdfkit/internal/yi;)V

    :cond_9
    const/16 v6, 0x8

    .line 63
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 64
    iget-object v6, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->w:Lcom/pspdfkit/internal/no;

    invoke-virtual {v6, v5}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/mo;)V

    .line 67
    :try_start_0
    invoke-virtual {v5}, Lcom/pspdfkit/internal/dm;->d()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 68
    iget-object v6, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h0:Ljava/util/HashSet;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    nop

    goto :goto_4

    .line 69
    :cond_a
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :goto_5
    if-gt v3, v1, :cond_12

    .line 73
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h:Ljava/util/HashSet;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 74
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->w:Lcom/pspdfkit/internal/no;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/no$a;)Lcom/pspdfkit/internal/mo;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/dm;

    .line 76
    iget-object v5, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    .line 77
    invoke-virtual {v5, v3}, Lcom/pspdfkit/internal/ug;->e(I)Lcom/pspdfkit/utils/Size;

    move-result-object v5

    iget-object v6, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {v6, v3}, Lcom/pspdfkit/internal/ug;->i(I)F

    move-result v6

    .line 78
    invoke-virtual {v0, v5, v3, v6}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/utils/Size;IF)V

    .line 79
    iget-boolean v5, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->c:Z

    .line 80
    invoke-virtual {v0, v5}, Lcom/pspdfkit/internal/dm;->setRedactionAnnotationPreviewEnabled(Z)V

    .line 81
    invoke-direct {p0, v0, v4}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/internal/dm;Z)V

    .line 82
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Lcom/pspdfkit/internal/dm;)V

    .line 85
    iget-object v5, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h0:Ljava/util/HashSet;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v5, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->U:Lcom/pspdfkit/internal/zi;

    if-eqz v5, :cond_b

    .line 89
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getMediaPlayer()Lcom/pspdfkit/internal/yi;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/pspdfkit/internal/zi;->a(Lcom/pspdfkit/internal/yi;)V

    .line 92
    :cond_b
    iget-object v5, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {v5, v0}, Lcom/pspdfkit/internal/ug;->b(Lcom/pspdfkit/internal/dm;)V

    .line 93
    iget-object v5, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {v5, v0}, Lcom/pspdfkit/internal/ug;->a(Lcom/pspdfkit/internal/dm;)V

    .line 95
    iget-object v5, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    invoke-virtual {v5}, Ljava/lang/Enum;->ordinal()I

    move-result v5

    if-eq v5, v2, :cond_d

    const/4 v6, 0x5

    if-eq v5, v6, :cond_c

    goto :goto_6

    .line 108
    :cond_c
    iget-object v5, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->G:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {v0, v5}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V

    goto :goto_6

    .line 109
    :cond_d
    iget-object v5, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->D:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/specialMode/handler/a;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v5

    if-eqz v5, :cond_f

    .line 110
    iget-object v5, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->D:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/specialMode/handler/a;->getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v5

    if-nez v5, :cond_e

    .line 112
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v5

    .line 114
    :cond_e
    iget-object v6, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->D:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 115
    invoke-virtual {v6}, Lcom/pspdfkit/internal/specialMode/handler/a;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v6

    iget-object v7, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->D:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 116
    invoke-virtual {v0, v7, v6, v5}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 127
    :cond_f
    :goto_6
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 128
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-nez v5, :cond_10

    .line 129
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    invoke-virtual {p0, v0, v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_7

    .line 131
    :cond_10
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 132
    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    :cond_11
    :goto_7
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_12
    return-void
.end method

.method private synthetic f(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->t:Lcom/pspdfkit/listeners/DocumentListener;

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-interface {v0, v1, p1}, Lcom/pspdfkit/listeners/DocumentListener;->onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V

    :cond_0
    return-void
.end method

.method private h()V
    .locals 6

    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Landroid/view/View;->setWillNotDraw(Z)V

    .line 6
    invoke-virtual {p0, v0}, Landroid/view/View;->setSaveEnabled(Z)V

    .line 7
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setSaveFromParentEnabled(Z)V

    .line 11
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 15
    invoke-virtual {p0, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 16
    new-instance v1, Lcom/pspdfkit/internal/views/document/DocumentView$c;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/pspdfkit/internal/views/document/DocumentView$c;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/views/document/DocumentView$c-IA;)V

    invoke-virtual {p0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 18
    new-instance v1, Lcom/pspdfkit/internal/no;

    const/4 v3, 0x7

    invoke-direct {v1, v3}, Lcom/pspdfkit/internal/no;-><init>(I)V

    iput-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->w:Lcom/pspdfkit/internal/no;

    .line 20
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 21
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    iput v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->o:I

    .line 22
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->p:I

    .line 24
    new-instance v1, Lcom/pspdfkit/internal/views/document/DocumentView$b;

    invoke-direct {v1, p0, v2}, Lcom/pspdfkit/internal/views/document/DocumentView$b;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/views/document/DocumentView$b-IA;)V

    .line 25
    new-instance v3, Lcom/pspdfkit/internal/o7;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 26
    invoke-direct {v3, v4, v1, v0}, Lcom/pspdfkit/internal/o7;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/o7$c;I)V

    .line 27
    iput-object v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->y:Lcom/pspdfkit/internal/o7;

    .line 28
    invoke-virtual {v3, v0}, Lcom/pspdfkit/internal/o7;->a(Z)V

    .line 29
    new-instance v0, Lcom/pspdfkit/internal/q7;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 30
    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, v3, v1, v4}, Lcom/pspdfkit/internal/q7;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/q7$a;Landroid/os/Handler;)V

    .line 31
    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->z:Lcom/pspdfkit/internal/q7;

    .line 33
    new-instance v0, Lcom/pspdfkit/internal/views/document/DocumentView$h;

    invoke-direct {v0, p0, v2}, Lcom/pspdfkit/internal/views/document/DocumentView$h;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/views/document/DocumentView$h-IA;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->T:Lcom/pspdfkit/internal/views/document/DocumentView$h;

    .line 36
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/e8;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->A:I

    .line 38
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->W:I

    return-void
.end method

.method private h(I)V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;I)V

    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private i()V
    .locals 18

    move-object/from16 v11, p0

    .line 1
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    sub-int v2, v0, v1

    .line 2
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    sub-int v3, v0, v1

    .line 3
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getPagePadding()I

    move-result v1

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v6

    .line 7
    iget-object v0, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->p()Lcom/pspdfkit/internal/ug$a;

    move-result-object v0

    move-object v13, v0

    goto :goto_0

    :cond_0
    const/4 v13, 0x0

    :goto_0
    if-eqz v13, :cond_1

    .line 10
    iget v0, v13, Lcom/pspdfkit/internal/ug$a;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->p0:Ljava/lang/Integer;

    .line 14
    :cond_1
    iget-object v0, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 15
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getScrollDirection()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v0

    iget-object v1, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 16
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getScrollMode()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v1

    iget-object v4, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 17
    invoke-virtual {v4}, Lcom/pspdfkit/configuration/PdfConfiguration;->getFitMode()Lcom/pspdfkit/configuration/page/PageFitMode;

    move-result-object v4

    .line 22
    iget-object v5, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v5}, Lcom/pspdfkit/configuration/PdfConfiguration;->shouldZoomOutBounce()Z

    move-result v5

    if-eqz v5, :cond_2

    const v5, 0x3f4ccccd    # 0.8f

    goto :goto_1

    :cond_2
    const/high16 v5, 0x3f800000    # 1.0f

    :goto_1
    iget-object v7, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 23
    invoke-virtual {v7}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMaxZoomScale()F

    move-result v7

    .line 25
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    iget-object v9, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    iget-object v10, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-static {v8, v10, v9}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)Z

    move-result v8

    iget-object v9, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 26
    invoke-virtual {v9}, Lcom/pspdfkit/configuration/PdfConfiguration;->isFirstPageAlwaysSingle()Z

    move-result v9

    iget-object v10, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 27
    invoke-virtual {v10}, Lcom/pspdfkit/configuration/PdfConfiguration;->showGapBetweenPages()Z

    move-result v10

    iget-object v14, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    iget-object v15, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 31
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v12

    move-object/from16 v16, v13

    iget-object v13, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    move/from16 v17, v10

    iget-object v10, v11, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-static {v12, v10, v13}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)Z

    move-result v10

    const-string v12, "document"

    .line 32
    invoke-static {v14, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "configuration"

    invoke-static {v15, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {v14}, Lcom/pspdfkit/internal/zf;->getPageBinding()Lcom/pspdfkit/document/PageBinding;

    move-result-object v12

    sget-object v13, Lcom/pspdfkit/document/PageBinding;->RIGHT_EDGE:Lcom/pspdfkit/document/PageBinding;

    if-ne v12, v13, :cond_3

    invoke-virtual {v15}, Lcom/pspdfkit/configuration/PdfConfiguration;->getScrollDirection()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v12

    sget-object v11, Lcom/pspdfkit/configuration/page/PageScrollDirection;->HORIZONTAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    if-ne v12, v11, :cond_3

    .line 69
    new-instance v10, Lcom/pspdfkit/internal/gq;

    invoke-virtual {v14}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v11

    invoke-direct {v10, v11}, Lcom/pspdfkit/internal/gq;-><init>(I)V

    goto :goto_2

    .line 70
    :cond_3
    invoke-virtual {v14}, Lcom/pspdfkit/internal/zf;->getPageBinding()Lcom/pspdfkit/document/PageBinding;

    move-result-object v11

    if-ne v11, v13, :cond_4

    .line 71
    invoke-virtual {v15}, Lcom/pspdfkit/configuration/PdfConfiguration;->getScrollDirection()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v11

    sget-object v12, Lcom/pspdfkit/configuration/page/PageScrollDirection;->VERTICAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    if-ne v11, v12, :cond_4

    if-eqz v10, :cond_4

    .line 74
    new-instance v10, Lcom/pspdfkit/internal/hq;

    invoke-virtual {v14}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v11

    invoke-virtual {v15}, Lcom/pspdfkit/configuration/PdfConfiguration;->isFirstPageAlwaysSingle()Z

    move-result v12

    invoke-direct {v10, v11, v12}, Lcom/pspdfkit/internal/hq;-><init>(IZ)V

    goto :goto_2

    .line 76
    :cond_4
    new-instance v10, Lcom/pspdfkit/internal/a8;

    invoke-direct {v10}, Lcom/pspdfkit/internal/a8;-><init>()V

    .line 77
    :goto_2
    sget-object v11, Lcom/pspdfkit/configuration/page/PageFitMode;->FIT_TO_SCREEN:Lcom/pspdfkit/configuration/page/PageFitMode;

    const/4 v12, 0x0

    const/4 v13, 0x1

    if-ne v4, v11, :cond_5

    const/4 v11, 0x1

    goto :goto_3

    :cond_5
    const/4 v11, 0x0

    .line 83
    :goto_3
    sget-object v4, Lcom/pspdfkit/configuration/page/PageScrollMode;->CONTINUOUS:Lcom/pspdfkit/configuration/page/PageScrollMode;

    if-ne v1, v4, :cond_7

    .line 84
    sget-object v1, Lcom/pspdfkit/configuration/page/PageScrollDirection;->HORIZONTAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    if-ne v0, v1, :cond_6

    .line 85
    new-instance v9, Lcom/pspdfkit/internal/rd;

    move-object v0, v9

    move-object/from16 v1, p0

    move v4, v5

    move v5, v7

    move v7, v11

    move-object v8, v10

    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/internal/rd;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V

    goto :goto_4

    .line 96
    :cond_6
    new-instance v9, Lcom/pspdfkit/internal/kv;

    move-object v0, v9

    move-object/from16 v1, p0

    move v4, v5

    move v5, v7

    move v7, v11

    move-object v8, v10

    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/internal/kv;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V

    :goto_4
    move-object/from16 v0, p0

    goto/16 :goto_6

    .line 108
    :cond_7
    sget-object v1, Lcom/pspdfkit/configuration/page/PageScrollDirection;->HORIZONTAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    if-ne v0, v1, :cond_9

    if-eqz v8, :cond_8

    .line 110
    new-instance v14, Lcom/pspdfkit/internal/sd;

    xor-int/lit8 v8, v9, 0x1

    move-object v0, v14

    move-object/from16 v1, p0

    move v4, v5

    move v5, v7

    move v7, v11

    move/from16 v9, v17

    invoke-direct/range {v0 .. v10}, Lcom/pspdfkit/internal/sd;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZZZLcom/pspdfkit/internal/cm;)V

    goto :goto_5

    .line 123
    :cond_8
    new-instance v9, Lcom/pspdfkit/internal/td;

    move-object v0, v9

    move-object/from16 v1, p0

    move v4, v5

    move v5, v7

    move v7, v11

    move-object v8, v10

    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/internal/td;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V

    goto :goto_4

    :cond_9
    if-eqz v8, :cond_a

    .line 136
    new-instance v14, Lcom/pspdfkit/internal/lv;

    xor-int/lit8 v8, v9, 0x1

    move-object v0, v14

    move-object/from16 v1, p0

    move v4, v5

    move v5, v7

    move v7, v11

    move/from16 v9, v17

    invoke-direct/range {v0 .. v10}, Lcom/pspdfkit/internal/lv;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZZZLcom/pspdfkit/internal/cm;)V

    :goto_5
    move-object/from16 v0, p0

    move-object v9, v14

    goto :goto_6

    .line 149
    :cond_a
    new-instance v9, Lcom/pspdfkit/internal/mv;

    move-object v0, v9

    move-object/from16 v1, p0

    move v4, v5

    move v5, v7

    move v7, v11

    move-object v8, v10

    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/internal/mv;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V

    goto :goto_4

    .line 150
    :goto_6
    iput-object v9, v0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v16, :cond_b

    move-object/from16 v1, v16

    .line 170
    iget v2, v1, Lcom/pspdfkit/internal/ug$a;->c:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_b

    .line 171
    invoke-virtual {v9, v1}, Lcom/pspdfkit/internal/ug;->b(Lcom/pspdfkit/internal/ug$a;)V

    .line 175
    :cond_b
    :goto_7
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v12, v1, :cond_d

    .line 176
    invoke-super {v0, v12}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/dm;

    .line 177
    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v2

    if-nez v2, :cond_c

    goto :goto_8

    .line 179
    :cond_c
    iget-object v3, v0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/ug;->e(I)Lcom/pspdfkit/utils/Size;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/utils/Size;)V

    :goto_8
    add-int/lit8 v12, v12, 0x1

    goto :goto_7

    .line 182
    :cond_d
    iget-object v1, v0, Lcom/pspdfkit/internal/views/document/DocumentView;->e:Lcom/pspdfkit/internal/views/document/DocumentView$f;

    if-eqz v1, :cond_e

    const/4 v2, 0x0

    .line 184
    iput-object v2, v0, Lcom/pspdfkit/internal/views/document/DocumentView;->e:Lcom/pspdfkit/internal/views/document/DocumentView$f;

    .line 185
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/document/DocumentView$f;->a()V

    :cond_e
    return-void
.end method

.method private q()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->u()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getVisiblePages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h0:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 7
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i0:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/views/document/DocumentView$g;

    .line 8
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/document/DocumentView$g;->a()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private setScrollState(Lcom/pspdfkit/listeners/scrolling/ScrollState;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->S:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    if-ne v0, p1, :cond_0

    return-void

    .line 3
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->S:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->v:Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    .line 7
    invoke-interface {v0, v1, p1}, Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;->onScrollStateChanged(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/listeners/scrolling/ScrollState;)V

    .line 10
    :cond_1
    sget-object v0, Lcom/pspdfkit/listeners/scrolling/ScrollState;->IDLE:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    if-ne p1, v0, :cond_2

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->b:Lcom/pspdfkit/internal/h9;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/h9;->c()V

    :cond_2
    return-void
.end method

.method private v()V
    .locals 4

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->g0:Z

    .line 2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 3
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/dm;

    .line 4
    invoke-direct {p0, v3, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/internal/dm;Z)V

    .line 5
    invoke-direct {p0, v3}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Lcom/pspdfkit/internal/dm;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .locals 2

    .line 168
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p2

    .line 170
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    .line 174
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ug;->b(I)I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/ug;->c(I)I

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v1

    sub-int/2addr p1, v1

    int-to-float p1, p1

    .line 175
    invoke-virtual {p2, v0, p1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    :cond_0
    return-object p2
.end method

.method public final a(I)Lcom/pspdfkit/internal/dm;
    .locals 0

    .line 176
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/dm;

    return-object p1
.end method

.method public final a(IIIFJ)V
    .locals 7

    .line 162
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-wide v5, p5

    .line 163
    invoke-virtual/range {v0 .. v6}, Lcom/pspdfkit/internal/ug;->a(IIIFJ)V

    :cond_0
    return-void
.end method

.method public final a(ILcom/pspdfkit/datastructures/Range;)V
    .locals 2

    .line 186
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->TEXT_SELECTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 187
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isTextSelectionEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 188
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    sget-object v1, Lcom/pspdfkit/internal/views/document/DocumentView$d;->c:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    .line 189
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    .line 190
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    iget v0, v0, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    if-eq v0, p1, :cond_1

    .line 191
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->exitCurrentlyActiveMode()V

    .line 194
    :cond_1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 196
    iput-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    .line 198
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-static {v1, p1, p2}, Lcom/pspdfkit/datastructures/TextSelection;->fromTextRange(Lcom/pspdfkit/document/PdfDocument;ILcom/pspdfkit/datastructures/Range;)Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object p1

    .line 199
    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/specialMode/handler/e;)V

    .line 202
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->b:Lcom/pspdfkit/internal/h9;

    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/h9;->a(Lcom/pspdfkit/internal/specialMode/handler/e;)V

    :cond_3
    return-void
.end method

.method public final a(ILcom/pspdfkit/datastructures/TextSelectionRectangles;)V
    .locals 2

    .line 203
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->TEXT_SELECTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 204
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isTextSelectionEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 205
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    sget-object v1, Lcom/pspdfkit/internal/views/document/DocumentView$d;->c:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    .line 206
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    .line 207
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    iget v0, v0, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    if-eq v0, p1, :cond_1

    .line 208
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->exitCurrentlyActiveMode()V

    .line 211
    :cond_1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 213
    iput-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    .line 215
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-static {v1, p1, p2}, Lcom/pspdfkit/datastructures/TextSelection;->fromTextRects(Lcom/pspdfkit/document/PdfDocument;ILcom/pspdfkit/datastructures/TextSelectionRectangles;)Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object p1

    .line 216
    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/specialMode/handler/e;)V

    .line 219
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->b:Lcom/pspdfkit/internal/h9;

    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/h9;->a(Lcom/pspdfkit/internal/specialMode/handler/e;)V

    :cond_3
    return-void
.end method

.method public final a(IZ)V
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/ug;->a(IZ)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/RectF;IJ)V
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    .line 165
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/ug;->a(Landroid/graphics/RectF;IJ)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/RectF;IJZ)V
    .locals 6

    .line 166
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    move-object v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    .line 167
    invoke-virtual/range {v0 .. v5}, Lcom/pspdfkit/internal/ug;->a(Landroid/graphics/RectF;IJZ)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 220
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationFlags;->READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    .line 223
    :cond_1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 224
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->NONE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_4

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    sget-object v1, Lcom/pspdfkit/internal/views/document/DocumentView$d;->d:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-eq v0, v1, :cond_3

    .line 226
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->exitCurrentlyActiveMode()V

    .line 229
    :cond_3
    iput-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    .line 230
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->F:Lcom/pspdfkit/internal/specialMode/handler/b;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/b;->a(Lcom/pspdfkit/annotations/Annotation;)V

    :cond_4
    return-void
.end method

.method public final a(Lcom/pspdfkit/forms/FormElement;)V
    .locals 3

    .line 231
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/hb;->b(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 233
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    if-eqz p1, :cond_0

    .line 234
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->isReadOnly()Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 237
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    sget-object v1, Lcom/pspdfkit/internal/views/document/DocumentView$d;->e:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-eq v0, v1, :cond_1

    .line 238
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->exitCurrentlyActiveMode()V

    .line 241
    :cond_1
    iput-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    .line 242
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->H:Lcom/pspdfkit/internal/specialMode/handler/c;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/c;->a(Lcom/pspdfkit/forms/FormElement;)V

    :cond_2
    return-void
.end method

.method public final a(Lcom/pspdfkit/forms/FormField;)V
    .locals 4

    .line 307
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getFormElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    .line 310
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getFormElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 312
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 313
    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    goto :goto_1

    .line 316
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 317
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getFormElements()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/forms/FormElement;

    .line 318
    invoke-virtual {v2}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 322
    :cond_2
    :goto_1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    .line 323
    invoke-virtual {v1, v2, v0}, Lcom/pspdfkit/internal/yl;->a(Lcom/pspdfkit/document/PdfDocument;Ljava/util/Collection;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 324
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/u;

    const/4 v3, 0x5

    .line 325
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    .line 326
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 327
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda3;

    invoke-direct {v2, p0, p1, v0}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/forms/FormField;Ljava/util/Set;)V

    .line 328
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_3
    :goto_2
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/views/annotations/n;)V
    .locals 1

    .line 359
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m0:Lcom/pspdfkit/internal/ni;

    if-nez v0, :cond_0

    return-void

    .line 360
    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ni;->a(Lcom/pspdfkit/internal/views/annotations/n;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/views/document/DocumentView$g;)V
    .locals 1

    .line 351
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i0:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    .line 352
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->q()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;Lcom/pspdfkit/internal/pr;Lcom/pspdfkit/internal/i2;Lcom/pspdfkit/ui/audio/AudioModeManager;Lcom/pspdfkit/internal/l1;Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/internal/views/document/DocumentView$f;)V
    .locals 20

    move-object/from16 v8, p0

    move-object/from16 v7, p1

    move-object/from16 v6, p2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/i;

    invoke-direct {v0, v7, v8}, Lcom/pspdfkit/internal/i;-><init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/views/document/DocumentView;)V

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->x:Lcom/pspdfkit/internal/i;

    .line 2
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/zi;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    iget-object v1, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/zi;-><init>(Lcom/pspdfkit/configuration/PdfConfiguration;)V

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->U:Lcom/pspdfkit/internal/zi;

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/views/document/a;

    invoke-direct {v0, v8, v7, v6}, Lcom/pspdfkit/internal/views/document/a;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;)V

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->k:Lcom/pspdfkit/internal/views/document/a;

    .line 6
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->get(Landroid/content/Context;)Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    move-result-object v15

    .line 8
    new-instance v0, Lcom/pspdfkit/internal/uh;

    iget-object v2, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-direct {v0, v8, v2}, Lcom/pspdfkit/internal/uh;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->l0:Lcom/pspdfkit/internal/uh;

    const/4 v5, 0x0

    .line 10
    :try_start_0
    new-instance v0, Lcom/pspdfkit/internal/ni;

    .line 11
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->l0:Lcom/pspdfkit/internal/uh;

    invoke-direct {v0, v2, v8, v3}, Lcom/pspdfkit/internal/ni;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/uh;)V

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->m0:Lcom/pspdfkit/internal/ni;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 14
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t initialise measurement text magnifier view: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "Document View"

    invoke-static {v3, v0, v2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    :goto_0
    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/a;

    iget-object v10, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->j:Lcom/pspdfkit/internal/i1;

    iget-object v11, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->k:Lcom/pspdfkit/internal/views/document/a;

    iget-object v2, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->l0:Lcom/pspdfkit/internal/uh;

    move-object v9, v0

    move-object/from16 v12, p5

    move-object/from16 v13, p1

    move-object/from16 v14, p6

    move-object/from16 v16, p7

    move-object/from16 v17, v1

    move-object/from16 v18, p2

    move-object/from16 v19, v2

    invoke-direct/range {v9 .. v19}, Lcom/pspdfkit/internal/specialMode/handler/a;-><init>(Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/internal/views/document/a;Lcom/pspdfkit/ui/audio/AudioModeManager;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/l1;Lcom/pspdfkit/preferences/PSPDFKitPreferences;Lcom/pspdfkit/ui/fonts/Font;Landroid/os/Handler;Lcom/pspdfkit/internal/fl;Lcom/pspdfkit/internal/uh;)V

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->D:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 27
    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/e;

    iget-object v2, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->n:Lcom/pspdfkit/internal/yt;

    iget-object v3, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->k:Lcom/pspdfkit/internal/views/document/a;

    iget-object v9, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->l0:Lcom/pspdfkit/internal/uh;

    move-object v1, v0

    move-object/from16 v4, p1

    const/4 v10, 0x0

    move-object/from16 v5, p6

    move-object v11, v6

    move-object/from16 v6, p2

    move-object v12, v7

    move-object v7, v9

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/internal/specialMode/handler/e;-><init>(Lcom/pspdfkit/internal/yt;Lcom/pspdfkit/internal/views/document/a;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/l1;Lcom/pspdfkit/internal/fl;Lcom/pspdfkit/internal/uh;)V

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    .line 34
    new-instance v0, Lcom/pspdfkit/internal/h9;

    invoke-direct {v0, v12}, Lcom/pspdfkit/internal/h9;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->b:Lcom/pspdfkit/internal/h9;

    .line 35
    iget-object v1, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Lcom/pspdfkit/internal/zt$d;)V

    .line 37
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getTextSelectionListeners()Lcom/pspdfkit/internal/yt;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/pspdfkit/internal/yt;->addOnTextSelectionChangeListener(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionChangeListener;)V

    .line 38
    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/b;

    iget-object v2, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->j:Lcom/pspdfkit/internal/i1;

    iget-object v3, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->k:Lcom/pspdfkit/internal/views/document/a;

    move-object v1, v0

    move-object/from16 v4, p5

    move-object/from16 v5, p1

    move-object/from16 v6, p0

    move-object/from16 v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/internal/specialMode/handler/b;-><init>(Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/internal/views/document/a;Lcom/pspdfkit/ui/audio/AudioModeManager;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/fl;)V

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->F:Lcom/pspdfkit/internal/specialMode/handler/b;

    .line 45
    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    iget-object v1, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->l:Lcom/pspdfkit/internal/b7;

    invoke-direct {v0, v1, v12}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;-><init>(Lcom/pspdfkit/internal/b7;Lcom/pspdfkit/ui/PdfFragment;)V

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->G:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    .line 46
    new-instance v0, Lcom/pspdfkit/internal/specialMode/handler/c;

    iget-object v1, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->m:Lcom/pspdfkit/internal/ac;

    invoke-direct {v0, v1, v12, v11}, Lcom/pspdfkit/internal/specialMode/handler/c;-><init>(Lcom/pspdfkit/internal/ac;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;)V

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->H:Lcom/pspdfkit/internal/specialMode/handler/c;

    .line 47
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/ag;->getPasteManager()Lcom/pspdfkit/internal/h7;

    move-result-object v0

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->a0:Lcom/pspdfkit/internal/h7;

    move-object/from16 v1, p4

    .line 48
    iput-object v1, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->f0:Lcom/pspdfkit/internal/i2;

    .line 49
    iput-object v11, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->b0:Lcom/pspdfkit/internal/fl;

    move-object/from16 v1, p3

    .line 50
    iput-object v1, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->c0:Lcom/pspdfkit/internal/pr;

    .line 51
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    iget-object v2, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-static {v0, v2, v1}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    .line 52
    iput v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->A:I

    .line 59
    :cond_0
    iget-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isScrollbarsEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 60
    invoke-virtual {v8, v10}, Landroid/view/View;->setHorizontalScrollBarEnabled(Z)V

    .line 61
    invoke-virtual {v8, v10}, Landroid/view/View;->setVerticalScrollBarEnabled(Z)V

    .line 67
    :cond_1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->q()Lcom/pspdfkit/internal/lp;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 68
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    .line 69
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/hb;->c(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v2

    .line 74
    invoke-static {v1}, Lcom/pspdfkit/internal/e8;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v0, :cond_2

    .line 76
    sget-object v0, Lcom/pspdfkit/internal/tl;->a:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    if-nez v2, :cond_4

    .line 78
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->WIDGET:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 81
    :cond_2
    sget-object v0, Lcom/pspdfkit/internal/tl;->b:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    if-eqz v2, :cond_4

    .line 83
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->WIDGET:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 87
    :cond_3
    sget-object v0, Lcom/pspdfkit/internal/tl;->b:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    .line 88
    :cond_4
    :goto_1
    invoke-virtual {v8, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setOverlaidAnnotationTypes(Ljava/util/EnumSet;)V

    .line 90
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/ui/PdfFragment;->isRedactionAnnotationPreviewEnabled()Z

    move-result v0

    invoke-virtual {v8, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setRedactionAnnotationPreviewEnabled(Z)V

    const-string v0, "onDocumentViewReadyCallback"

    const-string v1, "argumentName"

    .line 91
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    move-object/from16 v2, p8

    .line 142
    invoke-static {v2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 143
    check-cast v0, Lcom/pspdfkit/internal/views/document/DocumentView$f;

    iput-object v0, v8, Lcom/pspdfkit/internal/views/document/DocumentView;->e:Lcom/pspdfkit/internal/views/document/DocumentView$f;

    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/overlay/OverlayViewProvider;)V
    .locals 2

    const-string v0, "overlayViewProvider"

    const-string v1, "argumentName"

    .line 244
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 295
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 296
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->g:Lcom/pspdfkit/internal/fm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/fm;->a(Lcom/pspdfkit/ui/PageObjectProvider;)V

    return-void
.end method

.method public final a()Z
    .locals 4

    .line 297
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 298
    invoke-super {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/dm;

    .line 299
    invoke-virtual {v3}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/am;->b()Z

    move-result v3

    or-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return v2
.end method

.method public final a(Landroid/graphics/RectF;I)Z
    .locals 3

    .line 148
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 151
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 152
    invoke-virtual {v0, v1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    iget v2, v1, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iput v2, p1, Landroid/graphics/RectF;->left:F

    .line 155
    iget v2, v1, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iput v2, p1, Landroid/graphics/RectF;->top:F

    .line 156
    iget v2, v1, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iput v2, p1, Landroid/graphics/RectF;->right:F

    .line 157
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    iput v1, p1, Landroid/graphics/RectF;->bottom:F

    .line 158
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    const/4 v2, 0x0

    .line 159
    invoke-virtual {v1, p2, v2}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p2

    .line 160
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .line 357
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m0:Lcom/pspdfkit/internal/ni;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 358
    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ni;->a(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public final a(Z)Z
    .locals 4

    .line 353
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-static {v0, v2, v1}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 354
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result v3

    add-int/2addr v3, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 355
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 356
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(IZ)V

    return v1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public final addDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V
    .locals 2

    const-string v0, "drawableProvider"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->f:Lcom/pspdfkit/internal/fm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/fm;->a(Lcom/pspdfkit/ui/PageObjectProvider;)V

    return-void
.end method

.method public final b(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .locals 2

    if-eqz p2, :cond_0

    move-object v0, p2

    goto :goto_0

    .line 49
    :cond_0
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 51
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v1, :cond_1

    .line 54
    invoke-virtual {v1, p1, p2}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public final b(I)Lcom/pspdfkit/internal/dm;
    .locals 4

    .line 44
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 45
    invoke-super {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/dm;

    .line 46
    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v3

    if-ne v3, p1, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public final b()V
    .locals 4

    .line 118
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 119
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/dm;

    .line 120
    invoke-virtual {v3}, Lcom/pspdfkit/internal/dm;->getFormEditor()Lcom/pspdfkit/internal/tb;

    move-result-object v3

    .line 121
    invoke-virtual {v3, v1}, Lcom/pspdfkit/internal/tb;->a(Z)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(IIIFJ)V
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-wide v5, p5

    .line 48
    invoke-virtual/range {v0 .. v6}, Lcom/pspdfkit/internal/ug;->b(IIIFJ)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/views/document/DocumentView$g;)V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i0:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda1;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/ui/PdfFragment;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p2

    const/4 v0, 0x5

    .line 6
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p2

    .line 7
    invoke-virtual {p2}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    const/4 p2, 0x0

    .line 10
    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    .line 11
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    if-lez p2, :cond_2

    const/4 p2, 0x0

    .line 12
    invoke-super {p0, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/dm;

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->U:Lcom/pspdfkit/internal/zi;

    if-eqz v1, :cond_0

    .line 14
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getMediaPlayer()Lcom/pspdfkit/internal/yi;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zi;->b(Lcom/pspdfkit/internal/yi;)V

    :cond_0
    const/16 v1, 0x8

    .line 17
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 18
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->w:Lcom/pspdfkit/internal/no;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/mo;)V

    .line 21
    :try_start_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 22
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h0:Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    :catch_0
    :cond_1
    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    goto :goto_0

    .line 24
    :cond_2
    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->t:Lcom/pspdfkit/listeners/DocumentListener;

    if-eqz p2, :cond_3

    .line 27
    invoke-interface {p2, p1}, Lcom/pspdfkit/listeners/DocumentListener;->onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V

    .line 33
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    .line 35
    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->getPermissions()Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Ljava/util/EnumSet;)V

    .line 38
    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->k:Lcom/pspdfkit/internal/views/document/a;

    if-eqz p2, :cond_4

    .line 39
    invoke-virtual {p2}, Lcom/pspdfkit/internal/views/document/a;->a()V

    .line 42
    :cond_4
    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->j:Lcom/pspdfkit/internal/i1;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/i1;->a(Lcom/pspdfkit/document/PdfDocument;)V

    .line 43
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->j:Lcom/pspdfkit/internal/i1;

    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->n0:Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/i1;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/ui/overlay/OverlayViewProvider;)V
    .locals 2

    const-string v0, "overlayViewProvider"

    const-string v1, "argumentName"

    .line 65
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 116
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 117
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->g:Lcom/pspdfkit/internal/fm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/fm;->b(Lcom/pspdfkit/ui/PageObjectProvider;)V

    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 122
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 126
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 128
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    .line 129
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 130
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 133
    :cond_1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    .line 134
    invoke-virtual {v1, v2, v0}, Lcom/pspdfkit/internal/yl;->a(Lcom/pspdfkit/document/PdfDocument;Ljava/util/Collection;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 135
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    const/4 v2, 0x5

    .line 136
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    .line 137
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 138
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda8;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Ljava/util/List;)V

    .line 139
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_2
    :goto_1
    return-void
.end method

.method public final b(Z)Z
    .locals 4

    .line 151
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-static {v0, v2, v1}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 152
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result v3

    sub-int/2addr v3, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ltz v0, :cond_1

    .line 154
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(IZ)V

    return v1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public final c(I)I
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-nez v0, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ug;->d(I)I

    move-result p1

    :goto_0
    return p1
.end method

.method public final c()Lcom/pspdfkit/internal/dm;
    .locals 15

    .line 1
    new-instance v14, Lcom/pspdfkit/internal/dm;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    .line 2
    invoke-direct {v14, v0, v1}, Lcom/pspdfkit/internal/dm;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x1

    .line 3
    invoke-virtual {v14, v0}, Landroid/view/View;->setHorizontalScrollBarEnabled(Z)V

    .line 4
    invoke-virtual {v14, v0}, Landroid/view/View;->setVerticalScrollBarEnabled(Z)V

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->j:Lcom/pspdfkit/internal/i1;

    iget-object v4, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->k:Lcom/pspdfkit/internal/views/document/a;

    iget-object v5, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m:Lcom/pspdfkit/internal/ac;

    iget-object v6, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->f0:Lcom/pspdfkit/internal/i2;

    iget-object v7, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->b0:Lcom/pspdfkit/internal/fl;

    iget-object v8, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->c0:Lcom/pspdfkit/internal/pr;

    iget-object v9, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->T:Lcom/pspdfkit/internal/views/document/DocumentView$h;

    iget-object v10, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->f:Lcom/pspdfkit/internal/fm;

    iget-object v11, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->g:Lcom/pspdfkit/internal/fm;

    iget-object v12, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->x:Lcom/pspdfkit/internal/i;

    new-instance v13, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda10;

    invoke-direct {v13, p0}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    move-object v0, v14

    move-object v1, p0

    invoke-virtual/range {v0 .. v13}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/internal/views/document/a;Lcom/pspdfkit/internal/ac;Lcom/pspdfkit/internal/i2;Lcom/pspdfkit/internal/fl;Lcom/pspdfkit/internal/pr;Lcom/pspdfkit/internal/dm$c;Lcom/pspdfkit/internal/fm;Lcom/pspdfkit/internal/fm;Lcom/pspdfkit/internal/i;Lcom/pspdfkit/internal/il;)V

    .line 20
    new-instance v0, Lcom/pspdfkit/internal/views/document/DocumentView$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/views/document/DocumentView$c;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/views/document/DocumentView$c-IA;)V

    invoke-virtual {v14, v0}, Lcom/pspdfkit/internal/dm;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 22
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v14, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 23
    invoke-static {v14}, Lcom/pspdfkit/internal/ov;->c(Landroid/view/View;)V

    return-object v14
.end method

.method protected final computeHorizontalScrollOffset()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->f()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected final computeHorizontalScrollRange()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->g()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final computeScroll()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->k0:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 3
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 4
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/dm;

    .line 5
    invoke-virtual {v3, v1}, Lcom/pspdfkit/internal/dm;->a(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 8
    :cond_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->awakenScrollBars()Z

    .line 9
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->v()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->L:Z

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/pspdfkit/listeners/scrolling/ScrollState;->SETTLING:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    goto :goto_2

    :cond_3
    :goto_1
    sget-object v0, Lcom/pspdfkit/listeners/scrolling/ScrollState;->DRAGGED:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    :goto_2
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setScrollState(Lcom/pspdfkit/listeners/scrolling/ScrollState;)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->b:Lcom/pspdfkit/internal/h9;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h9;->b()V

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->v:Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;

    if-eqz v1, :cond_7

    .line 18
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->computeHorizontalScrollOffset()I

    move-result v3

    .line 19
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->computeVerticalScrollOffset()I

    move-result v4

    .line 20
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->computeHorizontalScrollRange()I

    move-result v5

    .line 21
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->computeVerticalScrollRange()I

    move-result v6

    .line 22
    invoke-virtual {p0}, Landroid/view/View;->computeHorizontalScrollExtent()I

    move-result v7

    .line 23
    invoke-virtual {p0}, Landroid/view/View;->computeVerticalScrollExtent()I

    move-result v8

    const/4 v2, 0x0

    .line 24
    invoke-interface/range {v1 .. v8}, Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;->onDocumentScrolled(Lcom/pspdfkit/ui/PdfFragment;IIIIII)V

    goto :goto_4

    :cond_4
    const/4 v0, 0x1

    .line 25
    iput v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->I:I

    .line 26
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    :goto_3
    if-ge v1, v2, :cond_6

    .line 27
    invoke-super {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/dm;

    .line 28
    invoke-virtual {v3}, Lcom/pspdfkit/internal/dm;->j()V

    .line 30
    invoke-virtual {v3}, Lcom/pspdfkit/internal/dm;->e()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getInteractionMode()Lcom/pspdfkit/internal/views/document/DocumentView$d;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/internal/views/document/DocumentView$d;->b:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-eq v4, v5, :cond_5

    .line 31
    invoke-virtual {v3}, Lcom/pspdfkit/internal/dm;->b()Z

    .line 33
    :cond_5
    invoke-virtual {v3, v0}, Lcom/pspdfkit/internal/dm;->a(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 36
    :cond_6
    sget-object v0, Lcom/pspdfkit/listeners/scrolling/ScrollState;->IDLE:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setScrollState(Lcom/pspdfkit/listeners/scrolling/ScrollState;)V

    .line 39
    :cond_7
    :goto_4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->b()V

    return-void
.end method

.method protected final computeVerticalScrollOffset()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->n()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected final computeVerticalScrollRange()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->o()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final bridge synthetic create()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->c()Lcom/pspdfkit/internal/dm;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ug;->i(I)F

    move-result p1

    return p1

    :cond_0
    const/high16 p1, 0x3f800000    # 1.0f

    return p1
.end method

.method public final d()V
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->b:Lcom/pspdfkit/internal/h9;

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/h9;->a()V

    :cond_0
    return-void
.end method

.method protected final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->l0:Lcom/pspdfkit/internal/uh;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/uh;->a(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 10
    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public final e()V
    .locals 4

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->b(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    sget-object v1, Lcom/pspdfkit/internal/views/document/DocumentView$d;->f:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->exitCurrentlyActiveMode()V

    .line 4
    :cond_0
    iput-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const/4 v0, 0x0

    .line 5
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_2

    .line 6
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/dm;

    .line 7
    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->e()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 8
    iget-object v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->G:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V

    goto :goto_1

    .line 10
    :cond_1
    new-instance v3, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda6;

    invoke-direct {v3, p0, v2}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/dm;)V

    invoke-virtual {p0, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void

    .line 14
    :cond_3
    new-instance v0, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string v1, "Entering content editing mode is not permitted, either by the license or configuration."

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final enterAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 4

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->R:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->b:Lcom/pspdfkit/internal/h9;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h9;->a()V

    .line 3
    sget-object v0, Lcom/pspdfkit/internal/views/document/DocumentView$d;->b:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const/4 v0, 0x0

    .line 4
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 5
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/dm;

    .line 6
    iget-object v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->D:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 7
    invoke-virtual {v2, v3, p1, p2}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 10
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p2

    const-string v0, "enter_annotation_creation_mode"

    .line 11
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p2

    .line 12
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p1

    const-string v0, "annotation_tool"

    invoke-virtual {p2, v0, p1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void

    .line 15
    :cond_1
    new-instance p2, Lcom/pspdfkit/exceptions/PSPDFKitException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Entering annotation creation mode for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " is not permitted, either by the license or configuration."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final exitCurrentlyActiveMode()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->b:Lcom/pspdfkit/internal/h9;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h9;->a()V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->k()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    const/4 v2, 0x0

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    goto :goto_1

    .line 21
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->b()V

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->H:Lcom/pspdfkit/internal/specialMode/handler/c;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/specialMode/handler/c;->a(Lcom/pspdfkit/forms/FormElement;)V

    goto :goto_1

    .line 23
    :cond_2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->a()Z

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->F:Lcom/pspdfkit/internal/specialMode/handler/b;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/specialMode/handler/b;->a(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_1

    .line 28
    :cond_3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    .line 29
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    const-string v2, "exit_annotation_creation_mode"

    .line 30
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    if-eqz v0, :cond_4

    .line 32
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    const-string v0, "null"

    :goto_0
    const-string v2, "annotation_tool"

    .line 33
    invoke-virtual {v1, v2, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 47
    :goto_1
    sget-object v0, Lcom/pspdfkit/internal/views/document/DocumentView$d;->a:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const/4 v0, 0x0

    .line 48
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    :goto_2
    if-ge v0, v1, :cond_5

    .line 49
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/dm;

    .line 50
    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->c()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    return-void
.end method

.method public final g()V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m0:Lcom/pspdfkit/internal/ni;

    if-nez v0, :cond_0

    return-void

    .line 26
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ni;->a()V

    return-void
.end method

.method public final g(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->V:I

    if-eq p1, v0, :cond_3

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    const-string v2, "change_page"

    .line 5
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    iget v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->V:I

    const-string v3, "page_index"

    .line 6
    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    const-string v2, "target_page_index"

    .line 7
    invoke-virtual {v1, p1, v2}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    .line 8
    invoke-virtual {v1}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 11
    :cond_1
    iput p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->V:I

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    sget-object v2, Lcom/pspdfkit/internal/views/document/DocumentView$d;->c:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-ne v1, v2, :cond_2

    .line 14
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->exitCurrentlyActiveMode()V

    .line 20
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->o0:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 22
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->t:Lcom/pspdfkit/listeners/DocumentListener;

    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    .line 24
    new-instance v0, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, v1, p1}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/listeners/DocumentListener;I)V

    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_3
    return-void
.end method

.method public getActionResolver()Lcom/pspdfkit/annotations/actions/ActionResolver;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->x:Lcom/pspdfkit/internal/i;

    return-object v0
.end method

.method public getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->D:Lcom/pspdfkit/internal/specialMode/handler/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    return-object v0
.end method

.method public getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->D:Lcom/pspdfkit/internal/specialMode/handler/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    return-object v0
.end method

.method public getAnnotationCreationHandler()Lcom/pspdfkit/internal/specialMode/handler/a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->D:Lcom/pspdfkit/internal/specialMode/handler/a;

    return-object v0
.end method

.method public getAnnotationEditingHandler()Lcom/pspdfkit/internal/specialMode/handler/b;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->F:Lcom/pspdfkit/internal/specialMode/handler/b;

    return-object v0
.end method

.method public getAnnotationListeners()Lcom/pspdfkit/internal/i1;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->j:Lcom/pspdfkit/internal/i1;

    return-object v0
.end method

.method public final getChildAt(I)Landroid/view/View;
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/dm;

    return-object p1
.end method

.method public getContentEditingHandler()Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->G:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    return-object v0
.end method

.method public getContentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->l:Lcom/pspdfkit/internal/b7;

    return-object v0
.end method

.method public getContentEditingState()Lcom/pspdfkit/internal/w6;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getInteractionMode()Lcom/pspdfkit/internal/views/document/DocumentView$d;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/views/document/DocumentView$d;->f:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/pspdfkit/internal/w6;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/w6;-><init>(I)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDocument()Lcom/pspdfkit/internal/zf;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    return-object v0
.end method

.method public getFormListeners()Lcom/pspdfkit/internal/ac;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m:Lcom/pspdfkit/internal/ac;

    return-object v0
.end method

.method public getInteractionMode()Lcom/pspdfkit/internal/views/document/DocumentView$d;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    return-object v0
.end method

.method public getMagnifierManager()Lcom/pspdfkit/internal/uh;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->l0:Lcom/pspdfkit/internal/uh;

    return-object v0
.end method

.method public getMediaContentStates()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/xi;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->U:Lcom/pspdfkit/internal/zi;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zi;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    return-object v0
.end method

.method public getOverlaidAnnotationTypes()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->d0:Ljava/util/EnumSet;

    return-object v0
.end method

.method public getOverlaidAnnotations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->e0:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPage()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->c()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getPageCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    return v0
.end method

.method public getSelectedAnnotations()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 3
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/dm;

    .line 4
    invoke-virtual {v3}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/am;->d()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getSelectedFormElement()Lcom/pspdfkit/forms/FormElement;
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 2
    invoke-super {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/dm;

    .line 3
    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->getFormEditor()Lcom/pspdfkit/internal/tb;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/tb;->e()Lcom/pspdfkit/forms/FormElement;

    move-result-object v2

    if-eqz v2, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 2
    invoke-super {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/dm;

    .line 3
    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v2

    if-eqz v2, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTextSelectionListeners()Lcom/pspdfkit/internal/yt;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->n:Lcom/pspdfkit/internal/yt;

    return-object v0
.end method

.method public getTextSelectionSpecialModeHandler()Lcom/pspdfkit/internal/specialMode/handler/e;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    return-object v0
.end method

.method public getViewState()Lcom/pspdfkit/internal/ug$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->p()Lcom/pspdfkit/internal/ug$a;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getVisiblePages()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 4
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 5
    invoke-super {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/dm;

    .line 6
    invoke-virtual {v4, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 7
    invoke-virtual {v4}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public final j()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->L:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->M:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->k0:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ug;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final k()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    sget-object v1, Lcom/pspdfkit/internal/views/document/DocumentView$d;->a:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final l()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->k0:Z

    return v0
.end method

.method public final m()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->J:Z

    return v0
.end method

.method public final n()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->K:Z

    return v0
.end method

.method public final o()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result v0

    if-gez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->d(I)F

    move-result v1

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->t:Lcom/pspdfkit/listeners/DocumentListener;

    if-eqz v2, :cond_1

    iget v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->P:F

    cmpl-float v3, v1, v3

    if-eqz v3, :cond_1

    .line 5
    iget-object v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-interface {v2, v3, v0, v1}, Lcom/pspdfkit/listeners/DocumentListener;->onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V

    .line 7
    :cond_1
    iput v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->P:F

    return-void
.end method

.method public final onAfterTextSelectionChange(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/datastructures/TextSelection;)V
    .locals 0

    if-eqz p2, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->b:Lcom/pspdfkit/internal/h9;

    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/h9;->a(Lcom/pspdfkit/internal/specialMode/handler/e;)V

    :cond_0
    return-void
.end method

.method protected final onAttachedToWindow()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->l0:Lcom/pspdfkit/internal/uh;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/uh;->i()V

    .line 4
    :cond_0
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    return-void
.end method

.method public final onBeforeTextSelectionChange(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/datastructures/TextSelection;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->l0:Lcom/pspdfkit/internal/uh;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/uh;->j()V

    .line 4
    :cond_0
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->t()V

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->j0:Lcom/pspdfkit/internal/views/document/DocumentView$e;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/document/DocumentView$e;->a()V

    .line 4
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    return v1

    .line 9
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    const/4 v2, 0x1

    if-eqz v0, :cond_10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v3, 0x3

    if-lt v0, v3, :cond_2

    goto/16 :goto_6

    .line 12
    :cond_2
    iput-boolean v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->N:Z

    .line 14
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 15
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 16
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    .line 17
    iget v4, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->q:I

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v4

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    if-eqz v3, :cond_d

    const/4 v5, 0x2

    if-eq v3, v5, :cond_5

    const/4 v4, 0x6

    if-eq v3, v4, :cond_3

    goto :goto_0

    .line 58
    :cond_3
    iget v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->q:I

    if-eq v0, v3, :cond_4

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ug;->a(Z)V

    .line 60
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->M:Z

    goto/16 :goto_5

    .line 71
    :cond_4
    :goto_0
    iget v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->q:I

    if-ne v0, v3, :cond_e

    .line 72
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/ug;->a(Z)V

    .line 73
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->M:Z

    .line 74
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->L:Z

    goto/16 :goto_5

    .line 75
    :cond_5
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    .line 76
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    .line 79
    iget v4, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->r:F

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 80
    iget v4, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->s:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 83
    iget v4, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->o:I

    int-to-float v4, v4

    cmpl-float v0, v0, v4

    if-gtz v0, :cond_7

    cmpl-float v0, v3, v4

    if-lez v0, :cond_6

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    :cond_7
    :goto_1
    const/4 v0, 0x1

    .line 86
    :goto_2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->k()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    sget-object v4, Lcom/pspdfkit/internal/views/document/DocumentView$d;->d:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-eq v3, v4, :cond_b

    sget-object v4, Lcom/pspdfkit/internal/views/document/DocumentView$d;->e:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-eq v3, v4, :cond_b

    sget-object v4, Lcom/pspdfkit/internal/views/document/DocumentView$d;->f:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-eq v3, v4, :cond_b

    sget-object v4, Lcom/pspdfkit/internal/views/document/DocumentView$d;->b:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-ne v3, v4, :cond_8

    iget-object v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->D:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 91
    invoke-virtual {v3}, Lcom/pspdfkit/internal/specialMode/handler/a;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NONE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq v3, v4, :cond_b

    :cond_8
    iget-object v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->C:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    sget-object v4, Lcom/pspdfkit/internal/views/document/DocumentView$d;->c:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-ne v3, v4, :cond_9

    iget-object v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->E:Lcom/pspdfkit/internal/specialMode/handler/e;

    .line 92
    invoke-virtual {v3}, Lcom/pspdfkit/internal/specialMode/handler/e;->c()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 93
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result p1

    if-ne p1, v5, :cond_a

    goto :goto_3

    :cond_a
    const/4 p1, 0x0

    goto :goto_4

    :cond_b
    :goto_3
    const/4 p1, 0x1

    :goto_4
    if-eqz p1, :cond_c

    if-eqz v0, :cond_c

    const/4 v1, 0x1

    :cond_c
    return v1

    .line 94
    :cond_d
    iput-boolean v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->L:Z

    .line 97
    iput v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->q:I

    .line 100
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->r:F

    .line 101
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->s:F

    .line 152
    :cond_e
    :goto_5
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->z:Lcom/pspdfkit/internal/q7;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/q7;->a(Landroid/view/MotionEvent;)V

    .line 153
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->M:Z

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->y:Lcom/pspdfkit/internal/o7;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/o7;->a(Landroid/view/MotionEvent;)Z

    :cond_f
    return v1

    :cond_10
    :goto_6
    return v2
.end method

.method protected final onLayout(ZIIII)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    if-eqz p1, :cond_5

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-nez p1, :cond_0

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->i()V

    .line 8
    :cond_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    if-nez p1, :cond_1

    .line 9
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->f()V

    .line 10
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    const-string p2, "Layout manager cannot be null."

    const-string p3, "message"

    .line 11
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_4

    .line 12
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    const/4 p4, 0x0

    const/4 p5, 0x0

    :goto_0
    if-ge p5, p1, :cond_2

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    .line 16
    invoke-super {p0, p5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/dm;

    .line 17
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ug;->b(Lcom/pspdfkit/internal/dm;)V

    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    .line 18
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    .line 19
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_3

    .line 20
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    :goto_1
    if-ge p4, p1, :cond_5

    .line 21
    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    .line 22
    invoke-super {p0, p4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/pspdfkit/internal/dm;

    .line 23
    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/ug;->a(Lcom/pspdfkit/internal/dm;)V

    add-int/lit8 p4, p4, 0x1

    goto :goto_1

    .line 24
    :cond_3
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 25
    :cond_4
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    return-void
.end method

.method public final onProvideStructure(Landroid/view/ViewStructure;)V
    .locals 5

    .line 1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onProvideStructure(Landroid/view/ViewStructure;)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    goto :goto_1

    .line 7
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v1

    .line 9
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getVisiblePages()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 13
    invoke-virtual {p0, v2, v4}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Landroid/graphics/RectF;I)Z

    .line 14
    invoke-virtual {v1, v4, v2}, Lcom/pspdfkit/internal/zf;->getPageText(ILandroid/graphics/RectF;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\n"

    .line 15
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 17
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ViewStructure;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_1
    return-void
.end method

.method protected final onSizeChanged(IIII)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    if-eqz p1, :cond_8

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    const/4 p2, 0x0

    if-eqz p1, :cond_3

    iget p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->W:I

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p3

    iget p3, p3, Landroid/content/res/Configuration;->orientation:I

    if-eq p1, p3, :cond_0

    goto :goto_1

    .line 3
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result p3

    sub-int/2addr p1, p3

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result p3

    sub-int/2addr p1, p3

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p3

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result p4

    sub-int/2addr p3, p4

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result p4

    sub-int/2addr p3, p4

    .line 5
    iget-object p4, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {p4}, Lcom/pspdfkit/internal/ug;->m()I

    move-result p4

    if-ne p4, p1, :cond_1

    iget-object p4, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {p4}, Lcom/pspdfkit/internal/ug;->l()I

    move-result p4

    if-eq p4, p3, :cond_2

    .line 6
    :cond_1
    iget-object p4, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {p4, p1, p3}, Lcom/pspdfkit/internal/ug;->f(II)V

    .line 10
    :cond_2
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    if-ge p2, p1, :cond_7

    .line 11
    invoke-super {p0, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/dm;

    .line 12
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 13
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->i()V

    .line 14
    :goto_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    if-lez p1, :cond_6

    .line 15
    invoke-super {p0, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/dm;

    .line 16
    iget-object p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->U:Lcom/pspdfkit/internal/zi;

    if-eqz p3, :cond_4

    .line 17
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getMediaPlayer()Lcom/pspdfkit/internal/yi;

    move-result-object p4

    invoke-virtual {p3, p4}, Lcom/pspdfkit/internal/zi;->b(Lcom/pspdfkit/internal/yi;)V

    :cond_4
    const/16 p3, 0x8

    .line 20
    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    .line 21
    iget-object p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->w:Lcom/pspdfkit/internal/no;

    invoke-virtual {p3, p1}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/mo;)V

    .line 24
    :try_start_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->d()Z

    move-result p3

    if-eqz p3, :cond_5

    .line 25
    iget-object p3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h0:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    :catch_0
    :cond_5
    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    goto :goto_2

    .line 27
    :cond_6
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->f()V

    .line 32
    :cond_7
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    iput p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->W:I

    :cond_8
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v2, 0x3

    if-lt v0, v2, :cond_1

    goto/16 :goto_3

    .line 8
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 9
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 10
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    .line 11
    iget v4, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->q:I

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v4

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    const/4 v5, 0x1

    if-eqz v3, :cond_4

    if-eq v3, v5, :cond_3

    if-eq v3, v2, :cond_3

    .line 32
    iget-boolean v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->O:Z

    if-eqz v2, :cond_2

    .line 33
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 35
    :cond_2
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->N:Z

    goto :goto_0

    .line 36
    :cond_3
    iget-boolean v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->O:Z

    if-eqz v2, :cond_5

    .line 37
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Landroid/view/MotionEvent;)Z

    .line 38
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->N:Z

    .line 39
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->O:Z

    goto :goto_0

    .line 40
    :cond_4
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Landroid/view/MotionEvent;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->O:Z

    .line 41
    iput-boolean v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->N:Z

    :cond_5
    :goto_0
    if-eqz v3, :cond_b

    const/4 v2, 0x2

    if-eq v3, v2, :cond_8

    const/4 v2, 0x5

    if-eq v3, v2, :cond_a

    const/4 v2, 0x6

    if-eq v3, v2, :cond_6

    goto :goto_1

    .line 91
    :cond_6
    iget v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->q:I

    if-eq v0, v2, :cond_7

    .line 92
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ug;->a(Z)V

    .line 93
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->M:Z

    goto :goto_2

    .line 104
    :cond_7
    :goto_1
    iget v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->q:I

    if-ne v0, v2, :cond_a

    .line 105
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {v0, v5}, Lcom/pspdfkit/internal/ug;->a(Z)V

    .line 106
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->M:Z

    .line 107
    iput-boolean v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->L:Z

    goto :goto_2

    .line 108
    :cond_8
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    .line 109
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    .line 112
    iget v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->r:F

    sub-float v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 113
    iget v3, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->s:F

    sub-float v3, v1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 116
    iget v4, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->o:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_9

    cmpl-float v2, v3, v4

    if-lez v2, :cond_a

    .line 117
    :cond_9
    iput v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->r:F

    .line 118
    iput v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->s:F

    .line 146
    :cond_a
    :goto_2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->z:Lcom/pspdfkit/internal/q7;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/q7;->a(Landroid/view/MotionEvent;)V

    .line 147
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->M:Z

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->y:Lcom/pspdfkit/internal/o7;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/o7;->a(Landroid/view/MotionEvent;)Z

    :cond_b
    return v5

    :cond_c
    :goto_3
    return v1
.end method

.method public final p()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result v0

    const/4 v1, 0x0

    .line 3
    iput-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    .line 6
    new-instance v1, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda9;

    invoke-direct {v1, p0, v0}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;I)V

    invoke-static {p0, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 17
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public final r()V
    .locals 4

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->g0:Z

    .line 2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 3
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/dm;

    .line 4
    invoke-direct {p0, v3, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/internal/dm;Z)V

    .line 5
    invoke-direct {p0, v3}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Lcom/pspdfkit/internal/dm;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 6
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 7
    new-instance v0, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    const-wide/32 v1, 0xea60

    invoke-virtual {p0, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final removeDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V
    .locals 2

    const-string v0, "drawableProvider"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->f:Lcom/pspdfkit/internal/fm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/fm;->b(Lcom/pspdfkit/ui/PageObjectProvider;)V

    return-void
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 3
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->L:Z

    :cond_0
    return-void
.end method

.method public final s()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->q()V

    return-void
.end method

.method public setDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->t:Lcom/pspdfkit/listeners/DocumentListener;

    return-void
.end method

.method public setDocumentScrollListener(Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->v:Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;

    return-void
.end method

.method public setMediaContentStates(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/xi;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->U:Lcom/pspdfkit/internal/zi;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zi;->a(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public setOnDocumentInteractionListener(Lcom/pspdfkit/internal/views/document/DocumentView$e;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->j0:Lcom/pspdfkit/internal/views/document/DocumentView$e;

    return-void
.end method

.method public setOnDocumentLongPressListener(Lcom/pspdfkit/listeners/OnDocumentLongPressListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->u:Lcom/pspdfkit/listeners/OnDocumentLongPressListener;

    return-void
.end method

.method public setOnPreparePopupToolbarListener(Lcom/pspdfkit/listeners/OnPreparePopupToolbarListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->b:Lcom/pspdfkit/internal/h9;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/h9;->a(Lcom/pspdfkit/listeners/OnPreparePopupToolbarListener;)V

    return-void
.end method

.method public setOverlaidAnnotationTypes(Ljava/util/EnumSet;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->d0:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->d0:Ljava/util/EnumSet;

    .line 3
    sget-object v1, Lcom/pspdfkit/internal/tl;->a:Ljava/util/EnumSet;

    .line 4
    const-class v1, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 5
    invoke-virtual {p1}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/AnnotationType;

    .line 6
    invoke-static {v2}, Lcom/pspdfkit/internal/tl;->a(Lcom/pspdfkit/annotations/AnnotationType;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 7
    invoke-virtual {v1, v2}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 10
    :cond_1
    sget-object p1, Lcom/pspdfkit/internal/tl;->b:Ljava/util/EnumSet;

    invoke-virtual {v1, p1}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    .line 11
    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    const/4 p1, 0x0

    .line 15
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    :goto_1
    if-ge p1, v0, :cond_2

    .line 16
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/dm;

    .line 17
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Lcom/pspdfkit/internal/dm;)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method public setOverlaidAnnotations(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->e0:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->e0:Ljava/util/ArrayList;

    sget-object v1, Lcom/pspdfkit/internal/tl;->a:Ljava/util/EnumSet;

    .line 3
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 4
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    .line 5
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    invoke-static {v3}, Lcom/pspdfkit/internal/tl;->a(Lcom/pspdfkit/annotations/AnnotationType;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 6
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 7
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 p1, 0x0

    .line 10
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    :goto_1
    if-ge p1, v0, :cond_2

    .line 11
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/dm;

    const/4 v2, 0x1

    .line 12
    invoke-direct {p0, v1, v2}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/internal/dm;Z)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method public setPage(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ug;->j(I)V

    :cond_0
    return-void
.end method

.method public setRedactionAnnotationPreviewEnabled(Z)V
    .locals 2

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->c:Z

    const/4 v0, 0x0

    .line 4
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 5
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/dm;

    if-eqz v1, :cond_0

    .line 6
    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/dm;->setRedactionAnnotationPreviewEnabled(Z)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 10
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    if-eqz p1, :cond_2

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 12
    invoke-static {p1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->Q:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->REDACT:Lcom/pspdfkit/annotations/AnnotationType;

    .line 14
    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    check-cast p1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/r1;->getAllAnnotationsOfTypeAsync(Ljava/util/EnumSet;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 15
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Observable;->toList()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 16
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/views/document/DocumentView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    .line 17
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_2
    return-void
.end method

.method public setScrollingEnabled(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->J:Z

    return-void
.end method

.method public setViewState(Lcom/pspdfkit/internal/ug$a;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ug;->b(Lcom/pspdfkit/internal/ug$a;)V

    :cond_0
    return-void
.end method

.method public setZoomingEnabled(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->K:Z

    return-void
.end method

.method public final t()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->o0:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 5
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    .line 6
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/dm;

    .line 7
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->U:Lcom/pspdfkit/internal/zi;

    if-eqz v2, :cond_0

    .line 8
    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm;->getMediaPlayer()Lcom/pspdfkit/internal/yi;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/zi;->b(Lcom/pspdfkit/internal/yi;)V

    :cond_0
    const/16 v2, 0x8

    .line 11
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 12
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->w:Lcom/pspdfkit/internal/no;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/mo;)V

    .line 15
    :try_start_0
    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 16
    iget-object v2, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->h0:Ljava/util/HashSet;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 17
    :catch_0
    :cond_1
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->removeViewAt(I)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final u()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->j:Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/i1;->a()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->m:Lcom/pspdfkit/internal/ac;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ac;->a()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->n:Lcom/pspdfkit/internal/yt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/yt;->a()V

    const/4 v0, 0x0

    .line 5
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->i0:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/nh;->clear()V

    .line 7
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setOnDocumentInteractionListener(Lcom/pspdfkit/internal/views/document/DocumentView$e;)V

    .line 8
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setDocumentScrollListener(Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;)V

    .line 9
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setOnDocumentLongPressListener(Lcom/pspdfkit/listeners/OnDocumentLongPressListener;)V

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/DocumentView;->b:Lcom/pspdfkit/internal/h9;

    if-eqz v1, :cond_0

    .line 13
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setOnPreparePopupToolbarListener(Lcom/pspdfkit/listeners/OnPreparePopupToolbarListener;)V

    :cond_0
    return-void
.end method

.class public Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/hu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$b;,
        Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/iu;

.field private final c:Landroidx/recyclerview/widget/ItemTouchHelper;

.field private final d:Lcom/pspdfkit/internal/views/document/editor/b;

.field private e:I

.field private f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

.field private g:Lcom/pspdfkit/internal/gu;

.field private h:Lcom/pspdfkit/internal/zf;

.field private i:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private j:Z

.field private k:Z

.field private final l:Lio/reactivex/rxjava3/processors/BehaviorProcessor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/processors/BehaviorProcessor<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;>;"
        }
    .end annotation
.end field

.field private final m:Lio/reactivex/rxjava3/processors/BehaviorProcessor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/processors/BehaviorProcessor<",
            "Lcom/pspdfkit/internal/jl<",
            "Lcom/pspdfkit/internal/gu;",
            ">;>;"
        }
    .end annotation
.end field

.field private n:Z

.field private o:Ljava/lang/Integer;


# direct methods
.method public static synthetic $r8$lambda$0cz_nCiTNPhs17ZgvxGZT0iNM1Q(Landroid/util/Pair;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a(Landroid/util/Pair;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/iu;

    invoke-direct {v0}, Lcom/pspdfkit/internal/iu;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->b:Lcom/pspdfkit/internal/iu;

    .line 6
    new-instance v0, Landroidx/recyclerview/widget/ItemTouchHelper;

    new-instance v1, Lcom/pspdfkit/internal/ku;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ku;-><init>(Lcom/pspdfkit/internal/hu;)V

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/ItemTouchHelper;-><init>(Landroidx/recyclerview/widget/ItemTouchHelper$Callback;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->c:Landroidx/recyclerview/widget/ItemTouchHelper;

    .line 9
    new-instance v0, Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-direct {v0}, Lcom/pspdfkit/internal/views/document/editor/b;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    .line 33
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->l:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 35
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->m:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 45
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 46
    invoke-direct {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    new-instance p2, Lcom/pspdfkit/internal/iu;

    invoke-direct {p2}, Lcom/pspdfkit/internal/iu;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->b:Lcom/pspdfkit/internal/iu;

    .line 51
    new-instance p2, Landroidx/recyclerview/widget/ItemTouchHelper;

    new-instance v0, Lcom/pspdfkit/internal/ku;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ku;-><init>(Lcom/pspdfkit/internal/hu;)V

    invoke-direct {p2, v0}, Landroidx/recyclerview/widget/ItemTouchHelper;-><init>(Landroidx/recyclerview/widget/ItemTouchHelper$Callback;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->c:Landroidx/recyclerview/widget/ItemTouchHelper;

    .line 54
    new-instance p2, Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-direct {p2}, Lcom/pspdfkit/internal/views/document/editor/b;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    .line 78
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->l:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 80
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->m:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 95
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 96
    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 97
    new-instance p2, Lcom/pspdfkit/internal/iu;

    invoke-direct {p2}, Lcom/pspdfkit/internal/iu;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->b:Lcom/pspdfkit/internal/iu;

    .line 101
    new-instance p2, Landroidx/recyclerview/widget/ItemTouchHelper;

    new-instance p3, Lcom/pspdfkit/internal/ku;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/ku;-><init>(Lcom/pspdfkit/internal/hu;)V

    invoke-direct {p2, p3}, Landroidx/recyclerview/widget/ItemTouchHelper;-><init>(Landroidx/recyclerview/widget/ItemTouchHelper$Callback;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->c:Landroidx/recyclerview/widget/ItemTouchHelper;

    .line 104
    new-instance p2, Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-direct {p2}, Lcom/pspdfkit/internal/views/document/editor/b;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    .line 128
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->l:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 130
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->m:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 150
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 13

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :cond_0
    const/4 v0, 0x5

    .line 3
    :goto_0
    iput v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->e:I

    .line 5
    new-instance v2, Landroidx/recyclerview/widget/GridLayoutManager;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v0, v1, v3}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;IIZ)V

    .line 6
    invoke-virtual {p0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 12
    new-instance p1, Landroid/view/animation/ScaleAnimation;

    const v5, 0x3c23d70a    # 0.01f

    const/high16 v6, 0x3f800000    # 1.0f

    const v7, 0x3c23d70a    # 0.01f

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x1

    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v11, 0x1

    const/high16 v12, 0x3f000000    # 0.5f

    move-object v4, p1

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 14
    new-instance v0, Landroidx/interpolator/view/animation/LinearOutSlowInInterpolator;

    invoke-direct {v0}, Landroidx/interpolator/view/animation/LinearOutSlowInInterpolator;-><init>()V

    invoke-virtual {p1, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const-wide/16 v0, 0xe1

    .line 15
    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 17
    new-instance v0, Landroid/view/animation/GridLayoutAnimationController;

    const v1, 0x3e99999a    # 0.3f

    invoke-direct {v0, p1, v1, v1}, Landroid/view/animation/GridLayoutAnimationController;-><init>(Landroid/view/animation/Animation;FF)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->m:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->l:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->getCombiner()Lio/reactivex/rxjava3/functions/BiFunction;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lio/reactivex/rxjava3/core/Flowable;->combineLatest(Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lio/reactivex/rxjava3/functions/BiFunction;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    .line 20
    invoke-static {}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->e()Lio/reactivex/rxjava3/functions/Consumer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Flowable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private static a(Landroid/util/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 21
    iget-object v0, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/pspdfkit/internal/jl;

    .line 22
    iget-object v0, v0, Lcom/pspdfkit/internal/jl;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    .line 23
    check-cast v0, Lcom/pspdfkit/internal/gu;

    iget-object p0, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p0, Ljava/util/List;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/gu;->a(Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method private c()Lcom/pspdfkit/internal/gu;
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->h:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->i:Lcom/pspdfkit/configuration/PdfConfiguration;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 6
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/gu;

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->h:Lcom/pspdfkit/internal/zf;

    iget-object v4, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->b:Lcom/pspdfkit/internal/iu;

    iget-object v5, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    iget-object v6, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    iget-object v7, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->i:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v8, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->e:I

    sget v9, Lcom/pspdfkit/internal/ju;->k:I

    .line 14
    div-int v8, v1, v8

    .line 15
    iget-boolean v9, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->j:Z

    iget-boolean v10, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->n:Z

    move-object v1, v0

    invoke-direct/range {v1 .. v10}, Lcom/pspdfkit/internal/gu;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/iu;Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;Lcom/pspdfkit/internal/views/document/editor/b;Lcom/pspdfkit/configuration/PdfConfiguration;IZZ)V

    .line 18
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 19
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Lcom/pspdfkit/internal/gu;->a(ILandroidx/recyclerview/widget/RecyclerView;)V

    .line 22
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->m:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    new-instance v2, Lcom/pspdfkit/internal/jl;

    invoke-direct {v2, v0}, Lcom/pspdfkit/internal/jl;-><init>(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->onNext(Ljava/lang/Object;)V

    .line 25
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->h:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->getPageBinding()Lcom/pspdfkit/document/PageBinding;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/document/PageBinding;->RIGHT_EDGE:Lcom/pspdfkit/document/PageBinding;

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    .line 26
    invoke-virtual {p0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutDirection(I)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 28
    invoke-virtual {p0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutDirection(I)V

    :goto_0
    return-object v0

    .line 29
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->m:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    new-instance v1, Lcom/pspdfkit/internal/jl;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/pspdfkit/internal/jl;-><init>(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->onNext(Ljava/lang/Object;)V

    return-object v2
.end method

.method private static e()Lio/reactivex/rxjava3/functions/Consumer;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$$ExternalSyntheticLambda2;

    invoke-direct {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$$ExternalSyntheticLambda2;-><init>()V

    return-object v0
.end method

.method private getCombiner()Lio/reactivex/rxjava3/functions/BiFunction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/functions/BiFunction<",
            "Lcom/pspdfkit/internal/jl<",
            "Lcom/pspdfkit/internal/gu;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;",
            "Landroid/util/Pair<",
            "Lcom/pspdfkit/internal/jl<",
            "Lcom/pspdfkit/internal/gu;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;>;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$$ExternalSyntheticLambda1;

    invoke-direct {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$$ExternalSyntheticLambda1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    .line 24
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 25
    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    return-void
.end method

.method public final a(I)V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {v0}, Lcom/pspdfkit/internal/gu;->getItemCount()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRangeChanged(II)V

    .line 64
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/editor/b;->a()V

    .line 66
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {v0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemMoved(II)V

    .line 44
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 45
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 48
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/editor/b;->a()V

    :cond_0
    return-void
.end method

.method public final a(IZ)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemInserted(I)V

    .line 38
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/editor/b;->a()V

    if-eqz p2, :cond_0

    .line 41
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_0
    return-void
.end method

.method public final a(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 1

    .line 67
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p1

    .line 68
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p2

    .line 71
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/views/document/editor/b;->a(II)V

    .line 74
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    if-eqz v0, :cond_0

    .line 75
    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;->onPageMoved(II)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_1

    .line 80
    invoke-virtual {v0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemMoved(II)V

    :cond_1
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/jni/NativeDocumentEditor;)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->h:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-nez v0, :cond_0

    goto :goto_0

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->c:Landroidx/recyclerview/widget/ItemTouchHelper;

    invoke-virtual {v0, p0}, Landroidx/recyclerview/widget/ItemTouchHelper;->attachToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    invoke-virtual {v0, p1, p0}, Lcom/pspdfkit/internal/gu;->a(Lcom/pspdfkit/internal/jni/NativeDocumentEditor;Landroidx/recyclerview/widget/RecyclerView;)V

    .line 31
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/views/document/editor/b;->b(Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 32
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->h:Lcom/pspdfkit/internal/zf;

    .line 33
    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->i:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 34
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->c()Lcom/pspdfkit/internal/gu;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    :cond_0
    return-void
.end method

.method public final a(Ljava/util/HashSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_1

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 51
    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 53
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 55
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemInserted(I)V

    goto :goto_0

    .line 59
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/editor/b;->a()V

    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .line 81
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->j:Z

    .line 82
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/gu;->b(Z)V

    .line 84
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method protected final attachLayoutAnimationParameters(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;II)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    instance-of v0, v0, Landroidx/recyclerview/widget/GridLayoutManager;

    if-eqz v0, :cond_1

    .line 2
    iget-object p1, p2, Landroid/view/ViewGroup$LayoutParams;->layoutAnimationParameters:Landroid/view/animation/LayoutAnimationController$AnimationParameters;

    check-cast p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;

    if-nez p1, :cond_0

    .line 6
    new-instance p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;

    invoke-direct {p1}, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;-><init>()V

    .line 7
    iput-object p1, p2, Landroid/view/ViewGroup$LayoutParams;->layoutAnimationParameters:Landroid/view/animation/LayoutAnimationController$AnimationParameters;

    .line 10
    :cond_0
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object p2

    check-cast p2, Landroidx/recyclerview/widget/GridLayoutManager;

    invoke-virtual {p2}, Landroidx/recyclerview/widget/GridLayoutManager;->getSpanCount()I

    move-result p2

    .line 12
    iput p4, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->count:I

    .line 13
    iput p3, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->index:I

    .line 14
    iput p2, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->columnsCount:I

    int-to-float p4, p4

    int-to-float v0, p2

    div-float/2addr p4, v0

    float-to-double v0, p4

    .line 15
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int p4, v0

    iput p4, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->rowsCount:I

    .line 16
    rem-int p4, p3, p2

    iput p4, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->column:I

    .line 17
    div-int/2addr p3, p2

    iput p3, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->row:I

    goto :goto_0

    .line 19
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroidx/recyclerview/widget/RecyclerView;->attachLayoutAnimationParameters(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;II)V

    :goto_0
    return-void
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/views/document/editor/b;->a(Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;)V

    return-void
.end method

.method public final b(I)V
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRemoved(I)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/editor/b;->a()V

    :cond_0
    return-void
.end method

.method public final b(Ljava/util/HashSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_1

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 9
    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 11
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRemoved(I)V

    goto :goto_0

    .line 17
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/editor/b;->a()V

    :cond_1
    return-void
.end method

.method public final c(I)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_0

    .line 31
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->c:Landroidx/recyclerview/widget/ItemTouchHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/ItemTouchHelper;->attachToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/gu;->c()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/editor/b;->b(Z)V

    return-void
.end method

.method public final d(I)V
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    :cond_0
    return-void
.end method

.method public final e(I)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/editor/b;->a(I)V

    return-void
.end method

.method public final f()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->k:Z

    return-void

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_1

    .line 6
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 7
    invoke-virtual {p0}, Landroid/view/ViewGroup;->startLayoutAnimation()V

    :cond_1
    return-void
.end method

.method public getSelectedPages()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/editor/b;->b()Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$b;

    if-nez v0, :cond_0

    .line 2
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 6
    :cond_0
    check-cast p1, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$b;

    .line 7
    invoke-virtual {p1}, Landroidx/customview/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 8
    iget-boolean v0, p1, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$b;->a:Z

    iput-boolean v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->n:Z

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    iget-object p1, p1, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$b;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/editor/b;->a(Ljava/util/HashSet;)V

    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/recyclerview/widget/RecyclerView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$b;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$b;-><init>(Landroid/os/Parcelable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/gu;->a()Z

    move-result v0

    iput-boolean v0, v1, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$b;->a:Z

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/editor/b;->b()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, v1, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$b;->b:Ljava/util/HashSet;

    return-object v1
.end method

.method protected final onSizeChanged(IIII)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroidx/recyclerview/widget/RecyclerView;->onSizeChanged(IIII)V

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p1

    if-lez p1, :cond_0

    iget-boolean p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->k:Z

    if-eqz p1, :cond_0

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->c()Lcom/pspdfkit/internal/gu;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    .line 4
    new-instance p1, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;)V

    invoke-virtual {p0, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    const/4 p1, 0x0

    .line 5
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->k:Z

    :cond_0
    return-void
.end method

.method public setDrawableProviders(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->l:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public setHighlightedItem(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1, p0}, Lcom/pspdfkit/internal/gu;->a(ILandroidx/recyclerview/widget/RecyclerView;)V

    goto :goto_0

    .line 4
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->o:Ljava/lang/Integer;

    :goto_0
    return-void
.end method

.method public setItemLabelBackground(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->b:Lcom/pspdfkit/internal/iu;

    iput p1, v0, Lcom/pspdfkit/internal/iu;->b:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->c()Lcom/pspdfkit/internal/gu;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    .line 3
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method

.method public setItemLabelTextStyle(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->b:Lcom/pspdfkit/internal/iu;

    iput p1, v0, Lcom/pspdfkit/internal/iu;->a:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->c()Lcom/pspdfkit/internal/gu;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    .line 3
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method

.method public setRedactionAnnotationPreviewEnabled(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->n:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->g:Lcom/pspdfkit/internal/gu;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/gu;->a(Z)V

    :cond_0
    return-void
.end method

.method public setSelectedPages(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/editor/b;->a()V

    .line 3
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 4
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/editor/b;->a(I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setThumbnailGridListener(Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/editor/b;->a(Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;)V

    return-void
.end method

.class public final enum Lcom/pspdfkit/internal/views/document/DocumentView$d;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/views/document/DocumentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/views/document/DocumentView$d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/pspdfkit/internal/views/document/DocumentView$d;

.field public static final enum b:Lcom/pspdfkit/internal/views/document/DocumentView$d;

.field public static final enum c:Lcom/pspdfkit/internal/views/document/DocumentView$d;

.field public static final enum d:Lcom/pspdfkit/internal/views/document/DocumentView$d;

.field public static final enum e:Lcom/pspdfkit/internal/views/document/DocumentView$d;

.field public static final enum f:Lcom/pspdfkit/internal/views/document/DocumentView$d;

.field private static final synthetic g:[Lcom/pspdfkit/internal/views/document/DocumentView$d;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const-string v1, "BROWSE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/views/document/DocumentView$d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/views/document/DocumentView$d;->a:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const-string v3, "ANNOTATION_CREATION"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/views/document/DocumentView$d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/views/document/DocumentView$d;->b:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    .line 3
    new-instance v3, Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const-string v5, "TEXT_SELECTION"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/views/document/DocumentView$d;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/views/document/DocumentView$d;->c:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    .line 4
    new-instance v5, Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const-string v7, "ANNOTATION_EDITING"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/views/document/DocumentView$d;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/views/document/DocumentView$d;->d:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    .line 5
    new-instance v7, Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const-string v9, "FORM_EDITING"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/views/document/DocumentView$d;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/views/document/DocumentView$d;->e:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    .line 6
    new-instance v9, Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const-string v11, "CONTENT_EDITING"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/views/document/DocumentView$d;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/views/document/DocumentView$d;->f:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    const/4 v11, 0x6

    new-array v11, v11, [Lcom/pspdfkit/internal/views/document/DocumentView$d;

    aput-object v0, v11, v2

    aput-object v1, v11, v4

    aput-object v3, v11, v6

    aput-object v5, v11, v8

    aput-object v7, v11, v10

    aput-object v9, v11, v12

    .line 7
    sput-object v11, Lcom/pspdfkit/internal/views/document/DocumentView$d;->g:[Lcom/pspdfkit/internal/views/document/DocumentView$d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/views/document/DocumentView$d;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/views/document/DocumentView$d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/views/document/DocumentView$d;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/views/document/DocumentView$d;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/views/document/DocumentView$d;->g:[Lcom/pspdfkit/internal/views/document/DocumentView$d;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/views/document/DocumentView$d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/views/document/DocumentView$d;

    return-object v0
.end method

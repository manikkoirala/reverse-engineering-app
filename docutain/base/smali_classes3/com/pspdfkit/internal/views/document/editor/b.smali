.class public final Lcom/pspdfkit/internal/views/document/editor/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/document/editor/b$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/views/document/editor/b$a;

.field private final b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

.field private d:Z

.field private e:Z

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/views/document/editor/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/views/document/editor/b$a;-><init>(Lcom/pspdfkit/internal/views/document/editor/b;Lcom/pspdfkit/internal/views/document/editor/b$a-IA;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->a:Lcom/pspdfkit/internal/views/document/editor/b$a;

    .line 8
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    const/4 v0, -0x1

    .line 16
    iput v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->f:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->a:Lcom/pspdfkit/internal/views/document/editor/b$a;

    .line 18
    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/editor/b$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/views/document/editor/b$a;)Landroid/util/SparseArray;

    move-result-object v1

    .line 19
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 20
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    .line 22
    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/editor/b$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/views/document/editor/b$a;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 23
    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/editor/b$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/views/document/editor/b$a;)Landroid/util/SparseArray;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    if-eqz v4, :cond_0

    .line 25
    invoke-virtual {v4}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/views/document/editor/a;

    if-eqz v4, :cond_0

    .line 27
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 28
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/views/document/editor/a;

    .line 29
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/views/document/editor/b;->b(Lcom/pspdfkit/internal/views/document/editor/a;)V

    goto :goto_1

    .line 30
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->c:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    if-eqz v0, :cond_3

    .line 31
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;->onPageSelectionStateChanged()V

    :cond_3
    return-void
.end method

.method final a(I)V
    .locals 4

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->a:Lcom/pspdfkit/internal/views/document/editor/b$a;

    .line 43
    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/editor/b$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/views/document/editor/b$a;)Landroid/util/SparseArray;

    move-result-object v1

    .line 44
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    goto :goto_1

    .line 50
    :cond_0
    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/views/document/editor/a;

    if-eqz v1, :cond_2

    .line 51
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v3

    if-eq v3, p1, :cond_1

    goto :goto_0

    :cond_1
    move-object v2, v1

    goto :goto_1

    .line 52
    :cond_2
    :goto_0
    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/editor/b$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/views/document/editor/b$a;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    :goto_1
    if-eqz v2, :cond_3

    .line 53
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/views/document/editor/b;->c(Lcom/pspdfkit/internal/views/document/editor/a;)V

    goto :goto_2

    .line 55
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Could not toggle selection for view holder at position "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " since no view holder for that position was known."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.ThumbnailGrid"

    invoke-static {v1, p1, v0}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_2
    return-void
.end method

.method final a(II)V
    .locals 3

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    if-eqz v0, :cond_1

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 14
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 15
    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method

.method final a(Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/b;->c:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/views/document/editor/a;)V
    .locals 4

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->a:Lcom/pspdfkit/internal/views/document/editor/b$a;

    .line 33
    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/editor/b$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/views/document/editor/b$a;)Landroid/util/SparseArray;

    move-result-object v1

    .line 34
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v2

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 37
    iget-object v1, v0, Lcom/pspdfkit/internal/views/document/editor/b$a;->b:Lcom/pspdfkit/internal/views/document/editor/b;

    iget v1, v1, Lcom/pspdfkit/internal/views/document/editor/b;->f:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v1

    iget-object v3, v0, Lcom/pspdfkit/internal/views/document/editor/b$a;->b:Lcom/pspdfkit/internal/views/document/editor/b;

    iget v3, v3, Lcom/pspdfkit/internal/views/document/editor/b;->f:I

    if-ne v1, v3, :cond_0

    .line 38
    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast v1, Lcom/pspdfkit/internal/ju;

    const/4 v3, 0x1

    .line 39
    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/ju;->setHighlighted(Z)V

    .line 40
    iget-object v0, v0, Lcom/pspdfkit/internal/views/document/editor/b$a;->b:Lcom/pspdfkit/internal/views/document/editor/b;

    iput v2, v0, Lcom/pspdfkit/internal/views/document/editor/b;->f:I

    .line 41
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/document/editor/b;->b(Lcom/pspdfkit/internal/views/document/editor/a;)V

    return-void
.end method

.method final a(Ljava/util/HashSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method final a(Z)V
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/editor/b;->d:Z

    return-void
.end method

.method public final b()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    return-object v0
.end method

.method public final b(Lcom/pspdfkit/internal/views/document/editor/a;)V
    .locals 2

    .line 19
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->e:Z

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/views/document/editor/a;->a(Z)V

    .line 21
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v0

    if-ltz v0, :cond_0

    .line 25
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->d:Z

    if-nez v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 27
    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast v1, Lcom/pspdfkit/internal/ju;

    .line 28
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ju;->isActivated()Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 29
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast p1, Lcom/pspdfkit/internal/ju;

    .line 30
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ju;->setActivated(Z)V

    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 5

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/views/document/editor/b;->e:Z

    if-nez p1, :cond_0

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/document/editor/b;->a()V

    goto :goto_2

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/editor/b;->a:Lcom/pspdfkit/internal/views/document/editor/b$a;

    .line 6
    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/editor/b$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/views/document/editor/b$a;)Landroid/util/SparseArray;

    move-result-object v0

    .line 7
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 8
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    .line 10
    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/editor/b$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/views/document/editor/b$a;)Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    .line 11
    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/editor/b$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/views/document/editor/b$a;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    if-eqz v3, :cond_1

    .line 13
    invoke-virtual {v3}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/views/document/editor/a;

    if-eqz v3, :cond_1

    .line 15
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 16
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/views/document/editor/a;

    .line 17
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/views/document/editor/b;->b(Lcom/pspdfkit/internal/views/document/editor/a;)V

    goto :goto_1

    :cond_3
    :goto_2
    return-void
.end method

.method final c(Lcom/pspdfkit/internal/views/document/editor/a;)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->e:Z

    if-eqz v0, :cond_2

    .line 2
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->b:Ljava/util/HashSet;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 9
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/editor/b;->c:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    if-eqz v0, :cond_1

    .line 10
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;->onPageSelectionStateChanged()V

    .line 13
    :cond_1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/views/document/editor/b;->b(Lcom/pspdfkit/internal/views/document/editor/a;)V

    :cond_2
    return-void
.end method

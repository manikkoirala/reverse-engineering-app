.class final Lcom/pspdfkit/internal/views/document/DocumentView$b;
.super Lcom/pspdfkit/internal/o7$c;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/q7$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/views/document/DocumentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/views/document/DocumentView;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-direct {p0}, Lcom/pspdfkit/internal/o7$c;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/views/document/DocumentView$b-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView$b;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/q7;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetK(Lcom/pspdfkit/internal/views/document/DocumentView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2
    iget-object v0, v0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/q7;->c()F

    move-result p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ug;->a(F)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/q7;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetK(Lcom/pspdfkit/internal/views/document/DocumentView;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q7;->c()F

    move-result v1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/q7;->a()F

    move-result v2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/q7;->b()F

    move-result p1

    invoke-virtual {v0, v1, v2, p1}, Lcom/pspdfkit/internal/ug;->a(FFF)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final c(Lcom/pspdfkit/internal/q7;)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetK(Lcom/pspdfkit/internal/views/document/DocumentView;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p1, 0x0

    .line 2
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fputM(Lcom/pspdfkit/internal/views/document/DocumentView;Z)V

    return p1

    .line 6
    :cond_0
    iget-object v1, v0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q7;->c()F

    move-result v2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/q7;->a()F

    move-result v3

    invoke-virtual {p1}, Lcom/pspdfkit/internal/q7;->b()F

    move-result p1

    invoke-virtual {v1, v2, v3, p1}, Lcom/pspdfkit/internal/ug;->b(FFF)Z

    move-result p1

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fputM(Lcom/pspdfkit/internal/views/document/DocumentView;Z)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetM(Lcom/pspdfkit/internal/views/document/DocumentView;)Z

    move-result p1

    return p1
.end method

.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->k()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetD(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/specialMode/handler/a;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    .line 4
    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MAGIC_INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->LINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SQUARE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->CIRCLE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->POLYGON:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->POLYLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->ERASER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-ne v0, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    .line 5
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetK(Lcom/pspdfkit/internal/views/document/DocumentView;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v0, v0, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    .line 6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    float-to-int p1, p1

    invoke-virtual {v0, v3, p1}, Lcom/pspdfkit/internal/ug;->c(II)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    return v1
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    const/4 v0, 0x1

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fputI(Lcom/pspdfkit/internal/views/document/DocumentView;I)V

    .line 3
    iget-object p1, p1, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ug;->x()V

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetM(Lcom/pspdfkit/internal/views/document/DocumentView;)Z

    move-result p2

    const/4 v0, 0x0

    if-nez p2, :cond_6

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetJ(Lcom/pspdfkit/internal/views/document/DocumentView;)Z

    move-result p2

    if-nez p2, :cond_0

    goto :goto_3

    .line 2
    :cond_0
    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetI(Lcom/pspdfkit/internal/views/document/DocumentView;)I

    move-result p2

    const/4 v1, 0x1

    const/4 v2, 0x2

    const/4 v3, 0x3

    if-ne p2, v1, :cond_3

    .line 3
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result p2

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v4, 0x40400000    # 3.0f

    mul-float v1, v1, v4

    cmpl-float p2, p2, v1

    if-lez p2, :cond_1

    .line 4
    invoke-static {p1, v2}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fputI(Lcom/pspdfkit/internal/views/document/DocumentView;I)V

    goto :goto_0

    .line 5
    :cond_1
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result p2

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float v1, v1, v4

    cmpl-float p2, p2, v1

    if-lez p2, :cond_2

    .line 6
    invoke-static {p1, v3}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fputI(Lcom/pspdfkit/internal/views/document/DocumentView;I)V

    goto :goto_0

    :cond_2
    const/4 p2, 0x4

    .line 8
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fputI(Lcom/pspdfkit/internal/views/document/DocumentView;I)V

    .line 9
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetI(Lcom/pspdfkit/internal/views/document/DocumentView;)I

    move-result p2

    if-ne p2, v3, :cond_4

    const/4 p1, 0x0

    goto :goto_1

    :cond_4
    float-to-int p2, p3

    .line 11
    iget p1, p1, Lcom/pspdfkit/internal/views/document/DocumentView;->p:I

    neg-int p3, p1

    .line 12
    invoke-static {p2, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {p3, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 13
    :goto_1
    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetI(Lcom/pspdfkit/internal/views/document/DocumentView;)I

    move-result p3

    if-ne p3, v2, :cond_5

    goto :goto_2

    :cond_5
    float-to-int p3, p4

    .line 15
    iget p2, p2, Lcom/pspdfkit/internal/views/document/DocumentView;->p:I

    neg-int p4, p2

    .line 16
    invoke-static {p3, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    invoke-static {p4, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 17
    :goto_2
    iget-object p2, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget-object p2, p2, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    neg-int p1, p1

    neg-int p3, v0

    invoke-virtual {p2, p1, p3}, Lcom/pspdfkit/internal/ug;->d(II)Z

    move-result p1

    return p1

    :cond_6
    :goto_3
    return v0
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetM(Lcom/pspdfkit/internal/views/document/DocumentView;)Z

    move-result p2

    const/4 v0, 0x0

    if-nez p2, :cond_6

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetJ(Lcom/pspdfkit/internal/views/document/DocumentView;)Z

    move-result p2

    if-nez p2, :cond_0

    goto :goto_3

    .line 2
    :cond_0
    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetI(Lcom/pspdfkit/internal/views/document/DocumentView;)I

    move-result p2

    const/4 v1, 0x2

    const/4 v2, 0x3

    const/4 v3, 0x1

    if-ne p2, v3, :cond_3

    .line 3
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result p2

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x40400000    # 3.0f

    mul-float v4, v4, v5

    cmpl-float p2, p2, v4

    if-lez p2, :cond_1

    .line 4
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fputI(Lcom/pspdfkit/internal/views/document/DocumentView;I)V

    goto :goto_0

    .line 5
    :cond_1
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result p2

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    mul-float v4, v4, v5

    cmpl-float p2, p2, v4

    if-lez p2, :cond_2

    .line 6
    invoke-static {p1, v2}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fputI(Lcom/pspdfkit/internal/views/document/DocumentView;I)V

    goto :goto_0

    :cond_2
    const/4 p2, 0x4

    .line 8
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fputI(Lcom/pspdfkit/internal/views/document/DocumentView;I)V

    .line 9
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetI(Lcom/pspdfkit/internal/views/document/DocumentView;)I

    move-result p2

    if-ne p2, v2, :cond_4

    const/4 p3, 0x0

    goto :goto_1

    :cond_4
    float-to-int p3, p3

    :goto_1
    if-ne p2, v1, :cond_5

    const/4 p2, 0x0

    goto :goto_2

    :cond_5
    float-to-int p2, p4

    .line 12
    :goto_2
    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetL(Lcom/pspdfkit/internal/views/document/DocumentView;)Z

    move-result p4

    if-eqz p4, :cond_6

    iget-object p1, p1, Lcom/pspdfkit/internal/views/document/DocumentView;->B:Lcom/pspdfkit/internal/ug;

    invoke-virtual {p1, p3, p2}, Lcom/pspdfkit/internal/ug;->e(II)V

    const/4 v0, 0x1

    :cond_6
    :goto_3
    return v0
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetN(Lcom/pspdfkit/internal/views/document/DocumentView;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_5

    invoke-static {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetM(Lcom/pspdfkit/internal/views/document/DocumentView;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 6
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$mc(Lcom/pspdfkit/internal/views/document/DocumentView;Landroid/view/MotionEvent;)Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    return v0

    .line 7
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget-object p1, p1, Lcom/pspdfkit/internal/views/document/DocumentView;->t:Lcom/pspdfkit/listeners/DocumentListener;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/pspdfkit/listeners/DocumentListener;->onDocumentClick()Z

    move-result p1

    if-eqz p1, :cond_1

    return v0

    .line 12
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetC(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/views/document/DocumentView$d;

    move-result-object v1

    sget-object v3, Lcom/pspdfkit/internal/views/document/DocumentView$d;->c:Lcom/pspdfkit/internal/views/document/DocumentView$d;

    if-ne v1, v3, :cond_2

    .line 13
    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetE(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/specialMode/handler/e;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->setTextSelection(Lcom/pspdfkit/datastructures/TextSelection;)V

    const/4 v2, 0x1

    .line 17
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a()Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v2, 0x1

    :cond_3
    if-nez v2, :cond_4

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b()V

    .line 27
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/views/document/DocumentView$b;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->-$$Nest$fgetb(Lcom/pspdfkit/internal/views/document/DocumentView;)Lcom/pspdfkit/internal/h9;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/h9;->a()V

    return v0

    :cond_5
    return v2
.end method

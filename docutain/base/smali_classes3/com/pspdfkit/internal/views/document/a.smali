.class public final Lcom/pspdfkit/internal/views/document/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/u0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/document/a$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/views/document/DocumentView;

.field private final b:Lcom/pspdfkit/ui/PdfFragment;

.field private final c:Lcom/pspdfkit/internal/fl;


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/views/document/a;)Lcom/pspdfkit/internal/views/document/DocumentView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/document/a;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/views/document/a;)Lcom/pspdfkit/internal/fl;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/views/document/a;->c:Lcom/pspdfkit/internal/fl;

    return-object p0
.end method

.method constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/views/document/a;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/views/document/a;->b:Lcom/pspdfkit/ui/PdfFragment;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/views/document/a;->c:Lcom/pspdfkit/internal/fl;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/a;->b:Lcom/pspdfkit/ui/PdfFragment;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/a;->c:Lcom/pspdfkit/internal/fl;

    invoke-static {v0, v1}, Lcom/pspdfkit/ui/editor/AnnotationEditor;->restoreFromState(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/ui/editor/AnnotationEditor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/views/document/a$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/pspdfkit/internal/views/document/a$a;-><init>(Lcom/pspdfkit/internal/views/document/a;Lcom/pspdfkit/internal/views/document/a$a-IA;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/editor/AnnotationEditor;->setOnDismissedListener(Lcom/pspdfkit/ui/editor/AnnotationEditor$OnDismissedListener;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 2

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/views/document/a;->b:Lcom/pspdfkit/ui/PdfFragment;

    iget-object v1, p0, Lcom/pspdfkit/internal/views/document/a;->c:Lcom/pspdfkit/internal/fl;

    invoke-static {p1, v0, v1}, Lcom/pspdfkit/ui/editor/AnnotationEditor;->forAnnotation(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/ui/editor/AnnotationEditor;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 7
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/views/document/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/views/document/a$a;-><init>(Lcom/pspdfkit/internal/views/document/a;Lcom/pspdfkit/internal/views/document/a$a-IA;)V

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/editor/AnnotationEditor;->setOnDismissedListener(Lcom/pspdfkit/ui/editor/AnnotationEditor$OnDismissedListener;)V

    .line 8
    invoke-virtual {p1, p2}, Lcom/pspdfkit/ui/editor/AnnotationEditor;->show(Z)V

    return-void
.end method

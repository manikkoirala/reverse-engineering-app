.class public final Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$b;
.super Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/views/inspector/bottomsheet/c<",
            "TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/inspector/bottomsheet/c<",
            "TV;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$c;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    const-string v0, "bottomSheet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 1

    const-string v0, "bottomSheet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x5

    if-ne p2, p1, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->b()V

    :cond_0
    return-void
.end method

.class final Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;
.super Landroidx/customview/widget/ViewDragHelper$Callback;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    invoke-direct {p0}, Landroidx/customview/widget/ViewDragHelper$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public final clampViewPositionHorizontal(Landroid/view/View;II)I
    .locals 0

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result p1

    return p1
.end method

.method public final clampViewPositionVertical(Landroid/view/View;II)I
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    invoke-static {p1}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->-$$Nest$mb(Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;)I

    move-result p1

    iget-object p3, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget-boolean v0, p3, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->r:Z

    if-eqz v0, :cond_0

    iget p3, p3, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->z:I

    goto :goto_0

    :cond_0
    iget p3, p3, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->p:I

    :goto_0
    invoke-static {p2, p1, p3}, Landroidx/core/math/MathUtils;->clamp(III)I

    move-result p1

    return p1
.end method

.method public final getViewVerticalDragRange(Landroid/view/View;)I
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget-boolean v0, p1, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->r:Z

    if-eqz v0, :cond_0

    .line 2
    iget p1, p1, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->z:I

    return p1

    .line 4
    :cond_0
    iget p1, p1, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->p:I

    return p1
.end method

.method public final onViewDragStateChanged(I)V
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->e(I)V

    :cond_0
    return-void
.end method

.method public final onViewPositionChanged(Landroid/view/View;IIII)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->a(I)V

    return-void
.end method

.method public final onViewReleased(Landroid/view/View;FF)V
    .locals 10

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x4

    const/4 v3, 0x6

    const/4 v4, 0x3

    cmpg-float v5, p3, v0

    if-gez v5, :cond_2

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    invoke-static {p2}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->-$$Nest$fgetb(Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 2
    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->m:I

    :goto_0
    const/4 v2, 0x3

    goto/16 :goto_7

    .line 5
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p2

    .line 6
    iget-object p3, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget v0, p3, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->n:I

    if-le p2, v0, :cond_1

    move p2, v0

    :goto_1
    const/4 v2, 0x6

    goto/16 :goto_7

    .line 10
    :cond_1
    iget p2, p3, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->l:I

    goto :goto_0

    .line 14
    :cond_2
    iget-object v5, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget-boolean v6, v5, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->r:Z

    if-eqz v6, :cond_d

    .line 15
    invoke-static {v5}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->-$$Nest$fgets(Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;)Z

    move-result v6

    const/4 v7, 0x0

    if-eqz v6, :cond_3

    goto :goto_3

    .line 18
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v6

    iget v8, v5, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->p:I

    if-ge v6, v8, :cond_4

    goto :goto_4

    .line 19
    :cond_4
    invoke-static {v5}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->-$$Nest$fgete(Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 20
    invoke-static {v5}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->-$$Nest$fgetf(Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;)I

    move-result v6

    iget v8, v5, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->z:I

    iget v9, v5, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->y:I

    mul-int/lit8 v9, v9, 0x9

    div-int/lit8 v9, v9, 0x10

    sub-int/2addr v8, v9

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    goto :goto_2

    .line 22
    :cond_5
    invoke-static {v5}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->-$$Nest$fgetd(Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;)I

    move-result v6

    .line 23
    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v8

    int-to-float v8, v8

    const v9, 0x3dcccccd    # 0.1f

    mul-float v9, v9, p3

    add-float/2addr v9, v8

    .line 24
    iget v5, v5, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->p:I

    int-to-float v5, v5

    sub-float/2addr v9, v5

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v5

    int-to-float v6, v6

    div-float/2addr v5, v6

    const/high16 v6, 0x3f000000    # 0.5f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_6

    :goto_3
    const/4 v5, 0x1

    goto :goto_5

    :cond_6
    :goto_4
    const/4 v5, 0x0

    :goto_5
    if-eqz v5, :cond_d

    .line 25
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p2

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float p2, p2, v0

    if-gez p2, :cond_7

    const/high16 p2, 0x43fa0000    # 500.0f

    cmpl-float p2, p3, p2

    if-gtz p2, :cond_9

    .line 26
    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p2

    iget-object p3, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget v0, p3, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->z:I

    invoke-static {p3}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->-$$Nest$mb(Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;)I

    move-result p3

    add-int/2addr v0, p3

    div-int/lit8 v0, v0, 0x2

    if-le p2, v0, :cond_8

    const/4 v7, 0x1

    :cond_8
    if-eqz v7, :cond_a

    .line 27
    :cond_9
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->z:I

    const/4 v2, 0x5

    goto/16 :goto_7

    .line 29
    :cond_a
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    invoke-static {p2}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->-$$Nest$fgetb(Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;)Z

    move-result p3

    if-eqz p3, :cond_b

    .line 30
    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->m:I

    goto/16 :goto_0

    .line 32
    :cond_b
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p2

    iget-object p3, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p3, p3, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->l:I

    sub-int/2addr p2, p3

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p2

    .line 33
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p3

    iget-object v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget v0, v0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->n:I

    sub-int/2addr p3, v0

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result p3

    if-ge p2, p3, :cond_c

    .line 34
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->l:I

    goto/16 :goto_0

    .line 37
    :cond_c
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->n:I

    goto/16 :goto_1

    :cond_d
    cmpl-float v0, p3, v0

    if-eqz v0, :cond_11

    .line 40
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p2

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result p3

    cmpl-float p2, p2, p3

    if-lez p2, :cond_e

    goto :goto_6

    .line 73
    :cond_e
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    invoke-static {p2}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->-$$Nest$fgetb(Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;)Z

    move-result p3

    if-eqz p3, :cond_f

    .line 74
    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->p:I

    goto/16 :goto_7

    .line 78
    :cond_f
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p2

    .line 79
    iget-object p3, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p3, p3, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->n:I

    sub-int p3, p2, p3

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result p3

    iget-object v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget v0, v0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->p:I

    sub-int/2addr p2, v0

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p2

    if-ge p3, p2, :cond_10

    .line 80
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->n:I

    goto/16 :goto_1

    .line 83
    :cond_10
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->p:I

    goto :goto_7

    .line 84
    :cond_11
    :goto_6
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p2

    .line 85
    iget-object p3, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    invoke-static {p3}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->-$$Nest$fgetb(Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 86
    iget p3, p3, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->m:I

    sub-int p3, p2, p3

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result p3

    iget-object v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget v0, v0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->p:I

    sub-int/2addr p2, v0

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p2

    if-ge p3, p2, :cond_12

    .line 87
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->m:I

    goto/16 :goto_0

    .line 90
    :cond_12
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->p:I

    goto :goto_7

    .line 94
    :cond_13
    iget v0, p3, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->n:I

    if-ge p2, v0, :cond_15

    .line 95
    iget p3, p3, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->p:I

    sub-int p3, p2, p3

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result p3

    if-ge p2, p3, :cond_14

    .line 96
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->l:I

    goto/16 :goto_0

    .line 99
    :cond_14
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->n:I

    goto/16 :goto_1

    :cond_15
    sub-int p3, p2, v0

    .line 103
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result p3

    iget-object v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget v0, v0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->p:I

    sub-int/2addr p2, v0

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p2

    if-ge p3, p2, :cond_16

    .line 104
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->n:I

    goto/16 :goto_1

    .line 107
    :cond_16
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->p:I

    .line 128
    :goto_7
    iget-object p3, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    invoke-virtual {p3, p1, v2, p2, v1}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->a(Landroid/view/View;IIZ)V

    return-void
.end method

.method public final tryCaptureView(Landroid/view/View;I)Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget v1, v0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->t:I

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    return v3

    .line 4
    :cond_0
    iget-boolean v4, v0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->G:Z

    if-eqz v4, :cond_1

    return v3

    :cond_1
    const/4 v4, 0x3

    if-ne v1, v4, :cond_3

    .line 7
    iget v1, v0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->E:I

    if-ne v1, p2, :cond_3

    .line 8
    iget-object p2, v0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->B:Ljava/lang/ref/WeakReference;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_3

    const/4 v0, -0x1

    .line 9
    invoke-virtual {p2, v0}, Landroid/view/View;->canScrollVertically(I)Z

    move-result p2

    if-eqz p2, :cond_3

    return v3

    .line 14
    :cond_3
    iget-object p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior$b;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget-object p2, p2, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->A:Ljava/lang/ref/WeakReference;

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p2

    if-ne p2, p1, :cond_4

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    :goto_1
    return v2
.end method

.class final Lcom/pspdfkit/internal/views/inspector/bottomsheet/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroidx/core/view/accessibility/AccessibilityViewCommand;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/b;->b:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iput p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/b;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final perform(Landroid/view/View;Landroidx/core/view/accessibility/AccessibilityViewCommand$CommandArguments;)Z
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/b;->b:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    iget p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/b;->a:I

    .line 2
    iget v0, p1, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->t:I

    if-ne p2, v0, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p1, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->A:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_2

    const/4 v0, 0x4

    if-eq p2, v0, :cond_1

    const/4 v0, 0x3

    if-eq p2, v0, :cond_1

    const/4 v0, 0x6

    if-eq p2, v0, :cond_1

    .line 7
    iget-boolean v0, p1, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->r:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    if-ne p2, v0, :cond_5

    .line 11
    :cond_1
    iput p2, p1, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->t:I

    goto :goto_0

    .line 12
    :cond_2
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_3

    goto :goto_0

    .line 17
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 18
    invoke-interface {v1}, Landroid/view/ViewParent;->isLayoutRequested()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->isAttachedToWindow(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 20
    new-instance v1, Lcom/pspdfkit/internal/views/inspector/bottomsheet/a;

    invoke-direct {v1, p1, v0, p2}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/a;-><init>(Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 27
    :cond_4
    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->a(Landroid/view/View;I)V

    :cond_5
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

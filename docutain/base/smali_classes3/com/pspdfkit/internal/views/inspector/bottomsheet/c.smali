.class public final Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "Lcom/pspdfkit/internal/b5;",
        ">",
        "Landroid/widget/FrameLayout;"
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/a5;

.field private c:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$a;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior<",
            "Lcom/pspdfkit/internal/views/inspector/bottomsheet/c<",
            "TV;>;>;"
        }
    .end annotation
.end field

.field private j:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$7TqCQIpIyyjrsnkQvt50ygK0prw(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;)Z
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->a(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;)Z

    move-result p0

    return p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Lcom/pspdfkit/internal/a5;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/a5;-><init>(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->b:Lcom/pspdfkit/internal/a5;

    const p1, 0x7fffffff

    .line 8
    iput p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->d:I

    .line 50
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->a()V

    return-void
.end method

.method private final a()V
    .locals 9

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/go;->a(Landroid/content/Context;)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 3
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__PropertyInspector_pspdf__minHeight:I

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x64

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v2

    .line 5
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->e:I

    .line 10
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__PropertyInspector_pspdf__maxHeight:I

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x190

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v2

    .line 12
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->f:I

    .line 17
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__PropertyInspector_pspdf__maxWidth:I

    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x1e0

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v2

    .line 19
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 23
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__PropertyInspector_pspdf__backgroundColor:I

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 24
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 27
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v4, 0x10

    invoke-static {v0, v4}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v0

    int-to-float v0, v0

    invoke-static {p0, v0}, Landroidx/core/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    .line 29
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    const/4 v4, 0x1

    if-ge v0, v1, :cond_0

    .line 31
    invoke-virtual {p0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 34
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/pspdfkit/R$dimen;->pspdf__inspector_corner_radius:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    const/4 v6, 0x2

    add-int/2addr v5, v6

    const/16 v7, 0x8

    new-array v7, v7, [F

    int-to-float v5, v5

    const/4 v8, 0x0

    aput v5, v7, v8

    aput v5, v7, v4

    aput v5, v7, v6

    const/4 v6, 0x3

    aput v5, v7, v6

    const/4 v5, 0x4

    const/4 v6, 0x0

    aput v6, v7, v5

    const/4 v5, 0x5

    aput v6, v7, v5

    const/4 v5, 0x6

    aput v6, v7, v5

    const/4 v5, 0x7

    aput v6, v7, v5

    .line 39
    invoke-static {p0, v2, v7}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;I[F)V

    .line 47
    :goto_0
    new-instance v2, Landroidx/coordinatorlayout/widget/CoordinatorLayout$LayoutParams;

    if-ge v0, v1, :cond_1

    const/4 v1, -0x1

    :cond_1
    const/4 v0, -0x2

    invoke-direct {v2, v1, v0}, Landroidx/coordinatorlayout/widget/CoordinatorLayout$LayoutParams;-><init>(II)V

    .line 51
    iput v4, v2, Landroidx/coordinatorlayout/widget/CoordinatorLayout$LayoutParams;->gravity:I

    .line 53
    new-instance v0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->i:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    .line 54
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->getBehavior()Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->b(Z)V

    .line 55
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->getBehavior()Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->a(Z)V

    .line 56
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->getBehavior()Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->c(Z)V

    .line 57
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->getBehavior()Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$b;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$b;-><init>(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->a(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$b;)V

    .line 67
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->getBehavior()Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroidx/coordinatorlayout/widget/CoordinatorLayout$LayoutParams;->setBehavior(Landroidx/coordinatorlayout/widget/CoordinatorLayout$Behavior;)V

    .line 69
    invoke-virtual {p0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    sget v0, Lcom/pspdfkit/R$id;->pspdf__bottom_sheet_layout:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;)Z
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iget-object p0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->b:Lcom/pspdfkit/internal/a5;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/a5;->b()V

    const/4 p0, 0x1

    return p0
.end method

.method public static synthetic getBehavior$annotations()V
    .locals 0

    return-void
.end method

.method private final getMaxHeight()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->j:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v0, "contentView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/pspdfkit/internal/b5;

    invoke-interface {v0}, Lcom/pspdfkit/internal/b5;->getMaximumHeight()I

    move-result v0

    .line 2
    iget v1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->f:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 3
    iget v1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->d:I

    invoke-static {v0, v1}, Lkotlin/ranges/RangesKt;->coerceAtMost(II)I

    move-result v0

    return v0
.end method

.method private final getMinHeight()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->e:I

    iget v1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->g:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->j:Landroid/view/View;

    if-nez v1, :cond_0

    const-string v1, "contentView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getMinimumHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->getMaxHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .line 74
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->getBehavior()Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    move-result-object v0

    const/4 v1, 0x5

    iput v1, v0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->t:I

    if-eqz p1, :cond_0

    .line 76
    iget-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->b:Lcom/pspdfkit/internal/a5;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/a5;->a()V

    goto :goto_0

    .line 78
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->b()V

    :goto_0
    return-void
.end method

.method public final b()V
    .locals 1

    const/16 v0, 0x8

    .line 13
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->c:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$a;->onHide(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;)V

    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 2

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->getBehavior()Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    move-result-object v0

    const/4 v1, 0x3

    iput v1, v0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->t:I

    if-eqz p1, :cond_0

    .line 4
    new-instance p1, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;)V

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_0

    .line 10
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->b:Lcom/pspdfkit/internal/a5;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/a5;->c()V

    const/4 p1, 0x0

    .line 11
    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->c()V

    :goto_0
    return-void
.end method

.method public final c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->c:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$a;->onShow(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;)V

    :cond_0
    return-void
.end method

.method public final getBehavior()Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior<",
            "Lcom/pspdfkit/internal/views/inspector/bottomsheet/c<",
            "TV;>;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->i:Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "behavior"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final onMeasure(II)V
    .locals 5

    .line 1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type android.view.View"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/view/View;

    .line 3
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v2

    if-ge v2, v0, :cond_0

    .line 5
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p1

    invoke-static {v0, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 8
    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    iput p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->d:I

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->j:Landroid/view/View;

    const/4 v1, 0x0

    const-string v2, "contentView"

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    const/high16 v3, -0x80000000

    invoke-static {p2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, p1, v3}, Landroid/view/View;->measure(II)V

    .line 14
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 17
    invoke-direct {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->getMaxHeight()I

    move-result v3

    invoke-direct {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->getMinHeight()I

    move-result v4

    .line 18
    invoke-static {v3, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    invoke-static {v4, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 19
    iget-object v3, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->j:Landroid/view/View;

    if-nez v3, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v3

    :goto_0
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {p2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, p1, v2}, Landroid/view/View;->measure(II)V

    if-eqz v0, :cond_4

    if-eq v0, p2, :cond_4

    .line 22
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->getBehavior()Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;

    move-result-object v1

    iget v1, v1, Lcom/pspdfkit/internal/views/inspector/bottomsheet/BottomSheetBehavior;->t:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    if-ge p2, v0, :cond_3

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, p2

    .line 29
    :goto_1
    iget v2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->h:I

    if-eq v2, p2, :cond_5

    .line 30
    iget-object v2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->b:Lcom/pspdfkit/internal/a5;

    invoke-virtual {v2, v0, p2}, Lcom/pspdfkit/internal/a5;->a(II)V

    goto :goto_2

    :cond_4
    move v1, p2

    .line 33
    :cond_5
    :goto_2
    iput p2, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->h:I

    .line 36
    invoke-virtual {p0}, Landroid/view/View;->getSuggestedMinimumWidth()I

    move-result p2

    invoke-static {p2, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result p1

    .line 37
    invoke-virtual {p0}, Landroid/view/View;->getSuggestedMinimumHeight()I

    move-result p2

    iget v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->d:I

    .line 38
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 39
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public final setBottomInset(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->g:I

    if-ne v0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->g:I

    const/4 p1, 0x0

    .line 5
    iput p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->h:I

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public final setCallback(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->c:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c$a;

    return-void
.end method

.method public final setContentView(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    const-string v0, "contentView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->j:Landroid/view/View;

    .line 3
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    const/4 v0, 0x0

    .line 4
    iput v0, p0, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->h:I

    .line 5
    invoke-virtual {p0, v0, v0}, Landroid/view/View;->setMeasuredDimension(II)V

    .line 6
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public final setMeasuredHeight$pspdfkit_release(I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setMeasuredDimension(II)V

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

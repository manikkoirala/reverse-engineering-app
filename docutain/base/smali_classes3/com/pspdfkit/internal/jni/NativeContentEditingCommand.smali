.class public final enum Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum APPLY_FORMAT:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum AVAILABLE_FACES:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum CREATE_TEXT_BLOCK:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum DELETE_CLUSTER:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum DELETE_RANGE:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum DETECT_PARAGRAPHS:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum GET_TEXT_BLOCKS:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum INSERT_CONTENT_REF:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum INSERT_TEXT:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum LAYOUT:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum MOVE_CURSOR:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum REDO:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum RENDER_TEXT_BLOCK:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum RESTORE:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum SAVE_TO_DOCUMENT:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum SET_CURSOR:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum SET_SELECTION:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum SET_SELECTION_RANGE:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field public static final enum UNDO:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;


# direct methods
.method static constructor <clinit>()V
    .locals 22

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v1, "GET_TEXT_BLOCKS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->GET_TEXT_BLOCKS:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v3, "RENDER_TEXT_BLOCK"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->RENDER_TEXT_BLOCK:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 3
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v5, "SET_CURSOR"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->SET_CURSOR:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 4
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v7, "MOVE_CURSOR"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->MOVE_CURSOR:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 5
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v9, "SET_SELECTION"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->SET_SELECTION:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 6
    new-instance v9, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v11, "SET_SELECTION_RANGE"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->SET_SELECTION_RANGE:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 7
    new-instance v11, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v13, "INSERT_TEXT"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->INSERT_TEXT:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 8
    new-instance v13, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v15, "DELETE_RANGE"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->DELETE_RANGE:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 9
    new-instance v15, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v14, "DELETE_CLUSTER"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->DELETE_CLUSTER:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 10
    new-instance v14, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v12, "UNDO"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->UNDO:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 11
    new-instance v12, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v10, "REDO"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->REDO:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 12
    new-instance v10, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v8, "RESTORE"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->RESTORE:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 13
    new-instance v8, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v6, "APPLY_FORMAT"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->APPLY_FORMAT:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 14
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v4, "LAYOUT"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->LAYOUT:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 15
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v2, "SAVE_TO_DOCUMENT"

    move-object/from16 v17, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->SAVE_TO_DOCUMENT:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 16
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v6, "AVAILABLE_FACES"

    move-object/from16 v18, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->AVAILABLE_FACES:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 17
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v4, "DETECT_PARAGRAPHS"

    move-object/from16 v19, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->DETECT_PARAGRAPHS:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 18
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v2, "CREATE_TEXT_BLOCK"

    move-object/from16 v20, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->CREATE_TEXT_BLOCK:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 19
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const-string v6, "INSERT_CONTENT_REF"

    move-object/from16 v21, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->INSERT_CONTENT_REF:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const/16 v6, 0x13

    new-array v6, v6, [Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    const/16 v16, 0x0

    aput-object v0, v6, v16

    const/4 v0, 0x1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    aput-object v3, v6, v0

    const/4 v0, 0x3

    aput-object v5, v6, v0

    const/4 v0, 0x4

    aput-object v7, v6, v0

    const/4 v0, 0x5

    aput-object v9, v6, v0

    const/4 v0, 0x6

    aput-object v11, v6, v0

    const/4 v0, 0x7

    aput-object v13, v6, v0

    const/16 v0, 0x8

    aput-object v15, v6, v0

    const/16 v0, 0x9

    aput-object v14, v6, v0

    const/16 v0, 0xa

    aput-object v12, v6, v0

    const/16 v0, 0xb

    aput-object v10, v6, v0

    const/16 v0, 0xc

    aput-object v8, v6, v0

    const/16 v0, 0xd

    aput-object v17, v6, v0

    const/16 v0, 0xe

    aput-object v18, v6, v0

    const/16 v0, 0xf

    aput-object v19, v6, v0

    const/16 v0, 0x10

    aput-object v20, v6, v0

    const/16 v0, 0x11

    aput-object v21, v6, v0

    aput-object v2, v6, v4

    .line 20
    sput-object v6, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    return-object v0
.end method

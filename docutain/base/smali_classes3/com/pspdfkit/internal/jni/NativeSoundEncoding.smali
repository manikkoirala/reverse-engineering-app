.class public final enum Lcom/pspdfkit/internal/jni/NativeSoundEncoding;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeSoundEncoding;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

.field public static final enum ALAW:Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

.field public static final enum MULAW:Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

.field public static final enum RAW:Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

.field public static final enum SIGNED:Lcom/pspdfkit/internal/jni/NativeSoundEncoding;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    const-string v1, "RAW"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;->RAW:Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    const-string v3, "SIGNED"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;->SIGNED:Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    .line 3
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    const-string v5, "MULAW"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;->MULAW:Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    .line 4
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    const-string v7, "ALAW"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;->ALAW:Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    const/4 v7, 0x4

    new-array v7, v7, [Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    .line 5
    sput-object v7, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeSoundEncoding;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeSoundEncoding;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeSoundEncoding;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeSoundEncoding;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeSoundEncoding;

    return-object v0
.end method

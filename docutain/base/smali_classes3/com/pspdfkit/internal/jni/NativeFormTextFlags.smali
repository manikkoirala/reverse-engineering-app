.class public final enum Lcom/pspdfkit/internal/jni/NativeFormTextFlags;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeFormTextFlags;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

.field public static final enum COMB:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

.field public static final enum DO_NOT_SCROLL:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

.field public static final enum DO_NOT_SPELL_CHECK:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

.field public static final enum FILE_SELECT:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

.field public static final enum MULTI_LINE:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

.field public static final enum PASSWORD:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

.field public static final enum RICH_TEXT:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    const-string v1, "MULTI_LINE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->MULTI_LINE:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    const-string v3, "PASSWORD"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->PASSWORD:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    .line 5
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    const-string v5, "FILE_SELECT"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->FILE_SELECT:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    .line 7
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    const-string v7, "DO_NOT_SPELL_CHECK"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->DO_NOT_SPELL_CHECK:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    .line 9
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    const-string v9, "DO_NOT_SCROLL"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->DO_NOT_SCROLL:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    .line 14
    new-instance v9, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    const-string v11, "COMB"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->COMB:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    .line 16
    new-instance v11, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    const-string v13, "RICH_TEXT"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->RICH_TEXT:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    const/4 v13, 0x7

    new-array v13, v13, [Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    aput-object v0, v13, v2

    aput-object v1, v13, v4

    aput-object v3, v13, v6

    aput-object v5, v13, v8

    aput-object v7, v13, v10

    aput-object v9, v13, v12

    aput-object v11, v13, v14

    .line 17
    sput-object v13, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeFormTextFlags;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeFormTextFlags;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    return-object v0
.end method

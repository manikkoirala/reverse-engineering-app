.class public final enum Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

.field public static final enum CURSOR_ENTERS:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

.field public static final enum CURSOR_EXITS:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

.field public static final enum FIELD_FORMAT:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

.field public static final enum FORM_CALCULATE:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

.field public static final enum FORM_CHANGED:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

.field public static final enum FORM_VALIDATE:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

.field public static final enum LOSE_FOCUS:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

.field public static final enum MOUSE_DOWN:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

.field public static final enum MOUSE_UP:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

.field public static final enum PAGE_CLOSED:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

.field public static final enum PAGE_OPENED:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

.field public static final enum PAGE_VISIBLE:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

.field public static final enum RECEIVE_FOCUS:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const-string v1, "CURSOR_ENTERS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->CURSOR_ENTERS:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const-string v3, "CURSOR_EXITS"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->CURSOR_EXITS:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    .line 5
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const-string v5, "MOUSE_DOWN"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->MOUSE_DOWN:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    .line 7
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const-string v7, "MOUSE_UP"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->MOUSE_UP:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    .line 9
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const-string v9, "RECEIVE_FOCUS"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->RECEIVE_FOCUS:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    .line 11
    new-instance v9, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const-string v11, "LOSE_FOCUS"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->LOSE_FOCUS:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    .line 13
    new-instance v11, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const-string v13, "PAGE_OPENED"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->PAGE_OPENED:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    .line 15
    new-instance v13, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const-string v15, "PAGE_CLOSED"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->PAGE_CLOSED:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    .line 17
    new-instance v15, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const-string v14, "PAGE_VISIBLE"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->PAGE_VISIBLE:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    .line 19
    new-instance v14, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const-string v12, "FORM_CHANGED"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->FORM_CHANGED:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    .line 21
    new-instance v12, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const-string v10, "FIELD_FORMAT"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->FIELD_FORMAT:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    .line 23
    new-instance v10, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const-string v8, "FORM_VALIDATE"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->FORM_VALIDATE:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    .line 25
    new-instance v8, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const-string v6, "FORM_CALCULATE"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->FORM_CALCULATE:Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    const/16 v6, 0xd

    new-array v6, v6, [Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    aput-object v0, v6, v2

    const/4 v0, 0x1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    aput-object v3, v6, v0

    const/4 v0, 0x3

    aput-object v5, v6, v0

    const/4 v0, 0x4

    aput-object v7, v6, v0

    const/4 v0, 0x5

    aput-object v9, v6, v0

    const/4 v0, 0x6

    aput-object v11, v6, v0

    const/4 v0, 0x7

    aput-object v13, v6, v0

    const/16 v0, 0x8

    aput-object v15, v6, v0

    const/16 v0, 0x9

    aput-object v14, v6, v0

    const/16 v0, 0xa

    aput-object v12, v6, v0

    const/16 v0, 0xb

    aput-object v10, v6, v0

    aput-object v8, v6, v4

    .line 26
    sput-object v6, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeAnnotationTriggerEvent;

    return-object v0
.end method

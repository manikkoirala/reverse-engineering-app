.class public final enum Lcom/pspdfkit/internal/jni/NativeStampType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeStampType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum ACCEPTED:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum APPROVED:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum ASIS:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum COMPLETED:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum CONFIDENTIAL:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum DEPARTMENTAL:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum DRAFT:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum EXPERIMENTAL:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum EXPIRED:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum FINAL:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum FORCOMMENT:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum FORPUBLICRELEASE:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum INFORMATIONONLY:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum INITIALHERE:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum NOTAPPROVED:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum NOTFORPUBLICRELEASE:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum PRELIMINARYRESULTS:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum REJECTED:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum REVISED:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum SIGNHERE:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum SOLD:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum TOPSECRET:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum VOID:Lcom/pspdfkit/internal/jni/NativeStampType;

.field public static final enum WITNESS:Lcom/pspdfkit/internal/jni/NativeStampType;


# direct methods
.method static constructor <clinit>()V
    .locals 26

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v1, "ACCEPTED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeStampType;->ACCEPTED:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v3, "APPROVED"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeStampType;->APPROVED:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 5
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v5, "ASIS"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeStampType;->ASIS:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 7
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v7, "COMPLETED"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeStampType;->COMPLETED:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 9
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v9, "CONFIDENTIAL"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativeStampType;->CONFIDENTIAL:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 11
    new-instance v9, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v11, "DEPARTMENTAL"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativeStampType;->DEPARTMENTAL:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 13
    new-instance v11, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v13, "DRAFT"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativeStampType;->DRAFT:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 15
    new-instance v13, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v15, "EXPERIMENTAL"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativeStampType;->EXPERIMENTAL:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 17
    new-instance v15, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v14, "EXPIRED"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativeStampType;->EXPIRED:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 19
    new-instance v14, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v12, "FINAL"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativeStampType;->FINAL:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 21
    new-instance v12, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v10, "FORCOMMENT"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/internal/jni/NativeStampType;->FORCOMMENT:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 23
    new-instance v10, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v8, "FORPUBLICRELEASE"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/internal/jni/NativeStampType;->FORPUBLICRELEASE:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 25
    new-instance v8, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v6, "INFORMATIONONLY"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/internal/jni/NativeStampType;->INFORMATIONONLY:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 27
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v4, "INITIALHERE"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeStampType;->INITIALHERE:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 29
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v2, "NOTAPPROVED"

    move-object/from16 v16, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeStampType;->NOTAPPROVED:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 31
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v6, "NOTFORPUBLICRELEASE"

    move-object/from16 v17, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeStampType;->NOTFORPUBLICRELEASE:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 33
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v4, "PRELIMINARYRESULTS"

    move-object/from16 v18, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeStampType;->PRELIMINARYRESULTS:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 35
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v2, "REJECTED"

    move-object/from16 v19, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeStampType;->REJECTED:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 37
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v6, "REVISED"

    move-object/from16 v20, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeStampType;->REVISED:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 39
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v4, "SIGNHERE"

    move-object/from16 v21, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeStampType;->SIGNHERE:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 41
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v2, "SOLD"

    move-object/from16 v22, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeStampType;->SOLD:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 43
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v6, "TOPSECRET"

    move-object/from16 v23, v4

    const/16 v4, 0x15

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeStampType;->TOPSECRET:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 45
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v4, "VOID"

    move-object/from16 v24, v2

    const/16 v2, 0x16

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeStampType;->VOID:Lcom/pspdfkit/internal/jni/NativeStampType;

    .line 47
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeStampType;

    const-string v4, "WITNESS"

    move-object/from16 v25, v6

    const/16 v6, 0x17

    invoke-direct {v2, v4, v6}, Lcom/pspdfkit/internal/jni/NativeStampType;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeStampType;->WITNESS:Lcom/pspdfkit/internal/jni/NativeStampType;

    const/16 v4, 0x18

    new-array v4, v4, [Lcom/pspdfkit/internal/jni/NativeStampType;

    const/4 v6, 0x0

    aput-object v0, v4, v6

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v3, v4, v0

    const/4 v0, 0x3

    aput-object v5, v4, v0

    const/4 v0, 0x4

    aput-object v7, v4, v0

    const/4 v0, 0x5

    aput-object v9, v4, v0

    const/4 v0, 0x6

    aput-object v11, v4, v0

    const/4 v0, 0x7

    aput-object v13, v4, v0

    const/16 v0, 0x8

    aput-object v15, v4, v0

    const/16 v0, 0x9

    aput-object v14, v4, v0

    const/16 v0, 0xa

    aput-object v12, v4, v0

    const/16 v0, 0xb

    aput-object v10, v4, v0

    const/16 v0, 0xc

    aput-object v8, v4, v0

    const/16 v0, 0xd

    aput-object v16, v4, v0

    const/16 v0, 0xe

    aput-object v17, v4, v0

    const/16 v0, 0xf

    aput-object v18, v4, v0

    const/16 v0, 0x10

    aput-object v19, v4, v0

    const/16 v0, 0x11

    aput-object v20, v4, v0

    const/16 v0, 0x12

    aput-object v21, v4, v0

    const/16 v0, 0x13

    aput-object v22, v4, v0

    const/16 v0, 0x14

    aput-object v23, v4, v0

    const/16 v0, 0x15

    aput-object v24, v4, v0

    const/16 v0, 0x16

    aput-object v25, v4, v0

    const/16 v0, 0x17

    aput-object v2, v4, v0

    .line 48
    sput-object v4, Lcom/pspdfkit/internal/jni/NativeStampType;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeStampType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeStampType;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeStampType;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeStampType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeStampType;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeStampType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeStampType;

    return-object v0
.end method

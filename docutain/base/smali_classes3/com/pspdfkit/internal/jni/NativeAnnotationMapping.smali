.class public final Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final mFirst:Lcom/pspdfkit/internal/jni/NativeAnnotation;

.field final mSecond:Lcom/pspdfkit/internal/jni/NativeAnnotation;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeAnnotation;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->mFirst:Lcom/pspdfkit/internal/jni/NativeAnnotation;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->mSecond:Lcom/pspdfkit/internal/jni/NativeAnnotation;

    return-void
.end method


# virtual methods
.method public getFirst()Lcom/pspdfkit/internal/jni/NativeAnnotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->mFirst:Lcom/pspdfkit/internal/jni/NativeAnnotation;

    return-object v0
.end method

.method public getSecond()Lcom/pspdfkit/internal/jni/NativeAnnotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->mSecond:Lcom/pspdfkit/internal/jni/NativeAnnotation;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NativeAnnotationMapping{mFirst="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->mFirst:Lcom/pspdfkit/internal/jni/NativeAnnotation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ",mSecond="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->mSecond:Lcom/pspdfkit/internal/jni/NativeAnnotation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

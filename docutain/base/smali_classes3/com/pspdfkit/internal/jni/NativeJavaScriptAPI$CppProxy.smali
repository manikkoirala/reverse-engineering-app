.class final Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;
.super Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CppProxy"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final destroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final nativeRef:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI;

    return-void
.end method

.method private constructor <init>(J)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->destroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-eqz v2, :cond_0

    .line 7
    iput-wide p1, p0, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->nativeRef:J

    return-void

    .line 8
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "nativeRef is zero"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private native nativeDestroy(J)V
.end method

.method private native native_addEvaluationObserver(JLcom/pspdfkit/internal/jni/NativeJSEvaluationObserver;)V
.end method

.method private native native_evaluateScript(JLcom/pspdfkit/internal/jni/NativeJSScriptDescriptor;Lcom/pspdfkit/internal/jni/NativeJSPlatformDelegate;)Lcom/pspdfkit/internal/jni/NativeJSResult;
.end method

.method private native native_getInitPath(J)Ljava/lang/String;
.end method

.method private native native_removeEvaluationObserver(JLcom/pspdfkit/internal/jni/NativeJSEvaluationObserver;)V
.end method


# virtual methods
.method public _djinni_private_destroy()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->destroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-wide v0, p0, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->nativeRef:J

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->nativeDestroy(J)V

    :cond_0
    return-void
.end method

.method public addEvaluationObserver(Lcom/pspdfkit/internal/jni/NativeJSEvaluationObserver;)V
    .locals 2

    .line 2
    iget-wide v0, p0, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->nativeRef:J

    invoke-direct {p0, v0, v1, p1}, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->native_addEvaluationObserver(JLcom/pspdfkit/internal/jni/NativeJSEvaluationObserver;)V

    return-void
.end method

.method public evaluateScript(Lcom/pspdfkit/internal/jni/NativeJSScriptDescriptor;Lcom/pspdfkit/internal/jni/NativeJSPlatformDelegate;)Lcom/pspdfkit/internal/jni/NativeJSResult;
    .locals 2

    .line 2
    iget-wide v0, p0, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->nativeRef:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->native_evaluateScript(JLcom/pspdfkit/internal/jni/NativeJSScriptDescriptor;Lcom/pspdfkit/internal/jni/NativeJSPlatformDelegate;)Lcom/pspdfkit/internal/jni/NativeJSResult;

    move-result-object p1

    return-object p1
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->_djinni_private_destroy()V

    .line 2
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method

.method public getInitPath()Ljava/lang/String;
    .locals 2

    .line 2
    iget-wide v0, p0, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->nativeRef:J

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->native_getInitPath(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public removeEvaluationObserver(Lcom/pspdfkit/internal/jni/NativeJSEvaluationObserver;)V
    .locals 2

    .line 2
    iget-wide v0, p0, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->nativeRef:J

    invoke-direct {p0, v0, v1, p1}, Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI$CppProxy;->native_removeEvaluationObserver(JLcom/pspdfkit/internal/jni/NativeJSEvaluationObserver;)V

    return-void
.end method

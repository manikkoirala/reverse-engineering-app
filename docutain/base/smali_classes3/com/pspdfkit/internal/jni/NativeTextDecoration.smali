.class public final enum Lcom/pspdfkit/internal/jni/NativeTextDecoration;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeTextDecoration;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeTextDecoration;

.field public static final enum STRIKE_THROUGH:Lcom/pspdfkit/internal/jni/NativeTextDecoration;

.field public static final enum UNDERLINE:Lcom/pspdfkit/internal/jni/NativeTextDecoration;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeTextDecoration;

    const-string v1, "UNDERLINE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeTextDecoration;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeTextDecoration;->UNDERLINE:Lcom/pspdfkit/internal/jni/NativeTextDecoration;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeTextDecoration;

    const-string v3, "STRIKE_THROUGH"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeTextDecoration;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeTextDecoration;->STRIKE_THROUGH:Lcom/pspdfkit/internal/jni/NativeTextDecoration;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/pspdfkit/internal/jni/NativeTextDecoration;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    .line 3
    sput-object v3, Lcom/pspdfkit/internal/jni/NativeTextDecoration;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeTextDecoration;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeTextDecoration;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeTextDecoration;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeTextDecoration;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeTextDecoration;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeTextDecoration;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeTextDecoration;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeTextDecoration;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeTextDecoration;

    return-object v0
.end method

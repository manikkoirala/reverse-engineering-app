.class public final Lcom/pspdfkit/internal/jni/PSPDFKitNative;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0010\u0011\n\u0002\u0008\u0007\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u001e\n\u0002\u0008\u0012\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\t\u0008\u0002\u00a2\u0006\u0004\u0008:\u00101J \u0010\u0008\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004H\u0007J\u001a\u0010\u000c\u001a\u00020\u000b2\u0006\u0010\t\u001a\u00020\u00052\u0008\u0010\n\u001a\u0004\u0018\u00010\u0005H\u0007J\u0010\u0010\r\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u0005H\u0002J\u0008\u0010\u000e\u001a\u00020\u0007H\u0007J\u0013\u0010\u0010\u001a\u00020\u00072\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0005H\u0087 J\u0010\u0010\u0011\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0002H\u0003J\u0010\u0010\u0012\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0002H\u0003J*\u0010\u0014\u001a\u00020\u00072\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0013H\u0086 \u00a2\u0006\u0004\u0008\u0014\u0010\u0015J\u0013\u0010\u0017\u001a\u00020\u000b2\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0005H\u0086 J\u001d\u0010\u0019\u001a\u00020\u000b2\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u00052\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0005H\u0086 J\t\u0010\u001a\u001a\u00020\u0007H\u0086 J\t\u0010\u001c\u001a\u00020\u001bH\u0086 J\u001f\u0010\u001f\u001a\u0004\u0018\u00010\u00052\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u00052\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u0005H\u0086 R\u0014\u0010 \u001a\u00020\u00058\u0006X\u0086T\u00a2\u0006\u0006\n\u0004\u0008 \u0010!R\u0014\u0010\"\u001a\u00020\u00058\u0006X\u0086T\u00a2\u0006\u0006\n\u0004\u0008\"\u0010!R\u0014\u0010#\u001a\u00020\u00058\u0006X\u0086T\u00a2\u0006\u0006\n\u0004\u0008#\u0010!R\u0014\u0010%\u001a\u00020$8\u0002X\u0082\u0004\u00a2\u0006\u0006\n\u0004\u0008%\u0010&R\u0016\u0010\'\u001a\u00020\u000b8\u0002@\u0002X\u0082\u000e\u00a2\u0006\u0006\n\u0004\u0008\'\u0010(R\u001d\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\u00050)8\u0006\u00a2\u0006\u000c\n\u0004\u0008*\u0010+\u001a\u0004\u0008,\u0010-R!\u0010.\u001a\u00020\u000b8\u00c6\u0001X\u0087\u0004\u00a2\u0006\u0012\n\u0004\u0008.\u0010(\u0012\u0004\u00080\u00101\u001a\u0004\u0008.\u0010/R!\u00102\u001a\u00020\u001b8\u00c6\u0001X\u0087\u0004\u00a2\u0006\u0012\n\u0004\u00082\u00103\u0012\u0004\u00086\u00101\u001a\u0004\u00084\u00105R\u0016\u00109\u001a\u0004\u0018\u00010\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00087\u00108\u00a8\u0006;"
    }
    d2 = {
        "Lcom/pspdfkit/internal/jni/PSPDFKitNative;",
        "",
        "Landroid/content/Context;",
        "context",
        "",
        "",
        "fontPaths",
        "",
        "initialize",
        "licenseKey",
        "hybridTechnology",
        "",
        "setLicenseKey",
        "validateProductIDForDetectedHybridTechnology",
        "release",
        "minidumpPath",
        "setNativeCrashPath",
        "loadNativeLibrary",
        "loadNativeOcrLibrary",
        "",
        "initializeNative",
        "(Landroid/content/Context;[Ljava/lang/String;)V",
        "license",
        "setLicense",
        "hybridId",
        "setLicenseWithHybrid",
        "destroy",
        "",
        "lid",
        "f",
        "p",
        "gdsN",
        "NDK_LIBRARY_NAME",
        "Ljava/lang/String;",
        "NDK_OCR_LIBRARY_NAME",
        "LOG_TAG",
        "Lcom/pspdfkit/internal/j7;",
        "coreLogHandler",
        "Lcom/pspdfkit/internal/j7;",
        "isInitialized",
        "Z",
        "",
        "whitelistedLaunchActivities",
        "Ljava/util/Collection;",
        "getWhitelistedLaunchActivities",
        "()Ljava/util/Collection;",
        "isDemoLicense",
        "()Z",
        "isDemoLicense$annotations",
        "()V",
        "numberOfCPUCores",
        "I",
        "getNumberOfCPUCores",
        "()I",
        "getNumberOfCPUCores$annotations",
        "getNativeLibraryPathFromResources",
        "()Ljava/lang/String;",
        "nativeLibraryPathFromResources",
        "<init>",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# static fields
.field public static final $stable:I

.field public static final INSTANCE:Lcom/pspdfkit/internal/jni/PSPDFKitNative;

.field public static final LOG_TAG:Ljava/lang/String; = "PSPDFKit.Internal.PSDPFKitNative"

.field public static final NDK_LIBRARY_NAME:Ljava/lang/String; = "pspdfkit"

.field public static final NDK_OCR_LIBRARY_NAME:Ljava/lang/String; = "pspdf_tesseract_bridge"

.field private static final coreLogHandler:Lcom/pspdfkit/internal/j7;

.field private static final isDemoLicense:Z

.field private static volatile isInitialized:Z

.field private static final numberOfCPUCores:I

.field private static final whitelistedLaunchActivities:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/pspdfkit/internal/jni/PSPDFKitNative;

    invoke-direct {v0}, Lcom/pspdfkit/internal/jni/PSPDFKitNative;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->INSTANCE:Lcom/pspdfkit/internal/jni/PSPDFKitNative;

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/j7;

    invoke-direct {v0}, Lcom/pspdfkit/internal/j7;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->coreLogHandler:Lcom/pspdfkit/internal/j7;

    const-string v0, "com.squareup.leakcanary.internal.DisplayLeakActivity"

    const-string v1, "leakcanary.internal.activity.LeakActivity"

    const-string v2, "leakcanary.internal.activity.LeakLauncherActivity"

    .line 14
    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    .line 15
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 16
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    const-string v1, "unmodifiableCollection(\n\u2026Activity\"\n        )\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->whitelistedLaunchActivities:Ljava/util/Collection;

    const/16 v0, 0x8

    sput v0, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->$stable:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getNativeLibraryPathFromResources()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    .line 1
    :try_start_0
    const-class v1, Lcom/pspdfkit/internal/jni/PSPDFKitNative;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "com.pspdfkit.native-library-path"

    invoke-virtual {v1, v2}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2
    invoke-static {v1}, Lcom/pspdfkit/internal/kb;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object v0

    :catch_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Internal.PSDPFKitNative"

    const-string v3, "Failed to get native library path but ignored exception."

    .line 5
    invoke-static {v2, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public static final native getNumberOfCPUCores()I
.end method

.method public static synthetic getNumberOfCPUCores$annotations()V
    .locals 0
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    return-void
.end method

.method public static final initialize(Landroid/content/Context;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, ", "

    const-string v1, "context"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.category.LAUNCHER"

    .line 3
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    .line 4
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 5
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 6
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v4, 0x0

    const/16 v5, 0x21

    if-lt v3, v5, :cond_0

    const-wide/16 v5, 0x0

    .line 7
    invoke-static {v5, v6}, Landroid/content/pm/PackageManager$ResolveInfoFlags;->of(J)Landroid/content/pm/PackageManager$ResolveInfoFlags;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;Landroid/content/pm/PackageManager$ResolveInfoFlags;)Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {v1, v2, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    :goto_0
    const-string v2, "if (Build.VERSION.SDK_IN\u2026ctivities(i, 0)\n        }"

    .line 11
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 20
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 21
    sget-object v6, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->whitelistedLaunchActivities:Ljava/util/Collection;

    iget-object v7, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "info"

    .line 22
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 25
    :cond_2
    invoke-interface {v1, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 26
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_4

    .line 32
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->INSTANCE:Lcom/pspdfkit/internal/jni/PSPDFKitNative;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->loadNativeLibrary(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_1

    .line 42
    :try_start_1
    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->loadNativeOcrLibrary(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0

    .line 55
    sget-object v0, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->coreLogHandler:Lcom/pspdfkit/internal/j7;

    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeNativeLogging;->setPlatformLogger(Lcom/pspdfkit/internal/jni/NativePlatformLogger;)V

    .line 59
    new-instance v0, Lcom/pspdfkit/internal/p2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/p2;-><init>(Landroid/content/Context;)V

    .line 60
    new-instance v3, Lcom/pspdfkit/internal/xu;

    invoke-direct {v3}, Lcom/pspdfkit/internal/xu;-><init>()V

    .line 61
    new-instance v5, Lcom/pspdfkit/internal/eg;

    invoke-direct {v5}, Lcom/pspdfkit/internal/eg;-><init>()V

    .line 62
    new-instance v6, Lcom/pspdfkit/internal/qh;

    invoke-direct {v6, p0}, Lcom/pspdfkit/internal/qh;-><init>(Landroid/content/Context;)V

    .line 63
    invoke-static {v0, v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeNativeServices;->init(Lcom/pspdfkit/internal/jni/NativeApplicationService;Lcom/pspdfkit/internal/jni/NativeUnicodeService;Lcom/pspdfkit/internal/jni/NativePlatformThreads;Lcom/pspdfkit/internal/jni/NativeLocalizationService;)V

    .line 69
    new-instance v0, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    if-eqz p1, :cond_3

    new-array v0, v4, [Ljava/lang/String;

    .line 254
    invoke-interface {p1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    .line 255
    :goto_2
    invoke-virtual {v1, p0, p1}, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->initializeNative(Landroid/content/Context;[Ljava/lang/String;)V

    .line 256
    sput-boolean v2, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->isInitialized:Z

    return-void

    :catch_0
    move-exception p0

    .line 257
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;

    .line 260
    invoke-static {}, Lcom/pspdfkit/internal/e8;->a()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to load native OCR libraries: The device\'s ABI set is either not compatible with PSPDFKit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", or you haven\'t added the pspdfkit-ocr depenency to your build.gradle file."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 261
    invoke-direct {p1, v0, p0}, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    :catch_1
    move-exception p0

    .line 262
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;

    .line 265
    invoke-static {}, Lcom/pspdfkit/internal/e8;->a()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to load native libraries: The device\'s ABI set is not compatible with PSPDFKit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 266
    invoke-direct {p1, v0, p0}, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    .line 267
    :cond_4
    new-instance p0, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p1, "Apps with multiple LAUNCH intents currently aren\'t supported, contact PSPDFKit support."

    invoke-direct {p0, p1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final native isDemoLicense()Z
.end method

.method public static synthetic isDemoLicense$annotations()V
    .locals 0
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    return-void
.end method

.method private final loadNativeLibrary(Landroid/content/Context;)V
    .locals 2

    const-string v0, "pspdfkit"

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->getNativeLibraryPathFromResources()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3
    invoke-static {v1}, Ljava/lang/System;->load(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    :try_start_0
    const-string v1, "com.getkeepsafe.relinker.ReLinker"

    .line 6
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    const-string v1, "8.7.3"

    .line 8
    invoke-static {p1, v0, v1}, Lcom/getkeepsafe/relinker/ReLinker;->loadLibrary(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 9
    :catch_0
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private final loadNativeOcrLibrary(Landroid/content/Context;)V
    .locals 7

    const-string v0, "pspdf_tesseract_bridge"

    .line 1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    if-nez v1, :cond_0

    return-void

    .line 2
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3
    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 7
    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "requireNonNull(primaryNativeLibraries)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, [Ljava/lang/String;

    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_2

    aget-object v5, v1, v4

    if-eqz v5, :cond_1

    const-string v6, "libpspdf_tesseract_bridge.so"

    .line 8
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    if-eqz v3, :cond_3

    :try_start_0
    const-string v1, "com.getkeepsafe.relinker.ReLinker"

    .line 15
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 16
    invoke-static {}, Lcom/getkeepsafe/relinker/ReLinker;->recursively()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->loadLibrary(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 18
    :catch_0
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    :cond_3
    :goto_2
    return-void
.end method

.method public static final release()V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeNativeLogging;->setPlatformLogger(Lcom/pspdfkit/internal/jni/NativePlatformLogger;)V

    .line 5
    sget-object v0, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->INSTANCE:Lcom/pspdfkit/internal/jni/PSPDFKitNative;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->destroy()V

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeNativeServices;->deinit()V

    const/4 v0, 0x0

    .line 7
    sput-boolean v0, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->isInitialized:Z

    return-void
.end method

.method public static final setLicenseKey(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "licenseKey"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-boolean v0, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->isInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 6
    sget-object v0, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->INSTANCE:Lcom/pspdfkit/internal/jni/PSPDFKitNative;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->validateProductIDForDetectedHybridTechnology(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 7
    invoke-virtual {v0, p0, p1}, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->setLicenseWithHybrid(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p0

    return p0

    .line 9
    :cond_0
    sget-object p1, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->INSTANCE:Lcom/pspdfkit/internal/jni/PSPDFKitNative;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->setLicense(Ljava/lang/String;)Z

    move-result p0

    return p0

    .line 10
    :cond_1
    new-instance p0, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;

    const-string p1, "initialize has to be called first."

    invoke-direct {p0, p1}, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final native setNativeCrashPath(Ljava/lang/String;)V
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation
.end method

.method private final validateProductIDForDetectedHybridTechnology(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 1
    sget v0, Lcom/pspdfkit/internal/zd;->b:I

    .line 2
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/zd;->a()Ljava/util/HashMap;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 4
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 6
    :try_start_0
    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 7
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    nop

    goto :goto_1

    :cond_1
    const-string v1, "Flutter"

    .line 8
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "ReactNative"

    if-nez v2, :cond_2

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 9
    :cond_2
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    return-object p1

    .line 14
    :cond_3
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 20
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 21
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;

    const-string v0, "PSPDFKit detected an unusual setup for your React Native project. Please follow the instructions for integrating PSPDFKit for React Native here: https://pspdfkit.com/getting-started/react-native/?react-native-platform=android-ios&project=existing-project"

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 28
    :cond_4
    new-instance v0, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;

    sget-object v1, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v2, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    const-string v1, "PSPDFKit does not support the hybrid technology %s"

    invoke-static {v1, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "format(format, *args)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_5
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;

    const-string v0, "PSPDFKit detected an unusual setup for your Flutter project. Please follow the instructions for integrating PSPDFKit for Flutter here: https://pspdfkit.com/getting-started/flutter/?flutter-platform=android-ios&project=existing-project"

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final native destroy()V
.end method

.method public final native gdsN(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public final getWhitelistedLaunchActivities()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->whitelistedLaunchActivities:Ljava/util/Collection;

    return-object v0
.end method

.method public final native initializeNative(Landroid/content/Context;[Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;
        }
    .end annotation
.end method

.method public final native lid()I
.end method

.method public final native setLicense(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;
        }
    .end annotation
.end method

.method public final native setLicenseWithHybrid(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;
        }
    .end annotation
.end method

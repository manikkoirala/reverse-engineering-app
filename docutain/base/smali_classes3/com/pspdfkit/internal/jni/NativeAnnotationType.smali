.class public final enum Lcom/pspdfkit/internal/jni/NativeAnnotationType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeAnnotationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum CARET:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum CIRCLE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum FILE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum FREETEXT:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum HIGHLIGHT:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum INK:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum LINE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum LINK:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum NONE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum NOTE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum POLYGON:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum POLYLINE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum POPUP:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum REDACT:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum RICHMEDIA:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum SCREEN:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum SOUND:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum SQUARE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum SQUIGGLY:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum STAMP:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum STRIKEOUT:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum TRAPNET:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum TYPE3D:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum UNDERLINE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum UNKNOWN:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum WATERMARK:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

.field public static final enum WIDGET:Lcom/pspdfkit/internal/jni/NativeAnnotationType;


# direct methods
.method static constructor <clinit>()V
    .locals 29

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->NONE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v3, "UNKNOWN"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->UNKNOWN:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 5
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v5, "LINK"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->LINK:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 6
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v7, "HIGHLIGHT"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->HIGHLIGHT:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 7
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v9, "STRIKEOUT"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->STRIKEOUT:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 8
    new-instance v9, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v11, "UNDERLINE"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->UNDERLINE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 9
    new-instance v11, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v13, "SQUIGGLY"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->SQUIGGLY:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 10
    new-instance v13, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v15, "FREETEXT"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->FREETEXT:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 12
    new-instance v15, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v14, "INK"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->INK:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 13
    new-instance v14, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v12, "SQUARE"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->SQUARE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 14
    new-instance v12, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v10, "CIRCLE"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->CIRCLE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 15
    new-instance v10, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v8, "LINE"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->LINE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 16
    new-instance v8, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v6, "NOTE"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->NOTE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 18
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v4, "STAMP"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->STAMP:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 19
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v2, "CARET"

    move-object/from16 v16, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->CARET:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 21
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v6, "RICHMEDIA"

    move-object/from16 v17, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->RICHMEDIA:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 23
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v4, "SCREEN"

    move-object/from16 v18, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->SCREEN:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 25
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v2, "WIDGET"

    move-object/from16 v19, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->WIDGET:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 27
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v6, "FILE"

    move-object/from16 v20, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->FILE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 28
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v4, "SOUND"

    move-object/from16 v21, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->SOUND:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 29
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v2, "POLYGON"

    move-object/from16 v22, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->POLYGON:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 30
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v6, "POLYLINE"

    move-object/from16 v23, v4

    const/16 v4, 0x15

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->POLYLINE:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 31
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v4, "POPUP"

    move-object/from16 v24, v2

    const/16 v2, 0x16

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->POPUP:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 32
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v4, "WATERMARK"

    move-object/from16 v25, v6

    const/16 v6, 0x17

    invoke-direct {v2, v4, v6}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->WATERMARK:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 33
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v6, "TRAPNET"

    move-object/from16 v26, v2

    const/16 v2, 0x18

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->TRAPNET:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 34
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v6, "TYPE3D"

    move-object/from16 v27, v4

    const/16 v4, 0x19

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->TYPE3D:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 35
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const-string v6, "REDACT"

    move-object/from16 v28, v2

    const/16 v2, 0x1a

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->REDACT:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const/16 v2, 0x1b

    new-array v2, v2, [Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    const/4 v6, 0x0

    aput-object v0, v2, v6

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object v5, v2, v0

    const/4 v0, 0x4

    aput-object v7, v2, v0

    const/4 v0, 0x5

    aput-object v9, v2, v0

    const/4 v0, 0x6

    aput-object v11, v2, v0

    const/4 v0, 0x7

    aput-object v13, v2, v0

    const/16 v0, 0x8

    aput-object v15, v2, v0

    const/16 v0, 0x9

    aput-object v14, v2, v0

    const/16 v0, 0xa

    aput-object v12, v2, v0

    const/16 v0, 0xb

    aput-object v10, v2, v0

    const/16 v0, 0xc

    aput-object v8, v2, v0

    const/16 v0, 0xd

    aput-object v16, v2, v0

    const/16 v0, 0xe

    aput-object v17, v2, v0

    const/16 v0, 0xf

    aput-object v18, v2, v0

    const/16 v0, 0x10

    aput-object v19, v2, v0

    const/16 v0, 0x11

    aput-object v20, v2, v0

    const/16 v0, 0x12

    aput-object v21, v2, v0

    const/16 v0, 0x13

    aput-object v22, v2, v0

    const/16 v0, 0x14

    aput-object v23, v2, v0

    const/16 v0, 0x15

    aput-object v24, v2, v0

    const/16 v0, 0x16

    aput-object v25, v2, v0

    const/16 v0, 0x17

    aput-object v26, v2, v0

    const/16 v0, 0x18

    aput-object v27, v2, v0

    const/16 v0, 0x19

    aput-object v28, v2, v0

    const/16 v0, 0x1a

    aput-object v4, v2, v0

    .line 36
    sput-object v2, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeAnnotationType;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeAnnotationType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeAnnotationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    return-object v0
.end method

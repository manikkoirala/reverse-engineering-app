.class public final Lcom/pspdfkit/internal/jni/NativeImageFactory$Companion;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/jni/NativeImageFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J$\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u00062\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0004H\u0007J\u001c\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u00062\u0006\u0010\r\u001a\u00020\u000eH\u0007J$\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u00062\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/pspdfkit/internal/jni/NativeImageFactory$Companion;",
        "",
        "()V",
        "DEFAULT_BUFFER_SIZE",
        "",
        "fromBitmap",
        "Landroid/util/Pair;",
        "Lcom/pspdfkit/internal/jni/NativeImage;",
        "Lcom/pspdfkit/utils/Size;",
        "bitmap",
        "Landroid/graphics/Bitmap;",
        "quality",
        "fromDataProvider",
        "dataProvider",
        "Lcom/pspdfkit/document/providers/DataProvider;",
        "fromUri",
        "context",
        "Landroid/content/Context;",
        "fileUri",
        "Landroid/net/Uri;",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
    xi = 0x30
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativeImageFactory$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromBitmap(Landroid/graphics/Bitmap;I)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "I)",
            "Landroid/util/Pair<",
            "Lcom/pspdfkit/internal/jni/NativeImage;",
            "Lcom/pspdfkit/utils/Size;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "bitmap"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const v1, 0x7d000

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 3
    sget-object p2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x64

    invoke-virtual {p1, p2, v1, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 4
    new-instance p2, Lcom/pspdfkit/utils/Size;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    int-to-float p1, p1

    invoke-direct {p2, v1, p1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 5
    new-instance p1, Landroid/util/Pair;

    new-instance v1, Lcom/pspdfkit/internal/jni/NativeImage;

    sget-object v3, Lcom/pspdfkit/internal/jni/NativeImageEncoding;->PNG:Lcom/pspdfkit/internal/jni/NativeImageEncoding;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {v1, v3, v0, v2}, Lcom/pspdfkit/internal/jni/NativeImage;-><init>(Lcom/pspdfkit/internal/jni/NativeImageEncoding;[BLcom/pspdfkit/internal/jni/NativeDataDescriptor;)V

    invoke-direct {p1, v1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 7
    :cond_0
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {p1, v1, p2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 8
    new-instance p2, Lcom/pspdfkit/utils/Size;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    int-to-float p1, p1

    invoke-direct {p2, v1, p1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 9
    new-instance p1, Landroid/util/Pair;

    new-instance v1, Lcom/pspdfkit/internal/jni/NativeImage;

    sget-object v3, Lcom/pspdfkit/internal/jni/NativeImageEncoding;->JPEG:Lcom/pspdfkit/internal/jni/NativeImageEncoding;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {v1, v3, v0, v2}, Lcom/pspdfkit/internal/jni/NativeImage;-><init>(Lcom/pspdfkit/internal/jni/NativeImageEncoding;[BLcom/pspdfkit/internal/jni/NativeDataDescriptor;)V

    invoke-direct {p1, v1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-object p1
.end method

.method public final fromDataProvider(Lcom/pspdfkit/document/providers/DataProvider;)Landroid/util/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/document/providers/DataProvider;",
            ")",
            "Landroid/util/Pair<",
            "Lcom/pspdfkit/internal/jni/NativeImage;",
            "Lcom/pspdfkit/utils/Size;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "dataProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/internal/r4;->a(Lcom/pspdfkit/document/providers/DataProvider;)Lcom/pspdfkit/internal/l4;

    move-result-object p1

    .line 2
    new-instance v0, Landroid/util/Pair;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeImage;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/l4;->a()Landroid/graphics/Bitmap$CompressFormat;

    move-result-object v2

    invoke-static {v2}, Lcom/pspdfkit/internal/sj;->a(Landroid/graphics/Bitmap$CompressFormat;)Lcom/pspdfkit/internal/jni/NativeImageEncoding;

    move-result-object v2

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/l4;->b()[B

    move-result-object v3

    const/4 v4, 0x0

    .line 6
    invoke-direct {v1, v2, v3, v4}, Lcom/pspdfkit/internal/jni/NativeImage;-><init>(Lcom/pspdfkit/internal/jni/NativeImageEncoding;[BLcom/pspdfkit/internal/jni/NativeDataDescriptor;)V

    .line 11
    new-instance v2, Lcom/pspdfkit/utils/Size;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/l4;->d()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Lcom/pspdfkit/internal/l4;->c()I

    move-result p1

    int-to-float p1, p1

    invoke-direct {v2, v3, p1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 12
    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final fromUri(Landroid/content/Context;Landroid/net/Uri;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            ")",
            "Landroid/util/Pair<",
            "Lcom/pspdfkit/internal/jni/NativeImage;",
            "Lcom/pspdfkit/utils/Size;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileUri"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/r4;->b(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/internal/l4;

    move-result-object p1

    .line 2
    new-instance p2, Landroid/util/Pair;

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeImage;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/l4;->a()Landroid/graphics/Bitmap$CompressFormat;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/sj;->a(Landroid/graphics/Bitmap$CompressFormat;)Lcom/pspdfkit/internal/jni/NativeImageEncoding;

    move-result-object v1

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/l4;->b()[B

    move-result-object v2

    const/4 v3, 0x0

    .line 6
    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/internal/jni/NativeImage;-><init>(Lcom/pspdfkit/internal/jni/NativeImageEncoding;[BLcom/pspdfkit/internal/jni/NativeDataDescriptor;)V

    .line 11
    new-instance v1, Lcom/pspdfkit/utils/Size;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/l4;->d()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/l4;->c()I

    move-result p1

    int-to-float p1, p1

    invoke-direct {v1, v2, p1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 12
    invoke-direct {p2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p2
.end method

.class public final enum Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum DONT_RENDER_APSTREAM:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum DONT_RENDER_IMAGE_OBJECTS:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum DONT_RENDER_PATH_OBJECTS:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum DONT_RENDER_TEXT_OBJECTS:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum DRAW_REDACT_AS_REDACTED:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum PREMULTIPLY_ALPHA:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum RENDER_ANNOTATIONS:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum RENDER_FOR_PRINTING:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum RENDER_GRAYSCALE:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum RENDER_INVERTED_COLORS:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum RENDER_ON_ORIGINAL_DOCUMENT:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum RENDER_TEXT_NATIVE:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum REVERSE_BYTE_ORDER:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

.field public static final enum USE_CLEAR_TYPE_AA:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v1, "RENDER_ANNOTATIONS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->RENDER_ANNOTATIONS:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v3, "RENDER_TEXT_NATIVE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->RENDER_TEXT_NATIVE:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    .line 5
    new-instance v3, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v5, "RENDER_GRAYSCALE"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->RENDER_GRAYSCALE:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    .line 7
    new-instance v5, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v7, "RENDER_INVERTED_COLORS"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->RENDER_INVERTED_COLORS:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    .line 9
    new-instance v7, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v9, "RENDER_FOR_PRINTING"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->RENDER_FOR_PRINTING:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    .line 11
    new-instance v9, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v11, "USE_CLEAR_TYPE_AA"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->USE_CLEAR_TYPE_AA:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    .line 13
    new-instance v11, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v13, "REVERSE_BYTE_ORDER"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->REVERSE_BYTE_ORDER:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    .line 18
    new-instance v13, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v15, "PREMULTIPLY_ALPHA"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->PREMULTIPLY_ALPHA:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    .line 23
    new-instance v15, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v14, "DONT_RENDER_APSTREAM"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->DONT_RENDER_APSTREAM:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    .line 28
    new-instance v14, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v12, "DRAW_REDACT_AS_REDACTED"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->DRAW_REDACT_AS_REDACTED:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    .line 33
    new-instance v12, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v10, "RENDER_ON_ORIGINAL_DOCUMENT"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->RENDER_ON_ORIGINAL_DOCUMENT:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    .line 38
    new-instance v10, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v8, "DONT_RENDER_TEXT_OBJECTS"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->DONT_RENDER_TEXT_OBJECTS:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    .line 44
    new-instance v8, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v6, "DONT_RENDER_PATH_OBJECTS"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->DONT_RENDER_PATH_OBJECTS:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    .line 50
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const-string v4, "DONT_RENDER_IMAGE_OBJECTS"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->DONT_RENDER_IMAGE_OBJECTS:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const/16 v4, 0xe

    new-array v4, v4, [Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    const/16 v16, 0x0

    aput-object v0, v4, v16

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v3, v4, v0

    const/4 v0, 0x3

    aput-object v5, v4, v0

    const/4 v0, 0x4

    aput-object v7, v4, v0

    const/4 v0, 0x5

    aput-object v9, v4, v0

    const/4 v0, 0x6

    aput-object v11, v4, v0

    const/4 v0, 0x7

    aput-object v13, v4, v0

    const/16 v0, 0x8

    aput-object v15, v4, v0

    const/16 v0, 0x9

    aput-object v14, v4, v0

    const/16 v0, 0xa

    aput-object v12, v4, v0

    const/16 v0, 0xb

    aput-object v10, v4, v0

    const/16 v0, 0xc

    aput-object v8, v4, v0

    aput-object v6, v4, v2

    .line 51
    sput-object v4, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->$VALUES:[Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->$VALUES:[Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    return-object v0
.end method

.class public final enum Lcom/pspdfkit/internal/jni/NativeFormType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeFormType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeFormType;

.field public static final enum CHECKBOX:Lcom/pspdfkit/internal/jni/NativeFormType;

.field public static final enum COMBOBOX:Lcom/pspdfkit/internal/jni/NativeFormType;

.field public static final enum LISTBOX:Lcom/pspdfkit/internal/jni/NativeFormType;

.field public static final enum PUSHBUTTON:Lcom/pspdfkit/internal/jni/NativeFormType;

.field public static final enum RADIOBUTTON:Lcom/pspdfkit/internal/jni/NativeFormType;

.field public static final enum SIGNATURE:Lcom/pspdfkit/internal/jni/NativeFormType;

.field public static final enum TEXT:Lcom/pspdfkit/internal/jni/NativeFormType;

.field public static final enum UNKNOWN:Lcom/pspdfkit/internal/jni/NativeFormType;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeFormType;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeFormType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeFormType;->UNKNOWN:Lcom/pspdfkit/internal/jni/NativeFormType;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeFormType;

    const-string v3, "PUSHBUTTON"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeFormType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeFormType;->PUSHBUTTON:Lcom/pspdfkit/internal/jni/NativeFormType;

    .line 4
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeFormType;

    const-string v5, "RADIOBUTTON"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeFormType;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeFormType;->RADIOBUTTON:Lcom/pspdfkit/internal/jni/NativeFormType;

    .line 5
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeFormType;

    const-string v7, "CHECKBOX"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeFormType;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeFormType;->CHECKBOX:Lcom/pspdfkit/internal/jni/NativeFormType;

    .line 7
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeFormType;

    const-string v9, "TEXT"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativeFormType;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativeFormType;->TEXT:Lcom/pspdfkit/internal/jni/NativeFormType;

    .line 9
    new-instance v9, Lcom/pspdfkit/internal/jni/NativeFormType;

    const-string v11, "LISTBOX"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativeFormType;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativeFormType;->LISTBOX:Lcom/pspdfkit/internal/jni/NativeFormType;

    .line 10
    new-instance v11, Lcom/pspdfkit/internal/jni/NativeFormType;

    const-string v13, "COMBOBOX"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativeFormType;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativeFormType;->COMBOBOX:Lcom/pspdfkit/internal/jni/NativeFormType;

    .line 12
    new-instance v13, Lcom/pspdfkit/internal/jni/NativeFormType;

    const-string v15, "SIGNATURE"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativeFormType;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativeFormType;->SIGNATURE:Lcom/pspdfkit/internal/jni/NativeFormType;

    const/16 v15, 0x8

    new-array v15, v15, [Lcom/pspdfkit/internal/jni/NativeFormType;

    aput-object v0, v15, v2

    aput-object v1, v15, v4

    aput-object v3, v15, v6

    aput-object v5, v15, v8

    aput-object v7, v15, v10

    aput-object v9, v15, v12

    const/4 v0, 0x6

    aput-object v11, v15, v0

    aput-object v13, v15, v14

    .line 13
    sput-object v15, Lcom/pspdfkit/internal/jni/NativeFormType;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeFormType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeFormType;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeFormType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeFormType;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeFormType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeFormType;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeFormType;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeFormType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeFormType;

    return-object v0
.end method

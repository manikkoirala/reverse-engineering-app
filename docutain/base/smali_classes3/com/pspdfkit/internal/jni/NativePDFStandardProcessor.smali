.class public abstract Lcom/pspdfkit/internal/jni/NativePDFStandardProcessor;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/jni/NativePDFStandardProcessor$CppProxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Lcom/pspdfkit/internal/jni/NativeDocument;Lcom/pspdfkit/internal/jni/NativePDFStandardLevel;Lcom/pspdfkit/internal/jni/NativePDFStandardProcessorContext;)Lcom/pspdfkit/internal/jni/NativePDFStandardProcessor;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/jni/NativePDFStandardProcessor$CppProxy;->create(Lcom/pspdfkit/internal/jni/NativeDocument;Lcom/pspdfkit/internal/jni/NativePDFStandardLevel;Lcom/pspdfkit/internal/jni/NativePDFStandardProcessorContext;)Lcom/pspdfkit/internal/jni/NativePDFStandardProcessor;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract convert(Lcom/pspdfkit/internal/jni/NativePDFStandardProcessorDelegate;Lcom/pspdfkit/internal/jni/NativeDataProvider;)Lcom/pspdfkit/internal/jni/NativePDFStandardProcessorResult;
.end method

.method public abstract validate(Lcom/pspdfkit/internal/jni/NativePDFStandardProcessorDelegate;)Lcom/pspdfkit/internal/jni/NativePDFStandardProcessorResult;
.end method

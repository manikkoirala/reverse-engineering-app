.class public final Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final mDocumentDataStore:Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;

.field final mErrorCode:I

.field final mErrorString:Ljava/lang/String;

.field final mHasError:Z


# direct methods
.method public constructor <init>(ZLjava/lang/String;ILcom/pspdfkit/internal/jni/NativeDocumentDataStore;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->mHasError:Z

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->mErrorString:Ljava/lang/String;

    .line 4
    iput p3, p0, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->mErrorCode:I

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->mDocumentDataStore:Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;

    return-void
.end method


# virtual methods
.method public getDocumentDataStore()Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->mDocumentDataStore:Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;

    return-object v0
.end method

.method public getErrorCode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->mErrorCode:I

    return v0
.end method

.method public getErrorString()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->mErrorString:Ljava/lang/String;

    return-object v0
.end method

.method public getHasError()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->mHasError:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NativeDocumentDataStoreCreateResult{mHasError="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->mHasError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ",mErrorString="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->mErrorString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",mErrorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->mErrorCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",mDocumentDataStore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->mDocumentDataStore:Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

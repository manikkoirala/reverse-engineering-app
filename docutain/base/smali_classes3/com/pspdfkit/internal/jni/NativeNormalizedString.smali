.class public final Lcom/pspdfkit/internal/jni/NativeNormalizedString;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final mNewLength:I

.field final mOldLength:I

.field final mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/jni/NativeNormalizedString;->mValue:Ljava/lang/String;

    .line 3
    iput p2, p0, Lcom/pspdfkit/internal/jni/NativeNormalizedString;->mOldLength:I

    .line 4
    iput p3, p0, Lcom/pspdfkit/internal/jni/NativeNormalizedString;->mNewLength:I

    return-void
.end method


# virtual methods
.method public getNewLength()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/jni/NativeNormalizedString;->mNewLength:I

    return v0
.end method

.method public getOldLength()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/jni/NativeNormalizedString;->mOldLength:I

    return v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativeNormalizedString;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NativeNormalizedString{mValue="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativeNormalizedString;->mValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",mOldLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/internal/jni/NativeNormalizedString;->mOldLength:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",mNewLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/internal/jni/NativeNormalizedString;->mNewLength:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

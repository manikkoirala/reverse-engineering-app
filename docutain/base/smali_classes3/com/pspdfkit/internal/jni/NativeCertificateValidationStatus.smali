.class public final enum Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum EXPIRED:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum EXPIRED_BUT_VALID_IN_THE_PAST:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum EXPIRED_NO_POE:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum FAILED_RETRIEVE_SIGNATURE_CONTENTS:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum GENERAL_VALIDATION_PROBLEM:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum INVALID:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum NOT_YET_VALID:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum NOT_YET_VALID_NO_POE:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum OK:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum OK_BUT_COULD_NOT_CHECK_REVOCATION:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum OK_BUT_SELF_SIGNED:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum REVOKED:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum REVOKED_BUT_VALID_IN_THE_PAST:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum REVOKED_NO_POE:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

.field public static final enum UNTRUSTED:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v1, "OK"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->OK:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v3, "OK_BUT_SELF_SIGNED"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->OK_BUT_SELF_SIGNED:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 8
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v5, "OK_BUT_COULD_NOT_CHECK_REVOCATION"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->OK_BUT_COULD_NOT_CHECK_REVOCATION:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 13
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v7, "UNTRUSTED"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->UNTRUSTED:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 19
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v9, "EXPIRED"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->EXPIRED:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 26
    new-instance v9, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v11, "EXPIRED_NO_POE"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->EXPIRED_NO_POE:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 33
    new-instance v11, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v13, "EXPIRED_BUT_VALID_IN_THE_PAST"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->EXPIRED_BUT_VALID_IN_THE_PAST:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 38
    new-instance v13, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v15, "NOT_YET_VALID"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->NOT_YET_VALID:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 45
    new-instance v15, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v14, "NOT_YET_VALID_NO_POE"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->NOT_YET_VALID_NO_POE:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 47
    new-instance v14, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v12, "INVALID"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->INVALID:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 52
    new-instance v12, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v10, "REVOKED"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->REVOKED:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 58
    new-instance v10, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v8, "REVOKED_NO_POE"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->REVOKED_NO_POE:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 65
    new-instance v8, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v6, "REVOKED_BUT_VALID_IN_THE_PAST"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->REVOKED_BUT_VALID_IN_THE_PAST:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 67
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v4, "FAILED_RETRIEVE_SIGNATURE_CONTENTS"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->FAILED_RETRIEVE_SIGNATURE_CONTENTS:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 69
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const-string v2, "GENERAL_VALIDATION_PROBLEM"

    move-object/from16 v17, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->GENERAL_VALIDATION_PROBLEM:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const/16 v2, 0xf

    new-array v2, v2, [Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    const/16 v16, 0x0

    aput-object v0, v2, v16

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object v5, v2, v0

    const/4 v0, 0x4

    aput-object v7, v2, v0

    const/4 v0, 0x5

    aput-object v9, v2, v0

    const/4 v0, 0x6

    aput-object v11, v2, v0

    const/4 v0, 0x7

    aput-object v13, v2, v0

    const/16 v0, 0x8

    aput-object v15, v2, v0

    const/16 v0, 0x9

    aput-object v14, v2, v0

    const/16 v0, 0xa

    aput-object v12, v2, v0

    const/16 v0, 0xb

    aput-object v10, v2, v0

    const/16 v0, 0xc

    aput-object v8, v2, v0

    const/16 v0, 0xd

    aput-object v17, v2, v0

    aput-object v4, v2, v6

    .line 70
    sput-object v2, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    return-object v0
.end method

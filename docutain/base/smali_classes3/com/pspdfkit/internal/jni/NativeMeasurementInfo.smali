.class public abstract Lcom/pspdfkit/internal/jni/NativeMeasurementInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/jni/NativeMeasurementInfo$CppProxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getMode()Lcom/pspdfkit/internal/jni/NativeMeasurementMode;
.end method

.method public abstract getPrecision()Lcom/pspdfkit/internal/jni/NativeMeasurementPrecision;
.end method

.method public abstract getScale()Lcom/pspdfkit/internal/jni/NativeMeasurementScale;
.end method

.method public abstract getValue()D
.end method

.class public final enum Lcom/pspdfkit/internal/jni/NativeLineEndType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeLineEndType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeLineEndType;

.field public static final enum BUTT:Lcom/pspdfkit/internal/jni/NativeLineEndType;

.field public static final enum CIRCLE:Lcom/pspdfkit/internal/jni/NativeLineEndType;

.field public static final enum CLOSED_ARROW:Lcom/pspdfkit/internal/jni/NativeLineEndType;

.field public static final enum DIAMOND:Lcom/pspdfkit/internal/jni/NativeLineEndType;

.field public static final enum NONE:Lcom/pspdfkit/internal/jni/NativeLineEndType;

.field public static final enum OPEN_ARROW:Lcom/pspdfkit/internal/jni/NativeLineEndType;

.field public static final enum REVERSE_CLOSED_ARROW:Lcom/pspdfkit/internal/jni/NativeLineEndType;

.field public static final enum REVERSE_OPEN_ARROW:Lcom/pspdfkit/internal/jni/NativeLineEndType;

.field public static final enum SLASH:Lcom/pspdfkit/internal/jni/NativeLineEndType;

.field public static final enum SQUARE:Lcom/pspdfkit/internal/jni/NativeLineEndType;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeLineEndType;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeLineEndType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeLineEndType;->NONE:Lcom/pspdfkit/internal/jni/NativeLineEndType;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeLineEndType;

    const-string v3, "SQUARE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeLineEndType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeLineEndType;->SQUARE:Lcom/pspdfkit/internal/jni/NativeLineEndType;

    .line 3
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeLineEndType;

    const-string v5, "CIRCLE"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeLineEndType;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeLineEndType;->CIRCLE:Lcom/pspdfkit/internal/jni/NativeLineEndType;

    .line 4
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeLineEndType;

    const-string v7, "DIAMOND"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeLineEndType;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeLineEndType;->DIAMOND:Lcom/pspdfkit/internal/jni/NativeLineEndType;

    .line 5
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeLineEndType;

    const-string v9, "OPEN_ARROW"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativeLineEndType;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativeLineEndType;->OPEN_ARROW:Lcom/pspdfkit/internal/jni/NativeLineEndType;

    .line 6
    new-instance v9, Lcom/pspdfkit/internal/jni/NativeLineEndType;

    const-string v11, "CLOSED_ARROW"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativeLineEndType;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativeLineEndType;->CLOSED_ARROW:Lcom/pspdfkit/internal/jni/NativeLineEndType;

    .line 7
    new-instance v11, Lcom/pspdfkit/internal/jni/NativeLineEndType;

    const-string v13, "BUTT"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativeLineEndType;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativeLineEndType;->BUTT:Lcom/pspdfkit/internal/jni/NativeLineEndType;

    .line 8
    new-instance v13, Lcom/pspdfkit/internal/jni/NativeLineEndType;

    const-string v15, "REVERSE_OPEN_ARROW"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativeLineEndType;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativeLineEndType;->REVERSE_OPEN_ARROW:Lcom/pspdfkit/internal/jni/NativeLineEndType;

    .line 9
    new-instance v15, Lcom/pspdfkit/internal/jni/NativeLineEndType;

    const-string v14, "REVERSE_CLOSED_ARROW"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativeLineEndType;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativeLineEndType;->REVERSE_CLOSED_ARROW:Lcom/pspdfkit/internal/jni/NativeLineEndType;

    .line 10
    new-instance v14, Lcom/pspdfkit/internal/jni/NativeLineEndType;

    const-string v12, "SLASH"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativeLineEndType;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativeLineEndType;->SLASH:Lcom/pspdfkit/internal/jni/NativeLineEndType;

    const/16 v12, 0xa

    new-array v12, v12, [Lcom/pspdfkit/internal/jni/NativeLineEndType;

    aput-object v0, v12, v2

    aput-object v1, v12, v4

    aput-object v3, v12, v6

    aput-object v5, v12, v8

    const/4 v0, 0x4

    aput-object v7, v12, v0

    const/4 v0, 0x5

    aput-object v9, v12, v0

    const/4 v0, 0x6

    aput-object v11, v12, v0

    const/4 v0, 0x7

    aput-object v13, v12, v0

    const/16 v0, 0x8

    aput-object v15, v12, v0

    aput-object v14, v12, v10

    .line 11
    sput-object v12, Lcom/pspdfkit/internal/jni/NativeLineEndType;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeLineEndType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeLineEndType;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeLineEndType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeLineEndType;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeLineEndType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeLineEndType;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeLineEndType;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeLineEndType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeLineEndType;

    return-object v0
.end method

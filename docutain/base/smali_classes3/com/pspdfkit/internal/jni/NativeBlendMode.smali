.class public final enum Lcom/pspdfkit/internal/jni/NativeBlendMode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeBlendMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeBlendMode;

.field public static final enum COLOR_BURN:Lcom/pspdfkit/internal/jni/NativeBlendMode;

.field public static final enum COLOR_DODGE:Lcom/pspdfkit/internal/jni/NativeBlendMode;

.field public static final enum DARKEN:Lcom/pspdfkit/internal/jni/NativeBlendMode;

.field public static final enum DIFFERENCE:Lcom/pspdfkit/internal/jni/NativeBlendMode;

.field public static final enum EXCLUSION:Lcom/pspdfkit/internal/jni/NativeBlendMode;

.field public static final enum HARD_LIGHT:Lcom/pspdfkit/internal/jni/NativeBlendMode;

.field public static final enum LIGHTEN:Lcom/pspdfkit/internal/jni/NativeBlendMode;

.field public static final enum MULTIPLY:Lcom/pspdfkit/internal/jni/NativeBlendMode;

.field public static final enum NORMAL:Lcom/pspdfkit/internal/jni/NativeBlendMode;

.field public static final enum OVERLAY:Lcom/pspdfkit/internal/jni/NativeBlendMode;

.field public static final enum SCREEN:Lcom/pspdfkit/internal/jni/NativeBlendMode;

.field public static final enum SOFT_LIGHT:Lcom/pspdfkit/internal/jni/NativeBlendMode;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    const-string v1, "NORMAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeBlendMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeBlendMode;->NORMAL:Lcom/pspdfkit/internal/jni/NativeBlendMode;

    .line 7
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    const-string v3, "MULTIPLY"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeBlendMode;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeBlendMode;->MULTIPLY:Lcom/pspdfkit/internal/jni/NativeBlendMode;

    .line 13
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    const-string v5, "SCREEN"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeBlendMode;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeBlendMode;->SCREEN:Lcom/pspdfkit/internal/jni/NativeBlendMode;

    .line 18
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    const-string v7, "OVERLAY"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeBlendMode;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeBlendMode;->OVERLAY:Lcom/pspdfkit/internal/jni/NativeBlendMode;

    .line 24
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    const-string v9, "DARKEN"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativeBlendMode;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativeBlendMode;->DARKEN:Lcom/pspdfkit/internal/jni/NativeBlendMode;

    .line 30
    new-instance v9, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    const-string v11, "LIGHTEN"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativeBlendMode;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativeBlendMode;->LIGHTEN:Lcom/pspdfkit/internal/jni/NativeBlendMode;

    .line 35
    new-instance v11, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    const-string v13, "COLOR_DODGE"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativeBlendMode;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativeBlendMode;->COLOR_DODGE:Lcom/pspdfkit/internal/jni/NativeBlendMode;

    .line 40
    new-instance v13, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    const-string v15, "COLOR_BURN"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativeBlendMode;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativeBlendMode;->COLOR_BURN:Lcom/pspdfkit/internal/jni/NativeBlendMode;

    .line 47
    new-instance v15, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    const-string v14, "SOFT_LIGHT"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativeBlendMode;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativeBlendMode;->SOFT_LIGHT:Lcom/pspdfkit/internal/jni/NativeBlendMode;

    .line 52
    new-instance v14, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    const-string v12, "HARD_LIGHT"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativeBlendMode;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativeBlendMode;->HARD_LIGHT:Lcom/pspdfkit/internal/jni/NativeBlendMode;

    .line 58
    new-instance v12, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    const-string v10, "DIFFERENCE"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/internal/jni/NativeBlendMode;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/internal/jni/NativeBlendMode;->DIFFERENCE:Lcom/pspdfkit/internal/jni/NativeBlendMode;

    .line 63
    new-instance v10, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    const-string v8, "EXCLUSION"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/internal/jni/NativeBlendMode;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/internal/jni/NativeBlendMode;->EXCLUSION:Lcom/pspdfkit/internal/jni/NativeBlendMode;

    const/16 v8, 0xc

    new-array v8, v8, [Lcom/pspdfkit/internal/jni/NativeBlendMode;

    aput-object v0, v8, v2

    aput-object v1, v8, v4

    const/4 v0, 0x2

    aput-object v3, v8, v0

    const/4 v0, 0x3

    aput-object v5, v8, v0

    const/4 v0, 0x4

    aput-object v7, v8, v0

    const/4 v0, 0x5

    aput-object v9, v8, v0

    const/4 v0, 0x6

    aput-object v11, v8, v0

    const/4 v0, 0x7

    aput-object v13, v8, v0

    const/16 v0, 0x8

    aput-object v15, v8, v0

    const/16 v0, 0x9

    aput-object v14, v8, v0

    const/16 v0, 0xa

    aput-object v12, v8, v0

    aput-object v10, v8, v6

    .line 64
    sput-object v8, Lcom/pspdfkit/internal/jni/NativeBlendMode;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeBlendMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeBlendMode;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeBlendMode;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeBlendMode;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeBlendMode;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeBlendMode;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeBlendMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeBlendMode;

    return-object v0
.end method

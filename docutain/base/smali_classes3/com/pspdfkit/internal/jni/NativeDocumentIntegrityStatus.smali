.class public final enum Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

.field public static final enum FAILED_COMPUTE_DIGEST:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

.field public static final enum FAILED_ENCRYPTION_PADDING:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

.field public static final enum FAILED_RETRIEVE_BYTE_RANGE:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

.field public static final enum FAILED_RETRIEVE_PUBLIC_KEY:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

.field public static final enum FAILED_RETRIEVE_SIGNATURE_CONTENTS:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

.field public static final enum FAILED_RETRIEVE_SIGNING_CERTIFICATE:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

.field public static final enum FAILED_UNSUPPORTED_SIGNATURE_TYPE:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

.field public static final enum GENERAL_FAILURE:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

.field public static final enum OK:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

.field public static final enum TAMPERED_DOCUMENT:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    const-string v1, "OK"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->OK:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    const-string v3, "TAMPERED_DOCUMENT"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->TAMPERED_DOCUMENT:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    .line 5
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    const-string v5, "FAILED_RETRIEVE_SIGNATURE_CONTENTS"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->FAILED_RETRIEVE_SIGNATURE_CONTENTS:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    .line 7
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    const-string v7, "FAILED_RETRIEVE_BYTE_RANGE"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->FAILED_RETRIEVE_BYTE_RANGE:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    .line 9
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    const-string v9, "FAILED_COMPUTE_DIGEST"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->FAILED_COMPUTE_DIGEST:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    .line 11
    new-instance v9, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    const-string v11, "FAILED_RETRIEVE_SIGNING_CERTIFICATE"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->FAILED_RETRIEVE_SIGNING_CERTIFICATE:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    .line 13
    new-instance v11, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    const-string v13, "FAILED_RETRIEVE_PUBLIC_KEY"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->FAILED_RETRIEVE_PUBLIC_KEY:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    .line 15
    new-instance v13, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    const-string v15, "FAILED_ENCRYPTION_PADDING"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->FAILED_ENCRYPTION_PADDING:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    .line 20
    new-instance v15, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    const-string v14, "FAILED_UNSUPPORTED_SIGNATURE_TYPE"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->FAILED_UNSUPPORTED_SIGNATURE_TYPE:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    .line 22
    new-instance v14, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    const-string v12, "GENERAL_FAILURE"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->GENERAL_FAILURE:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    const/16 v12, 0xa

    new-array v12, v12, [Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    aput-object v0, v12, v2

    aput-object v1, v12, v4

    aput-object v3, v12, v6

    aput-object v5, v12, v8

    const/4 v0, 0x4

    aput-object v7, v12, v0

    const/4 v0, 0x5

    aput-object v9, v12, v0

    const/4 v0, 0x6

    aput-object v11, v12, v0

    const/4 v0, 0x7

    aput-object v13, v12, v0

    const/16 v0, 0x8

    aput-object v15, v12, v0

    aput-object v14, v12, v10

    .line 23
    sput-object v12, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    return-object v0
.end method

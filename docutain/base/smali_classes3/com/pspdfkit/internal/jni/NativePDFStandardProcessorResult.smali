.class public final Lcom/pspdfkit/internal/jni/NativePDFStandardProcessorResult;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final mError:Lcom/pspdfkit/internal/jni/NativePDFStandardError;

.field final mInfo:Lcom/pspdfkit/internal/jni/NativePDFStandardCheckResult;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/jni/NativePDFStandardError;Lcom/pspdfkit/internal/jni/NativePDFStandardCheckResult;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardProcessorResult;->mError:Lcom/pspdfkit/internal/jni/NativePDFStandardError;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardProcessorResult;->mInfo:Lcom/pspdfkit/internal/jni/NativePDFStandardCheckResult;

    return-void
.end method


# virtual methods
.method public getError()Lcom/pspdfkit/internal/jni/NativePDFStandardError;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardProcessorResult;->mError:Lcom/pspdfkit/internal/jni/NativePDFStandardError;

    return-object v0
.end method

.method public getInfo()Lcom/pspdfkit/internal/jni/NativePDFStandardCheckResult;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardProcessorResult;->mInfo:Lcom/pspdfkit/internal/jni/NativePDFStandardCheckResult;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NativePDFStandardProcessorResult{mError="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardProcessorResult;->mError:Lcom/pspdfkit/internal/jni/NativePDFStandardError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ",mInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardProcessorResult;->mInfo:Lcom/pspdfkit/internal/jni/NativePDFStandardCheckResult;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final enum Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum ACROFORM_HAS_NEED_APPEARANCES:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum ADDITIONAL_ACTIONS_IN_CATALOG_OR_PAGE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum ALTERNATE_PRESENTATIONS_IN_DOCUMENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum COMPRESSED_STREAM_FILTER:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum DATA_AFTER_EOF:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum DEVICE_CMYK_WITH_NO_CMYK_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum DEVICE_GRAY_WITH_NO_ICC_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum DEVICE_RGB_WITH_NO_RGB_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum DOCUMENT_CATALOG_HAS_NEEDS_RENDERING:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum DOCUMENT_HAS_XFA_FORMS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum ENCRYPTED_FILE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum EXTGSTATE_SHALL_NOT_CONTAIN_HTP:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum EXTGSTATE_SHALL_NOT_CONTAIN_NON_DEFAULT_TR2:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum FIELD_HAS_ACTIONS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum FILE_EXCEEDS_IMPLEMENTATION_LIMITS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum FILE_HEADER_NOT_OFFSET_0:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum FILE_NO_BYTE_MARKS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum FONT_NOT_EMBEDDED:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum HALFTONE_MUST_NOT_CONTAIN_HALFTONE_NAME:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum HALFTONE_TYPE_MUST_BE_1_OR_5:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INCOMPATIBLE_CIDSET:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INCOMPATIBLE_CID_SYSTEM_INFO:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INCOMPATIBLE_CID_TO_GID_MAP:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_ANNOTATION_ACTIONS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_APPEARANCE_STREAM:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_BLEND_MODE_IN_EXT_GSTATE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_DIGITAL_SIGNATURE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_DOCUMENT_METADATA:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_EMBEDDED_FILE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_FILE_SPECIFICATION:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_FORM_X_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_IMAGE_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_IMAGE_PROPERTY:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_NAME_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_OPTIONAL_CONTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_OUTPUT_INTENT_COLOR_PROFILE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_RENDERING_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_STREAM_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_STRING_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_TRANSFER_FUNCTION:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum INVALID_USER_RIGHTS_DICTIONARY:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum MISSING_DOCUMENT_METADATA:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum NON_CONFORMING_FONT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum NOT_PERMITTED_ANNOTATION_FLAGS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum NOT_PERMITTED_ANNOTATION_TYPE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum NO_ID_IN_TRAILER:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum OUTPUT_INTENTS_WITH_NON_MATCHING_DESTINATION_OUTPUT_PROFILES:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum OVERPRINT_WITH_ICCBASED_CMYK:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum PAGE_TRANSPARENCY_BUT_NO_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum REQUIREMENTS_IN_DOCUMENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum SEPARATION_WITH_DIFFERENT_TINT_TRANSFORM_OR_ALTERNATE_SPACE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

.field public static final enum SPOT_COLOR_IN_DEVICE_N_BUT_NO_COLORANT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;


# direct methods
.method static constructor <clinit>()V
    .locals 55

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v1, "FILE_HEADER_NOT_OFFSET_0"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->FILE_HEADER_NOT_OFFSET_0:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v3, "FILE_NO_BYTE_MARKS"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->FILE_NO_BYTE_MARKS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 5
    new-instance v3, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v5, "NO_ID_IN_TRAILER"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->NO_ID_IN_TRAILER:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 7
    new-instance v5, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v7, "ENCRYPTED_FILE"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->ENCRYPTED_FILE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 9
    new-instance v7, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v9, "DATA_AFTER_EOF"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->DATA_AFTER_EOF:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 11
    new-instance v9, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v11, "NON_CONFORMING_FONT"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->NON_CONFORMING_FONT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 13
    new-instance v11, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v13, "INVALID_STRING_OBJECT"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_STRING_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 15
    new-instance v13, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v15, "INVALID_STREAM_OBJECT"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_STREAM_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 17
    new-instance v15, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v14, "COMPRESSED_STREAM_FILTER"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->COMPRESSED_STREAM_FILTER:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 19
    new-instance v14, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v12, "INVALID_NAME_OBJECT"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_NAME_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 21
    new-instance v12, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v10, "FILE_EXCEEDS_IMPLEMENTATION_LIMITS"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->FILE_EXCEEDS_IMPLEMENTATION_LIMITS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 23
    new-instance v10, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v8, "INVALID_USER_RIGHTS_DICTIONARY"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_USER_RIGHTS_DICTIONARY:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 25
    new-instance v8, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INCOMPATIBLE_CID_SYSTEM_INFO"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INCOMPATIBLE_CID_SYSTEM_INFO:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 27
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v4, "INVALID_OUTPUT_INTENT"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 29
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v2, "INCOMPATIBLE_CID_TO_GID_MAP"

    move-object/from16 v16, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INCOMPATIBLE_CID_TO_GID_MAP:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 31
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INCOMPATIBLE_CIDSET"

    move-object/from16 v17, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INCOMPATIBLE_CIDSET:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 33
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v4, "INVALID_OUTPUT_INTENT_COLOR_PROFILE"

    move-object/from16 v18, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_OUTPUT_INTENT_COLOR_PROFILE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 35
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v2, "OUTPUT_INTENTS_WITH_NON_MATCHING_DESTINATION_OUTPUT_PROFILES"

    move-object/from16 v19, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->OUTPUT_INTENTS_WITH_NON_MATCHING_DESTINATION_OUTPUT_PROFILES:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 37
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "OVERPRINT_WITH_ICCBASED_CMYK"

    move-object/from16 v20, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->OVERPRINT_WITH_ICCBASED_CMYK:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 39
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v4, "DEVICE_RGB_WITH_NO_RGB_OUTPUT_INTENT"

    move-object/from16 v21, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->DEVICE_RGB_WITH_NO_RGB_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 41
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v2, "DEVICE_CMYK_WITH_NO_CMYK_OUTPUT_INTENT"

    move-object/from16 v22, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->DEVICE_CMYK_WITH_NO_CMYK_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 43
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "DEVICE_GRAY_WITH_NO_ICC_OUTPUT_INTENT"

    move-object/from16 v23, v4

    const/16 v4, 0x15

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->DEVICE_GRAY_WITH_NO_ICC_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 45
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v4, "SPOT_COLOR_IN_DEVICE_N_BUT_NO_COLORANT"

    move-object/from16 v24, v2

    const/16 v2, 0x16

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->SPOT_COLOR_IN_DEVICE_N_BUT_NO_COLORANT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 47
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v4, "SEPARATION_WITH_DIFFERENT_TINT_TRANSFORM_OR_ALTERNATE_SPACE"

    move-object/from16 v25, v6

    const/16 v6, 0x17

    invoke-direct {v2, v4, v6}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->SEPARATION_WITH_DIFFERENT_TINT_TRANSFORM_OR_ALTERNATE_SPACE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 49
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "EXTGSTATE_SHALL_NOT_CONTAIN_NON_DEFAULT_TR2"

    move-object/from16 v26, v2

    const/16 v2, 0x18

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->EXTGSTATE_SHALL_NOT_CONTAIN_NON_DEFAULT_TR2:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 51
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "EXTGSTATE_SHALL_NOT_CONTAIN_HTP"

    move-object/from16 v27, v4

    const/16 v4, 0x19

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->EXTGSTATE_SHALL_NOT_CONTAIN_HTP:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 53
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "HALFTONE_TYPE_MUST_BE_1_OR_5"

    move-object/from16 v28, v2

    const/16 v2, 0x1a

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->HALFTONE_TYPE_MUST_BE_1_OR_5:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 55
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "HALFTONE_MUST_NOT_CONTAIN_HALFTONE_NAME"

    move-object/from16 v29, v4

    const/16 v4, 0x1b

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->HALFTONE_MUST_NOT_CONTAIN_HALFTONE_NAME:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 57
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INVALID_TRANSFER_FUNCTION"

    move-object/from16 v30, v2

    const/16 v2, 0x1c

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_TRANSFER_FUNCTION:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 59
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INVALID_RENDERING_INTENT"

    move-object/from16 v31, v4

    const/16 v4, 0x1d

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_RENDERING_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 61
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "FONT_NOT_EMBEDDED"

    move-object/from16 v32, v2

    const/16 v2, 0x1e

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->FONT_NOT_EMBEDDED:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 63
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INVALID_IMAGE_PROPERTY"

    move-object/from16 v33, v4

    const/16 v4, 0x1f

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_IMAGE_PROPERTY:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 65
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INVALID_IMAGE_INTENT"

    move-object/from16 v34, v2

    const/16 v2, 0x20

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_IMAGE_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 67
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INVALID_FORM_X_OBJECT"

    move-object/from16 v35, v4

    const/16 v4, 0x21

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_FORM_X_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 69
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INVALID_BLEND_MODE_IN_EXT_GSTATE"

    move-object/from16 v36, v2

    const/16 v2, 0x22

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_BLEND_MODE_IN_EXT_GSTATE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 71
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "PAGE_TRANSPARENCY_BUT_NO_OUTPUT_INTENT"

    move-object/from16 v37, v4

    const/16 v4, 0x23

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->PAGE_TRANSPARENCY_BUT_NO_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 73
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "NOT_PERMITTED_ANNOTATION_TYPE"

    move-object/from16 v38, v2

    const/16 v2, 0x24

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->NOT_PERMITTED_ANNOTATION_TYPE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 75
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "NOT_PERMITTED_ANNOTATION_FLAGS"

    move-object/from16 v39, v4

    const/16 v4, 0x25

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->NOT_PERMITTED_ANNOTATION_FLAGS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 77
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INVALID_APPEARANCE_STREAM"

    move-object/from16 v40, v2

    const/16 v2, 0x26

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_APPEARANCE_STREAM:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 79
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "FIELD_HAS_ACTIONS"

    move-object/from16 v41, v4

    const/16 v4, 0x27

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->FIELD_HAS_ACTIONS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 81
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "ACROFORM_HAS_NEED_APPEARANCES"

    move-object/from16 v42, v2

    const/16 v2, 0x28

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->ACROFORM_HAS_NEED_APPEARANCES:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 83
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "DOCUMENT_HAS_XFA_FORMS"

    move-object/from16 v43, v4

    const/16 v4, 0x29

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->DOCUMENT_HAS_XFA_FORMS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 85
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "DOCUMENT_CATALOG_HAS_NEEDS_RENDERING"

    move-object/from16 v44, v2

    const/16 v2, 0x2a

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->DOCUMENT_CATALOG_HAS_NEEDS_RENDERING:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 87
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INVALID_DIGITAL_SIGNATURE"

    move-object/from16 v45, v4

    const/16 v4, 0x2b

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_DIGITAL_SIGNATURE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 89
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INVALID_ANNOTATION_ACTIONS"

    move-object/from16 v46, v2

    const/16 v2, 0x2c

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_ANNOTATION_ACTIONS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 91
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "ADDITIONAL_ACTIONS_IN_CATALOG_OR_PAGE"

    move-object/from16 v47, v4

    const/16 v4, 0x2d

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->ADDITIONAL_ACTIONS_IN_CATALOG_OR_PAGE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 93
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "MISSING_DOCUMENT_METADATA"

    move-object/from16 v48, v2

    const/16 v2, 0x2e

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->MISSING_DOCUMENT_METADATA:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 95
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INVALID_DOCUMENT_METADATA"

    move-object/from16 v49, v4

    const/16 v4, 0x2f

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_DOCUMENT_METADATA:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 97
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INVALID_EMBEDDED_FILE"

    move-object/from16 v50, v2

    const/16 v2, 0x30

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_EMBEDDED_FILE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 99
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INVALID_FILE_SPECIFICATION"

    move-object/from16 v51, v4

    const/16 v4, 0x31

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_FILE_SPECIFICATION:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 101
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "INVALID_OPTIONAL_CONTENT"

    move-object/from16 v52, v2

    const/16 v2, 0x32

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->INVALID_OPTIONAL_CONTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 103
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "ALTERNATE_PRESENTATIONS_IN_DOCUMENT"

    move-object/from16 v53, v4

    const/16 v4, 0x33

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->ALTERNATE_PRESENTATIONS_IN_DOCUMENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 105
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const-string v6, "REQUIREMENTS_IN_DOCUMENT"

    move-object/from16 v54, v2

    const/16 v2, 0x34

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->REQUIREMENTS_IN_DOCUMENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const/16 v2, 0x35

    new-array v2, v2, [Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    const/4 v6, 0x0

    aput-object v0, v2, v6

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object v5, v2, v0

    const/4 v0, 0x4

    aput-object v7, v2, v0

    const/4 v0, 0x5

    aput-object v9, v2, v0

    const/4 v0, 0x6

    aput-object v11, v2, v0

    const/4 v0, 0x7

    aput-object v13, v2, v0

    const/16 v0, 0x8

    aput-object v15, v2, v0

    const/16 v0, 0x9

    aput-object v14, v2, v0

    const/16 v0, 0xa

    aput-object v12, v2, v0

    const/16 v0, 0xb

    aput-object v10, v2, v0

    const/16 v0, 0xc

    aput-object v8, v2, v0

    const/16 v0, 0xd

    aput-object v16, v2, v0

    const/16 v0, 0xe

    aput-object v17, v2, v0

    const/16 v0, 0xf

    aput-object v18, v2, v0

    const/16 v0, 0x10

    aput-object v19, v2, v0

    const/16 v0, 0x11

    aput-object v20, v2, v0

    const/16 v0, 0x12

    aput-object v21, v2, v0

    const/16 v0, 0x13

    aput-object v22, v2, v0

    const/16 v0, 0x14

    aput-object v23, v2, v0

    const/16 v0, 0x15

    aput-object v24, v2, v0

    const/16 v0, 0x16

    aput-object v25, v2, v0

    const/16 v0, 0x17

    aput-object v26, v2, v0

    const/16 v0, 0x18

    aput-object v27, v2, v0

    const/16 v0, 0x19

    aput-object v28, v2, v0

    const/16 v0, 0x1a

    aput-object v29, v2, v0

    const/16 v0, 0x1b

    aput-object v30, v2, v0

    const/16 v0, 0x1c

    aput-object v31, v2, v0

    const/16 v0, 0x1d

    aput-object v32, v2, v0

    const/16 v0, 0x1e

    aput-object v33, v2, v0

    const/16 v0, 0x1f

    aput-object v34, v2, v0

    const/16 v0, 0x20

    aput-object v35, v2, v0

    const/16 v0, 0x21

    aput-object v36, v2, v0

    const/16 v0, 0x22

    aput-object v37, v2, v0

    const/16 v0, 0x23

    aput-object v38, v2, v0

    const/16 v0, 0x24

    aput-object v39, v2, v0

    const/16 v0, 0x25

    aput-object v40, v2, v0

    const/16 v0, 0x26

    aput-object v41, v2, v0

    const/16 v0, 0x27

    aput-object v42, v2, v0

    const/16 v0, 0x28

    aput-object v43, v2, v0

    const/16 v0, 0x29

    aput-object v44, v2, v0

    const/16 v0, 0x2a

    aput-object v45, v2, v0

    const/16 v0, 0x2b

    aput-object v46, v2, v0

    const/16 v0, 0x2c

    aput-object v47, v2, v0

    const/16 v0, 0x2d

    aput-object v48, v2, v0

    const/16 v0, 0x2e

    aput-object v49, v2, v0

    const/16 v0, 0x2f

    aput-object v50, v2, v0

    const/16 v0, 0x30

    aput-object v51, v2, v0

    const/16 v0, 0x31

    aput-object v52, v2, v0

    const/16 v0, 0x32

    aput-object v53, v2, v0

    const/16 v0, 0x33

    aput-object v54, v2, v0

    const/16 v0, 0x34

    aput-object v4, v2, v0

    .line 106
    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->$VALUES:[Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->$VALUES:[Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    return-object v0
.end method

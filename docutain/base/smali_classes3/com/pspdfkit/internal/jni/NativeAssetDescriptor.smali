.class public final enum Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

.field public static final enum NOTE_ICON_CHECK:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

.field public static final enum NOTE_ICON_CIRCLE:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

.field public static final enum NOTE_ICON_COMMENT:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

.field public static final enum NOTE_ICON_CROSS:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

.field public static final enum NOTE_ICON_HELP:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

.field public static final enum NOTE_ICON_INSERT:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

.field public static final enum NOTE_ICON_NEW_PARAGRAPH:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

.field public static final enum NOTE_ICON_NOTE:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

.field public static final enum NOTE_ICON_PARAGRAPH:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

.field public static final enum NOTE_ICON_RIGHT_ARROW:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

.field public static final enum NOTE_ICON_STAR:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

.field public static final enum PSPDFKIT_LOGO:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    const-string v1, "PSPDFKIT_LOGO"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->PSPDFKIT_LOGO:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    const-string v3, "NOTE_ICON_COMMENT"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->NOTE_ICON_COMMENT:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    .line 3
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    const-string v5, "NOTE_ICON_RIGHT_ARROW"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->NOTE_ICON_RIGHT_ARROW:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    .line 4
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    const-string v7, "NOTE_ICON_CHECK"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->NOTE_ICON_CHECK:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    .line 5
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    const-string v9, "NOTE_ICON_CIRCLE"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->NOTE_ICON_CIRCLE:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    .line 6
    new-instance v9, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    const-string v11, "NOTE_ICON_CROSS"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->NOTE_ICON_CROSS:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    .line 7
    new-instance v11, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    const-string v13, "NOTE_ICON_INSERT"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->NOTE_ICON_INSERT:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    .line 8
    new-instance v13, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    const-string v15, "NOTE_ICON_NEW_PARAGRAPH"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->NOTE_ICON_NEW_PARAGRAPH:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    .line 9
    new-instance v15, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    const-string v14, "NOTE_ICON_NOTE"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->NOTE_ICON_NOTE:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    .line 10
    new-instance v14, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    const-string v12, "NOTE_ICON_PARAGRAPH"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->NOTE_ICON_PARAGRAPH:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    .line 11
    new-instance v12, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    const-string v10, "NOTE_ICON_HELP"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->NOTE_ICON_HELP:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    .line 12
    new-instance v10, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    const-string v8, "NOTE_ICON_STAR"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->NOTE_ICON_STAR:Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    const/16 v8, 0xc

    new-array v8, v8, [Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    aput-object v0, v8, v2

    aput-object v1, v8, v4

    const/4 v0, 0x2

    aput-object v3, v8, v0

    const/4 v0, 0x3

    aput-object v5, v8, v0

    const/4 v0, 0x4

    aput-object v7, v8, v0

    const/4 v0, 0x5

    aput-object v9, v8, v0

    const/4 v0, 0x6

    aput-object v11, v8, v0

    const/4 v0, 0x7

    aput-object v13, v8, v0

    const/16 v0, 0x8

    aput-object v15, v8, v0

    const/16 v0, 0x9

    aput-object v14, v8, v0

    const/16 v0, 0xa

    aput-object v12, v8, v0

    aput-object v10, v8, v6

    .line 13
    sput-object v8, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeAssetDescriptor;

    return-object v0
.end method

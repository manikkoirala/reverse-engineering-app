.class public final Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/jni/PSPDFKitNative;->initialize(Landroid/content/Context;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\'\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0008\u0010\u0006\u001a\u00020\u0003H\u0016J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u000cH\u0002\u00a8\u0006\r"
    }
    d2 = {
        "com/pspdfkit/internal/jni/PSPDFKitNative$initialize$1",
        "Landroid/content/ComponentCallbacks2;",
        "onConfigurationChanged",
        "",
        "configuration",
        "Landroid/content/res/Configuration;",
        "onLowMemory",
        "onTrimMemory",
        "level",
        "",
        "trimCoreMemoryAsync",
        "coreMemoryNotificationLevel",
        "Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
    xi = 0x30
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1;->$context:Landroid/content/Context;

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final trimCoreMemoryAsync(Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;)V
    .locals 7

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getIO()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-static {v0}, Lkotlinx/coroutines/CoroutineScopeKt;->CoroutineScope(Lkotlin/coroutines/CoroutineContext;)Lkotlinx/coroutines/CoroutineScope;

    move-result-object v1

    new-instance v4, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;

    iget-object v0, p0, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1;->$context:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v4, p1, v0, v2}, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;-><init>(Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;Landroid/content/Context;Lkotlin/coroutines/Continuation;)V

    const/4 v3, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->launch$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onLowMemory()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1;->$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/pspdfkit/internal/cj;->a(Landroid/content/Context;)Landroid/app/ActivityManager$MemoryInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/cj;->a(Landroid/app/ActivityManager$MemoryInfo;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "`onLowMemory` signal received. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Internal.PSDPFKitNative"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;->CRITICAL:Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1;->trimCoreMemoryAsync(Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;)V

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 4

    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    const/16 v0, 0xf

    if-eq p1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;->WARNING:Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;

    goto :goto_0

    .line 2
    :cond_0
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;->CRITICAL:Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;

    .line 6
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1;->$context:Landroid/content/Context;

    invoke-static {v1}, Lcom/pspdfkit/internal/cj;->a(Landroid/content/Context;)Landroid/app/ActivityManager$MemoryInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/cj;->a(Landroid/app/ActivityManager$MemoryInfo;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "`onTrimMemory` signal "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " received. "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Internal.PSDPFKitNative"

    invoke-static {v2, p1, v1}, Lcom/pspdfkit/utils/PdfLog;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1;->trimCoreMemoryAsync(Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/jni/NativePDFStandardRuleValidationStatus;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final mErrorDescription:Ljava/lang/String;

.field final mPdfObjects:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final mStatusCode:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleValidationStatus;->mStatusCode:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleValidationStatus;->mErrorDescription:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleValidationStatus;->mPdfObjects:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getErrorDescription()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleValidationStatus;->mErrorDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getPdfObjects()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleValidationStatus;->mPdfObjects:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getStatusCode()Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleValidationStatus;->mStatusCode:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NativePDFStandardRuleValidationStatus{mStatusCode="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleValidationStatus;->mStatusCode:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleStatusCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ",mErrorDescription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleValidationStatus;->mErrorDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",mPdfObjects="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleValidationStatus;->mPdfObjects:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final enum Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum ACRO_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum ANNOTATION_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum ANNOTATION_REPLIES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum COMPARISON:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum CONTENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum DIGITAL_SIGNATURES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum EDITABLE_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum ELECTRONIC_SIGNATURES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum IMAGE_DOCUMENT:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum INDEXED_FTS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum MEASUREMENT_TOOLS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum OCR:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum PDFA:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum PDF_CREATION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum PDF_VIEWER:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum READER_VIEW:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum REDACTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum REQUIRE_SIGNED_SOURCE:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum STRONG_ENCRYPTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum TEXT_SELECTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum UI:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

.field public static final enum WEBKIT_HTML_CONVERSION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;


# direct methods
.method static constructor <clinit>()V
    .locals 25

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v1, "PDF_VIEWER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->PDF_VIEWER:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v3, "TEXT_SELECTION"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->TEXT_SELECTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 8
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v5, "STRONG_ENCRYPTION"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->STRONG_ENCRYPTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 10
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v7, "PDF_CREATION"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->PDF_CREATION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 12
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v9, "ANNOTATION_EDITING"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 14
    new-instance v9, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v11, "ACRO_FORMS"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ACRO_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 16
    new-instance v11, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v13, "INDEXED_FTS"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->INDEXED_FTS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 18
    new-instance v13, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v15, "DIGITAL_SIGNATURES"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DIGITAL_SIGNATURES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 20
    new-instance v15, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v14, "REQUIRE_SIGNED_SOURCE"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->REQUIRE_SIGNED_SOURCE:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 22
    new-instance v14, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v12, "DOCUMENT_EDITING"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 24
    new-instance v12, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v10, "UI"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->UI:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 26
    new-instance v10, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v8, "ANNOTATION_REPLIES"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_REPLIES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 28
    new-instance v8, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v6, "IMAGE_DOCUMENT"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->IMAGE_DOCUMENT:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 30
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v4, "REDACTION"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->REDACTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 32
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v2, "COMPARISON"

    move-object/from16 v16, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->COMPARISON:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 34
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v6, "EDITABLE_FORMS"

    move-object/from16 v17, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->EDITABLE_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 36
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v4, "WEBKIT_HTML_CONVERSION"

    move-object/from16 v18, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->WEBKIT_HTML_CONVERSION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 38
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v2, "READER_VIEW"

    move-object/from16 v19, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->READER_VIEW:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 40
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v6, "OCR"

    move-object/from16 v20, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->OCR:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 42
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v4, "ELECTRONIC_SIGNATURES"

    move-object/from16 v21, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ELECTRONIC_SIGNATURES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 44
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v2, "PDFA"

    move-object/from16 v22, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->PDFA:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 46
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v6, "MEASUREMENT_TOOLS"

    move-object/from16 v23, v4

    const/16 v4, 0x15

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->MEASUREMENT_TOOLS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 48
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const-string v4, "CONTENT_EDITING"

    move-object/from16 v24, v2

    const/16 v2, 0x16

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->CONTENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const/16 v2, 0x17

    new-array v2, v2, [Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    const/4 v4, 0x0

    aput-object v0, v2, v4

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object v5, v2, v0

    const/4 v0, 0x4

    aput-object v7, v2, v0

    const/4 v0, 0x5

    aput-object v9, v2, v0

    const/4 v0, 0x6

    aput-object v11, v2, v0

    const/4 v0, 0x7

    aput-object v13, v2, v0

    const/16 v0, 0x8

    aput-object v15, v2, v0

    const/16 v0, 0x9

    aput-object v14, v2, v0

    const/16 v0, 0xa

    aput-object v12, v2, v0

    const/16 v0, 0xb

    aput-object v10, v2, v0

    const/16 v0, 0xc

    aput-object v8, v2, v0

    const/16 v0, 0xd

    aput-object v16, v2, v0

    const/16 v0, 0xe

    aput-object v17, v2, v0

    const/16 v0, 0xf

    aput-object v18, v2, v0

    const/16 v0, 0x10

    aput-object v19, v2, v0

    const/16 v0, 0x11

    aput-object v20, v2, v0

    const/16 v0, 0x12

    aput-object v21, v2, v0

    const/16 v0, 0x13

    aput-object v22, v2, v0

    const/16 v0, 0x14

    aput-object v23, v2, v0

    const/16 v0, 0x15

    aput-object v24, v2, v0

    const/16 v0, 0x16

    aput-object v6, v2, v0

    .line 49
    sput-object v2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    return-object v0
.end method

.class public final enum Lcom/pspdfkit/internal/jni/NativeOcrLanguage;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeOcrLanguage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum CROATIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum CZECH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum DANISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum DUTCH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum ENGLISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum FINNISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum FRENCH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum GERMAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum INDONESIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum ITALIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum MALAY:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum NORWEGIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum POLISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum PORTUGUESE:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum SERBIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum SLOVAK:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum SLOVENIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum SPANISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum SWEDISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum TURKISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

.field public static final enum WELSH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;


# direct methods
.method static constructor <clinit>()V
    .locals 24

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v1, "CROATIAN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->CROATIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v3, "CZECH"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->CZECH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 3
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v5, "DANISH"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->DANISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 4
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v7, "DUTCH"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->DUTCH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 5
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v9, "ENGLISH"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->ENGLISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 6
    new-instance v9, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v11, "FINNISH"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->FINNISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 7
    new-instance v11, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v13, "FRENCH"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->FRENCH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 8
    new-instance v13, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v15, "GERMAN"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->GERMAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 9
    new-instance v15, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v14, "INDONESIAN"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->INDONESIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 10
    new-instance v14, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v12, "ITALIAN"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->ITALIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 11
    new-instance v12, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v10, "MALAY"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->MALAY:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 12
    new-instance v10, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v8, "NORWEGIAN"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->NORWEGIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 13
    new-instance v8, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v6, "POLISH"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->POLISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 14
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v4, "PORTUGUESE"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->PORTUGUESE:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 15
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v2, "SERBIAN"

    move-object/from16 v17, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->SERBIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 16
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v6, "SLOVAK"

    move-object/from16 v18, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->SLOVAK:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 17
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v4, "SLOVENIAN"

    move-object/from16 v19, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->SLOVENIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 18
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v2, "SPANISH"

    move-object/from16 v20, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->SPANISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 19
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v6, "SWEDISH"

    move-object/from16 v21, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->SWEDISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 20
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v4, "TURKISH"

    move-object/from16 v22, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->TURKISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    .line 21
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const-string v2, "WELSH"

    move-object/from16 v23, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->WELSH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const/16 v2, 0x15

    new-array v2, v2, [Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    const/16 v16, 0x0

    aput-object v0, v2, v16

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object v5, v2, v0

    const/4 v0, 0x4

    aput-object v7, v2, v0

    const/4 v0, 0x5

    aput-object v9, v2, v0

    const/4 v0, 0x6

    aput-object v11, v2, v0

    const/4 v0, 0x7

    aput-object v13, v2, v0

    const/16 v0, 0x8

    aput-object v15, v2, v0

    const/16 v0, 0x9

    aput-object v14, v2, v0

    const/16 v0, 0xa

    aput-object v12, v2, v0

    const/16 v0, 0xb

    aput-object v10, v2, v0

    const/16 v0, 0xc

    aput-object v8, v2, v0

    const/16 v0, 0xd

    aput-object v17, v2, v0

    const/16 v0, 0xe

    aput-object v18, v2, v0

    const/16 v0, 0xf

    aput-object v19, v2, v0

    const/16 v0, 0x10

    aput-object v20, v2, v0

    const/16 v0, 0x11

    aput-object v21, v2, v0

    const/16 v0, 0x12

    aput-object v22, v2, v0

    const/16 v0, 0x13

    aput-object v23, v2, v0

    aput-object v4, v2, v6

    .line 22
    sput-object v2, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeOcrLanguage;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeOcrLanguage;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object v0
.end method

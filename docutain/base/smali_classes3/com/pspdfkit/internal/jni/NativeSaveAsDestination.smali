.class public final Lcom/pspdfkit/internal/jni/NativeSaveAsDestination;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final mDataProvider:Lcom/pspdfkit/internal/jni/NativeDataProvider;

.field final mDeprecatedDataSink:Lcom/pspdfkit/internal/jni/NativeDataSink;

.field final mFilePath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeDataProvider;Lcom/pspdfkit/internal/jni/NativeDataSink;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/jni/NativeSaveAsDestination;->mFilePath:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/jni/NativeSaveAsDestination;->mDataProvider:Lcom/pspdfkit/internal/jni/NativeDataProvider;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/jni/NativeSaveAsDestination;->mDeprecatedDataSink:Lcom/pspdfkit/internal/jni/NativeDataSink;

    return-void
.end method


# virtual methods
.method public getDataProvider()Lcom/pspdfkit/internal/jni/NativeDataProvider;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativeSaveAsDestination;->mDataProvider:Lcom/pspdfkit/internal/jni/NativeDataProvider;

    return-object v0
.end method

.method public getDeprecatedDataSink()Lcom/pspdfkit/internal/jni/NativeDataSink;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativeSaveAsDestination;->mDeprecatedDataSink:Lcom/pspdfkit/internal/jni/NativeDataSink;

    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativeSaveAsDestination;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NativeSaveAsDestination{mFilePath="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativeSaveAsDestination;->mFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",mDataProvider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativeSaveAsDestination;->mDataProvider:Lcom/pspdfkit/internal/jni/NativeDataProvider;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ",mDeprecatedDataSink="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativeSaveAsDestination;->mDeprecatedDataSink:Lcom/pspdfkit/internal/jni/NativeDataSink;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

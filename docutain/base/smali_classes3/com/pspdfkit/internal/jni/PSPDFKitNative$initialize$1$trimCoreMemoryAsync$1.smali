.class final Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1;->trimCoreMemoryAsync(Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\u0010\u0002\u001a\u00020\u0001*\u00020\u0000H\u008a@"
    }
    d2 = {
        "Lkotlinx/coroutines/CoroutineScope;",
        "",
        "<anonymous>"
    }
    k = 0x3
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.pspdfkit.internal.jni.PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1"
    f = "PSPDFKitNative.kt"
    i = {}
    l = {}
    m = "invokeSuspend"
    n = {}
    s = {}
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;

.field final synthetic $coreMemoryNotificationLevel:Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;

.field label:I


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;Landroid/content/Context;Lkotlin/coroutines/Continuation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;",
            "Landroid/content/Context;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;->$coreMemoryNotificationLevel:Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;

    iput-object p2, p0, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;->$context:Landroid/content/Context;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    new-instance p1, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;

    iget-object v0, p0, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;->$coreMemoryNotificationLevel:Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;->$context:Landroid/content/Context;

    invoke-direct {p1, v0, v1, p2}, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;-><init>(Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;Landroid/content/Context;Lkotlin/coroutines/Continuation;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;->invoke(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;->label:I

    if-nez v0, :cond_0

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;->$coreMemoryNotificationLevel:Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;

    invoke-static {p1}, Lcom/pspdfkit/internal/jni/NativeNativeServices;->memoryNotification(Lcom/pspdfkit/internal/jni/NativeMemoryNotificationLevel;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/jni/PSPDFKitNative$initialize$1$trimCoreMemoryAsync$1;->$context:Landroid/content/Context;

    invoke-static {p1}, Lcom/pspdfkit/internal/cj;->a(Landroid/content/Context;)Landroid/app/ActivityManager$MemoryInfo;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/cj;->a(Landroid/app/ActivityManager$MemoryInfo;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Core memory trimmed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Internal.PSDPFKitNative"

    invoke-static {v1, p1, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1

    .line 5
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

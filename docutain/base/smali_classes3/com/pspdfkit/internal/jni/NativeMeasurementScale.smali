.class public final Lcom/pspdfkit/internal/jni/NativeMeasurementScale;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final mFrom:D

.field final mTo:D

.field final mUnitFrom:Lcom/pspdfkit/internal/jni/NativeUnitFrom;

.field final mUnitTo:Lcom/pspdfkit/internal/jni/NativeUnitTo;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/jni/NativeUnitFrom;Lcom/pspdfkit/internal/jni/NativeUnitTo;DD)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->mUnitFrom:Lcom/pspdfkit/internal/jni/NativeUnitFrom;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->mUnitTo:Lcom/pspdfkit/internal/jni/NativeUnitTo;

    .line 4
    iput-wide p3, p0, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->mFrom:D

    .line 5
    iput-wide p5, p0, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->mTo:D

    return-void
.end method


# virtual methods
.method public getFrom()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->mFrom:D

    return-wide v0
.end method

.method public getTo()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->mTo:D

    return-wide v0
.end method

.method public getUnitFrom()Lcom/pspdfkit/internal/jni/NativeUnitFrom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->mUnitFrom:Lcom/pspdfkit/internal/jni/NativeUnitFrom;

    return-object v0
.end method

.method public getUnitTo()Lcom/pspdfkit/internal/jni/NativeUnitTo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->mUnitTo:Lcom/pspdfkit/internal/jni/NativeUnitTo;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NativeMeasurementScale{mUnitFrom="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->mUnitFrom:Lcom/pspdfkit/internal/jni/NativeUnitFrom;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ",mUnitTo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->mUnitTo:Lcom/pspdfkit/internal/jni/NativeUnitTo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ",mFrom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->mFrom:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ",mTo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->mTo:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

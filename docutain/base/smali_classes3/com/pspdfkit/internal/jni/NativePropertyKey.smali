.class public final enum Lcom/pspdfkit/internal/jni/NativePropertyKey;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativePropertyKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum ACTION:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum ADDITIONAL_ACTIONS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum ALPHA:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum ANNOTATION_ID:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum ANNOTATION_TYPE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum ASSET_EXTERNAL_URL:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum ASSET_NAME:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum AUTHOR_STATE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum AUTHOR_STATE_MODEL:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum BLEND_MODE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum BORDER_COLOR:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum BORDER_EFFECT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum BORDER_EFFECT_INTENSITY:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum BORDER_STYLE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum BOUNDING_BOX:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum COLOR:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum CONTENTS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum CONTENT_SIZE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum CREATION_DATE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum CREATOR:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum CUSTOM_DATA:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum DASH_ARRAY:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum FILL_COLOR:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum FIRST_COMMENT_CUSTOM_DATA:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum FIRST_COMMENT_WEB_ID:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum FLAGS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum FONT_NAME:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum FONT_SIZE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum FONT_STROKE_COLOR:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum FONT_STYLE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum FORM_APPEARANCE_STATE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum FREE_TEXT_INTENT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum GROUP:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum HORIZONTAL_CORNER_RADIUS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum ICON_NAME:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum IN_REPLY_TO:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum IN_REPLY_TO_UUID:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum IS_INSTANT_COMMENT_THREAD_ROOT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum IS_NATURAL_DRAWING:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum IS_SIGNATURE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum LAST_MODIFIED_DATE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum LINES:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum LINE_ENDS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum LINE_INTENT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum LINE_WIDTH:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum MEASUREMENT_PRECISION:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum MEASUREMENT_SCALE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum MEDIA_OPTIONS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum MEDIA_WINDOW_TYPE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum NAME:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum NOTE_IS_OPEN:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum OUTLINE_COLOR:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum OVERLAY_TEXT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum PAGE_INDEX:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum POINTS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum POLYGON_INTENT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum POLYLINE_INTENT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum QUADRILATERALS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum REPEAT_OVERLAY_TEXT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum RICH_TEXT_CONTENTS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum ROTATION:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum SOUND_BITS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum SOUND_CHANNELS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum SOUND_ENCODING:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum SOUND_RATE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum STAMPTITLE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum SUBJECT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum SUB_TEXT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum TEXT_DECORATION:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum TEXT_EDGE_INSETS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum TEXT_JUSTIFICATION:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum TEXT_VERTICAL_ALIGNMENT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum TYPE_STRING:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum UUID:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum VARIANT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum VERTICAL_CORNER_RADIUS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

.field public static final enum WEB_ID:Lcom/pspdfkit/internal/jni/NativePropertyKey;


# direct methods
.method static constructor <clinit>()V
    .locals 79

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v1, "ANNOTATION_TYPE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativePropertyKey;->ANNOTATION_TYPE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v3, "TYPE_STRING"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativePropertyKey;->TYPE_STRING:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 5
    new-instance v3, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v5, "ANNOTATION_ID"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativePropertyKey;->ANNOTATION_ID:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 7
    new-instance v5, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v7, "PAGE_INDEX"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativePropertyKey;->PAGE_INDEX:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 9
    new-instance v7, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v9, "QUADRILATERALS"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativePropertyKey;->QUADRILATERALS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 11
    new-instance v9, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v11, "BOUNDING_BOX"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativePropertyKey;->BOUNDING_BOX:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 13
    new-instance v11, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v13, "CONTENT_SIZE"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativePropertyKey;->CONTENT_SIZE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 15
    new-instance v13, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v15, "LINE_WIDTH"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativePropertyKey;->LINE_WIDTH:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 17
    new-instance v15, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v14, "BORDER_STYLE"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativePropertyKey;->BORDER_STYLE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 19
    new-instance v14, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v12, "BORDER_EFFECT"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativePropertyKey;->BORDER_EFFECT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 21
    new-instance v12, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v10, "BORDER_EFFECT_INTENSITY"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/internal/jni/NativePropertyKey;->BORDER_EFFECT_INTENSITY:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 23
    new-instance v10, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v8, "DASH_ARRAY"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/internal/jni/NativePropertyKey;->DASH_ARRAY:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 25
    new-instance v8, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "COLOR"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/internal/jni/NativePropertyKey;->COLOR:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 27
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v4, "FILL_COLOR"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativePropertyKey;->FILL_COLOR:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 29
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v2, "ALPHA"

    move-object/from16 v16, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->ALPHA:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 31
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "NAME"

    move-object/from16 v17, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->NAME:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 33
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v4, "CREATOR"

    move-object/from16 v18, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativePropertyKey;->CREATOR:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 35
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v2, "SUBJECT"

    move-object/from16 v19, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->SUBJECT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 37
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "STAMPTITLE"

    move-object/from16 v20, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->STAMPTITLE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 39
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v4, "SUB_TEXT"

    move-object/from16 v21, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativePropertyKey;->SUB_TEXT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 41
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v2, "CREATION_DATE"

    move-object/from16 v22, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->CREATION_DATE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 43
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "LAST_MODIFIED_DATE"

    move-object/from16 v23, v4

    const/16 v4, 0x15

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->LAST_MODIFIED_DATE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 45
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v4, "CONTENTS"

    move-object/from16 v24, v2

    const/16 v2, 0x16

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativePropertyKey;->CONTENTS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 47
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v4, "RICH_TEXT_CONTENTS"

    move-object/from16 v25, v6

    const/16 v6, 0x17

    invoke-direct {v2, v4, v6}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->RICH_TEXT_CONTENTS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 49
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "FONT_NAME"

    move-object/from16 v26, v2

    const/16 v2, 0x18

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->FONT_NAME:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 51
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "FONT_SIZE"

    move-object/from16 v27, v4

    const/16 v4, 0x19

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->FONT_SIZE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 53
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "FONT_STROKE_COLOR"

    move-object/from16 v28, v2

    const/16 v2, 0x1a

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->FONT_STROKE_COLOR:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 55
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "FONT_STYLE"

    move-object/from16 v29, v4

    const/16 v4, 0x1b

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->FONT_STYLE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 57
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "TEXT_DECORATION"

    move-object/from16 v30, v2

    const/16 v2, 0x1c

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->TEXT_DECORATION:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 59
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "TEXT_JUSTIFICATION"

    move-object/from16 v31, v4

    const/16 v4, 0x1d

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->TEXT_JUSTIFICATION:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 61
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "TEXT_VERTICAL_ALIGNMENT"

    move-object/from16 v32, v2

    const/16 v2, 0x1e

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->TEXT_VERTICAL_ALIGNMENT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 63
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "TEXT_EDGE_INSETS"

    move-object/from16 v33, v4

    const/16 v4, 0x1f

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->TEXT_EDGE_INSETS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 65
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "BORDER_COLOR"

    move-object/from16 v34, v2

    const/16 v2, 0x20

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->BORDER_COLOR:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 67
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "POINTS"

    move-object/from16 v35, v4

    const/16 v4, 0x21

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->POINTS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 69
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "LINES"

    move-object/from16 v36, v2

    const/16 v2, 0x22

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->LINES:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 71
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "VERTICAL_CORNER_RADIUS"

    move-object/from16 v37, v4

    const/16 v4, 0x23

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->VERTICAL_CORNER_RADIUS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 73
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "HORIZONTAL_CORNER_RADIUS"

    move-object/from16 v38, v2

    const/16 v2, 0x24

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->HORIZONTAL_CORNER_RADIUS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 75
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "ICON_NAME"

    move-object/from16 v39, v4

    const/16 v4, 0x25

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->ICON_NAME:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 77
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "NOTE_IS_OPEN"

    move-object/from16 v40, v2

    const/16 v2, 0x26

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->NOTE_IS_OPEN:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 79
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "IS_SIGNATURE"

    move-object/from16 v41, v4

    const/16 v4, 0x27

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->IS_SIGNATURE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 81
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "IS_NATURAL_DRAWING"

    move-object/from16 v42, v2

    const/16 v2, 0x28

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->IS_NATURAL_DRAWING:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 83
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "ACTION"

    move-object/from16 v43, v4

    const/16 v4, 0x29

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->ACTION:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 85
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "ADDITIONAL_ACTIONS"

    move-object/from16 v44, v2

    const/16 v2, 0x2a

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->ADDITIONAL_ACTIONS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 87
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "FREE_TEXT_INTENT"

    move-object/from16 v45, v4

    const/16 v4, 0x2b

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->FREE_TEXT_INTENT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 89
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "ROTATION"

    move-object/from16 v46, v2

    const/16 v2, 0x2c

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->ROTATION:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 91
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "GROUP"

    move-object/from16 v47, v4

    const/16 v4, 0x2d

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->GROUP:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 93
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "VARIANT"

    move-object/from16 v48, v2

    const/16 v2, 0x2e

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->VARIANT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 95
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "FLAGS"

    move-object/from16 v49, v4

    const/16 v4, 0x2f

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->FLAGS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 97
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "LINE_ENDS"

    move-object/from16 v50, v2

    const/16 v2, 0x30

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->LINE_ENDS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 99
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "LINE_INTENT"

    move-object/from16 v51, v4

    const/16 v4, 0x31

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->LINE_INTENT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 101
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "POLYLINE_INTENT"

    move-object/from16 v52, v2

    const/16 v2, 0x32

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->POLYLINE_INTENT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 103
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "POLYGON_INTENT"

    move-object/from16 v53, v4

    const/16 v4, 0x33

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->POLYGON_INTENT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 105
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "SOUND_RATE"

    move-object/from16 v54, v2

    const/16 v2, 0x34

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->SOUND_RATE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 107
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "SOUND_CHANNELS"

    move-object/from16 v55, v4

    const/16 v4, 0x35

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->SOUND_CHANNELS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 109
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "SOUND_BITS"

    move-object/from16 v56, v2

    const/16 v2, 0x36

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->SOUND_BITS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 111
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "SOUND_ENCODING"

    move-object/from16 v57, v4

    const/16 v4, 0x37

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->SOUND_ENCODING:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 113
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "ASSET_NAME"

    move-object/from16 v58, v2

    const/16 v2, 0x38

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->ASSET_NAME:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 115
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "ASSET_EXTERNAL_URL"

    move-object/from16 v59, v4

    const/16 v4, 0x39

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->ASSET_EXTERNAL_URL:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 117
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "MEDIA_WINDOW_TYPE"

    move-object/from16 v60, v2

    const/16 v2, 0x3a

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->MEDIA_WINDOW_TYPE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 119
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "MEDIA_OPTIONS"

    move-object/from16 v61, v4

    const/16 v4, 0x3b

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->MEDIA_OPTIONS:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 121
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "FORM_APPEARANCE_STATE"

    move-object/from16 v62, v2

    const/16 v2, 0x3c

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->FORM_APPEARANCE_STATE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 123
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "UUID"

    move-object/from16 v63, v4

    const/16 v4, 0x3d

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->UUID:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 125
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "IN_REPLY_TO_UUID"

    move-object/from16 v64, v2

    const/16 v2, 0x3e

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->IN_REPLY_TO_UUID:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 127
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "IN_REPLY_TO"

    move-object/from16 v65, v4

    const/16 v4, 0x3f

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->IN_REPLY_TO:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 129
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "AUTHOR_STATE_MODEL"

    move-object/from16 v66, v2

    const/16 v2, 0x40

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->AUTHOR_STATE_MODEL:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 131
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "AUTHOR_STATE"

    move-object/from16 v67, v4

    const/16 v4, 0x41

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->AUTHOR_STATE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 138
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "BLEND_MODE"

    move-object/from16 v68, v2

    const/16 v2, 0x42

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->BLEND_MODE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 145
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "OUTLINE_COLOR"

    move-object/from16 v69, v4

    const/16 v4, 0x43

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->OUTLINE_COLOR:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 151
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "OVERLAY_TEXT"

    move-object/from16 v70, v2

    const/16 v2, 0x44

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->OVERLAY_TEXT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 158
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "REPEAT_OVERLAY_TEXT"

    move-object/from16 v71, v4

    const/16 v4, 0x45

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->REPEAT_OVERLAY_TEXT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 165
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "CUSTOM_DATA"

    move-object/from16 v72, v2

    const/16 v2, 0x46

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->CUSTOM_DATA:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 167
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "WEB_ID"

    move-object/from16 v73, v4

    const/16 v4, 0x47

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->WEB_ID:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 174
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "IS_INSTANT_COMMENT_THREAD_ROOT"

    move-object/from16 v74, v2

    const/16 v2, 0x48

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->IS_INSTANT_COMMENT_THREAD_ROOT:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 180
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "FIRST_COMMENT_WEB_ID"

    move-object/from16 v75, v4

    const/16 v4, 0x49

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->FIRST_COMMENT_WEB_ID:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 186
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "FIRST_COMMENT_CUSTOM_DATA"

    move-object/from16 v76, v2

    const/16 v2, 0x4a

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->FIRST_COMMENT_CUSTOM_DATA:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 188
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "MEASUREMENT_SCALE"

    move-object/from16 v77, v4

    const/16 v4, 0x4b

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->MEASUREMENT_SCALE:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    .line 190
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const-string v6, "MEASUREMENT_PRECISION"

    move-object/from16 v78, v2

    const/16 v2, 0x4c

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePropertyKey;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePropertyKey;->MEASUREMENT_PRECISION:Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const/16 v2, 0x4d

    new-array v2, v2, [Lcom/pspdfkit/internal/jni/NativePropertyKey;

    const/4 v6, 0x0

    aput-object v0, v2, v6

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object v5, v2, v0

    const/4 v0, 0x4

    aput-object v7, v2, v0

    const/4 v0, 0x5

    aput-object v9, v2, v0

    const/4 v0, 0x6

    aput-object v11, v2, v0

    const/4 v0, 0x7

    aput-object v13, v2, v0

    const/16 v0, 0x8

    aput-object v15, v2, v0

    const/16 v0, 0x9

    aput-object v14, v2, v0

    const/16 v0, 0xa

    aput-object v12, v2, v0

    const/16 v0, 0xb

    aput-object v10, v2, v0

    const/16 v0, 0xc

    aput-object v8, v2, v0

    const/16 v0, 0xd

    aput-object v16, v2, v0

    const/16 v0, 0xe

    aput-object v17, v2, v0

    const/16 v0, 0xf

    aput-object v18, v2, v0

    const/16 v0, 0x10

    aput-object v19, v2, v0

    const/16 v0, 0x11

    aput-object v20, v2, v0

    const/16 v0, 0x12

    aput-object v21, v2, v0

    const/16 v0, 0x13

    aput-object v22, v2, v0

    const/16 v0, 0x14

    aput-object v23, v2, v0

    const/16 v0, 0x15

    aput-object v24, v2, v0

    const/16 v0, 0x16

    aput-object v25, v2, v0

    const/16 v0, 0x17

    aput-object v26, v2, v0

    const/16 v0, 0x18

    aput-object v27, v2, v0

    const/16 v0, 0x19

    aput-object v28, v2, v0

    const/16 v0, 0x1a

    aput-object v29, v2, v0

    const/16 v0, 0x1b

    aput-object v30, v2, v0

    const/16 v0, 0x1c

    aput-object v31, v2, v0

    const/16 v0, 0x1d

    aput-object v32, v2, v0

    const/16 v0, 0x1e

    aput-object v33, v2, v0

    const/16 v0, 0x1f

    aput-object v34, v2, v0

    const/16 v0, 0x20

    aput-object v35, v2, v0

    const/16 v0, 0x21

    aput-object v36, v2, v0

    const/16 v0, 0x22

    aput-object v37, v2, v0

    const/16 v0, 0x23

    aput-object v38, v2, v0

    const/16 v0, 0x24

    aput-object v39, v2, v0

    const/16 v0, 0x25

    aput-object v40, v2, v0

    const/16 v0, 0x26

    aput-object v41, v2, v0

    const/16 v0, 0x27

    aput-object v42, v2, v0

    const/16 v0, 0x28

    aput-object v43, v2, v0

    const/16 v0, 0x29

    aput-object v44, v2, v0

    const/16 v0, 0x2a

    aput-object v45, v2, v0

    const/16 v0, 0x2b

    aput-object v46, v2, v0

    const/16 v0, 0x2c

    aput-object v47, v2, v0

    const/16 v0, 0x2d

    aput-object v48, v2, v0

    const/16 v0, 0x2e

    aput-object v49, v2, v0

    const/16 v0, 0x2f

    aput-object v50, v2, v0

    const/16 v0, 0x30

    aput-object v51, v2, v0

    const/16 v0, 0x31

    aput-object v52, v2, v0

    const/16 v0, 0x32

    aput-object v53, v2, v0

    const/16 v0, 0x33

    aput-object v54, v2, v0

    const/16 v0, 0x34

    aput-object v55, v2, v0

    const/16 v0, 0x35

    aput-object v56, v2, v0

    const/16 v0, 0x36

    aput-object v57, v2, v0

    const/16 v0, 0x37

    aput-object v58, v2, v0

    const/16 v0, 0x38

    aput-object v59, v2, v0

    const/16 v0, 0x39

    aput-object v60, v2, v0

    const/16 v0, 0x3a

    aput-object v61, v2, v0

    const/16 v0, 0x3b

    aput-object v62, v2, v0

    const/16 v0, 0x3c

    aput-object v63, v2, v0

    const/16 v0, 0x3d

    aput-object v64, v2, v0

    const/16 v0, 0x3e

    aput-object v65, v2, v0

    const/16 v0, 0x3f

    aput-object v66, v2, v0

    const/16 v0, 0x40

    aput-object v67, v2, v0

    const/16 v0, 0x41

    aput-object v68, v2, v0

    const/16 v0, 0x42

    aput-object v69, v2, v0

    const/16 v0, 0x43

    aput-object v70, v2, v0

    const/16 v0, 0x44

    aput-object v71, v2, v0

    const/16 v0, 0x45

    aput-object v72, v2, v0

    const/16 v0, 0x46

    aput-object v73, v2, v0

    const/16 v0, 0x47

    aput-object v74, v2, v0

    const/16 v0, 0x48

    aput-object v75, v2, v0

    const/16 v0, 0x49

    aput-object v76, v2, v0

    const/16 v0, 0x4a

    aput-object v77, v2, v0

    const/16 v0, 0x4b

    aput-object v78, v2, v0

    const/16 v0, 0x4c

    aput-object v4, v2, v0

    .line 191
    sput-object v2, Lcom/pspdfkit/internal/jni/NativePropertyKey;->$VALUES:[Lcom/pspdfkit/internal/jni/NativePropertyKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativePropertyKey;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativePropertyKey;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativePropertyKey;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativePropertyKey;->$VALUES:[Lcom/pspdfkit/internal/jni/NativePropertyKey;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativePropertyKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativePropertyKey;

    return-object v0
.end method

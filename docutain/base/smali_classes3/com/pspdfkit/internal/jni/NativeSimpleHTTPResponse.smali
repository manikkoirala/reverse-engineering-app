.class public final Lcom/pspdfkit/internal/jni/NativeSimpleHTTPResponse;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final mBody:[B

.field final mErrorString:Ljava/lang/String;

.field final mHttpResponseCode:I


# direct methods
.method public constructor <init>([BILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/jni/NativeSimpleHTTPResponse;->mBody:[B

    .line 3
    iput p2, p0, Lcom/pspdfkit/internal/jni/NativeSimpleHTTPResponse;->mHttpResponseCode:I

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/jni/NativeSimpleHTTPResponse;->mErrorString:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getBody()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativeSimpleHTTPResponse;->mBody:[B

    return-object v0
.end method

.method public getErrorString()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/jni/NativeSimpleHTTPResponse;->mErrorString:Ljava/lang/String;

    return-object v0
.end method

.method public getHttpResponseCode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/jni/NativeSimpleHTTPResponse;->mHttpResponseCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NativeSimpleHTTPResponse{mBody="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativeSimpleHTTPResponse;->mBody:[B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ",mHttpResponseCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/internal/jni/NativeSimpleHTTPResponse;->mHttpResponseCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",mErrorString="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/internal/jni/NativeSimpleHTTPResponse;->mErrorString:Ljava/lang/String;

    const-string v2, "}"

    .line 2
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/rg;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

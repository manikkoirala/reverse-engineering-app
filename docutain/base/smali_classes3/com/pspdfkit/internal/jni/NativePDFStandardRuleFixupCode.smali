.class public final enum Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum ADAPT_EMBEDDED_FILE_METADATA:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum ADAPT_FILE_SPECIFICATION:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum ADAPT_OPTIONAL_CONTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum ADD_DEFAULT_COLOR_SPACE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum CREATE_MISSING_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum DISABLE_OVERPRINT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum EMBED_FONT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum FIX_COLORANT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum FIX_HALFTONE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum FIX_IMAGE_PROPERTIES:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum FIX_INCONGRUENT_SEPARATING_COLOR_SPACES:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum FIX_INVALID_FORM_X_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum FIX_NAME_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum FIX_NON_CONFORMING_FONT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum FIX_RENDERING_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum FIX_STREAM_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum FIX_STRING_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum GENERATE_NEW_ANNOTATION_APPEARANCE_STREAM:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum IMPLEMENTATION_LIMIT_WONT_BE_FIXED:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum INVALID_SIGNATURE_WONT_BE_REMOVED:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum INVALID_USER_RIGHTS_DICTIONARY_WONT_BE_REMOVED:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum MODIFY_INCOMPATIBLE_BLEND_MODE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum RECOMPRESS_STREAM:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REMOVE_ADDITIONAL_ACTIONS_PAGE_OR_CATALOG:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REMOVE_ALTERNATE_PRESENTATIONS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REMOVE_DOCUMENT_REQUIREMENTS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REMOVE_ENCRYPTION:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REMOVE_INCOMPATIBLE_ANNOTATIONS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REMOVE_INCOMPATIBLE_CIDSET:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REMOVE_INCOMPATIBLE_FORM_PROPERTIES:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REMOVE_INCOMPATIBLE_PERMS_KEYS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REMOVE_INVALID_ACTIONS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REMOVE_INVALID_DOCUMENT_METADATA:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REMOVE_INVALID_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REMOVE_NON_PDF_ATTACHMENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REMOVE_OR_ADAPT_TRANSFER_FUNCTION:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REPAIR_DOCUMENT_METADATA:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REPLACE_OUTPUT_INTENTS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum REWRITE_PDF_DOCUMENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

.field public static final enum SET_ANNOTATION_FLAG_DEFAULTS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;


# direct methods
.method static constructor <clinit>()V
    .locals 42

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v1, "REWRITE_PDF_DOCUMENT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REWRITE_PDF_DOCUMENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 6
    new-instance v1, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v3, "REMOVE_ENCRYPTION"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REMOVE_ENCRYPTION:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 8
    new-instance v3, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v5, "FIX_STRING_OBJECT"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->FIX_STRING_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 13
    new-instance v5, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v7, "FIX_STREAM_OBJECT"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->FIX_STREAM_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 18
    new-instance v7, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v9, "RECOMPRESS_STREAM"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->RECOMPRESS_STREAM:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 20
    new-instance v9, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v11, "FIX_NAME_OBJECT"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->FIX_NAME_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 26
    new-instance v11, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v13, "IMPLEMENTATION_LIMIT_WONT_BE_FIXED"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->IMPLEMENTATION_LIMIT_WONT_BE_FIXED:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 31
    new-instance v13, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v15, "REMOVE_INCOMPATIBLE_PERMS_KEYS"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REMOVE_INCOMPATIBLE_PERMS_KEYS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 37
    new-instance v15, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v14, "INVALID_USER_RIGHTS_DICTIONARY_WONT_BE_REMOVED"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->INVALID_USER_RIGHTS_DICTIONARY_WONT_BE_REMOVED:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 42
    new-instance v14, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v12, "CREATE_MISSING_OUTPUT_INTENT"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->CREATE_MISSING_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 47
    new-instance v12, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v10, "REPLACE_OUTPUT_INTENTS"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REPLACE_OUTPUT_INTENTS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 52
    new-instance v10, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v8, "REMOVE_INVALID_OUTPUT_INTENT"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REMOVE_INVALID_OUTPUT_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 57
    new-instance v8, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "MODIFY_INCOMPATIBLE_BLEND_MODE"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->MODIFY_INCOMPATIBLE_BLEND_MODE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 62
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v4, "DISABLE_OVERPRINT"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->DISABLE_OVERPRINT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 67
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v2, "REMOVE_OR_ADAPT_TRANSFER_FUNCTION"

    move-object/from16 v16, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REMOVE_OR_ADAPT_TRANSFER_FUNCTION:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 69
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "FIX_HALFTONE"

    move-object/from16 v17, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->FIX_HALFTONE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 71
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v4, "FIX_RENDERING_INTENT"

    move-object/from16 v18, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->FIX_RENDERING_INTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 77
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v2, "FIX_INCONGRUENT_SEPARATING_COLOR_SPACES"

    move-object/from16 v19, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->FIX_INCONGRUENT_SEPARATING_COLOR_SPACES:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 82
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "FIX_COLORANT"

    move-object/from16 v20, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->FIX_COLORANT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 87
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v4, "ADD_DEFAULT_COLOR_SPACE"

    move-object/from16 v21, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->ADD_DEFAULT_COLOR_SPACE:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 92
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v2, "FIX_IMAGE_PROPERTIES"

    move-object/from16 v22, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->FIX_IMAGE_PROPERTIES:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 97
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "FIX_INVALID_FORM_X_OBJECT"

    move-object/from16 v23, v4

    const/16 v4, 0x15

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->FIX_INVALID_FORM_X_OBJECT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 102
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v4, "REMOVE_INCOMPATIBLE_ANNOTATIONS"

    move-object/from16 v24, v2

    const/16 v2, 0x16

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REMOVE_INCOMPATIBLE_ANNOTATIONS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 104
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v4, "SET_ANNOTATION_FLAG_DEFAULTS"

    move-object/from16 v25, v6

    const/16 v6, 0x17

    invoke-direct {v2, v4, v6}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->SET_ANNOTATION_FLAG_DEFAULTS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 106
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "GENERATE_NEW_ANNOTATION_APPEARANCE_STREAM"

    move-object/from16 v26, v2

    const/16 v2, 0x18

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->GENERATE_NEW_ANNOTATION_APPEARANCE_STREAM:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 111
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "REMOVE_INCOMPATIBLE_FORM_PROPERTIES"

    move-object/from16 v27, v4

    const/16 v4, 0x19

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REMOVE_INCOMPATIBLE_FORM_PROPERTIES:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 118
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "INVALID_SIGNATURE_WONT_BE_REMOVED"

    move-object/from16 v28, v2

    const/16 v2, 0x1a

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->INVALID_SIGNATURE_WONT_BE_REMOVED:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 120
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "REMOVE_INVALID_ACTIONS"

    move-object/from16 v29, v4

    const/16 v4, 0x1b

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REMOVE_INVALID_ACTIONS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 122
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "REMOVE_ADDITIONAL_ACTIONS_PAGE_OR_CATALOG"

    move-object/from16 v30, v2

    const/16 v2, 0x1c

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REMOVE_ADDITIONAL_ACTIONS_PAGE_OR_CATALOG:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 127
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "REPAIR_DOCUMENT_METADATA"

    move-object/from16 v31, v4

    const/16 v4, 0x1d

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REPAIR_DOCUMENT_METADATA:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 132
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "REMOVE_INVALID_DOCUMENT_METADATA"

    move-object/from16 v32, v2

    const/16 v2, 0x1e

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REMOVE_INVALID_DOCUMENT_METADATA:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 137
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "ADAPT_EMBEDDED_FILE_METADATA"

    move-object/from16 v33, v4

    const/16 v4, 0x1f

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->ADAPT_EMBEDDED_FILE_METADATA:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 143
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "ADAPT_FILE_SPECIFICATION"

    move-object/from16 v34, v2

    const/16 v2, 0x20

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->ADAPT_FILE_SPECIFICATION:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 145
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "REMOVE_NON_PDF_ATTACHMENT"

    move-object/from16 v35, v4

    const/16 v4, 0x21

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REMOVE_NON_PDF_ATTACHMENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 150
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "ADAPT_OPTIONAL_CONTENT"

    move-object/from16 v36, v2

    const/16 v2, 0x22

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->ADAPT_OPTIONAL_CONTENT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 152
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "REMOVE_ALTERNATE_PRESENTATIONS"

    move-object/from16 v37, v4

    const/16 v4, 0x23

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REMOVE_ALTERNATE_PRESENTATIONS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 154
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "REMOVE_DOCUMENT_REQUIREMENTS"

    move-object/from16 v38, v2

    const/16 v2, 0x24

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REMOVE_DOCUMENT_REQUIREMENTS:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 156
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "FIX_NON_CONFORMING_FONT"

    move-object/from16 v39, v4

    const/16 v4, 0x25

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->FIX_NON_CONFORMING_FONT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 158
    new-instance v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "EMBED_FONT"

    move-object/from16 v40, v2

    const/16 v2, 0x26

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->EMBED_FONT:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    .line 160
    new-instance v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const-string v6, "REMOVE_INCOMPATIBLE_CIDSET"

    move-object/from16 v41, v4

    const/16 v4, 0x27

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->REMOVE_INCOMPATIBLE_CIDSET:Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const/16 v4, 0x28

    new-array v4, v4, [Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    const/4 v6, 0x0

    aput-object v0, v4, v6

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v3, v4, v0

    const/4 v0, 0x3

    aput-object v5, v4, v0

    const/4 v0, 0x4

    aput-object v7, v4, v0

    const/4 v0, 0x5

    aput-object v9, v4, v0

    const/4 v0, 0x6

    aput-object v11, v4, v0

    const/4 v0, 0x7

    aput-object v13, v4, v0

    const/16 v0, 0x8

    aput-object v15, v4, v0

    const/16 v0, 0x9

    aput-object v14, v4, v0

    const/16 v0, 0xa

    aput-object v12, v4, v0

    const/16 v0, 0xb

    aput-object v10, v4, v0

    const/16 v0, 0xc

    aput-object v8, v4, v0

    const/16 v0, 0xd

    aput-object v16, v4, v0

    const/16 v0, 0xe

    aput-object v17, v4, v0

    const/16 v0, 0xf

    aput-object v18, v4, v0

    const/16 v0, 0x10

    aput-object v19, v4, v0

    const/16 v0, 0x11

    aput-object v20, v4, v0

    const/16 v0, 0x12

    aput-object v21, v4, v0

    const/16 v0, 0x13

    aput-object v22, v4, v0

    const/16 v0, 0x14

    aput-object v23, v4, v0

    const/16 v0, 0x15

    aput-object v24, v4, v0

    const/16 v0, 0x16

    aput-object v25, v4, v0

    const/16 v0, 0x17

    aput-object v26, v4, v0

    const/16 v0, 0x18

    aput-object v27, v4, v0

    const/16 v0, 0x19

    aput-object v28, v4, v0

    const/16 v0, 0x1a

    aput-object v29, v4, v0

    const/16 v0, 0x1b

    aput-object v30, v4, v0

    const/16 v0, 0x1c

    aput-object v31, v4, v0

    const/16 v0, 0x1d

    aput-object v32, v4, v0

    const/16 v0, 0x1e

    aput-object v33, v4, v0

    const/16 v0, 0x1f

    aput-object v34, v4, v0

    const/16 v0, 0x20

    aput-object v35, v4, v0

    const/16 v0, 0x21

    aput-object v36, v4, v0

    const/16 v0, 0x22

    aput-object v37, v4, v0

    const/16 v0, 0x23

    aput-object v38, v4, v0

    const/16 v0, 0x24

    aput-object v39, v4, v0

    const/16 v0, 0x25

    aput-object v40, v4, v0

    const/16 v0, 0x26

    aput-object v41, v4, v0

    const/16 v0, 0x27

    aput-object v2, v4, v0

    .line 161
    sput-object v4, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->$VALUES:[Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->$VALUES:[Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativePDFStandardRuleFixupCode;

    return-object v0
.end method

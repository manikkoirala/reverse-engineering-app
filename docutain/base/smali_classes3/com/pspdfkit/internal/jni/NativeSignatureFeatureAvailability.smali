.class public final enum Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

.field public static final enum ELECTRONICSIGNATURES:Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

.field public static final enum LEGACYSIGNATURES:Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

.field public static final enum NONE:Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    const-string v1, "ELECTRONICSIGNATURES"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;->ELECTRONICSIGNATURES:Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    const-string v3, "LEGACYSIGNATURES"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;->LEGACYSIGNATURES:Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    .line 5
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    const-string v5, "NONE"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;->NONE:Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    const/4 v5, 0x3

    new-array v5, v5, [Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    aput-object v0, v5, v2

    aput-object v1, v5, v4

    aput-object v3, v5, v6

    .line 6
    sput-object v5, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;->$VALUES:[Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    return-object v0
.end method

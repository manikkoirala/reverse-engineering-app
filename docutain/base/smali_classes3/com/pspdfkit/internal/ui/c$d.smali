.class final Lcom/pspdfkit/internal/ui/c$d;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/ui/c;->a(Ljava/util/List;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/ui/c;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/c$d;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c$d;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetv(Lcom/pspdfkit/internal/ui/c;)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/c;)Landroidx/appcompat/app/AppCompatActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/appcompat/app/AppCompatActivity;->isChangingConfigurations()Z

    move-result p1

    if-nez p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c$d;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$mt(Lcom/pspdfkit/internal/ui/c;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c$d;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetv(Lcom/pspdfkit/internal/ui/c;)Landroid/animation/AnimatorSet;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/animation/AnimatorSet;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_0
    return-void
.end method

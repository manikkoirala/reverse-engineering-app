.class public Lcom/pspdfkit/internal/ui/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/listeners/DocumentListener;
.implements Lcom/pspdfkit/listeners/OnVisibilityChangedListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionModeChangeListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingModeChangeListener;
.implements Lcom/pspdfkit/internal/ui/c$g;
.implements Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;
.implements Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;
.implements Lcom/pspdfkit/internal/zq;
.implements Lcom/pspdfkit/internal/ev;
.implements Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;
.implements Lcom/pspdfkit/internal/og$a;
.implements Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController$OnSearchSelectedTextListener;
.implements Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioPlaybackModeChangeListener;
.implements Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioRecordingModeChangeListener;
.implements Lcom/pspdfkit/internal/zf$f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/f$d;,
        Lcom/pspdfkit/internal/ui/f$g;,
        Lcom/pspdfkit/internal/ui/f$e;,
        Lcom/pspdfkit/internal/ui/f$f;
    }
.end annotation


# static fields
.field public static final DEFAULT_PDF_FRAGMENT_TAG:Ljava/lang/String; = "PSPDFKit.Fragment"

.field public static final PARAM_ACTIVITY_STATE:Ljava/lang/String; = "activityState"

.field private static final STATE_ACTIVE_VIEW_ITEM:Ljava/lang/String; = "PSPDFKit.ActiveMenuOption"

.field private static final STATE_ANNOTATION_CREATION_ACTIVE:Ljava/lang/String; = "PdfUiImpl.AnnotationCreationActive"

.field private static final STATE_ANNOTATION_CREATION_INSPECTOR:Ljava/lang/String; = "PdfActivity.AnnotationCreationInspector"

.field private static final STATE_ANNOTATION_EDITING_INSPECTOR:Ljava/lang/String; = "PdfActivity.AnnotationEditingInspector"

.field private static final STATE_CONFIGURATION:Ljava/lang/String; = "PdfActivity.Configuration"

.field private static final STATE_CONTENT_EDITING_ACTIVE:Ljava/lang/String; = "PdfUiImpl.ContentEditingActive"

.field private static final STATE_CONTENT_EDITING_INSPECTOR:Ljava/lang/String; = "PdfActivity.ContentEditingInspector"

.field private static final STATE_DOCUMENT_COORDINATOR:Ljava/lang/String; = "PdfActivity.PdfDocumentCoordinatorState"

.field private static final STATE_FORM_EDITING_INSPECTOR:Ljava/lang/String; = "PdfActivity.FormEditingInspector"

.field private static final STATE_FRAGMENT:Ljava/lang/String; = "PdfActivity.FragmentState"

.field private static final STATE_FRAGMENT_CONTAINER_ID:Ljava/lang/String; = "PdfActivity.FragmentContainerId"

.field private static final STATE_LAST_ENABLED_UI_STATE:Ljava/lang/String; = "PdfActivity.LastEnabledUiState"

.field private static final STATE_PENDING_INITIAL_PAGE:Ljava/lang/String; = "PdfActivity.PendingInitialPage"

.field private static final STATE_SCREEN_TIMEOUT:Ljava/lang/String; = "PdfUiImpl.ScreenTimeout"

.field private static final STATE_UI_STATE:Ljava/lang/String; = "PdfActivity.UiState"

.field private static final USER_INTERFACE_ENABLED_REFRESH_DELAY:J = 0x64L

.field public static retainedDocument:Lcom/pspdfkit/document/PdfDocument;


# instance fields
.field private actionResolver:Lcom/pspdfkit/internal/ui/b;

.field protected final activity:Landroidx/appcompat/app/AppCompatActivity;

.field private final activityJsPlatformDelegate:Lcom/pspdfkit/internal/lg;

.field private final activityListener:Lcom/pspdfkit/listeners/PdfActivityListener;

.field private annotationCreationInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;

.field private annotationCreationToolbar:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;

.field private annotationEditingInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;

.field private annotationEditingToolbar:Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;

.field private annotationNoteHinter:Lcom/pspdfkit/ui/note/AnnotationNoteHinter;

.field private configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

.field private contentEditingInspectorController:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

.field private contentEditingToolBar:Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;

.field private currentContentEditingController:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

.field private currentlyEditedBlockID:Ljava/util/UUID;

.field protected document:Lcom/pspdfkit/internal/zf;

.field private final documentCoordinator:Lcom/pspdfkit/internal/ui/e;

.field private documentEditingToolbar:Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;

.field private documentInteractionEnabled:Z

.field private documentPrintDialogFactory:Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;

.field private final documentScrollListener:Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;

.field private documentSharingDialogFactory:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

.field private formEditingInspectorController:Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;

.field protected fragment:Lcom/pspdfkit/ui/PdfFragment;

.field private fragmentContainerId:I

.field private final handler:Landroid/os/Handler;

.field private final internalPdfUi:Lcom/pspdfkit/internal/bg;

.field private isInAnnotationCreationMode:Z

.field private isInContentEditingMode:Z

.field private keyEventContract:Lcom/pspdfkit/internal/og;

.field private lastEnabledUiState:Landroid/os/Bundle;

.field private menuConfiguration:Lcom/pspdfkit/internal/qm;

.field private menuManager:Lcom/pspdfkit/internal/pm;

.field private final onAnnotationSelectedListenerAdapter:Lcom/pspdfkit/ui/special_mode/manager/OnAnnotationSelectedListenerAdapter;

.field private final pdfUi:Lcom/pspdfkit/ui/PdfUi;

.field private pendingInitialPage:I

.field private positionListener:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;

.field private printOptionsProvider:Lcom/pspdfkit/document/printing/PrintOptionsProvider;

.field propertyInspectorCoordinatorLayout:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

.field private redactionApplicator:Lcom/pspdfkit/internal/so;

.field private rootView:Landroid/view/View;

.field private screenTimeoutMillis:J

.field private settingsModePicker:Lcom/pspdfkit/ui/settings/SettingsModePicker;

.field private settingsModePopup:Landroid/widget/PopupWindow;

.field sharingMenuFragment:Lcom/pspdfkit/internal/kr;

.field private sharingMenuListener:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

.field private sharingOptionsProvider:Lcom/pspdfkit/document/sharing/SharingOptionsProvider;

.field private textSelectionToolbar:Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;

.field private toolbar:Landroidx/appcompat/widget/Toolbar;

.field protected toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

.field private toolbarElevation:F

.field private userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

.field private userInterfaceEnabled:Z

.field private userInterfaceEnabledRunnable:Ljava/lang/Runnable;

.field views:Lcom/pspdfkit/internal/yf;


# direct methods
.method public static synthetic $r8$lambda$Xo3KlzWhbm4FzlsCWqkucRCcWKc(Lcom/pspdfkit/internal/ui/f;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->removeKeepScreenOn()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetcurrentlyEditedBlockID(Lcom/pspdfkit/internal/ui/f;)Ljava/util/UUID;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/f;->currentlyEditedBlockID:Ljava/util/UUID;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetisInContentEditingMode(Lcom/pspdfkit/internal/ui/f;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/ui/f;->isInContentEditingMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetuserInterfaceCoordinator(Lcom/pspdfkit/internal/ui/f;)Lcom/pspdfkit/internal/ui/c;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhideContentEditingStylingBar(Lcom/pspdfkit/internal/ui/f;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->hideContentEditingStylingBar()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowContentEditingStylingBar(Lcom/pspdfkit/internal/ui/f;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->showContentEditingStylingBar()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mtoggleView(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/f;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    return-void
.end method

.method public constructor <init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/internal/bg;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 2
    iput v0, p0, Lcom/pspdfkit/internal/ui/f;->pendingInitialPage:I

    const/4 v0, 0x0

    .line 42
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->isInAnnotationCreationMode:Z

    .line 45
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->isInContentEditingMode:Z

    const-wide/16 v1, 0x0

    .line 48
    iput-wide v1, p0, Lcom/pspdfkit/internal/ui/f;->screenTimeoutMillis:J

    .line 50
    new-instance v1, Landroid/os/Handler;

    .line 51
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/f;->handler:Landroid/os/Handler;

    .line 72
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceEnabled:Z

    const/4 v0, 0x1

    .line 73
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->documentInteractionEnabled:Z

    .line 75
    new-instance v0, Lcom/pspdfkit/internal/ui/f$a;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/f$a;-><init>(Lcom/pspdfkit/internal/ui/f;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->onAnnotationSelectedListenerAdapter:Lcom/pspdfkit/ui/special_mode/manager/OnAnnotationSelectedListenerAdapter;

    .line 89
    new-instance v0, Lcom/pspdfkit/internal/om;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/om;-><init>(Lcom/pspdfkit/internal/ui/f;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activityJsPlatformDelegate:Lcom/pspdfkit/internal/lg;

    .line 96
    new-instance v0, Lcom/pspdfkit/internal/ui/e;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/e;-><init>(Lcom/pspdfkit/internal/ui/f;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentCoordinator:Lcom/pspdfkit/internal/ui/e;

    .line 100
    new-instance v0, Lcom/pspdfkit/internal/ui/f$d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/ui/f$d;-><init>(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/internal/ui/f$d-IA;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentScrollListener:Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;

    .line 2055
    iput-object v1, p0, Lcom/pspdfkit/internal/ui/f;->currentContentEditingController:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 2080
    iput-object v1, p0, Lcom/pspdfkit/internal/ui/f;->currentlyEditedBlockID:Ljava/util/UUID;

    const-string v0, "pdfUi"

    .line 2081
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2082
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/f;->pdfUi:Lcom/pspdfkit/ui/PdfUi;

    .line 2083
    iput-object p3, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    .line 2084
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2085
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/f;->activityListener:Lcom/pspdfkit/listeners/PdfActivityListener;

    return-void
.end method

.method public static applyConfigurationToParamsAndState(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "PSPDF.Configuration"

    .line 1
    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    if-eqz p2, :cond_0

    const-string p1, "PdfActivity.Configuration"

    .line 6
    invoke-virtual {p2, p1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string p0, "PdfActivity.FragmentState"

    .line 8
    invoke-virtual {p2, p0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {p2, p0, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method private ensureSharingMenuFragment()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/internal/bg;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/kr;->a(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/kr;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    :cond_0
    return-void
.end method

.method private getAnnotationCreationInspectorController()Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationCreationInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationInspectorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    new-instance v0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->propertyInspectorCoordinatorLayout:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationCreationInspectorController;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationCreationInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationCreationInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;

    return-object v0
.end method

.method private getAnnotationEditingInspectorController()Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationEditingInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationInspectorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    new-instance v0, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->propertyInspectorCoordinatorLayout:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/ui/inspector/annotation/DefaultAnnotationEditingInspectorController;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationEditingInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationEditingInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;

    return-object v0
.end method

.method private getAnnotationEditingToolbar()Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationEditingToolbar:Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationEditingToolbar:Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;

    .line 3
    iget v1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarElevation:F

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationEditingToolbar:Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;

    return-object v0
.end method

.method private getContentEditingInspectorController()Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->contentEditingInspectorController:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

    if-nez v0, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->b(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    new-instance v0, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->propertyInspectorCoordinatorLayout:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->contentEditingInspectorController:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->contentEditingInspectorController:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

    return-object v0
.end method

.method private getContentEditingToolBar()Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->contentEditingToolBar:Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;

    if-nez v0, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->b(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    new-instance v0, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->contentEditingToolBar:Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;

    .line 4
    iget v1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarElevation:F

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->contentEditingToolBar:Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;

    return-object v0
.end method

.method private getDocumentEditingToolbar()Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentEditingToolbar:Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentEditingToolbar:Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;

    .line 3
    iget v1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarElevation:F

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentEditingToolbar:Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;

    return-object v0
.end method

.method private getFormEditingInspectorController()Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->formEditingInspectorController:Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isFormEditingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    new-instance v0, Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->propertyInspectorCoordinatorLayout:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->formEditingInspectorController:Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->formEditingInspectorController:Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;

    return-object v0
.end method

.method private getManifestTheme()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 3
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 4
    iget v0, v0, Landroid/content/pm/ActivityInfo;->theme:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 7
    :catch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "com.pspdfkit.ui.PdfActivity not found"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getTextSelectionToolbar()Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->textSelectionToolbar:Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isTextSelectionPopupToolbarEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->textSelectionToolbar:Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;

    .line 4
    iget v1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarElevation:F

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->textSelectionToolbar:Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;

    return-object v0
.end method

.method private hideActions()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->menuManager:Lcom/pspdfkit/internal/pm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pm;->a()V

    return-void
.end method

.method private hideContentEditingStylingBar()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda3;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private initializeSettingsPopup()V
    .locals 4

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/settings/SettingsModePicker;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePicker:Lcom/pspdfkit/ui/settings/SettingsModePicker;

    .line 2
    invoke-virtual {v0, p0}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setOnModeChangedListener(Lcom/pspdfkit/ui/settings/SettingsModePicker$OnModeChangedListener;)V

    .line 3
    new-instance v0, Landroid/widget/PopupWindow;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    sget v3, Landroidx/appcompat/R$style;->Widget_AppCompat_PopupMenu:I

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePopup:Landroid/widget/PopupWindow;

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePicker:Lcom/pspdfkit/ui/settings/SettingsModePicker;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePopup:Landroid/widget/PopupWindow;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePopup:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroidx/core/widget/PopupWindowCompat;->setOverlapAnchor(Landroid/widget/PopupWindow;Z)V

    .line 9
    invoke-static {}, Lcom/pspdfkit/internal/v;->c()Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePopup:Landroid/widget/PopupWindow;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePopup:Landroid/widget/PopupWindow;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 19
    :goto_0
    invoke-static {}, Lcom/pspdfkit/internal/v;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v0}, Landroid/transition/TransitionInflater;->from(Landroid/content/Context;)Landroid/transition/TransitionInflater;

    move-result-object v0

    .line 22
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePopup:Landroid/widget/PopupWindow;

    sget v3, Lcom/pspdfkit/R$transition;->pspdf__popup_window_enter:I

    invoke-virtual {v0, v3}, Landroid/transition/TransitionInflater;->inflateTransition(I)Landroid/transition/Transition;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setEnterTransition(Landroid/transition/Transition;)V

    .line 23
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePopup:Landroid/widget/PopupWindow;

    sget v3, Lcom/pspdfkit/R$transition;->pspdf__popup_window_exit:I

    invoke-virtual {v0, v3}, Landroid/transition/TransitionInflater;->inflateTransition(I)Landroid/transition/Transition;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/PopupWindow;->setExitTransition(Landroid/transition/Transition;)V

    .line 26
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    return-void
.end method

.method private isUsingCustomFragmentTag()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    invoke-interface {v0}, Lcom/pspdfkit/internal/bg;->getPdfParameters()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const-string v1, "PSPDF.PdfFragmentTag"

    const-string v2, "PSPDFKit.Fragment"

    .line 5
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 6
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method static synthetic lambda$performPrint$0(Lcom/pspdfkit/document/printing/PrintOptions;Lcom/pspdfkit/document/PdfDocument;I)Lcom/pspdfkit/document/printing/PrintOptions;
    .locals 0

    return-object p0
.end method

.method private refreshPropertyInspectorCoordinatorLayout(Lcom/pspdfkit/internal/ui/c;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/c;->isUserInterfaceVisible()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getThumbnailBarMode()Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    move-result-object v0

    sget-object v2, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_PINNED:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    if-ne v0, v2, :cond_0

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/c;->i()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 4
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->propertyInspectorCoordinatorLayout:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    xor-int/2addr p1, v1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->setDrawUnderBottomInset(Z)V

    return-void
.end method

.method private registerDocumentEditingToolbarListener(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->isDocumentEditorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditorSavingToolbarHandler()Lcom/pspdfkit/internal/m8;

    move-result-object p1

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/m8;->getDocumentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager;

    move-result-object p1

    .line 5
    check-cast p1, Lcom/pspdfkit/internal/j8;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/j8;->addOnDocumentEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingModeChangeListener;)V

    :cond_0
    return-void
.end method

.method private removeKeepScreenOn()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    return-void
.end method

.method private resetUI()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/kr;->a()V

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->removeContextualToolbar(Z)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    invoke-interface {v0}, Lcom/pspdfkit/internal/bg;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/ui/AnnotationCreatorInputDialogFragment;->hide(Landroidx/fragment/app/FragmentManager;)V

    return-void
.end method

.method private restoreUserInterfaceState(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/c;->a(Landroid/os/Bundle;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/sm;->onRestoreViewHierarchyState(Landroid/os/Bundle;)V

    const-string v0, "PdfUiImpl.AnnotationCreationActive"

    .line 7
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->isInAnnotationCreationMode:Z

    const-string v0, "PdfUiImpl.ContentEditingActive"

    .line 8
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->isInContentEditingMode:Z

    .line 9
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->updateMenuIcons()V

    .line 12
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    .line 13
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PSPDFKit.ActiveMenuOption"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 14
    invoke-static {v1}, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v1

    if-ne v1, v0, :cond_0

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    const-wide/16 v2, 0x0

    .line 18
    invoke-virtual {v0, v1, v2, v3}, Lcom/pspdfkit/internal/sm;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)Z

    goto :goto_0

    .line 19
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/sm;->showView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)Z

    :goto_0
    const-string v0, "PdfActivity.AnnotationCreationInspector"

    .line 24
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 27
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getAnnotationCreationInspectorController()Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 29
    invoke-interface {v1, v0}, Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;->onRestoreInstanceState(Landroid/os/Bundle;)V

    :cond_1
    const-string v0, "PdfActivity.AnnotationEditingInspector"

    .line 32
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 35
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getAnnotationEditingInspectorController()Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 37
    invoke-interface {v1, v0}, Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;->onRestoreInstanceState(Landroid/os/Bundle;)V

    :cond_2
    const-string v0, "PdfActivity.FormEditingInspector"

    .line 42
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 44
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getFormEditingInspectorController()Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 46
    invoke-virtual {v1, v0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->onRestoreInstanceState(Landroid/os/Bundle;)V

    :cond_3
    const-string v0, "PdfActivity.ContentEditingInspector"

    .line 51
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 54
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getContentEditingInspectorController()Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 56
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;->onRestoreInstanceState(Landroid/os/Bundle;)V

    :cond_4
    return-void
.end method

.method private saveUserInterfaceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/c;->b(Landroid/os/Bundle;)V

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/sm;->onSaveViewHierarchyState(Landroid/os/Bundle;)V

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getActiveView()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PSPDFKit.ActiveMenuOption"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->isInAnnotationCreationMode:Z

    const-string v1, "PdfUiImpl.AnnotationCreationActive"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 12
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->isInContentEditingMode:Z

    const-string v1, "PdfUiImpl.ContentEditingActive"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationCreationInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;

    if-eqz v0, :cond_1

    .line 16
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 17
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->annotationCreationInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;

    invoke-interface {v1, v0}, Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v1, "PdfActivity.AnnotationCreationInspector"

    .line 18
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 20
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationEditingInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;

    if-eqz v0, :cond_2

    .line 21
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 22
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->annotationEditingInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;

    invoke-interface {v1, v0}, Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v1, "PdfActivity.AnnotationEditingInspector"

    .line 23
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 27
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->formEditingInspectorController:Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;

    if-eqz v0, :cond_3

    .line 28
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 29
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->formEditingInspectorController:Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/ui/inspector/AbstractPropertyInspectorController;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v1, "PdfActivity.FormEditingInspector"

    .line 30
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 34
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->contentEditingInspectorController:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

    if-eqz v0, :cond_4

    .line 35
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 36
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->contentEditingInspectorController:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

    invoke-interface {v1, v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v1, "PdfActivity.ContentEditingInspector"

    .line 37
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_4
    return-void
.end method

.method private setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Z)V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    return-void

    .line 56
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 58
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    invoke-interface {p2, p1}, Lcom/pspdfkit/internal/bg;->performApplyConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    return-void
.end method

.method private showActions()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->menuManager:Lcom/pspdfkit/internal/pm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pm;->b()V

    return-void
.end method

.method private showAnnotationEditorWhenAppropriate(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 2

    .line 1
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->getCurrentlySelectedAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->hasInstantComments()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3
    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->showAnnotationEditor(Lcom/pspdfkit/annotations/Annotation;)V

    :cond_0
    return-void
.end method

.method private showContentEditingStylingBar()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->currentContentEditingController:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->currentContentEditingController:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->bindController(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V

    :cond_0
    return-void
.end method

.method private showSettingsDialog()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/internal/bg;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    iget-wide v7, p0, Lcom/pspdfkit/internal/ui/f;->screenTimeoutMillis:J

    const-string v2, "config"

    .line 3
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v10, Lcom/pspdfkit/internal/ar;

    .line 53
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getScrollDirection()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v3

    const-string v2, "config.configuration.scrollDirection"

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getScrollMode()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v4

    const-string v2, "config.configuration.scrollMode"

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getLayoutMode()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v5

    const-string v2, "config.configuration.layoutMode"

    invoke-static {v5, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getThemeMode()Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-result-object v6

    const-string v2, "config.configuration.themeMode"

    invoke-static {v6, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSettingsMenuItemShown()Ljava/util/EnumSet;

    move-result-object v9

    const-string v1, "config.settingsMenuItemShown"

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v10

    .line 59
    invoke-direct/range {v2 .. v9}, Lcom/pspdfkit/internal/ar;-><init>(Lcom/pspdfkit/configuration/page/PageScrollDirection;Lcom/pspdfkit/configuration/page/PageScrollMode;Lcom/pspdfkit/configuration/page/PageLayoutMode;Lcom/pspdfkit/configuration/theming/ThemeMode;JLjava/util/EnumSet;)V

    .line 60
    sget v1, Lcom/pspdfkit/internal/xq;->f:I

    const-string v1, "fragmentManager"

    .line 61
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "listener"

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "options"

    invoke-static {v10, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "com.pspdfkit.ui.dialog.SettingsDialog.FRAGMENT_TAG"

    .line 243
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/xq;

    if-eqz v2, :cond_0

    .line 244
    invoke-static {v2, p0}, Lcom/pspdfkit/internal/xq;->a(Lcom/pspdfkit/internal/xq;Lcom/pspdfkit/internal/zq;)V

    const-string v3, "<set-?>"

    .line 245
    invoke-static {v10, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 309
    iput-object v10, v2, Lcom/pspdfkit/internal/xq;->c:Lcom/pspdfkit/internal/ar;

    goto :goto_0

    .line 310
    :cond_0
    new-instance v2, Lcom/pspdfkit/internal/xq;

    invoke-direct {v2, p0, v10}, Lcom/pspdfkit/internal/xq;-><init>(Lcom/pspdfkit/internal/zq;Lcom/pspdfkit/internal/ar;)V

    .line 311
    :goto_0
    invoke-virtual {v2}, Landroidx/appcompat/app/AppCompatDialogFragment;->isAdded()Z

    move-result v3

    if-nez v3, :cond_1

    .line 312
    invoke-virtual {v2, v0, v1}, Landroidx/appcompat/app/AppCompatDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private showSettingsPopup()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    sget v1, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SETTINGS:I

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__toolbar_coordinator:I

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    .line 6
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 8
    :cond_0
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/f;->showSettingsPopupWindow(Landroid/view/View;)V

    return-void
.end method

.method private showSettingsPopupWindow(Landroid/view/View;)V
    .locals 3

    const-string v0, "anchorView"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePicker:Lcom/pspdfkit/ui/settings/SettingsModePicker;

    if-nez v0, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->initializeSettingsPopup()V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePicker:Lcom/pspdfkit/ui/settings/SettingsModePicker;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getScrollMode()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setTransitionMode(Lcom/pspdfkit/configuration/page/PageScrollMode;)V

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePicker:Lcom/pspdfkit/ui/settings/SettingsModePicker;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getLayoutMode()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setPageLayoutMode(Lcom/pspdfkit/configuration/page/PageLayoutMode;)V

    .line 60
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePicker:Lcom/pspdfkit/ui/settings/SettingsModePicker;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getScrollDirection()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setScrollMode(Lcom/pspdfkit/configuration/page/PageScrollDirection;)V

    .line 61
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePicker:Lcom/pspdfkit/ui/settings/SettingsModePicker;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getThemeMode()Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setThemeMode(Lcom/pspdfkit/configuration/theming/ThemeMode;)V

    .line 62
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePicker:Lcom/pspdfkit/ui/settings/SettingsModePicker;

    iget-wide v1, p0, Lcom/pspdfkit/internal/ui/f;->screenTimeoutMillis:J

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setScreenTimeoutMode(J)V

    .line 63
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePicker:Lcom/pspdfkit/ui/settings/SettingsModePicker;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSettingsMenuItemShown()Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/settings/SettingsModePicker;->setItemsVisibility(Ljava/util/EnumSet;)V

    .line 66
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;)V

    return-void
.end method

.method private toggleAnnotationCreationMode()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    goto :goto_0

    .line 4
    :cond_0
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/f;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->clearSelectedAnnotations()Z

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->enterAnnotationCreationMode()V

    .line 8
    :goto_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->updateMenuIcons()V

    return-void
.end method

.method private toggleContentEditMode()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/f;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->enterContentEditingMode()V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->updateMenuIcons()V

    return-void
.end method

.method private toggleSignatureCreationMode()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->isInAnnotationCreationMode:Z

    goto :goto_0

    .line 5
    :cond_0
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/f;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->clearSelectedAnnotations()Z

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    sget-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SIGNATURE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/ui/PdfFragment;->enterAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    const/4 v0, 0x1

    .line 8
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->isInAnnotationCreationMode:Z

    .line 10
    :goto_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->updateMenuIcons()V

    return-void
.end method

.method private toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V
    .locals 3

    const-string v0, "viewType"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    const-wide/16 v1, 0x0

    .line 56
    invoke-virtual {v0, p1, v1, v2}, Lcom/pspdfkit/internal/sm;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)Z

    return-void
.end method

.method private toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)V
    .locals 2

    const-string v0, "viewType"

    const-string v1, "argumentName"

    .line 58
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 109
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 110
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    .line 111
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0, p1, p2, p3}, Lcom/pspdfkit/internal/sm;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)Z

    return-void
.end method

.method private unbindToolbarControllers()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->removeContextualToolbar(Z)V

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationCreationToolbar:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->unbindController()V

    .line 7
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationEditingToolbar:Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;

    if-eqz v0, :cond_2

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->unbindController()V

    .line 10
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentEditingToolbar:Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;

    if-eqz v0, :cond_3

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;->unbindController()V

    .line 13
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->textSelectionToolbar:Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;

    if-eqz v0, :cond_4

    .line 14
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->unbindController()V

    .line 16
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationCreationInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;

    if-eqz v0, :cond_5

    .line 17
    invoke-interface {v0}, Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;->unbindAnnotationCreationController()V

    .line 19
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationEditingInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;

    if-eqz v0, :cond_6

    .line 20
    invoke-interface {v0}, Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;->unbindAnnotationEditingController()V

    :cond_6
    return-void
.end method

.method private updateMenuIcons()V
    .locals 5

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/ui/f$c;->a:[I

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getActiveView()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x4

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eq v0, v4, :cond_4

    if-eq v0, v3, :cond_3

    if-eq v0, v2, :cond_2

    if-eq v0, v1, :cond_5

    .line 15
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->isInAnnotationCreationMode:Z

    if-eqz v0, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    .line 17
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->isInContentEditingMode:Z

    if-eqz v0, :cond_1

    const/4 v1, 0x7

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x6

    goto :goto_0

    :cond_3
    const/4 v1, 0x3

    goto :goto_0

    :cond_4
    const/4 v1, 0x2

    .line 24
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->menuConfiguration:Lcom/pspdfkit/internal/qm;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/qm;->e(I)V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->supportInvalidateOptionsMenu()V

    return-void
.end method

.method private updateTaskDescription()V
    .locals 4

    .line 1
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Landroidx/appcompat/R$attr;->colorPrimary:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/v;->e()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 5
    new-instance v1, Landroid/app/ActivityManager$TaskDescription;

    iget v0, v0, Landroid/util/TypedValue;->data:I

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;II)V

    goto :goto_0

    .line 7
    :cond_0
    new-instance v1, Landroid/app/ActivityManager$TaskDescription;

    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-direct {v1, v2, v2, v0}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    .line 9
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AppCompatActivity;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public OnPageLayoutChange(Lcom/pspdfkit/configuration/page/PageLayoutMode;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-direct {v0, v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->layoutMode(Lcom/pspdfkit/configuration/page/PageLayoutMode;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object p1

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p1

    .line 4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/f;->setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    return-void
.end method

.method public OnScreenTimeoutChange(J)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/ui/f;->setScreenTimeout(J)V

    return-void
.end method

.method public OnScrollDirectionChange(Lcom/pspdfkit/configuration/page/PageScrollDirection;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-direct {v0, v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->scrollDirection(Lcom/pspdfkit/configuration/page/PageScrollDirection;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object p1

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p1

    .line 4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/f;->setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    return-void
.end method

.method public OnScrollModeChange(Lcom/pspdfkit/configuration/page/PageScrollMode;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-direct {v0, v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->scrollMode(Lcom/pspdfkit/configuration/page/PageScrollMode;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object p1

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p1

    .line 4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/f;->setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    return-void
.end method

.method public OnThemeChange(Lcom/pspdfkit/configuration/theming/ThemeMode;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-direct {v0, v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->themeMode(Lcom/pspdfkit/configuration/theming/ThemeMode;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object p1

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p1

    .line 4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/f;->setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    return-void
.end method

.method public attemptPrinting()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->get()Lcom/pspdfkit/document/printing/DocumentPrintManager;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->isPrintingEnabled(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/PdfDocument;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->ensureSharingMenuFragment()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/kr;->performPrint()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->keyEventContract:Lcom/pspdfkit/internal/og;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/og;->a(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public getActiveView()Lcom/pspdfkit/ui/PSPDFKitViews$Type;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getActiveViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    return-object v0
.end method

.method public getActivityState(ZZ)Landroid/os/Bundle;
    .locals 1

    .line 1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2
    invoke-virtual {p0, v0, p1, p2}, Lcom/pspdfkit/internal/ui/f;->onSaveInstanceState(Landroid/os/Bundle;ZZ)V

    return-object v0
.end method

.method public getAnnotationCreationToolbar()Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationCreationToolbar:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationCreationToolbar:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;

    .line 3
    iget v1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarElevation:F

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->annotationCreationToolbar:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;

    return-object v0
.end method

.method public getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    return-object v0
.end method

.method public getDocument()Lcom/pspdfkit/internal/zf;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    return-object v0
.end method

.method public getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentCoordinator:Lcom/pspdfkit/internal/ui/e;

    return-object v0
.end method

.method public getFragment()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    return-object v0
.end method

.method getHostingActivity()Landroidx/appcompat/app/AppCompatActivity;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    return-object v0
.end method

.method public getPageIndex()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v0

    return v0
.end method

.method public getPropertyInspectorCoordinatorLayout()Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->propertyInspectorCoordinatorLayout:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    return-object v0
.end method

.method public getScreenTimeout()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/ui/f;->screenTimeoutMillis:J

    return-wide v0
.end method

.method public getSiblingPageIndex(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->getSiblingPageIndex(I)I

    move-result p1

    return p1
.end method

.method public getUserInterfaceCoordinator()Lcom/pspdfkit/internal/ui/c;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    return-object v0
.end method

.method public getViews()Lcom/pspdfkit/internal/yf;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    return-object v0
.end method

.method public isDocumentInteractionEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->documentInteractionEnabled:Z

    return v0
.end method

.method public isUserInterfaceEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceEnabled:Z

    return v0
.end method

.method synthetic lambda$onUserInterfaceEnabled$1$com-pspdfkit-internal-ui-f(Z)V
    .locals 2

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceEnabled:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->supportInvalidateOptionsMenu()V

    if-nez p1, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->lastEnabledUiState:Landroid/os/Bundle;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_2

    .line 5
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->lastEnabledUiState:Landroid/os/Bundle;

    .line 6
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/f;->saveUserInterfaceState(Landroid/os/Bundle;)V

    goto :goto_0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->lastEnabledUiState:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_1

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/sm;->setDocument(Lcom/pspdfkit/document/PdfDocument;)V

    .line 12
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->lastEnabledUiState:Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/f;->restoreUserInterfaceState(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    .line 13
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->lastEnabledUiState:Landroid/os/Bundle;

    .line 15
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/c;->o(Z)V

    return-void
.end method

.method synthetic lambda$setFragment$2$com-pspdfkit-internal-ui-f(Landroid/view/MenuItem;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/f;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public navigateNextPage()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    return-void

    .line 5
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    .line 6
    :goto_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v3

    sub-int/2addr v3, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v2

    add-int/2addr v2, v1

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(I)V

    :cond_2
    return-void
.end method

.method public navigatePreviousPage()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    return-void

    .line 5
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    .line 6
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v1

    sub-int/2addr v1, v0

    const/4 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(I)V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method public onBackPressed()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->getCurrentlyDisplayedContextualToolbar()Lcom/pspdfkit/ui/toolbar/ContextualToolbar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->onBackPressed()Z

    move-result v0

    return v0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getActiveView()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v1

    check-cast v0, Lcom/pspdfkit/internal/sm;

    const-wide/16 v2, 0x0

    .line 7
    invoke-virtual {v0, v1, v2, v3}, Lcom/pspdfkit/internal/sm;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)Z

    move-result v0

    return v0
.end method

.method public onBindToUserInterfaceCoordinator(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/f;->refreshPropertyInspectorCoordinatorLayout(Lcom/pspdfkit/internal/ui/c;)V

    return-void
.end method

.method public onChangeAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 0

    return-void
.end method

.method public onChangeAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/f;->showAnnotationEditorWhenAppropriate(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V

    return-void
.end method

.method public onChangeAudioPlaybackMode(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V
    .locals 0

    return-void
.end method

.method public onChangeAudioRecordingMode(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V
    .locals 0

    return-void
.end method

.method public onChangeFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 0

    return-void
.end method

.method public synthetic onContentChange(Ljava/util/UUID;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener$-CC;->$default$onContentChange(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;Ljava/util/UUID;)V

    return-void
.end method

.method public synthetic onContentSelectionChange(Ljava/util/UUID;IILcom/pspdfkit/internal/jt;Z)V
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener$-CC;->$default$onContentSelectionChange(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;Ljava/util/UUID;IILcom/pspdfkit/internal/jt;Z)V

    return-void
.end method

.method public onContextualToolbarPositionChanged(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->positionListener:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1, p2, p3}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;->onContextualToolbarPositionChanged(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;)V

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/c;->onContextualToolbarPositionChanged(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->requirePdfParameters()Landroid/os/Bundle;

    move-result-object v0

    if-nez p1, :cond_0

    const-string p1, "activityState"

    .line 5
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    :cond_0
    if-eqz p1, :cond_1

    const-string v1, "PdfActivity.Configuration"

    .line 10
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 13
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    if-nez v1, :cond_3

    const-string v1, "PSPDF.Configuration"

    .line 14
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    if-eqz v1, :cond_2

    goto :goto_0

    .line 16
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "PdfActivity requires a configuration extra!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 21
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    invoke-interface {v1}, Lcom/pspdfkit/internal/bg;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    sget v2, Lcom/pspdfkit/internal/xq;->f:I

    const-string v2, "fragmentManager"

    .line 22
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "com.pspdfkit.ui.dialog.SettingsDialog.FRAGMENT_TAG"

    .line 203
    invoke-virtual {v1, v2}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xq;

    if-eqz v1, :cond_4

    .line 204
    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/xq;->a(Lcom/pspdfkit/internal/zq;)V

    .line 208
    :cond_4
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    sget v2, Lcom/pspdfkit/internal/wl;->b:I

    const-string v2, "context"

    .line 209
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_6

    .line 265
    invoke-static {v1}, Lcom/pspdfkit/internal/wl;->a(Landroid/content/Context;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 266
    invoke-virtual {v1}, Lio/reactivex/rxjava3/core/Completable;->onErrorComplete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 267
    invoke-virtual {v1}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V

    .line 268
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_5

    goto :goto_1

    .line 269
    :cond_5
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;

    const-string v0, "PSPDFKit is not initialized!"

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 270
    :cond_6
    :goto_1
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getThemeMode()Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/configuration/theming/ThemeMode;->NIGHT:Lcom/pspdfkit/configuration/theming/ThemeMode;

    const/4 v3, -0x1

    if-ne v1, v2, :cond_8

    .line 272
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getDarkTheme()I

    move-result v1

    if-eq v1, v3, :cond_7

    .line 274
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getDarkTheme()I

    move-result v2

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->setTheme(I)V

    goto :goto_2

    .line 276
    :cond_7
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getManifestTheme()I

    move-result v1

    if-nez v1, :cond_a

    .line 281
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    sget v2, Lcom/pspdfkit/R$style;->PSPDFKit_Theme_Dark:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->setTheme(I)V

    goto :goto_2

    .line 286
    :cond_8
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getTheme()I

    move-result v1

    if-eq v1, v3, :cond_9

    .line 288
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getTheme()I

    move-result v2

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->setTheme(I)V

    goto :goto_2

    .line 290
    :cond_9
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getManifestTheme()I

    move-result v1

    if-nez v1, :cond_a

    .line 295
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    sget v2, Lcom/pspdfkit/R$style;->PSPDFKit_Theme_Default:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->setTheme(I)V

    .line 302
    :cond_a
    :goto_2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->updateTaskDescription()V

    .line 306
    :try_start_1
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v1}, Lcom/pspdfkit/internal/fv;->a(Landroidx/appcompat/app/AppCompatActivity;)V
    :try_end_1
    .catch Lcom/pspdfkit/exceptions/InvalidThemeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 314
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$style;->PSPDFKit_DefaultStyles:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 317
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getLayout()I

    move-result v2

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/f;->rootView:Landroid/view/View;

    .line 319
    sget v2, Lcom/pspdfkit/R$id;->pspdf__toolbar_coordinator:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    if-eqz v1, :cond_19

    .line 324
    invoke-virtual {v1, p0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->setOnContextualToolbarPositionListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;)V

    .line 328
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v1}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$dimen;->pspdf__toolbar_elevation:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarElevation:F

    .line 331
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->rootView:Landroid/view/View;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__toolbar_main:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/widget/Toolbar;

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/f;->toolbar:Landroidx/appcompat/widget/Toolbar;

    if-eqz v1, :cond_18

    .line 336
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v5, Lcom/pspdfkit/R$drawable;->pspdf__ic_more:I

    invoke-static {v2, v5}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/appcompat/widget/Toolbar;->setOverflowIcon(Landroid/graphics/drawable/Drawable;)V

    .line 337
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->rootView:Landroid/view/View;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__inspector_coordinator:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/f;->propertyInspectorCoordinatorLayout:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    if-eqz v1, :cond_17

    .line 344
    new-instance v1, Lcom/pspdfkit/internal/qm;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v5, p0, Lcom/pspdfkit/internal/ui/f;->pdfUi:Lcom/pspdfkit/ui/PdfUi;

    invoke-interface {v5}, Lcom/pspdfkit/ui/PdfUi;->getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v5

    invoke-direct {v1, v2, v5}, Lcom/pspdfkit/internal/qm;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/f;->menuConfiguration:Lcom/pspdfkit/internal/qm;

    .line 345
    new-instance v2, Lcom/pspdfkit/internal/pm;

    iget-object v5, p0, Lcom/pspdfkit/internal/ui/f;->pdfUi:Lcom/pspdfkit/ui/PdfUi;

    invoke-direct {v2, v1, v5, v5}, Lcom/pspdfkit/internal/pm;-><init>(Lcom/pspdfkit/internal/qm;Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/PdfUi;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/ui/f;->menuManager:Lcom/pspdfkit/internal/pm;

    .line 347
    new-instance v1, Lcom/pspdfkit/internal/ui/b;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ui/b;-><init>(Lcom/pspdfkit/internal/ui/f;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/f;->actionResolver:Lcom/pspdfkit/internal/ui/b;

    .line 349
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->rootView:Landroid/view/View;

    invoke-interface {v1, v2}, Lcom/pspdfkit/internal/bg;->setPdfView(Landroid/view/View;)V

    const-string v1, "PSPDF.PdfFragmentTag"

    const-string v2, "PSPDFKit.Fragment"

    .line 351
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 352
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->rootView:Landroid/view/View;

    sget v5, Lcom/pspdfkit/R$id;->pspdf__activity_fragment_container:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_16

    .line 357
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->isUsingCustomFragmentTag()Z

    move-result v2

    if-eqz v2, :cond_c

    if-eqz p1, :cond_b

    const-string v2, "PdfActivity.FragmentContainerId"

    .line 366
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/ui/f;->fragmentContainerId:I

    goto :goto_3

    .line 369
    :cond_b
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/ui/f;->fragmentContainerId:I

    .line 375
    :goto_3
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getHostingActivity()Landroidx/appcompat/app/AppCompatActivity;

    move-result-object v5

    invoke-direct {v2, v5}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 376
    iget v5, p0, Lcom/pspdfkit/internal/ui/f;->fragmentContainerId:I

    invoke-virtual {v2, v5}, Landroid/view/View;->setId(I)V

    .line 377
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/f;->rootView:Landroid/view/View;

    sget v6, Lcom/pspdfkit/R$id;->pspdf__activity_fragment_container:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout;

    .line 382
    invoke-virtual {v5, v2, v3, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    goto :goto_4

    .line 385
    :cond_c
    sget v2, Lcom/pspdfkit/R$id;->pspdf__activity_fragment_container:I

    iput v2, p0, Lcom/pspdfkit/internal/ui/f;->fragmentContainerId:I

    :goto_4
    if-nez p1, :cond_e

    .line 390
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->page()I

    move-result p1

    if-eqz p1, :cond_d

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->page()I

    move-result v3

    :cond_d
    iput v3, p0, Lcom/pspdfkit/internal/ui/f;->pendingInitialPage:I

    .line 392
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/f;->setDocument(Landroid/os/Bundle;)V

    goto :goto_6

    :cond_e
    const-string v2, "PdfActivity.PendingInitialPage"

    .line 395
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/ui/f;->pendingInitialPage:I

    .line 398
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    invoke-interface {v2}, Lcom/pspdfkit/internal/bg;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/PdfFragment;

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    .line 399
    sget-object v2, Lcom/pspdfkit/internal/ui/f;->retainedDocument:Lcom/pspdfkit/document/PdfDocument;

    if-nez v2, :cond_f

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 401
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 403
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/f;->setFragment(Lcom/pspdfkit/ui/PdfFragment;)V

    goto :goto_5

    .line 405
    :cond_f
    sget-object v1, Lcom/pspdfkit/internal/ui/f;->retainedDocument:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v1, :cond_10

    .line 407
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ui/f;->setDocument(Lcom/pspdfkit/document/PdfDocument;)V

    goto :goto_5

    .line 408
    :cond_10
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    if-nez v1, :cond_11

    .line 410
    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/ui/f;->setFragment(Lcom/pspdfkit/ui/PdfFragment;)V

    goto :goto_5

    .line 411
    :cond_11
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v1

    if-eqz v1, :cond_12

    .line 413
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/f;->setDocument(Lcom/pspdfkit/document/PdfDocument;)V

    goto :goto_5

    .line 416
    :cond_12
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/f;->setDocument(Landroid/os/Bundle;)V

    .line 420
    :goto_5
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/f;->setActivityState(Landroid/os/Bundle;)V

    .line 423
    :goto_6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getViews()Lcom/pspdfkit/internal/yf;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getOutlineView()Lcom/pspdfkit/ui/PdfOutlineView;

    move-result-object p1

    if-eqz p1, :cond_13

    .line 425
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfOutlineView;->addOnDocumentInfoViewSaveListener(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V

    .line 428
    :cond_13
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getViews()Lcom/pspdfkit/internal/yf;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getDocumentInfoView()Lcom/pspdfkit/ui/PdfDocumentInfoView;

    move-result-object p1

    if-eqz p1, :cond_14

    .line 430
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfOutlineView;->addOnDocumentInfoViewSaveListener(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V

    .line 433
    :cond_14
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object p1

    if-eqz p1, :cond_15

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getTabBarHidingMode()Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;->HIDE:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    if-eq p1, v0, :cond_15

    .line 434
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentCoordinator:Lcom/pspdfkit/internal/ui/e;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->bindToDocumentCoordinator(Lcom/pspdfkit/ui/DocumentCoordinator;)V

    .line 437
    :cond_15
    sput-object v4, Lcom/pspdfkit/internal/ui/f;->retainedDocument:Lcom/pspdfkit/document/PdfDocument;

    .line 439
    new-instance p1, Lcom/pspdfkit/internal/og;

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-direct {p1, p0, v0}, Lcom/pspdfkit/internal/og;-><init>(Lcom/pspdfkit/internal/og$a;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->keyEventContract:Lcom/pspdfkit/internal/og;

    .line 441
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->propertyInspectorCoordinatorLayout:Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;

    new-instance v0, Lcom/pspdfkit/internal/ui/f$b;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/f$b;-><init>(Lcom/pspdfkit/internal/ui/f;)V

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayout;->addPropertyInspectorLifecycleListener(Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;)V

    return-void

    .line 442
    :cond_16
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "The activity layout is missing the required ViewGroup with id \'R.id.pspdf__activity_fragment_container\'."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 443
    :cond_17
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "The activity layout is missing the required PropertyInspectorCoordinatorLayout with id \'R.id.pspdf__inspector_coordinator\'."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 444
    :cond_18
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "The activity is missing the required Toolbar widget with id \'R.id.pspdf__toolbar_main\'."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 445
    :cond_19
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "The activity layout is missing the required ToolbarCoordinatorLayout with id \'R.id.pspdf__toolbar_coordinator\'."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_0
    move-exception p1

    .line 446
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 447
    throw p1

    :catch_1
    move-exception p1

    .line 448
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 449
    throw p1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->isUsingCustomFragmentTag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->toolbar:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p1}, Landroidx/appcompat/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object p1

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->menuManager:Lcom/pspdfkit/internal/pm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/pm;->a(Landroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method public onDestroy()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->b()V

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_1

    .line 7
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/f;->removeListeners(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 10
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    if-eqz v0, :cond_2

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/tabs/PdfTabBar;->unbindDocumentCoordinator()V

    .line 15
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentCoordinator:Lcom/pspdfkit/internal/ui/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/e;->a(Z)V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->settingsModePopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_3

    .line 21
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 25
    :cond_3
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->unbindToolbarControllers()V

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_4

    .line 28
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/zf;->b(Lcom/pspdfkit/internal/zf$f;)V

    :cond_4
    return-void
.end method

.method public onDocumentClick()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->isInSpecialMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getUserInterfaceCoordinator()Lcom/pspdfkit/internal/ui/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->r()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onDocumentInfoChangesSaved(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/f;->refreshDocumentTitle(Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Landroidx/appcompat/app/AppCompatActivity;->supportInvalidateOptionsMenu()V

    return-void
.end method

.method public onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 8

    .line 1
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/internal/zf;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    .line 2
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/zf;->a(Lcom/pspdfkit/internal/zf$f;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentCoordinator:Lcom/pspdfkit/internal/ui/e;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/e;->b(Lcom/pspdfkit/document/PdfDocument;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activityListener:Lcom/pspdfkit/listeners/PdfActivityListener;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-interface {v0, v1, p1}, Lcom/pspdfkit/listeners/PdfActivityListener;->onSetActivityTitle(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/PdfDocument;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/sm;->setDocument(Lcom/pspdfkit/document/PdfDocument;)V

    .line 9
    iget v0, p0, Lcom/pspdfkit/internal/ui/f;->pendingInitialPage:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_1

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/ag;->isLastViewedPageRestorationActiveAndIsConfigChange()Z

    move-result v0

    if-nez v0, :cond_0

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    iget v2, p0, Lcom/pspdfkit/internal/ui/f;->pendingInitialPage:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(IZ)V

    .line 17
    :cond_0
    iput v1, p0, Lcom/pspdfkit/internal/ui/f;->pendingInitialPage:I

    .line 20
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getThumbnailGridView()Lcom/pspdfkit/ui/PdfThumbnailGrid;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getThumbnailGridView()Lcom/pspdfkit/ui/PdfThumbnailGrid;

    move-result-object v0

    .line 22
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/f;->registerDocumentEditingToolbarListener(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V

    .line 26
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isRedactionUiEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 27
    new-instance v0, Lcom/pspdfkit/internal/so;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    .line 29
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isUndoEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/vu;

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :goto_0
    move-object v3, v1

    iget-object v4, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    .line 31
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v5

    new-instance v6, Lcom/pspdfkit/internal/x7;

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    .line 33
    invoke-static {}, Lcom/pspdfkit/internal/t$a;->a()Lcom/pspdfkit/internal/t;

    move-result-object v1

    invoke-direct {v6, p1, v1}, Lcom/pspdfkit/internal/x7;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/internal/t;)V

    iget-object v7, p0, Lcom/pspdfkit/internal/ui/f;->pdfUi:Lcom/pspdfkit/ui/PdfUi;

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/internal/so;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/internal/vu;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/annotations/AnnotationProvider;Lcom/pspdfkit/internal/x7;Lcom/pspdfkit/ui/PdfUi;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->redactionApplicator:Lcom/pspdfkit/internal/so;

    .line 35
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->redactionApplicator:Lcom/pspdfkit/internal/so;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/redaction/RedactionView;->setListener(Lcom/pspdfkit/ui/redaction/RedactionView$RedactionViewListener;)V

    .line 38
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isJavaScriptEnabled()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 39
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activityJsPlatformDelegate:Lcom/pspdfkit/internal/lg;

    check-cast p1, Lcom/pspdfkit/internal/ig;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ig;->a(Lcom/pspdfkit/internal/lg;)V

    .line 42
    :cond_5
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Landroidx/appcompat/app/AppCompatActivity;->supportInvalidateOptionsMenu()V

    return-void
.end method

.method public onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method public onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V
    .locals 0

    return-void
.end method

.method public onEnterAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getAnnotationCreationInspectorController()Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;->bindAnnotationCreationController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getAnnotationCreationToolbar()Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->bindController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getAnnotationCreationToolbar()Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->displayContextualToolbar(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;Z)V

    .line 11
    :cond_1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->isInAnnotationCreationMode:Z

    .line 12
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->updateMenuIcons()V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/c;->q(Z)V

    return-void
.end method

.method public onEnterAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getAnnotationEditingInspectorController()Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;->bindAnnotationEditingController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getAnnotationEditingToolbar()Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->bindController(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getAnnotationEditingToolbar()Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->displayContextualToolbar(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;Z)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->showUserInterface()V

    .line 10
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/c;->c(Z)V

    .line 11
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/f;->showAnnotationEditorWhenAppropriate(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V

    return-void
.end method

.method public onEnterAudioPlaybackMode(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/audio/AudioView;->bindController(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V

    :cond_0
    return-void
.end method

.method public onEnterAudioRecordingMode(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/audio/AudioView;->bindController(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V

    :cond_0
    return-void
.end method

.method public onEnterContentEditingMode(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V
    .locals 3

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/f;->isInContentEditingMode:Z

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getContentEditingToolBar()Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 4
    invoke-virtual {v1, p1}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->bindController(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    if-eqz v2, :cond_0

    .line 7
    invoke-virtual {v2, v1, v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->displayContextualToolbar(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;Z)V

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/c;->showUserInterface()V

    .line 10
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/c;->c(Z)V

    .line 12
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->currentContentEditingController:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 15
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getContentEditingInspectorController()Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 17
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;->bindContentEditingController(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V

    :cond_1
    return-void
.end method

.method public onEnterDocumentEditingMode(Lcom/pspdfkit/ui/special_mode/controller/DocumentEditingController;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getDocumentEditingToolbar()Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;->bindController(Lcom/pspdfkit/ui/special_mode/controller/DocumentEditingController;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    if-eqz p1, :cond_0

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getDocumentEditingToolbar()Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->displayContextualToolbar(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;Z)V

    :cond_0
    return-void
.end method

.method public onEnterFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getActiveViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-eq v0, v1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    const-wide/16 v2, 0x0

    .line 3
    invoke-virtual {v0, v1, v2, v3}, Lcom/pspdfkit/internal/sm;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)Z

    .line 4
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getFormEditingInspectorController()Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v2, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0, v2}, Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;->setFormEditingBarEnabled(Z)V

    .line 7
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;->bindFormEditingController(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    .line 9
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/forms/FormEditingBar;->bindController(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    .line 12
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/ui/c;->c(Z)V

    return-void
.end method

.method public onEnterTextSelectionMode(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->getTextSelectionToolbar()Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->bindController(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 6
    invoke-virtual {v1, v0, v2}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->displayContextualToolbar(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;Z)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->showUserInterface()V

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/ui/c;->c(Z)V

    .line 12
    :cond_1
    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->setOnSearchSelectedTextListener(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController$OnSearchSelectedTextListener;)V

    return-void
.end method

.method public onExitAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 2
    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->removeContextualToolbar(Z)V

    .line 4
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->annotationCreationToolbar:Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;

    if-eqz p1, :cond_1

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;->unbindController()V

    .line 8
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->annotationCreationInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;

    if-eqz p1, :cond_2

    .line 9
    invoke-interface {p1}, Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;->unbindAnnotationCreationController()V

    :cond_2
    const/4 p1, 0x0

    .line 12
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/f;->isInAnnotationCreationMode:Z

    .line 13
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->updateMenuIcons()V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/c;->q(Z)V

    return-void
.end method

.method public onExitAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->removeContextualToolbar(Z)V

    .line 4
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->annotationEditingToolbar:Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;

    if-eqz p1, :cond_1

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/AnnotationEditingToolbar;->unbindController()V

    .line 7
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->annotationEditingInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;

    if-eqz p1, :cond_2

    .line 8
    invoke-interface {p1}, Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;->unbindAnnotationEditingController()V

    .line 10
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/c;->s(Z)V

    return-void
.end method

.method public onExitAudioPlaybackMode(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/audio/AudioView;->unbindController()V

    :cond_0
    return-void
.end method

.method public onExitAudioRecordingMode(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/audio/AudioView;->unbindController()V

    :cond_0
    return-void
.end method

.method public onExitContentEditingMode(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->contentEditingToolBar:Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;

    if-eqz p1, :cond_2

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->removeContextualToolbar(Z)V

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->unbindController()V

    .line 8
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->contentEditingToolBar:Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/ContentEditingToolbar;->unbindController()V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/c;->s(Z)V

    .line 12
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->contentEditingInspectorController:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

    if-eqz p1, :cond_3

    .line 13
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;->unbindContentEditingController()V

    :cond_3
    const/4 p1, 0x0

    .line 16
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->currentContentEditingController:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    const/4 p1, 0x0

    .line 17
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/f;->isInContentEditingMode:Z

    .line 18
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->updateMenuIcons()V

    return-void
.end method

.method public onExitDocumentEditingMode(Lcom/pspdfkit/ui/special_mode/controller/DocumentEditingController;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 2
    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->removeContextualToolbar(Z)V

    .line 4
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->documentEditingToolbar:Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;

    if-eqz p1, :cond_1

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/DocumentEditingToolbar;->unbindController()V

    :cond_1
    return-void
.end method

.method public onExitFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->formEditingInspectorController:Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/forms/FormEditingInspectorController;->unbindFormEditingController()V

    .line 4
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/forms/FormEditingBar;->unbindController()V

    .line 7
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/c;->s(Z)V

    return-void
.end method

.method public onExitTextSelectionMode(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;->setOnSearchSelectedTextListener(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController$OnSearchSelectedTextListener;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->textSelectionToolbar:Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;

    if-eqz p1, :cond_1

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 5
    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->removeContextualToolbar(Z)V

    .line 7
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->textSelectionToolbar:Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/toolbar/TextSelectionToolbar;->unbindController()V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/c;->s(Z)V

    :cond_1
    return-void
.end method

.method public onFinishEditingContentBlock(Ljava/util/UUID;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->currentlyEditedBlockID:Ljava/util/UUID;

    if-eq p1, v0, :cond_0

    if-nez v0, :cond_1

    .line 2
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->hideContentEditingStylingBar()V

    const/4 p1, 0x0

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->currentlyEditedBlockID:Ljava/util/UUID;

    :cond_1
    return-void
.end method

.method public onHide(Landroid/view/View;)V
    .locals 2

    .line 1
    instance-of p1, p1, Lcom/pspdfkit/ui/search/PdfSearchViewInline;

    if-eqz p1, :cond_1

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->showActions()V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Landroidx/appcompat/app/AppCompatActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;

    move-result-object p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 5
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 6
    invoke-virtual {p1, v1}, Landroidx/appcompat/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 8
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/ui/c;->p(Z)V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/c;->s(Z)V

    .line 11
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->updateMenuIcons()V

    return-void
.end method

.method public onInternalDocumentSaveFailed(Lcom/pspdfkit/internal/zf;Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method public onInternalDocumentSaved(Lcom/pspdfkit/internal/zf;)V
    .locals 0

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .line 1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    .line 2
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_THUMBNAIL_GRID:I

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    .line 3
    sget-object p1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_THUMBNAIL_GRID:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/f;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    goto :goto_1

    .line 4
    :cond_0
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SEARCH:I

    if-ne p1, v0, :cond_2

    .line 5
    sget-object p1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_SEARCH:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSearchType()I

    move-result v0

    if-ne v0, v1, :cond_1

    const-wide/16 v2, 0x12c

    goto :goto_0

    :cond_1
    const-wide/16 v2, 0x0

    .line 8
    :goto_0
    invoke-direct {p0, p1, v2, v3}, Lcom/pspdfkit/internal/ui/f;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)V

    goto :goto_1

    .line 13
    :cond_2
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_OUTLINE:I

    if-ne p1, v0, :cond_3

    .line 14
    sget-object p1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_OUTLINE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/f;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    goto :goto_1

    .line 15
    :cond_3
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_DOCUMENT_INFO:I

    if-ne p1, v0, :cond_4

    .line 16
    sget-object p1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_DOCUMENT_INFO:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/f;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    goto :goto_1

    .line 17
    :cond_4
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_READER_VIEW:I

    if-ne p1, v0, :cond_5

    .line 18
    sget-object p1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_READER:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/f;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    goto :goto_1

    .line 19
    :cond_5
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_ANNOTATIONS:I

    if-ne p1, v0, :cond_6

    .line 20
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->toggleAnnotationCreationMode()V

    goto :goto_1

    .line 21
    :cond_6
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SIGNATURE:I

    if-ne p1, v0, :cond_7

    .line 22
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->toggleSignatureCreationMode()V

    goto :goto_1

    .line 23
    :cond_7
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_CONTENT:I

    if-ne p1, v0, :cond_8

    .line 24
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->toggleContentEditMode()V

    goto :goto_1

    .line 25
    :cond_8
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SETTINGS:I

    if-ne p1, v0, :cond_9

    .line 26
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->showSettingsDialog()V

    goto :goto_1

    .line 27
    :cond_9
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SHARE:I

    if-ne p1, v0, :cond_a

    .line 28
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->showSharingMenu()V

    :goto_1
    return v1

    :cond_a
    const/4 p1, 0x0

    return p1
.end method

.method public onPageBindingChanged()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/ui/f;->setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Z)V

    return-void
.end method

.method public onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    return-void
.end method

.method public onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    if-nez p5, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->isInSpecialMode()Z

    move-result p1

    if-nez p1, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getUserInterfaceCoordinator()Lcom/pspdfkit/internal/ui/c;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/c;->r()V

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onPageRotationOffsetChanged()V
    .locals 0

    return-void
.end method

.method public onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    return-void
.end method

.method public onPause()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/f;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->isUsingCustomFragmentTag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->toolbar:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p1}, Landroidx/appcompat/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object p1

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->menuConfiguration:Lcom/pspdfkit/internal/qm;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->isUserInterfaceEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/qm;->a(Lcom/pspdfkit/internal/zf;)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->menuManager:Lcom/pspdfkit/internal/pm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/pm;->b(Landroid/view/Menu;)V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSearchType()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/ui/search/PdfSearchView;->isShown()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 16
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->hideActions()V

    .line 22
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->toolbar:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p1}, Landroidx/appcompat/widget/Toolbar;->getOverflowIcon()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 24
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->menuConfiguration:Lcom/pspdfkit/internal/qm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/qm;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    :cond_3
    return v0
.end method

.method public onResume()V
    .locals 9

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/ui/f;->screenTimeoutMillis:J

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/ui/f;->setScreenTimeout(J)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/internal/bg;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    iget-object v4, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuListener:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

    iget-object v5, p0, Lcom/pspdfkit/internal/ui/f;->documentSharingDialogFactory:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

    iget-object v6, p0, Lcom/pspdfkit/internal/ui/f;->documentPrintDialogFactory:Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;

    iget-object v7, p0, Lcom/pspdfkit/internal/ui/f;->sharingOptionsProvider:Lcom/pspdfkit/document/sharing/SharingOptionsProvider;

    iget-object v8, p0, Lcom/pspdfkit/internal/ui/f;->printOptionsProvider:Lcom/pspdfkit/document/printing/PrintOptionsProvider;

    .line 4
    invoke-static/range {v1 .. v8}, Lcom/pspdfkit/internal/kr;->a(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;Lcom/pspdfkit/document/sharing/SharingOptionsProvider;Lcom/pspdfkit/document/printing/PrintOptionsProvider;)Lcom/pspdfkit/internal/kr;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1
    invoke-virtual {p0, p1, v0, v1}, Lcom/pspdfkit/internal/ui/f;->onSaveInstanceState(Landroid/os/Bundle;ZZ)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;ZZ)V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->lastEnabledUiState:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->lastEnabledUiState:Landroid/os/Bundle;

    const-string v1, "PdfActivity.LastEnabledUiState"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 7
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 8
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/f;->saveUserInterfaceState(Landroid/os/Bundle;)V

    const-string v1, "PdfActivity.UiState"

    .line 9
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    const-string v1, "PdfActivity.Configuration"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 13
    iget v0, p0, Lcom/pspdfkit/internal/ui/f;->pendingInitialPage:I

    const-string v1, "PdfActivity.PendingInitialPage"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 17
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getState()Landroid/os/Bundle;

    move-result-object p2

    const-string v0, "PdfActivity.FragmentState"

    invoke-virtual {p1, v0, p2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    if-eqz p3, :cond_2

    .line 22
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 23
    iget-object p3, p0, Lcom/pspdfkit/internal/ui/f;->documentCoordinator:Lcom/pspdfkit/internal/ui/e;

    invoke-virtual {p3, p2}, Lcom/pspdfkit/internal/ui/e;->b(Landroid/os/Bundle;)V

    const-string p3, "PdfActivity.PdfDocumentCoordinatorState"

    .line 24
    invoke-virtual {p1, p3, p2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 28
    :cond_2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getScreenTimeout()J

    move-result-wide p2

    const-string v0, "PdfUiImpl.ScreenTimeout"

    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 31
    iget p2, p0, Lcom/pspdfkit/internal/ui/f;->fragmentContainerId:I

    const-string p3, "PdfActivity.FragmentContainerId"

    invoke-virtual {p1, p3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public onSearchSelectedText(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->showUserInterface()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->isDisplayed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_SEARCH:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/f;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 9
    invoke-interface {v0, p1, v1}, Lcom/pspdfkit/ui/search/PdfSearchView;->setInputFieldText(Ljava/lang/String;Z)V

    :cond_1
    return-void
.end method

.method public onSetActivityTitle(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/c;->s()V

    :cond_0
    return-void
.end method

.method public onSettingsClose()V
    .locals 0

    return-void
.end method

.method public onSettingsSave(Lcom/pspdfkit/internal/ar;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ar;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/ui/f;->setScreenTimeout(J)V

    .line 2
    new-instance v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/PdfConfiguration;)V

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ar;->c()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->scrollDirection(Lcom/pspdfkit/configuration/page/PageScrollDirection;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    move-result-object v0

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ar;->d()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->scrollMode(Lcom/pspdfkit/configuration/page/PageScrollMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    move-result-object v0

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ar;->a()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->layoutMode(Lcom/pspdfkit/configuration/page/PageLayoutMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    move-result-object v0

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ar;->e()Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->themeMode(Lcom/pspdfkit/configuration/theming/ThemeMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->build()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    .line 9
    new-instance v1, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-direct {v1, v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    .line 10
    invoke-virtual {v1, v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->configuration(Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object v0

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ar;->e()Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->themeMode(Lcom/pspdfkit/configuration/theming/ThemeMode;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object p1

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p1

    .line 13
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/f;->setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    return-void
.end method

.method public onShow(Landroid/view/View;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->showUserInterface()V

    .line 8
    instance-of v0, p1, Lcom/pspdfkit/ui/search/PdfSearchViewInline;

    const/4 v1, 0x1

    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/pspdfkit/ui/search/PdfSearchViewLazy;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSearchType()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 11
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->hideActions()V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    .line 14
    invoke-virtual {v0, v2}, Landroidx/appcompat/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 15
    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 16
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 17
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 19
    :cond_2
    new-instance v2, Landroidx/appcompat/app/ActionBar$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v3}, Landroidx/appcompat/app/ActionBar$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v2}, Landroidx/appcompat/app/ActionBar;->setCustomView(Landroid/view/View;Landroidx/appcompat/app/ActionBar$LayoutParams;)V

    .line 21
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/c;->p(Z)V

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/c;->c(Z)V

    .line 25
    :cond_4
    instance-of v0, p1, Lcom/pspdfkit/ui/PdfThumbnailGrid;

    if-eqz v0, :cond_5

    .line 26
    check-cast p1, Lcom/pspdfkit/ui/PdfThumbnailGrid;

    .line 27
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/f;->registerDocumentEditingToolbarListener(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V

    .line 30
    :cond_5
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->updateMenuIcons()V

    return-void
.end method

.method public onStart()V
    .locals 4

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->b()Lcom/pspdfkit/internal/k;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    .line 3
    invoke-virtual {v2}, Lcom/pspdfkit/internal/ui/c;->d()Lcom/pspdfkit/internal/ui/a;

    move-result-object v2

    .line 4
    const-class v3, Lcom/pspdfkit/internal/ui/a;

    invoke-virtual {v0, v1, v2, v3}, Lcom/pspdfkit/internal/k;->a(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/internal/ui/a;Ljava/lang/Class;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->p()V

    :cond_0
    return-void
.end method

.method public onStartEditingContentBlock(Ljava/util/UUID;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->currentlyEditedBlockID:Ljava/util/UUID;

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->showContentEditingStylingBar()V

    return-void
.end method

.method public onStop()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->b()Lcom/pspdfkit/internal/k;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/k;->a(Landroidx/appcompat/app/AppCompatActivity;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    if-eqz v0, :cond_0

    .line 3
    iget-object v1, v0, Lcom/pspdfkit/internal/ui/c;->k:Lcom/pspdfkit/internal/pg$c;

    if-eqz v1, :cond_0

    .line 4
    invoke-virtual {v1}, Lcom/pspdfkit/internal/pg$c;->d()V

    const/4 v1, 0x0

    .line 5
    iput-object v1, v0, Lcom/pspdfkit/internal/ui/c;->k:Lcom/pspdfkit/internal/pg$c;

    :cond_0
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    const/16 v0, 0xf

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 1
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->documentCoordinator:Lcom/pspdfkit/internal/ui/e;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/e;->a(Z)V

    :goto_0
    return-void
.end method

.method public onUserInteraction()V
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/ui/f;->screenTimeoutMillis:J

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/ui/f;->setScreenTimeout(J)V

    return-void
.end method

.method public onUserInterfaceEnabled(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceEnabledRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/ui/f;Z)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceEnabledRunnable:Ljava/lang/Runnable;

    if-nez p1, :cond_1

    .line 27
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->handler:Landroid/os/Handler;

    const-wide/16 v1, 0x64

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 29
    :cond_1
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void
.end method

.method public onUserInterfaceViewModeChanged(Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_HIDDEN:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    .line 2
    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getActiveViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-eq p1, v0, :cond_0

    .line 3
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/f;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    :cond_0
    return-void
.end method

.method public onUserInterfaceVisibilityChanged(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/f;->refreshPropertyInspectorCoordinatorLayout(Lcom/pspdfkit/internal/ui/c;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activityListener:Lcom/pspdfkit/listeners/PdfActivityListener;

    invoke-interface {v0, p1}, Lcom/pspdfkit/listeners/PdfActivityListener;->onUserInterfaceVisibilityChanged(Z)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/c;->m(Z)V

    :cond_0
    return-void
.end method

.method public performPrint(Lcom/pspdfkit/document/printing/PrintOptions;)V
    .locals 4

    const-string v0, "printOptions"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    sget-object v1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    const-wide/16 v2, 0x0

    .line 56
    invoke-virtual {v0, v1, v2, v3}, Lcom/pspdfkit/internal/sm;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)Z

    .line 57
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->ensureSharingMenuFragment()V

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    new-instance v1, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda1;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/document/printing/PrintOptions;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V

    .line 60
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/kr;->performPrint()V

    return-void
.end method

.method public refreshDocumentTitle(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->pdfUi:Lcom/pspdfkit/ui/PdfUi;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-interface {v0, v1, p1}, Lcom/pspdfkit/listeners/PdfActivityListener;->onSetActivityTitle(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/PdfDocument;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->B()Z

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentCoordinator:Lcom/pspdfkit/internal/ui/e;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/e;->a(Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method protected removeListeners(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 1

    .line 1
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V

    .line 2
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeOnTextSelectionModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionModeChangeListener;)V

    .line 3
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V

    .line 4
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeOnFormElementEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V

    .line 5
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeOnContentEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;)V

    .line 6
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeOnContentEditingContentChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;)V

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getAudioModeManager()Lcom/pspdfkit/ui/audio/AudioModeManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/audio/AudioModeListeners;->removeAudioPlaybackModeChangeListener(Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioPlaybackModeChangeListener;)V

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getAudioModeManager()Lcom/pspdfkit/ui/audio/AudioModeManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/audio/AudioModeListeners;->removeAudioRecordingModeChangeListener(Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioRecordingModeChangeListener;)V

    .line 9
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/ag;->removeUserInterfaceListener(Lcom/pspdfkit/internal/ev;)V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->actionResolver:Lcom/pspdfkit/internal/ui/b;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->removeDocumentActionListener(Lcom/pspdfkit/document/DocumentActionListener;)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activityListener:Lcom/pspdfkit/listeners/PdfActivityListener;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->removeDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->onAnnotationSelectedListenerAdapter:Lcom/pspdfkit/ui/special_mode/manager/OnAnnotationSelectedListenerAdapter;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->removeOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentScrollListener:Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->removeDocumentScrollListener(Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;)V

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    if-eqz p1, :cond_0

    .line 17
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activityJsPlatformDelegate:Lcom/pspdfkit/internal/lg;

    check-cast p1, Lcom/pspdfkit/internal/ig;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ig;->b(Lcom/pspdfkit/internal/lg;)V

    :cond_0
    return-void
.end method

.method protected requirePdfParameters()Landroid/os/Bundle;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    invoke-interface {v0}, Lcom/pspdfkit/internal/bg;->getPdfParameters()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PSPDF.Configuration"

    const-string v2, "PSPDF.DocumentDescriptors"

    if-eqz v0, :cond_1

    .line 3
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    return-object v0

    .line 5
    :cond_1
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v0, :cond_3

    .line 9
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "- Neither file paths nor data providers were set.\n"

    .line 10
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    :cond_2
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "- No configuration was passed.\n"

    .line 13
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    const-string v0, "- Extras bundle was missing entirely.\n"

    .line 14
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    :cond_4
    :goto_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PdfActivity was not initialized with proper arguments:\n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setActivityState(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "PdfActivity.LastEnabledUiState"

    .line 1
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->lastEnabledUiState:Landroid/os/Bundle;

    const-string v0, "PdfActivity.PendingInitialPage"

    .line 4
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/ui/f;->pendingInitialPage:I

    const-string v0, "PdfActivity.PdfDocumentCoordinatorState"

    .line 7
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->documentCoordinator:Lcom/pspdfkit/internal/ui/e;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/e;->a(Landroid/os/Bundle;)V

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_1

    const-string v0, "PdfActivity.FragmentState"

    .line 14
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/ui/PdfFragment;->setState(Landroid/os/Bundle;)V

    :cond_1
    const-string v0, "PdfActivity.UiState"

    .line 21
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 23
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/f;->restoreUserInterfaceState(Landroid/os/Bundle;)V

    :cond_2
    const-wide/16 v0, 0x0

    const-string v2, "PdfUiImpl.ScreenTimeout"

    .line 26
    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/ui/f;->setScreenTimeout(J)V

    return-void
.end method

.method public setAnnotationCreationInspectorController(Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;)V
    .locals 2

    const-string v0, "annotationCreationInspectorController"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->annotationCreationInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationCreationInspectorController;

    return-void
.end method

.method public setAnnotationEditingInspectorController(Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;)V
    .locals 2

    const-string v0, "annotationEditingInspectorController"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->annotationEditingInspectorController:Lcom/pspdfkit/ui/inspector/annotation/AnnotationEditingInspectorController;

    return-void
.end method

.method public setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 2

    const-string v0, "configuration"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/ui/f;->setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Z)V

    return-void
.end method

.method protected setDocument(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "PSPDF.DocumentDescriptors"

    .line 1
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v1, "PSPDF.VisibleDocumentDescriptorIndex"

    const/4 v2, 0x0

    .line 2
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    if-eqz v0, :cond_1

    .line 4
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->documentCoordinator:Lcom/pspdfkit/internal/ui/e;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/e;->a(Ljava/util/ArrayList;)V

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->documentCoordinator:Lcom/pspdfkit/internal/ui/e;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/DocumentDescriptor;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/ui/e;->setVisibleDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 9
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/f;->setFragment(Lcom/pspdfkit/ui/PdfFragment;)V

    :goto_1
    return-void
.end method

.method protected setDocument(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentCoordinator:Lcom/pspdfkit/internal/ui/e;

    invoke-static {p1}, Lcom/pspdfkit/ui/DocumentDescriptor;->fromDocument(Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/ui/DocumentDescriptor;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/e;->setDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    return-void
.end method

.method public setDocumentInteractionEnabled(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/f;->documentInteractionEnabled:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setDocumentInteractionEnabled(Z)V

    :cond_0
    return-void
.end method

.method public setDocumentPrintDialogFactory(Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->documentPrintDialogFactory:Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;)V

    :cond_0
    return-void
.end method

.method public setDocumentSharingDialogFactory(Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->documentSharingDialogFactory:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;)V

    :cond_0
    return-void
.end method

.method public setFragment(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/zf;->b(Lcom/pspdfkit/internal/zf$f;)V

    :cond_0
    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 10
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->isRedactionAnnotationPreviewEnabled()Z

    move-result v1

    .line 13
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    .line 14
    invoke-virtual {v3}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/ag;->getViewCoordinator()Lcom/pspdfkit/internal/xm;

    move-result-object v3

    .line 15
    invoke-virtual {v3}, Lcom/pspdfkit/internal/xm;->q()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 16
    invoke-virtual {v3}, Lcom/pspdfkit/internal/xm;->h()Lio/reactivex/rxjava3/core/Single;

    move-result-object v3

    invoke-virtual {v3}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/views/document/DocumentView;->exitCurrentlyActiveMode()V

    .line 17
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v3}, Lcom/pspdfkit/ui/PdfFragment;->getAudioModeManager()Lcom/pspdfkit/ui/audio/AudioModeManager;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/ui/audio/AudioModeManager;->exitActiveAudioMode()V

    .line 19
    :cond_1
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/ui/f;->removeListeners(Lcom/pspdfkit/ui/PdfFragment;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 25
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    if-nez v3, :cond_5

    .line 26
    new-instance v3, Lcom/pspdfkit/internal/sm;

    iget-object v4, p0, Lcom/pspdfkit/internal/ui/f;->rootView:Landroid/view/View;

    iget-object v5, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-direct {v3, v4, v5}, Lcom/pspdfkit/internal/sm;-><init>(Landroid/view/View;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    iput-object v3, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    .line 27
    invoke-virtual {v3, p0}, Lcom/pspdfkit/internal/sm;->addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    .line 33
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v4, p0, Lcom/pspdfkit/internal/ui/f;->toolbar:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {v3, v4}, Landroidx/appcompat/app/AppCompatActivity;->setSupportActionBar(Landroidx/appcompat/widget/Toolbar;)V

    .line 37
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->isUsingCustomFragmentTag()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 38
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->toolbar:Landroidx/appcompat/widget/Toolbar;

    new-instance v4, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda2;

    invoke-direct {v4, p0}, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/ui/f;)V

    invoke-virtual {v3, v4}, Landroidx/appcompat/widget/Toolbar;->setOnMenuItemClickListener(Landroidx/appcompat/widget/Toolbar$OnMenuItemClickListener;)V

    .line 40
    :cond_3
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v3, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/sm;->getThumbnailGridView()Lcom/pspdfkit/ui/PdfThumbnailGrid;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 41
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v3, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/sm;->getThumbnailGridView()Lcom/pspdfkit/ui/PdfThumbnailGrid;

    move-result-object v3

    .line 42
    new-instance v4, Lcom/pspdfkit/internal/ui/f$f;

    invoke-direct {v4, p0, v0}, Lcom/pspdfkit/internal/ui/f$f;-><init>(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/internal/ui/f$f-IA;)V

    .line 43
    invoke-virtual {v3, v4}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->addOnPageClickListener(Lcom/pspdfkit/ui/PdfThumbnailGrid$OnPageClickListener;)V

    .line 44
    invoke-virtual {v3, v4}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->addOnDocumentSavedListener(Lcom/pspdfkit/ui/PdfThumbnailGrid$OnDocumentSavedListener;)V

    .line 45
    invoke-direct {p0, v3}, Lcom/pspdfkit/internal/ui/f;->registerDocumentEditingToolbarListener(Lcom/pspdfkit/ui/PdfThumbnailGrid;)V

    .line 48
    :cond_4
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v3, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 49
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v3, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/pspdfkit/ui/redaction/RedactionView;->setRedactionAnnotationPreviewEnabled(Z)V

    .line 54
    :cond_5
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    if-nez v3, :cond_6

    .line 56
    new-instance v10, Lcom/pspdfkit/internal/cp;

    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 57
    invoke-virtual {v3}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isRedactionUiEnabled()Z

    move-result v3

    .line 58
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->REDACTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v4

    invoke-direct {v10, v3, v4}, Lcom/pspdfkit/internal/cp;-><init>(ZZ)V

    .line 59
    new-instance v3, Lcom/pspdfkit/internal/ui/c;

    iget-object v5, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v6, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    iget-object v7, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    iget-object v8, p0, Lcom/pspdfkit/internal/ui/f;->documentCoordinator:Lcom/pspdfkit/internal/ui/e;

    iget-object v9, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-object v4, v3

    move-object v11, p0

    invoke-direct/range {v4 .. v11}, Lcom/pspdfkit/internal/ui/c;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/internal/yf;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;Lcom/pspdfkit/internal/ui/e;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/internal/cp;Lcom/pspdfkit/internal/ui/c$g;)V

    iput-object v3, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    :cond_6
    if-nez p1, :cond_9

    .line 70
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    if-eqz p1, :cond_7

    .line 71
    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->resetDocument()V

    .line 72
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getEmptyView()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 73
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getEmptyView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 77
    :cond_7
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz p1, :cond_8

    .line 78
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    .line 79
    invoke-interface {p1}, Lcom/pspdfkit/internal/bg;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    .line 80
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    .line 81
    invoke-virtual {p1, v1}, Landroidx/fragment/app/FragmentTransaction;->remove(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    .line 82
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    .line 85
    :cond_8
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    .line 88
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->activityListener:Lcom/pspdfkit/listeners/PdfActivityListener;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-interface {p1, v1, v0}, Lcom/pspdfkit/listeners/PdfActivityListener;->onSetActivityTitle(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/PdfDocument;)V

    .line 91
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/ui/f;->onUserInterfaceEnabled(Z)V

    .line 94
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Landroidx/appcompat/app/AppCompatActivity;->supportInvalidateOptionsMenu()V

    return-void

    .line 98
    :cond_9
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/f;->setupListeners(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 101
    iget-boolean v3, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceEnabled:Z

    invoke-virtual {p1, v3}, Lcom/pspdfkit/ui/PdfFragment;->setUserInterfaceEnabled(Z)V

    .line 102
    iget-boolean v3, p0, Lcom/pspdfkit/internal/ui/f;->documentInteractionEnabled:Z

    invoke-virtual {p1, v3}, Lcom/pspdfkit/ui/PdfFragment;->setDocumentInteractionEnabled(Z)V

    .line 104
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    if-eq v3, p1, :cond_d

    .line 105
    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/PdfFragment;->setRedactionAnnotationPreviewEnabled(Z)V

    .line 108
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v1, :cond_a

    .line 109
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v1

    .line 110
    invoke-interface {v1}, Lcom/pspdfkit/internal/ag;->getDocumentListeners()Lcom/pspdfkit/internal/nh;

    move-result-object v1

    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    .line 111
    invoke-virtual {v3}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/ag;->getDocumentListeners()Lcom/pspdfkit/internal/nh;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Iterable;)V

    .line 116
    :cond_a
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    if-eqz v1, :cond_b

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v1

    if-nez v1, :cond_b

    .line 117
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->resetDocument()V

    .line 120
    :cond_b
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    .line 122
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    if-eqz v1, :cond_c

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 124
    :cond_c
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    invoke-interface {v1}, Lcom/pspdfkit/internal/bg;->getPdfParameters()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "PSPDF.PdfFragmentTag"

    const-string v4, "PSPDFKit.Fragment"

    .line 125
    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 127
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->internalPdfUi:Lcom/pspdfkit/internal/bg;

    .line 128
    invoke-interface {v3}, Lcom/pspdfkit/internal/bg;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v3

    .line 129
    invoke-virtual {v3}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v3

    iget v4, p0, Lcom/pspdfkit/internal/ui/f;->fragmentContainerId:I

    .line 130
    invoke-virtual {v3, v4, p1, v1}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    .line 131
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    .line 134
    :cond_d
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/sm;->a(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 136
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getThumbnailBarView()Lcom/pspdfkit/ui/PdfThumbnailBar;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 137
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getThumbnailBarView()Lcom/pspdfkit/ui/PdfThumbnailBar;

    move-result-object v1

    new-instance v3, Lcom/pspdfkit/internal/ui/f$e;

    invoke-direct {v3, p0, v0}, Lcom/pspdfkit/internal/ui/f$e;-><init>(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/internal/ui/f$e-IA;)V

    invoke-virtual {v1, v3}, Lcom/pspdfkit/ui/PdfThumbnailBar;->setOnPageChangedListener(Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;)V

    .line 140
    :cond_e
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getEmptyView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 141
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getEmptyView()Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 145
    :cond_f
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 146
    new-instance v1, Lcom/pspdfkit/ui/search/SearchResultHighlighter;

    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-direct {v1, v3}, Lcom/pspdfkit/ui/search/SearchResultHighlighter;-><init>(Landroid/content/Context;)V

    .line 147
    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/PdfFragment;->addDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    .line 148
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v3, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object v3

    new-instance v4, Lcom/pspdfkit/internal/ui/f$g;

    invoke-direct {v4, p0, v1, v0}, Lcom/pspdfkit/internal/ui/f$g;-><init>(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/ui/search/SearchResultHighlighter;Lcom/pspdfkit/internal/ui/f$g-IA;)V

    invoke-interface {v3, v4}, Lcom/pspdfkit/ui/search/PdfSearchView;->setSearchViewListener(Lcom/pspdfkit/ui/search/PdfSearchView$Listener;)V

    .line 152
    :cond_10
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v1

    if-eqz v1, :cond_11

    .line 153
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/f;->redactionApplicator:Lcom/pspdfkit/internal/so;

    .line 154
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/ui/redaction/RedactionView;->setListener(Lcom/pspdfkit/ui/redaction/RedactionView$RedactionViewListener;)V

    .line 155
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/ui/redaction/RedactionView;->setRedactionButtonVisible(ZZ)V

    .line 156
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v1

    .line 157
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->isRedactionAnnotationPreviewEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/redaction/RedactionView;->setRedactionAnnotationPreviewEnabled(Z)V

    .line 159
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_11

    .line 160
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 164
    :cond_11
    new-instance v1, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-direct {v1, v2}, Lcom/pspdfkit/ui/note/AnnotationNoteHinter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/f;->annotationNoteHinter:Lcom/pspdfkit/ui/note/AnnotationNoteHinter;

    .line 165
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isAnnotationNoteHintingEnabled()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 166
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->annotationNoteHinter:Lcom/pspdfkit/ui/note/AnnotationNoteHinter;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/PdfFragment;->addDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    .line 167
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->annotationNoteHinter:Lcom/pspdfkit/ui/note/AnnotationNoteHinter;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/PdfFragment;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 170
    :cond_12
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceCoordinator:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/ui/c;->b(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 173
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->activityListener:Lcom/pspdfkit/listeners/PdfActivityListener;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-interface {p1, v1, v0}, Lcom/pspdfkit/listeners/PdfActivityListener;->onSetActivityTitle(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/PdfDocument;)V

    .line 174
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getThumbnailGridView()Lcom/pspdfkit/ui/PdfThumbnailGrid;

    move-result-object p1

    if-eqz p1, :cond_13

    .line 175
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getThumbnailGridView()Lcom/pspdfkit/ui/PdfThumbnailGrid;

    move-result-object p1

    new-instance v1, Lcom/pspdfkit/internal/ui/f$f;

    invoke-direct {v1, p0, v0}, Lcom/pspdfkit/internal/ui/f$f;-><init>(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/internal/ui/f$f-IA;)V

    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->setOnPageClickListener(Lcom/pspdfkit/ui/PdfThumbnailGrid$OnPageClickListener;)V

    .line 178
    :cond_13
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->resetUI()V

    return-void
.end method

.method public setOnContextualToolbarLifecycleListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarLifecycleListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->setOnContextualToolbarLifecycleListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarLifecycleListener;)V

    :cond_0
    return-void
.end method

.method public setOnContextualToolbarMovementListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarMovementListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->setOnContextualToolbarMovementListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarMovementListener;)V

    :cond_0
    return-void
.end method

.method public setOnContextualToolbarPositionListener(Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->positionListener:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;

    return-void
.end method

.method public setPageIndex(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(I)V

    return-void
.end method

.method public setPageIndex(IZ)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(IZ)V

    return-void
.end method

.method public setPrintOptionsProvider(Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->printOptionsProvider:Lcom/pspdfkit/document/printing/PrintOptionsProvider;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V

    :cond_0
    return-void
.end method

.method public setScreenTimeout(J)V
    .locals 8

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/ui/f;->screenTimeoutMillis:J

    const-wide v2, 0x7fffffffffffffffL

    const-wide/16 v4, 0x0

    const/16 v6, 0x80

    cmp-long v7, v0, p1

    if-eqz v7, :cond_2

    cmp-long v0, p1, v4

    if-gez v0, :cond_0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "PSPDFKit.PdfActivity"

    const-string v0, "screenTimeoutMillis cannot be a negative number"

    .line 3
    invoke-static {p2, v0, p1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 6
    :cond_0
    iput-wide p1, p0, Lcom/pspdfkit/internal/ui/f;->screenTimeoutMillis:J

    if-nez v0, :cond_1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0

    :cond_1
    cmp-long v0, p1, v2

    if-nez v0, :cond_2

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/Window;->addFlags(I)V

    :cond_2
    :goto_0
    cmp-long v0, p1, v4

    if-eqz v0, :cond_3

    cmp-long v0, p1, v2

    if-eqz v0, :cond_3

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/f;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/Window;->addFlags(I)V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ui/f$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/f;)V

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_3
    return-void
.end method

.method public setSharingActionMenuListener(Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuListener:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;)V

    :cond_0
    return-void
.end method

.method public setSharingOptionsProvider(Lcom/pspdfkit/document/sharing/SharingOptionsProvider;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f;->sharingOptionsProvider:Lcom/pspdfkit/document/sharing/SharingOptionsProvider;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/document/sharing/SharingOptionsProvider;)V

    :cond_0
    return-void
.end method

.method public setUserInterfaceEnabled(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/f;->userInterfaceEnabled:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setUserInterfaceEnabled(Z)V

    :cond_0
    return-void
.end method

.method protected setupListeners(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 1

    .line 1
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->addOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V

    .line 2
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->addOnTextSelectionModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionModeChangeListener;)V

    .line 3
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->addOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V

    .line 4
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->addOnContentEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingModeChangeListener;)V

    .line 5
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->addOnContentEditingContentChangeListener(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;)V

    .line 6
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->addOnFormElementEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getAudioModeManager()Lcom/pspdfkit/ui/audio/AudioModeManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/audio/AudioModeListeners;->addAudioPlaybackModeChangeListener(Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioPlaybackModeChangeListener;)V

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getAudioModeManager()Lcom/pspdfkit/ui/audio/AudioModeManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/audio/AudioModeListeners;->addAudioRecordingModeChangeListener(Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioRecordingModeChangeListener;)V

    .line 9
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/ag;->addUserInterfaceListener(Lcom/pspdfkit/internal/ev;)V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->actionResolver:Lcom/pspdfkit/internal/ui/b;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentActionListener(Lcom/pspdfkit/document/DocumentActionListener;)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activityListener:Lcom/pspdfkit/listeners/PdfActivityListener;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->onAnnotationSelectedListenerAdapter:Lcom/pspdfkit/ui/special_mode/manager/OnAnnotationSelectedListenerAdapter;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->addOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->documentScrollListener:Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentScrollListener(Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;)V

    return-void
.end method

.method public showPrintDialog()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    sget-object v1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    const-wide/16 v2, 0x0

    .line 3
    invoke-virtual {v0, v1, v2, v3}, Lcom/pspdfkit/internal/sm;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)Z

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->ensureSharingMenuFragment()V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->printOptionsProvider:Lcom/pspdfkit/document/printing/PrintOptionsProvider;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->documentPrintDialogFactory:Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/kr;->performPrint()V

    return-void
.end method

.method public showSaveAsDialog()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    sget-object v1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    const-wide/16 v2, 0x0

    .line 3
    invoke-virtual {v0, v1, v2, v3}, Lcom/pspdfkit/internal/sm;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)Z

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->ensureSharingMenuFragment()V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->sharingOptionsProvider:Lcom/pspdfkit/document/sharing/SharingOptionsProvider;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/document/sharing/SharingOptionsProvider;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->documentSharingDialogFactory:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/kr;->performSaveAs()V

    return-void
.end method

.method public showSearchView()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->isDisplayed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_SEARCH:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/f;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    :cond_0
    return-void
.end method

.method public showSharingMenu()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAutosaveEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->save()V

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    sget-object v1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    const-wide/16 v2, 0x0

    .line 10
    invoke-virtual {v0, v1, v2, v3}, Lcom/pspdfkit/internal/sm;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)Z

    .line 11
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f;->ensureSharingMenuFragment()V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuListener:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->documentSharingDialogFactory:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;)V

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->documentPrintDialogFactory:Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;)V

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->sharingOptionsProvider:Lcom/pspdfkit/document/sharing/SharingOptionsProvider;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/document/sharing/SharingOptionsProvider;)V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f;->printOptionsProvider:Lcom/pspdfkit/document/printing/PrintOptionsProvider;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->sharingMenuFragment:Lcom/pspdfkit/internal/kr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/kr;->c()V

    return-void
.end method

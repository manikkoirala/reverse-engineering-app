.class final Lcom/pspdfkit/internal/ui/f$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/ui/f;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/pspdfkit/internal/ui/f;


# direct methods
.method public static synthetic $r8$lambda$V69NwNMkAJBqFokMCk7QBPPlkAQ(Lcom/pspdfkit/internal/ui/f$b;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/f$b;->a()V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/ui/f;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f$b;->b:Lcom/pspdfkit/internal/ui/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synthetic a()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$b;->b:Lcom/pspdfkit/internal/ui/f;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/f;->-$$Nest$mshowContentEditingStylingBar(Lcom/pspdfkit/internal/ui/f;)V

    return-void
.end method


# virtual methods
.method public final onDisplayPropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$b;->b:Lcom/pspdfkit/internal/ui/f;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/f;->-$$Nest$fgetisInContentEditingMode(Lcom/pspdfkit/internal/ui/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/f;->-$$Nest$mhideContentEditingStylingBar(Lcom/pspdfkit/internal/ui/f;)V

    .line 4
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$b;->b:Lcom/pspdfkit/internal/ui/f;

    iget-object p1, p1, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/sm;->a(Z)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$b;->b:Lcom/pspdfkit/internal/ui/f;

    iget-object p1, p1, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    const/high16 v0, 0x60000

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    return-void
.end method

.method public final onPreparePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 0

    return-void
.end method

.method public final onRemovePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$b;->b:Lcom/pspdfkit/internal/ui/f;

    iget-object p1, p1, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/sm;->a(Z)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$b;->b:Lcom/pspdfkit/internal/ui/f;

    iget-object p1, p1, Lcom/pspdfkit/internal/ui/f;->toolbarCoordinatorLayout:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    const/high16 v0, 0x40000

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$b;->b:Lcom/pspdfkit/internal/ui/f;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/f;->-$$Nest$fgetisInContentEditingMode(Lcom/pspdfkit/internal/ui/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/f;->-$$Nest$fgetcurrentlyEditedBlockID(Lcom/pspdfkit/internal/ui/f;)Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    iget-object p1, p1, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 6
    new-instance v0, Lcom/pspdfkit/internal/ui/f$b$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/f$b$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/f$b;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.class final Lcom/pspdfkit/internal/ui/c$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener<",
        "Lcom/pspdfkit/ui/navigation/NavigationBackStack$NavigationItem<",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/ui/c;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onBackStackChanged()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetm(Lcom/pspdfkit/internal/ui/c;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetz(Lcom/pspdfkit/internal/ui/c;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4
    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v1

    .line 5
    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetf(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v1

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowNavigationButtonsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 7
    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v0

    .line 8
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getActiveViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x4

    if-nez v0, :cond_4

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 12
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    return-void

    .line 19
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetl(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->getBackItem()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 23
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 26
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetl(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->getForwardItem()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 30
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 33
    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetl(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->getBackItem()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetl(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->getForwardItem()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_9

    goto :goto_4

    .line 36
    :cond_9
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/ui/c;->d(Z)V

    goto :goto_5

    .line 37
    :cond_a
    :goto_4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$b;->a:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/ui/c;->t(Z)V

    :goto_5
    return-void
.end method

.method public final bridge synthetic visitedItem(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/ui/navigation/NavigationBackStack$NavigationItem;

    return-void
.end method

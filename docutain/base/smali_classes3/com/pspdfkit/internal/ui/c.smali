.class public final Lcom/pspdfkit/internal/ui/c;
.super Lcom/pspdfkit/listeners/SimpleDocumentListener;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ui/a$a;
.implements Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$OnContextualToolbarPositionListener;
.implements Lcom/pspdfkit/ui/forms/FormEditingBar$OnFormEditingBarLifecycleListener;
.implements Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;
.implements Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;
.implements Lcom/pspdfkit/ui/audio/AudioView$AudioInspectorLifecycleListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/c$g;
    }
.end annotation


# instance fields
.field private final A:Lcom/pspdfkit/internal/ap;

.field private final B:Lcom/pspdfkit/ui/audio/AudioView;

.field private final C:Lcom/pspdfkit/ui/PdfThumbnailBar;

.field private D:Lio/reactivex/rxjava3/disposables/Disposable;

.field private final E:Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;

.field private final F:Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener<",
            "Lcom/pspdfkit/ui/navigation/NavigationBackStack$NavigationItem<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Landroidx/appcompat/app/AppCompatActivity;

.field private final c:Lcom/pspdfkit/internal/yf;

.field private final d:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

.field private final e:Lcom/pspdfkit/ui/DocumentCoordinator;

.field private final f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

.field private final g:Lcom/pspdfkit/internal/ui/a;

.field private final h:Lcom/pspdfkit/internal/ui/c$g;

.field private final i:Landroid/os/Handler;

.field private j:Lcom/pspdfkit/ui/PdfFragment;

.field k:Lcom/pspdfkit/internal/pg$c;

.field private l:Lcom/pspdfkit/ui/navigation/NavigationBackStack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/ui/navigation/NavigationBackStack<",
            "Lcom/pspdfkit/ui/navigation/NavigationBackStack$NavigationItem<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Z

.field private n:Z

.field private o:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:I

.field private u:I

.field private v:Landroid/animation/AnimatorSet;

.field private final w:J

.field private final x:Lio/reactivex/rxjava3/subjects/ReplaySubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/subjects/ReplaySubject<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private y:Z

.field private z:Z


# direct methods
.method public static synthetic $r8$lambda$1BxytLwrVPMO2jyWNhiQJaUrwMk(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/c;->d(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$3Lghj33OrDDbZcAa850ZZjiNvQQ(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->w()V

    return-void
.end method

.method public static synthetic $r8$lambda$4E4eh7TfmExAQgqgFmHjn7zBBx0(Lcom/pspdfkit/internal/ui/c;Ljava/lang/Runnable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$5YnnAgMQK62iC0A5c8xtmCAeQg0(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->k()V

    return-void
.end method

.method public static synthetic $r8$lambda$5xekHGbYeXAhZF4c5EM4ErxE9GU(Lcom/pspdfkit/internal/ui/c;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Landroidx/appcompat/app/AppCompatActivity;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ui/c;->a(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Landroidx/appcompat/app/AppCompatActivity;)V

    return-void
.end method

.method public static synthetic $r8$lambda$7LvpmqYtqw8Ty4psL3z1QBJFiq4(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->t()V

    return-void
.end method

.method public static synthetic $r8$lambda$9XDdP4R5bxdEzVbSdf_Ix-vagt4(Lcom/pspdfkit/internal/ui/c;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->e(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$AyPY8nnu2aa6S16UC9AgNnaUCBw(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->m()V

    return-void
.end method

.method public static synthetic $r8$lambda$BeX9AlUx68S7ewt0UUKm90Ia7YY(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/c;->c(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$D8kPTeD2WpnfUZFC372UD6yz1go(Ljava/lang/Runnable;Ljava/lang/Integer;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/lang/Runnable;Ljava/lang/Integer;)V

    return-void
.end method

.method public static synthetic $r8$lambda$Dl-jMnEo4H8PR14OQsAm2pPkwLE(Lcom/pspdfkit/internal/ui/c;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->l(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$FIntxKz-AUvchtTo8Ph5JdUKBro(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->o()V

    return-void
.end method

.method public static synthetic $r8$lambda$FK9OxVi-VAp_uFnT4efAyFHOFzw(Lcom/pspdfkit/internal/ui/c;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->g(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$FYv6wo8tsOQigFvrVT3MCI59Vrs(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->j()V

    return-void
.end method

.method public static synthetic $r8$lambda$GXZid5VojbcnGRm5wuWDbCSv9v4(Lcom/pspdfkit/internal/ui/c;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->d(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$HeONDg2wUUGKG0Btxi5HO6wP1EQ(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/c;->b(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$IwUzz4bK80J8GTFU2CXX63LcH7Y(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->n()V

    return-void
.end method

.method public static synthetic $r8$lambda$JWKe-h7sA-YHhJqEHAKuEU_drLk(Lcom/pspdfkit/internal/ui/c;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->i(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$KqOvnSHvR2QQGRfWquqttr62XJo(Landroid/view/View;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/ui/c;->b(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$O4B4yKgiYlz7EIVkKIFgbO1Ipmk(Lcom/pspdfkit/internal/ui/c;Landroid/view/View;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ui/c;->a(Landroid/view/View;Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$UAuIEyiL-T-geSu9Mbj_ZP06PuE(Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$VLp_qHuxFcYsDD1z4A_B5ZwJobQ(Landroid/view/View;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/ui/c;->a(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$YLKWbv3y6MifrqTlZecKqXD1fhU(Lcom/pspdfkit/internal/ui/c;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->f(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$bfa5FxyHC-G-IytvmiOvTTE8IKQ(Lcom/pspdfkit/internal/ui/c;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->h(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$k1dKs7LgMomJ7LIYSarbkJv2Qc0(Lcom/pspdfkit/internal/ui/c;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->g(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$kXPjHOfi6knOtDhT0Kw-44U61xk(Landroid/view/View;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/ui/c;->c(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$mcQODKDJqrait8MCsNMTeo9qB64(Lcom/pspdfkit/internal/ui/c;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->j(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$pqVFz7SnJANtLlzryMXq7PDBD_8(Lcom/pspdfkit/internal/ui/c;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->k(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$sjkPSvAxDQ94T2ZOOlkazPg8atg(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/c;->a(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$xa3hyiKz8b-qvOqgaEhuvVMaHIo(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->q()V

    return-void
.end method

.method public static synthetic $r8$lambda$xpLAFAg1t-LBxqV80hW2L83tOHk(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->l()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetA(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/ap;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/c;->A:Lcom/pspdfkit/internal/ap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetC(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/ui/PdfThumbnailBar;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetF(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/c;->F:Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/ui/c;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetj(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetl(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/ui/navigation/NavigationBackStack;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/c;->l:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetm(Lcom/pspdfkit/internal/ui/c;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetv(Lcom/pspdfkit/internal/ui/c;)Landroid/animation/AnimatorSet;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/c;->v:Landroid/animation/AnimatorSet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetz(Lcom/pspdfkit/internal/ui/c;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/ui/c;->z:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputl(Lcom/pspdfkit/internal/ui/c;Lcom/pspdfkit/ui/navigation/NavigationBackStack;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/c;->l:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    return-void
.end method

.method static bridge synthetic -$$Nest$mt(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->t()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mu(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->u()V

    return-void
.end method

.method public constructor <init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/internal/yf;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;Lcom/pspdfkit/internal/ui/e;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/internal/cp;Lcom/pspdfkit/internal/ui/c$g;)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/listeners/SimpleDocumentListener;-><init>()V

    .line 2
    new-instance v0, Landroid/os/Handler;

    .line 3
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/c;->i:Landroid/os/Handler;

    const/4 v0, 0x1

    .line 17
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    .line 19
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->n:Z

    .line 21
    sget-object v1, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_AUTOMATIC:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/c;->o:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    const/4 v1, 0x0

    .line 24
    iput-boolean v1, p0, Lcom/pspdfkit/internal/ui/c;->p:Z

    .line 26
    iput-boolean v1, p0, Lcom/pspdfkit/internal/ui/c;->q:Z

    .line 31
    iput-boolean v1, p0, Lcom/pspdfkit/internal/ui/c;->r:Z

    .line 33
    iput-boolean v1, p0, Lcom/pspdfkit/internal/ui/c;->s:Z

    .line 35
    iput v1, p0, Lcom/pspdfkit/internal/ui/c;->t:I

    .line 37
    iput v1, p0, Lcom/pspdfkit/internal/ui/c;->u:I

    .line 42
    invoke-static {}, Lcom/pspdfkit/internal/ov;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/pspdfkit/internal/ui/c;->w:J

    .line 44
    invoke-static {v0}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->create(I)Lio/reactivex/rxjava3/subjects/ReplaySubject;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/internal/ui/c;->x:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    .line 46
    iput-boolean v1, p0, Lcom/pspdfkit/internal/ui/c;->y:Z

    .line 48
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->z:Z

    .line 63
    new-instance v0, Lcom/pspdfkit/internal/ui/c$a;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/c$a;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/c;->E:Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;

    .line 88
    new-instance v1, Lcom/pspdfkit/internal/ui/c$b;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ui/c$b;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/c;->F:Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;

    .line 138
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    .line 139
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 140
    iput-object p3, p0, Lcom/pspdfkit/internal/ui/c;->d:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    .line 141
    iput-object p4, p0, Lcom/pspdfkit/internal/ui/c;->e:Lcom/pspdfkit/ui/DocumentCoordinator;

    .line 142
    iput-object p5, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 143
    iput-object p7, p0, Lcom/pspdfkit/internal/ui/c;->h:Lcom/pspdfkit/internal/ui/c$g;

    .line 145
    invoke-virtual {p6, p0}, Lcom/pspdfkit/internal/cp;->a(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/ap;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/internal/ui/c;->A:Lcom/pspdfkit/internal/ap;

    if-eqz p3, :cond_0

    .line 147
    invoke-interface {p3, p4}, Lcom/pspdfkit/internal/ap;->a(Lcom/pspdfkit/internal/ui/e;)V

    .line 150
    :cond_0
    check-cast p2, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/internal/ui/c;->B:Lcom/pspdfkit/ui/audio/AudioView;

    .line 151
    invoke-virtual {p5}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getThumbnailBarMode()Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    move-result-object p3

    sget-object p6, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_NONE:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    if-eq p3, p6, :cond_1

    .line 152
    invoke-virtual {p2}, Lcom/pspdfkit/internal/sm;->getThumbnailBarView()Lcom/pspdfkit/ui/PdfThumbnailBar;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    goto :goto_0

    :cond_1
    const/4 p3, 0x0

    .line 154
    iput-object p3, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    .line 158
    :goto_0
    new-instance p3, Lcom/pspdfkit/internal/ui/a;

    invoke-direct {p3, p1, p0}, Lcom/pspdfkit/internal/ui/a;-><init>(Landroid/app/Activity;Lcom/pspdfkit/internal/ui/a$a;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/ui/c;->g:Lcom/pspdfkit/internal/ui/a;

    .line 159
    invoke-virtual {p5}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isImmersiveMode()Z

    move-result p6

    invoke-virtual {p3, p6}, Lcom/pspdfkit/internal/ui/a;->a(Z)Z

    .line 162
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->z()V

    if-eqz p7, :cond_2

    .line 165
    invoke-interface {p7, p0}, Lcom/pspdfkit/internal/ui/c$g;->onBindToUserInterfaceCoordinator(Lcom/pspdfkit/internal/ui/c;)V

    .line 169
    :cond_2
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p3

    invoke-virtual {p3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p3

    new-instance p6, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda15;

    invoke-direct {p6, p0, p5, p1}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda15;-><init>(Lcom/pspdfkit/internal/ui/c;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Landroidx/appcompat/app/AppCompatActivity;)V

    invoke-static {p3, p6}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 182
    invoke-virtual {p2}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 183
    invoke-virtual {p2}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object p1

    new-instance p3, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda16;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda16;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    invoke-virtual {p2}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object p1

    new-instance p3, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda17;

    invoke-direct {p3}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda17;-><init>()V

    invoke-static {p1, p3}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    .line 193
    :cond_3
    invoke-virtual {p2}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 194
    invoke-virtual {p2}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object p1

    new-instance p3, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda18;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda18;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    invoke-virtual {p2}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object p1

    new-instance p3, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda19;

    invoke-direct {p3}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda19;-><init>()V

    invoke-static {p1, p3}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    .line 205
    :cond_4
    invoke-virtual {p2}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 206
    invoke-virtual {p4, v0}, Lcom/pspdfkit/internal/ui/e;->addOnDocumentsChangedListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;)V

    .line 207
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->u()V

    .line 210
    :cond_5
    invoke-virtual {p2}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 211
    invoke-virtual {p2}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object p1

    new-instance p3, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda20;

    invoke-direct {p3}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda20;-><init>()V

    invoke-static {p1, p3}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    .line 219
    :cond_6
    invoke-virtual {p2}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 220
    invoke-virtual {p2}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda21;

    invoke-direct {p2}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda21;-><init>()V

    invoke-static {p1, p2}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    :cond_7
    return-void
.end method

.method private A()Z
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getDocumentTitleOverlayView()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowDocumentTitleOverlayEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->d:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->isDisplayingContextualToolbar()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 5
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getActiveViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    sget-object v3, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-ne v0, v3, :cond_1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/pspdfkit/R$bool;->pspdf__display_document_title_in_actionbar:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 7
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method private a(Z)Landroid/animation/AnimatorSet;
    .locals 8

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 48
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->h()Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0

    .line 49
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 50
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getThumbnailBarMode()Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_NONE:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_3

    const-string v2, "translationY"

    const/4 v3, 0x0

    const/4 v6, 0x2

    if-eqz p1, :cond_2

    .line 51
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 52
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {p1, v7}, Landroid/widget/FrameLayout;->setAlpha(F)V

    .line 53
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    new-array v6, v6, [F

    .line 54
    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getTranslationY()F

    move-result v7

    aput v7, v6, v5

    aput v3, v6, v4

    .line 55
    invoke-static {p1, v2, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 58
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    new-array v6, v6, [F

    aput v3, v6, v5

    .line 59
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    aput v3, v6, v4

    invoke-static {p1, v2, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object p1

    .line 60
    new-instance v2, Lcom/pspdfkit/internal/ui/d;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/ui/d;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    invoke-virtual {p1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 68
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_3
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_4

    return-object v0

    .line 74
    :cond_4
    new-instance p1, Landroid/animation/AnimatorSet;

    invoke-direct {p1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 75
    invoke-virtual {p1, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    return-object p1
.end method

.method private static synthetic a(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 1

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    check-cast p0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 7
    invoke-virtual {p1}, Landroidx/core/view/WindowInsetsCompat;->getSystemWindowInsetRight()I

    move-result v0

    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    return-object p1
.end method

.method private a()V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 100
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 102
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getDocumentTitleOverlayView()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 103
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getDocumentTitleOverlayView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 105
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 106
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 108
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->v:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_5

    .line 109
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    const/4 v0, 0x0

    .line 110
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/c;->v:Landroid/animation/AnimatorSet;

    :cond_5
    return-void
.end method

.method private a(IIZ)V
    .locals 9

    .line 111
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageNumberOverlay()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x0

    if-eqz v0, :cond_2

    .line 112
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_1

    .line 113
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v3

    :goto_1
    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_f

    .line 115
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageNumberOverlay()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_5

    .line 116
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_4

    .line 117
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    goto :goto_4

    :cond_4
    move-object v0, v3

    :goto_4
    if-eqz v0, :cond_5

    .line 118
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    goto :goto_5

    :cond_5
    const/4 v0, 0x0

    :goto_5
    if-eqz v0, :cond_d

    .line 119
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object v0

    if-nez v0, :cond_6

    goto/16 :goto_a

    :cond_6
    const/4 v4, -0x1

    if-eq p2, v4, :cond_7

    const/4 v4, 0x1

    goto :goto_6

    :cond_7
    const/4 v4, 0x0

    .line 120
    :goto_6
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v5, :cond_8

    .line 121
    invoke-virtual {v5}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v5

    goto :goto_7

    :cond_8
    move-object v5, v3

    .line 122
    :goto_7
    invoke-interface {v5, p1, v1}, Lcom/pspdfkit/document/PdfDocument;->getPageLabel(IZ)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    const/4 v7, 0x2

    if-eqz v5, :cond_b

    .line 123
    iget-object v8, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v8}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageLabels()Z

    move-result v8

    if-eqz v8, :cond_b

    if-eqz v4, :cond_9

    goto :goto_8

    :cond_9
    add-int/lit8 p2, p1, 0x1

    .line 145
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 146
    iget-object v4, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    sget v5, Lcom/pspdfkit/R$string;->pspdf__page_overlay:I

    new-array v6, v7, [Ljava/lang/Object;

    .line 150
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v6, v1

    iget-object p2, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    .line 151
    invoke-virtual {p2}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    invoke-interface {p2}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v6, v2

    .line 152
    invoke-static {v4, v5, v0, v6}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 159
    :cond_a
    iget-object v4, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    sget v8, Lcom/pspdfkit/R$string;->pspdf__page_overlay_with_label:I

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v5, v6, v1

    .line 164
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v6, v2

    iget-object p2, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    .line 165
    invoke-virtual {p2}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    invoke-interface {p2}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v6, v7

    .line 166
    invoke-static {v4, v8, v0, v6}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_9

    :cond_b
    :goto_8
    if-nez v4, :cond_c

    .line 167
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    sget v4, Lcom/pspdfkit/R$string;->pspdf__page_overlay:I

    new-array v5, v7, [Ljava/lang/Object;

    add-int/lit8 v6, p1, 0x1

    .line 171
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    iget-object v6, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    .line 172
    invoke-virtual {v6}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v6

    invoke-interface {v6}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    .line 173
    invoke-static {p2, v4, v0, v5}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_9

    .line 180
    :cond_c
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    add-int/2addr p2, v2

    .line 181
    iget-object v4, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    sget v5, Lcom/pspdfkit/R$string;->pspdf__page_overlay_double_page:I

    new-array v6, v6, [Ljava/lang/Object;

    .line 185
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v1

    add-int/2addr p2, v2

    .line 186
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v6, v2

    iget-object p2, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    .line 187
    invoke-virtual {p2}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    invoke-interface {p2}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v6, v7

    .line 188
    invoke-static {v4, v5, v0, v6}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    :goto_9
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_d

    .line 220
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object p2

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    sget v4, Lcom/pspdfkit/R$string;->pspdf__page_with_number:I

    new-array v5, v2, [Ljava/lang/Object;

    add-int/2addr p1, v2

    .line 222
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v5, v1

    .line 223
    invoke-static {v0, v4, v3, v5}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 224
    :cond_d
    :goto_a
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 225
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object p1

    .line 226
    invoke-virtual {p1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const/high16 p2, 0x3f800000    # 1.0f

    .line 227
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const-wide/16 v0, 0x0

    .line 228
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    if-eqz p3, :cond_e

    .line 229
    iget-wide v0, p0, Lcom/pspdfkit/internal/ui/c;->w:J

    :cond_e
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 231
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->D:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 232
    invoke-static {p1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 233
    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 p2, 0x5dc

    invoke-static {p2, p3, p1}, Lio/reactivex/rxjava3/core/Completable;->timer(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 234
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda27;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda27;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    .line 235
    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/c;->D:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_f
    return-void
.end method

.method private static synthetic a(Landroid/view/View;)V
    .locals 1

    const/16 v0, 0x8

    .line 246
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private synthetic a(Landroid/view/View;Z)V
    .locals 3

    const/4 v0, 0x0

    .line 236
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 237
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 239
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    if-eqz p2, :cond_0

    const-wide/16 v1, 0xfa

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    .line 240
    :goto_0
    invoke-virtual {p1, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-direct {p2, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    .line 241
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const/high16 p2, 0x3f800000    # 1.0f

    .line 242
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 243
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/c;->b(Z)I

    move-result p2

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda4;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    .line 244
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 245
    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Landroidx/appcompat/app/AppCompatActivity;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->g:Lcom/pspdfkit/internal/ui/a;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isImmersiveMode()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/a;->a(Z)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda2;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->q()V

    :goto_0
    return-void
.end method

.method private a(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 1

    .line 255
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 256
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/ui/forms/FormEditingBar;->removeOnFormEditingBarLifecycleListener(Lcom/pspdfkit/ui/forms/FormEditingBar$OnFormEditingBarLifecycleListener;)V

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 260
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->removeOnContentEditingBarLifecycleListener(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;)V

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 263
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/ui/audio/AudioView;->removeOnAudioInspectorLifecycleListener(Lcom/pspdfkit/ui/audio/AudioView$AudioInspectorLifecycleListener;)V

    .line 265
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getNavigationHistory()Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->F:Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->removeBackStackListener(Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;)V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 3

    .line 8
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->y:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/u;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->x:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    .line 12
    invoke-virtual {v0}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->firstOrError()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 13
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    const/4 v2, 0x5

    .line 14
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 16
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda28;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda28;-><init>(Ljava/lang/Runnable;)V

    new-instance p1, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda29;

    invoke-direct {p1}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda29;-><init>()V

    .line 17
    invoke-virtual {v0, v1, p1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    :goto_0
    return-void
.end method

.method private static synthetic a(Ljava/lang/Runnable;Ljava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 18
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method private a(Ljava/lang/Runnable;Z)V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    if-eqz p2, :cond_2

    .line 79
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->h()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/c;->i()Z

    move-result p2

    if-nez p2, :cond_1

    const/4 p2, 0x1

    .line 80
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/ui/c;->r(Z)V

    .line 81
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    invoke-virtual {p2, v1}, Landroid/widget/FrameLayout;->setAlpha(F)V

    .line 82
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p2, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    if-eqz p1, :cond_3

    .line 84
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    new-instance v0, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda0;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda0;-><init>(Ljava/lang/Runnable;)V

    invoke-static {p2, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_3

    .line 88
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 92
    :cond_2
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    invoke-virtual {p2, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ui/c;Ljava/lang/Runnable;)V

    invoke-virtual {p2, v0}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :cond_3
    :goto_0
    return-void
.end method

.method private static synthetic a(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 19
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.PdfActivity"

    invoke-static {v2, p0, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private a(Ljava/util/List;ZZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/animation/Animator;",
            ">;ZZ)V"
        }
    .end annotation

    .line 20
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 21
    :cond_0
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/c;->v:Landroid/animation/AnimatorSet;

    const-wide/16 v1, 0x0

    if-eqz p3, :cond_1

    const-wide/16 v3, 0xfa

    goto :goto_0

    :cond_1
    move-wide v3, v1

    .line 22
    :goto_0
    invoke-virtual {v0, v3, v4}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->v:Landroid/animation/AnimatorSet;

    if-eqz p3, :cond_2

    if-nez p2, :cond_2

    const-wide/16 v1, 0x64

    :cond_2
    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 29
    iget-object p3, p0, Lcom/pspdfkit/internal/ui/c;->v:Landroid/animation/AnimatorSet;

    const/high16 v0, 0x3fc00000    # 1.5f

    if-eqz p2, :cond_3

    .line 30
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1, v0}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    goto :goto_1

    :cond_3
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1, v0}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    .line 31
    :goto_1
    invoke-virtual {p3, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 33
    iget-object p3, p0, Lcom/pspdfkit/internal/ui/c;->v:Landroid/animation/AnimatorSet;

    invoke-virtual {p3, p1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    if-eqz p2, :cond_4

    .line 37
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->v:Landroid/animation/AnimatorSet;

    new-instance p2, Lcom/pspdfkit/internal/ui/c$d;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/ui/c$d;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    invoke-virtual {p1, p2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 47
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->v:Landroid/animation/AnimatorSet;

    invoke-virtual {p1}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method private b(Z)I
    .locals 3

    .line 50
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->d:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->getCurrentlyDisplayedContextualToolbar()Lcom/pspdfkit/ui/toolbar/ContextualToolbar;

    move-result-object v0

    .line 51
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    const/4 v2, 0x0

    if-nez v1, :cond_2

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->isDraggable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 54
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getPosition()Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;->TOP:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->g:Lcom/pspdfkit/internal/ui/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v0}, Lcom/pspdfkit/internal/ce;->a(Landroidx/appcompat/app/AppCompatActivity;)I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 58
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->d:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->getToolbarInset()I

    move-result v0

    .line 59
    :goto_1
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz v1, :cond_4

    if-eqz v1, :cond_3

    .line 60
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v2, 0x1

    :cond_3
    if-eqz v2, :cond_4

    .line 61
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    if-eqz p1, :cond_5

    .line 63
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz p1, :cond_5

    .line 65
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->A()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 66
    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getDocumentTitleOverlayView()Landroid/widget/TextView;

    move-result-object p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 67
    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getDocumentTitleOverlayView()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_5

    .line 69
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getDocumentTitleOverlayView()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    add-int/2addr v0, p1

    :cond_5
    return v0
.end method

.method private static synthetic b(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    check-cast p0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2
    invoke-virtual {p1}, Landroidx/core/view/WindowInsetsCompat;->getSystemWindowInsetLeft()I

    move-result v0

    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    return-object p1
.end method

.method private static synthetic b(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x4

    .line 45
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private synthetic b(Ljava/lang/Runnable;)V
    .locals 1

    const/4 v0, 0x0

    .line 42
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/c;->r(Z)V

    if-eqz p1, :cond_0

    .line 44
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method private c()Landroid/animation/AnimatorSet;
    .locals 13

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->h()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    goto :goto_0

    .line 7
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    goto :goto_0

    .line 9
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    int-to-float v1, v1

    .line 11
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/c;->i()Z

    move-result v3

    const/4 v4, 0x1

    if-nez v3, :cond_4

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->f()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->e()Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v3, 0x1

    .line 15
    :goto_2
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/c;->B:Lcom/pspdfkit/ui/audio/AudioView;

    if-eqz v5, :cond_6

    .line 16
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v5, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v5, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/ui/audio/AudioView;->isVisible()Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    goto :goto_3

    :cond_5
    const/4 v5, 0x0

    :goto_3
    if-eqz v5, :cond_6

    .line 17
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/c;->B:Lcom/pspdfkit/ui/audio/AudioView;

    invoke-virtual {v5}, Lcom/pspdfkit/ui/audio/AudioView;->getAudioInspectorHeight()I

    move-result v5

    goto :goto_4

    :cond_6
    const/4 v5, 0x0

    :goto_4
    int-to-float v5, v5

    .line 20
    iget-object v6, p0, Lcom/pspdfkit/internal/ui/c;->B:Lcom/pspdfkit/ui/audio/AudioView;

    const/4 v7, 0x0

    const/4 v8, 0x2

    const-string v9, "translationY"

    if-eqz v6, :cond_8

    new-array v10, v8, [F

    .line 24
    invoke-virtual {v6}, Landroid/widget/FrameLayout;->getTranslationY()F

    move-result v11

    aput v11, v10, v2

    if-eqz v3, :cond_7

    .line 26
    iget-object v11, p0, Lcom/pspdfkit/internal/ui/c;->B:Lcom/pspdfkit/ui/audio/AudioView;

    invoke-virtual {v11}, Landroid/view/View;->getHeight()I

    move-result v11

    iget-object v12, p0, Lcom/pspdfkit/internal/ui/c;->B:Lcom/pspdfkit/ui/audio/AudioView;

    .line 27
    invoke-virtual {v12}, Lcom/pspdfkit/ui/audio/AudioView;->getAudioInspectorHeight()I

    move-result v12

    sub-int/2addr v11, v12

    int-to-float v11, v11

    sub-float/2addr v11, v1

    goto :goto_5

    :cond_7
    const/4 v11, 0x0

    :goto_5
    aput v11, v10, v4

    .line 30
    invoke-static {v6, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    :cond_8
    iget-object v6, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v6}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageNumberOverlay()Z

    move-result v6

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v6, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object v6

    if-eqz v6, :cond_9

    const/4 v6, 0x1

    goto :goto_6

    :cond_9
    const/4 v6, 0x0

    :goto_6
    if-eqz v6, :cond_b

    .line 32
    iget-object v6, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 33
    check-cast v6, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object v6

    new-array v10, v8, [F

    iget-object v11, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 35
    check-cast v11, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v11}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/TextView;->getTranslationY()F

    move-result v11

    aput v11, v10, v2

    if-eqz v3, :cond_a

    const/4 v11, 0x0

    goto :goto_7

    :cond_a
    move v11, v1

    :goto_7
    sub-float/2addr v11, v5

    aput v11, v10, v4

    .line 37
    invoke-static {v6, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    :cond_b
    iget-object v6, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v6, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_c

    iget-object v6, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 39
    check-cast v6, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_c

    iget-object v6, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 40
    invoke-virtual {v6}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowNavigationButtonsEnabled()Z

    move-result v6

    if-eqz v6, :cond_c

    const/4 v6, 0x1

    goto :goto_8

    :cond_c
    const/4 v6, 0x0

    :goto_8
    if-eqz v6, :cond_f

    .line 41
    iget-object v6, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 42
    check-cast v6, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v6

    new-array v10, v8, [F

    iget-object v11, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 44
    check-cast v11, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v11}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/View;->getTranslationY()F

    move-result v11

    aput v11, v10, v2

    if-eqz v3, :cond_d

    const/4 v11, 0x0

    goto :goto_9

    :cond_d
    move v11, v1

    :goto_9
    sub-float/2addr v11, v5

    aput v11, v10, v4

    .line 46
    invoke-static {v6, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    iget-object v6, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 52
    check-cast v6, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v6

    new-array v8, v8, [F

    iget-object v10, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 54
    check-cast v10, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v10}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->getTranslationY()F

    move-result v10

    aput v10, v8, v2

    if-eqz v3, :cond_e

    const/4 v1, 0x0

    :cond_e
    sub-float/2addr v1, v5

    aput v1, v8, v4

    .line 56
    invoke-static {v6, v9, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_f
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v0, 0x0

    return-object v0

    .line 66
    :cond_10
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 67
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    return-object v1
.end method

.method private static synthetic c(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    check-cast p0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2
    invoke-virtual {p1}, Landroidx/core/view/WindowInsetsCompat;->getStableInsetBottom()I

    move-result v0

    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 3
    invoke-virtual {p1}, Landroidx/core/view/WindowInsetsCompat;->getSystemWindowInsetRight()I

    move-result v0

    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    return-object p1
.end method

.method private static synthetic c(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x4

    .line 81
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private static synthetic d(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0, v0, v0, v0}, Landroid/view/View;->setPadding(IIII)V

    return-object p1
.end method

.method private synthetic d(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->l:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->goForward()V

    :cond_0
    return-void
.end method

.method private synthetic e(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->l:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->goBack()V

    :cond_0
    return-void
.end method

.method private e()Z
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 3
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->isDisplayed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private synthetic f(Landroid/view/View;)V
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->l:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->getBackItem()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 13
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private f()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/forms/FormEditingBar;->isDisplayed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private synthetic g(Landroid/view/View;)V
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->l:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->getForwardItem()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 12
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private synthetic g(Z)V
    .locals 2

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/ui/redaction/RedactionView;->setRedactionButtonVisible(ZZ)V

    return-void
.end method

.method private g()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_4

    .line 3
    sget-object v3, Lcom/pspdfkit/internal/ui/c$f;->b:[I

    iget-object v4, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v4}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getTabBarHidingMode()Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget v3, v3, v4

    if-eq v3, v2, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v2, 0x4

    if-eq v3, v2, :cond_5

    goto :goto_3

    :cond_1
    :goto_1
    const/4 v1, 0x1

    goto :goto_4

    .line 9
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->e:Lcom/pspdfkit/ui/DocumentCoordinator;

    invoke-interface {v0}, Lcom/pspdfkit/ui/DocumentCoordinator;->getDocuments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_5

    goto :goto_2

    .line 10
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->e:Lcom/pspdfkit/ui/DocumentCoordinator;

    invoke-interface {v0}, Lcom/pspdfkit/ui/DocumentCoordinator;->getDocuments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_5

    :goto_2
    goto :goto_1

    :cond_4
    :goto_3
    move v1, v0

    :cond_5
    :goto_4
    return v1
.end method

.method private synthetic h(Z)V
    .locals 3

    .line 5
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->q:Z

    if-eqz v0, :cond_0

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->v()V

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    .line 10
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/c;->r(Z)V

    .line 12
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ui/c;->p(Z)V

    goto :goto_0

    .line 15
    :cond_1
    iget-boolean v2, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz v2, :cond_2

    .line 16
    new-instance v0, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/lang/Runnable;Z)V

    goto :goto_0

    .line 18
    :cond_2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/c;->p(Z)V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->g:Lcom/pspdfkit/internal/ui/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/a;->f()V

    .line 22
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->g:Lcom/pspdfkit/internal/ui/a;

    xor-int/2addr p1, v1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/a;->b(Z)V

    return-void
.end method

.method private h()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    if-eqz v0, :cond_1

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->k:Lcom/pspdfkit/internal/pg$c;

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/pg$c;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private i(Z)V
    .locals 10

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v1

    .line 4
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 5
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 7
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v3, 0xfa

    const-wide/16 v5, 0x0

    if-eqz p1, :cond_0

    move-wide v7, v3

    goto :goto_0

    :cond_0
    move-wide v7, v5

    .line 8
    :goto_0
    invoke-virtual {v2, v7, v8}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v7, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v8, 0x3fc00000    # 1.5f

    invoke-direct {v7, v8}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    .line 9
    invoke-virtual {v2, v7}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v7, 0x3f800000    # 1.0f

    .line 10
    invoke-virtual {v2, v7}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 11
    invoke-virtual {v2, v7}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v9, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda30;

    invoke-direct {v9, p0, v0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda30;-><init>(Lcom/pspdfkit/internal/ui/c;Landroid/view/View;)V

    .line 12
    invoke-virtual {v2, v9}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v2, 0x0

    .line 19
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 21
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move-wide v3, v5

    .line 22
    :goto_1
    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v8}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    .line 23
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 24
    invoke-virtual {p1, v7}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 25
    invoke-virtual {p1, v7}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda31;

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda31;-><init>(Lcom/pspdfkit/internal/ui/c;Landroid/view/View;)V

    .line 26
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 33
    invoke-virtual {p1, v2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 34
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 35
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/redaction/RedactionView;->setOnRedactionButtonVisibilityChangedListener(Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;)V

    .line 36
    :cond_2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->x()V

    return-void
.end method

.method private isUserInterfaceEnabled()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->o:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    sget-object v1, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_MANUAL:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    if-ne v0, v1, :cond_0

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    return v0

    .line 4
    :cond_0
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ui/c;->r:Z

    if-nez v1, :cond_1

    sget-object v1, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_HIDDEN:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    if-eq v0, v1, :cond_1

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->e()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private synthetic j()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private synthetic j(Z)V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    .line 4
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->isRedactionAnnotationPreviewEnabled()Z

    move-result v1

    .line 5
    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/redaction/RedactionView;->setRedactionAnnotationPreviewEnabled(Z)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/ui/redaction/RedactionView;->setRedactionButtonVisible(ZZ)V

    return-void
.end method

.method private synthetic k()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->p:Z

    return-void
.end method

.method private k(Z)V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v1}, Lcom/pspdfkit/internal/e8;->d(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    neg-int v1, v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    int-to-float v1, v1

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda13;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda13;-><init>(Lcom/pspdfkit/internal/ui/c;Z)V

    .line 8
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :cond_1
    return-void
.end method

.method private synthetic l()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->isUserInterfaceEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0, v0}, Lcom/pspdfkit/internal/ui/c;->setUserInterfaceVisible(ZZ)V

    :cond_0
    return-void
.end method

.method private l(Z)V
    .locals 3

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    if-eqz p1, :cond_1

    const-wide/16 v1, 0xfa

    goto :goto_0

    :cond_1
    const-wide/16 v1, 0x0

    .line 7
    :goto_0
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    .line 8
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const/4 v0, 0x0

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->d:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->getCurrentlyDisplayedContextualToolbar()Lcom/pspdfkit/ui/toolbar/ContextualToolbar;

    move-result-object v1

    .line 10
    iget-boolean v2, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-nez v2, :cond_3

    if-eqz v1, :cond_2

    .line 12
    invoke-virtual {v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->isDraggable()Z

    move-result v2

    if-nez v2, :cond_2

    .line 13
    invoke-virtual {v1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getPosition()Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;->TOP:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    if-ne v1, v2, :cond_2

    goto :goto_1

    .line 15
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->g:Lcom/pspdfkit/internal/ui/a;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/a;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v0}, Lcom/pspdfkit/internal/ce;->a(Landroidx/appcompat/app/AppCompatActivity;)I

    move-result v0

    goto :goto_2

    .line 17
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->d:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->getToolbarInset()I

    move-result v0

    :cond_4
    :goto_2
    int-to-float v0, v0

    .line 18
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    .line 19
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 20
    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method private synthetic m()V
    .locals 1

    const/4 v0, 0x0

    .line 4
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/c;->p(Z)V

    return-void
.end method

.method private synthetic n()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 3
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-wide v1, p0, Lcom/pspdfkit/internal/ui/c;->w:J

    .line 4
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 5
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method private n(Z)V
    .locals 2

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->c()Landroid/animation/AnimatorSet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 8
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/util/List;ZZ)V

    :cond_0
    return-void
.end method

.method private synthetic o()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private q()V
    .locals 2

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->y:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->x:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->onNext(Ljava/lang/Object;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->x:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->onComplete()V

    return-void
.end method

.method private r(Z)V
    .locals 4

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->n:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->a(Z)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 6
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/c;->n:Z

    .line 8
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->c()Landroid/animation/AnimatorSet;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v0, v3, v2

    const/4 v0, 0x1

    aput-object v1, v3, v0

    .line 12
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 14
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 16
    :goto_0
    invoke-direct {p0, v0, p1, v2}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/util/List;ZZ)V

    :cond_2
    return-void
.end method

.method private t()V
    .locals 6

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-nez v0, :cond_0

    return-void

    .line 14
    :cond_0
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ui/c;->s:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    const/4 v0, 0x1

    .line 16
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/c;->b(Z)I

    move-result v0

    .line 17
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/c;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    if-eqz v1, :cond_2

    .line 21
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfThumbnailBar;->isBackgroundTransparent()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    goto :goto_0

    .line 22
    :cond_2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 25
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    :goto_0
    add-int/2addr v1, v2

    goto :goto_1

    .line 26
    :cond_3
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 28
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    goto :goto_0

    .line 29
    :cond_4
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->g:Lcom/pspdfkit/internal/ui/a;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/a;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->k:Lcom/pspdfkit/internal/pg$c;

    if-eqz v1, :cond_5

    .line 32
    invoke-virtual {v1}, Lcom/pspdfkit/internal/pg$c;->a()I

    move-result v1

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    .line 33
    :goto_1
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    iget v4, p0, Lcom/pspdfkit/internal/ui/c;->t:I

    sub-int v4, v0, v4

    iget v5, p0, Lcom/pspdfkit/internal/ui/c;->u:I

    sub-int v5, v1, v5

    invoke-virtual {v3, v2, v4, v2, v5}, Lcom/pspdfkit/ui/PdfFragment;->addInsets(IIII)V

    .line 35
    iput v0, p0, Lcom/pspdfkit/internal/ui/c;->t:I

    .line 36
    iput v1, p0, Lcom/pspdfkit/internal/ui/c;->u:I

    goto :goto_2

    .line 38
    :cond_6
    iget v1, p0, Lcom/pspdfkit/internal/ui/c;->t:I

    neg-int v1, v1

    iget v3, p0, Lcom/pspdfkit/internal/ui/c;->u:I

    neg-int v3, v3

    invoke-virtual {v0, v2, v1, v2, v3}, Lcom/pspdfkit/ui/PdfFragment;->addInsets(IIII)V

    .line 39
    iput v2, p0, Lcom/pspdfkit/internal/ui/c;->u:I

    .line 40
    iput v2, p0, Lcom/pspdfkit/internal/ui/c;->t:I

    :goto_2
    return-void
.end method

.method private u()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ui/c;->v(Z)V

    goto :goto_1

    .line 4
    :cond_1
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ui/c;->f(Z)V

    :goto_1
    return-void
.end method

.method private v()V
    .locals 4

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->q:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->hideUserInterfaceWhenCreatingAnnotations()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    .line 5
    invoke-static {v0}, Lcom/pspdfkit/internal/zj;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->d:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->getCurrentlyDisplayedContextualToolbar()Lcom/pspdfkit/ui/toolbar/ContextualToolbar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ContextualToolbar;->getPosition()Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    move-result-object v0

    sget-object v3, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;->TOP:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;

    if-eq v0, v3, :cond_0

    .line 10
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ui/c;->p(Z)V

    const/4 v1, 0x1

    .line 14
    :cond_0
    iput-boolean v1, p0, Lcom/pspdfkit/internal/ui/c;->r:Z

    if-eqz v1, :cond_1

    .line 16
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/c;->hideUserInterface()V

    goto :goto_0

    .line 18
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/c;->showUserInterface()V

    .line 22
    :goto_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->A()Z

    move-result v0

    if-nez v0, :cond_2

    .line 23
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/ui/c;->c(Z)V

    goto :goto_1

    .line 25
    :cond_2
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/ui/c;->s(Z)V

    :goto_1
    return-void
.end method

.method private w()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->isInSpecialMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/c;->toggleUserInterface()V

    :cond_0
    return-void
.end method

.method private x()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v0}, Lcom/pspdfkit/internal/e8;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 3
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/redaction/RedactionView;->isRedactionButtonExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 8
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 9
    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/ui/redaction/RedactionView;->getRedactionButtonWidth()I

    move-result v1

    .line 10
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/redaction/RedactionView;->isButtonRedactionButtonVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 15
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    const/16 v2, 0x30

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v1

    .line 16
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 17
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    .line 18
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    :cond_2
    :goto_0
    return-void
.end method

.method private z()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->k:Lcom/pspdfkit/internal/pg$c;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/pg$c;->d()V

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    new-instance v1, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda8;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/pg;->a(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/internal/pg$d;)Lcom/pspdfkit/internal/pg$c;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/c;->k:Lcom/pspdfkit/internal/pg$c;

    return-void
.end method


# virtual methods
.method public final B()Z
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->A()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getActivityTitle()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getActivityTitle()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_2

    .line 7
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/c;->e:Lcom/pspdfkit/ui/DocumentCoordinator;

    invoke-interface {v2}, Lcom/pspdfkit/ui/DocumentCoordinator;->getVisibleDocument()Lcom/pspdfkit/ui/DocumentDescriptor;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 11
    invoke-virtual {v2, v0}, Lcom/pspdfkit/ui/DocumentDescriptor;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 12
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getDocumentTitleOverlayView()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v1}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 15
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0

    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .line 247
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 249
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getUserInterfaceViewMode()Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "userInterfaceViewMode"

    .line 250
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    move-result-object p1

    .line 254
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/c;->setUserInterfaceViewMode(Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;)V

    return-void
.end method

.method public final b()V
    .locals 3

    .line 27
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->a()V

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_0

    .line 29
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/c;->a(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->e:Lcom/pspdfkit/ui/DocumentCoordinator;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->E:Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;

    invoke-interface {v0, v1}, Lcom/pspdfkit/ui/DocumentCoordinator;->removeOnDocumentsChangedListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;)V

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->k:Lcom/pspdfkit/internal/pg$c;

    if-eqz v0, :cond_1

    .line 33
    invoke-virtual {v0}, Lcom/pspdfkit/internal/pg$c;->d()V

    const/4 v0, 0x0

    .line 34
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/c;->k:Lcom/pspdfkit/internal/pg$c;

    .line 35
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getThumbnailBarMode()Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_NONE:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    const/4 v2, 0x1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    .line 36
    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/ui/c;->r(Z)V

    .line 40
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->A:Lcom/pspdfkit/internal/ap;

    if-eqz v0, :cond_4

    .line 41
    invoke-interface {v0}, Lcom/pspdfkit/internal/ap;->g()V

    :cond_4
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->o:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    .line 48
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "userInterfaceViewMode"

    .line 49
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_0

    .line 4
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->a(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 9
    :goto_0
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-nez v0, :cond_2

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getUserInterfaceViewMode()Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/c;->setUserInterfaceViewMode(Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;)V

    .line 14
    :cond_2
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/ui/forms/FormEditingBar;->addOnFormEditingBarLifecycleListener(Lcom/pspdfkit/ui/forms/FormEditingBar$OnFormEditingBarLifecycleListener;)V

    .line 18
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;->addOnContentEditingBarLifecycleListener(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;)V

    .line 21
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/ui/audio/AudioView;->addOnAudioInspectorLifecycleListener(Lcom/pspdfkit/ui/audio/AudioView$AudioInspectorLifecycleListener;)V

    .line 23
    :cond_5
    invoke-virtual {p1, v1, v1, v1, v1}, Lcom/pspdfkit/ui/PdfFragment;->setInsets(IIII)V

    .line 26
    new-instance v0, Lcom/pspdfkit/internal/ui/c$c;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/ui/c$c;-><init>(Lcom/pspdfkit/internal/ui/c;Lcom/pspdfkit/ui/PdfFragment;)V

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    return-void
.end method

.method public final c(Z)V
    .locals 4

    .line 68
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getDocumentTitleOverlayView()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 70
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 72
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    if-eqz p1, :cond_0

    const-wide/16 v2, 0xfa

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x0

    .line 73
    :goto_0
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-direct {v1, v2}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    .line 74
    invoke-virtual {p1, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 75
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v1, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    .line 76
    invoke-virtual {p1, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v1, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda22;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda22;-><init>(Landroid/view/View;)V

    .line 77
    invoke-virtual {p1, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 80
    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_1
    return-void
.end method

.method public final d()Lcom/pspdfkit/internal/ui/a;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->g:Lcom/pspdfkit/internal/ui/a;

    return-object v0
.end method

.method public final d(Z)V
    .locals 10

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v1

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 7
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 8
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 10
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v3, 0xfa

    const-wide/16 v5, 0x0

    if-eqz p1, :cond_0

    move-wide v7, v3

    goto :goto_0

    :cond_0
    move-wide v7, v5

    .line 11
    :goto_0
    invoke-virtual {v2, v7, v8}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v7, Landroid/view/animation/AccelerateInterpolator;

    const v8, 0x3fb33333    # 1.4f

    invoke-direct {v7, v8}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    .line 12
    invoke-virtual {v2, v7}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v7, 0x0

    .line 13
    invoke-virtual {v2, v7}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 14
    invoke-virtual {v2, v7}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v9, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda23;

    invoke-direct {v9, v0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda23;-><init>(Landroid/view/View;)V

    .line 15
    invoke-virtual {v2, v9}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v2, 0x0

    .line 17
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 19
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move-wide v3, v5

    .line 20
    :goto_1
    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v8}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    .line 21
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 22
    invoke-virtual {p1, v7}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 23
    invoke-virtual {p1, v7}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda24;

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda24;-><init>(Landroid/view/View;)V

    .line 24
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 26
    invoke-virtual {p1, v2}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 27
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 28
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/redaction/RedactionView;->setOnRedactionButtonVisibilityChangedListener(Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;)V

    .line 29
    :cond_2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->x()V

    :cond_3
    return-void
.end method

.method public final e(Z)V
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda10;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/internal/ui/c;Z)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public final f(Z)V
    .locals 3

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    if-eqz p1, :cond_0

    const-wide/16 v1, 0xfa

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    .line 7
    :goto_0
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-direct {v0, v1}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    .line 8
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 9
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda7;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    .line 10
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 11
    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_1
    return-void
.end method

.method public final getUserInterfaceViewMode()Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->o:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    return-object v0
.end method

.method public final hideUserInterface()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz v0, :cond_5

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->r:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->o:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    sget-object v3, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_VISIBLE:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_MANUAL:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    if-eq v0, v3, :cond_0

    goto :goto_1

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 10
    invoke-interface {v0}, Lcom/pspdfkit/ui/search/PdfSearchView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSearchType()I

    move-result v0

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 13
    :goto_0
    iget-boolean v3, p0, Lcom/pspdfkit/internal/ui/c;->p:Z

    if-nez v3, :cond_4

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_2

    .line 15
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getSelectedFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->d:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    .line 16
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->isDisplayingContextualToolbar()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->o:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    sget-object v3, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_VISIBLE:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    if-eq v0, v3, :cond_4

    sget-object v3, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_MANUAL:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    if-ne v0, v3, :cond_3

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v0, 0x1

    :goto_3
    if-nez v0, :cond_5

    .line 17
    invoke-virtual {p0, v1, v2}, Lcom/pspdfkit/internal/ui/c;->setUserInterfaceVisible(ZZ)V

    :cond_5
    return-void
.end method

.method public final i()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->n:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isUserInterfaceVisible()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    return v0
.end method

.method public final m(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->g:Lcom/pspdfkit/internal/ui/a;

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/a;->f()V

    goto :goto_0

    .line 3
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    :goto_0
    return-void
.end method

.method public final o(Z)V
    .locals 1

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/c;->z:Z

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 4
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/c;->u(Z)V

    .line 5
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/c;->t(Z)V

    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/c;->e(Z)V

    .line 8
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/c;->d(Z)V

    .line 11
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    if-eqz v0, :cond_1

    .line 12
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method public final onContextualToolbarPositionChanged(Lcom/pspdfkit/ui/toolbar/ContextualToolbar;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout$LayoutParams$Position;)V
    .locals 0

    .line 1
    instance-of p1, p1, Lcom/pspdfkit/ui/toolbar/AnnotationCreationToolbar;

    if-eqz p1, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->v()V

    :cond_0
    return-void
.end method

.method public final onDisplayAudioInspector(Lcom/pspdfkit/ui/audio/AudioView;)V
    .locals 0

    return-void
.end method

.method public final onDisplayContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V
    .locals 0

    const/4 p1, 0x1

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/c;->p(Z)V

    return-void
.end method

.method public final onDisplayFormEditingBar(Lcom/pspdfkit/ui/forms/FormEditingBar;)V
    .locals 0

    const/4 p1, 0x1

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/c;->p(Z)V

    return-void
.end method

.method public final onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/listeners/SimpleDocumentListener;->onDocumentLoadFailed(Ljava/lang/Throwable;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->C:Lcom/pspdfkit/ui/PdfThumbnailBar;

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    .line 3
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/listeners/SimpleDocumentListener;->onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v0

    const/4 v2, -0x1

    if-le v0, v2, :cond_0

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2, v0}, Lcom/pspdfkit/ui/PdfFragment;->getSiblingPageIndex(I)I

    move-result v2

    .line 5
    :cond_0
    invoke-direct {p0, v0, v2, v1}, Lcom/pspdfkit/internal/ui/c;->a(IIZ)V

    .line 6
    :cond_1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz v0, :cond_3

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 8
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 9
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/ui/search/PdfSearchView;->isShown()Z

    move-result v0

    if-nez v0, :cond_3

    .line 11
    :cond_2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->p:Z

    xor-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/c;->s(Z)V

    .line 12
    :cond_3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz v0, :cond_4

    .line 13
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->p:Z

    xor-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/c;->v(Z)V

    .line 16
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->A:Lcom/pspdfkit/internal/ap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_5

    .line 18
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/ag;->getViewCoordinator()Lcom/pspdfkit/internal/xm;

    move-result-object v0

    const/4 v1, 0x0

    .line 19
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/xm;->a(Z)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 20
    new-instance v1, Lcom/pspdfkit/internal/ui/c$e;

    invoke-direct {v1, p0, p1, v0}, Lcom/pspdfkit/internal/ui/c$e;-><init>(Lcom/pspdfkit/internal/ui/c;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/internal/views/document/DocumentView;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/internal/views/document/DocumentView$g;)V

    .line 30
    :cond_5
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->t()V

    return-void
.end method

.method public final onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 3

    .line 1
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/listeners/SimpleDocumentListener;->onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->o:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    .line 3
    sget-object v1, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_AUTOMATIC_BORDER_PAGES:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_1

    if-eqz p2, :cond_0

    .line 4
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p1

    sub-int/2addr p1, v2

    if-ne p2, p1, :cond_1

    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/c;->showUserInterface()V

    .line 6
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageNumberOverlay()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_4

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz p1, :cond_3

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p1

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_4

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz p1, :cond_4

    const/4 v0, 0x1

    :cond_4
    if-eqz v0, :cond_5

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/ui/PdfFragment;->getSiblingPageIndex(I)I

    move-result p1

    invoke-direct {p0, p2, p1, v2}, Lcom/pspdfkit/internal/ui/c;->a(IIZ)V

    :cond_5
    return-void
.end method

.method public final onPrepareAudioInspector(Lcom/pspdfkit/ui/audio/AudioView;)V
    .locals 0

    const/4 p1, 0x1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->n(Z)V

    return-void
.end method

.method public final onPrepareContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V
    .locals 1

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/lang/Runnable;Z)V

    :cond_0
    const/4 p1, 0x1

    .line 4
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->n(Z)V

    return-void
.end method

.method public final onPrepareFormEditingBar(Lcom/pspdfkit/ui/forms/FormEditingBar;)V
    .locals 1

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/lang/Runnable;Z)V

    :cond_0
    const/4 p1, 0x1

    .line 4
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->n(Z)V

    return-void
.end method

.method public final onRedactionButtonCollapsing()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v0}, Lcom/pspdfkit/internal/e8;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 3
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0xfa

    .line 6
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    const/16 v2, 0x30

    .line 7
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 9
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    return-void
.end method

.method public final onRedactionButtonExpanding()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v0}, Lcom/pspdfkit/internal/e8;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 3
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0xfa

    .line 6
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 7
    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/ui/redaction/RedactionView;->getRedactionButtonWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 9
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    return-void
.end method

.method public final onRedactionButtonSlidingInside()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v0}, Lcom/pspdfkit/internal/e8;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 3
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0xfa

    .line 6
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    const/16 v2, 0x30

    .line 7
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 9
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    return-void
.end method

.method public final onRedactionButtonSlidingOutside()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v0}, Lcom/pspdfkit/internal/e8;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0xfa

    .line 4
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 5
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    return-void
.end method

.method public final onRemoveAudioInspector(Lcom/pspdfkit/ui/audio/AudioView;)V
    .locals 0

    const/4 p1, 0x0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->n(Z)V

    return-void
.end method

.method public final onRemoveContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V
    .locals 2

    const/4 p1, 0x0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/c;->p(Z)V

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 3
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/lang/Runnable;Z)V

    .line 5
    :cond_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->n(Z)V

    return-void
.end method

.method public final onRemoveFormEditingBar(Lcom/pspdfkit/ui/forms/FormEditingBar;)V
    .locals 2

    const/4 p1, 0x0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/c;->p(Z)V

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 3
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/lang/Runnable;Z)V

    .line 5
    :cond_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->n(Z)V

    return-void
.end method

.method public final p()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->k:Lcom/pspdfkit/internal/pg$c;

    if-nez v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->z()V

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/c;->y()V

    return-void
.end method

.method public final p(Z)V
    .locals 3

    if-nez p1, :cond_2

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->o:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    sget-object v1, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_VISIBLE:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    const/4 v2, 0x1

    if-eq v0, v1, :cond_1

    .line 8
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 9
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 10
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getActiveViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_SEARCH:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSearchType()I

    move-result v0

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :cond_1
    :goto_0
    if-nez v2, :cond_3

    .line 12
    :cond_2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/c;->s:Z

    .line 14
    :cond_3
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->t()V

    return-void
.end method

.method public final q(Z)V
    .locals 1

    .line 4
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->q:Z

    if-ne p1, v0, :cond_0

    return-void

    .line 5
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/c;->q:Z

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->v()V

    return-void
.end method

.method public final r()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->i:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda25;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda25;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->i:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda25;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda25;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final s()V
    .locals 3

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 10
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->g()Z

    move-result v1

    if-nez v1, :cond_4

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v1}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$bool;->pspdf__display_document_title_in_actionbar:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowDocumentTitleOverlayEnabled()Z

    move-result v1

    if-nez v1, :cond_4

    .line 13
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getActivityTitle()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getActivityTitle()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 15
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v1, :cond_3

    .line 16
    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 18
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/c;->e:Lcom/pspdfkit/ui/DocumentCoordinator;

    invoke-interface {v2}, Lcom/pspdfkit/ui/DocumentCoordinator;->getVisibleDocument()Lcom/pspdfkit/ui/DocumentDescriptor;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 20
    invoke-virtual {v2, v1}, Lcom/pspdfkit/ui/DocumentDescriptor;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    .line 21
    :goto_0
    invoke-static {v1}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    const-string v1, ""

    .line 23
    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method

.method public final s(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getDocumentTitleOverlayView()Landroid/widget/TextView;

    move-result-object v0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->A()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v1, :cond_0

    .line 4
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/c;->B()Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    .line 6
    :cond_1
    new-instance v1, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda11;

    invoke-direct {v1, p0, v0, p1}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda11;-><init>(Lcom/pspdfkit/internal/ui/c;Landroid/view/View;Z)V

    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/lang/Runnable;)V

    :cond_2
    return-void
.end method

.method public final setUserInterfaceViewMode(Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;)V
    .locals 4

    if-eqz p1, :cond_6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->o:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    if-ne v0, p1, :cond_0

    goto :goto_2

    .line 3
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/c;->o:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->d:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->setMainToolbarEnabled(Z)V

    .line 6
    sget-object v0, Lcom/pspdfkit/internal/ui/c$f;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v0, v0, v2

    const/4 v2, 0x0

    if-eq v0, v1, :cond_3

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    goto :goto_0

    .line 14
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    if-eqz v0, :cond_2

    .line 16
    iput-boolean v2, p0, Lcom/pspdfkit/internal/ui/c;->p:Z

    .line 18
    new-instance v0, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda12;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda12;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/lang/Runnable;)V

    .line 24
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->d:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->setMainToolbarEnabled(Z)V

    goto :goto_0

    .line 25
    :cond_3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/c;->showUserInterface()V

    .line 26
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->o:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    sget-object v3, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_VISIBLE:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    if-eq v0, v3, :cond_5

    .line 27
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->f()Z

    move-result v0

    if-nez v0, :cond_5

    .line 28
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->e()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 29
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getActiveViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    sget-object v3, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_SEARCH:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-ne v0, v3, :cond_4

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 30
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSearchType()I

    move-result v0

    if-ne v0, v1, :cond_4

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    .line 31
    :cond_5
    :goto_1
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ui/c;->p(Z)V

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->h:Lcom/pspdfkit/internal/ui/c$g;

    if-eqz v0, :cond_6

    .line 35
    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/ui/c$g;->onUserInterfaceViewModeChanged(Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;)V

    :cond_6
    :goto_2
    return-void
.end method

.method public final setUserInterfaceVisible(ZZ)V
    .locals 7

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->a()V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->d:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    const-wide/16 v3, 0x0

    if-eqz p2, :cond_1

    const-wide/16 v5, 0xfa

    goto :goto_0

    :cond_1
    const-wide/16 v5, 0x0

    :goto_0
    move v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->toggleMainToolbarVisibility(ZJJ)V

    if-eqz p1, :cond_2

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/c;->y()V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->g:Lcom/pspdfkit/internal/ui/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/a;->d()V

    goto :goto_1

    .line 11
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 13
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->g:Lcom/pspdfkit/internal/ui/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/a;->a()V

    .line 14
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->t()V

    .line 17
    :goto_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_4

    .line 21
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ui/c;->t(Z)V

    goto :goto_2

    .line 23
    :cond_4
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ui/c;->d(Z)V

    .line 27
    :goto_2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/c;->a(Z)Landroid/animation/AnimatorSet;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 29
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/c;->n:Z

    .line 30
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    :cond_5
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->c()Landroid/animation/AnimatorSet;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 36
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    :cond_6
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageNumberOverlay()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    goto :goto_3

    :cond_7
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_9

    .line 38
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v1, :cond_8

    .line 39
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v1

    goto :goto_4

    :cond_8
    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_9

    .line 40
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    goto :goto_5

    :cond_9
    const/4 v1, 0x0

    :goto_5
    if-eqz v1, :cond_c

    .line 41
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v1, :cond_c

    if-eqz p1, :cond_b

    .line 44
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v1

    const/4 v2, -0x1

    if-le v1, v2, :cond_a

    .line 45
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/ui/PdfFragment;->getSiblingPageIndex(I)I

    move-result v2

    .line 46
    :cond_a
    invoke-direct {p0, v1, v2, p2}, Lcom/pspdfkit/internal/ui/c;->a(IIZ)V

    goto :goto_6

    .line 49
    :cond_b
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 50
    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object v1

    const/4 v4, 0x2

    new-array v4, v4, [F

    iget-object v5, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 52
    check-cast v5, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/sm;->getPageNumberOverlayView()Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/TextView;->getAlpha()F

    move-result v5

    aput v5, v4, v3

    const/4 v3, 0x0

    aput v3, v4, v2

    const-string v2, "alpha"

    .line 53
    invoke-static {v1, v2, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    :goto_6
    if-eqz p1, :cond_d

    .line 63
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ui/c;->s(Z)V

    goto :goto_7

    .line 65
    :cond_d
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ui/c;->c(Z)V

    :goto_7
    if-eqz p1, :cond_e

    .line 70
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ui/c;->v(Z)V

    goto :goto_8

    .line 72
    :cond_e
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ui/c;->f(Z)V

    .line 76
    :goto_8
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/c;->h:Lcom/pspdfkit/internal/ui/c$g;

    if-eqz v1, :cond_f

    .line 77
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/ui/c$g;->onUserInterfaceVisibilityChanged(Z)V

    :cond_f
    if-eqz p1, :cond_10

    .line 82
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ui/c;->u(Z)V

    goto :goto_9

    .line 84
    :cond_10
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ui/c;->e(Z)V

    .line 88
    :goto_9
    invoke-direct {p0, v0, p1, p2}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/util/List;ZZ)V

    return-void
.end method

.method public final showUserInterface()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->isUserInterfaceEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, v0, v0}, Lcom/pspdfkit/internal/ui/c;->setUserInterfaceVisible(ZZ)V

    :cond_0
    return-void
.end method

.method public final t(Z)V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->z:Z

    if-eqz v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 3
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowNavigationButtonsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 6
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getActiveViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    sget-object v3, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_4

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->l:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    if-eqz v0, :cond_2

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->getForwardItem()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->l:Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->getBackItem()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :cond_3
    :goto_2
    if-eqz v1, :cond_4

    .line 9
    new-instance v0, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda26;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda26;-><init>(Lcom/pspdfkit/internal/ui/c;Z)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/lang/Runnable;)V

    :cond_4
    return-void
.end method

.method public final toggleUserInterface()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->isUserInterfaceEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->g:Lcom/pspdfkit/internal/ui/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->g:Lcom/pspdfkit/internal/ui/a;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/a;->a()V

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getActiveViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->d:Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/ToolbarCoordinatorLayout;->isDisplayingContextualToolbar()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 10
    :cond_1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz v0, :cond_2

    .line 11
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/c;->hideUserInterface()V

    goto :goto_0

    .line 13
    :cond_2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/c;->showUserInterface()V

    :goto_0
    return-void

    .line 14
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->b:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    :cond_4
    return-void
.end method

.method public final u(Z)V
    .locals 2

    .line 5
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->j:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 8
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->A:Lcom/pspdfkit/internal/ap;

    if-eqz v0, :cond_0

    .line 10
    invoke-interface {v0}, Lcom/pspdfkit/internal/ap;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->f:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isRedactionUiEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->REDACTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->c:Lcom/pspdfkit/internal/yf;

    .line 13
    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getActiveViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 14
    new-instance v0, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda9;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/internal/ui/c;Z)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method public final v(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->m:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/c;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/ui/c;Z)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/c;->a(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method final y()V
    .locals 4

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/c;->p:Z

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c;->i:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda14;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ui/c$$ExternalSyntheticLambda14;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.class final Lcom/pspdfkit/internal/ui/f$g;
.super Lcom/pspdfkit/ui/search/SimpleSearchResultListener;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "g"
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/ui/search/SearchResultHighlighter;

.field final synthetic b:Lcom/pspdfkit/internal/ui/f;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/ui/search/SearchResultHighlighter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f$g;->b:Lcom/pspdfkit/internal/ui/f;

    invoke-direct {p0}, Lcom/pspdfkit/ui/search/SimpleSearchResultListener;-><init>()V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/f$g;->a:Lcom/pspdfkit/ui/search/SearchResultHighlighter;

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/ui/search/SearchResultHighlighter;Lcom/pspdfkit/internal/ui/f$g-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ui/f$g;-><init>(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/ui/search/SearchResultHighlighter;)V

    return-void
.end method


# virtual methods
.method public final onMoreSearchResults(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$g;->a:Lcom/pspdfkit/ui/search/SearchResultHighlighter;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->addSearchResults(Ljava/util/List;)V

    return-void
.end method

.method public final onSearchCleared()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$g;->a:Lcom/pspdfkit/ui/search/SearchResultHighlighter;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->clearSearchResults()V

    return-void
.end method

.method public final onSearchResultSelected(Lcom/pspdfkit/document/search/SearchResult;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$g;->a:Lcom/pspdfkit/ui/search/SearchResultHighlighter;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/search/SearchResultHighlighter;->setSelectedSearchResult(Lcom/pspdfkit/document/search/SearchResult;)V

    if-eqz p1, :cond_0

    .line 4
    iget-object v0, p1, Lcom/pspdfkit/document/search/SearchResult;->textBlock:Lcom/pspdfkit/datastructures/TextBlock;

    iget-object v0, v0, Lcom/pspdfkit/datastructures/TextBlock;->pageRects:Ljava/util/List;

    invoke-static {v0}, Lcom/pspdfkit/internal/di;->a(Ljava/util/List;)Landroid/graphics/RectF;

    move-result-object v2

    .line 6
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v0

    neg-float v0, v0

    const v1, 0x3dcccccd    # 0.1f

    mul-float v0, v0, v1

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v3

    neg-float v3, v3

    mul-float v3, v3, v1

    invoke-virtual {v2, v0, v3}, Landroid/graphics/RectF;->inset(FF)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$g;->b:Lcom/pspdfkit/internal/ui/f;

    iget-object v1, v0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    iget v3, p1, Lcom/pspdfkit/document/search/SearchResult;->pageIndex:I

    const-wide/16 v4, 0xc8

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/pspdfkit/ui/PdfFragment;->scrollTo(Landroid/graphics/RectF;IJZ)V

    :cond_0
    return-void
.end method

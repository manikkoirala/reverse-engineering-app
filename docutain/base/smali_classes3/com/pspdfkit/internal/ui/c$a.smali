.class final Lcom/pspdfkit/internal/ui/c$a;
.super Lcom/pspdfkit/internal/zr;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/pspdfkit/internal/ui/c;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ui/c;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/c$a;->b:Lcom/pspdfkit/internal/ui/c;

    invoke-direct {p0}, Lcom/pspdfkit/internal/zr;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDocumentAdded(Lcom/pspdfkit/ui/DocumentDescriptor;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c$a;->b:Lcom/pspdfkit/internal/ui/c;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$mu(Lcom/pspdfkit/internal/ui/c;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c$a;->b:Lcom/pspdfkit/internal/ui/c;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetj(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/c;->s()V

    :cond_0
    return-void
.end method

.method public final onDocumentRemoved(Lcom/pspdfkit/ui/DocumentDescriptor;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c$a;->b:Lcom/pspdfkit/internal/ui/c;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$mu(Lcom/pspdfkit/internal/ui/c;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c$a;->b:Lcom/pspdfkit/internal/ui/c;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetj(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/c;->s()V

    :cond_0
    return-void
.end method

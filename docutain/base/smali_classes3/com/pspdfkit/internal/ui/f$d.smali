.class final Lcom/pspdfkit/internal/ui/f$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic b:Lcom/pspdfkit/internal/ui/f;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/ui/f;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f$d;->b:Lcom/pspdfkit/internal/ui/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/internal/ui/f$d-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/f$d;-><init>(Lcom/pspdfkit/internal/ui/f;)V

    return-void
.end method


# virtual methods
.method public final onDocumentScrolled(Lcom/pspdfkit/ui/PdfFragment;IIIIII)V
    .locals 0

    return-void
.end method

.method public final onScrollStateChanged(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/listeners/scrolling/ScrollState;)V
    .locals 0

    .line 1
    sget-object p1, Lcom/pspdfkit/listeners/scrolling/ScrollState;->DRAGGED:Lcom/pspdfkit/listeners/scrolling/ScrollState;

    if-ne p2, p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$d;->b:Lcom/pspdfkit/internal/ui/f;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/f;->-$$Nest$fgetuserInterfaceCoordinator(Lcom/pspdfkit/internal/ui/f;)Lcom/pspdfkit/internal/ui/c;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/c;->hideUserInterface()V

    :cond_0
    return-void
.end method

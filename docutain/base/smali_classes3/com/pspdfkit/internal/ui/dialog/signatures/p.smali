.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private final b:F

.field final synthetic c:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/p;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->c(Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;)Landroid/widget/EditText;

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, "typeSignature"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextSize()F

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/p;->b:F

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    const/4 p2, 0x0

    if-eqz p1, :cond_1

    .line 1
    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p3, 0x1

    :goto_1
    const/4 p4, 0x0

    if-eqz p3, :cond_2

    .line 2
    iget p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/p;->b:F

    goto :goto_2

    .line 4
    :cond_2
    iget-object p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/p;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    invoke-static {p3}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;)Landroid/widget/TextView;

    move-result-object p3

    const-string v0, "autosizeHelper"

    if-nez p3, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p3, p4

    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Landroid/widget/TextView$BufferType;->EDITABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p3, p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/p;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;)Landroid/widget/TextView;

    move-result-object p1

    if-nez p1, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, p4

    :cond_4
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextSize()F

    move-result p1

    .line 7
    :goto_2
    iget-object p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/p;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    invoke-static {p3}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->c(Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;)Landroid/widget/EditText;

    move-result-object p3

    if-nez p3, :cond_5

    const-string p3, "typeSignature"

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    move-object p4, p3

    :goto_3
    invoke-virtual {p4, p2, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    return-void
.end method

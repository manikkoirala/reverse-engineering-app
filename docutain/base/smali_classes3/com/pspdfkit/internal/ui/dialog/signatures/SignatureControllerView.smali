.class public Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView$a;
    }
.end annotation


# static fields
.field private static final l:[I

.field private static final m:I

.field private static final n:I


# instance fields
.field b:Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView$a;

.field c:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private d:I

.field private e:I

.field private f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private h:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private final i:Ljava/util/HashMap;

.field private j:I

.field private k:Z


# direct methods
.method public static synthetic $r8$lambda$gKBt2stDNV1EEB0j8aazUzteUe4(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;ZILio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->a(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;ZILio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$j2Bg9wLxsqxxyd_9ge-JeOFA7mk(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;ZLio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->a(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;ZLio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout:[I

    sput-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->l:[I

    .line 3
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__signatureLayoutStyle:I

    sput v0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->m:I

    .line 4
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_SignatureLayout:I

    sput v0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->n:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->i:Ljava/util/HashMap;

    const/4 v0, 0x0

    .line 6
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->k:Z

    .line 10
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 11
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 12
    new-instance p2, Ljava/util/HashMap;

    const/4 v0, 0x3

    invoke-direct {p2, v0}, Ljava/util/HashMap;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->i:Ljava/util/HashMap;

    const/4 p2, 0x0

    .line 16
    iput-boolean p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->k:Z

    .line 25
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    new-instance p2, Ljava/util/HashMap;

    const/4 p3, 0x3

    invoke-direct {p2, p3}, Ljava/util/HashMap;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->i:Ljava/util/HashMap;

    const/4 p2, 0x0

    .line 31
    iput-boolean p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->k:Z

    .line 45
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;I)Lio/reactivex/rxjava3/core/Completable;
    .locals 3

    .line 49
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/ov;->c(Landroid/content/Context;)Z

    move-result v0

    .line 50
    invoke-static {}, Lio/reactivex/rxjava3/subjects/CompletableSubject;->create()Lio/reactivex/rxjava3/subjects/CompletableSubject;

    move-result-object v1

    .line 51
    new-instance v2, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView$$ExternalSyntheticLambda1;

    invoke-direct {v2, p1, v0, p2, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView$$ExternalSyntheticLambda1;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;ZILio/reactivex/rxjava3/subjects/CompletableSubject;)V

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/subjects/CompletableSubject;->doOnSubscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method private static a(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;Z)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    .line 58
    invoke-static {}, Lio/reactivex/rxjava3/subjects/CompletableSubject;->create()Lio/reactivex/rxjava3/subjects/CompletableSubject;

    move-result-object v0

    .line 59
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView$$ExternalSyntheticLambda0;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;ZLio/reactivex/rxjava3/subjects/CompletableSubject;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/CompletableSubject;->doOnSubscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    return-object p0
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__signature_layout_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->e:I

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__signature_canvas_controller_picker_circles_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->j:I

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__signature_canvas_controller_picker_circles_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->d:I

    .line 8
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->l:[I

    sget v2, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->m:I

    sget v3, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->n:I

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 9
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__signatureInkColorPrimary:I

    const/high16 v2, -0x1000000

    .line 10
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 11
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__signatureInkColorSecondary:I

    const/high16 v3, -0x10000

    .line 12
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 13
    sget v3, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__signatureInkColorTertiary:I

    const v4, -0xffff01

    .line 14
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 15
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 17
    new-instance v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-direct {v0, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 18
    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->i:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 23
    new-instance v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-direct {v0, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 24
    invoke-static {v2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->i:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 29
    new-instance v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-direct {v0, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->h:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 30
    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 31
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->h:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->i:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->h:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->h:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 39
    iget p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->d:I

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 40
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, p1, p1}, Landroid/view/View;->measure(II)V

    .line 41
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, p1, p1}, Landroid/view/View;->measure(II)V

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->h:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, p1, p1}, Landroid/view/View;->measure(II)V

    .line 45
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->c:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 46
    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    .line 47
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setAlpha(F)V

    .line 48
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->h:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setAlpha(F)V

    return-void
.end method

.method private static synthetic a(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;ZILio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 52
    invoke-static {p0}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p0

    if-eqz p1, :cond_0

    neg-int p1, p2

    int-to-float p1, p1

    goto :goto_0

    :cond_0
    int-to-float p1, p2

    .line 53
    :goto_0
    invoke-virtual {p0, p1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationX(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p0

    const/high16 p1, 0x3f800000    # 1.0f

    .line 54
    invoke-virtual {p0, p1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p0

    const-wide/16 p1, 0x12c

    .line 55
    invoke-virtual {p0, p1, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p0

    new-instance p1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 56
    invoke-virtual {p0, p1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p0

    .line 57
    invoke-static {p3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda0;

    invoke-direct {p1, p3}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda0;-><init>(Lio/reactivex/rxjava3/subjects/CompletableSubject;)V

    invoke-virtual {p0, p1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method

.method private static synthetic a(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;ZLio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 60
    invoke-static {p0}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p0

    const/4 p3, 0x0

    .line 61
    invoke-virtual {p0, p3}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationX(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p0

    if-eqz p1, :cond_0

    const/high16 p3, 0x3f800000    # 1.0f

    .line 62
    :cond_0
    invoke-virtual {p0, p3}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p0

    const-wide/16 v0, 0x12c

    .line 63
    invoke-virtual {p0, v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p0

    new-instance p1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 64
    invoke-virtual {p0, p1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p0

    .line 65
    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda0;

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda0;-><init>(Lio/reactivex/rxjava3/subjects/CompletableSubject;)V

    invoke-virtual {p0, p1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-eq p1, v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-eq p1, v1, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->h:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-ne p1, v1, :cond_6

    .line 2
    :cond_0
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->k:Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_5

    .line 3
    move-object v0, p1

    check-cast v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->c:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView$a;

    if-eqz v0, :cond_1

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->i:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->a(I)V

    .line 6
    :cond_1
    iput-boolean v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->k:Z

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->c:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-ne v0, v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->a(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;Z)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iget-object v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->c:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-ne v1, v4, :cond_3

    const/4 v4, 0x1

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    .line 9
    :goto_1
    invoke-static {v1, v4}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->a(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;Z)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->h:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iget-object v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->c:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-ne v1, v4, :cond_4

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    .line 10
    :goto_2
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->a(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;Z)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_3

    .line 12
    :cond_5
    iput-boolean v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->k:Z

    .line 14
    invoke-direct {p0, v0, v3}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->a(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;I)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->d:I

    iget v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->j:I

    add-int/2addr v2, v3

    .line 15
    invoke-direct {p0, v1, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->a(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;I)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->h:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->d:I

    iget v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->j:I

    add-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x2

    .line 16
    invoke-direct {p0, v1, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->a(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;I)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    .line 18
    :cond_6
    :goto_3
    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    return-void
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView$a;

    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/ov;->c(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    iget p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->e:I

    sub-int/2addr p1, p2

    iget p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->d:I

    sub-int/2addr p1, p3

    goto :goto_0

    .line 5
    :cond_0
    iget p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->e:I

    move p1, p2

    .line 9
    :goto_0
    iget p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->d:I

    add-int p4, p1, p3

    add-int/2addr p3, p2

    .line 15
    iget-object p5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p5, p1, p2, p4, p3}, Landroid/view/View;->layout(IIII)V

    .line 16
    iget-object p5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p5, p1, p2, p4, p3}, Landroid/view/View;->layout(IIII)V

    .line 17
    iget-object p5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->h:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p5, p1, p2, p4, p3}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method protected final onMeasure(II)V
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->d:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    mul-int v1, v1, v0

    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->j:I

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->e:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 2
    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->d:I

    add-int/2addr v1, v2

    const/4 v2, 0x0

    .line 3
    invoke-static {v0, p1, v2}, Landroid/view/ViewGroup;->resolveSizeAndState(III)I

    move-result p1

    .line 4
    invoke-static {v1, p2, v2}, Landroid/view/ViewGroup;->resolveSizeAndState(III)I

    move-result p2

    .line 5
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public setCurrentlySelectedColor(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 3
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->c:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 4
    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView$a;

    return-void
.end method

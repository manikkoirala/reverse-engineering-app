.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/n;
.super Landroidx/cardview/widget/CardView;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/n$c;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/n$a;
    }
.end annotation


# instance fields
.field private b:Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;

.field private c:Ljava/util/ArrayList;

.field private d:Ljava/lang/String;

.field private e:Lcom/pspdfkit/internal/ui/dialog/signatures/n$a;


# direct methods
.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/n;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->c:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/ui/dialog/signatures/n;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->d:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/n;)Lcom/pspdfkit/internal/ui/dialog/signatures/n$a;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/n$a;

    return-object p0
.end method

.method public constructor <init>(Landroid/view/ContextThemeWrapper;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroidx/cardview/widget/CardView;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->a()V

    return-void
.end method

.method private a()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__signer_list_view_popup:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signerRecyclerList:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/n;Lcom/pspdfkit/internal/ui/dialog/signatures/n$b-IA;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;

    .line 4
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 5
    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v3, v2, v4}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    return-void
.end method


# virtual methods
.method protected final onMeasure(II)V
    .locals 1

    .line 1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    const/high16 v0, 0x40000000    # 2.0f

    .line 2
    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 3
    invoke-super {p0, p1, p2}, Landroidx/cardview/widget/CardView;->onMeasure(II)V

    return-void
.end method

.method public setOnSignerClickedListener(Lcom/pspdfkit/internal/ui/dialog/signatures/n$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/n$a;

    return-void
.end method

.method public setSelectedSignerIdentifier(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    return-void

    :cond_0
    if-eqz v0, :cond_1

    .line 3
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 4
    :cond_1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->d:Ljava/lang/String;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->c:Ljava/util/ArrayList;

    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/m;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/m;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setSigners(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/signatures/signers/Signer;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->c:Ljava/util/ArrayList;

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->d:Ljava/lang/String;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/m;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/m;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

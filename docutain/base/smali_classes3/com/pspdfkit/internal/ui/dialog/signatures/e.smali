.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/e;
.super Landroidx/recyclerview/widget/RecyclerView$AdapterDataObserver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

.field final synthetic b:Landroidx/recyclerview/widget/RecyclerView;

.field final synthetic c:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ui/dialog/signatures/d;Landroidx/recyclerview/widget/RecyclerView;Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/e;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/e;->b:Landroidx/recyclerview/widget/RecyclerView;

    iput-object p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/e;->c:Landroid/widget/TextView;

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$AdapterDataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/e;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->getItemCount()I

    move-result v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/e;->b:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/e;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/e;->b:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/e;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public final onItemRangeRemoved(II)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/e;->onChanged()V

    return-void
.end method

.class public Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$d;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$e;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;
    }
.end annotation


# instance fields
.field b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$e;

.field c:Lcom/pspdfkit/internal/hl;

.field private d:I

.field private e:I

.field private f:Z

.field private g:Lcom/pspdfkit/internal/ui/dialog/signatures/o;

.field private final h:Ljava/util/HashMap;

.field private i:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

.field private j:I

.field private k:Z

.field private l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

.field private m:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$d;


# direct methods
.method public static synthetic $r8$lambda$1wb7yiOect9auTOj2-wb91X7wYg(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;ILio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(ILio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$KuTSAHkHXo0WUauKc587kfcBtTE(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;Landroid/view/View;ZLio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Landroid/view/View;ZLio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$_IvTyArkAmynYkLna34FVCPElT8(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;Lio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Lio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$y11SK9mpW9G0hSSmZwiXoq3sIaE(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;Landroid/view/View;ILio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Landroid/view/View;ILio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 1
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->f:Z

    .line 24
    new-instance v1, Ljava/util/HashMap;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    .line 30
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->k:Z

    .line 32
    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1, v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    .line 41
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->f:Z

    .line 63
    new-instance v1, Ljava/util/HashMap;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    .line 69
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->k:Z

    .line 71
    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    .line 83
    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .line 84
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    .line 85
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->f:Z

    .line 107
    new-instance v1, Ljava/util/HashMap;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    .line 113
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->k:Z

    .line 115
    sget-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    .line 132
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private a()Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    .line 125
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 128
    :cond_0
    invoke-static {}, Lio/reactivex/rxjava3/subjects/CompletableSubject;->create()Lio/reactivex/rxjava3/subjects/CompletableSubject;

    move-result-object v0

    .line 129
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;Lio/reactivex/rxjava3/subjects/CompletableSubject;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/CompletableSubject;->doOnSubscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    return-object v0

    .line 130
    :cond_1
    :goto_0
    invoke-static {}, Lio/reactivex/rxjava3/core/Completable;->complete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    .line 99
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 102
    :cond_0
    invoke-static {}, Lio/reactivex/rxjava3/subjects/CompletableSubject;->create()Lio/reactivex/rxjava3/subjects/CompletableSubject;

    move-result-object v0

    .line 103
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;ILio/reactivex/rxjava3/subjects/CompletableSubject;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/CompletableSubject;->doOnSubscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1

    .line 104
    :cond_1
    :goto_0
    invoke-static {}, Lio/reactivex/rxjava3/core/Completable;->complete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method private a(Landroid/view/View;I)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    .line 87
    invoke-static {}, Lio/reactivex/rxjava3/subjects/CompletableSubject;->create()Lio/reactivex/rxjava3/subjects/CompletableSubject;

    move-result-object v0

    .line 88
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1, p2, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;Landroid/view/View;ILio/reactivex/rxjava3/subjects/CompletableSubject;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/CompletableSubject;->doOnSubscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method private a(Landroid/view/View;Z)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    .line 114
    invoke-static {}, Lio/reactivex/rxjava3/subjects/CompletableSubject;->create()Lio/reactivex/rxjava3/subjects/CompletableSubject;

    move-result-object v0

    .line 115
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0, p1, p2, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;Landroid/view/View;ZLio/reactivex/rxjava3/subjects/CompletableSubject;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/CompletableSubject;->doOnSubscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method private a(ILio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 105
    iget-object p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/o;

    invoke-static {p3}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p3

    .line 106
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/ov;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    neg-int p1, p1

    :cond_0
    int-to-float p1, p1

    .line 108
    invoke-virtual {p3, p1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationX(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    goto :goto_0

    :cond_1
    neg-int p1, p1

    int-to-float p1, p1

    .line 110
    invoke-virtual {p3, p1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationY(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    :goto_0
    const-wide/16 v0, 0x12c

    .line 111
    invoke-virtual {p1, v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    new-instance p3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 112
    invoke-virtual {p1, p3}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    .line 113
    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p3, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda0;

    invoke-direct {p3, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda0;-><init>(Lio/reactivex/rxjava3/subjects/CompletableSubject;)V

    invoke-virtual {p1, p3}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/R$styleable;->pspdf__ElectronicSignatureControllerView:[I

    const/4 v2, 0x0

    .line 2
    invoke-virtual {v0, p2, v1, p3, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 3
    sget p3, Lcom/pspdfkit/R$styleable;->pspdf__ElectronicSignatureControllerView_pspdf__fontSelectionVisible:I

    invoke-virtual {p2, p3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    iput-boolean p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->f:Z

    .line 5
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_layout_padding:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    float-to-int p2, p2

    iput p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->e:I

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_canvas_controller_picker_circles_padding:I

    .line 9
    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    float-to-int p2, p2

    iput p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->j:I

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_canvas_controller_picker_circles_size:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    float-to-int p2, p2

    iput p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    .line 12
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/b;->a(Landroid/content/Context;)V

    .line 14
    new-instance p2, Lcom/pspdfkit/internal/ui/dialog/signatures/o;

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/o;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/o;

    .line 15
    sget p1, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_font_selection_spinner:I

    invoke-virtual {p2, p1}, Landroid/view/View;->setId(I)V

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/o;

    sget p2, Lcom/pspdfkit/R$drawable;->pspdf__electronic_signature_tt_icon_selector:I

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 17
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getAvailableFonts(Landroid/content/Context;)Ljava/util/Set;

    move-result-object p1

    .line 18
    new-instance p2, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$d;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p2, p3, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$d;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    const p1, 0x1090009

    .line 19
    invoke-virtual {p2, p1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 20
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->m:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$d;

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/o;

    invoke-virtual {p1, p2}, Landroid/widget/AbsSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/o;

    new-instance p2, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$a;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$a;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;)V

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/o;->setSpinnerEventsListener(Lcom/pspdfkit/internal/ui/dialog/signatures/o$a;)V

    .line 37
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/o;

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 39
    iget p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    const/high16 p2, 0x40000000    # 2.0f

    invoke-static {p1, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 40
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/o;

    invoke-virtual {p2, p1, p1}, Landroid/view/View;->measure(II)V

    .line 41
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/o;

    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    return-void
.end method

.method private a(Landroid/view/View;ILio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 89
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    .line 90
    iget-object p4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    sget-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    invoke-virtual {p4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_1

    .line 91
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p4

    invoke-static {p4}, Lcom/pspdfkit/internal/ov;->c(Landroid/content/Context;)Z

    move-result p4

    if-eqz p4, :cond_0

    neg-int p2, p2

    :cond_0
    int-to-float p2, p2

    .line 92
    invoke-virtual {p1, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationX(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    goto :goto_0

    :cond_1
    neg-int p2, p2

    int-to-float p2, p2

    .line 94
    invoke-virtual {p1, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationY(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    :goto_0
    const/high16 p2, 0x3f800000    # 1.0f

    .line 95
    invoke-virtual {p1, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    const-wide/16 v0, 0x12c

    .line 96
    invoke-virtual {p1, v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    new-instance p2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 97
    invoke-virtual {p1, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    .line 98
    invoke-static {p3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p2, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda0;

    invoke-direct {p2, p3}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda0;-><init>(Lio/reactivex/rxjava3/subjects/CompletableSubject;)V

    invoke-virtual {p1, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method

.method private a(Landroid/view/View;ZLio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 116
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    .line 117
    iget-object p4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    sget-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    invoke-virtual {p4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p4

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    .line 118
    invoke-virtual {p1, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationX(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    goto :goto_0

    .line 120
    :cond_0
    invoke-virtual {p1, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationY(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    :goto_0
    if-eqz p2, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 121
    :cond_1
    invoke-virtual {p1, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    const-wide/16 v0, 0x12c

    .line 122
    invoke-virtual {p1, v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    new-instance p2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 123
    invoke-virtual {p1, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    .line 124
    invoke-static {p3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p2, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda0;

    invoke-direct {p2, p3}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda0;-><init>(Lio/reactivex/rxjava3/subjects/CompletableSubject;)V

    invoke-virtual {p1, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method

.method private a(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;Z)V
    .locals 5

    .line 80
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 81
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 82
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    .line 83
    :goto_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object v3, v3, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setSelected(Z)V

    if-eqz p2, :cond_3

    .line 84
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object v3, v3, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    if-eqz v2, :cond_2

    const/high16 v4, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    :cond_3
    if-eqz v2, :cond_0

    .line 85
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object v1, v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    goto :goto_0

    :cond_4
    return-void

    .line 86
    :cond_5
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "Signature color options have not been initialized correctly."

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method private a(Lio/reactivex/rxjava3/subjects/CompletableSubject;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 131
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/o;

    invoke-static {p2}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    .line 132
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {p2, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationX(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    goto :goto_0

    .line 135
    :cond_0
    invoke-virtual {p2, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationY(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    :goto_0
    const-wide/16 v0, 0x12c

    .line 136
    invoke-virtual {p2, v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 137
    invoke-virtual {p2, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    .line 138
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda0;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$$ExternalSyntheticLambda0;-><init>(Lio/reactivex/rxjava3/subjects/CompletableSubject;)V

    invoke-virtual {p2, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;)V
    .locals 8

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    new-instance v2, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    sget v3, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_color_option_primary:I

    .line 43
    new-instance v4, Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 44
    invoke-virtual {v4, v3}, Landroid/view/View;->setId(I)V

    .line 45
    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 47
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;->option1(Landroid/content/Context;)I

    move-result v3

    invoke-direct {v2, v4, v3}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;-><init>(Landroid/view/View;I)V

    .line 48
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    sget-object v2, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    new-instance v3, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    sget v4, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_color_option_secondary:I

    .line 54
    new-instance v5, Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 55
    invoke-virtual {v5, v4}, Landroid/view/View;->setId(I)V

    .line 56
    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    invoke-virtual {p0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 58
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {p1, v4}, Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;->option2(Landroid/content/Context;)I

    move-result v4

    invoke-direct {v3, v5, v4}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;-><init>(Landroid/view/View;I)V

    .line 59
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    sget-object v3, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    new-instance v4, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    sget v5, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_color_option_tertiary:I

    .line 65
    new-instance v6, Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 66
    invoke-virtual {v6, v5}, Landroid/view/View;->setId(I)V

    .line 67
    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 69
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {p1, v5}, Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;->option3(Landroid/content/Context;)I

    move-result p1

    invoke-direct {v4, v6, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;-><init>(Landroid/view/View;I)V

    .line 70
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result p1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 72
    iget p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 73
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object v0, v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    invoke-virtual {v0, p1, p1}, Landroid/view/View;->measure(II)V

    .line 74
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object v0, v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    invoke-virtual {v0, p1, p1}, Landroid/view/View;->measure(II)V

    .line 75
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object v0, v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    invoke-virtual {v0, p1, p1}, Landroid/view/View;->measure(II)V

    .line 77
    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    const/4 p1, 0x1

    .line 78
    invoke-direct {p0, v1, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;Z)V

    return-void

    .line 79
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Signature color options have not been initialized correctly."

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method public getOrientation()Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    return-object v0
.end method

.method public getSelectedFont()Lcom/pspdfkit/ui/fonts/Font;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/o;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/fonts/Font;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object v2, v2, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    if-ne p1, v2, :cond_0

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->k:Z

    const-string v0, "Signature color options have not been initialized correctly."

    const/4 v2, 0x3

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_6

    .line 4
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    .line 5
    invoke-direct {p0, p1, v4}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;Z)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$e;

    if-eqz p1, :cond_1

    .line 7
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget v1, v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->b:I

    invoke-interface {p1, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$e;->a(I)V

    .line 8
    :cond_1
    iput-boolean v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->k:Z

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result p1

    if-ne p1, v2, :cond_5

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    sget-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    .line 11
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object p1, p1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 12
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Landroid/view/View;Z)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    .line 16
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object v0, v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    .line 17
    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Landroid/view/View;Z)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    .line 21
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object v0, v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    if-ne v1, v2, :cond_4

    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    .line 22
    :goto_2
    invoke-direct {p0, v0, v3}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Landroid/view/View;Z)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 25
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 26
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_3

    .line 27
    :cond_5
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 28
    :cond_6
    iput-boolean v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->k:Z

    .line 29
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result p1

    if-ne p1, v2, :cond_7

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    sget-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object p1, p1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    invoke-direct {p0, p1, v4}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Landroid/view/View;I)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    .line 32
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object v0, v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    iget v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->j:I

    add-int/2addr v1, v2

    .line 33
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Landroid/view/View;I)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    .line 37
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object v0, v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    iget v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->j:I

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x2

    .line 38
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Landroid/view/View;I)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    iget v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->j:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x2

    .line 41
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(I)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 42
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_3

    .line 43
    :cond_7
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_8
    :goto_3
    return-void
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$e;

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->c:Lcom/pspdfkit/internal/hl;

    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/ov;->c(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    iget p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->e:I

    sub-int/2addr p1, p2

    iget p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    sub-int/2addr p1, p2

    goto :goto_0

    .line 4
    :cond_0
    iget p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->e:I

    .line 6
    :goto_0
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    sget-object p3, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    invoke-virtual {p2, p3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 7
    iget p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->e:I

    goto :goto_1

    .line 9
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    iget p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->e:I

    sub-int/2addr p2, p3

    iget p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    sub-int/2addr p2, p3

    .line 12
    :goto_1
    iget p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    add-int p4, p1, p3

    add-int/2addr p3, p2

    .line 18
    iget-object p5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    invoke-virtual {p5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p5

    invoke-interface {p5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p5

    :goto_2
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 19
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget-object v0, v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->a:Landroid/view/View;

    invoke-virtual {v0, p1, p2, p4, p3}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    .line 22
    :cond_2
    iget-boolean p5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->f:Z

    if-eqz p5, :cond_4

    .line 23
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p5

    invoke-static {p5}, Lcom/pspdfkit/internal/ov;->c(Landroid/content/Context;)Z

    move-result p5

    if-eqz p5, :cond_3

    .line 24
    iget-object p4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/o;

    iget p5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->j:I

    sub-int/2addr p1, p5

    iget p5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    sub-int p5, p1, p5

    invoke-virtual {p4, p5, p2, p1, p3}, Landroid/view/View;->layout(IIII)V

    goto :goto_3

    .line 30
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/o;

    iget p5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->j:I

    add-int/2addr p4, p5

    iget p5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    add-int/2addr p5, p4

    invoke-virtual {p1, p4, p2, p5, p3}, Landroid/view/View;->layout(IIII)V

    :cond_4
    :goto_3
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x2

    if-eqz v0, :cond_1

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    .line 3
    :goto_0
    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    mul-int v3, v3, v2

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->j:I

    mul-int v0, v0, v2

    add-int/2addr v0, v3

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->e:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 6
    iget v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    add-int/2addr v2, v1

    goto :goto_2

    .line 9
    :cond_1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->f:Z

    if-eqz v0, :cond_2

    .line 10
    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    mul-int/lit8 v2, v0, 0x2

    iget v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->e:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v2

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->j:I

    add-int/2addr v2, v3

    .line 11
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    mul-int v3, v3, v0

    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->j:I

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v3

    iget v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->e:I

    goto :goto_1

    .line 13
    :cond_2
    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->d:I

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->e:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v0

    .line 14
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    mul-int v3, v3, v0

    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->j:I

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v3

    iget v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->e:I

    :goto_1
    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    move v4, v2

    move v2, v0

    move v0, v4

    :goto_2
    const/4 v1, 0x0

    .line 17
    invoke-static {v0, p1, v1}, Landroid/view/ViewGroup;->resolveSizeAndState(III)I

    move-result p1

    .line 18
    invoke-static {v2, p2, v1}, Landroid/view/ViewGroup;->resolveSizeAndState(III)I

    move-result p2

    .line 19
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public setCurrentlySelectedColor(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;

    iget v2, v2, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$c;->b:I

    if-ne v2, p1, :cond_0

    .line 3
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;

    const/4 v2, 0x1

    .line 4
    invoke-direct {p0, v1, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$b;Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$e;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$e;

    return-void
.end method

.method public setOnFontSelectionListener(Lcom/pspdfkit/internal/hl;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->c:Lcom/pspdfkit/internal/hl;

    return-void
.end method

.method public setOrientation(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    if-eq v0, p1, :cond_0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setTypedSignature(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->m:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$d;

    if-eqz v0, :cond_0

    .line 2
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$d;->-$$Nest$fputd(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$d;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

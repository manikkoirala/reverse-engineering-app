.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/d;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;
.implements Lcom/pspdfkit/internal/pa;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

.field private c:Lcom/pspdfkit/internal/qa;

.field private d:Landroidx/viewpager2/widget/ViewPager2;

.field private final e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

.field private final f:Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;

.field private g:Landroid/widget/RelativeLayout;

.field private final h:Landroid/widget/RelativeLayout$LayoutParams;

.field private i:Lcom/pspdfkit/internal/or;

.field private j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

.field private k:Landroidx/appcompat/widget/AppCompatImageButton;

.field private l:Z

.field private m:Z

.field private n:I

.field private o:Z

.field private p:I

.field private q:Landroid/view/View;

.field private r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private t:Z


# direct methods
.method public static synthetic $r8$lambda$E3gsWvVsexpLFMQmRHCV2CoEIWQ(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V

    return-void
.end method

.method public static synthetic $r8$lambda$N5g8wueJG6kzoZHwIGddNYewgCw(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->k(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V

    return-void
.end method

.method public static synthetic $r8$lambda$nQijU1VKcjFqXj6joGo4tZaQXHE(Lcom/pspdfkit/internal/ui/dialog/signatures/d;Lcom/google/android/material/tabs/TabLayout$Tab;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/d;Lcom/google/android/material/tabs/TabLayout$Tab;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureOptions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    .line 17
    new-instance p2, Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-direct {p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    .line 20
    new-instance p2, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->f:Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;

    .line 26
    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p2, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->h:Landroid/widget/RelativeLayout$LayoutParams;

    const/4 p2, 0x1

    .line 44
    iput-boolean p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->m:Z

    .line 69
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a(Landroid/content/Context;)V

    return-void
.end method

.method private final a()V
    .locals 5

    .line 229
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    .line 230
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->t:Z

    if-eqz v0, :cond_0

    .line 231
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->o:Z

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v0, "titleView"

    const-string v3, "backButton"

    const/4 v4, 0x0

    if-eqz v1, :cond_3

    .line 233
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->k:Landroidx/appcompat/widget/AppCompatImageButton;

    if-nez v1, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v4

    :cond_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 234
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez v1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v4, v1

    :goto_1
    invoke-virtual {v4}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->a()V

    goto :goto_3

    .line 236
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->k:Landroidx/appcompat/widget/AppCompatImageButton;

    if-nez v1, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v4

    :cond_4
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 237
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez v1, :cond_5

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    move-object v4, v1

    :goto_2
    invoke-virtual {v4}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->e()V

    :goto_3
    return-void
.end method

.method private final a(Landroid/content/Context;)V
    .locals 11

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    monitor-enter v0

    .line 2
    :try_start_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/hb;->b()Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;->ELECTRONICSIGNATURES:Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    if-eqz v1, :cond_2b

    .line 3
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/utils/b;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/utils/b;-><init>(Landroid/content/Context;)V

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/utils/b;->getCornerRadius()I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->n:I

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;)V

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->p:I

    .line 9
    invoke-virtual {p0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 10
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ui/dialog/utils/b;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    .line 11
    sget v2, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_layout_title_view:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    const-string v1, "titleView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_1
    iget-boolean v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->t:Z

    if-eqz v5, :cond_2

    sget v5, Lcom/pspdfkit/R$string;->pspdf__add_signature:I

    goto :goto_1

    :cond_2
    sget v5, Lcom/pspdfkit/R$string;->pspdf__signatures:I

    :goto_1
    invoke-virtual {v1, v5}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(I)V

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez v1, :cond_3

    const-string v1, "titleView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_3
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->f:Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;

    invoke-virtual {v1, v5}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setBackButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez v1, :cond_4

    const-string v1, "titleView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_4
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->f:Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;

    invoke-virtual {v1, v5}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setCloseButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez v1, :cond_5

    const-string v1, "titleView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    .line 17
    :cond_5
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x2

    const/4 v7, -0x1

    invoke-direct {v5, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 18
    invoke-virtual {p0, v1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 25
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->h:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez v5, :cond_6

    const-string v5, "titleView"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v5, v2

    :cond_6
    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v5

    const/4 v6, 0x3

    invoke-virtual {v1, v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 26
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 27
    sget v5, Lcom/pspdfkit/R$layout;->pspdf__recycler_view_with_empty_message:I

    invoke-virtual {v1, v5, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const-string v5, "from(getContext())\n     \u2026pty_message, this, false)"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->q:Landroid/view/View;

    if-nez v1, :cond_7

    const-string v1, "signatureItemsListLayout"

    .line 30
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_7
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->h:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 31
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->q:Landroid/view/View;

    if-nez v1, :cond_8

    const-string v1, "signatureItemsListLayout"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_8
    iget-boolean v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->t:Z

    const/16 v8, 0x8

    if-eqz v5, :cond_9

    const/16 v5, 0x8

    goto :goto_2

    :cond_9
    const/4 v5, 0x0

    :goto_2
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 32
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->q:Landroid/view/View;

    if-nez v1, :cond_a

    const-string v1, "signatureItemsListLayout"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_a
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 34
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->q:Landroid/view/View;

    if-nez v1, :cond_b

    const-string v1, "signatureItemsListLayout"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_b
    sget v5, Lcom/pspdfkit/R$id;->pspdf__empty_text:I

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 35
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->getItemCount()I

    move-result v5

    if-nez v5, :cond_c

    const/4 v5, 0x0

    goto :goto_3

    :cond_c
    const/16 v5, 0x8

    :goto_3
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 36
    sget v5, Lcom/pspdfkit/R$string;->pspdf__no_signatures:I

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 38
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->q:Landroid/view/View;

    if-nez v5, :cond_d

    const-string v5, "signatureItemsListLayout"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v5, v2

    :cond_d
    sget v9, Lcom/pspdfkit/R$id;->pspdf__recycler_view:I

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const-string v9, "signatureItemsListLayout\u2026.id.pspdf__recycler_view)"

    invoke-static {v5, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Landroidx/recyclerview/widget/RecyclerView;

    .line 39
    sget v9, Lcom/pspdfkit/R$id;->pspdf__signature_items_list:I

    invoke-virtual {v5, v9}, Landroid/view/View;->setId(I)V

    .line 40
    iget-object v9, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v5, v9}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 41
    new-instance v9, Landroidx/recyclerview/widget/DefaultItemAnimator;

    invoke-direct {v9}, Landroidx/recyclerview/widget/DefaultItemAnimator;-><init>()V

    invoke-virtual {v5, v9}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    .line 42
    new-instance v9, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v9, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v9}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 43
    new-instance v9, Lcom/pspdfkit/internal/oo;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/pspdfkit/internal/oo;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v9}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 44
    iget-object v9, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v9}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->getItemCount()I

    move-result v9

    if-nez v9, :cond_e

    const/16 v9, 0x8

    goto :goto_4

    :cond_e
    const/4 v9, 0x0

    :goto_4
    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 46
    iget-object v9, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    .line 47
    new-instance v10, Lcom/pspdfkit/internal/ui/dialog/signatures/e;

    invoke-direct {v10, p0, v5, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/e;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/d;Landroidx/recyclerview/widget/RecyclerView;Landroid/widget/TextView;)V

    .line 48
    invoke-virtual {v9, v10}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->registerAdapterDataObserver(Landroidx/recyclerview/widget/RecyclerView$AdapterDataObserver;)V

    .line 49
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g:Landroid/widget/RelativeLayout;

    .line 50
    sget v5, Lcom/pspdfkit/R$id;->pspdf__electronic_signature_layout_add_new_signature_container:I

    invoke-virtual {v1, v5}, Landroid/view/View;->setId(I)V

    .line 51
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g:Landroid/widget/RelativeLayout;

    if-nez v1, :cond_f

    const-string v1, "addSignatureView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_f
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->h:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g:Landroid/widget/RelativeLayout;

    if-nez v1, :cond_10

    const-string v1, "addSignatureView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_10
    iget-boolean v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->t:Z

    if-eqz v5, :cond_11

    const/4 v8, 0x0

    :cond_11
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 54
    new-instance v1, Landroidx/viewpager2/widget/ViewPager2;

    invoke-direct {v1, p1}, Landroidx/viewpager2/widget/ViewPager2;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->d:Landroidx/viewpager2/widget/ViewPager2;

    .line 55
    sget p1, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_view_pager:I

    invoke-virtual {v1, p1}, Landroid/view/View;->setId(I)V

    .line 56
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->d:Landroidx/viewpager2/widget/ViewPager2;

    if-nez p1, :cond_12

    const-string p1, "viewPager"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_12
    invoke-virtual {p1, v4}, Landroidx/viewpager2/widget/ViewPager2;->setUserInputEnabled(Z)V

    .line 57
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->d:Landroidx/viewpager2/widget/ViewPager2;

    if-nez p1, :cond_13

    const-string p1, "viewPager"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_13
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroidx/viewpager2/widget/ViewPager2;->setOffscreenPageLimit(I)V

    .line 58
    new-instance p1, Lcom/pspdfkit/internal/qa;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-direct {p1, p0, v1}, Lcom/pspdfkit/internal/qa;-><init>(Lcom/pspdfkit/internal/pa;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c:Lcom/pspdfkit/internal/qa;

    .line 59
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->d:Landroidx/viewpager2/widget/ViewPager2;

    if-nez p1, :cond_14

    const-string p1, "viewPager"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_14
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c:Lcom/pspdfkit/internal/qa;

    if-nez v1, :cond_15

    const-string v1, "electronicSignatureViewPagerAdapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_15
    invoke-virtual {p1, v1}, Landroidx/viewpager2/widget/ViewPager2;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 61
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a(Lcom/pspdfkit/internal/ui/dialog/utils/b;)V

    .line 63
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p1, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 67
    sget v0, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_view_pager_tab_layout:I

    invoke-virtual {p1, v6, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 68
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->d:Landroidx/viewpager2/widget/ViewPager2;

    if-nez v0, :cond_16

    const-string v0, "viewPager"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_16
    invoke-virtual {v0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g:Landroid/widget/RelativeLayout;

    if-nez p1, :cond_17

    const-string p1, "addSignatureView"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_17
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->d:Landroidx/viewpager2/widget/ViewPager2;

    if-nez v0, :cond_18

    const-string v0, "viewPager"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_18
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 70
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g:Landroid/widget/RelativeLayout;

    if-nez p1, :cond_19

    const-string p1, "addSignatureView"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_19
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 71
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a()V

    .line 74
    invoke-virtual {p0, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 75
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    .line 76
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/16 v0, 0x38

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    .line 77
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 78
    iput-boolean v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 p1, 0xc

    .line 79
    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 p1, 0x15

    .line 80
    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 81
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/16 v1, 0x10

    invoke-static {p1, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    .line 82
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 84
    new-instance p1, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 85
    sget v1, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_signature_fab_add_new_signature:I

    invoke-virtual {p1, v1}, Landroid/view/View;->setId(I)V

    .line 86
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_1a

    const-string p1, "addNewSignatureLayoutFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    .line 88
    :cond_1a
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v5, 0x4

    .line 89
    invoke-static {v1, v5}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v1

    int-to-float v1, v1

    .line 90
    invoke-virtual {p1, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setCompatElevation(F)V

    .line 95
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_1b

    const-string p1, "addNewSignatureLayoutFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_1b
    invoke-virtual {p1, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setUseCompatPadding(Z)V

    .line 96
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_1c

    const-string p1, "addNewSignatureLayoutFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_1c
    invoke-virtual {p1, v4}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setSize(I)V

    .line 97
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_1d

    const-string p1, "addNewSignatureLayoutFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_1d
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v4, Lcom/pspdfkit/R$color;->pspdf__color:I

    invoke-static {v1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 98
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_1e

    const-string p1, "addNewSignatureLayoutFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_1e
    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_add:I

    invoke-virtual {p1, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setImageResource(I)V

    .line 99
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_1f

    const-string p1, "addNewSignatureLayoutFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_1f
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v4, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    invoke-static {v1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setColorFilter(I)V

    .line 100
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_20

    const-string p1, "addNewSignatureLayoutFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_20
    invoke-virtual {p1, v3}, Landroid/view/View;->setClickable(Z)V

    .line 101
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_21

    const-string p1, "addNewSignatureLayoutFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_21
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->f:Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_22

    const-string p1, "addNewSignatureLayoutFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_22
    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 104
    new-instance p1, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 105
    sget v1, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_fab_delete_selected_signatures:I

    invoke-virtual {p1, v1}, Landroid/view/View;->setId(I)V

    .line 106
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_23

    const-string p1, "deleteSelectedSignaturesFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_23
    invoke-virtual {p1, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setUseCompatPadding(Z)V

    .line 107
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_24

    const-string p1, "deleteSelectedSignaturesFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    .line 108
    :cond_24
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 109
    invoke-static {v1, v5}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setCompatElevation(F)V

    .line 113
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_25

    const-string p1, "deleteSelectedSignaturesFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    .line 115
    :cond_25
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 116
    sget v4, Lcom/pspdfkit/R$color;->pspdf__color_red_light:I

    .line 117
    invoke-static {v1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 118
    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 124
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_26

    const-string p1, "deleteSelectedSignaturesFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_26
    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_delete:I

    invoke-virtual {p1, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setImageResource(I)V

    .line 125
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_27

    const-string p1, "deleteSelectedSignaturesFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_27
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v4, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    invoke-static {v1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setColorFilter(I)V

    .line 126
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_28

    const-string p1, "deleteSelectedSignaturesFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_28
    invoke-virtual {p1, v3}, Landroid/view/View;->setClickable(Z)V

    .line 127
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_29

    const-string p1, "deleteSelectedSignaturesFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_29
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->f:Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_2a

    const-string p1, "deleteSelectedSignaturesFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_5

    :cond_2a
    move-object v2, p1

    :goto_5
    invoke-virtual {p0, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 130
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e()V

    return-void

    .line 131
    :cond_2b
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Creating signature annotations requires Electronic Signatures."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public static final a(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V
    .locals 6

    .line 273
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "deleteSelectedSignaturesFab"

    .line 274
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    .line 275
    :cond_0
    new-instance v2, Lcom/pspdfkit/internal/mq;

    const/4 v3, 0x1

    const-wide/16 v4, 0x64

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    .line 276
    invoke-static {v2}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    const-string v2, "create(\n            Scal\u2026)\n            )\n        )"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p0, :cond_1

    const-string p0, "addNewSignatureLayoutFab"

    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, p0

    .line 278
    :goto_0
    new-instance p0, Lcom/pspdfkit/internal/mq;

    const/4 v3, 0x2

    invoke-direct {p0, v1, v3, v4, v5}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    .line 279
    invoke-static {p0}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 280
    invoke-virtual {v0, p0}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    .line 281
    invoke-virtual {p0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/ui/dialog/signatures/d;IZ)V
    .locals 0

    .line 218
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c:Lcom/pspdfkit/internal/qa;

    if-nez p0, :cond_0

    const-string p0, "electronicSignatureViewPagerAdapter"

    .line 219
    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/qa;->a(I)Lcom/pspdfkit/internal/ui/dialog/signatures/f;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/f;->setActive(Z)V

    :cond_1
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/ui/dialog/signatures/d;Lcom/google/android/material/tabs/TabLayout$Tab;I)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tab"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c:Lcom/pspdfkit/internal/qa;

    const/4 v1, 0x0

    if-nez p0, :cond_0

    const-string p0, "electronicSignatureViewPagerAdapter"

    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p0, v1

    :cond_0
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/qa;->b(I)I

    move-result p0

    .line 216
    invoke-static {v0, p0, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p0

    .line 217
    invoke-virtual {p1, p0}, Lcom/google/android/material/tabs/TabLayout$Tab;->setText(Ljava/lang/CharSequence;)Lcom/google/android/material/tabs/TabLayout$Tab;

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/ui/dialog/utils/b;)V
    .locals 9

    .line 132
    new-instance v0, Landroidx/appcompat/widget/AppCompatImageButton;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/appcompat/widget/AppCompatImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->k:Landroidx/appcompat/widget/AppCompatImageButton;

    .line 133
    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_arrow_back:I

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageButton;->setImageResource(I)V

    .line 134
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->k:Landroidx/appcompat/widget/AppCompatImageButton;

    const-string v1, "backButton"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_0
    sget v3, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_back_button:I

    invoke-virtual {v0, v3}, Landroid/view/View;->setId(I)V

    .line 135
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->k:Landroidx/appcompat/widget/AppCompatImageButton;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->f:Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->k:Landroidx/appcompat/widget/AppCompatImageButton;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/utils/b;->getTitleColor()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 137
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v3, 0x30

    invoke-static {v0, v3}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v0

    .line 138
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->k:Landroidx/appcompat/widget/AppCompatImageButton;

    if-nez v3, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v3, v2

    :cond_3
    invoke-virtual {v3, v0}, Landroid/view/View;->setMinimumHeight(I)V

    .line 139
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->k:Landroidx/appcompat/widget/AppCompatImageButton;

    if-nez v3, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v3, v2

    :cond_4
    invoke-virtual {v3, v0}, Landroid/view/View;->setMinimumWidth(I)V

    .line 140
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->k:Landroidx/appcompat/widget/AppCompatImageButton;

    if-nez v3, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v3, v2

    :cond_5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroidx/appcompat/widget/AppCompatImageButton;->setElevation(F)V

    .line 145
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v3}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_b

    .line 146
    new-instance v3, Lcom/google/android/material/tabs/TabLayout;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v3, v6}, Lcom/google/android/material/tabs/TabLayout;-><init>(Landroid/content/Context;)V

    .line 147
    sget v6, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_view_pager_tab_layout:I

    invoke-virtual {v3, v6}, Landroid/view/View;->setId(I)V

    const/4 v6, 0x0

    .line 148
    invoke-virtual {v3, v6}, Lcom/google/android/material/tabs/TabLayout;->setTabGravity(I)V

    .line 151
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/utils/b;->getTitleColor()I

    move-result v7

    .line 152
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/utils/b;->getTitleTextColor()I

    move-result p1

    .line 153
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v5}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3, v5}, Lcom/google/android/material/tabs/TabLayout;->setElevation(F)V

    .line 154
    invoke-virtual {v3, v0}, Landroid/view/View;->setMinimumHeight(I)V

    .line 156
    invoke-virtual {v3, v7}, Landroid/view/View;->setBackgroundColor(I)V

    const/16 v0, 0xcc

    .line 157
    invoke-static {p1, v0}, Landroidx/core/graphics/ColorUtils;->setAlphaComponent(II)I

    move-result v0

    invoke-virtual {v3, v0, p1}, Lcom/google/android/material/tabs/TabLayout;->setTabTextColors(II)V

    .line 158
    invoke-virtual {v3, p1}, Lcom/google/android/material/tabs/TabLayout;->setSelectedTabIndicatorColor(I)V

    .line 159
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget v0, Lcom/pspdfkit/R$color;->pspdf__electronic_signature_ripple_background_color:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 160
    new-instance v0, Landroid/content/res/ColorStateList;

    new-array v5, v4, [[I

    new-array v7, v6, [I

    aput-object v7, v5, v6

    new-array v7, v4, [I

    aput p1, v7, v6

    invoke-direct {v0, v5, v7}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    invoke-virtual {v3, v0}, Lcom/google/android/material/tabs/TabLayout;->setTabRippleColor(Landroid/content/res/ColorStateList;)V

    .line 161
    invoke-virtual {v3, v4}, Lcom/google/android/material/tabs/TabLayout;->setUnboundedRipple(Z)V

    .line 163
    new-instance p1, Lcom/google/android/material/tabs/TabLayoutMediator;

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->d:Landroidx/viewpager2/widget/ViewPager2;

    if-nez v0, :cond_6

    const-string v0, "viewPager"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    .line 167
    :cond_6
    new-instance v5, Lcom/pspdfkit/internal/ui/dialog/signatures/d$$ExternalSyntheticLambda0;

    invoke-direct {v5, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V

    .line 168
    invoke-direct {p1, v3, v0, v5}, Lcom/google/android/material/tabs/TabLayoutMediator;-><init>(Lcom/google/android/material/tabs/TabLayout;Landroidx/viewpager2/widget/ViewPager2;Lcom/google/android/material/tabs/TabLayoutMediator$TabConfigurationStrategy;)V

    .line 172
    invoke-virtual {p1}, Lcom/google/android/material/tabs/TabLayoutMediator;->attach()V

    .line 176
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    invoke-direct {p1, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v5, 0xa

    .line 180
    invoke-virtual {p1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v5, 0x9

    .line 181
    invoke-virtual {p1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 182
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->k:Landroidx/appcompat/widget/AppCompatImageButton;

    if-nez v5, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v5, v2

    :cond_7
    invoke-virtual {v5, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 183
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {p1, v5, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 187
    sget v0, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_back_button:I

    invoke-virtual {p1, v4, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 188
    invoke-virtual {v3, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 194
    new-instance p1, Lcom/pspdfkit/internal/ui/dialog/signatures/d$c;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$c;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V

    invoke-virtual {v3, p1}, Lcom/google/android/material/tabs/TabLayout;->addOnTabSelectedListener(Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;)V

    .line 213
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g:Landroid/widget/RelativeLayout;

    const-string v0, "addSignatureView"

    if-nez p1, :cond_8

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_8
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 214
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g:Landroid/widget/RelativeLayout;

    if-nez p1, :cond_9

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_9
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->k:Landroidx/appcompat/widget/AppCompatImageButton;

    if-nez v0, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_a
    move-object v2, v0

    :goto_0
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_b
    return-void
.end method

.method private final a(Z)V
    .locals 7

    const/4 v0, 0x1

    .line 238
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->t:Z

    .line 239
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a()V

    .line 240
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    const-string v1, "titleView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_0
    sget v3, Lcom/pspdfkit/R$string;->pspdf__add_signature:I

    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(I)V

    const-string v1, "addSignatureView"

    const-string v3, "signatureItemsListLayout"

    if-eqz p1, :cond_4

    .line 243
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->q:Landroid/view/View;

    if-nez p1, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    .line 244
    :cond_1
    new-instance v3, Lcom/pspdfkit/internal/ou;

    .line 248
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    const/4 v5, 0x5

    .line 249
    invoke-direct {v3, p1, v5, v4}, Lcom/pspdfkit/internal/ou;-><init>(Landroid/view/View;IF)V

    .line 250
    invoke-static {v3}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    const-string v3, "create(\n            Tran\u2026)\n            )\n        )"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    iget-object v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g:Landroid/widget/RelativeLayout;

    if-nez v4, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v4, v2

    .line 252
    :cond_2
    new-instance v1, Lcom/pspdfkit/internal/ou;

    .line 256
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    const/4 v6, 0x3

    .line 257
    invoke-direct {v1, v4, v6, v5}, Lcom/pspdfkit/internal/ou;-><init>(Landroid/view/View;IF)V

    .line 258
    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 260
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v1, :cond_3

    const-string v1, "addNewSignatureLayoutFab"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v2, v1

    .line 261
    :goto_0
    new-instance v1, Lcom/pspdfkit/internal/mq;

    const-wide/16 v3, 0x64

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    .line 262
    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    const-string v1, "create(\n            Scal\u2026)\n            )\n        )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 263
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 264
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_2

    .line 268
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->q:Landroid/view/View;

    if-nez p1, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_5
    const/16 v3, 0x8

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 269
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g:Landroid/widget/RelativeLayout;

    if-nez p1, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_6
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 270
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->d:Landroidx/viewpager2/widget/ViewPager2;

    if-nez p1, :cond_7

    const-string p1, "viewPager"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_7
    invoke-virtual {p1}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    move-result p1

    .line 271
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c:Lcom/pspdfkit/internal/qa;

    if-nez v1, :cond_8

    const-string v1, "electronicSignatureViewPagerAdapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    move-object v2, v1

    :goto_1
    invoke-virtual {v2, p1}, Lcom/pspdfkit/internal/qa;->a(I)Lcom/pspdfkit/internal/ui/dialog/signatures/f;

    move-result-object p1

    if-eqz p1, :cond_9

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/f;->setActive(Z)V

    .line 272
    :cond_9
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e()V

    :goto_2
    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/google/android/material/floatingactionbutton/FloatingActionButton;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    return-object p0
.end method

.method private final b()V
    .locals 8

    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->t:Z

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->d:Landroidx/viewpager2/widget/ViewPager2;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    const-string v1, "viewPager"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_0
    invoke-virtual {v1}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    move-result v1

    .line 13
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c:Lcom/pspdfkit/internal/qa;

    if-nez v3, :cond_1

    const-string v3, "electronicSignatureViewPagerAdapter"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v3, v2

    :cond_1
    invoke-virtual {v3, v1}, Lcom/pspdfkit/internal/qa;->a(I)Lcom/pspdfkit/internal/ui/dialog/signatures/f;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/f;->setActive(Z)V

    .line 14
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez v0, :cond_3

    const-string v0, "titleView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_3
    sget v1, Lcom/pspdfkit/R$string;->pspdf__signatures:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(I)V

    .line 15
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a()V

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_4

    const-string v0, "addSignatureView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    .line 17
    :cond_4
    new-instance v1, Lcom/pspdfkit/internal/ou;

    .line 21
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    const/4 v4, 0x2

    div-int/2addr v3, v4

    int-to-float v3, v3

    const/4 v5, 0x6

    .line 22
    invoke-direct {v1, v0, v5, v3}, Lcom/pspdfkit/internal/ou;-><init>(Landroid/view/View;IF)V

    .line 23
    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    const-string v1, "create(\n            Tran\u2026)\n            )\n        )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->q:Landroid/view/View;

    if-nez v3, :cond_5

    const-string v3, "signatureItemsListLayout"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v3, v2

    .line 25
    :cond_5
    new-instance v5, Lcom/pspdfkit/internal/ou;

    .line 29
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v6

    div-int/2addr v6, v4

    int-to-float v6, v6

    const/4 v7, 0x4

    .line 30
    invoke-direct {v5, v3, v7, v6}, Lcom/pspdfkit/internal/ou;-><init>(Landroid/view/View;IF)V

    .line 31
    invoke-static {v5}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {v0, v3}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v1, :cond_6

    const-string v1, "addNewSignatureLayoutFab"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    move-object v2, v1

    .line 34
    :goto_0
    new-instance v1, Lcom/pspdfkit/internal/mq;

    const-wide/16 v5, 0x64

    invoke-direct {v1, v2, v4, v5, v6}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    .line 35
    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    const-string v2, "create(\n            Scal\u2026)\n            )\n        )"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 37
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/d$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Landroidx/appcompat/widget/AppCompatImageButton;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->k:Landroidx/appcompat/widget/AppCompatImageButton;

    return-object p0
.end method

.method public static final synthetic d(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/google/android/material/floatingactionbutton/FloatingActionButton;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    return-object p0
.end method

.method public static final synthetic e(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/qa;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c:Lcom/pspdfkit/internal/qa;

    return-object p0
.end method

.method private final e()V
    .locals 6

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const-string v1, "addNewSignatureLayoutFab"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_0
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const-string v4, "deleteSelectedSignaturesFab"

    if-nez v0, :cond_1

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    invoke-virtual {v0, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_2
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_3
    invoke-virtual {v0, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_4

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_4
    invoke-virtual {v0, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_5

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_5
    invoke-virtual {v0, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    .line 8
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->t:Z

    const/4 v3, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_6
    invoke-virtual {v0, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_7
    invoke-virtual {v0, v5}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_8
    move-object v2, v0

    :goto_0
    invoke-virtual {v2, v5}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    goto :goto_2

    .line 12
    :cond_9
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object v0

    const-string v1, "signaturesAdapter.checkedItems"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_d

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_a

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_a
    invoke-virtual {v0, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_b

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_b
    invoke-virtual {v0, v5}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_c

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_1

    :cond_c
    move-object v2, v0

    :goto_1
    invoke-virtual {v2, v5}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    :cond_d
    :goto_2
    return-void
.end method

.method public static final synthetic f(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/or;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->i:Lcom/pspdfkit/internal/or;

    return-object p0
.end method

.method public static final synthetic g(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/ui/dialog/signatures/l;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    return-object p0
.end method

.method public static final synthetic h(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/ui/dialog/utils/a;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    return-object p0
.end method

.method public static final synthetic i(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a(Z)V

    return-void
.end method

.method private static final j(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c:Lcom/pspdfkit/internal/qa;

    if-nez p0, :cond_0

    const-string p0, "electronicSignatureViewPagerAdapter"

    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/qa;->b()V

    return-void
.end method

.method private static final k(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->d:Landroidx/viewpager2/widget/ViewPager2;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "viewPager"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    move-result v0

    const/4 v2, 0x1

    .line 2
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c:Lcom/pspdfkit/internal/qa;

    if-nez p0, :cond_1

    const-string p0, "electronicSignatureViewPagerAdapter"

    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, p0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/qa;->a(I)Lcom/pspdfkit/internal/ui/dialog/signatures/f;

    move-result-object p0

    if-eqz p0, :cond_2

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/f;->setActive(Z)V

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/signatures/Signature;)V
    .locals 6

    const-string v0, "signature"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 221
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const-string p1, "deleteSelectedSignaturesFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v0

    .line 222
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/mq;

    const/4 v2, 0x1

    const-wide/16 v3, 0x64

    invoke-direct {v1, p1, v2, v3, v4}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    .line 223
    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    const-string v1, "create(\n            Scal\u2026)\n            )\n        )"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 224
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v2, :cond_1

    const-string v2, "addNewSignatureLayoutFab"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 225
    :goto_0
    new-instance v2, Lcom/pspdfkit/internal/mq;

    const/4 v5, 0x2

    invoke-direct {v2, v0, v5, v3, v4}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    .line 226
    invoke-static {v2}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 228
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_2
    return-void
.end method

.method public final b(Lcom/pspdfkit/signatures/Signature;)V
    .locals 6

    const-string v0, "signature"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->r:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const-string p1, "addNewSignatureLayoutFab"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v1

    .line 4
    :cond_0
    new-instance v2, Lcom/pspdfkit/internal/mq;

    const-wide/16 v3, 0x64

    invoke-direct {v2, p1, v0, v3, v4}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    .line 5
    invoke-static {v2}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    const-string v0, "create(\n            Scal\u2026)\n            )\n        )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->s:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v2, :cond_1

    const-string v2, "deleteSelectedSignaturesFab"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 7
    :goto_0
    new-instance v2, Lcom/pspdfkit/internal/mq;

    const/4 v5, 0x2

    invoke-direct {v2, v1, v5, v3, v4}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    .line 8
    invoke-static {v2}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 10
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_2
    return-void
.end method

.method public final c()V
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->t:Z

    if-eqz v0, :cond_1

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->m:Z

    if-eqz v0, :cond_0

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->b()V

    goto :goto_0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->i:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onDismiss()V

    goto :goto_0

    .line 9
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->i:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onDismiss()V

    :cond_2
    :goto_0
    return-void
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->i:Lcom/pspdfkit/internal/or;

    return-void
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-nez p1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c()V

    :cond_0
    const/4 p1, 0x1

    return p1

    .line 8
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method protected final fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 2

    const-string v0, "insets"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->o:Z

    if-eqz v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez v0, :cond_0

    const-string v0, "titleView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    iget v1, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTopInset(I)V

    .line 4
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    move-result p1

    return p1
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;

    .line 2
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->t:Z

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->l:Z

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    .line 6
    iget-object v1, p1, Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;->c:Ljava/util/List;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "signatures"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    .line 7
    :goto_0
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->b(Ljava/util/List;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    .line 9
    iget-object v1, p1, Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;->d:Ljava/util/List;

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const-string v1, "checkedSignatures"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    .line 10
    :goto_1
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a(Ljava/util/List;)V

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a(Landroid/content/Context;)V

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->m:Z

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c:Lcom/pspdfkit/internal/qa;

    if-nez v0, :cond_2

    const-string v0, "electronicSignatureViewPagerAdapter"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move-object v2, v0

    :goto_2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;->a()Landroid/util/SparseArray;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/pspdfkit/internal/qa;->a(Landroid/util/SparseArray;)V

    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 1
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;-><init>(Landroid/os/Parcelable;)V

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->t:Z

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;->a(Z)V

    .line 4
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->m:Z

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;->b(Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->b()Ljava/util/List;

    move-result-object v0

    const-string v2, "signaturesAdapter.items"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;->b(Ljava/util/List;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object v0

    const-string v2, "signaturesAdapter.checkedItems"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;->a(Ljava/util/List;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c:Lcom/pspdfkit/internal/qa;

    if-nez v0, :cond_0

    const-string v0, "electronicSignatureViewPagerAdapter"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/qa;->a()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d$a;->a(Landroid/util/SparseArray;)V

    return-object v1
.end method

.method public final onSignatureCreated(Lcom/pspdfkit/signatures/Signature;Z)V
    .locals 1

    const-string v0, "signature"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->i:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onSignatureCreated(Lcom/pspdfkit/signatures/Signature;Z)V

    :cond_0
    return-void
.end method

.method public final onSignaturePicked(Lcom/pspdfkit/signatures/Signature;)V
    .locals 1

    const-string v0, "signature"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->i:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onSignaturePicked(Lcom/pspdfkit/signatures/Signature;)V

    :cond_0
    return-void
.end method

.method public final onSignatureUiDataCollected(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V
    .locals 1

    const-string v0, "signature"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureUiData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->i:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onSignatureUiDataCollected(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V

    :cond_0
    return-void
.end method

.method public final setFullscreen(Z)V
    .locals 5

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->o:Z

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    const/4 v1, 0x0

    const-string v2, "titleView"

    if-nez v0, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, p1, v3}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b(ZZ)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    xor-int/lit8 v4, p1, 0x1

    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setCloseButtonVisible(Z)V

    if-nez p1, :cond_3

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez v0, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTopInset(I)V

    .line 10
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->j:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez v0, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move-object v1, v0

    :goto_0
    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->p:I

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->n:I

    invoke-static {p0, v1, v0, v2, p1}, Lcom/pspdfkit/internal/ui/dialog/utils/b;->setRoundedBackground(Landroid/view/View;Lcom/pspdfkit/internal/ui/dialog/utils/a;IIZ)V

    return-void
.end method

.method public final setItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;)V"
        }
    .end annotation

    const-string v0, "signatures"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->b(Ljava/util/List;)V

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->l:Z

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 3
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->m:Z

    .line 4
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a(Z)V

    :cond_0
    const/4 p1, 0x1

    .line 6
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->l:Z

    return-void
.end method

.method public final setListener(Lcom/pspdfkit/internal/or;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->i:Lcom/pspdfkit/internal/or;

    return-void
.end method

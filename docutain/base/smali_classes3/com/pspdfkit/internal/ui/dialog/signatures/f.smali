.class public abstract Lcom/pspdfkit/internal/ui/dialog/signatures/f;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field protected b:Lcom/pspdfkit/internal/pa;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public abstract f()V
.end method

.method public abstract getCanvasView()Lcom/pspdfkit/internal/ui/dialog/signatures/i;
.end method

.method public setActive(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/f;->getCanvasView()Lcom/pspdfkit/internal/ui/dialog/signatures/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->setActive(Ljava/lang/Boolean;)V

    :cond_0
    return-void
.end method

.method public final setListener(Lcom/pspdfkit/internal/pa;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/f;->b:Lcom/pspdfkit/internal/pa;

    return-void
.end method

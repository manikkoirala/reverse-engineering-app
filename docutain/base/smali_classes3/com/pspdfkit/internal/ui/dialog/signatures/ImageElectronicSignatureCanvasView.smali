.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;
.super Lcom/pspdfkit/internal/ui/dialog/signatures/i;
.source "SourceFile"


# annotations
.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0011\u0012\u0008\u0008\u0002\u0010\u0013\u001a\u00020\n\u00a2\u0006\u0004\u0008\u0014\u0010\u0015J\u0010\u0010\u0005\u001a\u00020\u00042\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0002J\u0010\u0010\u0008\u001a\u00020\u00042\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0006J\u0008\u0010\t\u001a\u0004\u0018\u00010\u0006J\u0008\u0010\u000b\u001a\u00020\nH\u0014J\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;",
        "Lcom/pspdfkit/internal/ui/dialog/signatures/i;",
        "Lcom/pspdfkit/internal/lf$c;",
        "onImagePickedListener",
        "",
        "setOnImagePickedListener",
        "Landroid/net/Uri;",
        "signatureUri",
        "setSignatureUri",
        "getSignatureUri",
        "",
        "getSignHereStringRes",
        "Lio/reactivex/rxjava3/core/Single;",
        "Lcom/pspdfkit/signatures/Signature;",
        "getSignatureImage",
        "Landroid/content/Context;",
        "context",
        "Landroid/util/AttributeSet;",
        "attrs",
        "defStyleAttr",
        "<init>",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# instance fields
.field private s:Z

.field private final t:Landroid/graphics/Paint;

.field private final u:Ljava/lang/String;

.field private v:Landroid/widget/ImageView;

.field private final w:Lcom/pspdfkit/internal/nf;

.field private final x:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private final y:Landroid/widget/TextView;


# direct methods
.method public static synthetic $r8$lambda$36YkIX_pwvwAIJ6w87xeewqOWxs(Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;Landroid/net/Uri;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;Landroid/net/Uri;)V

    return-void
.end method

.method public static synthetic $r8$lambda$cqarJ4OCNehAdbDUqKw8XKmUWP4(Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 5
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    const/4 p3, 0x1

    .line 6
    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 7
    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setDither(Z)V

    .line 8
    sget v0, Lcom/pspdfkit/R$color;->pspdf__electronic_signature_clear_signature_color:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 9
    sget-object v0, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const/high16 v0, 0x41800000    # 16.0f

    .line 10
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 11
    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 12
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->t:Landroid/graphics/Paint;

    .line 21
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 22
    sget v1, Lcom/pspdfkit/R$string;->pspdf__electronic_signature_replace_image:I

    .line 23
    invoke-static {p2, v1, p0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p2

    const-string v1, "getString(\n        getCo\u2026image,\n        this\n    )"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->u:Ljava/lang/String;

    .line 41
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 42
    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_width:I

    .line 43
    sget v2, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_height:I

    .line 44
    invoke-static {p2, v1, v2}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/res/Resources;II)Z

    move-result p2

    iput-boolean p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->s:Z

    const/16 p2, 0x38

    .line 50
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p2

    .line 51
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, p2, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 52
    iput-boolean p3, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 p2, 0xd

    .line 53
    invoke-virtual {v1, p2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 55
    new-instance p2, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-direct {p2, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->x:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 56
    sget v2, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_signature_fab_add_new_signature:I

    invoke-virtual {p2, v2}, Landroid/view/View;->setId(I)V

    const/4 v2, 0x4

    .line 57
    invoke-static {p1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setCompatElevation(F)V

    .line 58
    invoke-virtual {p2, p3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setUseCompatPadding(Z)V

    const/4 v2, 0x0

    .line 59
    invoke-virtual {p2, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setSize(I)V

    .line 60
    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_electronic_signature_select_image:I

    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    invoke-static {v2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 61
    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_add:I

    invoke-virtual {p2, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setImageResource(I)V

    .line 62
    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {p2, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setColorFilter(I)V

    .line 63
    invoke-virtual {p2, p3}, Landroid/view/View;->setClickable(Z)V

    .line 64
    new-instance p3, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView$$ExternalSyntheticLambda1;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    invoke-virtual {p0, p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    new-instance p3, Landroid/widget/TextView;

    invoke-direct {p3, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->y:Landroid/widget/TextView;

    .line 68
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__electronic_signature_select_image:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 70
    sget v0, Lcom/pspdfkit/R$color;->pspdf__color_electronic_signature_select_image:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 71
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object p1

    .line 72
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mt;->a()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 73
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/fonts/Font;

    .line 74
    invoke-virtual {p1}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object p1

    .line 75
    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 79
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    invoke-direct {p1, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xe

    .line 83
    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 84
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result p2

    const/4 v0, 0x3

    invoke-virtual {p1, v0, p2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 85
    invoke-virtual {p3, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 86
    invoke-virtual {p0, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 88
    invoke-static {p0}, Lcom/pspdfkit/internal/ov;->b(Landroid/view/View;)Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    const-string p2, "requireFragmentManager(this)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    new-instance p2, Lcom/pspdfkit/internal/nf;

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/nf;-><init>(Landroidx/fragment/app/FragmentManager;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->w:Lcom/pspdfkit/internal/nf;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 1
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;Landroid/net/Uri;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$signatureUri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    sget v0, Lcom/pspdfkit/internal/lf;->l:I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 439
    invoke-static {p0, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->deleteFile(Landroid/content/Context;Landroid/net/Uri;)Z

    :cond_0
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;Landroid/view/View;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->w:Lcom/pspdfkit/internal/nf;

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v0, Lcom/pspdfkit/R$string;->pspdf__electronic_signature_select_image:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    const-string v0, "resources.getString(R.st\u2026c_signature_select_image)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/nf;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final a()F
    .locals 4

    .line 12
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x41900000    # 18.0f

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result v1

    const/4 v2, 0x2

    int-to-float v2, v2

    mul-float v1, v1, v2

    .line 14
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x41800000    # 16.0f

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;F)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0
.end method

.method protected final a(Landroid/graphics/Canvas;)V
    .locals 4

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->u:Ljava/lang/String;

    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 17
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->b()F

    move-result v2

    .line 18
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->t:Landroid/graphics/Paint;

    .line 19
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected final a(Landroid/graphics/Paint;)V
    .locals 2

    const-string v0, "signHerePaint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setDither(Z)V

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$color;->pspdf__electronic_signature_sign_here_color:I

    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method protected final a(Landroid/view/MotionEvent;)V
    .locals 4

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->n:Z

    const-string v1, "resources.getString(R.st\u2026c_signature_select_image)"

    if-eqz v0, :cond_0

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->w:Lcom/pspdfkit/internal/nf;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$string;->pspdf__electronic_signature_select_image:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/nf;->a(Ljava/lang/String;)V

    .line 8
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->n:Z

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->a()F

    move-result v0

    cmpl-float p1, p1, v0

    if-lez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    .line 10
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c()V

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->w:Lcom/pspdfkit/internal/nf;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/pspdfkit/R$string;->pspdf__electronic_signature_select_image:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/nf;->a(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method protected final b()F
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    int-to-float v2, v2

    const/high16 v3, 0x41900000    # 18.0f

    add-float/2addr v2, v3

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method protected final d()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->n:Z

    return-void
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->n:Z

    return-void
.end method

.method protected getSignHereStringRes()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getSignatureImage()Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->o:Landroid/net/Uri;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t import signature image: Signature URI is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    const-string v1, "error(IllegalStateExcept\u2026Signature URI is null.\"))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 3
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/pspdfkit/document/image/BitmapUtils;->decodeBitmapAsync(Landroid/content/Context;Landroid/net/Uri;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    .line 4
    sget-object v2, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView$a;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView$a;

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Single;->flatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    .line 15
    new-instance v2, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Single;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    const-string v1, "decodeBitmapAsync(contex\u2026gnatureUri)\n            }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getSignatureUri()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->o:Landroid/net/Uri;

    return-object v0
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->n:Z

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0xc

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v0

    int-to-float v2, v0

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->a()F

    move-result v5

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float v4, v0, v2

    .line 9
    iget-object v6, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->b:Landroid/graphics/Paint;

    move-object v1, p1

    move v3, v5

    .line 10
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 11
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->a(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected final onFinishInflate()V
    .locals 4

    .line 1
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__electronic_signature_selected_image:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf_\u2026signature_selected_image)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->v:Landroid/widget/ImageView;

    .line 4
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x41900000    # 18.0f

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result v1

    const/4 v2, 0x2

    int-to-float v2, v2

    mul-float v1, v1, v2

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x41800000    # 16.0f

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;F)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    const/4 v2, 0x0

    .line 7
    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->v:Landroid/widget/ImageView;

    if-nez v1, :cond_0

    const-string v1, "selectedImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method protected final onMeasure(II)V
    .locals 6

    .line 1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    .line 4
    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 5
    invoke-static {p2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 7
    iget-boolean v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->s:Z

    if-nez v3, :cond_1

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    const v5, 0x3f2aaaab

    if-ne v3, v4, :cond_0

    int-to-float p1, v1

    div-float/2addr p1, v5

    float-to-int p1, p1

    .line 10
    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_0

    :cond_0
    int-to-float p2, v0

    mul-float p2, p2, v5

    float-to-int p2, p2

    .line 13
    invoke-static {p2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 17
    :cond_1
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    return-void
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->o:Landroid/net/Uri;

    if-eqz p1, :cond_1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->v:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string v0, "selectedImage"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    :cond_1
    return-void
.end method

.method public final setOnImagePickedListener(Lcom/pspdfkit/internal/lf$c;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->w:Lcom/pspdfkit/internal/nf;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nf;->a(Lcom/pspdfkit/internal/lf$c;)V

    return-void
.end method

.method public final setSignatureUri(Landroid/net/Uri;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->o:Landroid/net/Uri;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->v:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string v0, "selectedImage"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    if-eqz p1, :cond_1

    const/4 p1, 0x4

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 4
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->x:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->y:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

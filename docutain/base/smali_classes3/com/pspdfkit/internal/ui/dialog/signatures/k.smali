.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/k;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ui/dialog/signatures/a$b;
.implements Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/k$d;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

.field private c:I

.field private d:Lcom/pspdfkit/internal/ui/dialog/utils/a;

.field private e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

.field private f:Z

.field private final g:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

.field private final h:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

.field private final i:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

.field private final j:Ljava/lang/String;

.field private k:I

.field private final l:Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;

.field private m:Landroid/view/View;

.field private n:Lcom/pspdfkit/internal/ui/dialog/signatures/a;

.field private o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private q:Z

.field private r:Z

.field private s:Z


# direct methods
.method public static synthetic $r8$lambda$_GjYqFiv8ETuPaHzIRYSLN1z-90(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/pspdfkit/internal/ui/dialog/signatures/l;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/pspdfkit/internal/ui/dialog/utils/a;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeto(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/google/android/material/floatingactionbutton/FloatingActionButton;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetp(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/google/android/material/floatingactionbutton/FloatingActionButton;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetq(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->q:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgets(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->s:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$ma(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->a()V

    return-void
.end method

.method static bridge synthetic -$$Nest$ma(Lcom/pspdfkit/internal/ui/dialog/signatures/k;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->a(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Ljava/lang/String;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->a(Landroid/content/Context;)I

    move-result v1

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {p0, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    const/4 v0, 0x0

    .line 14
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->f:Z

    .line 34
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/k;Lcom/pspdfkit/internal/ui/dialog/signatures/k$e-IA;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;

    .line 46
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->q:Z

    .line 48
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->r:Z

    const/4 v0, 0x1

    .line 50
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->s:Z

    .line 59
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->g:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    .line 60
    iput-object p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->h:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 61
    iput-object p4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->i:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    .line 62
    iput-object p5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->j:Ljava/lang/String;

    .line 63
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b(Landroid/content/Context;)V

    return-void
.end method

.method private static a(Landroid/content/Context;)I
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->-$$Nest$sfgeti()I

    move-result v0

    invoke-static {}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->-$$Nest$sfgetj()I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/cu;->b(Landroid/content/Context;II)I

    move-result p0

    return p0
.end method

.method private a()V
    .locals 6

    .line 71
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    if-eqz v0, :cond_0

    .line 72
    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->c()V

    :cond_0
    const/4 v0, 0x0

    .line 75
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->q:Z

    .line 76
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    sget v1, Lcom/pspdfkit/R$string;->pspdf__signatures:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(I)V

    .line 77
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->n:Lcom/pspdfkit/internal/ui/dialog/signatures/a;

    .line 78
    new-instance v1, Lcom/pspdfkit/internal/ou;

    .line 79
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    const/4 v3, 0x2

    div-int/2addr v2, v3

    int-to-float v2, v2

    const/4 v4, 0x6

    invoke-direct {v1, v0, v4, v2}, Lcom/pspdfkit/internal/ou;-><init>(Landroid/view/View;IF)V

    .line 80
    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->m:Landroid/view/View;

    .line 82
    new-instance v2, Lcom/pspdfkit/internal/ou;

    .line 83
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/2addr v4, v3

    int-to-float v4, v4

    const/4 v5, 0x4

    invoke-direct {v2, v1, v5, v4}, Lcom/pspdfkit/internal/ou;-><init>(Landroid/view/View;IF)V

    .line 84
    invoke-static {v2}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 85
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 86
    new-instance v2, Lcom/pspdfkit/internal/mq;

    const-wide/16 v4, 0x64

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {v2}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 87
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)V

    .line 88
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    .line 93
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    if-eqz v0, :cond_1

    .line 94
    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    .line 95
    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x1

    .line 96
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    :cond_1
    return-void
.end method

.method private a(Z)V
    .locals 7

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v0, :cond_3

    .line 15
    sget-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$b;->a:[I

    iget-object v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->g:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget v0, v0, v4

    if-eq v0, v3, :cond_2

    if-eq v0, v2, :cond_1

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->b()V

    goto :goto_0

    .line 28
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->a()V

    goto :goto_0

    .line 29
    :cond_2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->f:Z

    if-eqz v0, :cond_3

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->a()V

    .line 46
    :cond_3
    :goto_0
    iput-boolean v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->q:Z

    .line 47
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    sget v4, Lcom/pspdfkit/R$string;->pspdf__add_signature:I

    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(I)V

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 49
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->m:Landroid/view/View;

    .line 50
    new-instance v4, Lcom/pspdfkit/internal/ou;

    .line 51
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/2addr v5, v2

    int-to-float v5, v5

    const/4 v6, 0x5

    invoke-direct {v4, p1, v6, v5}, Lcom/pspdfkit/internal/ou;-><init>(Landroid/view/View;IF)V

    .line 52
    invoke-static {v4}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 53
    iget-object v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->n:Lcom/pspdfkit/internal/ui/dialog/signatures/a;

    .line 54
    new-instance v5, Lcom/pspdfkit/internal/ou;

    .line 55
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v6

    div-int/2addr v6, v2

    int-to-float v2, v6

    invoke-direct {v5, v4, v1, v2}, Lcom/pspdfkit/internal/ou;-><init>(Landroid/view/View;IF)V

    .line 56
    invoke-static {v5}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 57
    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 58
    new-instance v2, Lcom/pspdfkit/internal/mq;

    const-wide/16 v4, 0x64

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {v2}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 59
    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_1

    .line 62
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 63
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->n:Lcom/pspdfkit/internal/ui/dialog/signatures/a;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 64
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d()V

    .line 67
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    if-eqz p1, :cond_5

    .line 68
    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    .line 69
    invoke-virtual {p1}, Landroidx/appcompat/app/AppCompatDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object p1

    .line 70
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    :cond_5
    return-void
.end method

.method private synthetic b()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->n:Lcom/pspdfkit/internal/ui/dialog/signatures/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->a()V

    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 10

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hb;->d()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 6
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/utils/b;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/utils/b;-><init>(Landroid/content/Context;)V

    .line 7
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;-><init>(Landroid/content/Context;)V

    .line 11
    iget-boolean v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->q:Z

    .line 13
    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->-$$Nest$fgeta(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I

    move-result v3

    iput v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->k:I

    .line 14
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/utils/b;->getCornerRadius()I

    move-result v3

    iput v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->c:I

    .line 15
    iget v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->k:I

    invoke-virtual {p0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 17
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v3, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;)V

    .line 19
    new-instance v3, Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-direct {v3, p1, v0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ui/dialog/utils/b;)V

    iput-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    .line 20
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_layout_title_view:I

    invoke-virtual {v3, v0}, Landroid/view/View;->setId(I)V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-eqz v2, :cond_0

    .line 22
    sget v3, Lcom/pspdfkit/R$string;->pspdf__add_signature:I

    goto :goto_0

    :cond_0
    sget v3, Lcom/pspdfkit/R$string;->pspdf__signatures:I

    .line 23
    :goto_0
    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(I)V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;

    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setBackButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v3, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 32
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 34
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    const/4 v4, 0x3

    invoke-virtual {v0, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 38
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    sget v4, Lcom/pspdfkit/R$layout;->pspdf__recycler_view_with_empty_message:I

    const/4 v5, 0x0

    .line 39
    invoke-virtual {v3, v4, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->m:Landroid/view/View;

    .line 40
    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 41
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->m:Landroid/view/View;

    const/16 v4, 0x8

    if-eqz v2, :cond_1

    const/16 v6, 0x8

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    :goto_1
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 42
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->m:Landroid/view/View;

    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 44
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->m:Landroid/view/View;

    sget v6, Lcom/pspdfkit/R$id;->pspdf__empty_text:I

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 45
    iget-object v6, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->getItemCount()I

    move-result v6

    if-nez v6, :cond_2

    const/4 v6, 0x0

    goto :goto_2

    :cond_2
    const/16 v6, 0x8

    :goto_2
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 46
    sget v6, Lcom/pspdfkit/R$string;->pspdf__no_signatures:I

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    .line 48
    iget-object v6, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->m:Landroid/view/View;

    sget v7, Lcom/pspdfkit/R$id;->pspdf__recycler_view:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroidx/recyclerview/widget/RecyclerView;

    .line 49
    sget v7, Lcom/pspdfkit/R$id;->pspdf__signature_items_list:I

    invoke-virtual {v6, v7}, Landroid/view/View;->setId(I)V

    .line 50
    iget-object v7, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v6, v7}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 51
    new-instance v7, Landroidx/recyclerview/widget/DefaultItemAnimator;

    invoke-direct {v7}, Landroidx/recyclerview/widget/DefaultItemAnimator;-><init>()V

    invoke-virtual {v6, v7}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    .line 52
    new-instance v7, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v7, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 53
    new-instance v7, Lcom/pspdfkit/internal/oo;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/pspdfkit/internal/oo;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 54
    iget-object v7, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v7}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->getItemCount()I

    move-result v7

    if-nez v7, :cond_3

    const/16 v7, 0x8

    goto :goto_3

    :cond_3
    const/4 v7, 0x0

    :goto_3
    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 56
    iget-object v7, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    new-instance v8, Lcom/pspdfkit/internal/ui/dialog/signatures/k$a;

    invoke-direct {v8, p0, v6, v3}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$a;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/k;Landroidx/recyclerview/widget/RecyclerView;Landroid/widget/TextView;)V

    invoke-virtual {v7, v8}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->registerAdapterDataObserver(Landroidx/recyclerview/widget/RecyclerView$AdapterDataObserver;)V

    .line 75
    new-instance v3, Lcom/pspdfkit/internal/ui/dialog/signatures/a;

    invoke-direct {v3, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->n:Lcom/pspdfkit/internal/ui/dialog/signatures/a;

    .line 76
    iget-object v6, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->h:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    sget-object v7, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;->SAVE_IF_SELECTED:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    const/4 v8, 0x1

    if-ne v6, v7, :cond_4

    const/4 v6, 0x1

    goto :goto_4

    :cond_4
    const/4 v6, 0x0

    :goto_4
    invoke-virtual {v3, v6}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->setStoreSignatureCheckboxVisible(Z)V

    .line 78
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->n:Lcom/pspdfkit/internal/ui/dialog/signatures/a;

    invoke-static {}, Lcom/pspdfkit/signatures/SignatureManager;->getSigners()Ljava/util/Map;

    move-result-object v6

    iget-object v7, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->i:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    iget-object v9, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->j:Ljava/lang/String;

    invoke-virtual {v3, v6, v7, v9}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->a(Ljava/util/Map;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Ljava/lang/String;)V

    .line 79
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->n:Lcom/pspdfkit/internal/ui/dialog/signatures/a;

    invoke-virtual {v3, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/a$b;)V

    .line 80
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->n:Lcom/pspdfkit/internal/ui/dialog/signatures/a;

    sget v6, Lcom/pspdfkit/R$id;->pspdf__signature_layout_add_new_signature:I

    invoke-virtual {v3, v6}, Landroid/view/View;->setId(I)V

    .line 81
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->n:Lcom/pspdfkit/internal/ui/dialog/signatures/a;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 82
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->n:Lcom/pspdfkit/internal/ui/dialog/signatures/a;

    if-eqz v2, :cond_5

    const/4 v4, 0x0

    :cond_5
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->n:Lcom/pspdfkit/internal/ui/dialog/signatures/a;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 86
    invoke-virtual {p0, v8}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 87
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    const/16 v0, 0x38

    int-to-float v0, v0

    .line 88
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 89
    invoke-static {v8, v0, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 90
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 91
    iput-boolean v8, v3, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v0, 0xc

    .line 92
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0x15

    .line 93
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0x10

    int-to-float v0, v0

    .line 94
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 95
    invoke-static {v8, v0, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v4, v4

    .line 96
    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    .line 97
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 98
    invoke-static {v8, v0, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 99
    iput v0, v3, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 101
    new-instance v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-direct {v0, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 102
    sget v4, Lcom/pspdfkit/R$id;->pspdf__signature_fab_add_new_signature:I

    invoke-virtual {v0, v4}, Landroid/view/View;->setId(I)V

    .line 103
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 v4, 0x4

    int-to-float v4, v4

    .line 104
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    .line 105
    invoke-static {v8, v4, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v6

    float-to-int v6, v6

    int-to-float v6, v6

    .line 106
    invoke-virtual {v0, v6}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setCompatElevation(F)V

    .line 107
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v8}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setUseCompatPadding(Z)V

    .line 108
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v5}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setSize(I)V

    .line 109
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->-$$Nest$fgetd(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I

    move-result v5

    invoke-static {v5}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 110
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setImageResource(I)V

    .line 111
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setColorFilter(I)V

    .line 112
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v8}, Landroid/view/View;->setClickable(Z)V

    .line 113
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iget-object v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p0, v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    new-instance v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-direct {v0, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 117
    sget v5, Lcom/pspdfkit/R$id;->pspdf__signature_fab_delete_selected_signatures:I

    invoke-virtual {v0, v5}, Landroid/view/View;->setId(I)V

    .line 118
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v8}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setUseCompatPadding(Z)V

    .line 119
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 120
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    .line 121
    invoke-static {v8, v4, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p1

    float-to-int p1, p1

    int-to-float p1, p1

    .line 122
    invoke-virtual {v0, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setCompatElevation(F)V

    .line 123
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->-$$Nest$fgetg(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I

    move-result v0

    .line 124
    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 125
    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 127
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->-$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setImageResource(I)V

    .line 128
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->-$$Nest$fgetf(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setColorFilter(I)V

    .line 129
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1, v8}, Landroid/view/View;->setClickable(Z)V

    .line 130
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->l:Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p0, p1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    if-eqz v2, :cond_6

    .line 133
    iput-boolean v8, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->q:Z

    .line 135
    :cond_6
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d()V

    return-void

    .line 136
    :cond_7
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Creating signature annotations requires Electronic Signatures."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private d()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    .line 10
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->q:Z

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    goto :goto_0

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/signatures/Signature;)V
    .locals 5

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 9
    new-instance v0, Lcom/pspdfkit/internal/mq;

    const/4 v1, 0x1

    const-wide/16 v2, 0x64

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 11
    new-instance v1, Lcom/pspdfkit/internal/mq;

    const/4 v4, 0x2

    invoke-direct {v1, v0, v4, v2, v3}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 12
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 13
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    if-eqz v0, :cond_0

    .line 6
    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onSignatureUiDataCollected(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/signatures/Signature;Z)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onSignatureCreated(Lcom/pspdfkit/signatures/Signature;Z)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->c()V

    :cond_0
    return-void
.end method

.method public final b(Lcom/pspdfkit/signatures/Signature;)V
    .locals 5

    .line 137
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 138
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->o:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 139
    new-instance v1, Lcom/pspdfkit/internal/mq;

    const-wide/16 v2, 0x64

    invoke-direct {v1, p1, v0, v2, v3}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 140
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->p:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 141
    new-instance v1, Lcom/pspdfkit/internal/mq;

    const/4 v4, 0x2

    invoke-direct {v1, v0, v4, v2, v3}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 142
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 143
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    return-void
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-nez p1, :cond_2

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->q:Z

    if-eqz p1, :cond_1

    .line 4
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->s:Z

    if-eqz p1, :cond_0

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->a()V

    goto :goto_0

    .line 7
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    if-eqz p1, :cond_2

    .line 8
    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->c()V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    invoke-interface {p1}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onDismiss()V

    goto :goto_0

    .line 12
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    if-eqz p1, :cond_2

    .line 13
    invoke-interface {p1}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onDismiss()V

    :cond_2
    :goto_0
    const/4 p1, 0x1

    return p1

    .line 14
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method protected final fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->f:Z

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    iget v1, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTopInset(I)V

    .line 4
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    move-result p1

    return p1
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$d;

    .line 2
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 3
    iget-boolean v0, p1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$d;->a:Z

    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->q:Z

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->r:Z

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    iget-object v1, p1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$d;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->b(Ljava/util/List;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    iget-object v1, p1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$d;->d:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a(Ljava/util/List;)V

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b(Landroid/content/Context;)V

    .line 10
    iget-boolean p1, p1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$d;->b:Z

    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->s:Z

    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 1
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$d;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$d;-><init>(Landroid/os/Parcelable;)V

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->q:Z

    iput-boolean v0, v1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$d;->a:Z

    .line 4
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->s:Z

    iput-boolean v0, v1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$d;->b:Z

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->b()Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$d;->c:Ljava/util/List;

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$d;->d:Ljava/util/List;

    return-object v1
.end method

.method public final onSignaturePicked(Lcom/pspdfkit/signatures/Signature;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onSignaturePicked(Lcom/pspdfkit/signatures/Signature;)V

    :cond_0
    return-void
.end method

.method public setFullscreen(Z)V
    .locals 3

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->f:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b(ZZ)V

    if-nez p1, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTopInset(I)V

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->d:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    iget v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->k:I

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->c:I

    invoke-static {p0, v0, v1, v2, p1}, Lcom/pspdfkit/internal/ui/dialog/utils/b;->setRoundedBackground(Landroid/view/View;Lcom/pspdfkit/internal/ui/dialog/utils/a;IIZ)V

    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->b(Ljava/util/List;)V

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->r:Z

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 3
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->s:Z

    .line 4
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->a(Z)V

    :cond_0
    const/4 p1, 0x1

    .line 6
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->r:Z

    return-void
.end method

.method public setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    return-void
.end method

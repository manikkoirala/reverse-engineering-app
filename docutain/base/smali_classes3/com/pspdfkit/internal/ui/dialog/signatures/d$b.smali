.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/dialog/signatures/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation


# instance fields
.field final synthetic b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Landroidx/appcompat/widget/AppCompatImageButton;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "backButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    if-eq p1, v0, :cond_7

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->h(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/ui/dialog/utils/a;

    move-result-object v0

    const-string v2, "titleView"

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->getBackButton()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-eq p1, v0, :cond_7

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->h(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/ui/dialog/utils/a;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->getCloseButton()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-ne p1, v0, :cond_3

    goto/16 :goto_1

    .line 3
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->b(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, "addNewSignatureLayoutFab"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_4
    if-ne p1, v0, :cond_5

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->i(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V

    goto :goto_2

    .line 5
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->d(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    move-result-object v0

    if-nez v0, :cond_6

    const-string v0, "deleteSelectedSignaturesFab"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    move-object v1, v0

    :goto_0
    if-ne p1, v1, :cond_8

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->f(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/or;

    move-result-object p1

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object p1

    const-string v0, "signaturesAdapter.checkedItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_8

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->f(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/or;

    move-result-object p1

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/or;->onSignaturesDeleted(Ljava/util/List;)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->g(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->c()V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V

    goto :goto_2

    .line 10
    :cond_7
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->c()V

    :cond_8
    :goto_2
    return-void
.end method

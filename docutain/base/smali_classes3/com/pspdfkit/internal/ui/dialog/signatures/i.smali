.class public abstract Lcom/pspdfkit/internal/ui/dialog/signatures/i;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;
    }
.end annotation


# static fields
.field private static final p:[I

.field private static final q:I

.field private static final r:I


# instance fields
.field protected final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field public d:F

.field private e:F

.field private f:F

.field private g:Ljava/util/ArrayList;

.field private h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

.field private i:I

.field private j:I

.field private k:F

.field private l:I

.field protected m:Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;

.field protected n:Z

.field protected o:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout:[I

    sput-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->p:[I

    .line 2
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__signatureLayoutStyle:I

    sput v0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->q:I

    .line 3
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_SignatureLayout:I

    sput v0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->r:I

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->b:Landroid/graphics/Paint;

    .line 5
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    const/high16 v0, 0x3f800000    # 1.0f

    .line 11
    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->d:F

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    const/high16 v1, -0x1000000

    .line 32
    iput v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->l:I

    const/4 v1, 0x1

    .line 38
    iput-boolean v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->n:Z

    .line 40
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->o:Landroid/net/Uri;

    .line 45
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->a(Landroid/content/Context;)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->b:Landroid/graphics/Paint;

    .line 50
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    const/high16 p2, 0x3f800000    # 1.0f

    .line 56
    iput p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->d:F

    .line 65
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    const/4 p2, 0x0

    .line 68
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    const/high16 v0, -0x1000000

    .line 77
    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->l:I

    const/4 v0, 0x1

    .line 83
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->n:Z

    .line 85
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->o:Landroid/net/Uri;

    .line 95
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->a(Landroid/content/Context;)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 96
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 97
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->b:Landroid/graphics/Paint;

    .line 100
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    const/high16 p2, 0x3f800000    # 1.0f

    .line 106
    iput p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->d:F

    .line 115
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    const/4 p2, 0x0

    .line 118
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    const/high16 p3, -0x1000000

    .line 127
    iput p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->l:I

    const/4 p3, 0x1

    .line 133
    iput-boolean p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->n:Z

    .line 135
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->o:Landroid/net/Uri;

    .line 150
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->a(Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/util/List;F)Ljava/util/ArrayList;
    .locals 10

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 28
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 29
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 30
    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 31
    new-instance v5, Landroid/graphics/PointF;

    iget v6, v4, Landroid/graphics/PointF;->x:F

    mul-float v6, v6, p1

    iget v4, v4, Landroid/graphics/PointF;->y:F

    mul-float v4, v4, p1

    invoke-direct {v5, v6, v4}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 33
    :cond_0
    new-instance v9, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetd(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)I

    move-result v6

    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetg(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)F

    move-result v7

    const/4 v8, 0x0

    move-object v2, v9

    invoke-direct/range {v2 .. v8}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IFLcom/pspdfkit/internal/ui/dialog/signatures/i$a-IA;)V

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->p:[I

    sget v1, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->q:I

    sget v2, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->r:I

    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0, v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 2
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->b:Landroid/graphics/Paint;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->a(Landroid/graphics/Paint;)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setDither(Z)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->l:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method private getPrevailingMotionEventToolType()I
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 3
    :cond_0
    new-instance v0, Landroid/util/SparseIntArray;

    const/4 v2, 0x4

    invoke-direct {v0, v2}, Landroid/util/SparseIntArray;-><init>(I)V

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 5
    invoke-static {v3}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    const/4 v3, -0x1

    .line 9
    :goto_1
    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 10
    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v4

    .line 11
    invoke-virtual {v0, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v5

    if-le v5, v3, :cond_2

    move v2, v4

    move v3, v5

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    return v2
.end method


# virtual methods
.method abstract a()F
.end method

.method protected final a(Ljava/lang/String;)Lcom/pspdfkit/signatures/Signature;
    .locals 12

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->d:F

    const/high16 v3, 0x3f800000    # 1.0f

    div-float/2addr v3, v2

    invoke-static {v0, v3}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->a(Ljava/util/List;F)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v3, 0x1

    .line 46
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 47
    invoke-static {v6}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    .line 48
    iget v8, v7, Landroid/graphics/PointF;->x:F

    cmpg-float v9, v8, v2

    if-gez v9, :cond_3

    move v2, v8

    .line 49
    :cond_3
    iget v7, v7, Landroid/graphics/PointF;->y:F

    cmpl-float v9, v7, v5

    if-lez v9, :cond_4

    move v5, v7

    :cond_4
    cmpl-float v7, v8, v3

    if-lez v7, :cond_2

    move v3, v8

    goto :goto_0

    :cond_5
    const/high16 v0, 0x40000000    # 2.0f

    add-float/2addr v5, v0

    add-float/2addr v3, v0

    sub-float/2addr v2, v0

    const/high16 v0, 0x43480000    # 200.0f

    sub-float v5, v0, v5

    .line 63
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 66
    iget-object v6, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 67
    invoke-static {v7}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/PointF;

    .line 69
    iget v11, v10, Landroid/graphics/PointF;->x:F

    sub-float/2addr v11, v2

    iput v11, v10, Landroid/graphics/PointF;->x:F

    .line 70
    iget v11, v10, Landroid/graphics/PointF;->y:F

    add-float/2addr v11, v5

    sub-float v11, v0, v11

    .line 73
    iput v11, v10, Landroid/graphics/PointF;->y:F

    goto :goto_2

    .line 75
    :cond_6
    invoke-static {v7}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 76
    :cond_7
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v5, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DIGITAL_SIGNATURES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v5}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-nez v0, :cond_8

    :goto_3
    move-object v10, v1

    goto/16 :goto_8

    .line 79
    :cond_8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 80
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 81
    iget-object v6, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 82
    invoke-static {v7}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetd(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 83
    invoke-static {v7}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    .line 86
    :cond_9
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->getPrevailingMotionEventToolType()I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_c

    const/4 v7, 0x2

    if-eq v6, v7, :cond_b

    const/4 v7, 0x3

    if-eq v6, v7, :cond_a

    move-object v6, v1

    goto :goto_5

    .line 87
    :cond_a
    sget-object v6, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->MOUSE:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    goto :goto_5

    .line 88
    :cond_b
    sget-object v6, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->STYLUS:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    goto :goto_5

    .line 89
    :cond_c
    sget-object v6, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->FINGER:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    .line 90
    :goto_5
    iget-object v7, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_d

    goto :goto_7

    .line 94
    :cond_d
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 95
    invoke-static {v7}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetg(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)F

    move-result v7

    add-float/2addr v4, v7

    goto :goto_6

    .line 97
    :cond_e
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v4, v1

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 98
    :goto_7
    new-instance v4, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;

    invoke-direct {v4}, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;-><init>()V

    .line 99
    invoke-virtual {v4, v0}, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->setPressurePoints(Ljava/util/List;)Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;

    move-result-object v0

    .line 100
    invoke-virtual {v0, v5}, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->setTimePoints(Ljava/util/List;)Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;

    move-result-object v0

    .line 101
    invoke-virtual {v0, v6}, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->setInputMethod(Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;)Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;

    move-result-object v0

    .line 106
    invoke-virtual {v0, v1}, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->setTouchRadius(Ljava/lang/Float;)Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->build()Lcom/pspdfkit/signatures/BiometricSignatureData;

    move-result-object v1

    goto/16 :goto_3

    .line 108
    :goto_8
    iget v6, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->l:I

    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->d:F

    sub-float/2addr v3, v2

    mul-float v3, v3, v0

    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->i:I

    int-to-float v0, v0

    div-float v11, v3, v0

    const/high16 v7, 0x40800000    # 4.0f

    move-object v9, p1

    invoke-static/range {v6 .. v11}, Lcom/pspdfkit/signatures/Signature;->create(IFLjava/util/List;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;F)Lcom/pspdfkit/signatures/Signature;

    move-result-object p1

    return-object p1
.end method

.method abstract a(Landroid/graphics/Canvas;)V
.end method

.method abstract a(Landroid/graphics/Paint;)V
.end method

.method protected a(Landroid/view/MotionEvent;)V
    .locals 10

    .line 12
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->k:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->i:I

    int-to-float v3, v3

    sub-float/2addr v3, v1

    .line 13
    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 14
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->k:F

    div-float/2addr v3, v2

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->j:I

    int-to-float v2, v2

    sub-float/2addr v2, v3

    .line 15
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 16
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 17
    iget v0, v3, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->e:F

    .line 18
    iget v0, v3, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->f:F

    .line 20
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 21
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v6

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSize()F

    move-result v8

    const/4 v9, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v9}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;-><init>(Landroid/graphics/PointF;JFIFLcom/pspdfkit/internal/ui/dialog/signatures/i$a-IA;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->m:Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 23
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->m:Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;

    invoke-interface {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;->e()V

    .line 25
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 26
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->f()V

    :cond_1
    return-void
.end method

.method abstract b()F
.end method

.method public c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->m:Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;

    if-eqz v0, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;->d()V

    .line 7
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->d()V

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method abstract d()V
.end method

.method protected final e()Lcom/pspdfkit/ui/signatures/SignatureUiData;
    .locals 7

    .line 1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 7
    invoke-static {v5}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 8
    invoke-static {v5}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetd(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 9
    invoke-static {v5}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 10
    invoke-static {v5}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetf(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 13
    :cond_0
    new-instance v6, Lcom/pspdfkit/ui/signatures/SignatureUiData;

    .line 18
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->getPrevailingMotionEventToolType()I

    move-result v0

    const/4 v5, 0x1

    if-eq v0, v5, :cond_3

    const/4 v5, 0x2

    if-eq v0, v5, :cond_2

    const/4 v5, 0x3

    if-eq v0, v5, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    .line 19
    :cond_1
    sget-object v0, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->MOUSE:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    goto :goto_1

    .line 20
    :cond_2
    sget-object v0, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->STYLUS:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    goto :goto_1

    .line 21
    :cond_3
    sget-object v0, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->FINGER:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    :goto_1
    move-object v5, v0

    move-object v0, v6

    .line 22
    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/ui/signatures/SignatureUiData;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;)V

    return-object v6
.end method

.method abstract f()V
.end method

.method protected getCurrentLines()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    if-eqz v1, :cond_0

    .line 4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method public getInkColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->l:I

    return v0
.end method

.method abstract getSignHereStringRes()I
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .line 1
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0xc

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v0

    int-to-float v2, v0

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->a()F

    move-result v5

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float v4, v0, v2

    iget-object v6, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->b:Landroid/graphics/Paint;

    move-object v1, p1

    move v3, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 6
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->n:Z

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->getSignHereStringRes()I

    move-result v1

    invoke-static {v0, v1, p0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->b()F

    move-result v2

    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->a(Landroid/graphics/Canvas;)V

    .line 13
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 14
    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v5, v4, :cond_3

    .line 15
    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 16
    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Landroid/graphics/PointF;

    :cond_2
    if-eqz v3, :cond_1

    .line 17
    iget v1, v3, Landroid/graphics/PointF;->x:F

    iget v2, v3, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 21
    :cond_3
    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Landroid/graphics/Path;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 25
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    if-eqz v0, :cond_7

    .line 26
    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_6

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 29
    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v1

    .line 30
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 31
    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/graphics/PointF;

    :cond_5
    if-eqz v3, :cond_7

    .line 32
    iget v0, v3, Landroid/graphics/PointF;->x:F

    iget v1, v3, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 36
    :cond_6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Landroid/graphics/Path;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_7
    :goto_2
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 0

    .line 1
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->i:I

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->j:I

    int-to-float p1, p1

    const/high16 p2, 0x43480000    # 200.0f

    div-float/2addr p1, p2

    .line 7
    iget p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->d:F

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/di;->a(FF)Z

    move-result p2

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_0

    .line 8
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    iget p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->d:F

    div-float p3, p1, p3

    invoke-static {p2, p3}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->a(Ljava/util/List;F)Ljava/util/ArrayList;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    .line 11
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->d:F

    const/high16 p2, 0x40800000    # 4.0f

    mul-float p1, p1, p2

    .line 12
    iput p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->k:F

    .line 13
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;

    .line 2
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->-$$Nest$fgeta(Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    .line 4
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->n:Z

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->o:Landroid/net/Uri;

    .line 7
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->o:Landroid/net/Uri;

    if-eqz p1, :cond_1

    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->m:Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;

    if-eqz p1, :cond_1

    .line 8
    invoke-interface {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;->b()V

    :cond_1
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 1
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;-><init>(Landroid/os/Parcelable;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->d:F

    const/high16 v3, 0x3f800000    # 1.0f

    div-float/2addr v3, v2

    invoke-static {v0, v3}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->a(Ljava/util/List;F)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->-$$Nest$fputa(Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;Ljava/util/ArrayList;)V

    .line 7
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->n:Z

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->-$$Nest$fputb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;Z)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->o:Landroid/net/Uri;

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->-$$Nest$fputc(Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;Landroid/net/Uri;)V

    return-object v1
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    const/4 v2, 0x0

    if-eq v0, v1, :cond_3

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    const/4 p1, 0x3

    if-eq v0, p1, :cond_0

    goto/16 :goto_0

    .line 2
    :cond_0
    iput-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    goto/16 :goto_0

    .line 3
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->k:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->i:I

    int-to-float v4, v4

    sub-float/2addr v4, v2

    .line 4
    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->k:F

    div-float/2addr v4, v3

    iget v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->j:I

    int-to-float v3, v3

    sub-float/2addr v3, v4

    .line 6
    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 7
    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 8
    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->e:F

    iget v2, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v2, 0x40800000    # 4.0f

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_2

    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->f:F

    iget v3, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_2

    goto :goto_0

    .line 11
    :cond_2
    iget v0, v4, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->e:F

    .line 12
    iget v0, v4, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->f:F

    .line 14
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    if-eqz v3, :cond_5

    .line 16
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v7

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSize()F

    move-result v9

    .line 17
    invoke-static/range {v3 .. v9}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->-$$Nest$ma(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;Landroid/graphics/PointF;JFIF)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->m:Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;

    if-eqz p1, :cond_5

    .line 20
    invoke-interface {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;->c()V

    goto :goto_0

    .line 21
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    if-eqz p1, :cond_5

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->g:Ljava/util/ArrayList;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    iput-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 24
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->m:Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;

    if-eqz p1, :cond_5

    .line 25
    invoke-interface {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;->c()V

    goto :goto_0

    .line 26
    :cond_4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->a(Landroid/view/MotionEvent;)V

    .line 40
    :cond_5
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return v1
.end method

.method public setActive(Ljava/lang/Boolean;)V
    .locals 0

    return-void
.end method

.method public setInkColor(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->l:I

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->m:Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;

    return-void
.end method

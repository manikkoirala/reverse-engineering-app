.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/g;
.super Lcom/pspdfkit/internal/ui/dialog/signatures/f;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;
.implements Lcom/pspdfkit/internal/lf$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/dialog/signatures/g$a;
    }
.end annotation


# instance fields
.field private c:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;

.field private d:Landroid/view/ViewGroup;

.field private e:Landroid/view/ViewGroup;

.field private f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private g:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

.field private h:Z

.field private i:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$Bx-0c0U1dnVncaPeEq8jM-1F7Pc(Lcom/pspdfkit/internal/ui/dialog/signatures/g;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->b(Lcom/pspdfkit/internal/ui/dialog/signatures/g;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$wj0PnEcHPN2yQXVYd6HcpMH6Lo8(Lcom/pspdfkit/internal/ui/dialog/signatures/g;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/g;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureOptions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/f;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->a(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/ui/dialog/signatures/g;)Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    return-object p0
.end method

.method private final a(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V
    .locals 5

    .line 2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_image_signature:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    .line 4
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 5
    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_width:I

    .line 6
    sget v2, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_height:I

    .line 7
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/res/Resources;II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->h:Z

    .line 12
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 14
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->h:Z

    if-eqz v1, :cond_0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__image_electronic_signature_dialog_layout:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/pspdfkit/R$layout;->pspdf__image_electronic_signature_layout:I

    :goto_0
    const/4 v2, 0x1

    .line 15
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 20
    sget v0, Lcom/pspdfkit/R$color;->pspdf__electronic_signature_bg_color:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 21
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_controller_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf_\u2026ure_controller_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->d:Landroid/view/ViewGroup;

    .line 22
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_canvas_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf_\u2026gnature_canvas_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->e:Landroid/view/ViewGroup;

    .line 24
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_fab_accept_edited_signature:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf_\u2026_accept_edited_signature)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const-string v1, "acceptSignatureFab"

    const/4 v3, 0x0

    if-nez v0, :cond_1

    .line 25
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v3

    :cond_1
    sget v4, Lcom/pspdfkit/R$color;->pspdf__color_teal:I

    invoke-static {p1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    invoke-static {v4}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v3

    :cond_2
    sget v4, Lcom/pspdfkit/R$drawable;->pspdf__ic_done:I

    invoke-virtual {v0, v4}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setImageResource(I)V

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v3

    .line 28
    :cond_3
    sget v4, Lcom/pspdfkit/R$color;->pspdf__color_black:I

    invoke-static {p1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 29
    invoke-virtual {v0, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setColorFilter(I)V

    .line 32
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v3

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 33
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v3

    :cond_5
    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    .line 36
    sget p1, Lcom/pspdfkit/R$id;->pspdf__signature_canvas_view:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.pspdf__signature_canvas_view)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;

    const-string v0, "signatureCanvasView"

    if-nez p1, :cond_6

    .line 37
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v3

    :cond_6
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;)V

    .line 41
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;

    if-nez p1, :cond_7

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v3

    :cond_7
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->setOnImagePickedListener(Lcom/pspdfkit/internal/lf$c;)V

    .line 43
    sget p1, Lcom/pspdfkit/R$id;->pspdf__electronic_signature_save_chip:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.pspdf_\u2026onic_signature_save_chip)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    if-nez p1, :cond_8

    const-string p1, "saveSignatureChip"

    .line 44
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v3

    :cond_8
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/signatures/g$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/g$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/g;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez p1, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_1

    :cond_9
    move-object v3, p1

    :goto_1
    new-instance p1, Lcom/pspdfkit/internal/ui/dialog/signatures/g$$ExternalSyntheticLambda1;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/g$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/g;)V

    invoke-virtual {v3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    invoke-virtual {p2}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object p1

    sget-object p2, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;->SAVE_IF_SELECTED:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    if-ne p1, p2, :cond_a

    goto :goto_2

    :cond_a
    const/4 v2, 0x0

    :goto_2
    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->setSaveSignatureChipVisible(Z)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/ui/dialog/signatures/g;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    const/4 v0, 0x0

    const-string v1, "saveSignatureChip"

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v0

    :cond_0
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    if-nez p0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, p0

    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;->setSelected(Z)V

    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/ui/dialog/signatures/g;)Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;

    return-object p0
.end method

.method private static final b(Lcom/pspdfkit/internal/ui/dialog/signatures/g;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;

    if-nez p1, :cond_0

    const-string p1, "signatureCanvasView"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->getSignatureImage()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/signatures/g$b;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/g$b;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/g;)V

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/g$c;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/g$c;

    invoke-virtual {p1, v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->i:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private final setSaveSignatureChipVisible(Z)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "saveSignatureChip"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const/4 v2, 0x0

    if-eqz p1, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 5
    iget-boolean v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->h:Z

    if-nez v3, :cond_9

    const/4 v3, 0x2

    if-ne v0, v3, :cond_9

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->d:Landroid/view/ViewGroup;

    const-string v4, "saveSignatureChipContainer"

    if-nez v0, :cond_2

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    if-eqz p1, :cond_3

    .line 9
    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__electronic_signature_controls_view_background:I

    .line 10
    :cond_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    const/4 v0, 0x6

    const-string v2, "signatureCanvasContainer"

    const-string v5, "null cannot be cast to non-null type android.widget.RelativeLayout.LayoutParams"

    if-eqz p1, :cond_6

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->e:Landroid/view/ViewGroup;

    if-nez p1, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v1

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 20
    sget v2, Lcom/pspdfkit/R$id;->pspdf__signature_fab_accept_edited_signature:I

    invoke-virtual {p1, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->d:Landroid/view/ViewGroup;

    if-nez p1, :cond_5

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    move-object v1, p1

    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 22
    sget v1, Lcom/pspdfkit/R$id;->pspdf__signature_fab_accept_edited_signature:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_3

    .line 24
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->e:Landroid/view/ViewGroup;

    if-nez p1, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v1

    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 25
    invoke-virtual {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->d:Landroid/view/ViewGroup;

    if-nez p1, :cond_8

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_2

    :cond_8
    move-object v1, p1

    :goto_2
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 27
    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    :cond_9
    :goto_3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final b()V
    .locals 4

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "signatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->getSignatureUri()Landroid/net/Uri;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_5

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const-string v3, "acceptSignatureFab"

    if-nez v0, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    invoke-virtual {v0, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_3
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move-object v1, v0

    :goto_1
    invoke-virtual {v1, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    :cond_5
    return-void
.end method

.method public final c()V
    .locals 0

    return-void
.end method

.method public final d()V
    .locals 4

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/mq;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    const-string v1, "acceptSignatureFab"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_0
    const/4 v3, 0x1

    .line 3
    invoke-direct {v0, v1, v3}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;I)V

    .line 4
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;

    if-nez v0, :cond_1

    const-string v0, "signatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->setSignatureUri(Landroid/net/Uri;)V

    return-void
.end method

.method public final e()V
    .locals 0

    return-void
.end method

.method public final f()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;

    if-nez v0, :cond_0

    const-string v0, "signatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c()V

    return-void
.end method

.method public getCanvasView()Lcom/pspdfkit/internal/ui/dialog/signatures/i;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;

    if-nez v0, :cond_0

    const-string v0, "signatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->i:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->i:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public final onImagePicked(Landroid/net/Uri;)V
    .locals 4

    const-string v0, "imageUri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;

    const-string v1, "signatureCanvasView"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->setSignatureUri(Landroid/net/Uri;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_1
    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p1, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->n:Z

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const-string v3, "acceptSignatureFab"

    if-nez p1, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_6

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v2

    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->getSignatureUri()Landroid/net/Uri;

    move-result-object p1

    if-eqz p1, :cond_4

    const/4 v0, 0x1

    :cond_4
    if-eqz v0, :cond_6

    .line 6
    new-instance p1, Lcom/pspdfkit/internal/mq;

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    move-object v2, v0

    :goto_0
    const/4 v0, 0x2

    .line 8
    invoke-direct {p1, v2, v0}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;I)V

    .line 9
    invoke-static {p1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 17
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_6
    return-void
.end method

.method public final onImagePickerCancelled()V
    .locals 0

    return-void
.end method

.method public final onImagePickerUnknownError()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__file_not_available:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/g$a;

    .line 2
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/g$a;->a()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->setSaveSignatureChipVisible(Z)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    if-nez v0, :cond_0

    const-string v0, "saveSignatureChip"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/g$a;->b()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;->setSelected(Z)V

    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 1
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/g$a;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/g$a;-><init>(Landroid/os/Parcelable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    const/4 v2, 0x0

    const-string v3, "saveSignatureChip"

    if-nez v0, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/g$a;->a(Z)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/g;->g:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    if-nez v0, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v2, v0

    :goto_1
    invoke-virtual {v2}, Landroid/view/View;->isSelected()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/g$a;->b(Z)V

    return-object v1
.end method

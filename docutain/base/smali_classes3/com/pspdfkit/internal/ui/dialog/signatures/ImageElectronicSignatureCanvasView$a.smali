.class final Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView;->getSignatureImage()Lio/reactivex/rxjava3/core/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Function;"
    }
.end annotation


# static fields
.field public static final a:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView$a<",
            "TT;TR;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView$a;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView$a;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView$a;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ImageElectronicSignatureCanvasView$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    const-string v0, "bitmap"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-direct {v0, v3, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 275
    invoke-static {p1, v0, v1, v1, v2}, Lcom/pspdfkit/signatures/Signature;->create(Landroid/graphics/Bitmap;Landroid/graphics/RectF;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;F)Lcom/pspdfkit/signatures/Signature;

    move-result-object p1

    .line 276
    invoke-static {p1}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

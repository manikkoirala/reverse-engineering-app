.class final Lcom/pspdfkit/internal/ui/dialog/signatures/q$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/ui/dialog/signatures/q;->a(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/ui/dialog/signatures/q;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ui/dialog/signatures/q;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q$b;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 4

    .line 1
    check-cast p1, Lcom/pspdfkit/signatures/Signature;

    const-string v0, "signature"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q$b;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/q;

    iget-object v1, v0, Lcom/pspdfkit/internal/ui/dialog/signatures/f;->b:Lcom/pspdfkit/internal/pa;

    if-eqz v1, :cond_2

    .line 176
    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->b(Lcom/pspdfkit/internal/ui/dialog/signatures/q;)Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    move-result-object v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    const-string v2, "typingElectronicSignatureCanvasView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v2, v3

    .line 177
    :cond_0
    invoke-virtual {v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->e()Lcom/pspdfkit/ui/signatures/SignatureUiData;

    move-result-object v2

    .line 178
    invoke-interface {v1, p1, v2}, Lcom/pspdfkit/internal/pa;->onSignatureUiDataCollected(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V

    .line 183
    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/q;)Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "saveSignatureChip"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v3, v0

    :goto_0
    invoke-virtual {v3}, Landroid/view/View;->isSelected()Z

    move-result v0

    invoke-interface {v1, p1, v0}, Lcom/pspdfkit/internal/pa;->onSignatureCreated(Lcom/pspdfkit/signatures/Signature;Z)V

    :cond_2
    return-void
.end method

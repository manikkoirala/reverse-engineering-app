.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/dialog/signatures/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/ur;

.field final synthetic c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;


# direct methods
.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;)Lcom/pspdfkit/internal/ur;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->b:Lcom/pspdfkit/internal/ur;

    return-object p0
.end method

.method constructor <init>(Lcom/pspdfkit/internal/ui/dialog/signatures/l;Lcom/pspdfkit/internal/ur;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    .line 2
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->b:Lcom/pspdfkit/internal/ur;

    .line 4
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 p1, 0x1

    .line 5
    invoke-virtual {p2, p1}, Landroid/view/View;->setLongClickable(Z)V

    .line 6
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 2
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_2

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgeta(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/signatures/Signature;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->b:Lcom/pspdfkit/internal/ur;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ur;->setChecked(Z)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;->a(Lcom/pspdfkit/signatures/Signature;)V

    goto :goto_0

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->b:Lcom/pspdfkit/internal/ur;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ur;->setChecked(Z)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;->b(Lcom/pspdfkit/signatures/Signature;)V

    goto :goto_0

    .line 14
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;->onSignaturePicked(Lcom/pspdfkit/signatures/Signature;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgeta(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/signatures/Signature;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->b:Lcom/pspdfkit/internal/ur;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ur;->setChecked(Z)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;->b(Lcom/pspdfkit/signatures/Signature;)V

    return v1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

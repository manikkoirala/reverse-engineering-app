.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/q;
.super Lcom/pspdfkit/internal/ui/dialog/signatures/f;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;
.implements Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$e;
.implements Lcom/pspdfkit/internal/hl;
.implements Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/dialog/signatures/q$a;
    }
.end annotation


# instance fields
.field private c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;

.field private d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

.field private e:Landroid/view/ViewGroup;

.field private f:Landroid/view/ViewGroup;

.field private g:Landroidx/recyclerview/widget/RecyclerView;

.field private h:Lcom/pspdfkit/internal/po;

.field private i:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private j:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

.field private k:Z

.field private l:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$EqD1TsCjEo-350of8TBGWq_vqzk(Lcom/pspdfkit/internal/ui/dialog/signatures/q;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/q;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$GvitSCYTWhZ0FZ2CfQr8Mhl9Q20(Lcom/pspdfkit/internal/ui/dialog/signatures/q;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->b(Lcom/pspdfkit/internal/ui/dialog/signatures/q;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureOptions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/f;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->a(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/ui/dialog/signatures/q;)Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->j:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    return-object p0
.end method

.method private final a(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V
    .locals 7

    .line 2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_typing_signature:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    .line 4
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 5
    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_width:I

    .line 6
    sget v2, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_height:I

    .line 7
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/res/Resources;II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->k:Z

    .line 12
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 14
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->k:Z

    if-eqz v1, :cond_0

    .line 15
    sget v1, Lcom/pspdfkit/R$layout;->pspdf__typing_electronic_signature_dialog_layout:I

    goto :goto_0

    .line 18
    :cond_0
    sget v1, Lcom/pspdfkit/R$layout;->pspdf__typing_electronic_signature_layout:I

    :goto_0
    const/4 v2, 0x1

    .line 19
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 29
    sget v0, Lcom/pspdfkit/R$color;->pspdf__electronic_signature_bg_color:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 30
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_controller_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf_\u2026ure_controller_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->e:Landroid/view/ViewGroup;

    .line 31
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_canvas_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf_\u2026gnature_canvas_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->f:Landroid/view/ViewGroup;

    .line 33
    sget v0, Lcom/pspdfkit/R$id;->pspdf__electronic_signature_typing_font_list:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    .line 34
    new-instance v4, Lcom/pspdfkit/internal/po;

    .line 36
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getAvailableFonts(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 37
    invoke-direct {v4, p1, v5, p0}, Lcom/pspdfkit/internal/po;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/pspdfkit/internal/hl;)V

    iput-object v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->h:Lcom/pspdfkit/internal/po;

    .line 42
    new-instance v4, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v4, p1, v2, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v4}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 43
    new-instance v4, Landroidx/recyclerview/widget/DividerItemDecoration;

    invoke-direct {v4, p1, v2}, Landroidx/recyclerview/widget/DividerItemDecoration;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v4}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 44
    iget-object v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->h:Lcom/pspdfkit/internal/po;

    invoke-virtual {v0, v4}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    goto :goto_1

    :cond_1
    move-object v0, v3

    .line 45
    :goto_1
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->g:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_2

    .line 55
    sget v4, Lcom/pspdfkit/R$color;->pspdf__electronic_signature_font_select_bg_color:I

    invoke-static {p1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 58
    :cond_2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_canvas_view:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    .line 59
    invoke-virtual {p2}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-result-object v5

    invoke-interface {v5, p1}, Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;->option1(Landroid/content/Context;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->setInkColor(I)V

    .line 60
    invoke-virtual {v4, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;)V

    .line 61
    invoke-virtual {v4, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->setOnSignatureTypedListener(Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView$a;)V

    const-string v5, "findViewById<TypingElect\u2026ignatureLayout)\n        }"

    .line 62
    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    .line 69
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_controller_view:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;

    .line 70
    invoke-virtual {v4, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$e;)V

    .line 71
    invoke-virtual {v4, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->setOnFontSelectionListener(Lcom/pspdfkit/internal/hl;)V

    const-string v5, "findViewById<ElectronicS\u2026ignatureLayout)\n        }"

    .line 72
    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;

    const-string v0, "signatureControllerView"

    if-nez v4, :cond_3

    .line 76
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v4, v3

    .line 77
    :cond_3
    iget-boolean v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->k:Z

    if-eqz v5, :cond_4

    sget-object v5, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    goto :goto_2

    .line 78
    :cond_4
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    sget-object v5, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    goto :goto_2

    .line 79
    :cond_5
    sget-object v5, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    .line 80
    :goto_2
    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->setOrientation(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;)V

    .line 86
    iget-object v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;

    if-nez v4, :cond_6

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    move-object v3, v4

    :goto_3
    invoke-virtual {p2}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;)V

    .line 87
    sget v0, Lcom/pspdfkit/R$id;->pspdf__electronic_signature_save_chip:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    .line 88
    new-instance v4, Lcom/pspdfkit/internal/ui/dialog/signatures/q$$ExternalSyntheticLambda0;

    invoke-direct {v4, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/q$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/q;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v4, "findViewById<SaveSignatu\u2026ip.isSelected }\n        }"

    .line 89
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->j:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    .line 92
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_fab_accept_edited_signature:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 93
    sget v4, Lcom/pspdfkit/R$color;->pspdf__color_teal:I

    invoke-static {p1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    invoke-static {v4}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 94
    sget v4, Lcom/pspdfkit/R$drawable;->pspdf__ic_done:I

    invoke-virtual {v3, v4}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setImageResource(I)V

    .line 95
    sget v4, Lcom/pspdfkit/R$color;->pspdf__color_black:I

    invoke-static {p1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v3, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setColorFilter(I)V

    const/4 p1, 0x0

    .line 96
    invoke-virtual {v3, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 97
    invoke-virtual {v3, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    .line 99
    new-instance p1, Lcom/pspdfkit/internal/ui/dialog/signatures/q$$ExternalSyntheticLambda1;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/q$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/q;)V

    invoke-virtual {v3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string p1, "findViewById<FloatingAct\u2026}\n            }\n        }"

    .line 100
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->i:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 132
    invoke-virtual {p2}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object p1

    sget-object p2, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;->SAVE_IF_SELECTED:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    if-ne p1, p2, :cond_7

    goto :goto_4

    :cond_7
    const/4 v2, 0x0

    :goto_4
    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->setSaveSignatureChipVisible(Z)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/ui/dialog/signatures/q;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->j:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    const/4 v0, 0x0

    const-string v1, "saveSignatureChip"

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v0

    :cond_0
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->j:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    if-nez p0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, p0

    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;->setSelected(Z)V

    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/ui/dialog/signatures/q;)Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    return-object p0
.end method

.method private static final b(Lcom/pspdfkit/internal/ui/dialog/signatures/q;Landroid/view/View;)V
    .locals 3

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    const/4 v0, 0x0

    const-string v1, "typingElectronicSignatureCanvasView"

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v0

    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->getSelectedFontOrDefault()Lcom/pspdfkit/ui/fonts/Font;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, v2

    :goto_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->a(Lcom/pspdfkit/ui/fonts/Font;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/signatures/q$b;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/q$b;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/q;)V

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/q$c;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/q$c;

    invoke-virtual {p1, v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->l:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void

    .line 6
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Selected font used for creating a signature was null."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private final setSaveSignatureChipVisible(Z)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->j:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "saveSignatureChip"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const/4 v2, 0x0

    if-eqz p1, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 5
    iget-boolean v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->k:Z

    if-nez v3, :cond_b

    const/4 v3, 0x2

    if-ne v0, v3, :cond_b

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;

    if-nez v0, :cond_2

    const-string v0, "signatureControllerView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    if-eqz p1, :cond_3

    .line 9
    sget-object v4, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    goto :goto_1

    :cond_3
    sget-object v4, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    .line 10
    :goto_1
    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->setOrientation(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->e:Landroid/view/ViewGroup;

    const-string v4, "signatureControllerContainer"

    if-nez v0, :cond_4

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_4
    if-eqz p1, :cond_5

    .line 15
    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__electronic_signature_controls_view_background:I

    .line 16
    :cond_5
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    const/4 v0, 0x6

    const-string v2, "signatureCanvasContainer"

    const-string v5, "null cannot be cast to non-null type android.widget.RelativeLayout.LayoutParams"

    if-eqz p1, :cond_8

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->f:Landroid/view/ViewGroup;

    if-nez p1, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v1

    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 27
    sget v2, Lcom/pspdfkit/R$id;->pspdf__signature_fab_accept_edited_signature:I

    invoke-virtual {p1, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 28
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->e:Landroid/view/ViewGroup;

    if-nez p1, :cond_7

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    move-object v1, p1

    :goto_2
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 29
    sget v1, Lcom/pspdfkit/R$id;->pspdf__signature_fab_accept_edited_signature:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_4

    .line 31
    :cond_8
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->f:Landroid/view/ViewGroup;

    if-nez p1, :cond_9

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v1

    :cond_9
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 32
    invoke-virtual {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 33
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->e:Landroid/view/ViewGroup;

    if-nez p1, :cond_a

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_3

    :cond_a
    move-object v1, p1

    :goto_3
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 34
    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    :cond_b
    :goto_4
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    if-nez v0, :cond_0

    const-string v0, "typingElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->setInkColor(I)V

    .line 135
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->h:Lcom/pspdfkit/internal/po;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/po;->a(I)V

    :cond_1
    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/fonts/Font;)V
    .locals 1

    const-string v0, "font"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    if-nez v0, :cond_0

    const-string v0, "typingElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->setSelectedFont(Lcom/pspdfkit/ui/fonts/Font;)V

    return-void
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "signatureControllerView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    move-object v2, v1

    :goto_0
    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->setTypedSignature(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->h:Lcom/pspdfkit/internal/po;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/po;->a(Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method public final b()V
    .locals 4

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "typingElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->g()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->i:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const-string v2, "acceptSignatureFab"

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->i:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->i:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v1, v0

    :goto_0
    invoke-virtual {v1, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    :cond_4
    return-void
.end method

.method public final c()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->i:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const-string v1, "acceptSignatureFab"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    if-nez v0, :cond_1

    const-string v0, "typingElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->g()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/mq;

    .line 4
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->i:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v3, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v2, v3

    :goto_0
    const/4 v1, 0x2

    .line 5
    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;I)V

    .line 6
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_3
    return-void
.end method

.method public final d()V
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/mq;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->i:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v1, :cond_0

    const-string v1, "acceptSignatureFab"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_0
    const/4 v2, 0x1

    .line 3
    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;I)V

    .line 4
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final e()V
    .locals 0

    return-void
.end method

.method public final f()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    if-nez v0, :cond_0

    const-string v0, "typingElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->c()V

    return-void
.end method

.method public getCanvasView()Lcom/pspdfkit/internal/ui/dialog/signatures/i;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    if-nez v0, :cond_0

    const-string v0, "typingElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->l:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->l:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 2

    .line 1
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 2
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->k:Z

    if-nez p1, :cond_2

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->g:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz p1, :cond_2

    const/4 p2, 0x0

    .line 5
    iget-object p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    const/4 p4, 0x0

    if-nez p3, :cond_0

    const-string p3, "typingElectronicSignatureCanvasView"

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p3, p4

    :cond_0
    invoke-virtual {p3}, Landroid/view/View;->getMeasuredHeight()I

    move-result p3

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p5

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->i:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v1, :cond_1

    const-string v1, "acceptSignatureFab"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object p4, v1

    :goto_0
    invoke-virtual {p4}, Landroid/view/View;->getMeasuredHeight()I

    move-result p4

    sub-int/2addr v0, p4

    .line 8
    invoke-virtual {p1, p2, p3, p5, v0}, Landroid/view/View;->layout(IIII)V

    :cond_2
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 2

    .line 1
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->k:Z

    if-nez p1, :cond_2

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->g:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz p1, :cond_2

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "typingElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr p2, v0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->i:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_1

    const-string v0, "acceptSignatureFab"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr p2, v0

    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    iput p2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_2
    return-void
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/q$a;

    .line 2
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "typingElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/q$a;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->setInkColor(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;

    if-nez v0, :cond_1

    const-string v0, "signatureControllerView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/q$a;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->setCurrentlySelectedColor(I)V

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/q$a;->c()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->setSaveSignatureChipVisible(Z)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->j:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    if-nez v0, :cond_2

    const-string v0, "saveSignatureChip"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/q$a;->d()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;->setSelected(Z)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->h:Lcom/pspdfkit/internal/po;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/q$a;->b()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/po;->b(I)I

    move-result p1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->g:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_3
    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .line 1
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/q$a;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/q$a;-><init>(Landroid/os/Parcelable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    const-string v2, "typingElectronicSignatureCanvasView"

    const/4 v3, 0x0

    if-nez v0, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v3

    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->getInkColor()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/q$a;->a(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->j:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    const-string v4, "saveSignatureChip"

    if-nez v0, :cond_1

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v3

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/q$a;->a(Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->j:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    if-nez v0, :cond_3

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v3

    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/q$a;->b(Z)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/q;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;

    if-nez v0, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move-object v3, v0

    :goto_1
    invoke-virtual {v3}, Lcom/pspdfkit/internal/ui/dialog/signatures/TypingElectronicSignatureCanvasView;->getSelectedFontOrDefault()Lcom/pspdfkit/ui/fonts/Font;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/pspdfkit/ui/fonts/Font;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_5
    const/4 v0, -0x1

    :goto_2
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/q$a;->b(I)V

    return-object v1
.end method

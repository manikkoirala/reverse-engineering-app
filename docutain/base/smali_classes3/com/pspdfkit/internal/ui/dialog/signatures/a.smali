.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/a;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;
.implements Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/a$b;
    }
.end annotation


# instance fields
.field private b:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

.field private c:Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;

.field private d:Lcom/pspdfkit/internal/ui/dialog/signatures/LegacySignatureCanvasView;

.field private e:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private g:Landroid/widget/CheckBox;

.field private h:Lcom/pspdfkit/internal/ui/dialog/signatures/a$b;

.field private i:Ljava/lang/String;

.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/signatures/signers/Signer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$OKOyVEQ-Wb1VXKVns5FjErY0aUM(Lcom/pspdfkit/internal/ui/dialog/signatures/a;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->a(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$OMdMwJ6yn4DsiW-ys0Yh60f7Xr0(Lcom/pspdfkit/internal/ui/dialog/signatures/a;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->b(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$TAlnicXemXqWCbsjT1Jqz9-PpX8(Lcom/pspdfkit/internal/ui/dialog/signatures/a;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->c(Landroid/view/View;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetSignerIdentifier(Lcom/pspdfkit/internal/ui/dialog/signatures/a;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->setSignerIdentifier(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 9

    .line 1
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__signature_layout_add_new_signature:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout:[I

    .line 3
    sget v1, Lcom/pspdfkit/R$attr;->pspdf__signatureLayoutStyle:I

    .line 4
    sget v2, Lcom/pspdfkit/R$style;->PSPDFKit_SignatureLayout:I

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 27
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__clearSignatureCanvasIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_delete:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 29
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__clearSignatureCanvasIconColor:I

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 31
    sget v3, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__clearSignatureCanvasIconBackgroundColor:I

    const v5, -0x777778

    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 33
    sget v5, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__acceptSignatureIcon:I

    sget v6, Lcom/pspdfkit/R$drawable;->pspdf__ic_done:I

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    .line 35
    sget v6, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__acceptSignatureIconColor:I

    sget v7, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 37
    invoke-static {p1, v7}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v7

    .line 38
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v6

    .line 41
    sget v7, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__acceptSignatureIconBackgroundColor:I

    sget v8, Lcom/pspdfkit/R$color;->pspdf__color_teal:I

    .line 43
    invoke-static {p1, v8}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 44
    invoke-virtual {v0, v7, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    .line 47
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    const/16 v0, 0x11

    .line 48
    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 51
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_canvas_view:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/LegacySignatureCanvasView;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/LegacySignatureCanvasView;

    .line 52
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;)V

    .line 55
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_controller_view:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;

    .line 56
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView$a;)V

    .line 58
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_signer_chip:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    const/16 v7, 0x8

    .line 59
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 60
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    new-instance v7, Lcom/pspdfkit/internal/ui/dialog/signatures/a$$ExternalSyntheticLambda0;

    invoke-direct {v7, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/a$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/a;)V

    invoke-virtual {v0, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;->setSigner(Lcom/pspdfkit/signatures/signers/Signer;)V

    .line 63
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_store_checkbox:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->g:Landroid/widget/CheckBox;

    .line 65
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_fab_accept_edited_signature:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->e:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 66
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 67
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->e:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1, v5}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setImageResource(I)V

    .line 68
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->e:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1, v6}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setColorFilter(I)V

    .line 69
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->e:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 70
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->e:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    .line 71
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->e:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    new-instance v4, Lcom/pspdfkit/internal/ui/dialog/signatures/a$$ExternalSyntheticLambda1;

    invoke-direct {v4, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/a$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/a;)V

    invoke-virtual {p1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    sget p1, Lcom/pspdfkit/R$id;->pspdf__signature_fab_clear_edited_signature:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 82
    invoke-virtual {p1, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setImageResource(I)V

    .line 83
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v2, v1}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 84
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 85
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 86
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    .line 87
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/signatures/a$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/a$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/a;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private synthetic a(Landroid/view/View;)V
    .locals 0

    .line 88
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->f()V

    return-void
.end method

.method private synthetic b(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/LegacySignatureCanvasView;

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->i:Ljava/lang/String;

    .line 2
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->a(Ljava/lang/String;)Lcom/pspdfkit/signatures/Signature;

    move-result-object p1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/a$b;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/LegacySignatureCanvasView;

    .line 5
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->e()Lcom/pspdfkit/ui/signatures/SignatureUiData;

    move-result-object v1

    .line 6
    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->a(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/a$b;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->g:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->a(Lcom/pspdfkit/signatures/Signature;Z)V

    :cond_0
    return-void
.end method

.method private synthetic c(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/LegacySignatureCanvasView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c()V

    return-void
.end method

.method private f()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->j:Ljava/util/Map;

    .line 2
    new-instance v1, Landroid/view/ContextThemeWrapper;

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Landroidx/appcompat/R$style;->Widget_AppCompat_PopupMenu:I

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 5
    new-instance v2, Lcom/pspdfkit/internal/ui/dialog/signatures/n;

    invoke-direct {v2, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/n;-><init>(Landroid/view/ContextThemeWrapper;)V

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->setSigners(Ljava/util/Map;)V

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->i:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->setSelectedSignerIdentifier(Ljava/lang/String;)V

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "window"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    const/4 v3, 0x2

    if-eqz v0, :cond_1

    .line 16
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 17
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 18
    iget v0, v4, Landroid/graphics/Point;->x:I

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getPaddingStart()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v0, v5

    .line 19
    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getPaddingEnd()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    goto :goto_0

    .line 21
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 22
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    :goto_0
    const/high16 v5, -0x80000000

    .line 25
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 26
    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 27
    invoke-virtual {v2, v0, v4}, Landroid/view/View;->measure(II)V

    .line 28
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    .line 29
    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    const/4 v1, -0x2

    .line 30
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 31
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    const/4 v1, 0x1

    .line 32
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 33
    invoke-static {v0, v1}, Landroidx/core/widget/PopupWindowCompat;->setOverlapAnchor(Landroid/widget/PopupWindow;Z)V

    .line 34
    invoke-static {}, Lcom/pspdfkit/internal/v;->c()Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    .line 35
    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 39
    :cond_2
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 42
    :goto_1
    new-instance v4, Lcom/pspdfkit/internal/ui/dialog/signatures/a$a;

    invoke-direct {v4, p0, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/a$a;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/a;Landroid/widget/PopupWindow;)V

    invoke-virtual {v2, v4}, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->setOnSignerClickedListener(Lcom/pspdfkit/internal/ui/dialog/signatures/n$a;)V

    new-array v2, v3, [I

    .line 57
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    invoke-virtual {v3, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 58
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    aget v1, v2, v1

    const/16 v2, 0x31

    invoke-virtual {v0, v3, v2, v5, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    return-void
.end method

.method private setSignerIdentifier(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->j:Ljava/util/Map;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/signatures/signers/Signer;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    .line 3
    :goto_1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->i:Ljava/lang/String;

    .line 4
    invoke-static {p0}, Landroidx/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;->setSigner(Lcom/pspdfkit/signatures/signers/Signer;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/LegacySignatureCanvasView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/LegacySignatureCanvasView;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->setInkColor(I)V

    return-void
.end method

.method public final a(Ljava/util/Map;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/signatures/signers/Signer;",
            ">;",
            "Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 89
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->j:Ljava/util/Map;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 90
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p3, :cond_1

    .line 92
    invoke-static {}, Lcom/pspdfkit/signatures/SignatureManager;->getSigners()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 94
    invoke-direct {p0, p3}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->setSignerIdentifier(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    new-array p3, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Signatures"

    const-string v3, "Specified default signer was not found in the list of registered signers inside the SignatureManager."

    .line 96
    invoke-static {v2, v3, p3}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 p3, 0x0

    .line 102
    :goto_1
    sget-object v2, Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;->ALWAYS:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    if-eq p2, v2, :cond_4

    sget-object v3, Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;->IF_AVAILABLE:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    if-ne p2, v3, :cond_2

    if-nez p1, :cond_4

    :cond_2
    if-eqz p3, :cond_3

    goto :goto_2

    :cond_3
    const/4 p3, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 p3, 0x1

    :goto_3
    if-eq p2, v2, :cond_6

    .line 106
    sget-object v2, Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;->IF_AVAILABLE:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    if-ne p2, v2, :cond_5

    if-eqz p1, :cond_5

    goto :goto_4

    :cond_5
    const/4 v0, 0x0

    .line 109
    :cond_6
    :goto_4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    if-eqz p3, :cond_7

    goto :goto_5

    :cond_7
    const/16 v1, 0x8

    :goto_5
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 110
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;->setClickable(Z)V

    return-void
.end method

.method public final b()V
    .locals 4

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/LegacySignatureCanvasView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->getCurrentLines()Ljava/util/List;

    move-result-object v0

    .line 11
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-gt v1, v3, :cond_1

    .line 12
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v3, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->a()I

    move-result v0

    if-le v0, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :cond_1
    :goto_0
    const/high16 v0, 0x3f800000    # 1.0f

    if-eqz v3, :cond_2

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->e:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v1, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->e:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 15
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->e:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    .line 18
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v1, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 19
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    return-void
.end method

.method public final c()V
    .locals 5

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->e:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/LegacySignatureCanvasView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->getCurrentLines()Ljava/util/List;

    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-gt v1, v3, :cond_0

    .line 6
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v3, :cond_1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->a()I

    move-result v0

    if-le v0, v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    :cond_1
    if-eqz v2, :cond_2

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->e:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 8
    new-instance v1, Lcom/pspdfkit/internal/mq;

    const/4 v2, 0x2

    const-wide/16 v3, 0xc8

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_2
    return-void
.end method

.method public final d()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->e:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/mq;

    const/4 v2, 0x1

    const-wide/16 v3, 0xc8

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 4
    new-instance v5, Lcom/pspdfkit/internal/mq;

    invoke-direct {v5, v1, v2, v3, v4}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {v5}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 5
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->mergeWith(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final e()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/mq;

    const/4 v2, 0x2

    const-wide/16 v3, 0xc8

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;

    .line 2
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/LegacySignatureCanvasView;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;->-$$Nest$fgeta(Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->setInkColor(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;->-$$Nest$fgeta(Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureControllerView;->setCurrentlySelectedColor(I)V

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;)Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->setStoreSignatureCheckboxVisible(Z)V

    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 1
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;-><init>(Landroid/os/Parcelable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/LegacySignatureCanvasView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->getInkColor()I

    move-result v0

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;->-$$Nest$fputa(Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;->-$$Nest$fputb(Lcom/pspdfkit/internal/ui/dialog/signatures/a$c;Z)V

    return-object v1
.end method

.method public setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/a$b;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/a$b;

    return-void
.end method

.method public setStoreSignatureCheckboxVisible(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/a;->g:Landroid/widget/CheckBox;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/h;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/dialog/signatures/h$a;
    }
.end annotation


# instance fields
.field private b:Lcom/pspdfkit/internal/ui/dialog/signatures/h$a;

.field private c:Landroid/widget/TextView;

.field private d:Ljava/lang/String;


# direct methods
.method public static synthetic $r8$lambda$ir6mCLVKNfqCW6Ccn_94Bsut2d8(Lcom/pspdfkit/internal/ui/dialog/signatures/h;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/h;->a(Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/h;->a()V

    return-void
.end method

.method private a()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__signer_list_selected_item:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 5
    invoke-static {}, Lcom/pspdfkit/internal/v;->c()Z

    move-result v1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    new-array v4, v2, [I

    const v5, 0x101030e

    aput v5, v4, v0

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 8
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/widget/LinearLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 9
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 14
    :cond_0
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/h;->setSelected(Z)V

    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v4, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout:[I

    sget v5, Lcom/pspdfkit/R$attr;->pspdf__signatureLayoutStyle:I

    sget v6, Lcom/pspdfkit/R$style;->PSPDFKit_SignatureLayout:I

    .line 17
    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 22
    sget v4, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__signerListSelectedItemBackground:I

    invoke-virtual {v1, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 23
    sget v4, Lcom/pspdfkit/R$id;->pspdf__signature_list_signer_item_deletebutton:I

    invoke-virtual {p0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 24
    new-instance v5, Lcom/pspdfkit/internal/ui/dialog/signatures/h$$ExternalSyntheticLambda0;

    invoke-direct {v5, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/h$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/h;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    sget v4, Lcom/pspdfkit/R$id;->pspdf__signature_list_signer_item_textview:I

    invoke-virtual {p0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/h;->c:Landroid/widget/TextView;

    .line 28
    sget v5, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__signerListSelectedItemForeground:I

    .line 30
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    invoke-static {v6, v7}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v6

    .line 31
    invoke-virtual {v1, v5, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 34
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 35
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v4, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout:[I

    sget v5, Lcom/pspdfkit/R$attr;->pspdf__signatureLayoutStyle:I

    sget v6, Lcom/pspdfkit/R$style;->PSPDFKit_SignatureLayout:I

    .line 36
    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 42
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/pspdfkit/R$dimen;->pspdf__signature_signer_list_selected_item_avatar_size:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 43
    sget v5, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__signerListSelectedItemForeground:I

    .line 44
    invoke-virtual {v1, v5}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    .line 45
    new-instance v6, Lcom/pspdfkit/internal/at;

    new-instance v7, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v7}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v6, v7, v5}, Lcom/pspdfkit/internal/at;-><init>(Landroid/graphics/drawable/shapes/OvalShape;Landroid/content/res/ColorStateList;)V

    .line 47
    invoke-virtual {v6, v4}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicWidth(I)V

    .line 48
    invoke-virtual {v6, v4}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicHeight(I)V

    .line 50
    sget v4, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__signerChipIconRes:I

    invoke-virtual {v1, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 52
    sget v5, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__signerListSelectedItemBackground:I

    .line 53
    invoke-virtual {v1, v5}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    .line 54
    invoke-static {v4}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 55
    invoke-static {v4, v5}, Landroidx/core/graphics/drawable/DrawableCompat;->setTintList(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 56
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0x8

    invoke-static {v4, v5}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v4

    .line 57
    new-instance v5, Landroid/graphics/drawable/InsetDrawable;

    invoke-direct {v5, v7, v4}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 58
    new-instance v4, Landroid/graphics/drawable/LayerDrawable;

    const/4 v7, 0x2

    new-array v7, v7, [Landroid/graphics/drawable/Drawable;

    aput-object v6, v7, v0

    aput-object v5, v7, v2

    invoke-direct {v4, v7}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/h;->c:Landroid/widget/TextView;

    invoke-static {v0, v4, v3, v3, v3}, Landroidx/core/widget/TextViewCompat;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 63
    :cond_1
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 64
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/h;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/h;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    return-void
.end method

.method private synthetic a(Landroid/view/View;)V
    .locals 0

    .line 65
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/h;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/h$a;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/h$a;->a()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final onMeasure(II)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/pspdfkit/R$dimen;->pspdf__signature_signer_list_selected_item_height:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    const/high16 v0, 0x40000000    # 2.0f

    .line 2
    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 3
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    return-void
.end method

.method public setOnDeleteButtonClickedListener(Lcom/pspdfkit/internal/ui/dialog/signatures/h$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/h;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/h$a;

    return-void
.end method

.method public setSelected(Z)V
    .locals 0

    const/4 p1, 0x1

    .line 1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    return-void
.end method

.method public setSigner(Lcom/pspdfkit/signatures/signers/Signer;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/signers/Signer;->getDisplayName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/h;->d:Ljava/lang/String;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/h;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

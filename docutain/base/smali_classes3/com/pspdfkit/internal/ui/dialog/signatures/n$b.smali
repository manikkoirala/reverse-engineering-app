.class final Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/dialog/signatures/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/pspdfkit/internal/ui/dialog/signatures/n$c;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/ui/dialog/signatures/n;


# direct methods
.method public static synthetic $r8$lambda$FZ7rHDrZKk7Abhi1XLX4humDAsQ(Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;Ljava/lang/String;Lcom/pspdfkit/signatures/signers/Signer;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;->a(Ljava/lang/String;Lcom/pspdfkit/signatures/signers/Signer;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$lJRNbA4_2Oe3UFpDt4ZdOfwwNT0(Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;->a()V

    return-void
.end method

.method private constructor <init>(Lcom/pspdfkit/internal/ui/dialog/signatures/n;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/n;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/ui/dialog/signatures/n;Lcom/pspdfkit/internal/ui/dialog/signatures/n$b-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/n;)V

    return-void
.end method

.method private synthetic a()V
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/n;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->-$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/n;)Lcom/pspdfkit/internal/ui/dialog/signatures/n$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 20
    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/a$a;->a()V

    :cond_0
    return-void
.end method

.method private synthetic a(Ljava/lang/String;Lcom/pspdfkit/signatures/signers/Signer;Landroid/view/View;)V
    .locals 0

    .line 15
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/n;

    invoke-static {p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->-$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/n;)Lcom/pspdfkit/internal/ui/dialog/signatures/n$a;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 17
    check-cast p2, Lcom/pspdfkit/internal/ui/dialog/signatures/a$a;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/a$a;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/ui/dialog/signatures/n$c;I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/n;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/n;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map$Entry;

    .line 2
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/signatures/signers/Signer;

    .line 3
    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 4
    iget-object v1, p1, Lcom/pspdfkit/internal/ui/dialog/signatures/n$c;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/pspdfkit/signatures/signers/Signer;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5
    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v2, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, p2, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;Ljava/lang/String;Lcom/pspdfkit/signatures/signers/Signer;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 12
    new-instance p2, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b$$ExternalSyntheticLambda1;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;)V

    .line 13
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    instance-of v0, p1, Lcom/pspdfkit/internal/ui/dialog/signatures/h;

    if-eqz v0, :cond_0

    .line 14
    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/h;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/h;->setOnDeleteButtonClickedListener(Lcom/pspdfkit/internal/ui/dialog/signatures/h$a;)V

    :cond_0
    return-void
.end method

.method public final getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/n;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/n;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/n;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/n;->-$$Nest$fgetd(Lcom/pspdfkit/internal/ui/dialog/signatures/n;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    return p1
.end method

.method public final bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/n$c;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/n$c;I)V

    return-void
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 5

    if-nez p2, :cond_0

    .line 1
    new-instance p2, Lcom/pspdfkit/internal/ui/dialog/signatures/h;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/h;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {p1, v0, v1}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;-><init>(II)V

    invoke-virtual {p2, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 5
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v0, Lcom/pspdfkit/R$layout;->pspdf__signer_list_item:I

    const/4 v1, 0x0

    .line 6
    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 7
    move-object p1, p2

    check-cast p1, Landroid/widget/TextView;

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/n$b;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/n;

    .line 8
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_gray:I

    invoke-static {v0, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 9
    invoke-static {p1}, Landroidx/core/widget/TextViewCompat;->getCompoundDrawablesRelative(Landroid/widget/TextView;)[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x4

    if-ge v3, v4, :cond_2

    .line 11
    aget-object v4, v2, v3

    if-eqz v4, :cond_1

    .line 12
    invoke-static {v4}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 13
    invoke-static {v4, v0}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 14
    aput-object v4, v2, v3

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 16
    :cond_2
    aget-object v0, v2, v1

    const/4 v1, 0x1

    aget-object v1, v2, v1

    const/4 v3, 0x2

    aget-object v3, v2, v3

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-static {p1, v0, v1, v3, v2}, Landroidx/core/widget/TextViewCompat;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 17
    :goto_1
    new-instance p1, Lcom/pspdfkit/internal/ui/dialog/signatures/n$c;

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/n$c;-><init>(Landroid/view/View;)V

    return-object p1
.end method

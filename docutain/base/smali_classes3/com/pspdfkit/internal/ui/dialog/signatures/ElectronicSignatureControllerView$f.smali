.class public final enum Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

.field public static final enum b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

.field private static final synthetic c:[Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    const-string v1, "HORIZONTAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    const-string v3, "VERTICAL"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    .line 3
    sput-object v3, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->c:[Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->c:[Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    return-object v0
.end method

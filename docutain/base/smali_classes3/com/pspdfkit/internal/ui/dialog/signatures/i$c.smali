.class public Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;
.super Landroid/view/View$BaseSavedState;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/dialog/signatures/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:Z

.field private c:Landroid/net/Uri;


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->a:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->b:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;)Landroid/net/Uri;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->c:Landroid/net/Uri;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputa(Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->a:Ljava/util/ArrayList;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->b:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputc(Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->c:Landroid/net/Uri;

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c$a;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c$a;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 2
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 3
    sget-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->a:Ljava/util/ArrayList;

    .line 4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->b:Z

    .line 5
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/Uri;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->c:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 3
    iget-boolean p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->b:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$c;->c:Landroid/net/Uri;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    return-void
.end method

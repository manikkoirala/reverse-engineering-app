.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/c;
.super Lcom/pspdfkit/internal/ui/dialog/signatures/f;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;
.implements Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/dialog/signatures/c$a;
    }
.end annotation


# instance fields
.field private c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;

.field private d:Lcom/pspdfkit/internal/ui/dialog/signatures/DrawElectronicSignatureCanvasView;

.field private e:Landroid/view/ViewGroup;

.field private f:Landroid/view/ViewGroup;

.field private g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private h:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

.field private i:Z


# direct methods
.method public static synthetic $r8$lambda$6B4t0d2IXqX2XAGrllAQ8zI1vhE(Lcom/pspdfkit/internal/ui/dialog/signatures/c;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->b(Lcom/pspdfkit/internal/ui/dialog/signatures/c;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$vUfTP7oQjFP1-qR39ICA61YIZUc(Lcom/pspdfkit/internal/ui/dialog/signatures/c;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/c;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureOptions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/f;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->a(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V

    return-void
.end method

.method private final a(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V
    .locals 6

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_draw_signature:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 4
    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_width:I

    .line 5
    sget v2, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_height:I

    .line 6
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/res/Resources;II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->i:Z

    .line 11
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 13
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->i:Z

    if-eqz v1, :cond_0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__draw_electronic_signature_dialog_layout:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/pspdfkit/R$layout;->pspdf__draw_electronic_signature_layout:I

    :goto_0
    const/4 v2, 0x1

    .line 14
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 19
    sget v0, Lcom/pspdfkit/R$color;->pspdf__electronic_signature_bg_color:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 20
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_controller_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf_\u2026ure_controller_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->e:Landroid/view/ViewGroup;

    .line 21
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_canvas_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf_\u2026gnature_canvas_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->f:Landroid/view/ViewGroup;

    .line 24
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_canvas_view:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/pspdfkit/internal/ui/dialog/signatures/DrawElectronicSignatureCanvasView;

    .line 25
    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/i$b;)V

    .line 26
    invoke-virtual {p2}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-result-object v3

    invoke-interface {v3, p1}, Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;->option1(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->setInkColor(I)V

    const-string v3, "findViewById<DrawElectro\u2026ption1(context)\n        }"

    .line 27
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/DrawElectronicSignatureCanvasView;

    .line 33
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_controller_view:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;

    .line 34
    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$e;)V

    const-string v3, "findViewById<ElectronicS\u2026ignatureLayout)\n        }"

    .line 35
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;

    const/4 v0, 0x0

    const-string v3, "signatureControllerView"

    if-nez v1, :cond_1

    .line 38
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v0

    .line 39
    :cond_1
    iget-boolean v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->i:Z

    if-eqz v4, :cond_2

    sget-object v4, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    goto :goto_1

    .line 40
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    sget-object v4, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    goto :goto_1

    .line 41
    :cond_3
    sget-object v4, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    .line 42
    :goto_1
    invoke-virtual {v1, v4}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->setOrientation(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;)V

    .line 48
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;

    if-nez v1, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    move-object v0, v1

    :goto_2
    invoke-virtual {p2}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->a(Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;)V

    .line 49
    sget v0, Lcom/pspdfkit/R$id;->pspdf__electronic_signature_save_chip:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    .line 50
    new-instance v3, Lcom/pspdfkit/internal/ui/dialog/signatures/c$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/c$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/c;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v3, "findViewById<SaveSignatu\u2026ip.isSelected }\n        }"

    .line 51
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    .line 54
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_fab_accept_edited_signature:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 55
    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_teal:I

    invoke-static {p1, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 56
    sget v3, Lcom/pspdfkit/R$drawable;->pspdf__ic_done:I

    invoke-virtual {v1, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setImageResource(I)V

    .line 57
    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_black:I

    invoke-static {p1, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v1, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setColorFilter(I)V

    const/4 p1, 0x0

    .line 58
    invoke-virtual {v1, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 59
    invoke-virtual {v1, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    .line 61
    new-instance p1, Lcom/pspdfkit/internal/ui/dialog/signatures/c$$ExternalSyntheticLambda1;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/c$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/c;)V

    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string p1, "findViewById<FloatingAct\u2026}\n            }\n        }"

    .line 62
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 86
    invoke-virtual {p2}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object p1

    sget-object p2, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;->SAVE_IF_SELECTED:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    if-ne p1, p2, :cond_5

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_3
    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->setSaveSignatureChipVisible(Z)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/ui/dialog/signatures/c;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    const/4 v0, 0x0

    const-string v1, "saveSignatureChip"

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v0

    :cond_0
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    if-nez p0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, p0

    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;->setSelected(Z)V

    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/ui/dialog/signatures/c;Landroid/view/View;)V
    .locals 4

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/DrawElectronicSignatureCanvasView;

    const-string v0, "drawElectronicSignatureCanvasView"

    const/4 v1, 0x0

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v1

    :cond_0
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->a(Ljava/lang/String;)Lcom/pspdfkit/signatures/Signature;

    move-result-object p1

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/f;->b:Lcom/pspdfkit/internal/pa;

    if-eqz v2, :cond_3

    if-eqz p1, :cond_3

    .line 6
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/DrawElectronicSignatureCanvasView;

    if-nez v3, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v3, v1

    .line 7
    :cond_1
    invoke-virtual {v3}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->e()Lcom/pspdfkit/ui/signatures/SignatureUiData;

    move-result-object v0

    .line 8
    invoke-interface {v2, p1, v0}, Lcom/pspdfkit/internal/pa;->onSignatureUiDataCollected(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V

    .line 15
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    if-nez p0, :cond_2

    const-string p0, "saveSignatureChip"

    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, p0

    :goto_0
    invoke-virtual {v1}, Landroid/view/View;->isSelected()Z

    move-result p0

    .line 16
    invoke-interface {v2, p1, p0}, Lcom/pspdfkit/internal/pa;->onSignatureCreated(Lcom/pspdfkit/signatures/Signature;Z)V

    :cond_3
    return-void
.end method

.method private final g()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/DrawElectronicSignatureCanvasView;

    if-nez v0, :cond_0

    const-string v0, "drawElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->getCurrentLines()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_5

    .line 56
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    .line 57
    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;

    .line 58
    invoke-virtual {v3}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->a()I

    move-result v3

    const/16 v4, 0xa

    if-lt v3, v4, :cond_3

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v0, 0x0

    :goto_2
    if-ne v0, v2, :cond_5

    const/4 v1, 0x1

    :cond_5
    return v1
.end method

.method private final setSaveSignatureChipVisible(Z)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "saveSignatureChip"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const/4 v2, 0x0

    if-eqz p1, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 5
    iget-boolean v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->i:Z

    if-nez v3, :cond_b

    const/4 v3, 0x2

    if-ne v0, v3, :cond_b

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;

    if-nez v0, :cond_2

    const-string v0, "signatureControllerView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    if-eqz p1, :cond_3

    .line 9
    sget-object v4, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    goto :goto_1

    :cond_3
    sget-object v4, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;

    .line 10
    :goto_1
    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->setOrientation(Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView$f;)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->e:Landroid/view/ViewGroup;

    const-string v4, "signatureControllerContainer"

    if-nez v0, :cond_4

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_4
    if-eqz p1, :cond_5

    .line 15
    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__electronic_signature_controls_view_background:I

    .line 16
    :cond_5
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    const/4 v0, 0x6

    const-string v2, "signatureCanvasContainer"

    const-string v5, "null cannot be cast to non-null type android.widget.RelativeLayout.LayoutParams"

    if-eqz p1, :cond_8

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->f:Landroid/view/ViewGroup;

    if-nez p1, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v1

    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 27
    sget v2, Lcom/pspdfkit/R$id;->pspdf__signature_fab_accept_edited_signature:I

    invoke-virtual {p1, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 28
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->e:Landroid/view/ViewGroup;

    if-nez p1, :cond_7

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    move-object v1, p1

    :goto_2
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 29
    sget v1, Lcom/pspdfkit/R$id;->pspdf__signature_fab_accept_edited_signature:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_4

    .line 31
    :cond_8
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->f:Landroid/view/ViewGroup;

    if-nez p1, :cond_9

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v1

    :cond_9
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 32
    invoke-virtual {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 33
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->e:Landroid/view/ViewGroup;

    if-nez p1, :cond_a

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_3

    :cond_a
    move-object v1, p1

    :goto_3
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 34
    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    :cond_b
    :goto_4
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/DrawElectronicSignatureCanvasView;

    if-nez v0, :cond_0

    const-string v0, "drawElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->setInkColor(I)V

    return-void
.end method

.method public final b()V
    .locals 4

    .line 17
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 v1, 0x0

    const-string v2, "acceptSignatureFab"

    if-nez v0, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleX(F)V

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v0, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v0

    :goto_0
    invoke-virtual {v1, v3}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setScaleY(F)V

    :cond_3
    return-void
.end method

.method public final c()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 v1, 0x0

    const-string v2, "acceptSignatureFab"

    if-nez v0, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/mq;

    .line 4
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v3, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v3

    :goto_0
    const/4 v2, 0x2

    .line 5
    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;I)V

    .line 6
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_2
    return-void
.end method

.method public final d()V
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/mq;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->g:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-nez v1, :cond_0

    const-string v1, "acceptSignatureFab"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_0
    const/4 v2, 0x1

    .line 3
    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;I)V

    .line 4
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final e()V
    .locals 0

    return-void
.end method

.method public final f()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/DrawElectronicSignatureCanvasView;

    if-nez v0, :cond_0

    const-string v0, "drawElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->c()V

    return-void
.end method

.method public getCanvasView()Lcom/pspdfkit/internal/ui/dialog/signatures/i;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/DrawElectronicSignatureCanvasView;

    if-nez v0, :cond_0

    const-string v0, "drawElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/c$a;

    .line 2
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/DrawElectronicSignatureCanvasView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "drawElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/c$a;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->setInkColor(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;

    if-nez v0, :cond_1

    const-string v0, "signatureControllerView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/c$a;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/ElectronicSignatureControllerView;->setCurrentlySelectedColor(I)V

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/c$a;->b()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->setSaveSignatureChipVisible(Z)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    if-nez v0, :cond_2

    const-string v0, "saveSignatureChip"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/c$a;->c()Z

    move-result p1

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;->setSelected(Z)V

    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 1
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/c$a;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/c$a;-><init>(Landroid/os/Parcelable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->d:Lcom/pspdfkit/internal/ui/dialog/signatures/DrawElectronicSignatureCanvasView;

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const-string v0, "drawElectronicSignatureCanvasView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i;->getInkColor()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/c$a;->a(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    const-string v3, "saveSignatureChip"

    if-nez v0, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/c$a;->a(Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/c;->h:Lcom/pspdfkit/internal/ui/dialog/signatures/SaveSignatureChip;

    if-nez v0, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v2, v0

    :goto_1
    invoke-virtual {v2}, Landroid/view/View;->isSelected()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/c$a;->b(Z)V

    return-object v1
.end method

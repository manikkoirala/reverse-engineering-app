.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/l;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;,
        Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->b:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/l;)Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;

    return-object p0
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a:Ljava/util/List;

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->b:Ljava/util/List;

    const/4 v0, 0x1

    .line 14
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->setHasStableIds(Z)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->b:Ljava/util/List;

    return-object v0
.end method

.method public final a(Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l$a;

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;)V"
        }
    .end annotation

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->b:Ljava/util/List;

    .line 4
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a:Ljava/util/List;

    return-object v0
.end method

.method public final b(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;)V"
        }
    .end annotation

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a:Ljava/util/List;

    .line 3
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final c()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/signatures/Signature;

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 4
    invoke-virtual {p0, v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRemoved(I)V

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public final getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItemId(I)J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/signatures/Signature;

    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getId()J

    move-result-wide v0

    return-wide v0
.end method

.method public final onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/signatures/Signature;

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;)Lcom/pspdfkit/internal/ur;

    move-result-object v0

    .line 4
    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/ur;->setSignature(Lcom/pspdfkit/signatures/Signature;)V

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;)Lcom/pspdfkit/internal/ur;

    move-result-object v0

    iget-object p1, p1, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    iget-object p1, p1, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->b:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ur;->setChecked(Z)V

    return-void
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2

    .line 1
    new-instance p2, Lcom/pspdfkit/internal/ur;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/pspdfkit/internal/ur;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/pspdfkit/R$dimen;->pspdf__signature_list_item_height:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 3
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4
    new-instance p1, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/l$b;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/l;Lcom/pspdfkit/internal/ur;)V

    return-object p1
.end method

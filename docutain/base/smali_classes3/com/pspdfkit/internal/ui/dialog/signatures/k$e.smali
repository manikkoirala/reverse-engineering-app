.class final Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/dialog/signatures/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "e"
.end annotation


# instance fields
.field final synthetic b:Lcom/pspdfkit/internal/ui/dialog/signatures/k;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/ui/dialog/signatures/k;Lcom/pspdfkit/internal/ui/dialog/signatures/k$e-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgetd(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/pspdfkit/internal/ui/dialog/utils/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->getBackButton()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgetq(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgets(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$ma(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)V

    goto/16 :goto_0

    .line 7
    :cond_0
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 8
    check-cast v0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->c()V

    .line 9
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onDismiss()V

    goto/16 :goto_0

    .line 12
    :cond_1
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 13
    invoke-interface {p1}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onDismiss()V

    goto :goto_0

    .line 14
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgeto(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    move-result-object v1

    const/4 v2, 0x1

    if-ne p1, v1, :cond_3

    .line 15
    invoke-static {v0, v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$ma(Lcom/pspdfkit/internal/ui/dialog/signatures/k;Z)V

    goto :goto_0

    .line 16
    :cond_3
    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgetp(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    move-result-object v1

    if-ne p1, v1, :cond_4

    .line 17
    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_4

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    invoke-static {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->a()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    check-cast p1, Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->onSignaturesDeleted(Ljava/util/List;)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->c()V

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$e;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    .line 21
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgetp(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    move-result-object v0

    .line 22
    new-instance v1, Lcom/pspdfkit/internal/mq;

    const-wide/16 v3, 0x64

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 23
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgeto(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    move-result-object p1

    .line 24
    new-instance v1, Lcom/pspdfkit/internal/mq;

    const/4 v2, 0x2

    invoke-direct {v1, p1, v2, v3, v4}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 25
    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 26
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_4
    :goto_0
    return-void
.end method

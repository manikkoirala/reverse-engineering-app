.class public final Lcom/pspdfkit/internal/ui/dialog/signatures/d$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a(Lcom/pspdfkit/internal/ui/dialog/utils/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/ui/dialog/signatures/d;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$c;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTabReselected(Lcom/google/android/material/tabs/TabLayout$Tab;)V
    .locals 0

    return-void
.end method

.method public final onTabSelected(Lcom/google/android/material/tabs/TabLayout$Tab;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_2

    .line 1
    invoke-virtual {p1}, Lcom/google/android/material/tabs/TabLayout$Tab;->getPosition()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$c;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v3

    invoke-static {v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/qa;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, "electronicSignatureViewPagerAdapter"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v2, v0

    :cond_0
    invoke-virtual {v2}, Lcom/pspdfkit/internal/qa;->getItemCount()I

    move-result v2

    if-ge v3, v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_2

    move-object v0, p1

    :cond_2
    if-eqz v0, :cond_3

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$c;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 3
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/d;IZ)V

    :cond_3
    return-void
.end method

.method public final onTabUnselected(Lcom/google/android/material/tabs/TabLayout$Tab;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    .line 1
    invoke-virtual {p1}, Lcom/google/android/material/tabs/TabLayout$Tab;->getPosition()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$c;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v3

    invoke-static {v2}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->e(Lcom/pspdfkit/internal/ui/dialog/signatures/d;)Lcom/pspdfkit/internal/qa;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, "electronicSignatureViewPagerAdapter"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v2, v0

    :cond_0
    invoke-virtual {v2}, Lcom/pspdfkit/internal/qa;->getItemCount()I

    move-result v2

    if-ge v3, v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_2

    move-object v0, p1

    :cond_2
    if-eqz v0, :cond_3

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/d$c;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 3
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->a(Lcom/pspdfkit/internal/ui/dialog/signatures/d;IZ)V

    :cond_3
    return-void
.end method

.class final Lcom/pspdfkit/internal/ui/dialog/signatures/k$a;
.super Landroidx/recyclerview/widget/RecyclerView$AdapterDataObserver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/ui/dialog/signatures/k;->b(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroidx/recyclerview/widget/RecyclerView;

.field final synthetic b:Landroid/widget/TextView;

.field final synthetic c:Lcom/pspdfkit/internal/ui/dialog/signatures/k;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ui/dialog/signatures/k;Landroidx/recyclerview/widget/RecyclerView;Landroid/widget/TextView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$a;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$a;->a:Landroidx/recyclerview/widget/RecyclerView;

    iput-object p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$a;->b:Landroid/widget/TextView;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$AdapterDataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$a;->c:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    invoke-static {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/k;)Lcom/pspdfkit/internal/ui/dialog/signatures/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/l;->getItemCount()I

    move-result v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$a;->a:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$a;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$a;->a:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$a;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public final onItemRangeRemoved(II)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k$a;->onChanged()V

    return-void
.end method

.class final Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/dialog/signatures/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "f"
.end annotation


# static fields
.field private static final h:[I

.field private static final i:I

.field private static final j:I


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->a:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->b:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->c:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->d:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->e:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->f:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetg(Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->g:I

    return p0
.end method

.method static bridge synthetic -$$Nest$sfgeti()I
    .locals 1

    sget v0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->i:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetj()I
    .locals 1

    sget v0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->j:I

    return v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout:[I

    sput-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->h:[I

    .line 2
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__signatureLayoutStyle:I

    sput v0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->i:I

    .line 3
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_SignatureLayout:I

    sput v0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->j:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->h:[I

    sget v2, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->i:I

    sget v3, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->j:I

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 5
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__backgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 7
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 8
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->a:I

    .line 12
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__addSignatureIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_add:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->b:I

    .line 14
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__addSignatureIconColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 16
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 17
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->c:I

    .line 20
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__addSignatureIconBackgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color:I

    .line 22
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 23
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->d:I

    .line 27
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__deleteSelectedSignaturesIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_delete:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->e:I

    .line 30
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__deleteSelectedSignaturesIconColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 32
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 33
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->f:I

    .line 36
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__SignatureLayout_pspdf__deleteSelectedSignaturesIconBackgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_red_light:I

    .line 38
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 39
    invoke-virtual {v0, v1, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/k$f;->g:I

    .line 43
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

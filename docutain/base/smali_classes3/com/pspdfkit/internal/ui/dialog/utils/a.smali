.class public Lcom/pspdfkit/internal/ui/dialog/utils/a;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/dialog/utils/a$a;,
        Lcom/pspdfkit/internal/ui/dialog/utils/a$b;
    }
.end annotation


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Landroid/widget/TextView;

.field private g:Ljava/lang/String;

.field private h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

.field private i:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

.field private final j:Landroid/graphics/Rect;

.field private k:F

.field private l:Ljava/lang/Runnable;

.field private m:Lcom/pspdfkit/internal/ui/dialog/utils/a$b;


# direct methods
.method public static synthetic $r8$lambda$fsfSyZoiWFYcyWE6X1KUIu0r5Go(Lcom/pspdfkit/internal/ui/dialog/utils/a;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->a(ZZ)V

    return-void
.end method

.method public static synthetic $r8$lambda$heaqbErUzKp5AmZcUWf7wBftJcc(Lcom/pspdfkit/internal/ui/dialog/utils/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->c()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/utils/a;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/ui/dialog/utils/a;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/internal/ui/dialog/utils/a;)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeti(Lcom/pspdfkit/internal/ui/dialog/utils/a;)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->i:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetj(Lcom/pspdfkit/internal/ui/dialog/utils/a;)Landroid/graphics/Rect;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->j:Landroid/graphics/Rect;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetl(Lcom/pspdfkit/internal/ui/dialog/utils/a;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->l:Ljava/lang/Runnable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputl(Lcom/pspdfkit/internal/ui/dialog/utils/a;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->l:Ljava/lang/Runnable;

    return-void
.end method

.method static bridge synthetic -$$Nest$md(Lcom/pspdfkit/internal/ui/dialog/utils/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->d()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/ui/dialog/utils/b;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->j:Landroid/graphics/Rect;

    .line 19
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->a(Lcom/pspdfkit/internal/ui/dialog/utils/a$a;)V

    return-void
.end method

.method private a(Lcom/pspdfkit/internal/ui/dialog/utils/a$a;)V
    .locals 18

    move-object/from16 v0, p0

    .line 3
    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a$a;->getTitleColor()I

    move-result v1

    iput v1, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->c:I

    .line 4
    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a$a;->getTitleHeight()I

    move-result v1

    iput v1, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b:I

    .line 5
    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a$a;->getCornerRadius()I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->k:F

    .line 7
    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a$a;->getTitlePadding()I

    move-result v1

    .line 9
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a$a;->getBackButtonIcon()I

    move-result v3

    invoke-static {v2, v3}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    if-eqz v6, :cond_0

    const/4 v2, 0x1

    .line 10
    invoke-virtual {v6, v2}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 12
    :cond_0
    new-instance v2, Lcom/pspdfkit/internal/ui/dialog/utils/a$b;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/pspdfkit/internal/ui/dialog/utils/a$b;-><init>(Lcom/pspdfkit/internal/ui/dialog/utils/a;Landroid/content/Context;)V

    iput-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->m:Lcom/pspdfkit/internal/ui/dialog/utils/a$b;

    .line 13
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 18
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/pspdfkit/R$id;->pspdf__toolbar_back_button:I

    .line 22
    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a$a;->getTitleIconsColor()I

    move-result v8

    .line 23
    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a$a;->getTitleIconsColor()I

    move-result v9

    sget-object v16, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;->START:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;

    const/4 v11, 0x0

    const-string v7, ""

    move-object/from16 v10, v16

    .line 24
    invoke-static/range {v4 .. v11}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    .line 33
    iget v3, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setMinimumHeight(I)V

    .line 34
    iget-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 35
    iget-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 36
    iget-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->m:Lcom/pspdfkit/internal/ui/dialog/utils/a$b;

    iget-object v3, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 39
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    const/4 v3, 0x0

    .line 40
    invoke-virtual {v2, v1, v3, v1, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 41
    iget-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a$a;->getTitleTextSize()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 42
    iget-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a$a;->getTitleTextColor()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 43
    iget-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    sget v4, Lcom/pspdfkit/R$id;->pspdf__share_dialog_title:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setId(I)V

    .line 44
    iget-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    const/16 v4, 0x10

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 45
    iget-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    const/4 v4, 0x5

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextAlignment(I)V

    .line 46
    iget-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->m:Lcom/pspdfkit/internal/ui/dialog/utils/a$b;

    iget-object v4, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 51
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v10

    sget v11, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_close:I

    .line 53
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a$a;->getCloseButtonIcon()I

    move-result v4

    invoke-static {v2, v4}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    .line 55
    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a$a;->getTitleIconsColor()I

    move-result v14

    .line 56
    invoke-interface/range {p1 .. p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a$a;->getTitleIconsColor()I

    move-result v15

    const/16 v17, 0x0

    const-string v13, ""

    .line 57
    invoke-static/range {v10 .. v17}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->createSingleItem(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;IILcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem$Position;Z)Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->i:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    .line 66
    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 67
    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setCloseButtonVisible(Z)V

    .line 68
    iget-object v1, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->m:Lcom/pspdfkit/internal/ui/dialog/utils/a$b;

    iget-object v2, v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->i:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageButton;->setTranslationX(F)V

    .line 71
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 72
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    neg-int p1, p1

    :goto_0
    int-to-float p1, p1

    invoke-virtual {v0, p1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationX(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 73
    invoke-virtual {p1, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    const-wide/16 v0, 0xc8

    .line 74
    invoke-virtual {p1, v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/utils/a$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ui/dialog/utils/a$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/dialog/utils/a;)V

    .line 75
    invoke-virtual {p1, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method

.method private synthetic a(ZZ)V
    .locals 0

    .line 69
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b(ZZ)V

    return-void
.end method

.method private synthetic c()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private d()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->d:I

    if-ne v0, v1, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->d:I

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    if-le v0, v1, :cond_2

    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->k:F

    const/4 v1, 0x0

    cmpl-float v2, v0, v1

    if-nez v2, :cond_1

    goto :goto_0

    .line 11
    :cond_1
    iget v2, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->c:I

    const/16 v3, 0x8

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v0, v3, v4

    const/4 v4, 0x1

    aput v0, v3, v4

    const/4 v4, 0x2

    aput v0, v3, v4

    const/4 v4, 0x3

    aput v0, v3, v4

    const/4 v0, 0x4

    aput v1, v3, v0

    const/4 v0, 0x5

    aput v1, v3, v0

    const/4 v0, 0x6

    aput v1, v3, v0

    const/4 v0, 0x7

    aput v1, v3, v0

    invoke-static {p0, v2, v3}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;I[F)V

    goto :goto_1

    .line 12
    :cond_2
    :goto_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->c:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 13
    invoke-static {p0, v0}, Landroidx/core/view/ViewCompat;->setBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->m:Lcom/pspdfkit/internal/ui/dialog/utils/a$b;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public final b()V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final b(ZZ)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/utils/a$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/ui/dialog/utils/a$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ui/dialog/utils/a;ZZ)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->l:Ljava/lang/Runnable;

    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v0}, Landroidx/appcompat/widget/AppCompatImageButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 9
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/ov;->c(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-nez p2, :cond_4

    .line 11
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {p2, v2}, Landroidx/appcompat/widget/AppCompatImageButton;->setTranslationX(F)V

    .line 12
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 13
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    if-eqz v0, :cond_2

    neg-int p1, p1

    :cond_2
    int-to-float v2, p1

    :cond_3
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setTranslationX(F)V

    return-void

    .line 17
    :cond_4
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result p2

    if-nez p2, :cond_5

    const/4 p2, 0x1

    goto :goto_1

    :cond_5
    const/4 p2, 0x0

    :goto_1
    if-ne p2, p1, :cond_6

    return-void

    :cond_6
    const-wide/16 v3, 0xc8

    if-eqz p1, :cond_9

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p2

    if-eqz v0, :cond_7

    goto :goto_2

    :cond_7
    neg-int p2, p2

    :goto_2
    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroidx/appcompat/widget/AppCompatImageButton;->setTranslationX(F)V

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    .line 21
    invoke-virtual {p1, v2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationX(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 22
    invoke-virtual {p1, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    .line 23
    invoke-virtual {p1, v3, v4}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    .line 24
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setTranslationX(F)V

    .line 25
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    .line 26
    invoke-virtual {p1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    if-eqz v0, :cond_8

    .line 27
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result p2

    neg-int p2, p2

    goto :goto_3

    :cond_8
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result p2

    :goto_3
    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 28
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 29
    invoke-virtual {p1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_4

    .line 30
    :cond_9
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->a(Z)V

    .line 31
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result p2

    if-eqz v0, :cond_a

    neg-int p2, p2

    :cond_a
    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTranslationX(F)V

    .line 32
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    .line 33
    invoke-virtual {p1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 34
    invoke-virtual {p1, v2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 35
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 36
    invoke-virtual {p1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    :goto_4
    return-void
.end method

.method public final e()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->m:Lcom/pspdfkit/internal/ui/dialog/utils/a$b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public getBackButton()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    return-object v0
.end method

.method public getCloseButton()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->i:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    return-object v0
.end method

.method public getTitleHeight()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b:I

    iget v1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->e:I

    add-int/2addr v0, v1

    return v0
.end method

.method protected final onLayout(ZIIII)V
    .locals 2

    const/4 p1, 0x0

    .line 1
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p3

    if-ge p1, p3, :cond_1

    .line 2
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p3

    .line 3
    invoke-virtual {p3}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 4
    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->e:I

    add-int v1, v0, p5

    invoke-virtual {p3, p2, v0, p4, v1}, Landroid/view/View;->layout(IIII)V

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->m:Lcom/pspdfkit/internal/ui/dialog/utils/a$b;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getSuggestedMinimumWidth()I

    move-result p2

    invoke-static {p2, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result p1

    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->m:Lcom/pspdfkit/internal/ui/dialog/utils/a$b;

    .line 4
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    iget v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->e:I

    add-int/2addr p2, v0

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 5
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public setBackButtonColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->i:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setIconColor(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->i:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setIconColorActivated(I)V

    return-void
.end method

.method public setBackButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->h:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setCloseButtonColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->i:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setIconColor(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->i:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;->setIconColorActivated(I)V

    return-void
.end method

.method public setCloseButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->i:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setCloseButtonVisible(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->i:Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setDetailTitle(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->g:Ljava/lang/String;

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(Ljava/lang/String;)V

    return-void
.end method

.method public setRoundedCornersRadius(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->k:F

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->d()V

    return-void
.end method

.method public setTitle(I)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    invoke-static {v1, p1, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitleColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->c:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->d()V

    return-void
.end method

.method public setTitleTextColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setTopInset(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ui/dialog/utils/a;->e:I

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

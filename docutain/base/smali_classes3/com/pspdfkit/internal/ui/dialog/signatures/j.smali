.class public Lcom/pspdfkit/internal/ui/dialog/signatures/j;
.super Landroidx/appcompat/app/AppCompatDialogFragment;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;


# instance fields
.field private b:Ljava/lang/Integer;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/pspdfkit/internal/or;

.field private e:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

.field private f:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

.field private g:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

.field private h:Ljava/lang/String;

.field private i:Lcom/pspdfkit/internal/ui/dialog/signatures/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;-><init>()V

    .line 84
    sget-object v0, Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;->AUTOMATIC:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->e:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    .line 87
    sget-object v0, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;->SAVE_IF_SELECTED:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->f:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 90
    sget-object v0, Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;->IF_AVAILABLE:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->g:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    const/4 v0, 0x0

    .line 94
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->h:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/internal/or;Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Ljava/lang/String;)Lcom/pspdfkit/internal/ui/dialog/signatures/j;
    .locals 3

    const-string v0, "fragmentManager"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "listener"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "orientation"

    .line 108
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "savingStrategy"

    .line 161
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    invoke-static {p3, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "certificateSelectionMode"

    .line 214
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 265
    invoke-static {p4, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "com.pspdfkit.ui.dialog.signatures.SignaturePickerDialog.FRAGMENT_TAG"

    .line 266
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    if-eqz p0, :cond_0

    .line 267
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->d:Lcom/pspdfkit/internal/or;

    .line 268
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->e:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    .line 269
    iput-object p3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->f:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 270
    iput-object p4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->g:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    .line 271
    iput-object p5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->h:Ljava/lang/String;

    :cond_0
    return-object p0
.end method

.method public static b(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/internal/or;Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Ljava/lang/String;)Lcom/pspdfkit/internal/ui/dialog/signatures/j;
    .locals 3

    const-string v0, "fragmentManager"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "listener"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "orientation"

    .line 108
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "savingStrategy"

    .line 161
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    invoke-static {p3, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "certificateSelectionMode"

    .line 214
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 265
    invoke-static {p4, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "com.pspdfkit.ui.dialog.signatures.SignaturePickerDialog.FRAGMENT_TAG"

    .line 266
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    if-nez v1, :cond_0

    .line 267
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/signatures/j;

    invoke-direct {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/j;-><init>()V

    .line 268
    :cond_0
    iput-object p1, v1, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->d:Lcom/pspdfkit/internal/or;

    .line 269
    iput-object p2, v1, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->e:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    .line 270
    iput-object p3, v1, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->f:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 271
    iput-object p4, v1, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->g:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    .line 272
    iput-object p5, v1, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->h:Ljava/lang/String;

    .line 274
    invoke-virtual {v1}, Landroidx/appcompat/app/AppCompatDialogFragment;->isAdded()Z

    move-result p1

    if-nez p1, :cond_1

    .line 275
    invoke-virtual {v1, p0, v0}, Landroidx/appcompat/app/AppCompatDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 276
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 278
    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->b:Ljava/lang/Integer;

    const/4 v1, 0x6

    .line 279
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;)V"
        }
    .end annotation

    .line 272
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    if-eqz v0, :cond_0

    .line 273
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->setItems(Ljava/util/List;)V

    goto :goto_0

    .line 275
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->c:Ljava/util/List;

    :goto_0
    return-void
.end method

.method public final b()V
    .locals 2

    .line 276
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 278
    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->b:Ljava/lang/Integer;

    const/4 v1, 0x1

    .line 279
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->b:Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    if-eqz p1, :cond_0

    const-string v0, "STATE_SIGNATURES"

    .line 1
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->c:Ljava/util/List;

    const-string v0, "STATE_ORIGINAL_ORIENTATION"

    .line 2
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->b:Ljava/lang/Integer;

    .line 4
    :cond_0
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_Dialog_Light_Panel_Dim:I

    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Landroidx/appcompat/app/AppCompatDialogFragment;->setStyle(II)V

    .line 5
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object p1

    const/4 v0, 0x1

    .line 6
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    return-object p1
.end method

.method public final onDismiss()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->d:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onDismiss()V

    .line 4
    :cond_0
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->dismiss()V

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .line 5
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->d:Lcom/pspdfkit/internal/or;

    if-eqz p1, :cond_0

    .line 7
    invoke-interface {p1}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onDismiss()V

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->c:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    const-string v1, "STATE_SIGNATURES"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v1, "STATE_ORIGINAL_ORIENTATION"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public final onSignatureCreated(Lcom/pspdfkit/signatures/Signature;Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->d:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onSignatureCreated(Lcom/pspdfkit/signatures/Signature;Z)V

    .line 4
    :cond_0
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->dismiss()V

    return-void
.end method

.method public final onSignaturePicked(Lcom/pspdfkit/signatures/Signature;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->d:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onSignaturePicked(Lcom/pspdfkit/signatures/Signature;)V

    .line 4
    :cond_0
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->dismiss()V

    return-void
.end method

.method public final onSignatureUiDataCollected(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->d:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onSignatureUiDataCollected(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V

    :cond_0
    return-void
.end method

.method public final onSignaturesDeleted(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->d:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/or;->onSignaturesDeleted(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 6

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 3
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 4
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 6
    :cond_0
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$dimen;->pspdf__signature_dialog_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 7
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__signature_dialog_height:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 12
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/pspdfkit/R$dimen;->pspdf__signature_dialog_width:I

    sget v5, Lcom/pspdfkit/R$dimen;->pspdf__signature_dialog_height:I

    .line 13
    invoke-static {v3, v4, v5}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/res/Resources;II)Z

    move-result v3

    .line 16
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/4 v5, -0x1

    if-nez v3, :cond_1

    const/4 v1, -0x1

    :cond_1
    if-nez v3, :cond_2

    const/4 v2, -0x1

    .line 17
    :cond_2
    invoke-virtual {v4, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 20
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    .line 23
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    if-eqz v0, :cond_3

    xor-int/lit8 v1, v3, 0x1

    .line 26
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->setFullscreen(Z)V

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;)V

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->c:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 30
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->setItems(Ljava/util/List;)V

    const/4 v0, 0x0

    .line 31
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->c:Ljava/util/List;

    :cond_3
    :goto_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStop()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->c()V

    :cond_0
    return-void
.end method

.method public final setupDialog(Landroid/app/Dialog;I)V
    .locals 6

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/appcompat/app/AppCompatDialogFragment;->setupDialog(Landroid/app/Dialog;I)V

    .line 3
    new-instance p2, Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    .line 4
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->e:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    iget-object v3, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->f:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    iget-object v4, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->g:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    iget-object v5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->h:Ljava/lang/String;

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    .line 9
    invoke-virtual {p2, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/k;->setListener(Lcom/pspdfkit/internal/ui/dialog/signatures/k$c;)V

    .line 10
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_layout:I

    invoke-virtual {p2, v0}, Landroid/view/View;->setId(I)V

    .line 11
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/j;->i:Lcom/pspdfkit/internal/ui/dialog/signatures/k;

    invoke-virtual {p1, p2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    return-void
.end method

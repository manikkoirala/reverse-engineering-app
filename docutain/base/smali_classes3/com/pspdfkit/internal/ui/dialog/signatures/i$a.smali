.class final Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/dialog/signatures/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/graphics/Path;

.field private final b:Ljava/util/ArrayList;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/ArrayList;

.field private e:I

.field private final f:Ljava/util/ArrayList;

.field private g:F


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Landroid/graphics/Path;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->a:Landroid/graphics/Path;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->b:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->c:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->d:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->e:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->f:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetg(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;)F
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->g:F

    return p0
.end method

.method static bridge synthetic -$$Nest$ma(Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;Landroid/graphics/PointF;JFIF)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->a(Landroid/graphics/PointF;JFIF)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a$a;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a$a;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/graphics/PointF;JFIF)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->a:Landroid/graphics/Path;

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->b:Ljava/util/ArrayList;

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->c:Ljava/util/ArrayList;

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->d:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 9
    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->e:I

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->f:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 13
    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->g:F

    .line 21
    invoke-direct/range {p0 .. p6}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->b(Landroid/graphics/PointF;JFIF)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/PointF;JFIFLcom/pspdfkit/internal/ui/dialog/signatures/i$a-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;-><init>(Landroid/graphics/PointF;JFIF)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->a:Landroid/graphics/Path;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->b:Ljava/util/ArrayList;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->c:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->d:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 60
    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->e:I

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->f:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 64
    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->g:F

    .line 86
    sget-object v0, Landroid/graphics/PointF;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v2

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 89
    new-array v0, v0, [J

    .line 90
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readLongArray([J)V

    .line 91
    invoke-static {v0}, Lcom/pspdfkit/internal/fv;->a([J)[Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 93
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 94
    new-array v0, v0, [F

    .line 95
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readFloatArray([F)V

    .line 96
    invoke-static {v0}, Lcom/pspdfkit/internal/fv;->a([F)[Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v6

    move-object v1, p0

    .line 101
    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->a(Ljava/util/ArrayList;Ljava/util/List;Ljava/util/List;IF)V

    return-void
.end method

.method private constructor <init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IF)V
    .locals 2

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->a:Landroid/graphics/Path;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->b:Ljava/util/ArrayList;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->c:Ljava/util/ArrayList;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->d:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 30
    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->e:I

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->f:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 34
    iput v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->g:F

    .line 51
    invoke-direct/range {p0 .. p5}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->a(Ljava/util/ArrayList;Ljava/util/List;Ljava/util/List;IF)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IFLcom/pspdfkit/internal/ui/dialog/signatures/i$a-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IF)V

    return-void
.end method

.method private a(Landroid/graphics/PointF;JFIF)V
    .locals 6

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->a:Landroid/graphics/Path;

    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v3, p1, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iget v5, p1, Landroid/graphics/PointF;->y:F

    add-float/2addr v5, v0

    div-float/2addr v5, v4

    invoke-virtual {v1, v2, v0, v3, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->c:Ljava/util/ArrayList;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->d:Ljava/util/ArrayList;

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 12
    iput p5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->e:I

    .line 13
    iput p6, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->g:F

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->f:Ljava/util/ArrayList;

    invoke-static {p6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    .line 16
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Starting point is not set."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/List;Ljava/util/List;IF)V
    .locals 9

    const/4 v0, 0x0

    .line 1
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    if-nez v0, :cond_0

    .line 3
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Landroid/graphics/PointF;

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v6

    move-object v2, p0

    move v7, p4

    move v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->b(Landroid/graphics/PointF;JFIF)V

    goto :goto_1

    .line 5
    :cond_0
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Landroid/graphics/PointF;

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v6

    move-object v2, p0

    move v7, p4

    move v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->a(Landroid/graphics/PointF;JFIF)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private b(Landroid/graphics/PointF;JFIF)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->c:Ljava/util/ArrayList;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->d:Ljava/util/ArrayList;

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5
    iput p5, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->e:I

    .line 6
    iput p6, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->g:F

    .line 7
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->f:Ljava/util/ArrayList;

    invoke-static {p6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 8
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->a:Landroid/graphics/Path;

    iget p3, p1, Landroid/graphics/PointF;->x:F

    iget p1, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p2, p3, p1}, Landroid/graphics/Path;->moveTo(FF)V

    return-void

    .line 10
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Starting point is already set."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method final a()I
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->b:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->c:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->c:Ljava/util/ArrayList;

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Long;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Ljava/lang/Long;

    const/4 v1, 0x0

    if-nez p2, :cond_0

    move-object v2, v1

    goto :goto_1

    .line 4
    :cond_0
    array-length v2, p2

    new-array v2, v2, [J

    const/4 v3, 0x0

    .line 5
    :goto_0
    array-length v4, p2

    if-ge v3, v4, :cond_1

    .line 6
    aget-object v4, p2, v3

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 7
    :cond_1
    :goto_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeLongArray([J)V

    .line 8
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->d:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 9
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->d:Ljava/util/ArrayList;

    new-array v2, v0, [Ljava/lang/Float;

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Ljava/lang/Float;

    if-nez p2, :cond_2

    goto :goto_3

    .line 10
    :cond_2
    array-length v1, p2

    new-array v1, v1, [F

    .line 11
    :goto_2
    array-length v2, p2

    if-ge v0, v2, :cond_3

    .line 12
    aget-object v2, p2, v0

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 13
    :cond_3
    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeFloatArray([F)V

    .line 14
    iget p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->e:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 15
    iget p2, p0, Lcom/pspdfkit/internal/ui/dialog/signatures/i$a;->g:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    return-void
.end method

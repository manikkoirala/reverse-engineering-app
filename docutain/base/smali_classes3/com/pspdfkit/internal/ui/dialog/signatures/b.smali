.class final Lcom/pspdfkit/internal/ui/dialog/signatures/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:I = 0x0

.field private static b:I = 0x0

.field private static c:I = 0x0

.field static d:Z = false


# direct methods
.method public static a(I)Landroid/graphics/drawable/StateListDrawable;
    .locals 11

    .line 7
    sget-boolean v0, Lcom/pspdfkit/internal/ui/dialog/signatures/b;->d:Z

    if-eqz v0, :cond_0

    .line 10
    sget v0, Lcom/pspdfkit/internal/ui/dialog/signatures/b;->a:I

    sget v1, Lcom/pspdfkit/internal/ui/dialog/signatures/b;->b:I

    .line 11
    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    const/4 v3, 0x1

    .line 12
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    const/4 v4, 0x0

    .line 13
    invoke-virtual {v2, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 14
    invoke-virtual {v2, v1, v0}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 15
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 16
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 17
    invoke-virtual {v0, p0}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 18
    invoke-virtual {v0, v4, v4}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    const/4 p0, 0x2

    new-array v1, p0, [Landroid/graphics/drawable/Drawable;

    aput-object v2, v1, v4

    aput-object v0, v1, v3

    .line 20
    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 21
    sget v10, Lcom/pspdfkit/internal/ui/dialog/signatures/b;->c:I

    const/4 v6, 0x1

    move-object v5, v2

    move v7, v10

    move v8, v10

    move v9, v10

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    new-array p0, p0, [Landroid/graphics/drawable/Drawable;

    .line 22
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    aput-object v1, p0, v4

    aput-object v0, p0, v3

    .line 23
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, p0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 24
    sget v10, Lcom/pspdfkit/internal/ui/dialog/signatures/b;->c:I

    move-object v5, v0

    move v7, v10

    move v8, v10

    move v9, v10

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    .line 25
    new-instance p0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {p0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    new-array v1, v3, [I

    const v3, 0x10100a1

    aput v3, v1, v4

    .line 26
    invoke-virtual {p0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-array v1, v4, [I

    .line 27
    invoke-virtual {p0, v1, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    return-object p0

    .line 28
    :cond_0
    new-instance p0, Ljava/lang/AssertionError;

    const-string v0, "ColorButtonDrawableCreator constants have not been initialized"

    invoke-direct {p0, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$color;->pspdf__color_electronic_signature_selected_border:I

    invoke-static {p0, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    sput v0, Lcom/pspdfkit/internal/ui/dialog/signatures/b;->a:I

    .line 2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_canvas_controller_picker_circles_border_width:I

    .line 3
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/pspdfkit/internal/ui/dialog/signatures/b;->b:I

    .line 4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v0, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_canvas_controller_picker_circles_inner_inset:I

    .line 5
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p0

    float-to-int p0, p0

    sput p0, Lcom/pspdfkit/internal/ui/dialog/signatures/b;->c:I

    const/4 p0, 0x1

    .line 6
    sput-boolean p0, Lcom/pspdfkit/internal/ui/dialog/signatures/b;->d:Z

    return-void
.end method

.class public final Lcom/pspdfkit/internal/ui/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/DocumentCoordinator;


# instance fields
.field private final a:Lcom/pspdfkit/internal/ui/f;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/ui/DocumentDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/pspdfkit/ui/DocumentDescriptor;

.field private final d:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentVisibleListener;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentCoordinatorEmptyListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/ui/f;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    .line 8
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/e;->d:Lcom/pspdfkit/internal/nh;

    .line 11
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/e;->e:Lcom/pspdfkit/internal/nh;

    .line 14
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/e;->f:Lcom/pspdfkit/internal/nh;

    .line 19
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/e;->a:Lcom/pspdfkit/internal/ui/f;

    return-void
.end method

.method private a(IZ)V
    .locals 3

    if-ltz p1, :cond_2

    if-eqz p2, :cond_0

    .line 87
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le p1, v0, :cond_1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    goto :goto_0

    :cond_1
    return-void

    .line 88
    :cond_2
    :goto_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Target index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " is out of bounds: [0;"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    .line 92
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_3

    const-string p1, "]"

    goto :goto_1

    :cond_3
    const-string p1, ")"

    .line 93
    :goto_1
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Lcom/pspdfkit/ui/DocumentDescriptor;)Z
    .locals 5

    .line 8
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "setVisibleDocument() may only be called from the UI thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    const/4 v1, 0x0

    if-eq v0, p1, :cond_b

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    .line 11
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_6

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 18
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/e;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v3, v2, v1}, Lcom/pspdfkit/internal/ui/f;->getActivityState(ZZ)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/pspdfkit/ui/DocumentDescriptor;->setState(Landroid/os/Bundle;)V

    .line 21
    :cond_1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    const/4 v0, 0x0

    if-nez p1, :cond_2

    goto :goto_1

    .line 22
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/ui/DocumentDescriptor;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 23
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/e;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/f;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 25
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 27
    invoke-virtual {p1}, Landroid/view/View;->buildDrawingCache()V

    .line 28
    invoke-virtual {p1}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 30
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v3, v4, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0

    :cond_3
    move-object v3, v0

    .line 32
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->destroyDrawingCache()V

    goto :goto_2

    :cond_4
    :goto_1
    move-object v3, v0

    .line 33
    :goto_2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    .line 34
    iget-object v4, p0, Lcom/pspdfkit/internal/ui/e;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/ui/f;->getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v4

    if-nez p1, :cond_5

    goto :goto_3

    .line 38
    :cond_5
    invoke-virtual {p1}, Lcom/pspdfkit/ui/DocumentDescriptor;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 39
    invoke-virtual {p1}, Lcom/pspdfkit/ui/DocumentDescriptor;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p1

    invoke-virtual {v4}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->newInstance(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    goto :goto_3

    .line 40
    :cond_6
    invoke-virtual {p1}, Lcom/pspdfkit/ui/DocumentDescriptor;->isImageDocument()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 42
    invoke-virtual {p1}, Lcom/pspdfkit/ui/DocumentDescriptor;->getDocumentSources()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {v4}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    .line 43
    invoke-static {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->newImageInstance(Lcom/pspdfkit/document/DocumentSource;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    goto :goto_3

    .line 47
    :cond_7
    invoke-virtual {p1}, Lcom/pspdfkit/ui/DocumentDescriptor;->getDocumentSources()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v4}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    .line 48
    invoke-static {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->newInstanceFromDocumentSources(Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    .line 51
    :goto_3
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/e;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/f;->setFragment(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 52
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/pspdfkit/ui/DocumentDescriptor;->getState()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_8

    .line 53
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/e;->a:Lcom/pspdfkit/internal/ui/f;

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/DocumentDescriptor;->getState()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/f;->setActivityState(Landroid/os/Bundle;)V

    if-eqz v3, :cond_8

    .line 58
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/e;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/f;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    if-eqz p1, :cond_8

    .line 60
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/e;->a:Lcom/pspdfkit/internal/ui/f;

    .line 61
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/f;->getHostingActivity()Landroidx/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 62
    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->setPageLoadingDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 69
    :cond_8
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    if-eqz p1, :cond_9

    .line 70
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentVisibleListener;

    .line 71
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentVisibleListener;->onDocumentVisible(Lcom/pspdfkit/ui/DocumentDescriptor;)V

    goto :goto_4

    .line 72
    :cond_9
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/e;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentCoordinatorEmptyListener;

    .line 73
    invoke-interface {v0}, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentCoordinatorEmptyListener;->onDocumentCoordinatorEmpty()V

    goto :goto_5

    :cond_a
    return v2

    :cond_b
    :goto_6
    return v1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "PdfActivityDocumentCoordinator.Documents"

    .line 74
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/e;->a(Ljava/util/ArrayList;)V

    :cond_0
    const/4 v0, -0x1

    const-string v1, "PdfActivityDocumentCoordinator.VisibleDocumentIndex"

    .line 79
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    if-ltz p1, :cond_1

    .line 80
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/DocumentDescriptor;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    .line 82
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentVisibleListener;

    .line 83
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentVisibleListener;->onDocumentVisible(Lcom/pspdfkit/ui/DocumentDescriptor;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method final a(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/DocumentDescriptor;

    .line 2
    invoke-virtual {v1}, Lcom/pspdfkit/ui/DocumentDescriptor;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/e;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;

    .line 4
    invoke-interface {v0, v1}, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;->onDocumentUpdated(Lcom/pspdfkit/ui/DocumentDescriptor;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 85
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/DocumentDescriptor;

    .line 86
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ui/e;->addDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method final a(Z)V
    .locals 3

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/DocumentDescriptor;

    if-eqz p1, :cond_0

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 7
    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/DocumentDescriptor;->setDocument(Lcom/pspdfkit/document/PdfDocument;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final addDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z
    .locals 2

    const-string v0, "documentDescriptor"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "addDocument() may only be called from the UI thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;

    .line 58
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;->onDocumentAdded(Lcom/pspdfkit/ui/DocumentDescriptor;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public final addDocument(Lcom/pspdfkit/ui/DocumentDescriptor;I)Z
    .locals 2

    const-string v0, "documentDescriptor"

    const-string v1, "argumentName"

    .line 59
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 110
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 111
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "addDocument() may only be called from the UI thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 114
    invoke-direct {p0, p2, v0}, Lcom/pspdfkit/internal/ui/e;->a(IZ)V

    .line 115
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 116
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/e;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;

    .line 117
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;->onDocumentAdded(Lcom/pspdfkit/ui/DocumentDescriptor;)V

    goto :goto_0

    :cond_0
    return v0

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public final addDocumentAfterVisibleDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z
    .locals 2

    const-string v0, "documentDescriptor"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    .line 55
    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "addDocumentAfterVisibleDocument() may only be called from the UI thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    if-eqz v0, :cond_0

    .line 57
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x1

    add-int/2addr v0, v1

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/ui/e;->addDocument(Lcom/pspdfkit/ui/DocumentDescriptor;I)Z

    return v1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final addOnDocumentCoordinatorEmptyListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentCoordinatorEmptyListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final addOnDocumentVisibleListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentVisibleListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final addOnDocumentsChangedListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/DocumentDescriptor;->setState(Landroid/os/Bundle;)V

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    const-string v1, "PdfActivityDocumentCoordinator.Documents"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    :goto_0
    const-string v1, "PdfActivityDocumentCoordinator.VisibleDocumentIndex"

    .line 12
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/DocumentDescriptor;->setDocument(Lcom/pspdfkit/document/PdfDocument;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;

    .line 5
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;->onDocumentUpdated(Lcom/pspdfkit/ui/DocumentDescriptor;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final getDocuments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/DocumentDescriptor;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getVisibleDocument()Lcom/pspdfkit/ui/DocumentDescriptor;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    return-object v0
.end method

.method public final moveDocument(Lcom/pspdfkit/ui/DocumentDescriptor;I)Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "moveDocument() may only be called from the UI thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    const-string v0, "documentToMove"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, p2, v0}, Lcom/pspdfkit/internal/ui/e;->a(IZ)V

    .line 57
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_1

    if-eq v1, p2, :cond_1

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 60
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 61
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;

    .line 62
    invoke-interface {v1, p1, p2}, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;->onDocumentMoved(Lcom/pspdfkit/ui/DocumentDescriptor;I)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    return p1

    :cond_1
    return v0
.end method

.method public final removeAllDocuments()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    :goto_0
    if-ltz v1, :cond_0

    .line 3
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/ui/DocumentDescriptor;

    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/ui/e;->removeDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    move-result v3

    or-int/2addr v2, v3

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    return v2
.end method

.method public final removeAllDocumentsExceptVisible()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x0

    :goto_0
    if-ltz v2, :cond_1

    .line 4
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/ui/DocumentDescriptor;

    if-eq v4, v1, :cond_0

    .line 6
    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/ui/e;->removeDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    move-result v4

    or-int/2addr v3, v4

    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    return v3
.end method

.method public final removeDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z
    .locals 5

    const-string v0, "documentDescriptor"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v2, "removeDocument() may only be called from the UI thread."

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v2, 0x0

    if-ltz v0, :cond_4

    .line 58
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 59
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/e;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;

    .line 60
    invoke-interface {v4, p1}, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;->onDocumentRemoved(Lcom/pspdfkit/ui/DocumentDescriptor;)V

    goto :goto_0

    .line 61
    :cond_0
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    const/4 v4, 0x1

    if-ne p1, v3, :cond_3

    .line 62
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_2

    .line 63
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v0, -0x1

    :goto_1
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/DocumentDescriptor;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/e;->setVisibleDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    goto :goto_2

    .line 65
    :cond_2
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/ui/e;->a(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    :cond_3
    :goto_2
    return v4

    :cond_4
    return v2
.end method

.method public final removeOnDocumentCoordinatorEmptyListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentCoordinatorEmptyListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final removeOnDocumentVisibleListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentVisibleListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final removeOnDocumentsChangedListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final setDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z
    .locals 4

    const-string v0, "documentDescriptor"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v2, "setDocument() may only be called from the UI thread."

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    if-ne v0, p1, :cond_0

    return v3

    .line 60
    :cond_0
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {v3, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 61
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    .line 62
    iput-object v1, p0, Lcom/pspdfkit/internal/ui/e;->c:Lcom/pspdfkit/ui/DocumentDescriptor;

    .line 63
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/e;->setVisibleDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    .line 65
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/e;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;

    .line 66
    invoke-interface {v3, v0, p1}, Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentsChangedListener;->onDocumentReplaced(Lcom/pspdfkit/ui/DocumentDescriptor;Lcom/pspdfkit/ui/DocumentDescriptor;)V

    goto :goto_0

    :cond_1
    return v2

    .line 70
    :cond_2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/e;->addDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 71
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/e;->setVisibleDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    return v2

    :cond_3
    return v3
.end method

.method public final setVisibleDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "setVisibleDocument() may only be called from the UI thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    const-string v0, "visibleDocument"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/e;->a(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    move-result p1

    return p1
.end method

.class final Lcom/pspdfkit/internal/ui/c$c;
.super Lcom/pspdfkit/listeners/SimpleDocumentListener;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/ui/c;->b(Lcom/pspdfkit/ui/PdfFragment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/pspdfkit/ui/PdfFragment;

.field final synthetic c:Lcom/pspdfkit/internal/ui/c;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ui/c;Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/c$c;->c:Lcom/pspdfkit/internal/ui/c;

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/c$c;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-direct {p0}, Lcom/pspdfkit/listeners/SimpleDocumentListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c$c;->c:Lcom/pspdfkit/internal/ui/c;

    iget-object v0, p0, Lcom/pspdfkit/internal/ui/c$c;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getNavigationHistory()Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    move-result-object v0

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fputl(Lcom/pspdfkit/internal/ui/c;Lcom/pspdfkit/ui/navigation/NavigationBackStack;)V

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetF(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->addBackStackListener(Lcom/pspdfkit/ui/navigation/NavigationBackStack$BackStackListener;)V

    .line 6
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateBackButton()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    if-eqz v0, :cond_0

    .line 8
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 10
    :cond_0
    invoke-static {p1}, Lcom/pspdfkit/internal/ui/c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/yf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sm;->getNavigateForwardButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 12
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    const/4 v0, 0x0

    .line 16
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/c;->t(Z)V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/c$c;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    return-void
.end method

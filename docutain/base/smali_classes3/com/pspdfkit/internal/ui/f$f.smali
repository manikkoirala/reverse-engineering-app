.class final Lcom/pspdfkit/internal/ui/f$f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/PdfThumbnailGrid$OnPageClickListener;
.implements Lcom/pspdfkit/ui/PdfThumbnailGrid$OnDocumentSavedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/ui/f;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/ui/f;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/internal/ui/f$f-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/f$f;-><init>(Lcom/pspdfkit/internal/ui/f;)V

    return-void
.end method

.method private a(Lcom/pspdfkit/ui/DocumentDescriptor;Z)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ui/f;->getActivityState(ZZ)Landroid/os/Bundle;

    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    iget-object v1, v1, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast v1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sm;->getThumbnailGridView()Lcom/pspdfkit/ui/PdfThumbnailGrid;

    move-result-object v1

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    .line 7
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getDocumentEditor()Lcom/pspdfkit/document/editor/PdfDocumentEditor;

    move-result-object v1

    .line 8
    instance-of v4, v1, Lcom/pspdfkit/internal/k8;

    if-eqz v4, :cond_0

    .line 9
    move-object v5, v1

    check-cast v5, Lcom/pspdfkit/internal/k8;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/k8;->c()Ljava/lang/Integer;

    move-result-object v5

    goto :goto_0

    :cond_0
    move-object v5, v3

    :goto_0
    if-eqz p2, :cond_1

    if-eqz v4, :cond_1

    .line 13
    check-cast v1, Lcom/pspdfkit/internal/k8;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/k8;->b()Ljava/util/HashSet;

    move-result-object v3

    :cond_1
    move-object p2, v3

    move-object v3, v5

    goto :goto_1

    :cond_2
    move-object p2, v3

    :goto_1
    if-eqz v3, :cond_3

    .line 20
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v4, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    iget-object v4, v4, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v4}, Lcom/pspdfkit/ui/PdfFragment;->getPageCount()I

    move-result v4

    if-ge v1, v4, :cond_3

    if-eqz p2, :cond_4

    .line 21
    invoke-interface {p2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_4

    .line 22
    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :cond_4
    const-string p2, "PdfActivity.FragmentState"

    .line 25
    invoke-virtual {v0, p2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 28
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const-string v3, "PSPDFKit.ViewState"

    .line 29
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/ug$a;

    if-eqz v4, :cond_5

    .line 31
    new-instance v5, Lcom/pspdfkit/internal/ug$a;

    iget-object v6, v4, Lcom/pspdfkit/internal/ug$a;->a:Landroid/graphics/RectF;

    iget v4, v4, Lcom/pspdfkit/internal/ug$a;->b:F

    invoke-direct {v5, v6, v2, v4}, Lcom/pspdfkit/internal/ug$a;-><init>(Landroid/graphics/RectF;IF)V

    invoke-virtual {v1, v3, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 32
    :cond_5
    invoke-virtual {v0, p2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 35
    :cond_6
    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/DocumentDescriptor;->setState(Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public final onDocumentExported(Landroid/net/Uri;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getActiveView()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_THUMBNAIL_GRID:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-ne v0, v1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ui/f;->-$$Nest$mtoggleView(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    iget-object v0, v0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_1

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->getPassword()Ljava/lang/String;

    move-result-object v0

    .line 7
    invoke-static {p1, v0}, Lcom/pspdfkit/ui/DocumentDescriptor;->fromUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/pspdfkit/ui/DocumentDescriptor;

    move-result-object p1

    const/4 v0, 0x1

    .line 9
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/ui/f$f;->a(Lcom/pspdfkit/ui/DocumentDescriptor;Z)V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/DocumentCoordinator;->addDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/DocumentCoordinator;->setVisibleDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    :cond_1
    return-void
.end method

.method public final onDocumentSaved()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getActiveView()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_THUMBNAIL_GRID:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-ne v0, v1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ui/f;->-$$Nest$mtoggleView(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    iget-object v1, v0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    if-eqz v1, :cond_1

    .line 10
    iget-object v0, v0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getNavigationHistory()Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/ui/navigation/NavigationBackStack;

    invoke-direct {v1}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;-><init>()V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/navigation/NavigationBackStack;->replaceWith(Lcom/pspdfkit/ui/navigation/NavigationBackStack;)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    iget-object v0, v0, Lcom/pspdfkit/internal/ui/f;->document:Lcom/pspdfkit/internal/zf;

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/ui/DocumentDescriptor;->fromDocumentSource(Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/ui/DocumentDescriptor;

    move-result-object v0

    const/4 v1, 0x0

    .line 14
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/ui/f$f;->a(Lcom/pspdfkit/ui/DocumentDescriptor;Z)V

    .line 15
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/f;->getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/pspdfkit/ui/DocumentCoordinator;->setDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    :cond_1
    return-void
.end method

.method public final onPageClick(Lcom/pspdfkit/ui/PdfThumbnailGrid;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    iget-object v0, v0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->beginNavigation()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    iget-object v0, v0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(I)V

    .line 3
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/f$f;->a:Lcom/pspdfkit/internal/ui/f;

    iget-object p2, p2, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p2}, Lcom/pspdfkit/ui/PdfFragment;->endNavigation()V

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->hide()V

    return-void
.end method

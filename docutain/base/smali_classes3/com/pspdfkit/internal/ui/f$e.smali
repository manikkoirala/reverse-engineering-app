.class final Lcom/pspdfkit/internal/ui/f$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/PdfThumbnailBar$OnPageChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "e"
.end annotation


# instance fields
.field a:Z

.field b:Lio/reactivex/rxjava3/disposables/Disposable;

.field final synthetic c:Lcom/pspdfkit/internal/ui/f;


# direct methods
.method public static synthetic $r8$lambda$0f0dYYaA0VM_Ovks-FJPyC-83rw(Lcom/pspdfkit/internal/ui/f$e;Ljava/lang/Long;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/f$e;->a(Ljava/lang/Long;)V

    return-void
.end method

.method private constructor <init>(Lcom/pspdfkit/internal/ui/f;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f$e;->c:Lcom/pspdfkit/internal/ui/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 3
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/f$e;->a:Z

    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f$e;->b:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/internal/ui/f$e-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/f$e;-><init>(Lcom/pspdfkit/internal/ui/f;)V

    return-void
.end method

.method private synthetic a(Ljava/lang/Long;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$e;->c:Lcom/pspdfkit/internal/ui/f;

    iget-object p1, p1, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->endNavigation()V

    const/4 p1, 0x0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/f$e;->a:Z

    return-void
.end method


# virtual methods
.method public final onPageChanged(Lcom/pspdfkit/ui/thumbnail/PdfThumbnailBarController;I)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    const-string v0, "navigate_thumbnail_bar"

    .line 2
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    const-string v0, "page_index"

    .line 3
    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 5
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ui/f$e;->a:Z

    if-nez p1, :cond_0

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$e;->c:Lcom/pspdfkit/internal/ui/f;

    iget-object p1, p1, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->beginNavigation()V

    const/4 p1, 0x1

    .line 7
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/f$e;->a:Z

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$e;->c:Lcom/pspdfkit/internal/ui/f;

    iget-object p1, p1, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(I)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$e;->b:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 11
    invoke-static {p1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 12
    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v0, 0x1f4

    invoke-static {v0, v1, p1}, Lio/reactivex/rxjava3/core/Observable;->timer(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 13
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/ui/f$e$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/ui/f$e$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/f$e;)V

    .line 14
    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f$e;->b:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

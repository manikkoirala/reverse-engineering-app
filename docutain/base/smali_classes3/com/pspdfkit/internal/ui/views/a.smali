.class public final Lcom/pspdfkit/internal/ui/views/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:Lcom/pspdfkit/internal/ui/views/ValueSliderView;


# direct methods
.method constructor <init>(ILcom/pspdfkit/internal/ui/views/ValueSliderView;)V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/pspdfkit/internal/ui/views/a;->a:I

    iput p1, p0, Lcom/pspdfkit/internal/ui/views/a;->b:I

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/views/a;->c:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1

    const-string v0, "seekBar"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget p1, p0, Lcom/pspdfkit/internal/ui/views/a;->a:I

    add-int/2addr p2, p1

    iget v0, p0, Lcom/pspdfkit/internal/ui/views/a;->b:I

    .line 2
    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 3
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/views/a;->c:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-virtual {p2, p1, p3}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(IZ)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/views/a;->c:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-static {p1}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(Lcom/pspdfkit/internal/ui/views/ValueSliderView;)Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->focusCheck()V

    return-void
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    const-string v0, "seekBar"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    const-string v0, "seekBar"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

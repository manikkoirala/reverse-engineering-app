.class public final Lcom/pspdfkit/internal/ui/views/ValueSliderView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0010\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0011\u0012\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u0003\u00a2\u0006\u0004\u0008\u0014\u0010\u0015R0\u0010\u000b\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00028\u0006@\u0006X\u0086\u000e\u00a2\u0006\u0012\n\u0004\u0008\u0005\u0010\u0006\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\nR\u0011\u0010\u000e\u001a\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/pspdfkit/internal/ui/views/ValueSliderView;",
        "Landroid/widget/LinearLayout;",
        "Lkotlin/Function1;",
        "",
        "",
        "g",
        "Lkotlin/jvm/functions/Function1;",
        "getListener",
        "()Lkotlin/jvm/functions/Function1;",
        "setListener",
        "(Lkotlin/jvm/functions/Function1;)V",
        "listener",
        "getValue",
        "()I",
        "value",
        "Landroid/content/Context;",
        "context",
        "Landroid/util/AttributeSet;",
        "attrs",
        "defStyleAttr",
        "<init>",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# instance fields
.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/SeekBar;

.field private final d:Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

.field private e:I

.field private f:I

.field private g:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$pbRgSmOz3OnhpbTkjQQyGzBWksE(Lcom/pspdfkit/internal/ui/views/ValueSliderView;Lcom/pspdfkit/ui/editor/UnitSelectionEditText;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(Lcom/pspdfkit/internal/ui/views/ValueSliderView;Lcom/pspdfkit/ui/editor/UnitSelectionEditText;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 19
    sget p2, Lcom/pspdfkit/R$layout;->pspdf__value_slider:I

    invoke-virtual {p1, p2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 20
    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 22
    sget p1, Lcom/pspdfkit/R$id;->pspdf__sliderLabel:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.pspdf__sliderLabel)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->b:Landroid/widget/TextView;

    .line 23
    sget p1, Lcom/pspdfkit/R$id;->pspdf__sliderSeekBar:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.pspdf__sliderSeekBar)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/SeekBar;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->c:Landroid/widget/SeekBar;

    .line 24
    sget p1, Lcom/pspdfkit/R$id;->pspdf__sliderUnitEditText:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.pspdf__sliderUnitEditText)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->d:Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 1
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/ui/views/ValueSliderView;)Lcom/pspdfkit/ui/editor/UnitSelectionEditText;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->d:Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

    return-object p0
.end method

.method private static final a(Lcom/pspdfkit/internal/ui/views/ValueSliderView;Lcom/pspdfkit/ui/editor/UnitSelectionEditText;I)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 21
    invoke-virtual {p0, p2, p1}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(IZ)V

    return-void
.end method


# virtual methods
.method public final a(IZ)V
    .locals 2

    if-eqz p2, :cond_0

    .line 22
    iget v0, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->e:I

    iget v1, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->f:I

    .line 23
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->c:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->e:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->d:Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->setTextToFormat(I)V

    if-eqz p2, :cond_1

    .line 27
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->g:Lkotlin/jvm/functions/Function1;

    if-eqz p2, :cond_1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 8

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->e:I

    .line 3
    iput p2, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->f:I

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->d:Lcom/pspdfkit/ui/editor/UnitSelectionEditText;

    new-instance v7, Lcom/pspdfkit/internal/ui/views/ValueSliderView$$ExternalSyntheticLambda0;

    invoke-direct {v7, p0}, Lcom/pspdfkit/internal/ui/views/ValueSliderView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/views/ValueSliderView;)V

    const-string v3, "%d"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v6, p2

    invoke-virtual/range {v2 .. v7}, Lcom/pspdfkit/ui/editor/UnitSelectionEditText;->setUnitLabel(Ljava/lang/String;IIILcom/pspdfkit/ui/editor/UnitSelectionEditText$UnitSelectionListener;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->c:Landroid/widget/SeekBar;

    add-int/lit8 v1, p2, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->c:Landroid/widget/SeekBar;

    new-instance v1, Lcom/pspdfkit/internal/ui/views/a;

    invoke-direct {v1, p2, p0}, Lcom/pspdfkit/internal/ui/views/a;-><init>(ILcom/pspdfkit/internal/ui/views/ValueSliderView;)V

    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 20
    invoke-virtual {p0, v0, v0}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(IZ)V

    return-void
.end method

.method public final getListener()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->g:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getValue()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->c:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->e:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final setListener(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->g:Lkotlin/jvm/functions/Function1;

    return-void
.end method

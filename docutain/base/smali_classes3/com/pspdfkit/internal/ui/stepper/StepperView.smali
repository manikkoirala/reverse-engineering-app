.class public final Lcom/pspdfkit/internal/ui/stepper/StepperView;
.super Landroid/view/View;
.source "SourceFile"


# annotations
.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0015\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u001d\u001a\u00020\u001c\u0012\n\u0008\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0011\u0012\u0008\u0008\u0002\u0010\u001b\u001a\u00020\u0002\u00a2\u0006\u0004\u0008\u001e\u0010\u001fJ\u0008\u0010\u0003\u001a\u00020\u0002H\u0002J\u0008\u0010\u0005\u001a\u00020\u0004H\u0002J\u0008\u0010\u0006\u001a\u00020\u0002H\u0002J\u0008\u0010\u0007\u001a\u00020\u0002H\u0002J\u000e\u0010\n\u001a\u00020\t2\u0006\u0010\u0008\u001a\u00020\u0002J\u0014\u0010\u000e\u001a\u00020\t2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bJ\u000e\u0010\u0010\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u0002R\u0019\u0010\u0016\u001a\u0004\u0018\u00010\u00118\u0006\u00a2\u0006\u000c\n\u0004\u0008\u0012\u0010\u0013\u001a\u0004\u0008\u0014\u0010\u0015R\u0017\u0010\u001b\u001a\u00020\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008\u0017\u0010\u0018\u001a\u0004\u0008\u0019\u0010\u001a\u00a8\u0006 "
    }
    d2 = {
        "Lcom/pspdfkit/internal/ui/stepper/StepperView;",
        "Landroid/view/View;",
        "",
        "getCircleY",
        "",
        "getCirclePositions",
        "getStartCirclePosition",
        "getEndCirclePosition",
        "count",
        "",
        "setStepsCount",
        "",
        "",
        "stepLabels",
        "setSteps",
        "theme",
        "setTheme",
        "Landroid/util/AttributeSet;",
        "b",
        "Landroid/util/AttributeSet;",
        "getAttrs",
        "()Landroid/util/AttributeSet;",
        "attrs",
        "c",
        "I",
        "getDefStyle",
        "()I",
        "defStyle",
        "Landroid/content/Context;",
        "context",
        "<init>",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# instance fields
.field private final b:Landroid/util/AttributeSet;

.field private final c:I

.field private final d:Landroid/graphics/Paint;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:F

.field private m:F

.field private n:I

.field private o:F

.field private p:F

.field private q:I

.field private r:I

.field private s:I

.field private t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private u:[I

.field private v:I

.field private w:[I

.field private x:[I

.field private final y:Landroid/graphics/Rect;

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/stepper/StepperView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/stepper/StepperView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->b:Landroid/util/AttributeSet;

    .line 4
    iput p3, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->c:I

    .line 8
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    const/4 p2, 0x1

    .line 9
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 10
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    const/4 p1, 0x3

    .line 42
    iput p1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    const/high16 p1, 0x42200000    # 40.0f

    .line 48
    iput p1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->p:F

    const/16 p1, 0x8

    .line 54
    iput p1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->r:I

    .line 60
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->t:Ljava/util/ArrayList;

    const/4 p1, 0x0

    new-array p2, p1, [I

    .line 63
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->u:[I

    new-array p2, p1, [I

    .line 69
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->w:[I

    new-array p1, p1, [I

    .line 72
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->x:[I

    .line 75
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->y:Landroid/graphics/Rect;

    .line 78
    sget p1, Lcom/pspdfkit/R$style;->PSPDFKit_StepView:I

    iput p1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->z:I

    .line 81
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->a()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 1
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/stepper/StepperView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final a()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView:[I

    sget v2, Lcom/pspdfkit/R$attr;->pspdf__stepperViewStyle:I

    iget v3, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->z:I

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const-string v1, "context.obtainStyledAttr\u2026wStyle, stepperViewTheme)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView_pspdf__selectedTextColor:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->g:I

    .line 3
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView_pspdf__unselectedTextColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->h:I

    .line 4
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView_pspdf__selectedStepColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->e:I

    .line 5
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView_pspdf__unselectedStepColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->f:I

    .line 6
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView_pspdf__selectedDividerColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->i:I

    .line 7
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView_pspdf__unselectedDividerColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->j:I

    .line 8
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView_pspdf__labelTextColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->k:I

    .line 9
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView_pspdf__stepNumberTextSize:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->l:F

    .line 10
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView_pspdf__stepLabelTextSize:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->m:F

    .line 11
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView_pspdf__stepRadius:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->o:F

    .line 12
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView_pspdf__stepPadding:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->q:I

    .line 13
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView_pspdf__stepPadding:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->p:F

    .line 14
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StepperView_pspdf__stepPadding:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->r:I

    .line 15
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private final a(Landroid/graphics/Canvas;IFLandroid/graphics/Paint;)V
    .locals 4

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "steps[step]"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/String;

    .line 20
    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p4, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 21
    iget v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->m:F

    invoke-virtual {p4, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 22
    iget v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->k:I

    invoke-virtual {p4, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 23
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    iget v2, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->s:I

    const/4 v3, 0x0

    if-ne v2, p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-static {v1, p2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object p2

    invoke-virtual {p4, p2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 24
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p2

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->y:Landroid/graphics/Rect;

    invoke-virtual {p4, v0, v3, p2, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 25
    iget p2, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->v:I

    int-to-float p2, p2

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->y:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v1, p2

    iget-object p2, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->y:Landroid/graphics/Rect;

    iget p2, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float p2, p2

    sub-float/2addr v1, p2

    iget p2, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->o:F

    iget v2, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->p:F

    add-float/2addr p2, v2

    add-float/2addr p2, v1

    .line 26
    invoke-virtual {p1, v0, p3, p2, p4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method private final getCirclePositions()[I
    .locals 6

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    new-array v1, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v0, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_1

    return-object v1

    .line 5
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->getStartCirclePosition()I

    move-result v4

    aput v4, v1, v2

    if-ne v0, v3, :cond_2

    return-object v1

    .line 9
    :cond_2
    iget v4, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    sub-int/2addr v4, v3

    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->getEndCirclePosition()I

    move-result v5

    aput v5, v1, v4

    const/4 v4, 0x3

    if-ge v0, v4, :cond_3

    return-object v1

    .line 10
    :cond_3
    invoke-static {p0}, Landroidx/core/view/ViewCompat;->getLayoutDirection(Landroid/view/View;)I

    move-result v0

    if-ne v0, v3, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_5

    .line 11
    aget v0, v1, v2

    iget v4, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    sub-int/2addr v4, v3

    aget v4, v1, v4

    sub-int/2addr v0, v4

    int-to-float v0, v0

    goto :goto_2

    :cond_5
    iget v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    sub-int/2addr v0, v3

    aget v0, v1, v0

    int-to-float v0, v0

    aget v4, v1, v2

    int-to-float v4, v4

    sub-float/2addr v0, v4

    .line 12
    :goto_2
    iget v4, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    sub-int/2addr v4, v3

    int-to-float v4, v4

    div-float/2addr v0, v4

    float-to-int v0, v0

    .line 13
    invoke-static {p0}, Landroidx/core/view/ViewCompat;->getLayoutDirection(Landroid/view/View;)I

    move-result v4

    if-ne v4, v3, :cond_6

    const/4 v2, 0x1

    :cond_6
    if-eqz v2, :cond_7

    .line 14
    iget v2, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    sub-int/2addr v2, v3

    :goto_3
    if-ge v3, v2, :cond_8

    add-int/lit8 v4, v3, -0x1

    .line 15
    aget v4, v1, v4

    sub-int/2addr v4, v0

    aput v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 18
    :cond_7
    iget v2, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    sub-int/2addr v2, v3

    :goto_4
    if-ge v3, v2, :cond_8

    add-int/lit8 v4, v3, -0x1

    .line 19
    aget v4, v1, v4

    add-int/2addr v4, v0

    aput v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_8
    return-object v1
.end method

.method private final getCircleY()I
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    add-int/2addr v2, v1

    iget v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->m:F

    float-to-int v1, v1

    iget v3, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->r:I

    add-int/2addr v1, v3

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 2
    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private final getEndCirclePosition()I
    .locals 2

    .line 1
    invoke-static {p0}, Landroidx/core/view/ViewCompat;->getLayoutDirection(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->o:F

    float-to-int v1, v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->q:I

    add-int/2addr v0, v1

    goto :goto_1

    .line 4
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->o:F

    float-to-int v1, v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->q:I

    sub-int/2addr v0, v1

    :goto_1
    return v0
.end method

.method private final getStartCirclePosition()I
    .locals 2

    .line 1
    invoke-static {p0}, Landroidx/core/view/ViewCompat;->getLayoutDirection(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->o:F

    float-to-int v1, v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->q:I

    sub-int/2addr v0, v1

    goto :goto_1

    .line 4
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->o:F

    float-to-int v1, v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->q:I

    add-int/2addr v0, v1

    :goto_1
    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    if-ltz p1, :cond_1

    .line 16
    iget v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    if-nez v0, :cond_0

    goto :goto_0

    .line 17
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->s:I

    .line 18
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_1
    :goto_0
    return-void
.end method

.method public final getAttrs()Landroid/util/AttributeSet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->b:Landroid/util/AttributeSet;

    return-object v0
.end method

.method public final getDefStyle()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->c:I

    return v0
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v7, p1

    if-nez v7, :cond_0

    return-void

    .line 1
    :cond_0
    iget v8, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    const/4 v9, 0x1

    if-ge v8, v9, :cond_1

    return-void

    :cond_1
    const/4 v10, 0x0

    const/4 v11, 0x0

    :goto_0
    if-ge v11, v8, :cond_6

    .line 3
    iget-object v1, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->u:[I

    aget v1, v1, v11

    int-to-float v12, v1

    iget v1, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->v:I

    int-to-float v1, v1

    .line 4
    iget v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->s:I

    if-ne v11, v2, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-ge v11, v2, :cond_3

    const/4 v2, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/lit8 v13, v11, 0x1

    .line 6
    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const/high16 v5, 0x40000000    # 2.0f

    if-eqz v3, :cond_4

    if-nez v2, :cond_4

    .line 10
    iget-object v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    iget v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->e:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 14
    iget v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->o:F

    .line 15
    iget-object v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    .line 16
    invoke-virtual {v7, v12, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 22
    iget-object v1, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    iget v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->g:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 23
    iget-object v1, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    .line 24
    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 25
    iget v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->l:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 26
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-static {v2, v9}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 27
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->y:Landroid/graphics/Rect;

    invoke-virtual {v1, v4, v10, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 28
    iget v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->v:I

    int-to-float v2, v2

    iget-object v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->y:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    add-float/2addr v3, v2

    iget-object v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->y:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    sub-float/2addr v3, v2

    .line 29
    invoke-virtual {v7, v4, v12, v3, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 30
    iget-object v1, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    invoke-direct {v0, v7, v11, v12, v1}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->a(Landroid/graphics/Canvas;IFLandroid/graphics/Paint;)V

    move/from16 v18, v8

    goto/16 :goto_3

    :cond_4
    if-eqz v2, :cond_5

    .line 32
    iget-object v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    iget v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->e:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 33
    iget v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->o:F

    iget-object v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    invoke-virtual {v7, v12, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    float-to-int v2, v12

    float-to-int v1, v1

    .line 34
    iget-object v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    iget v4, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->g:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 35
    iget v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->l:F

    const v4, 0x3dcccccd    # 0.1f

    mul-float v14, v3, v4

    .line 36
    iget-object v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    invoke-virtual {v3, v14}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 37
    new-instance v15, Landroid/graphics/Rect;

    int-to-double v2, v2

    float-to-double v4, v14

    const-wide/high16 v16, 0x4012000000000000L    # 4.5

    mul-double v16, v16, v4

    sub-double v9, v2, v16

    double-to-int v6, v9

    int-to-double v9, v1

    const-wide/high16 v18, 0x400c000000000000L    # 3.5

    mul-double v4, v4, v18

    move/from16 v18, v8

    sub-double v7, v9, v4

    double-to-int v1, v7

    add-double v2, v2, v16

    double-to-int v2, v2

    add-double/2addr v9, v4

    double-to-int v3, v9

    invoke-direct {v15, v6, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 44
    iget v1, v15, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float v2, v2, v14

    add-float/2addr v2, v1

    .line 45
    iget v3, v15, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    const/high16 v4, 0x40500000    # 3.25f

    mul-float v4, v4, v14

    sub-float v5, v3, v4

    add-float/2addr v4, v1

    const/high16 v1, 0x3f400000    # 0.75f

    mul-float v7, v14, v1

    sub-float v6, v3, v7

    .line 48
    iget-object v8, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v3, v5

    move v5, v6

    move-object v6, v8

    .line 49
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 57
    iget v1, v15, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    const/high16 v2, 0x40300000    # 2.75f

    mul-float v2, v2, v14

    add-float/2addr v2, v1

    .line 58
    iget v1, v15, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    sub-float v3, v1, v7

    .line 59
    iget v1, v15, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    const/high16 v4, 0x3ec00000    # 0.375f

    mul-float v14, v14, v4

    sub-float v4, v1, v14

    .line 60
    iget v1, v15, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    add-float v5, v1, v7

    .line 61
    iget-object v6, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    .line 62
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 63
    iget-object v1, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    move-object/from16 v7, p1

    invoke-direct {v0, v7, v11, v12, v1}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->a(Landroid/graphics/Canvas;IFLandroid/graphics/Paint;)V

    :goto_3
    const/4 v8, 0x1

    const/4 v9, 0x0

    goto :goto_4

    :cond_5
    move/from16 v18, v8

    .line 65
    iget-object v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    iget v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->f:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 69
    iget v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->o:F

    .line 70
    iget-object v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    .line 71
    invoke-virtual {v7, v12, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 77
    iget-object v1, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    iget v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->h:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    iget-object v1, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    .line 79
    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 80
    iget v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->l:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 81
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v8, 0x1

    invoke-static {v2, v8}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 82
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->y:Landroid/graphics/Rect;

    const/4 v9, 0x0

    invoke-virtual {v1, v4, v9, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 83
    iget v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->v:I

    int-to-float v2, v2

    iget-object v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->y:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    add-float/2addr v3, v2

    iget-object v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->y:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    sub-float/2addr v3, v2

    .line 84
    invoke-virtual {v7, v4, v12, v3, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 85
    iget-object v1, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    invoke-direct {v0, v7, v11, v12, v1}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->a(Landroid/graphics/Canvas;IFLandroid/graphics/Paint;)V

    :goto_4
    move v11, v13

    move/from16 v8, v18

    const/4 v9, 0x1

    const/4 v10, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 86
    iget-object v1, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->w:[I

    array-length v10, v1

    const/4 v11, 0x0

    :goto_5
    if-ge v11, v10, :cond_9

    .line 87
    iget-object v1, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->w:[I

    aget v1, v1, v11

    iget-object v2, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->x:[I

    aget v2, v2, v11

    iget v3, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->v:I

    iget v4, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->s:I

    if-ge v11, v4, :cond_7

    const/4 v4, 0x1

    goto :goto_6

    :cond_7
    const/4 v4, 0x0

    :goto_6
    if-eqz v4, :cond_8

    .line 88
    iget-object v4, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    iget v5, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->i:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_7

    .line 90
    :cond_8
    iget-object v4, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    iget v5, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->j:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 92
    :goto_7
    iget-object v4, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    const/high16 v5, 0x40400000    # 3.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    int-to-float v4, v1

    int-to-float v5, v3

    int-to-float v6, v2

    .line 93
    iget-object v12, v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->d:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v2, v4

    move v3, v5

    move v4, v6

    move-object v6, v12

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    :cond_9
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 3
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->t:Ljava/util/ArrayList;

    add-int/lit8 v2, v2, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Step "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    .line 8
    :cond_1
    iget v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    if-lez v0, :cond_5

    .line 9
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->getCircleY()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    add-int/2addr v2, v0

    iput v2, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->v:I

    .line 10
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->getCirclePositions()[I

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->u:[I

    .line 11
    iget v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    const/4 v2, 0x1

    if-ge v0, v2, :cond_2

    goto :goto_4

    :cond_2
    add-int/lit8 v3, v0, -0x1

    .line 12
    new-array v4, v3, [I

    iput-object v4, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->w:[I

    .line 13
    new-array v3, v3, [I

    iput-object v3, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->x:[I

    .line 14
    iget v3, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->q:I

    iget v4, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->o:F

    float-to-int v4, v4

    add-int/2addr v3, v4

    const/4 v4, 0x1

    :goto_1
    if-ge v4, v0, :cond_5

    .line 15
    invoke-static {p0}, Landroidx/core/view/ViewCompat;->getLayoutDirection(Landroid/view/View;)I

    move-result v5

    if-ne v5, v2, :cond_3

    const/4 v5, 0x1

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    if-eqz v5, :cond_4

    .line 16
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->w:[I

    add-int/lit8 v6, v4, -0x1

    iget-object v7, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->u:[I

    aget v8, v7, v6

    sub-int/2addr v8, v3

    aput v8, v5, v6

    .line 17
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->x:[I

    aget v7, v7, v4

    add-int/2addr v7, v3

    aput v7, v5, v6

    goto :goto_3

    .line 19
    :cond_4
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->w:[I

    add-int/lit8 v6, v4, -0x1

    iget-object v7, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->u:[I

    aget v8, v7, v6

    add-int/2addr v8, v3

    aput v8, v5, v6

    .line 20
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->x:[I

    aget v7, v7, v4

    sub-int/2addr v7, v3

    aput v7, v5, v6

    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 21
    :cond_5
    :goto_4
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    return-void
.end method

.method public final setSteps(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "stepLabels"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->t:Ljava/util/ArrayList;

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public final setStepsCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->n:I

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public final setTheme(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ui/stepper/StepperView;->z:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->a()V

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

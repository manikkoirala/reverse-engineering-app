.class final Lcom/pspdfkit/internal/ui/f$a;
.super Lcom/pspdfkit/ui/special_mode/manager/OnAnnotationSelectedListenerAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ui/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/pspdfkit/internal/ui/f;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ui/f;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/f$a;->b:Lcom/pspdfkit/internal/ui/f;

    invoke-direct {p0}, Lcom/pspdfkit/ui/special_mode/manager/OnAnnotationSelectedListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnnotationSelected(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$a;->b:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/f;->getActiveView()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object p1

    sget-object p2, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_SEARCH:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-ne p1, p2, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$a;->b:Lcom/pspdfkit/internal/ui/f;

    sget-object p2, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ui/f;->-$$Nest$mtoggleView(Lcom/pspdfkit/internal/ui/f;Lcom/pspdfkit/ui/PSPDFKitViews$Type;)V

    goto :goto_0

    .line 3
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$a;->b:Lcom/pspdfkit/internal/ui/f;

    iget-object p1, p1, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/f$a;->b:Lcom/pspdfkit/internal/ui/f;

    iget-object p1, p1, Lcom/pspdfkit/internal/ui/f;->views:Lcom/pspdfkit/internal/yf;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sm;->b()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/ui/search/PdfSearchView;->clearSearch()V

    :cond_1
    :goto_0
    return-void
.end method

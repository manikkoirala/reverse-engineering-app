.class public final Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0007\u0018\u00002\u00020\u0001:\u0001#B\'\u0008\u0007\u0012\u0006\u0010\u001d\u001a\u00020\u001c\u0012\n\u0008\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u001e\u0012\u0008\u0008\u0002\u0010 \u001a\u00020\u0003\u00a2\u0006\u0004\u0008!\u0010\"R6\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00022\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00028\u0006@FX\u0086\u000e\u00a2\u0006\u0012\n\u0004\u0008\u0005\u0010\u0006\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\nR$\u0010\u0013\u001a\u0004\u0018\u00010\u000c8\u0006@\u0006X\u0086\u000e\u00a2\u0006\u0012\n\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R*\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u0004\u001a\u00020\u00148\u0006@FX\u0086\u000e\u00a2\u0006\u0012\n\u0004\u0008\u0015\u0010\u0016\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001a\u00a8\u0006$"
    }
    d2 = {
        "Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;",
        "Landroid/widget/FrameLayout;",
        "",
        "",
        "value",
        "b",
        "Ljava/util/List;",
        "getAvailableColors",
        "()Ljava/util/List;",
        "setAvailableColors",
        "(Ljava/util/List;)V",
        "availableColors",
        "Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$a;",
        "c",
        "Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$a;",
        "getOnColorPickedListener",
        "()Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$a;",
        "setOnColorPickedListener",
        "(Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$a;)V",
        "onColorPickedListener",
        "",
        "d",
        "Z",
        "getShowSelectionIndicator",
        "()Z",
        "setShowSelectionIndicator",
        "(Z)V",
        "showSelectionIndicator",
        "Landroid/content/Context;",
        "context",
        "Landroid/util/AttributeSet;",
        "attrs",
        "defStyleAttr",
        "<init>",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "a",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$a;

.field private d:Z

.field private e:I

.field private final f:I

.field private g:I

.field private h:I

.field private final i:Landroid/graphics/drawable/LayerDrawable;


# direct methods
.method public static synthetic $r8$lambda$s1S1FFV424Hd7VHfytvpCo8WJDQ(Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;ILandroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->a(Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;ILandroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 5
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->b:Ljava/util/List;

    const/high16 p2, -0x1000000

    .line 26
    iput p2, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->e:I

    .line 28
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/pspdfkit/R$dimen;->pspdf__color_picker_color_padding:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->f:I

    .line 35
    sget p2, Lcom/pspdfkit/R$drawable;->pspdf__ic_color_selected:I

    const/4 p3, -0x1

    .line 36
    invoke-static {p1, p2, p3}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    .line 44
    sget p3, Lcom/pspdfkit/R$drawable;->pspdf__ic_color_selected_bg:I

    .line 45
    invoke-static {p1, p3}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 50
    new-instance p3, Landroid/graphics/drawable/LayerDrawable;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    invoke-direct {p3, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->i:Landroid/graphics/drawable/LayerDrawable;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 1
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final a()V
    .locals 7

    .line 6
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->d:Z

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_5

    .line 7
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    :goto_0
    if-ge v1, v0, :cond_7

    .line 8
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 9
    instance-of v4, v3, Landroid/widget/ImageView;

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    instance-of v6, v5, Ljava/lang/Integer;

    if-eqz v6, :cond_0

    check-cast v5, Ljava/lang/Integer;

    goto :goto_1

    :cond_0
    move-object v5, v2

    :goto_1
    iget v6, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->e:I

    if-nez v5, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v6, :cond_2

    .line 10
    check-cast v3, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->i:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    :cond_2
    :goto_2
    if-eqz v4, :cond_3

    .line 12
    check-cast v3, Landroid/widget/ImageView;

    goto :goto_3

    :cond_3
    move-object v3, v2

    :goto_3
    if-eqz v3, :cond_4

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 16
    :cond_5
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    :goto_5
    if-ge v1, v0, :cond_7

    .line 17
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 18
    instance-of v4, v3, Landroid/widget/ImageView;

    if-eqz v4, :cond_6

    .line 19
    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_7
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;ILandroid/view/View;)V
    .locals 0

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->a(I)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->c:Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$a;

    if-eqz p2, :cond_0

    invoke-interface {p2, p0, p1}, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$a;->a(Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;I)V

    :cond_0
    return-void
.end method

.method private final b()V
    .locals 7

    .line 1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    .line 5
    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 6
    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 7
    new-instance v3, Lcom/pspdfkit/internal/q5;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x4

    invoke-direct {v3, v4, v1, v5}, Lcom/pspdfkit/internal/q5;-><init>(Landroid/content/Context;II)V

    .line 8
    invoke-static {v2, v3}, Landroidx/core/view/ViewCompat;->setBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 10
    new-instance v4, Landroid/graphics/drawable/RippleDrawable;

    const/16 v5, 0x42

    const/16 v6, 0xff

    invoke-static {v5, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-static {v5}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, v5, v3, v6}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 12
    new-instance v3, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0, v1}, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;I)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v3, 0x1

    .line 17
    invoke-virtual {v2, v3}, Landroid/view/View;->setClickable(Z)V

    .line 19
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 20
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Z
    .locals 1

    .line 3
    iget v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->e:I

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->d:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 4
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->e:I

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->a()V

    const/4 p1, 0x1

    return p1
.end method

.method public final getAvailableColors()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->b:Ljava/util/List;

    return-object v0
.end method

.method public final getOnColorPickedListener()Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->c:Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$a;

    return-object v0
.end method

.method public final getShowSelectionIndicator()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->d:Z

    return v0
.end method

.method protected final onLayout(ZIIII)V
    .locals 4

    .line 1
    iget p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->h:I

    mul-int/lit8 p2, p1, 0x9

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    sub-int/2addr p3, p2

    div-int/lit8 p3, p3, 0x2

    .line 7
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    const/4 p4, 0x0

    :goto_0
    if-ge p4, p2, :cond_0

    .line 8
    invoke-virtual {p0, p4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p5

    .line 9
    div-int/lit8 v0, p4, 0x9

    .line 10
    rem-int/lit8 v1, p4, 0x9

    mul-int v1, v1, p1

    add-int/2addr v1, p3

    .line 12
    iget v2, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->f:I

    add-int/2addr v1, v2

    mul-int v0, v0, p1

    add-int/2addr v0, v2

    .line 18
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v1

    .line 19
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    .line 20
    invoke-virtual {p5, v1, v0, v2, v3}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 4

    const/4 p2, 0x0

    .line 1
    invoke-static {p2, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result p1

    .line 4
    iget p2, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->f:I

    mul-int/lit8 p2, p2, 0x2

    sub-int p2, p1, p2

    div-int/lit8 p2, p2, 0x9

    iput p2, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->h:I

    .line 6
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    int-to-double v0, p2

    const-wide/high16 v2, 0x4022000000000000L    # 9.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int p2, v0

    iput p2, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->g:I

    .line 9
    iget p2, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->h:I

    iget v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->f:I

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr p2, v0

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 10
    invoke-virtual {p0, p2, p2}, Landroid/view/ViewGroup;->measureChildren(II)V

    .line 11
    iget p2, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->g:I

    iget v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->h:I

    mul-int p2, p2, v0

    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public final setAvailableColors(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->b:Ljava/util/List;

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->b()V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->a()V

    return-void
.end method

.method public final setOnColorPickedListener(Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->c:Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView$a;

    return-void
.end method

.method public final setShowSelectionIndicator(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->d:Z

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/inspector/ColorPaletteView;->a()V

    return-void
.end method

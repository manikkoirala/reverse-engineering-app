.class public final Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\u001eB\'\u0008\u0007\u0012\u0006\u0010\u0018\u001a\u00020\u0017\u0012\n\u0008\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0019\u0012\u0008\u0008\u0002\u0010\u001b\u001a\u00020\u0002\u00a2\u0006\u0004\u0008\u001c\u0010\u001dR*\u0010\n\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00028\u0006@FX\u0087\u000e\u00a2\u0006\u0012\n\u0004\u0008\u0004\u0010\u0005\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR*\u0010\u000e\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00028\u0006@FX\u0087\u000e\u00a2\u0006\u0012\n\u0004\u0008\u000b\u0010\u0005\u001a\u0004\u0008\u000c\u0010\u0007\"\u0004\u0008\r\u0010\tR$\u0010\u0016\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006X\u0086\u000e\u00a2\u0006\u0012\n\u0004\u0008\u0010\u0010\u0011\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;",
        "Landroid/widget/LinearLayout;",
        "",
        "value",
        "b",
        "I",
        "getPreviousColor",
        "()I",
        "setPreviousColor",
        "(I)V",
        "previousColor",
        "c",
        "getCurrentColor",
        "setCurrentColor",
        "currentColor",
        "Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$a;",
        "d",
        "Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$a;",
        "getOnPreviousColorSelected",
        "()Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$a;",
        "setOnPreviousColorSelected",
        "(Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$a;)V",
        "onPreviousColorSelected",
        "Landroid/content/Context;",
        "context",
        "Landroid/util/AttributeSet;",
        "attrs",
        "defStyleAttr",
        "<init>",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "a",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# instance fields
.field private b:I

.field private c:I

.field private d:Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$a;

.field private e:Landroidx/palette/graphics/Palette$Swatch;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/view/View;

.field private final j:Landroid/view/View;

.field private k:Landroid/animation/ValueAnimator;


# direct methods
.method public static synthetic $r8$lambda$0OawWGW0p4rcsvftkEP898yuA0Y(Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->a(Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method public static synthetic $r8$lambda$UAjt44JHH8dzKi4fZ6ONMlTZJbY(Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->a(Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 p2, -0x1000000

    .line 5
    iput p2, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->b:I

    .line 12
    iput p2, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->c:I

    .line 24
    new-instance p3, Landroidx/palette/graphics/Palette$Swatch;

    const/4 v0, 0x1

    invoke-direct {p3, p2, v0}, Landroidx/palette/graphics/Palette$Swatch;-><init>(II)V

    iput-object p3, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->e:Landroidx/palette/graphics/Palette$Swatch;

    .line 34
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget p3, Lcom/pspdfkit/R$layout;->pspdf__color_preview_view:I

    invoke-virtual {p2, p3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 35
    new-instance p2, Lcom/pspdfkit/internal/q5;

    const/4 p3, 0x0

    invoke-direct {p2, p1, p3, p3}, Lcom/pspdfkit/internal/q5;-><init>(Landroid/content/Context;II)V

    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 36
    invoke-virtual {p0, p3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 38
    sget p1, Lcom/pspdfkit/R$id;->pspdf__hex_title:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.pspdf__hex_title)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->f:Landroid/widget/TextView;

    .line 39
    sget p1, Lcom/pspdfkit/R$id;->pspdf__hsl_title:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.pspdf__hsl_title)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->g:Landroid/widget/TextView;

    .line 40
    sget p1, Lcom/pspdfkit/R$id;->pspdf__rgb_title:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.pspdf__rgb_title)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->h:Landroid/widget/TextView;

    .line 41
    sget p1, Lcom/pspdfkit/R$id;->pspdf__current_color_view:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.pspdf__current_color_view)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->i:Landroid/view/View;

    .line 42
    sget p1, Lcom/pspdfkit/R$id;->pspdf__previous_color_view:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.pspdf__previous_color_view)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->j:Landroid/view/View;

    .line 43
    new-instance p2, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 1
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final a(II)V
    .locals 3

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->k:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 4
    :cond_0
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v1, p2

    invoke-static {v0, v1}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object p1

    const-wide/16 v0, 0xa0

    .line 5
    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 6
    new-instance p2, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$$ExternalSyntheticLambda1;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;)V

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 9
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    .line 10
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->k:Landroid/animation/ValueAnimator;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;Landroid/animation/ValueAnimator;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    iget-object p0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->i:Landroid/view/View;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "null cannot be cast to non-null type kotlin.Int"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->b:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->setCurrentColor(I)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->d:Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$a;

    if-eqz p1, :cond_0

    iget p0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->b:I

    invoke-interface {p1, p0}, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$a;->a(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final getCurrentColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->c:I

    return v0
.end method

.method public final getOnPreviousColorSelected()Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->d:Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$a;

    return-object v0
.end method

.method public final getPreviousColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->b:I

    return v0
.end method

.method public final setCurrentColor(I)V
    .locals 10

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->c:I

    if-eq v0, p1, :cond_0

    .line 2
    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->a(II)V

    .line 4
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->c:I

    .line 5
    new-instance v0, Landroidx/palette/graphics/Palette$Swatch;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Landroidx/palette/graphics/Palette$Swatch;-><init>(II)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->e:Landroidx/palette/graphics/Palette$Swatch;

    .line 6
    invoke-virtual {v0}, Landroidx/palette/graphics/Palette$Swatch;->getRgb()I

    move-result p1

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result p1

    const/16 v0, 0xff

    if-ne p1, v0, :cond_1

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->e:Landroidx/palette/graphics/Palette$Swatch;

    invoke-virtual {p1}, Landroidx/palette/graphics/Palette$Swatch;->getBodyTextColor()I

    move-result p1

    goto :goto_0

    :cond_1
    const/high16 p1, -0x1000000

    .line 12
    :goto_0
    iget v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->c:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ft;->a(IZZ)Ljava/lang/String;

    move-result-object v0

    .line 13
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->f:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v0, 0x3

    new-array v3, v0, [F

    .line 17
    iget v4, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->c:I

    invoke-static {v4, v3}, Landroidx/core/graphics/ColorUtils;->colorToHSL(I[F)V

    .line 19
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/pspdfkit/R$string;->pspdf__color_picker_hsl:I

    const/4 v6, 0x0

    .line 20
    invoke-static {v4, v5, v6}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "getString(context, R.str\u2026.pspdf__color_picker_hsl)"

    .line 21
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    sget-object v5, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    const/4 v5, 0x4

    new-array v7, v5, [Ljava/lang/Object;

    aput-object v4, v7, v2

    aget v4, v3, v2

    float-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v1

    aget v4, v3, v1

    const/16 v8, 0x64

    int-to-float v8, v8

    mul-float v4, v4, v8

    float-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v9, 0x2

    aput-object v4, v7, v9

    aget v3, v3, v9

    mul-float v3, v3, v8

    float-to-int v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v0

    invoke-static {v7, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v3

    const-string v4, "%s %d %d %d"

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v7, "format(format, *args)"

    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v8, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->g:Landroid/widget/TextView;

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 24
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->g:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 26
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v8, Lcom/pspdfkit/R$string;->pspdf__color_picker_rgb:I

    .line 27
    invoke-static {v3, v8, v6}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "getString(context, R.str\u2026.pspdf__color_picker_rgb)"

    .line 28
    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v6, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->h:Landroid/widget/TextView;

    sget-object v8, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    new-array v8, v5, [Ljava/lang/Object;

    aput-object v3, v8, v2

    iget v2, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->c:I

    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v1

    iget v1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->c:I

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v9

    iget v1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->c:I

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v0

    invoke-static {v8, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public final setOnPreviousColorSelected(Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->d:Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView$a;

    return-void
.end method

.method public final setPreviousColor(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->b:I

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/inspector/ColorPreviewView;->j:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method

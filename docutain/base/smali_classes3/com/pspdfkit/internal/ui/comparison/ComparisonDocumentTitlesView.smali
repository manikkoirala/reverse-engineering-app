.class public final Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;
.super Landroid/view/View;
.source "SourceFile"


# annotations
.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\u0008\u0012\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u0002\u00a2\u0006\u0004\u0008\u0015\u0010\u0016J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0002R\u0019\u0010\r\u001a\u0004\u0018\u00010\u00088\u0006\u00a2\u0006\u000c\n\u0004\u0008\t\u0010\n\u001a\u0004\u0008\u000b\u0010\u000cR\u0017\u0010\u0012\u001a\u00020\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008\u000e\u0010\u000f\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;",
        "Landroid/view/View;",
        "",
        "index",
        "",
        "setCurrentDocument",
        "theme",
        "setTheme",
        "Landroid/util/AttributeSet;",
        "b",
        "Landroid/util/AttributeSet;",
        "getAttrs",
        "()Landroid/util/AttributeSet;",
        "attrs",
        "c",
        "I",
        "getDefStyle",
        "()I",
        "defStyle",
        "Landroid/content/Context;",
        "context",
        "<init>",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# instance fields
.field private final b:Landroid/util/AttributeSet;

.field private final c:I

.field private final d:Landroid/graphics/Paint;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:F

.field private final j:[Ljava/lang/String;

.field private k:F

.field private l:[F

.field private m:F

.field private n:F

.field private o:F

.field private p:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->b:Landroid/util/AttributeSet;

    .line 4
    iput p3, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->c:I

    .line 8
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    const/4 p3, 0x1

    .line 9
    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 10
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->d:Landroid/graphics/Paint;

    .line 15
    sget p2, Lcom/pspdfkit/R$style;->PSPDFKit_BreadCrumbsView:I

    iput p2, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->e:I

    const/4 p2, 0x2

    new-array v0, p2, [Ljava/lang/String;

    .line 30
    sget v1, Lcom/pspdfkit/R$string;->pspdf__old_document:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget v1, Lcom/pspdfkit/R$string;->pspdf__new_document:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, p3

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->j:[Ljava/lang/String;

    new-array p1, p2, [F

    .line 36
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->l:[F

    .line 51
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->a()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 1
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final a()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->b:Landroid/util/AttributeSet;

    .line 3
    sget-object v2, Lcom/pspdfkit/R$styleable;->pspdf__BreadCrumbsView:[I

    .line 4
    sget v3, Lcom/pspdfkit/R$attr;->pspdf__breadCrumbsViewStyle:I

    .line 5
    iget v4, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->e:I

    .line 6
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const-string v1, "context.obtainStyledAttr\u2026efaultStyle\n            )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__BreadCrumbsView_pspdf__titleColor:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->f:I

    .line 13
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__BreadCrumbsView_pspdf__selectedTitleColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->g:I

    .line 14
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__BreadCrumbsView_pspdf__dividerColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->h:I

    .line 15
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__BreadCrumbsView_pspdf__textSize:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->i:F

    .line 17
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__BreadCrumbsView_pspdf__backgroundColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 18
    invoke-virtual {p0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 21
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public final getAttrs()Landroid/util/AttributeSet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->b:Landroid/util/AttributeSet;

    return-object v0
.end method

.method public final getDefStyle()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->c:I

    return v0
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->j:[Ljava/lang/String;

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_3

    .line 2
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->d:Landroid/graphics/Paint;

    .line 3
    iget-object v4, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->l:[F

    aget v4, v4, v2

    .line 4
    iget-object v5, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->j:[Ljava/lang/String;

    aget-object v5, v5, v2

    const-string v6, "titles[position]"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    sget-object v6, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 6
    iget v6, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->i:F

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 7
    iget v6, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->p:I

    if-ne v2, v6, :cond_0

    iget v6, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->g:I

    goto :goto_1

    :cond_0
    iget v6, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->f:I

    :goto_1
    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 8
    iget v6, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->p:I

    if-ne v2, v6, :cond_1

    .line 9
    sget-object v6, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v7, 0x1

    invoke-static {v6, v7}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v6

    goto :goto_2

    .line 13
    :cond_1
    sget-object v6, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 14
    invoke-static {v6, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v6

    .line 15
    :goto_2
    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 24
    iget v6, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->p:I

    if-ne v2, v6, :cond_2

    const/16 v6, 0xff

    goto :goto_3

    :cond_2
    const/16 v6, 0x96

    :goto_3
    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 25
    iget v6, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->m:F

    iget v7, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->i:F

    const/4 v8, 0x2

    int-to-float v8, v8

    div-float/2addr v7, v8

    add-float/2addr v7, v6

    invoke-virtual {p1, v5, v4, v7, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 26
    :cond_3
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget v0, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->k:F

    iget v1, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->m:F

    .line 27
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->d:Landroid/graphics/Paint;

    iget v3, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->h:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 28
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->d:Landroid/graphics/Paint;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 29
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->d:Landroid/graphics/Paint;

    const/16 v3, 0xb4

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 32
    iget v5, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->o:F

    const/16 v2, 0x10

    int-to-float v2, v2

    add-float/2addr v2, v0

    .line 35
    iget-object v8, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->d:Landroid/graphics/Paint;

    move-object v3, p1

    move v4, v0

    move v6, v2

    move v7, v1

    .line 36
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 47
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->o:F

    sub-float v7, v3, v4

    .line 48
    iget-object v8, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->d:Landroid/graphics/Paint;

    move-object v3, p1

    move v4, v2

    move v5, v1

    move v6, v0

    .line 49
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected final onMeasure(II)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingStart()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingEnd()I

    move-result v2

    add-int/2addr v2, v1

    sub-int/2addr v0, v2

    int-to-float v0, v0

    iput v0, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->n:F

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    add-int/2addr v2, v1

    sub-int/2addr v0, v2

    int-to-float v0, v0

    iput v0, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->o:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 3
    iput v0, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->m:F

    .line 4
    iget v0, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->n:F

    div-float v1, v0, v1

    iput v1, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->k:F

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->l:[F

    const/high16 v2, 0x3e800000    # 0.25f

    mul-float v2, v2, v0

    const/4 v3, 0x0

    aput v2, v1, v3

    const/high16 v2, 0x3f400000    # 0.75f

    mul-float v0, v0, v2

    const/4 v2, 0x1

    .line 6
    aput v0, v1, v2

    .line 7
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    return-void
.end method

.method public final setCurrentDocument(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->p:I

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final setTheme(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->e:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->a()V

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

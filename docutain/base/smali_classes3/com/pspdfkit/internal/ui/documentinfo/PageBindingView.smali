.class public final Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0011\u0012\u0008\u0008\u0002\u0010\u0014\u001a\u00020\u0013\u00a2\u0006\u0004\u0008\u0015\u0010\u0016J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016R*\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00068\u0006@FX\u0086\u000e\u00a2\u0006\u0012\n\u0004\u0008\u0008\u0010\t\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;",
        "Landroid/widget/LinearLayout;",
        "",
        "selected",
        "",
        "setSelected",
        "Lcom/pspdfkit/document/PageBinding;",
        "value",
        "d",
        "Lcom/pspdfkit/document/PageBinding;",
        "getPageBinding",
        "()Lcom/pspdfkit/document/PageBinding;",
        "setPageBinding",
        "(Lcom/pspdfkit/document/PageBinding;)V",
        "pageBinding",
        "Landroid/content/Context;",
        "context",
        "Landroid/util/AttributeSet;",
        "attrs",
        "",
        "defStyleAttr",
        "<init>",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# instance fields
.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/widget/TextView;

.field private d:Lcom/pspdfkit/document/PageBinding;

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 7
    sget-object p2, Lcom/pspdfkit/document/PageBinding;->LEFT_EDGE:Lcom/pspdfkit/document/PageBinding;

    iput-object p2, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->d:Lcom/pspdfkit/document/PageBinding;

    const/high16 p2, -0x1000000

    .line 15
    iput p2, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->e:I

    .line 18
    iput p2, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->f:I

    const p2, -0xffff01

    .line 21
    iput p2, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->g:I

    .line 24
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget p2, Lcom/pspdfkit/R$layout;->pspdf__page_binding_view:I

    const/4 p3, 0x1

    invoke-virtual {p1, p2, p0, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 25
    sget p1, Lcom/pspdfkit/R$id;->pspdf__document_info_binding_icon:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.pspdf_\u2026cument_info_binding_icon)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->b:Landroid/widget/ImageView;

    .line 26
    sget p1, Lcom/pspdfkit/R$id;->pspdf__document_info_binding_title:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.pspdf_\u2026ument_info_binding_title)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->c:Landroid/widget/TextView;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 1
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final a()V
    .locals 5

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    iget v0, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->g:I

    goto :goto_0

    .line 8
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->f:I

    .line 11
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 12
    iget v1, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->g:I

    goto :goto_1

    .line 14
    :cond_1
    iget v1, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->e:I

    .line 17
    :goto_1
    iget-object v2, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 19
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->d:Lcom/pspdfkit/document/PageBinding;

    sget-object v2, Lcom/pspdfkit/document/PageBinding;->LEFT_EDGE:Lcom/pspdfkit/document/PageBinding;

    const/4 v3, 0x0

    if-ne v1, v2, :cond_2

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v4, Lcom/pspdfkit/R$string;->pspdf__page_binding_left:I

    .line 21
    invoke-static {v2, v4, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v2

    .line 22
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 23
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->b:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$drawable;->pspdf__document_binding_left:I

    invoke-static {v2, v3, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 25
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v4, Lcom/pspdfkit/R$string;->pspdf__page_binding_right:I

    .line 26
    invoke-static {v2, v4, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v2

    .line 27
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->b:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$drawable;->pspdf__document_binding_right:I

    invoke-static {v2, v3, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->e:I

    .line 2
    iput p2, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->f:I

    .line 3
    iput p3, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->g:I

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->a()V

    return-void
.end method

.method public final getPageBinding()Lcom/pspdfkit/document/PageBinding;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->d:Lcom/pspdfkit/document/PageBinding;

    return-object v0
.end method

.method public final setPageBinding(Lcom/pspdfkit/document/PageBinding;)V
    .locals 1

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->d:Lcom/pspdfkit/document/PageBinding;

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->a()V

    return-void
.end method

.method public setSelected(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->a()V

    return-void
.end method

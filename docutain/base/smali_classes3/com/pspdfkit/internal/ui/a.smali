.class public final Lcom/pspdfkit/internal/ui/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ui/a$a;,
        Lcom/pspdfkit/internal/ui/a$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/ui/a$a;

.field private final b:Landroid/app/Activity;

.field private c:Z

.field private final d:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/pspdfkit/internal/ui/a$a;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ui/a;->d:Ljava/util/HashSet;

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    .line 6
    iput-object p2, p0, Lcom/pspdfkit/internal/ui/a;->a:Lcom/pspdfkit/internal/ui/a$a;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/a;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/a;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 46
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/a;->c:Z

    if-nez v0, :cond_1

    return-void

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0xf06

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ui/a$b;)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/a;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 43
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/a;->f()V

    return-void
.end method

.method public final a(Z)Z
    .locals 4

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x18

    if-lt v0, v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_1

    .line 2
    iget-object v3, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4
    iput-boolean v2, p0, Lcom/pspdfkit/internal/ui/a;->c:Z

    goto :goto_1

    .line 6
    :cond_1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ui/a;->c:Z

    .line 8
    :goto_1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ui/a;->c:Z

    if-eqz p1, :cond_7

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    const/16 v3, 0x21c

    .line 15
    invoke-static {p1, v3}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;I)Z

    move-result p1

    if-nez p1, :cond_3

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-static {p1}, Lcom/pspdfkit/internal/e8;->d(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/high16 p1, 0x8000000

    :goto_3
    const/16 v3, 0x1c

    if-lt v0, v3, :cond_4

    const/4 v2, 0x1

    :cond_4
    if-eqz v2, :cond_6

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/pspdfkit/internal/e8;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    goto :goto_4

    .line 28
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const/4 v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 32
    :cond_6
    :goto_4
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->addFlags(I)V

    .line 33
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    const/16 v0, 0x700

    invoke-virtual {p1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 34
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    goto :goto_5

    .line 38
    :cond_7
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const v0, 0x8000400

    invoke-virtual {p1, v0}, Landroid/view/Window;->clearFlags(I)V

    .line 39
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 41
    :goto_5
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ui/a;->c:Z

    return p1
.end method

.method public final b(Z)V
    .locals 2

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/a;->c:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/high16 v0, 0x8000000

    if-eqz p1, :cond_2

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    const/16 v1, 0x21c

    .line 8
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;I)Z

    move-result p1

    if-nez p1, :cond_1

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-static {p1}, Lcom/pspdfkit/internal/e8;->d(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 10
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0

    .line 13
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/Window;->clearFlags(I)V

    :cond_3
    :goto_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/a;->c:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/pspdfkit/internal/ce;->d(Landroid/app/Activity;)Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ui/a;->c:Z

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/a;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x700

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    return-void
.end method

.method public final e()Lcom/pspdfkit/internal/ui/a$b;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ui/a$b;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ui/a$b;-><init>()V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/a;->d:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/ui/a;->a:Lcom/pspdfkit/internal/ui/a$a;

    check-cast v1, Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/c;->isUserInterfaceVisible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/a;->d()V

    :cond_0
    return-object v0
.end method

.method public final f()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/a;->a:Lcom/pspdfkit/internal/ui/a$a;

    check-cast v0, Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/c;->isUserInterfaceVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/a;->a()V

    return-void
.end method

.method public final onSystemUiVisibilityChange(I)V
    .locals 0

    and-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/a;->a:Lcom/pspdfkit/internal/ui/a$a;

    check-cast p1, Lcom/pspdfkit/internal/ui/c;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/c;->hideUserInterface()V

    goto :goto_0

    .line 3
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/a;->a:Lcom/pspdfkit/internal/ui/a$a;

    check-cast p1, Lcom/pspdfkit/internal/ui/c;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/c;->showUserInterface()V

    :goto_0
    return-void
.end method

.class public final Lcom/pspdfkit/internal/ui/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/DocumentActionListener;


# instance fields
.field private final a:Lcom/pspdfkit/internal/ui/f;


# direct methods
.method public static synthetic $r8$lambda$OnQ21YIt2lSYtV2vOoHeU7FuyAk(Lcom/pspdfkit/internal/ui/b;Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;Lcom/pspdfkit/document/files/EmbeddedFile;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ui/b;->a(Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;Lcom/pspdfkit/document/files/EmbeddedFile;)V

    return-void
.end method

.method public static synthetic $r8$lambda$uhVEhwhXoeLgvlE06HwV0c8AX1A(Lcom/pspdfkit/internal/ui/b;ILandroid/app/Activity;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ui/b;->a(ILandroid/app/Activity;Landroid/net/Uri;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/ui/f;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ui/b;->a:Lcom/pspdfkit/internal/ui/f;

    return-void
.end method

.method private synthetic a(ILandroid/app/Activity;Landroid/net/Uri;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 16
    new-instance v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    iget-object v1, p0, Lcom/pspdfkit/internal/ui/b;->a:Lcom/pspdfkit/internal/ui/f;

    .line 17
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/f;->getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    .line 18
    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->page(I)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object p1

    .line 19
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    .line 21
    invoke-static {p2, v0}, Lcom/pspdfkit/ui/PdfActivityIntentBuilder;->fromUri(Landroid/content/Context;[Landroid/net/Uri;)Lcom/pspdfkit/ui/PdfActivityIntentBuilder;

    move-result-object p3

    .line 22
    invoke-virtual {p3, p1}, Lcom/pspdfkit/ui/PdfActivityIntentBuilder;->configuration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Lcom/pspdfkit/ui/PdfActivityIntentBuilder;

    move-result-object p1

    .line 24
    instance-of p3, p2, Lcom/pspdfkit/ui/PdfActivity;

    if-eqz p3, :cond_0

    .line 25
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/pspdfkit/ui/PdfActivityIntentBuilder;->activityClass(Ljava/lang/Class;)Lcom/pspdfkit/ui/PdfActivityIntentBuilder;

    .line 28
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfActivityIntentBuilder;->build()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;Lcom/pspdfkit/document/files/EmbeddedFile;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->getPageIndex()I

    move-result p1

    invoke-direct {p0, p2, p1}, Lcom/pspdfkit/internal/ui/b;->a(Lcom/pspdfkit/document/files/EmbeddedFile;I)V

    return-void
.end method

.method private a(Lcom/pspdfkit/document/files/EmbeddedFile;I)V
    .locals 2

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/b;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/f;->getHostingActivity()Landroidx/appcompat/app/AppCompatActivity;

    move-result-object v0

    .line 13
    invoke-static {v0, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor;->prepareEmbeddedFileForSharing(Landroid/content/Context;Lcom/pspdfkit/document/files/EmbeddedFile;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 14
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance v1, Lcom/pspdfkit/internal/ui/b$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p2, v0}, Lcom/pspdfkit/internal/ui/b$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ui/b;ILandroid/app/Activity;)V

    .line 15
    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;)Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->isNewWindow()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/b;->a:Lcom/pspdfkit/internal/ui/f;

    iget-object v0, v0, Lcom/pspdfkit/internal/ui/f;->fragment:Lcom/pspdfkit/ui/PdfFragment;

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->getPdfPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 7
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    .line 8
    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getEmbeddedFilesProvider()Lcom/pspdfkit/document/files/EmbeddedFilesProvider;

    move-result-object v0

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->getPdfPath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/pspdfkit/document/files/EmbeddedFilesProvider;->getEmbeddedFileWithFileNameAsync(Ljava/lang/String;Z)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/ui/b$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/ui/b$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ui/b;Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;)V

    .line 10
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return v2

    :cond_2
    :goto_0
    return v1
.end method


# virtual methods
.method public final onExecuteAction(Lcom/pspdfkit/annotations/actions/Action;)Z
    .locals 6

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/ui/b$a;->a:[I

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/Action;->getType()Lcom/pspdfkit/annotations/actions/ActionType;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq v0, v3, :cond_1

    if-eq v0, v2, :cond_0

    return v1

    .line 5
    :cond_0
    check-cast p1, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ui/b;->a(Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;)Z

    move-result p1

    return p1

    .line 6
    :cond_1
    check-cast p1, Lcom/pspdfkit/annotations/actions/NamedAction;

    .line 7
    sget-object v0, Lcom/pspdfkit/internal/ui/b$a;->b:[I

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/NamedAction;->getNamedActionType()Lcom/pspdfkit/annotations/actions/NamedAction$NamedActionType;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    if-eq p1, v3, :cond_5

    const-wide/16 v4, 0x0

    if-eq p1, v2, :cond_4

    const/4 v0, 0x3

    if-eq p1, v0, :cond_3

    const/4 v0, 0x4

    if-eq p1, v0, :cond_3

    const/4 v0, 0x5

    if-eq p1, v0, :cond_2

    goto :goto_1

    .line 22
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/b;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/f;->showSaveAsDialog()V

    goto :goto_0

    .line 23
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/b;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/f;->getViews()Lcom/pspdfkit/internal/yf;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_SEARCH:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    .line 24
    invoke-virtual {p1, v0, v4, v5}, Lcom/pspdfkit/internal/sm;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)Z

    goto :goto_0

    .line 25
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/b;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/f;->getViews()Lcom/pspdfkit/internal/yf;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_OUTLINE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    check-cast p1, Lcom/pspdfkit/internal/sm;

    .line 26
    invoke-virtual {p1, v0, v4, v5}, Lcom/pspdfkit/internal/sm;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)Z

    goto :goto_0

    .line 27
    :cond_5
    iget-object p1, p0, Lcom/pspdfkit/internal/ui/b;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/f;->showPrintDialog()V

    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

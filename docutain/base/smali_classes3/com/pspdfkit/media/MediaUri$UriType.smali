.class public final enum Lcom/pspdfkit/media/MediaUri$UriType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/media/MediaUri;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UriType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/media/MediaUri$UriType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum GALLERY:Lcom/pspdfkit/media/MediaUri$UriType;

.field public static final enum MEDIA:Lcom/pspdfkit/media/MediaUri$UriType;

.field public static final enum OTHER:Lcom/pspdfkit/media/MediaUri$UriType;

.field public static final enum VIDEO_YOUTUBE:Lcom/pspdfkit/media/MediaUri$UriType;

.field public static final enum WEB:Lcom/pspdfkit/media/MediaUri$UriType;

.field private static final synthetic a:[Lcom/pspdfkit/media/MediaUri$UriType;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 1
    new-instance v0, Lcom/pspdfkit/media/MediaUri$UriType;

    const-string v1, "MEDIA"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/media/MediaUri$UriType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/media/MediaUri$UriType;->MEDIA:Lcom/pspdfkit/media/MediaUri$UriType;

    .line 3
    new-instance v1, Lcom/pspdfkit/media/MediaUri$UriType;

    const-string v3, "VIDEO_YOUTUBE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/media/MediaUri$UriType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/media/MediaUri$UriType;->VIDEO_YOUTUBE:Lcom/pspdfkit/media/MediaUri$UriType;

    .line 5
    new-instance v3, Lcom/pspdfkit/media/MediaUri$UriType;

    const-string v5, "GALLERY"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/media/MediaUri$UriType;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/media/MediaUri$UriType;->GALLERY:Lcom/pspdfkit/media/MediaUri$UriType;

    .line 7
    new-instance v5, Lcom/pspdfkit/media/MediaUri$UriType;

    const-string v7, "WEB"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/media/MediaUri$UriType;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/media/MediaUri$UriType;->WEB:Lcom/pspdfkit/media/MediaUri$UriType;

    .line 9
    new-instance v7, Lcom/pspdfkit/media/MediaUri$UriType;

    const-string v9, "OTHER"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/media/MediaUri$UriType;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/media/MediaUri$UriType;->OTHER:Lcom/pspdfkit/media/MediaUri$UriType;

    const/4 v9, 0x5

    new-array v9, v9, [Lcom/pspdfkit/media/MediaUri$UriType;

    aput-object v0, v9, v2

    aput-object v1, v9, v4

    aput-object v3, v9, v6

    aput-object v5, v9, v8

    aput-object v7, v9, v10

    .line 10
    sput-object v9, Lcom/pspdfkit/media/MediaUri$UriType;->a:[Lcom/pspdfkit/media/MediaUri$UriType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/media/MediaUri$UriType;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/media/MediaUri$UriType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/media/MediaUri$UriType;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/media/MediaUri$UriType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/media/MediaUri$UriType;->a:[Lcom/pspdfkit/media/MediaUri$UriType;

    invoke-virtual {v0}, [Lcom/pspdfkit/media/MediaUri$UriType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/media/MediaUri$UriType;

    return-object v0
.end method

.class public Lcom/pspdfkit/media/MediaGalleryView;
.super Landroidx/viewpager/widget/ViewPager;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/media/MediaViewController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/media/MediaGalleryView$GalleryPagerAdapter;,
        Lcom/pspdfkit/media/MediaGalleryView$GalleryElement;
    }
.end annotation


# instance fields
.field b:Lcom/pspdfkit/media/MediaViewListener;

.field private final c:Landroid/content/Context;


# direct methods
.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/media/MediaGalleryView;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/media/MediaGalleryView;->c:Landroid/content/Context;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroidx/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/media/MediaGalleryView;->c:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2}, Landroidx/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/media/MediaGalleryView;->c:Landroid/content/Context;

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 p0, 0x0

    .line 4
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge p0, v2, :cond_1

    .line 5
    invoke-virtual {v1, p0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 6
    new-instance v3, Lcom/pspdfkit/media/MediaGalleryView$GalleryElement;

    const-string v4, "contentURL"

    .line 7
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "caption"

    .line 8
    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_0
    const-string v2, ""

    :goto_1
    invoke-direct {v3, v4, v2}, Lcom/pspdfkit/media/MediaGalleryView$GalleryElement;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 0

    return-void
.end method

.method public setMediaViewListener(Lcom/pspdfkit/media/MediaViewListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/media/MediaGalleryView;->b:Lcom/pspdfkit/media/MediaViewListener;

    return-void
.end method

.method public start(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const-string p1, "localhost/"

    const/4 v0, 0x0

    .line 1
    :try_start_0
    invoke-virtual {p2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ""

    .line 2
    invoke-virtual {p2, p1, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 3
    iget-object p2, p0, Lcom/pspdfkit/media/MediaGalleryView;->c:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string p1, "file://"

    .line 4
    invoke-virtual {p2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 5
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 7
    new-instance p2, Ljava/io/FileInputStream;

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v0, p2

    :cond_1
    :goto_0
    if-nez v0, :cond_3

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/media/MediaGalleryView;->b:Lcom/pspdfkit/media/MediaViewListener;

    if-eqz p1, :cond_2

    .line 14
    invoke-interface {p1}, Lcom/pspdfkit/media/MediaViewListener;->onContentError()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :cond_2
    invoke-static {v0}, Lcom/pspdfkit/internal/fv;->a(Ljava/io/InputStream;)V

    return-void

    .line 34
    :cond_3
    :try_start_1
    new-instance p1, Ljava/io/BufferedReader;

    new-instance p2, Ljava/io/InputStreamReader;

    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {p2, v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {p1, p2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 36
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_4

    .line 39
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 42
    :cond_4
    invoke-virtual {p1}, Ljava/io/BufferedReader;->close()V

    .line 43
    new-instance p1, Lcom/pspdfkit/media/MediaGalleryView$GalleryPagerAdapter;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/pspdfkit/media/MediaGalleryView;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/media/MediaGalleryView$GalleryPagerAdapter;-><init>(Lcom/pspdfkit/media/MediaGalleryView;Ljava/util/List;)V

    invoke-virtual {p0, p1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 44
    iget-object p1, p0, Lcom/pspdfkit/media/MediaGalleryView;->b:Lcom/pspdfkit/media/MediaViewListener;

    if-eqz p1, :cond_5

    invoke-interface {p1}, Lcom/pspdfkit/media/MediaViewListener;->onContentReady()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception p1

    goto :goto_3

    .line 46
    :catch_0
    :try_start_2
    iget-object p1, p0, Lcom/pspdfkit/media/MediaGalleryView;->b:Lcom/pspdfkit/media/MediaViewListener;

    if-eqz p1, :cond_5

    invoke-interface {p1}, Lcom/pspdfkit/media/MediaViewListener;->onContentError()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 48
    :cond_5
    :goto_2
    invoke-static {v0}, Lcom/pspdfkit/internal/fv;->a(Ljava/io/InputStream;)V

    return-void

    :goto_3
    invoke-static {v0}, Lcom/pspdfkit/internal/fv;->a(Ljava/io/InputStream;)V

    .line 49
    throw p1
.end method

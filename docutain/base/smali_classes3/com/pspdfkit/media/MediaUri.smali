.class public Lcom/pspdfkit/media/MediaUri;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/media/MediaUri$UriType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/media/MediaUri;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/pspdfkit/media/MediaUri$UriType;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/media/MediaUri$1;

    invoke-direct {v0}, Lcom/pspdfkit/media/MediaUri$1;-><init>()V

    sput-object v0, Lcom/pspdfkit/media/MediaUri;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 7
    sget-object v0, Lcom/pspdfkit/media/MediaUri$UriType;->OTHER:Lcom/pspdfkit/media/MediaUri$UriType;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/pspdfkit/media/MediaUri$UriType;->values()[Lcom/pspdfkit/media/MediaUri$UriType;

    move-result-object v1

    aget-object v0, v1, v0

    :goto_0
    iput-object v0, p0, Lcom/pspdfkit/media/MediaUri;->a:Lcom/pspdfkit/media/MediaUri$UriType;

    .line 8
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/media/MediaUri;->b:Ljava/lang/String;

    .line 9
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/media/MediaUri;->c:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/media/MediaUri$UriType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/media/MediaUri;->a:Lcom/pspdfkit/media/MediaUri$UriType;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/media/MediaUri;->c:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/media/MediaUri;->b:Ljava/lang/String;

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/pspdfkit/media/MediaUri;
    .locals 24

    move-object/from16 v0, p0

    .line 1
    sget-object v1, Lcom/pspdfkit/media/MediaUri$UriType;->OTHER:Lcom/pspdfkit/media/MediaUri$UriType;

    const-string v2, "pspdfkit://"

    .line 6
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    const-string v4, ""

    if-eqz v3, :cond_8

    .line 7
    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    const-string v0, "UTF-8"

    .line 11
    invoke-static {v1, v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-array v3, v2, [Ljava/lang/Object;

    const-string v5, "PSPDFKit.MediaUri"

    const-string v6, "Can\'t decode media Uri."

    .line 13
    invoke-static {v5, v0, v6, v3}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    :goto_0
    invoke-static {v1}, Lcom/pspdfkit/media/MediaLinkUtils;->extractOptionsAndPath(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 16
    aget-object v1, v0, v2

    if-nez v1, :cond_0

    move-object v1, v4

    :cond_0
    const/4 v3, 0x1

    .line 17
    aget-object v0, v0, v3

    if-nez v0, :cond_1

    move-object v0, v4

    :cond_1
    const-string v4, "youtube.com/"

    .line 20
    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 21
    sget-object v2, Lcom/pspdfkit/media/MediaUri$UriType;->VIDEO_YOUTUBE:Lcom/pspdfkit/media/MediaUri$UriType;

    :goto_1
    move-object v4, v1

    move-object v1, v2

    goto :goto_4

    :cond_2
    const-string v4, ".gallery"

    .line 24
    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 25
    sget-object v2, Lcom/pspdfkit/media/MediaUri$UriType;->GALLERY:Lcom/pspdfkit/media/MediaUri$UriType;

    goto :goto_1

    :cond_3
    const/16 v4, 0x13

    const-string v5, ".3gp"

    const-string v6, ".mp4"

    const-string v7, ".ts"

    const-string v8, ".webm"

    const-string v9, ".mkv"

    const-string v10, ".m3u8"

    const-string v11, ".mov"

    const-string v12, ".avi"

    const-string v13, ".mpg"

    const-string v14, ".m4v"

    const-string v15, ".bmp"

    const-string v16, ".gif"

    const-string v17, ".jpeg"

    const-string v18, ".png"

    const-string v19, ".webp"

    const-string v20, ".mp3"

    const-string v21, ".flac"

    const-string v22, ".ota"

    const-string v23, ".ogg"

    .line 26
    filled-new-array/range {v5 .. v23}, [Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v4, :cond_5

    .line 30
    aget-object v7, v5, v6

    .line 31
    invoke-virtual {v0, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v2, 0x1

    goto :goto_3

    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_5
    :goto_3
    if-eqz v2, :cond_6

    .line 32
    sget-object v2, Lcom/pspdfkit/media/MediaUri$UriType;->MEDIA:Lcom/pspdfkit/media/MediaUri$UriType;

    goto :goto_1

    :cond_6
    const-string v2, "localhost"

    .line 33
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 34
    sget-object v2, Lcom/pspdfkit/media/MediaUri$UriType;->OTHER:Lcom/pspdfkit/media/MediaUri$UriType;

    goto :goto_1

    .line 36
    :cond_7
    sget-object v2, Lcom/pspdfkit/media/MediaUri$UriType;->WEB:Lcom/pspdfkit/media/MediaUri$UriType;

    goto :goto_1

    .line 40
    :cond_8
    :goto_4
    new-instance v2, Lcom/pspdfkit/media/MediaUri;

    invoke-direct {v2, v1, v0, v4}, Lcom/pspdfkit/media/MediaUri;-><init>(Lcom/pspdfkit/media/MediaUri$UriType;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 1
    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/media/MediaUri;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    return v1

    .line 3
    :cond_1
    check-cast p1, Lcom/pspdfkit/media/MediaUri;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/media/MediaUri;->a:Lcom/pspdfkit/media/MediaUri$UriType;

    iget-object v2, p1, Lcom/pspdfkit/media/MediaUri;->a:Lcom/pspdfkit/media/MediaUri$UriType;

    if-eq v0, v2, :cond_2

    return v1

    .line 6
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/media/MediaUri;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/pspdfkit/media/MediaUri;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    return v1

    .line 7
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/media/MediaUri;->c:Ljava/lang/String;

    iget-object p1, p1, Lcom/pspdfkit/media/MediaUri;->c:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getFileUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/media/MediaUri;->a:Lcom/pspdfkit/media/MediaUri$UriType;

    sget-object v1, Lcom/pspdfkit/media/MediaUri$UriType;->MEDIA:Lcom/pspdfkit/media/MediaUri$UriType;

    if-ne v0, v1, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/media/MediaUri;->getUri()Ljava/lang/String;

    move-result-object v0

    const-string v1, "localhost/"

    .line 3
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4
    invoke-static {p1}, Lcom/pspdfkit/media/AssetsContentProvider;->getAuthority(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object p1

    .line 5
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p1

    const-string v2, ""

    .line 6
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p1

    .line 7
    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    return-object p1

    .line 11
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/media/MediaUri;->getParsedUri()Landroid/net/Uri;

    move-result-object p1

    return-object p1
.end method

.method public getOptions()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/media/MediaUri;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getParsedUri()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/media/MediaUri;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/pspdfkit/media/MediaUri$UriType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/media/MediaUri;->a:Lcom/pspdfkit/media/MediaUri$UriType;

    return-object v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/media/MediaUri;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoSettingsFromOptions()Lcom/pspdfkit/media/MediaLinkUtils$VideoSettings;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/media/MediaUri;->getOptions()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/media/MediaLinkUtils;->getVideoSettingsFromOptions(Ljava/lang/String;)Lcom/pspdfkit/media/MediaLinkUtils$VideoSettings;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/media/MediaUri;->a:Lcom/pspdfkit/media/MediaUri$UriType;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/media/MediaUri;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/media/MediaUri;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public isVideoUri()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/media/MediaUri;->getType()Lcom/pspdfkit/media/MediaUri$UriType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/media/MediaUri$UriType;->MEDIA:Lcom/pspdfkit/media/MediaUri$UriType;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/pspdfkit/media/MediaUri;->getType()Lcom/pspdfkit/media/MediaUri$UriType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/media/MediaUri$UriType;->VIDEO_YOUTUBE:Lcom/pspdfkit/media/MediaUri$UriType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaUri{type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/media/MediaUri;->a:Lcom/pspdfkit/media/MediaUri$UriType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", options=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/media/MediaUri;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', uri=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/media/MediaUri;->c:Ljava/lang/String;

    const-string v2, "\'}"

    .line 2
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/rg;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/media/MediaUri;->getType()Lcom/pspdfkit/media/MediaUri$UriType;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/media/MediaUri;->getOptions()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/media/MediaUri;->getUri()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

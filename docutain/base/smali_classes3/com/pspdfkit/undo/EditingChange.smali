.class public Lcom/pspdfkit/undo/EditingChange;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final affectedPageIndex:I

.field private final editingOperation:Lcom/pspdfkit/undo/EditingOperation;

.field private final pageIndexDestination:I

.field private final pageReferenceSourceIndex:I


# direct methods
.method public constructor <init>(Lcom/pspdfkit/undo/EditingOperation;III)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "editingOperation"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-ltz p2, :cond_1

    if-ltz p3, :cond_0

    .line 9
    iput-object p1, p0, Lcom/pspdfkit/undo/EditingChange;->editingOperation:Lcom/pspdfkit/undo/EditingOperation;

    .line 10
    iput p2, p0, Lcom/pspdfkit/undo/EditingChange;->affectedPageIndex:I

    .line 11
    iput p3, p0, Lcom/pspdfkit/undo/EditingChange;->pageIndexDestination:I

    .line 12
    iput p4, p0, Lcom/pspdfkit/undo/EditingChange;->pageReferenceSourceIndex:I

    return-void

    .line 13
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p4, "Invalid page index destination "

    invoke-direct {p2, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 14
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    const-string p4, "Invalid affected page index "

    invoke-direct {p3, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public getAffectedPageIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/undo/EditingChange;->affectedPageIndex:I

    return v0
.end method

.method public getEditingOperation()Lcom/pspdfkit/undo/EditingOperation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/undo/EditingChange;->editingOperation:Lcom/pspdfkit/undo/EditingOperation;

    return-object v0
.end method

.method public getPageIndexDestination()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/undo/EditingChange;->pageIndexDestination:I

    return v0
.end method

.method public getPageReferenceSourceIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/undo/EditingChange;->pageReferenceSourceIndex:I

    return v0
.end method

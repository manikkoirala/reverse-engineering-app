.class public final enum Lcom/pspdfkit/undo/EditingOperation;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/undo/EditingOperation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/undo/EditingOperation;

.field public static final enum INSERT:Lcom/pspdfkit/undo/EditingOperation;

.field public static final enum INSERTREFERENCE:Lcom/pspdfkit/undo/EditingOperation;

.field public static final enum MOVE:Lcom/pspdfkit/undo/EditingOperation;

.field public static final enum REMOVE:Lcom/pspdfkit/undo/EditingOperation;

.field public static final enum ROTATE:Lcom/pspdfkit/undo/EditingOperation;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 1
    new-instance v0, Lcom/pspdfkit/undo/EditingOperation;

    const-string v1, "REMOVE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/undo/EditingOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/undo/EditingOperation;->REMOVE:Lcom/pspdfkit/undo/EditingOperation;

    .line 6
    new-instance v1, Lcom/pspdfkit/undo/EditingOperation;

    const-string v3, "MOVE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/undo/EditingOperation;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/undo/EditingOperation;->MOVE:Lcom/pspdfkit/undo/EditingOperation;

    .line 8
    new-instance v3, Lcom/pspdfkit/undo/EditingOperation;

    const-string v5, "INSERT"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/undo/EditingOperation;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/undo/EditingOperation;->INSERT:Lcom/pspdfkit/undo/EditingOperation;

    .line 10
    new-instance v5, Lcom/pspdfkit/undo/EditingOperation;

    const-string v7, "ROTATE"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/undo/EditingOperation;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/undo/EditingOperation;->ROTATE:Lcom/pspdfkit/undo/EditingOperation;

    .line 12
    new-instance v7, Lcom/pspdfkit/undo/EditingOperation;

    const-string v9, "INSERTREFERENCE"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/undo/EditingOperation;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/undo/EditingOperation;->INSERTREFERENCE:Lcom/pspdfkit/undo/EditingOperation;

    const/4 v9, 0x5

    new-array v9, v9, [Lcom/pspdfkit/undo/EditingOperation;

    aput-object v0, v9, v2

    aput-object v1, v9, v4

    aput-object v3, v9, v6

    aput-object v5, v9, v8

    aput-object v7, v9, v10

    .line 13
    sput-object v9, Lcom/pspdfkit/undo/EditingOperation;->$VALUES:[Lcom/pspdfkit/undo/EditingOperation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/undo/EditingOperation;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/undo/EditingOperation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/undo/EditingOperation;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/undo/EditingOperation;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/undo/EditingOperation;->$VALUES:[Lcom/pspdfkit/undo/EditingOperation;

    invoke-virtual {v0}, [Lcom/pspdfkit/undo/EditingOperation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/undo/EditingOperation;

    return-object v0
.end method

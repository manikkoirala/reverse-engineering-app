.class public abstract Lcom/pspdfkit/signatures/SignatureMetadata;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/signatures/SignatureMetadata$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract signatureLocation()Ljava/lang/String;
.end method

.method public abstract signatureReason()Ljava/lang/String;
.end method

.method public abstract signersName()Ljava/lang/String;
.end method

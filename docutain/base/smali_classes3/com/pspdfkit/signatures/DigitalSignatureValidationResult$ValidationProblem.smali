.class public final enum Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ValidationProblem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

.field public static final enum CERTIFICATE_CHAIN_FAILURE:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

.field public static final enum COULD_NOT_CHECK_REVOCATION_STATUS:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

.field public static final enum DOCUMENT_INTEGRITY_FAILURE:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

.field public static final enum EMPTY_TRUSTED_KEYSTORE:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

.field public static final enum SELF_SIGNED:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 1
    new-instance v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    const-string v1, "EMPTY_TRUSTED_KEYSTORE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;->EMPTY_TRUSTED_KEYSTORE:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    .line 3
    new-instance v1, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    const-string v3, "CERTIFICATE_CHAIN_FAILURE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;->CERTIFICATE_CHAIN_FAILURE:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    .line 5
    new-instance v3, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    const-string v5, "DOCUMENT_INTEGRITY_FAILURE"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;->DOCUMENT_INTEGRITY_FAILURE:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    .line 7
    new-instance v5, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    const-string v7, "SELF_SIGNED"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;->SELF_SIGNED:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    .line 9
    new-instance v7, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    const-string v9, "COULD_NOT_CHECK_REVOCATION_STATUS"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;->COULD_NOT_CHECK_REVOCATION_STATUS:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    const/4 v9, 0x5

    new-array v9, v9, [Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    aput-object v0, v9, v2

    aput-object v1, v9, v4

    aput-object v3, v9, v6

    aput-object v5, v9, v8

    aput-object v7, v9, v10

    .line 10
    sput-object v9, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;->$VALUES:[Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;->$VALUES:[Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    invoke-virtual {v0}, [Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    return-object v0
.end method


# virtual methods
.method public getLocalizedDescription(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .line 1
    sget-object v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$2;->$SwitchMap$com$pspdfkit$signatures$DigitalSignatureValidationResult$ValidationProblem:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 p1, 0x5

    if-ne v0, p1, :cond_0

    const-string p1, "todo"

    return-object p1

    .line 17
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Missing localization for state."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 19
    :cond_1
    sget v0, Lcom/pspdfkit/R$string;->pspdf__digital_signature_integrity_self_signed:I

    .line 20
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 21
    :cond_2
    sget v0, Lcom/pspdfkit/R$string;->pspdf__digital_signature_error_integrity_check:I

    .line 22
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 23
    :cond_3
    sget v0, Lcom/pspdfkit/R$string;->pspdf__digital_signature_error_certificate_chain_invalid:I

    .line 24
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 25
    :cond_4
    sget v0, Lcom/pspdfkit/R$string;->pspdf__digital_signature_error_certificate_chain_not_provided:I

    .line 26
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

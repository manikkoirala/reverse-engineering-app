.class abstract Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;
.super Lcom/pspdfkit/signatures/BiometricSignatureData;
.source "SourceFile"


# instance fields
.field private final getInputMethod:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

.field private final getTouchRadius:Ljava/lang/Float;

.field private final mutablePressurePoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final mutableTimePoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/lang/Float;Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/Float;",
            "Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/signatures/BiometricSignatureData;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->mutablePressurePoints:Ljava/util/List;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->mutableTimePoints:Ljava/util/List;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->getTouchRadius:Ljava/lang/Float;

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->getInputMethod:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/signatures/BiometricSignatureData;

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    .line 2
    check-cast p1, Lcom/pspdfkit/signatures/BiometricSignatureData;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->mutablePressurePoints:Ljava/util/List;

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lcom/pspdfkit/signatures/BiometricSignatureData;->mutablePressurePoints()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_5

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/BiometricSignatureData;->mutablePressurePoints()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->mutableTimePoints:Ljava/util/List;

    if-nez v1, :cond_2

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/BiometricSignatureData;->mutableTimePoints()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_5

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/BiometricSignatureData;->mutableTimePoints()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_1
    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->getTouchRadius:Ljava/lang/Float;

    if-nez v1, :cond_3

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/BiometricSignatureData;->getTouchRadius()Ljava/lang/Float;

    move-result-object v1

    if-nez v1, :cond_5

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/BiometricSignatureData;->getTouchRadius()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_2
    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->getInputMethod:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    if-nez v1, :cond_4

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/BiometricSignatureData;->getInputMethod()Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    move-result-object p1

    if-nez p1, :cond_5

    goto :goto_3

    :cond_4
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/BiometricSignatureData;->getInputMethod()Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_6
    return v2
.end method

.method public getInputMethod()Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->getInputMethod:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    return-object v0
.end method

.method public getTouchRadius()Ljava/lang/Float;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->getTouchRadius:Ljava/lang/Float;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->mutablePressurePoints:Ljava/util/List;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    :goto_0
    const v2, 0xf4243

    xor-int/2addr v0, v2

    mul-int v0, v0, v2

    .line 3
    iget-object v3, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->mutableTimePoints:Ljava/util/List;

    if-nez v3, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v3

    :goto_1
    xor-int/2addr v0, v3

    mul-int v0, v0, v2

    .line 5
    iget-object v3, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->getTouchRadius:Ljava/lang/Float;

    if-nez v3, :cond_2

    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Ljava/lang/Float;->hashCode()I

    move-result v3

    :goto_2
    xor-int/2addr v0, v3

    mul-int v0, v0, v2

    .line 7
    iget-object v2, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->getInputMethod:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    if-nez v2, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_3
    xor-int/2addr v0, v1

    return v0
.end method

.method mutablePressurePoints()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->mutablePressurePoints:Ljava/util/List;

    return-object v0
.end method

.method mutableTimePoints()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->mutableTimePoints:Ljava/util/List;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BiometricSignatureData{mutablePressurePoints="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->mutablePressurePoints:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mutableTimePoints="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->mutableTimePoints:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getTouchRadius="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->getTouchRadius:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getInputMethod="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_BiometricSignatureData;->getInputMethod:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

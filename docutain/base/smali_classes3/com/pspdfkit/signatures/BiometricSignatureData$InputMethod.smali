.class public final enum Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/signatures/BiometricSignatureData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InputMethod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

.field public static final enum APPLE_PENCIL:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

.field public static final enum FINGER:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

.field public static final enum MOUSE:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

.field public static final enum STYLUS:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 1
    new-instance v0, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    const-string v1, "FINGER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->FINGER:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    .line 3
    new-instance v1, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    const-string v3, "STYLUS"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->STYLUS:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    .line 5
    new-instance v3, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    const-string v5, "MOUSE"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->MOUSE:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    .line 7
    new-instance v5, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    const-string v7, "APPLE_PENCIL"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->APPLE_PENCIL:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    const/4 v7, 0x4

    new-array v7, v7, [Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    .line 8
    sput-object v7, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->$VALUES:[Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->$VALUES:[Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    invoke-virtual {v0}, [Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    return-object v0
.end method

.class final Lcom/pspdfkit/signatures/AutoValue_SignatureAppearance;
.super Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/signatures/AutoValue_SignatureAppearance;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/signatures/AutoValue_SignatureAppearance$1;

    invoke-direct {v0}, Lcom/pspdfkit/signatures/AutoValue_SignatureAppearance$1;-><init>()V

    sput-object v0, Lcom/pspdfkit/signatures/AutoValue_SignatureAppearance;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;ZZZZLcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;ZZ)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p9}, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;-><init>(Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;ZZZZLcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;ZZ)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureAppearanceMode()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignerName()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignDate()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignatureReason()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignatureLocation()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureGraphic()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureWatermark()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->reuseExistingSignatureAppearanceStream()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showWatermark()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.class public final Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;,
        Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;,
        Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final certificateChainValidationErrorMessage:Ljava/lang/String;

.field private final certificateChainValidationStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field private final documentIntegrityStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;

.field private final problems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;",
            ">;"
        }
    .end annotation
.end field

.field private final status:Lcom/pspdfkit/signatures/ValidationStatus;

.field private final wasModified:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$1;

    invoke-direct {v0}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$1;-><init>()V

    sput-object v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {}, Lcom/pspdfkit/signatures/ValidationStatus;->values()[Lcom/pspdfkit/signatures/ValidationStatus;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->status:Lcom/pspdfkit/signatures/ValidationStatus;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->problems:Ljava/util/List;

    .line 39
    const-class v1, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 40
    invoke-static {}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;->values()[Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->documentIntegrityStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;

    .line 41
    invoke-static {}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->values()[Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationErrorMessage:Ljava/lang/String;

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->wasModified:Z

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/jni/NativeSignatureValidationResult;Z)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeSignatureValidationResult;->getStatus()Lcom/pspdfkit/internal/jni/NativeSignatureValidationStatus;

    move-result-object v0

    const-class v1, Lcom/pspdfkit/signatures/ValidationStatus;

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/signatures/ValidationStatus;

    .line 3
    sget-object v1, Lcom/pspdfkit/signatures/ValidationStatus;->ERROR:Lcom/pspdfkit/signatures/ValidationStatus;

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->shouldDemoteValidationErrorToValidationWarning(Lcom/pspdfkit/internal/jni/NativeSignatureValidationResult;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4
    sget-object v0, Lcom/pspdfkit/signatures/ValidationStatus;->WARNING:Lcom/pspdfkit/signatures/ValidationStatus;

    iput-object v0, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->status:Lcom/pspdfkit/signatures/ValidationStatus;

    goto :goto_0

    .line 6
    :cond_0
    iput-object v0, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->status:Lcom/pspdfkit/signatures/ValidationStatus;

    .line 9
    :goto_0
    iput-boolean p2, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->wasModified:Z

    .line 11
    new-instance p2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeSignatureValidationResult;->getProblems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->problems:Ljava/util/List;

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeSignatureValidationResult;->getProblems()Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/jni/NativeSignatureValidationProblem;

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->problems:Ljava/util/List;

    const-class v2, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    invoke-static {v2, v0}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 17
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeSignatureValidationResult;->getDocumentIntegrityStatus()Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    move-result-object p2

    const-class v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;

    invoke-static {v0, p2}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;

    iput-object p2, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->documentIntegrityStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;

    .line 19
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeSignatureValidationResult;->getCertificateChainValidationStatus()Lcom/pspdfkit/internal/jni/NativeCertificateChainValidationStatus;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 22
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeCertificateChainValidationStatus;->getOverallStatus()Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    move-result-object p2

    .line 23
    const-class v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    invoke-static {v0, p2}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    iput-object p2, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 25
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeCertificateChainValidationStatus;->getRawErrorMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationErrorMessage:Ljava/lang/String;

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    .line 27
    iput-object p1, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 28
    iput-object p1, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationErrorMessage:Ljava/lang/String;

    :goto_2
    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/signatures/ValidationStatus;Ljava/util/List;Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/signatures/ValidationStatus;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;",
            ">;",
            "Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;",
            "Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->status:Lcom/pspdfkit/signatures/ValidationStatus;

    .line 31
    iput-object p2, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->problems:Ljava/util/List;

    .line 32
    iput-object p3, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->documentIntegrityStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;

    .line 33
    iput-object p4, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 34
    iput-object p5, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationErrorMessage:Ljava/lang/String;

    .line 35
    iput-boolean p6, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->wasModified:Z

    return-void
.end method

.method private shouldDemoteValidationErrorToValidationWarning(Lcom/pspdfkit/internal/jni/NativeSignatureValidationResult;)Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeSignatureValidationResult;->getDocumentIntegrityStatus()Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    move-result-object v0

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;->OK:Lcom/pspdfkit/internal/jni/NativeDocumentIntegrityStatus;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_2

    .line 3
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->UNTRUSTED:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeSignatureValidationResult;->getCertificateChainValidationStatus()Lcom/pspdfkit/internal/jni/NativeCertificateChainValidationStatus;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeCertificateChainValidationStatus;->getOverallStatus()Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    .line 9
    :goto_0
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->OK:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    if-eq p1, v1, :cond_1

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;->OK_BUT_SELF_SIGNED:Lcom/pspdfkit/internal/jni/NativeCertificateValidationStatus;

    if-eq p1, v1, :cond_1

    if-ne p1, v0, :cond_2

    :cond_1
    const/4 v2, 0x1

    :cond_2
    return v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCertificateChainValidationErrorMessage()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getCertificateChainValidationStatus()Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    return-object v0
.end method

.method public getDocumentIntegrityStatus()Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->documentIntegrityStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;

    return-object v0
.end method

.method public getProblems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$ValidationProblem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->problems:Ljava/util/List;

    return-object v0
.end method

.method public getValidationStatus()Lcom/pspdfkit/signatures/ValidationStatus;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->status:Lcom/pspdfkit/signatures/ValidationStatus;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DigitalSignatureValidationResult{status="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->status:Lcom/pspdfkit/signatures/ValidationStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", problems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->problems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", documentIntegrityStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->documentIntegrityStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", certificateChainValidationStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", certificateChainValidationErrorMessage=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationErrorMessage:Ljava/lang/String;

    const-string v2, "\'}"

    .line 2
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/rg;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public wasDocumentModified()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->wasModified:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->status:Lcom/pspdfkit/signatures/ValidationStatus;

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->problems:Ljava/util/List;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 3
    iget-object p2, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->documentIntegrityStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$DocumentIntegrityStatus;

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 5
    iget-object p2, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationStatus:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    if-nez p2, :cond_0

    const/4 p2, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    .line 6
    :goto_0
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 8
    iget-object p2, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->certificateChainValidationErrorMessage:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 9
    iget-boolean p2, p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;->wasModified:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    return-void
.end method

.class final Lcom/pspdfkit/signatures/AutoValue_Signature;
.super Lcom/pspdfkit/signatures/$AutoValue_Signature;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/signatures/AutoValue_Signature;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/signatures/AutoValue_Signature$1;

    invoke-direct {v0}, Lcom/pspdfkit/signatures/AutoValue_Signature$1;-><init>()V

    sput-object v0, Lcom/pspdfkit/signatures/AutoValue_Signature;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/annotations/AnnotationType;JIFLjava/util/List;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;FILandroid/graphics/RectF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            "JIF",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/signatures/BiometricSignatureData;",
            "FI",
            "Landroid/graphics/RectF;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct/range {p0 .. p11}, Lcom/pspdfkit/signatures/$AutoValue_Signature;-><init>(Lcom/pspdfkit/annotations/AnnotationType;JIFLjava/util/List;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;FILandroid/graphics/RectF;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_Signature;->getAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_Signature;->getId()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_Signature;->getInkColor()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_Signature;->getLineWidth()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_Signature;->getLines()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_Signature;->getSignerIdentifier()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 10
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_Signature;->getSignerIdentifier()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 12
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_Signature;->getBiometricData()Lcom/pspdfkit/signatures/BiometricSignatureData;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 13
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_Signature;->getSignatureDrawWidthRatio()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 14
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_Signature;->getBitmapIdentifier()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 15
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/$AutoValue_Signature;->getStampRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

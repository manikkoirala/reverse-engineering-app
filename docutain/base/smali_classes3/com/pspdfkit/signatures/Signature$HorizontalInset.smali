.class Lcom/pspdfkit/signatures/Signature$HorizontalInset;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/signatures/Signature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HorizontalInset"
.end annotation


# instance fields
.field private final insetLeft:I

.field private final insetRight:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/pspdfkit/signatures/Signature$HorizontalInset;->insetLeft:I

    .line 3
    iput p2, p0, Lcom/pspdfkit/signatures/Signature$HorizontalInset;->insetRight:I

    return-void
.end method


# virtual methods
.method getInsetLeft()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/signatures/Signature$HorizontalInset;->insetLeft:I

    return v0
.end method

.method getInsetRight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/signatures/Signature$HorizontalInset;->insetRight:I

    return v0
.end method

.method getTotalInset()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature$HorizontalInset;->getInsetLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature$HorizontalInset;->getInsetRight()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

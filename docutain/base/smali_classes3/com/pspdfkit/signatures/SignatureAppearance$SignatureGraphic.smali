.class public final Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/signatures/SignatureAppearance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SignatureGraphic"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final dataProvider:Lcom/pspdfkit/document/providers/DataProvider;

.field private final isBitmap:Z

.field private final uri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic$1;

    invoke-direct {v0}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic$1;-><init>()V

    sput-object v0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->isBitmap:Z

    .line 17
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->uri:Landroid/net/Uri;

    .line 18
    const-class v0, Lcom/pspdfkit/document/providers/DataProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/document/providers/DataProvider;

    iput-object p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->dataProvider:Lcom/pspdfkit/document/providers/DataProvider;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(ZLandroid/net/Uri;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "uri"

    .line 2
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-boolean p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->isBitmap:Z

    .line 4
    iput-object p2, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->uri:Landroid/net/Uri;

    const/4 p1, 0x0

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->dataProvider:Lcom/pspdfkit/document/providers/DataProvider;

    return-void
.end method

.method private constructor <init>(ZLcom/pspdfkit/document/providers/DataProvider;)V
    .locals 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "dataProvider"

    .line 7
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    instance-of v0, p2, Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 11
    iput-boolean p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->isBitmap:Z

    const/4 p1, 0x0

    .line 12
    iput-object p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->uri:Landroid/net/Uri;

    .line 13
    iput-object p2, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->dataProvider:Lcom/pspdfkit/document/providers/DataProvider;

    return-void

    .line 14
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "You need to pass in a parcelable data provider."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static fromBitmap(Landroid/net/Uri;)Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;-><init>(ZLandroid/net/Uri;)V

    return-object v0
.end method

.method public static fromBitmap(Lcom/pspdfkit/document/providers/DataProvider;)Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;
    .locals 2

    .line 2
    new-instance v0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;-><init>(ZLcom/pspdfkit/document/providers/DataProvider;)V

    return-object v0
.end method

.method public static fromPdf(Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;
    .locals 2

    const-string v0, "source"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object p0

    .line 56
    new-instance v0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    invoke-direct {v0, v1, p0}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;-><init>(ZLcom/pspdfkit/document/providers/DataProvider;)V

    return-object v0

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 58
    new-instance v0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;-><init>(ZLandroid/net/Uri;)V

    return-object v0

    .line 60
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Passed in an invalid document source."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2
    :cond_1
    check-cast p1, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    .line 3
    iget-boolean v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->isBitmap:Z

    iget-boolean v3, p1, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->isBitmap:Z

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->uri:Landroid/net/Uri;

    iget-object v3, p1, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->uri:Landroid/net/Uri;

    .line 4
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->dataProvider:Lcom/pspdfkit/document/providers/DataProvider;

    iget-object p1, p1, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->dataProvider:Lcom/pspdfkit/document/providers/DataProvider;

    .line 5
    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->dataProvider:Lcom/pspdfkit/document/providers/DataProvider;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->uri:Landroid/net/Uri;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 1
    iget-boolean v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->isBitmap:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->uri:Landroid/net/Uri;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->dataProvider:Lcom/pspdfkit/document/providers/DataProvider;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isBitmap()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->isBitmap:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SignatureGraphic{isBitmap="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->isBitmap:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", dataProvider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->dataProvider:Lcom/pspdfkit/document/providers/DataProvider;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->isBitmap:Z

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->uri:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->dataProvider:Lcom/pspdfkit/document/providers/DataProvider;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

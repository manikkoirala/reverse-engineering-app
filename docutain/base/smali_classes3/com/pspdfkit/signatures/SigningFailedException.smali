.class public Lcom/pspdfkit/signatures/SigningFailedException;
.super Lcom/pspdfkit/exceptions/PSPDFKitException;
.source "SourceFile"


# instance fields
.field private final diagnosticMessage:Ljava/lang/String;

.field private final reason:Lcom/pspdfkit/signatures/SigningStatus;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/signatures/SigningStatus;Ljava/lang/String;)V
    .locals 2

    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Signing failed: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    .line 8
    iput-object p1, p0, Lcom/pspdfkit/signatures/SigningFailedException;->reason:Lcom/pspdfkit/signatures/SigningStatus;

    .line 9
    iput-object p2, p0, Lcom/pspdfkit/signatures/SigningFailedException;->diagnosticMessage:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/signatures/SigningFailedException;->reason:Lcom/pspdfkit/signatures/SigningStatus;

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/signatures/SigningFailedException;->diagnosticMessage:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0

    .line 4
    invoke-direct {p0, p1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/Throwable;)V

    const/4 p1, 0x0

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/signatures/SigningFailedException;->reason:Lcom/pspdfkit/signatures/SigningStatus;

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/signatures/SigningFailedException;->diagnosticMessage:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getDiagnosticMessage()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/SigningFailedException;->diagnosticMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getReason()Lcom/pspdfkit/signatures/SigningStatus;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/SigningFailedException;->reason:Lcom/pspdfkit/signatures/SigningStatus;

    return-object v0
.end method

.class public final enum Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/signatures/DigitalSignatureInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReferenceTransformMethod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

.field public static final enum DOCMDP:Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

.field public static final enum FIELDMDP:Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

.field public static final enum IDENTITY:Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

.field public static final enum UR:Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 1
    new-instance v0, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    const-string v1, "DOCMDP"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;->DOCMDP:Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    .line 6
    new-instance v1, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    const-string v3, "UR"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;->UR:Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    .line 8
    new-instance v3, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    const-string v5, "FIELDMDP"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;->FIELDMDP:Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    .line 13
    new-instance v5, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    const-string v7, "IDENTITY"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;->IDENTITY:Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    const/4 v7, 0x4

    new-array v7, v7, [Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    .line 14
    sput-object v7, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;->$VALUES:[Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;->$VALUES:[Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    invoke-virtual {v0}, [Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/signatures/DigitalSignatureInfo$ReferenceTransformMethod;

    return-object v0
.end method

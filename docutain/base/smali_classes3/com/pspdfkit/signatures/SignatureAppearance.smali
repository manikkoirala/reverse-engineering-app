.class public abstract Lcom/pspdfkit/signatures/SignatureAppearance;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/signatures/SignatureAppearance$Builder;,
        Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;,
        Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getSignatureAppearanceMode()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;
.end method

.method public abstract getSignatureGraphic()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;
.end method

.method public abstract getSignatureWatermark()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;
.end method

.method public abstract reuseExistingSignatureAppearanceStream()Z
.end method

.method public abstract showSignDate()Z
.end method

.method public abstract showSignatureLocation()Z
.end method

.method public abstract showSignatureReason()Z
.end method

.method public abstract showSignerName()Z
.end method

.method public abstract showWatermark()Z
.end method

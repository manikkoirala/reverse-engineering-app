.class public Lcom/pspdfkit/signatures/provider/PrivateKeySignatureProvider;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/signatures/provider/SignatureProvider;


# instance fields
.field private final encryptionAlgorithm:Lcom/pspdfkit/signatures/EncryptionAlgorithm;

.field private final nativePrivateKey:Lcom/pspdfkit/internal/jni/NativePrivateKey;


# direct methods
.method public constructor <init>(Ljava/security/KeyStore$PrivateKeyEntry;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateEncodingException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "privateKey"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->a(Ljava/security/KeyStore$PrivateKeyEntry;)Lcom/pspdfkit/internal/jni/NativePrivateKey;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/signatures/provider/PrivateKeySignatureProvider;->nativePrivateKey:Lcom/pspdfkit/internal/jni/NativePrivateKey;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativePrivateKey;->encryptionAlgorithm()Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;)Lcom/pspdfkit/signatures/EncryptionAlgorithm;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/signatures/provider/PrivateKeySignatureProvider;->encryptionAlgorithm:Lcom/pspdfkit/signatures/EncryptionAlgorithm;

    return-void
.end method


# virtual methods
.method public getEncryptionAlgorithm()Lcom/pspdfkit/signatures/EncryptionAlgorithm;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/provider/PrivateKeySignatureProvider;->encryptionAlgorithm:Lcom/pspdfkit/signatures/EncryptionAlgorithm;

    return-object v0
.end method

.method public signData([BLcom/pspdfkit/signatures/HashAlgorithm;)[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/provider/PrivateKeySignatureProvider;->nativePrivateKey:Lcom/pspdfkit/internal/jni/NativePrivateKey;

    .line 2
    invoke-static {p2}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/signatures/HashAlgorithm;)Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;

    move-result-object p2

    .line 3
    invoke-static {p1, v0, p2}, Lcom/pspdfkit/internal/jni/NativeDocumentSigner;->signData([BLcom/pspdfkit/internal/jni/NativePrivateKey;Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;)[B

    move-result-object p1

    return-object p1
.end method

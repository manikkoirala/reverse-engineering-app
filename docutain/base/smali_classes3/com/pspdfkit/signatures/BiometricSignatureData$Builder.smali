.class public final Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/signatures/BiometricSignatureData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private inputMethod:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

.field private pressurePoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private timePoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private touchRadius:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->pressurePoints:Ljava/util/List;

    .line 5
    iput-object v0, p0, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->timePoints:Ljava/util/List;

    .line 8
    iput-object v0, p0, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->touchRadius:Ljava/lang/Float;

    .line 11
    iput-object v0, p0, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->inputMethod:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    return-void
.end method


# virtual methods
.method public build()Lcom/pspdfkit/signatures/BiometricSignatureData;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->timePoints:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    .line 2
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x0

    .line 4
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 5
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 6
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 7
    :goto_1
    new-instance v1, Lcom/pspdfkit/signatures/AutoValue_BiometricSignatureData;

    iget-object v2, p0, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->pressurePoints:Ljava/util/List;

    iget-object v3, p0, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->touchRadius:Ljava/lang/Float;

    iget-object v4, p0, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->inputMethod:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/pspdfkit/signatures/AutoValue_BiometricSignatureData;-><init>(Ljava/util/List;Ljava/util/List;Ljava/lang/Float;Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;)V

    return-object v1
.end method

.method public setInputMethod(Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;)Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->inputMethod:Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    return-object p0
.end method

.method public setPressurePoints(Ljava/util/List;)Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;)",
            "Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->pressurePoints:Ljava/util/List;

    return-object p0
.end method

.method public setTimePoints(Ljava/util/List;)Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->timePoints:Ljava/util/List;

    return-object p0
.end method

.method public setTouchRadius(Ljava/lang/Float;)Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->touchRadius:Ljava/lang/Float;

    return-object p0
.end method

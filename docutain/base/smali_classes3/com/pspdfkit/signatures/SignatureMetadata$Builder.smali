.class public Lcom/pspdfkit/signatures/SignatureMetadata$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/signatures/SignatureMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private signatureLocation:Ljava/lang/String;

.field private signatureReason:Ljava/lang/String;

.field private signersName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/pspdfkit/signatures/SignatureMetadata;
    .locals 4

    .line 1
    new-instance v0, Lcom/pspdfkit/signatures/AutoValue_SignatureMetadata;

    iget-object v1, p0, Lcom/pspdfkit/signatures/SignatureMetadata$Builder;->signersName:Ljava/lang/String;

    iget-object v2, p0, Lcom/pspdfkit/signatures/SignatureMetadata$Builder;->signatureReason:Ljava/lang/String;

    iget-object v3, p0, Lcom/pspdfkit/signatures/SignatureMetadata$Builder;->signatureLocation:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/signatures/AutoValue_SignatureMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public signatureLocation(Ljava/lang/String;)Lcom/pspdfkit/signatures/SignatureMetadata$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/signatures/SignatureMetadata$Builder;->signatureLocation:Ljava/lang/String;

    return-object p0
.end method

.method public signatureReason(Ljava/lang/String;)Lcom/pspdfkit/signatures/SignatureMetadata$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/signatures/SignatureMetadata$Builder;->signatureReason:Ljava/lang/String;

    return-object p0
.end method

.method public signersName(Ljava/lang/String;)Lcom/pspdfkit/signatures/SignatureMetadata$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/signatures/SignatureMetadata$Builder;->signersName:Ljava/lang/String;

    return-object p0
.end method

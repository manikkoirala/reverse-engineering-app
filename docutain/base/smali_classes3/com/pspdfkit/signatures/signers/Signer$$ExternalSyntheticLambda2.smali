.class public final synthetic Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda2;
.super Ljava/lang/Object;
.source "D8$$SyntheticClass"

# interfaces
.implements Lcom/pspdfkit/signatures/signers/Signer$OnSigningParametersReadyCallback;


# instance fields
.field public final synthetic f$0:Lcom/pspdfkit/signatures/signers/Signer;

.field public final synthetic f$1:Lcom/pspdfkit/signatures/signers/SignerOptions;

.field public final synthetic f$2:Lio/reactivex/rxjava3/core/CompletableEmitter;


# direct methods
.method public synthetic constructor <init>(Lcom/pspdfkit/signatures/signers/Signer;Lcom/pspdfkit/signatures/signers/SignerOptions;Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda2;->f$0:Lcom/pspdfkit/signatures/signers/Signer;

    iput-object p2, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda2;->f$1:Lcom/pspdfkit/signatures/signers/SignerOptions;

    iput-object p3, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda2;->f$2:Lio/reactivex/rxjava3/core/CompletableEmitter;

    return-void
.end method


# virtual methods
.method public final onSigningParametersReady(Lcom/pspdfkit/signatures/provider/SignatureProvider;Ljava/security/cert/X509Certificate;)V
    .locals 3

    iget-object v0, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda2;->f$0:Lcom/pspdfkit/signatures/signers/Signer;

    iget-object v1, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda2;->f$1:Lcom/pspdfkit/signatures/signers/SignerOptions;

    iget-object v2, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda2;->f$2:Lio/reactivex/rxjava3/core/CompletableEmitter;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/pspdfkit/signatures/signers/Signer;->lambda$signFormFieldAsync$0$com-pspdfkit-signatures-signers-Signer(Lcom/pspdfkit/signatures/signers/SignerOptions;Lio/reactivex/rxjava3/core/CompletableEmitter;Lcom/pspdfkit/signatures/provider/SignatureProvider;Ljava/security/cert/X509Certificate;)V

    return-void
.end method

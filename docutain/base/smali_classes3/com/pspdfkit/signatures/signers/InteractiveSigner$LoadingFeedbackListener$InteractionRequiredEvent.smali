.class public final enum Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InteractionRequiredEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

.field public static final enum PASSWORD_INVALID:Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

.field public static final enum PASSWORD_MISSING:Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1
    new-instance v0, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

    const-string v1, "PASSWORD_MISSING"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;->PASSWORD_MISSING:Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

    .line 9
    new-instance v1, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

    const-string v3, "PASSWORD_INVALID"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;->PASSWORD_INVALID:Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    .line 10
    sput-object v3, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;->$VALUES:[Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;->$VALUES:[Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

    invoke-virtual {v0}, [Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

    return-object v0
.end method

.class public final synthetic Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda0;
.super Ljava/lang/Object;
.source "D8$$SyntheticClass"

# interfaces
.implements Lio/reactivex/rxjava3/core/CompletableOnSubscribe;


# instance fields
.field public final synthetic f$0:Lcom/pspdfkit/signatures/signers/Signer;

.field public final synthetic f$1:Lcom/pspdfkit/forms/SignatureFormField;

.field public final synthetic f$2:Lcom/pspdfkit/signatures/contents/SignatureContents;

.field public final synthetic f$3:Ljava/io/OutputStream;


# direct methods
.method public synthetic constructor <init>(Lcom/pspdfkit/signatures/signers/Signer;Lcom/pspdfkit/forms/SignatureFormField;Lcom/pspdfkit/signatures/contents/SignatureContents;Ljava/io/OutputStream;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda0;->f$0:Lcom/pspdfkit/signatures/signers/Signer;

    iput-object p2, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda0;->f$1:Lcom/pspdfkit/forms/SignatureFormField;

    iput-object p3, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda0;->f$2:Lcom/pspdfkit/signatures/contents/SignatureContents;

    iput-object p4, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda0;->f$3:Ljava/io/OutputStream;

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 4

    iget-object v0, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda0;->f$0:Lcom/pspdfkit/signatures/signers/Signer;

    iget-object v1, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda0;->f$1:Lcom/pspdfkit/forms/SignatureFormField;

    iget-object v2, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda0;->f$2:Lcom/pspdfkit/signatures/contents/SignatureContents;

    iget-object v3, p0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda0;->f$3:Ljava/io/OutputStream;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/pspdfkit/signatures/signers/Signer;->lambda$embedSignatureInFormFieldAsync$3$com-pspdfkit-signatures-signers-Signer(Lcom/pspdfkit/forms/SignatureFormField;Lcom/pspdfkit/signatures/contents/SignatureContents;Ljava/io/OutputStream;Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    return-void
.end method

.class public interface abstract Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/signatures/signers/InteractiveSigner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LoadingFeedbackListener"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;
    }
.end annotation


# virtual methods
.method public abstract onError(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract onInteractionRequired(Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;)V
.end method

.method public abstract onSuccess()V
.end method

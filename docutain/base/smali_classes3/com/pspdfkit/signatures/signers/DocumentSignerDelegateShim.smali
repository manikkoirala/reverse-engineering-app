.class Lcom/pspdfkit/signatures/signers/DocumentSignerDelegateShim;
.super Lcom/pspdfkit/internal/jni/NativeDocumentSignerDelegate;
.source "SourceFile"


# instance fields
.field private final signatureProvider:Lcom/pspdfkit/signatures/provider/SignatureProvider;


# direct methods
.method constructor <init>(Lcom/pspdfkit/signatures/provider/SignatureProvider;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativeDocumentSignerDelegate;-><init>()V

    const-string v0, "signatureProvider"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/DocumentSignerDelegateShim;->signatureProvider:Lcom/pspdfkit/signatures/provider/SignatureProvider;

    return-void
.end method


# virtual methods
.method public signData([BLcom/pspdfkit/internal/jni/NativeHashAlgorithm;Lcom/pspdfkit/internal/jni/NativeAsyncSignatureCallback;)V
    .locals 6

    .line 1
    invoke-static {p2}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;)Lcom/pspdfkit/signatures/HashAlgorithm;

    move-result-object v0

    const-string v1, "PSPDFKit.Signatures"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 3
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No matching HashAlgorithm found for NativeHashAlgorithm "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 6
    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v3, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    new-array p2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 7
    invoke-static {v1, v3, v4, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    new-array p2, v2, [B

    .line 12
    invoke-virtual {p3, v2, p2}, Lcom/pspdfkit/internal/jni/NativeAsyncSignatureCallback;->complete(Z[B)V

    .line 15
    :cond_0
    :try_start_0
    iget-object p2, p0, Lcom/pspdfkit/signatures/signers/DocumentSignerDelegateShim;->signatureProvider:Lcom/pspdfkit/signatures/provider/SignatureProvider;

    invoke-interface {p2, p1, v0}, Lcom/pspdfkit/signatures/provider/SignatureProvider;->signData([BLcom/pspdfkit/signatures/HashAlgorithm;)[B

    move-result-object p1

    if-nez p1, :cond_1

    const-string p1, "SignatureProvider returned a null signature."

    new-array p2, v2, [Ljava/lang/Object;

    .line 18
    invoke-static {v1, p1, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    new-array p1, v2, [B

    .line 19
    invoke-virtual {p3, v2, p1}, Lcom/pspdfkit/internal/jni/NativeAsyncSignatureCallback;->complete(Z[B)V

    goto :goto_0

    :cond_1
    const/4 p2, 0x1

    .line 21
    invoke-virtual {p3, p2, p1}, Lcom/pspdfkit/internal/jni/NativeAsyncSignatureCallback;->complete(Z[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-array p2, v2, [Ljava/lang/Object;

    const-string v0, "SignatureProvider threw exception while signing data."

    .line 24
    invoke-static {v1, p1, v0, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    new-array p1, v2, [B

    .line 25
    invoke-virtual {p3, v2, p1}, Lcom/pspdfkit/internal/jni/NativeAsyncSignatureCallback;->complete(Z[B)V

    :goto_0
    return-void
.end method

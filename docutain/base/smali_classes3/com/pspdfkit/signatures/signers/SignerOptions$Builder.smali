.class public Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/signatures/signers/SignerOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

.field private final destination:Ljava/io/OutputStream;

.field private estimatedSignatureSize:Ljava/lang/Integer;

.field private signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

.field private signatureContents:Lcom/pspdfkit/signatures/contents/SignatureContents;

.field private final signatureFormField:Lcom/pspdfkit/forms/SignatureFormField;

.field private signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/forms/SignatureFormField;Ljava/io/OutputStream;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "signatureFormField"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "destination"

    .line 3
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureFormField:Lcom/pspdfkit/forms/SignatureFormField;

    .line 5
    iput-object p2, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->destination:Ljava/io/OutputStream;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/signatures/signers/SignerOptions;Ljava/io/OutputStream;)V
    .locals 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "signerOptions"

    .line 7
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    iget-object v0, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureFormField:Lcom/pspdfkit/forms/SignatureFormField;

    iput-object v0, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureFormField:Lcom/pspdfkit/forms/SignatureFormField;

    .line 9
    iput-object p2, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->destination:Ljava/io/OutputStream;

    .line 10
    iget-object p2, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    iput-object p2, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    .line 11
    iget-object p2, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

    iput-object p2, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

    .line 12
    iget-object p2, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    iput-object p2, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    .line 13
    iget-object p2, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureContents:Lcom/pspdfkit/signatures/contents/SignatureContents;

    iput-object p2, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureContents:Lcom/pspdfkit/signatures/contents/SignatureContents;

    .line 14
    iget-object p1, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->estimatedSignatureSize:Ljava/lang/Integer;

    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->estimatedSignatureSize:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public biometricSignatureData(Lcom/pspdfkit/signatures/BiometricSignatureData;)Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;
    .locals 2

    if-eqz p1, :cond_1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureContents:Lcom/pspdfkit/signatures/contents/SignatureContents;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Can\'t set biometric signature data when custom signature contents are used."

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    return-object p0
.end method

.method public build()Lcom/pspdfkit/signatures/signers/SignerOptions;
    .locals 10

    .line 1
    new-instance v9, Lcom/pspdfkit/signatures/signers/SignerOptions;

    iget-object v1, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureFormField:Lcom/pspdfkit/forms/SignatureFormField;

    iget-object v2, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->destination:Ljava/io/OutputStream;

    iget-object v3, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    iget-object v4, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

    iget-object v5, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    iget-object v6, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureContents:Lcom/pspdfkit/signatures/contents/SignatureContents;

    iget-object v7, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->estimatedSignatureSize:Ljava/lang/Integer;

    const/4 v8, 0x0

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/signatures/signers/SignerOptions;-><init>(Lcom/pspdfkit/forms/SignatureFormField;Ljava/io/OutputStream;Lcom/pspdfkit/signatures/BiometricSignatureData;Lcom/pspdfkit/signatures/SignatureAppearance;Lcom/pspdfkit/signatures/SignatureMetadata;Lcom/pspdfkit/signatures/contents/SignatureContents;Ljava/lang/Integer;Lcom/pspdfkit/signatures/signers/SignerOptions-IA;)V

    return-object v9
.end method

.method public estimatedSignatureSize(Ljava/lang/Integer;)Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->estimatedSignatureSize:Ljava/lang/Integer;

    return-object p0
.end method

.method public signatureAppearance(Lcom/pspdfkit/signatures/SignatureAppearance;)Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

    return-object p0
.end method

.method public signatureContents(Lcom/pspdfkit/signatures/contents/SignatureContents;)Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Custom signature contents can\'t be used together with biometric signature data"

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureContents:Lcom/pspdfkit/signatures/contents/SignatureContents;

    return-object p0
.end method

.method public signatureMetadata(Lcom/pspdfkit/signatures/SignatureMetadata;)Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    return-object p0
.end method

.class Lcom/pspdfkit/signatures/signers/DocumentSignerDataSourceShim;
.super Lcom/pspdfkit/internal/jni/NativeDocumentSignerDataSource;
.source "SourceFile"


# instance fields
.field private final estimatedSignatureSize:Ljava/lang/Integer;

.field private final nativeBiometricProperties:Lcom/pspdfkit/internal/jni/NativeSignatureBiometricProperties;

.field private nativeEncryptionAlgorithm:Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;

.field private nativeSignatureAppearance:Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;


# direct methods
.method constructor <init>(Lcom/pspdfkit/signatures/provider/SignatureProvider;Lcom/pspdfkit/signatures/SignatureAppearance;Lcom/pspdfkit/signatures/BiometricSignatureData;Ljava/lang/Integer;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativeDocumentSignerDataSource;-><init>()V

    if-eqz p1, :cond_0

    .line 4
    invoke-interface {p1}, Lcom/pspdfkit/signatures/provider/SignatureProvider;->getEncryptionAlgorithm()Lcom/pspdfkit/signatures/EncryptionAlgorithm;

    move-result-object p1

    const-string v0, "Signature provider returned null when asked for encryption algorithm."

    .line 5
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/signatures/EncryptionAlgorithm;)Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/DocumentSignerDataSourceShim;->nativeEncryptionAlgorithm:Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;

    :cond_0
    if-eqz p2, :cond_1

    .line 15
    :try_start_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object p1

    .line 16
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/sj;->a(Landroid/content/Context;Lcom/pspdfkit/signatures/SignatureAppearance;)Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/DocumentSignerDataSourceShim;->nativeSignatureAppearance:Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 19
    new-instance p2, Lcom/pspdfkit/signatures/SigningFailedException;

    invoke-direct {p2, p1}, Lcom/pspdfkit/signatures/SigningFailedException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    .line 24
    :cond_1
    :goto_0
    invoke-static {p3}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/signatures/BiometricSignatureData;)Lcom/pspdfkit/internal/jni/NativeSignatureBiometricProperties;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/DocumentSignerDataSourceShim;->nativeBiometricProperties:Lcom/pspdfkit/internal/jni/NativeSignatureBiometricProperties;

    .line 26
    iput-object p4, p0, Lcom/pspdfkit/signatures/signers/DocumentSignerDataSourceShim;->estimatedSignatureSize:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public signatureAppearance(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/signatures/signers/DocumentSignerDataSourceShim;->nativeSignatureAppearance:Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;

    return-object p1
.end method

.method public signatureBiometricProperties(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeSignatureBiometricProperties;
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/signatures/signers/DocumentSignerDataSourceShim;->nativeBiometricProperties:Lcom/pspdfkit/internal/jni/NativeSignatureBiometricProperties;

    return-object p1
.end method

.method public signatureEncryptionAlgorithm(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/signatures/signers/DocumentSignerDataSourceShim;->nativeEncryptionAlgorithm:Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;

    return-object p1
.end method

.method public signatureEstimatedSize(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/signatures/signers/DocumentSignerDataSourceShim;->estimatedSignatureSize:Ljava/lang/Integer;

    return-object p1
.end method

.method public signatureHashAlgorithm(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.class public abstract Lcom/pspdfkit/signatures/signers/Signer;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/signatures/signers/Signer$OnSigningParametersReadyCallback;
    }
.end annotation


# instance fields
.field private final displayName:Ljava/lang/String;

.field private final filterSubtype:Lcom/pspdfkit/signatures/FilterSubtype;


# direct methods
.method static bridge synthetic -$$Nest$mconvertToSigningFailedException(Lcom/pspdfkit/signatures/signers/Signer;Lcom/pspdfkit/internal/jni/NativeDocumentSignerStatus;Ljava/lang/String;)Lcom/pspdfkit/signatures/SigningFailedException;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/signatures/signers/Signer;->convertToSigningFailedException(Lcom/pspdfkit/internal/jni/NativeDocumentSignerStatus;Ljava/lang/String;)Lcom/pspdfkit/signatures/SigningFailedException;

    move-result-object p0

    return-object p0
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "displayName"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/Signer;->displayName:Ljava/lang/String;

    .line 4
    sget-object p1, Lcom/pspdfkit/signatures/FilterSubtype;->ADOBE_PKCS7_DETACHED:Lcom/pspdfkit/signatures/FilterSubtype;

    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/Signer;->filterSubtype:Lcom/pspdfkit/signatures/FilterSubtype;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/pspdfkit/signatures/FilterSubtype;)V
    .locals 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "displayName"

    .line 6
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/Signer;->displayName:Ljava/lang/String;

    .line 8
    iput-object p2, p0, Lcom/pspdfkit/signatures/signers/Signer;->filterSubtype:Lcom/pspdfkit/signatures/FilterSubtype;

    return-void
.end method

.method private convertToSigningFailedException(Lcom/pspdfkit/internal/jni/NativeDocumentSignerStatus;Ljava/lang/String;)Lcom/pspdfkit/signatures/SigningFailedException;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/signatures/SigningFailedException;

    const-class v1, Lcom/pspdfkit/signatures/SigningStatus;

    invoke-static {v1, p1}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/signatures/SigningStatus;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/signatures/SigningFailedException;-><init>(Lcom/pspdfkit/signatures/SigningStatus;Ljava/lang/String;)V

    return-object v0
.end method

.method private nativeDocumentSignerCallback(Lio/reactivex/rxjava3/core/CompletableEmitter;)Lcom/pspdfkit/internal/jni/NativeDocumentSignerCallback;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/signatures/signers/Signer$1;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/signatures/signers/Signer$1;-><init>(Lcom/pspdfkit/signatures/signers/Signer;Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    return-object v0
.end method


# virtual methods
.method checkDigitalSignatureLicense()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DIGITAL_SIGNATURES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    new-instance v0, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v1, "Signing form fields requires digital signature feature in your license!"

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final embedSignatureInFormFieldAsync(Lcom/pspdfkit/forms/SignatureFormField;Lcom/pspdfkit/signatures/contents/SignatureContents;Ljava/io/OutputStream;)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/signers/Signer;->checkDigitalSignatureLicense()V

    const-string v0, "signatureFormField"

    const-string v1, "message"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_2

    const-string v0, "contents"

    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    const-string v0, "destination"

    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p3, :cond_0

    .line 8
    new-instance v0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/signatures/signers/Signer;Lcom/pspdfkit/forms/SignatureFormField;Lcom/pspdfkit/signatures/contents/SignatureContents;Ljava/io/OutputStream;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 17
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/u;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1

    .line 18
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 19
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 20
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/signers/Signer;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getFilterSubtype()Lcom/pspdfkit/signatures/FilterSubtype;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/signers/Signer;->filterSubtype:Lcom/pspdfkit/signatures/FilterSubtype;

    return-object v0
.end method

.method lambda$embedSignatureInFormFieldAsync$3$com-pspdfkit-signatures-signers-Signer(Lcom/pspdfkit/forms/SignatureFormField;Lcom/pspdfkit/signatures/contents/SignatureContents;Ljava/io/OutputStream;Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeDocumentSigner;->create()Lcom/pspdfkit/internal/jni/NativeDocumentSigner;

    move-result-object v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/signatures/signers/Signer;->filterSubtype:Lcom/pspdfkit/signatures/FilterSubtype;

    .line 3
    const-class v2, Lcom/pspdfkit/internal/jni/NativeFilterSubtype;

    invoke-static {v2, v1}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/jni/NativeFilterSubtype;

    .line 4
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentSigner;->setSubfilter(Lcom/pspdfkit/internal/jni/NativeFilterSubtype;)V

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/tf;->getNativeFormField()Lcom/pspdfkit/internal/jni/NativeFormField;

    move-result-object p1

    new-instance v1, Lcom/pspdfkit/internal/yj;

    invoke-direct {v1, p2}, Lcom/pspdfkit/internal/yj;-><init>(Lcom/pspdfkit/signatures/contents/SignatureContents;)V

    new-instance p2, Lcom/pspdfkit/internal/sl;

    invoke-direct {p2, p3}, Lcom/pspdfkit/internal/sl;-><init>(Ljava/io/OutputStream;)V

    .line 9
    invoke-direct {p0, p4}, Lcom/pspdfkit/signatures/signers/Signer;->nativeDocumentSignerCallback(Lio/reactivex/rxjava3/core/CompletableEmitter;)Lcom/pspdfkit/internal/jni/NativeDocumentSignerCallback;

    move-result-object p3

    .line 10
    invoke-virtual {v0, p1, v1, p2, p3}, Lcom/pspdfkit/internal/jni/NativeDocumentSigner;->embedSignatureContentsInFormField(Lcom/pspdfkit/internal/jni/NativeFormField;Lcom/pspdfkit/internal/jni/NativeSignatureContents;Lcom/pspdfkit/internal/jni/NativeDataSink;Lcom/pspdfkit/internal/jni/NativeDocumentSignerCallback;)V

    return-void
.end method

.method lambda$prepareFormFieldForSigningAsync$2$com-pspdfkit-signatures-signers-Signer(Lcom/pspdfkit/signatures/signers/SignerOptions;Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeDocumentSigner;->create()Lcom/pspdfkit/internal/jni/NativeDocumentSigner;

    move-result-object v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/signatures/signers/Signer;->filterSubtype:Lcom/pspdfkit/signatures/FilterSubtype;

    .line 3
    const-class v2, Lcom/pspdfkit/internal/jni/NativeFilterSubtype;

    invoke-static {v2, v1}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/jni/NativeFilterSubtype;

    .line 4
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentSigner;->setSubfilter(Lcom/pspdfkit/internal/jni/NativeFilterSubtype;)V

    .line 5
    new-instance v1, Lcom/pspdfkit/signatures/signers/DocumentSignerDataSourceShim;

    iget-object v2, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

    iget-object v3, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->estimatedSignatureSize:Ljava/lang/Integer;

    const/4 v4, 0x0

    invoke-direct {v1, v4, v2, v4, v3}, Lcom/pspdfkit/signatures/signers/DocumentSignerDataSourceShim;-><init>(Lcom/pspdfkit/signatures/provider/SignatureProvider;Lcom/pspdfkit/signatures/SignatureAppearance;Lcom/pspdfkit/signatures/BiometricSignatureData;Ljava/lang/Integer;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentSigner;->setDataSource(Lcom/pspdfkit/internal/jni/NativeDocumentSignerDataSource;)V

    .line 7
    iget-object v1, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureFormField:Lcom/pspdfkit/forms/SignatureFormField;

    .line 8
    invoke-virtual {v1}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/tf;->getNativeFormField()Lcom/pspdfkit/internal/jni/NativeFormField;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/yj;

    iget-object v3, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureContents:Lcom/pspdfkit/signatures/contents/SignatureContents;

    invoke-direct {v2, v3}, Lcom/pspdfkit/internal/yj;-><init>(Lcom/pspdfkit/signatures/contents/SignatureContents;)V

    new-instance v3, Lcom/pspdfkit/internal/sl;

    iget-object v5, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->destination:Ljava/io/OutputStream;

    invoke-direct {v3, v5}, Lcom/pspdfkit/internal/sl;-><init>(Ljava/io/OutputStream;)V

    iget-object p1, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    if-nez p1, :cond_0

    goto :goto_0

    .line 9
    :cond_0
    new-instance v4, Lcom/pspdfkit/internal/jni/NativeDocumentSignatureMetadata;

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureMetadata;->signersName()Ljava/lang/String;

    move-result-object v5

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureMetadata;->signatureReason()Ljava/lang/String;

    move-result-object v6

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureMetadata;->signatureLocation()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v4, v5, v6, p1}, Lcom/pspdfkit/internal/jni/NativeDocumentSignatureMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    :goto_0
    invoke-direct {p0, p2}, Lcom/pspdfkit/signatures/signers/Signer;->nativeDocumentSignerCallback(Lio/reactivex/rxjava3/core/CompletableEmitter;)Lcom/pspdfkit/internal/jni/NativeDocumentSignerCallback;

    move-result-object v5

    .line 14
    invoke-virtual/range {v0 .. v5}, Lcom/pspdfkit/internal/jni/NativeDocumentSigner;->prepareFormFieldToBeSigned(Lcom/pspdfkit/internal/jni/NativeFormField;Lcom/pspdfkit/internal/jni/NativeSignatureContents;Lcom/pspdfkit/internal/jni/NativeDataSink;Lcom/pspdfkit/internal/jni/NativeDocumentSignatureMetadata;Lcom/pspdfkit/internal/jni/NativeDocumentSignerCallback;)V

    return-void
.end method

.method lambda$signFormFieldAsync$0$com-pspdfkit-signatures-signers-Signer(Lcom/pspdfkit/signatures/signers/SignerOptions;Lio/reactivex/rxjava3/core/CompletableEmitter;Lcom/pspdfkit/signatures/provider/SignatureProvider;Ljava/security/cert/X509Certificate;)V
    .locals 10

    const-string v0, "argumentName"

    :try_start_0
    const-string v1, "certificate"

    .line 2
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p4, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v1, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureFormField:Lcom/pspdfkit/forms/SignatureFormField;

    const-string v3, "signatureFormField"

    .line 55
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {v1, v3, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    iget-object v1, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->destination:Ljava/io/OutputStream;

    const-string v3, "destination"

    .line 108
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-static {v1, v3, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v0, 0x0

    .line 160
    invoke-static {p4, v0}, Lcom/pspdfkit/internal/vr;->a(Ljava/security/cert/X509Certificate;Z)Lcom/pspdfkit/internal/jni/NativeX509Certificate;

    move-result-object v5

    .line 163
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeDocumentSigner;->create()Lcom/pspdfkit/internal/jni/NativeDocumentSigner;

    move-result-object v3

    .line 164
    iget-object p4, p0, Lcom/pspdfkit/signatures/signers/Signer;->filterSubtype:Lcom/pspdfkit/signatures/FilterSubtype;

    .line 165
    const-class v0, Lcom/pspdfkit/internal/jni/NativeFilterSubtype;

    invoke-static {v0, p4}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p4

    check-cast p4, Lcom/pspdfkit/internal/jni/NativeFilterSubtype;

    .line 166
    invoke-virtual {v3, p4}, Lcom/pspdfkit/internal/jni/NativeDocumentSigner;->setSubfilter(Lcom/pspdfkit/internal/jni/NativeFilterSubtype;)V

    .line 167
    new-instance p4, Lcom/pspdfkit/signatures/signers/DocumentSignerDataSourceShim;

    iget-object v0, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

    iget-object v1, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    iget-object v4, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->estimatedSignatureSize:Ljava/lang/Integer;

    invoke-direct {p4, p3, v0, v1, v4}, Lcom/pspdfkit/signatures/signers/DocumentSignerDataSourceShim;-><init>(Lcom/pspdfkit/signatures/provider/SignatureProvider;Lcom/pspdfkit/signatures/SignatureAppearance;Lcom/pspdfkit/signatures/BiometricSignatureData;Ljava/lang/Integer;)V

    invoke-virtual {v3, p4}, Lcom/pspdfkit/internal/jni/NativeDocumentSigner;->setDataSource(Lcom/pspdfkit/internal/jni/NativeDocumentSignerDataSource;)V

    .line 174
    iget-object p4, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureFormField:Lcom/pspdfkit/forms/SignatureFormField;

    .line 177
    invoke-virtual {p4}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object p4

    .line 178
    invoke-interface {p4}, Lcom/pspdfkit/internal/tf;->getNativeFormField()Lcom/pspdfkit/internal/jni/NativeFormField;

    move-result-object v4

    new-instance v6, Lcom/pspdfkit/internal/sl;

    iget-object p4, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->destination:Ljava/io/OutputStream;

    invoke-direct {v6, p4}, Lcom/pspdfkit/internal/sl;-><init>(Ljava/io/OutputStream;)V

    iget-object p1, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    if-nez p1, :cond_0

    :goto_0
    move-object v7, v2

    goto :goto_1

    .line 179
    :cond_0
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeDocumentSignatureMetadata;

    .line 180
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureMetadata;->signersName()Ljava/lang/String;

    move-result-object p4

    .line 181
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureMetadata;->signatureReason()Ljava/lang/String;

    move-result-object v0

    .line 182
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureMetadata;->signatureLocation()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p4, v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocumentSignatureMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 183
    :goto_1
    new-instance v8, Lcom/pspdfkit/signatures/signers/DocumentSignerDelegateShim;

    invoke-direct {v8, p3}, Lcom/pspdfkit/signatures/signers/DocumentSignerDelegateShim;-><init>(Lcom/pspdfkit/signatures/provider/SignatureProvider;)V

    .line 186
    invoke-direct {p0, p2}, Lcom/pspdfkit/signatures/signers/Signer;->nativeDocumentSignerCallback(Lio/reactivex/rxjava3/core/CompletableEmitter;)Lcom/pspdfkit/internal/jni/NativeDocumentSignerCallback;

    move-result-object v9

    .line 187
    invoke-virtual/range {v3 .. v9}, Lcom/pspdfkit/internal/jni/NativeDocumentSigner;->signFormElementAsync(Lcom/pspdfkit/internal/jni/NativeFormField;Lcom/pspdfkit/internal/jni/NativeX509Certificate;Lcom/pspdfkit/internal/jni/NativeDataSink;Lcom/pspdfkit/internal/jni/NativeDocumentSignatureMetadata;Lcom/pspdfkit/internal/jni/NativeDocumentSignerDelegate;Lcom/pspdfkit/internal/jni/NativeDocumentSignerCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 199
    invoke-interface {p2}, Lio/reactivex/rxjava3/core/CompletableEmitter;->isDisposed()Z

    move-result p3

    if-nez p3, :cond_1

    new-instance p3, Lcom/pspdfkit/signatures/SigningFailedException;

    invoke-direct {p3, p1}, Lcom/pspdfkit/signatures/SigningFailedException;-><init>(Ljava/lang/Throwable;)V

    invoke-interface {p2, p3}, Lio/reactivex/rxjava3/core/CompletableEmitter;->onError(Ljava/lang/Throwable;)V

    :cond_1
    :goto_2
    return-void
.end method

.method synthetic lambda$signFormFieldAsync$1$com-pspdfkit-signatures-signers-Signer(Lcom/pspdfkit/signatures/signers/SignerOptions;Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    :try_start_0
    new-instance v0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/signatures/signers/Signer;Lcom/pspdfkit/signatures/signers/SignerOptions;Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    invoke-virtual {p0, v0}, Lcom/pspdfkit/signatures/signers/Signer;->prepareSigningParameters(Lcom/pspdfkit/signatures/signers/Signer$OnSigningParametersReadyCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 37
    invoke-interface {p2}, Lio/reactivex/rxjava3/core/CompletableEmitter;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/pspdfkit/signatures/SigningFailedException;

    invoke-direct {v0, p1}, Lcom/pspdfkit/signatures/SigningFailedException;-><init>(Ljava/lang/Throwable;)V

    invoke-interface {p2, v0}, Lio/reactivex/rxjava3/core/CompletableEmitter;->onError(Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public final prepareFormFieldForSigningAsync(Lcom/pspdfkit/signatures/signers/SignerOptions;)Lio/reactivex/rxjava3/core/Completable;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/signers/Signer;->checkDigitalSignatureLicense()V

    const-string v0, "Signer options may not be null."

    const-string v1, "message"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_1

    .line 4
    iget-object v0, p1, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureContents:Lcom/pspdfkit/signatures/contents/SignatureContents;

    const-string v2, "Signature contents may not be null."

    .line 5
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    .line 6
    new-instance v0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/signatures/signers/Signer;Lcom/pspdfkit/signatures/signers/SignerOptions;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 19
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1

    .line 20
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 21
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected prepareSigningParameters(Lcom/pspdfkit/signatures/signers/Signer$OnSigningParametersReadyCallback;)V
    .locals 0

    return-void
.end method

.method public final signFormField(Lcom/pspdfkit/signatures/signers/SignerOptions;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "signFormField() may not be called from the main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->a(Ljava/lang/String;)V

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/signatures/signers/Signer;->signFormFieldAsync(Lcom/pspdfkit/signatures/signers/SignerOptions;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V

    return-void
.end method

.method public signFormFieldAsync(Lcom/pspdfkit/signatures/signers/SignerOptions;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/signers/Signer;->checkDigitalSignatureLicense()V

    .line 2
    new-instance v0, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/signatures/signers/Signer$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/signatures/signers/Signer;Lcom/pspdfkit/signatures/signers/SignerOptions;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 43
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.class public abstract Lcom/pspdfkit/signatures/signers/ContainedSignaturesSigner;
.super Lcom/pspdfkit/signatures/signers/Signer;
.source "SourceFile"


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 3
    invoke-direct {p0, p2}, Lcom/pspdfkit/signatures/signers/Signer;-><init>(Ljava/lang/String;)V

    const-string p2, "context"

    .line 4
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/ContainedSignaturesSigner;->context:Landroid/content/Context;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/pspdfkit/signatures/FilterSubtype;)V
    .locals 0

    .line 6
    invoke-direct {p0, p2, p3}, Lcom/pspdfkit/signatures/signers/Signer;-><init>(Ljava/lang/String;Lcom/pspdfkit/signatures/FilterSubtype;)V

    const-string p2, "context"

    .line 7
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/ContainedSignaturesSigner;->context:Landroid/content/Context;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/signers/Signer;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/ContainedSignaturesSigner;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method lambda$signFormFieldAsync$0$com-pspdfkit-signatures-signers-ContainedSignaturesSigner(Landroid/content/Context;Ljava/io/File;Lcom/pspdfkit/signatures/signers/SignerOptions;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-static {p2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocument(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/document/PdfDocument;

    move-result-object p1

    .line 8
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getFormProvider()Lcom/pspdfkit/forms/FormProvider;

    move-result-object v0

    iget-object v1, p3, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureFormField:Lcom/pspdfkit/forms/SignatureFormField;

    .line 10
    invoke-virtual {v1}, Lcom/pspdfkit/forms/FormField;->getFullyQualifiedName()Ljava/lang/String;

    move-result-object v1

    .line 11
    invoke-interface {v0, v1}, Lcom/pspdfkit/forms/FormProvider;->getFormFieldWithFullyQualifiedName(Ljava/lang/String;)Lcom/pspdfkit/forms/FormField;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 17
    instance-of v1, v0, Lcom/pspdfkit/forms/SignatureFormField;

    if-eqz v1, :cond_1

    .line 20
    check-cast v0, Lcom/pspdfkit/forms/SignatureFormField;

    .line 24
    invoke-virtual {p0, p3, p2, p1, v0}, Lcom/pspdfkit/signatures/signers/ContainedSignaturesSigner;->prepareSignatureContents(Lcom/pspdfkit/signatures/signers/SignerOptions;Ljava/io/File;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/forms/SignatureFormField;)Lcom/pspdfkit/signatures/contents/SignatureContents;

    move-result-object p1

    const-string p2, "Returned SignatureContents cannot be null"

    const-string v1, "message"

    .line 27
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 28
    iget-object p2, p3, Lcom/pspdfkit/signatures/signers/SignerOptions;->destination:Ljava/io/OutputStream;

    invoke-virtual {p0, v0, p1, p2}, Lcom/pspdfkit/signatures/signers/Signer;->embedSignatureInFormFieldAsync(Lcom/pspdfkit/forms/SignatureFormField;Lcom/pspdfkit/signatures/contents/SignatureContents;Ljava/io/OutputStream;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1

    .line 29
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 30
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Form field to sign must be a signature field."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 31
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Can\'t retrieve form field to sign in prepared document."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method synthetic lambda$signFormFieldAsync$1$com-pspdfkit-signatures-signers-ContainedSignaturesSigner(Lcom/pspdfkit/signatures/signers/SignerOptions;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/signers/ContainedSignaturesSigner;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_2

    const-string v1, ".pdf_preparedSignature"

    .line 10
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/kb;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 15
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 19
    new-instance v1, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, p1, v3}, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;-><init>(Lcom/pspdfkit/signatures/signers/SignerOptions;Ljava/io/OutputStream;)V

    const/4 v3, 0x0

    .line 23
    invoke-virtual {v1, v3}, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->biometricSignatureData(Lcom/pspdfkit/signatures/BiometricSignatureData;)Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;

    move-result-object v1

    new-instance v3, Lcom/pspdfkit/signatures/contents/BlankSignatureContents;

    invoke-direct {v3}, Lcom/pspdfkit/signatures/contents/BlankSignatureContents;-><init>()V

    .line 27
    invoke-virtual {v1, v3}, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->signatureContents(Lcom/pspdfkit/signatures/contents/SignatureContents;)Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;

    move-result-object v1

    .line 28
    invoke-virtual {v1}, Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;->build()Lcom/pspdfkit/signatures/signers/SignerOptions;

    move-result-object v1

    .line 32
    invoke-virtual {p0, v1}, Lcom/pspdfkit/signatures/signers/Signer;->prepareFormFieldForSigningAsync(Lcom/pspdfkit/signatures/signers/SignerOptions;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    new-instance v3, Lcom/pspdfkit/signatures/signers/ContainedSignaturesSigner$$ExternalSyntheticLambda1;

    invoke-direct {v3, p0, v0, v2, p1}, Lcom/pspdfkit/signatures/signers/ContainedSignaturesSigner$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/signatures/signers/ContainedSignaturesSigner;Landroid/content/Context;Ljava/io/File;Lcom/pspdfkit/signatures/signers/SignerOptions;)V

    .line 33
    invoke-static {v3}, Lio/reactivex/rxjava3/core/Completable;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    invoke-virtual {v1, p1}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1

    .line 34
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Failed to create temporary file as a destination for document prepared for signing"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 35
    :cond_2
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;

    const-string v0, "PSPDFKit must be initialized before signing!"

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected abstract prepareSignatureContents(Lcom/pspdfkit/signatures/signers/SignerOptions;Ljava/io/File;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/forms/SignatureFormField;)Lcom/pspdfkit/signatures/contents/SignatureContents;
.end method

.method public signFormFieldAsync(Lcom/pspdfkit/signatures/signers/SignerOptions;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/signers/Signer;->checkDigitalSignatureLicense()V

    .line 2
    new-instance v0, Lcom/pspdfkit/signatures/signers/ContainedSignaturesSigner$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/signatures/signers/ContainedSignaturesSigner$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/signatures/signers/ContainedSignaturesSigner;Lcom/pspdfkit/signatures/signers/SignerOptions;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 71
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

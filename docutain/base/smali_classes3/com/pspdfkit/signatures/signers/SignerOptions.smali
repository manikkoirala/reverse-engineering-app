.class public Lcom/pspdfkit/signatures/signers/SignerOptions;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/signatures/signers/SignerOptions$Builder;
    }
.end annotation


# instance fields
.field public final biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

.field public final destination:Ljava/io/OutputStream;

.field public final estimatedSignatureSize:Ljava/lang/Integer;

.field public final signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

.field public final signatureContents:Lcom/pspdfkit/signatures/contents/SignatureContents;

.field public final signatureFormField:Lcom/pspdfkit/forms/SignatureFormField;

.field public final signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/forms/SignatureFormField;Ljava/io/OutputStream;Lcom/pspdfkit/signatures/BiometricSignatureData;Lcom/pspdfkit/signatures/SignatureAppearance;Lcom/pspdfkit/signatures/SignatureMetadata;Lcom/pspdfkit/signatures/contents/SignatureContents;Ljava/lang/Integer;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p6, :cond_1

    if-nez p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "signatureContents and biometricSignatureData can\'t be used together."

    .line 2
    invoke-static {v1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureFormField:Lcom/pspdfkit/forms/SignatureFormField;

    .line 7
    iput-object p2, p0, Lcom/pspdfkit/signatures/signers/SignerOptions;->destination:Ljava/io/OutputStream;

    .line 8
    iput-object p3, p0, Lcom/pspdfkit/signatures/signers/SignerOptions;->biometricSignatureData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    .line 9
    iput-object p4, p0, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureAppearance:Lcom/pspdfkit/signatures/SignatureAppearance;

    .line 10
    iput-object p5, p0, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureMetadata:Lcom/pspdfkit/signatures/SignatureMetadata;

    .line 11
    iput-object p6, p0, Lcom/pspdfkit/signatures/signers/SignerOptions;->signatureContents:Lcom/pspdfkit/signatures/contents/SignatureContents;

    .line 12
    iput-object p7, p0, Lcom/pspdfkit/signatures/signers/SignerOptions;->estimatedSignatureSize:Ljava/lang/Integer;

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/forms/SignatureFormField;Ljava/io/OutputStream;Lcom/pspdfkit/signatures/BiometricSignatureData;Lcom/pspdfkit/signatures/SignatureAppearance;Lcom/pspdfkit/signatures/SignatureMetadata;Lcom/pspdfkit/signatures/contents/SignatureContents;Ljava/lang/Integer;Lcom/pspdfkit/signatures/signers/SignerOptions-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/pspdfkit/signatures/signers/SignerOptions;-><init>(Lcom/pspdfkit/forms/SignatureFormField;Ljava/io/OutputStream;Lcom/pspdfkit/signatures/BiometricSignatureData;Lcom/pspdfkit/signatures/SignatureAppearance;Lcom/pspdfkit/signatures/SignatureMetadata;Lcom/pspdfkit/signatures/contents/SignatureContents;Ljava/lang/Integer;)V

    return-void
.end method

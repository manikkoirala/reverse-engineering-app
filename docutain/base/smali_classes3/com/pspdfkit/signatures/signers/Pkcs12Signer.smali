.class public final Lcom/pspdfkit/signatures/signers/Pkcs12Signer;
.super Lcom/pspdfkit/signatures/signers/PrivateKeySigner;
.source "SourceFile"


# instance fields
.field private final certificateFileUri:Landroid/net/Uri;

.field private final keyAlias:Ljava/lang/String;

.field private signingKeyPair:Ljava/security/KeyStore$PrivateKeyEntry;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/signers/PrivateKeySigner;-><init>(Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 6
    iput-object p2, p0, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;->certificateFileUri:Landroid/net/Uri;

    .line 7
    iput-object p3, p0, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;->keyAlias:Ljava/lang/String;

    const/4 p1, 0x0

    .line 8
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;->signingKeyPair:Ljava/security/KeyStore$PrivateKeyEntry;

    return-void

    .line 9
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Signers PKCS12 certificate URI may not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private openSignatureFile(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/InputStream;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object p1

    return-object p1
.end method

.method private openSignatureFileFromAssets(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object p1

    .line 2
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "file:///android_asset/"

    const-string v1, ""

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public loadPrivateKey(Ljava/lang/String;Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;Lcom/pspdfkit/signatures/signers/PrivateKeySigner$OnPrivateKeyLoadedCallback;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;->signingKeyPair:Ljava/security/KeyStore$PrivateKeyEntry;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 2
    invoke-interface {p2}, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;->onSuccess()V

    .line 3
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;->signingKeyPair:Ljava/security/KeyStore$PrivateKeyEntry;

    invoke-interface {p3, p1}, Lcom/pspdfkit/signatures/signers/PrivateKeySigner$OnPrivateKeyLoadedCallback;->onPrivateKeyLoaded(Ljava/security/KeyStore$PrivateKeyEntry;)V

    return-void

    .line 8
    :cond_1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    if-eqz p2, :cond_2

    const-string p1, "PSPDFKit is not initialized!"

    .line 10
    invoke-interface {p2, p1, v1}, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;->onError(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    return-void

    .line 17
    :cond_3
    :try_start_0
    iget-object v2, p0, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;->certificateFileUri:Landroid/net/Uri;

    if-eqz v2, :cond_4

    .line 18
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "file:///android_asset/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_5

    .line 19
    iget-object v2, p0, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;->certificateFileUri:Landroid/net/Uri;

    invoke-direct {p0, v0, v2}, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;->openSignatureFileFromAssets(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_1

    .line 20
    :cond_5
    iget-object v2, p0, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;->certificateFileUri:Landroid/net/Uri;

    invoke-direct {p0, v0, v2}, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;->openSignatureFile(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    :goto_1
    move-object v1, v0

    if-eqz v1, :cond_7

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;->keyAlias:Ljava/lang/String;

    invoke-static {v1, p1, v0, p1}, Lcom/pspdfkit/signatures/SignatureManager;->loadPrivateKeyPairFromStream(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore$PrivateKeyEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;->signingKeyPair:Ljava/security/KeyStore$PrivateKeyEntry;

    if-eqz p2, :cond_6

    .line 23
    invoke-interface {p2}, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;->onSuccess()V

    .line 24
    :cond_6
    iget-object v0, p0, Lcom/pspdfkit/signatures/signers/Pkcs12Signer;->signingKeyPair:Ljava/security/KeyStore$PrivateKeyEntry;

    invoke-interface {p3, v0}, Lcom/pspdfkit/signatures/signers/PrivateKeySigner$OnPrivateKeyLoadedCallback;->onPrivateKeyLoaded(Ljava/security/KeyStore$PrivateKeyEntry;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    invoke-static {v1}, Lcom/pspdfkit/internal/fv;->a(Ljava/io/InputStream;)V

    goto :goto_5

    .line 42
    :cond_7
    :try_start_1
    new-instance p3, Ljava/io/IOException;

    const-string v0, "Certificate input stream is null!"

    invoke-direct {p3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p3
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    nop

    goto :goto_2

    :catchall_0
    move-exception p1

    goto :goto_3

    :goto_2
    if-nez p1, :cond_8

    if-eqz p2, :cond_9

    .line 54
    :try_start_2
    sget-object p1, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;->PASSWORD_MISSING:Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

    invoke-interface {p2, p1}, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;->onInteractionRequired(Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;)V

    goto :goto_4

    :cond_8
    if-eqz p2, :cond_9

    .line 58
    sget-object p1, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;->PASSWORD_INVALID:Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;

    invoke-interface {p2, p1}, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;->onInteractionRequired(Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener$InteractionRequiredEvent;)V

    goto :goto_4

    :catch_1
    move-exception p1

    if-eqz p2, :cond_9

    const-string p3, "The PKCS12 file could not be found or opened."

    .line 59
    invoke-interface {p2, p3, p1}, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;->onError(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 74
    :goto_3
    invoke-static {v1}, Lcom/pspdfkit/internal/fv;->a(Ljava/io/InputStream;)V

    .line 75
    throw p1

    .line 76
    :cond_9
    :goto_4
    invoke-static {v1}, Lcom/pspdfkit/internal/fv;->a(Ljava/io/InputStream;)V

    :goto_5
    return-void
.end method

.class public final Lcom/pspdfkit/signatures/signers/MemorySigner;
.super Lcom/pspdfkit/signatures/signers/PrivateKeySigner;
.source "SourceFile"


# instance fields
.field private final signingKeyPair:Ljava/security/KeyStore$PrivateKeyEntry;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/security/KeyStore$PrivateKeyEntry;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/signers/PrivateKeySigner;-><init>(Ljava/lang/String;)V

    const-string p1, "signingKeyPair"

    .line 2
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-virtual {p2}, Ljava/security/KeyStore$PrivateKeyEntry;->getPrivateKey()Ljava/security/PrivateKey;

    move-result-object p1

    const-string v0, "signingKeyPair.getPrivateKey"

    const-string v1, "signingKeyPair is missing the required private key."

    .line 5
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 10
    invoke-virtual {p2}, Ljava/security/KeyStore$PrivateKeyEntry;->getCertificateChain()[Ljava/security/cert/Certificate;

    move-result-object p1

    const-string v0, "signingKeyPair.getCertificateChain()"

    const-string v1, "signingKeyPair is missing the required certificate chain."

    .line 11
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 15
    invoke-virtual {p2}, Ljava/security/KeyStore$PrivateKeyEntry;->getCertificateChain()[Ljava/security/cert/Certificate;

    move-result-object p1

    array-length p1, p1

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    .line 17
    iput-object p2, p0, Lcom/pspdfkit/signatures/signers/MemorySigner;->signingKeyPair:Ljava/security/KeyStore$PrivateKeyEntry;

    return-void

    .line 18
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public loadPrivateKey(Ljava/lang/String;Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;Lcom/pspdfkit/signatures/signers/PrivateKeySigner$OnPrivateKeyLoadedCallback;)V
    .locals 0

    if-eqz p2, :cond_0

    .line 1
    invoke-interface {p2}, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;->onSuccess()V

    .line 2
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/signatures/signers/MemorySigner;->signingKeyPair:Ljava/security/KeyStore$PrivateKeyEntry;

    invoke-interface {p3, p1}, Lcom/pspdfkit/signatures/signers/PrivateKeySigner$OnPrivateKeyLoadedCallback;->onPrivateKeyLoaded(Ljava/security/KeyStore$PrivateKeyEntry;)V

    return-void
.end method

.class public abstract Lcom/pspdfkit/signatures/signers/PrivateKeySigner;
.super Lcom/pspdfkit/signatures/signers/Signer;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/signatures/signers/InteractiveSigner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/signatures/signers/PrivateKeySigner$OnPrivateKeyLoadedCallback;
    }
.end annotation


# instance fields
.field private loadingFeedbackListener:Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;

.field private onSigningParametersReadyCallback:Lcom/pspdfkit/signatures/signers/Signer$OnSigningParametersReadyCallback;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/signers/Signer;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private declared-synchronized finishLoading(Ljava/security/KeyStore$PrivateKeyEntry;)V
    .locals 4

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/signatures/signers/PrivateKeySigner;->onSigningParametersReadyCallback:Lcom/pspdfkit/signatures/signers/Signer$OnSigningParametersReadyCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    .line 3
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/pspdfkit/signatures/signers/PrivateKeySigner;->loadingFeedbackListener:Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6
    :try_start_2
    new-instance v2, Lcom/pspdfkit/signatures/provider/PrivateKeySignatureProvider;

    invoke-direct {v2, p1}, Lcom/pspdfkit/signatures/provider/PrivateKeySignatureProvider;-><init>(Ljava/security/KeyStore$PrivateKeyEntry;)V

    .line 7
    invoke-virtual {p1}, Ljava/security/KeyStore$PrivateKeyEntry;->getCertificate()Ljava/security/cert/Certificate;

    move-result-object v3

    instance-of v3, v3, Ljava/security/cert/X509Certificate;

    if-eqz v3, :cond_1

    .line 11
    invoke-virtual {p1}, Ljava/security/KeyStore$PrivateKeyEntry;->getCertificate()Ljava/security/cert/Certificate;

    move-result-object p1

    check-cast p1, Ljava/security/cert/X509Certificate;

    .line 12
    invoke-interface {v0, v2, p1}, Lcom/pspdfkit/signatures/signers/Signer$OnSigningParametersReadyCallback;->onSigningParametersReady(Lcom/pspdfkit/signatures/provider/SignatureProvider;Ljava/security/cert/X509Certificate;)V

    const/4 p1, 0x0

    .line 17
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/PrivateKeySigner;->onSigningParametersReadyCallback:Lcom/pspdfkit/signatures/signers/Signer$OnSigningParametersReadyCallback;

    goto :goto_0

    .line 18
    :cond_1
    new-instance p1, Ljava/security/cert/CertificateEncodingException;

    const-string v0, "The certificate inside the private key must be a X.509 certificate."

    invoke-direct {p1, v0}, Ljava/security/cert/CertificateEncodingException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    move-exception p1

    if-eqz v1, :cond_2

    :try_start_3
    const-string v0, "Error while extracting X.509 certificate from the private key."

    .line 30
    invoke-interface {v1, v0, p1}, Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;->onError(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private tryLoadingPrivateKey(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/signers/PrivateKeySigner;->loadingFeedbackListener:Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;

    new-instance v1, Lcom/pspdfkit/signatures/signers/PrivateKeySigner$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/signatures/signers/PrivateKeySigner$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/signatures/signers/PrivateKeySigner;)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/pspdfkit/signatures/signers/PrivateKeySigner;->loadPrivateKey(Ljava/lang/String;Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;Lcom/pspdfkit/signatures/signers/PrivateKeySigner$OnPrivateKeyLoadedCallback;)V

    return-void
.end method


# virtual methods
.method lambda$tryLoadingPrivateKey$0$com-pspdfkit-signatures-signers-PrivateKeySigner(Ljava/security/KeyStore$PrivateKeyEntry;)V
    .locals 2

    const-string v0, "privateKey"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/signers/PrivateKeySigner;->finishLoading(Ljava/security/KeyStore$PrivateKeyEntry;)V

    return-void
.end method

.method protected abstract loadPrivateKey(Ljava/lang/String;Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;Lcom/pspdfkit/signatures/signers/PrivateKeySigner$OnPrivateKeyLoadedCallback;)V
.end method

.method protected final prepareSigningParameters(Lcom/pspdfkit/signatures/signers/Signer$OnSigningParametersReadyCallback;)V
    .locals 2

    const-string v0, "callback"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/PrivateKeySigner;->onSigningParametersReadyCallback:Lcom/pspdfkit/signatures/signers/Signer$OnSigningParametersReadyCallback;

    .line 55
    invoke-direct {p0, v1}, Lcom/pspdfkit/signatures/signers/PrivateKeySigner;->tryLoadingPrivateKey(Ljava/lang/String;)V

    return-void
.end method

.method public final setLoadingFeedbackListener(Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/PrivateKeySigner;->loadingFeedbackListener:Lcom/pspdfkit/signatures/signers/InteractiveSigner$LoadingFeedbackListener;

    return-void
.end method

.method public final unlockPrivateKeyWithPassword(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/signers/PrivateKeySigner;->tryLoadingPrivateKey(Ljava/lang/String;)V

    return-void
.end method

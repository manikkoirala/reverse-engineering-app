.class Lcom/pspdfkit/signatures/signers/Signer$1;
.super Lcom/pspdfkit/internal/jni/NativeDocumentSignerCallback;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/signatures/signers/Signer;->nativeDocumentSignerCallback(Lio/reactivex/rxjava3/core/CompletableEmitter;)Lcom/pspdfkit/internal/jni/NativeDocumentSignerCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/signatures/signers/Signer;

.field final synthetic val$emitter:Lio/reactivex/rxjava3/core/CompletableEmitter;


# direct methods
.method constructor <init>(Lcom/pspdfkit/signatures/signers/Signer;Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/signatures/signers/Signer$1;->this$0:Lcom/pspdfkit/signatures/signers/Signer;

    iput-object p2, p0, Lcom/pspdfkit/signatures/signers/Signer$1;->val$emitter:Lio/reactivex/rxjava3/core/CompletableEmitter;

    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativeDocumentSignerCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public complete(Lcom/pspdfkit/internal/jni/NativeDocumentSignerStatus;Lcom/pspdfkit/internal/jni/NativeDataSink;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/jni/NativeDocumentSignerStatus;",
            "Lcom/pspdfkit/internal/jni/NativeDataSink;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object p2, Lcom/pspdfkit/internal/jni/NativeDocumentSignerStatus;->SIGNED:Lcom/pspdfkit/internal/jni/NativeDocumentSignerStatus;

    if-eq p1, p2, :cond_0

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/signatures/signers/Signer$1;->val$emitter:Lio/reactivex/rxjava3/core/CompletableEmitter;

    invoke-interface {p2}, Lio/reactivex/rxjava3/core/CompletableEmitter;->isDisposed()Z

    move-result p2

    if-nez p2, :cond_1

    .line 3
    iget-object p2, p0, Lcom/pspdfkit/signatures/signers/Signer$1;->val$emitter:Lio/reactivex/rxjava3/core/CompletableEmitter;

    iget-object p4, p0, Lcom/pspdfkit/signatures/signers/Signer$1;->this$0:Lcom/pspdfkit/signatures/signers/Signer;

    invoke-static {p4, p1, p3}, Lcom/pspdfkit/signatures/signers/Signer;->-$$Nest$mconvertToSigningFailedException(Lcom/pspdfkit/signatures/signers/Signer;Lcom/pspdfkit/internal/jni/NativeDocumentSignerStatus;Ljava/lang/String;)Lcom/pspdfkit/signatures/SigningFailedException;

    move-result-object p1

    invoke-interface {p2, p1}, Lio/reactivex/rxjava3/core/CompletableEmitter;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/signatures/signers/Signer$1;->val$emitter:Lio/reactivex/rxjava3/core/CompletableEmitter;

    invoke-interface {p1}, Lio/reactivex/rxjava3/core/CompletableEmitter;->isDisposed()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/pspdfkit/signatures/signers/Signer$1;->val$emitter:Lio/reactivex/rxjava3/core/CompletableEmitter;

    invoke-interface {p1}, Lio/reactivex/rxjava3/core/CompletableEmitter;->onComplete()V

    :cond_1
    :goto_0
    return-void
.end method

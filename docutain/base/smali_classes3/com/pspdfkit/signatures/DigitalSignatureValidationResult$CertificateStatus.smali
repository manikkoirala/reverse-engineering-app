.class public final enum Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CertificateStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum EXPIRED:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum EXPIRED_BUT_VALID_IN_THE_PAST:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum EXPIRED_NO_POE:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum FAILED_RETRIEVE_SIGNATURE_CONTENTS:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum GENERAL_VALIDATION_PROBLEM:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum INVALID:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum NOT_YET_VALID:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum NOT_YET_VALID_NO_POE:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum OK:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum OK_BUT_NOT_CHECKED_AGAINST_CA:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum OK_BUT_REVOCATION_CHECK_FAILED:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum OK_BUT_SELF_SIGNED:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum REVOKED:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum REVOKED_BUT_VALID_IN_THE_PAST:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

.field public static final enum REVOKED_NO_POE:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    .line 1
    new-instance v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v1, "OK"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->OK:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 3
    new-instance v1, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v3, "OK_BUT_SELF_SIGNED"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->OK_BUT_SELF_SIGNED:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 8
    new-instance v3, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v5, "OK_BUT_REVOCATION_CHECK_FAILED"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->OK_BUT_REVOCATION_CHECK_FAILED:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 10
    new-instance v5, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v7, "OK_BUT_NOT_CHECKED_AGAINST_CA"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->OK_BUT_NOT_CHECKED_AGAINST_CA:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 15
    new-instance v7, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v9, "EXPIRED"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->EXPIRED:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 21
    new-instance v9, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v11, "EXPIRED_NO_POE"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->EXPIRED_NO_POE:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 27
    new-instance v11, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v13, "EXPIRED_BUT_VALID_IN_THE_PAST"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->EXPIRED_BUT_VALID_IN_THE_PAST:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 32
    new-instance v13, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v15, "NOT_YET_VALID"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->NOT_YET_VALID:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 38
    new-instance v15, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v14, "NOT_YET_VALID_NO_POE"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->NOT_YET_VALID_NO_POE:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 40
    new-instance v14, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v12, "INVALID"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->INVALID:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 45
    new-instance v12, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v10, "REVOKED"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->REVOKED:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 51
    new-instance v10, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v8, "REVOKED_NO_POE"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->REVOKED_NO_POE:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 57
    new-instance v8, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v6, "REVOKED_BUT_VALID_IN_THE_PAST"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->REVOKED_BUT_VALID_IN_THE_PAST:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 59
    new-instance v6, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v4, "FAILED_RETRIEVE_SIGNATURE_CONTENTS"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->FAILED_RETRIEVE_SIGNATURE_CONTENTS:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    .line 61
    new-instance v4, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const-string v2, "GENERAL_VALIDATION_PROBLEM"

    move-object/from16 v17, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->GENERAL_VALIDATION_PROBLEM:Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const/16 v2, 0xf

    new-array v2, v2, [Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    const/16 v16, 0x0

    aput-object v0, v2, v16

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object v5, v2, v0

    const/4 v0, 0x4

    aput-object v7, v2, v0

    const/4 v0, 0x5

    aput-object v9, v2, v0

    const/4 v0, 0x6

    aput-object v11, v2, v0

    const/4 v0, 0x7

    aput-object v13, v2, v0

    const/16 v0, 0x8

    aput-object v15, v2, v0

    const/16 v0, 0x9

    aput-object v14, v2, v0

    const/16 v0, 0xa

    aput-object v12, v2, v0

    const/16 v0, 0xb

    aput-object v10, v2, v0

    const/16 v0, 0xc

    aput-object v8, v2, v0

    const/16 v0, 0xd

    aput-object v17, v2, v0

    aput-object v4, v2, v6

    .line 62
    sput-object v2, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->$VALUES:[Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->$VALUES:[Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    invoke-virtual {v0}, [Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$CertificateStatus;

    return-object v0
.end method


# virtual methods
.method public getLocalizedDescription(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult$2;->$SwitchMap$com$pspdfkit$signatures$DigitalSignatureValidationResult$CertificateStatus:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    return-object v1

    .line 74
    :pswitch_0
    sget v0, Lcom/pspdfkit/R$string;->pspdf__digital_signature_certificate_failed_retrieve_signature_contents:I

    .line 75
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 76
    :pswitch_1
    sget v0, Lcom/pspdfkit/R$string;->pspdf__digital_signature_certificate_general_validation_problem:I

    .line 77
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 78
    :pswitch_2
    sget v0, Lcom/pspdfkit/R$string;->pspdf__digital_signature_certificate_revoked:I

    .line 79
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 80
    :pswitch_3
    sget v0, Lcom/pspdfkit/R$string;->pspdf__digital_signature_certificate_invalid:I

    .line 81
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 82
    :pswitch_4
    sget v0, Lcom/pspdfkit/R$string;->pspdf__digital_signature_certificate_not_yet_valid:I

    .line 83
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 84
    :pswitch_5
    sget v0, Lcom/pspdfkit/R$string;->pspdf__digital_signature_certificate_status_expired:I

    .line 85
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

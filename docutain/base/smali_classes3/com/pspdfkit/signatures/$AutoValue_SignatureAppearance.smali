.class abstract Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;
.super Lcom/pspdfkit/signatures/SignatureAppearance;
.source "SourceFile"


# instance fields
.field private final getSignatureAppearanceMode:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

.field private final getSignatureGraphic:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

.field private final getSignatureWatermark:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

.field private final reuseExistingSignatureAppearanceStream:Z

.field private final showSignDate:Z

.field private final showSignatureLocation:Z

.field private final showSignatureReason:Z

.field private final showSignerName:Z

.field private final showWatermark:Z


# direct methods
.method constructor <init>(Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;ZZZZLcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;ZZ)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/signatures/SignatureAppearance;-><init>()V

    if-eqz p1, :cond_0

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureAppearanceMode:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

    .line 6
    iput-boolean p2, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignerName:Z

    .line 7
    iput-boolean p3, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignDate:Z

    .line 8
    iput-boolean p4, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignatureReason:Z

    .line 9
    iput-boolean p5, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignatureLocation:Z

    .line 10
    iput-object p6, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureGraphic:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    .line 11
    iput-object p7, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureWatermark:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    .line 12
    iput-boolean p8, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->reuseExistingSignatureAppearanceStream:Z

    .line 13
    iput-boolean p9, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showWatermark:Z

    return-void

    .line 14
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null getSignatureAppearanceMode"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/signatures/SignatureAppearance;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    .line 2
    check-cast p1, Lcom/pspdfkit/signatures/SignatureAppearance;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureAppearanceMode:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->getSignatureAppearanceMode()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignerName:Z

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->showSignerName()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignDate:Z

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->showSignDate()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignatureReason:Z

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->showSignatureReason()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignatureLocation:Z

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->showSignatureLocation()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureGraphic:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    if-nez v1, :cond_1

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->getSignatureGraphic()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    move-result-object v1

    if-nez v1, :cond_3

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->getSignatureGraphic()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureWatermark:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    if-nez v1, :cond_2

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->getSignatureWatermark()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    move-result-object v1

    if-nez v1, :cond_3

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->getSignatureWatermark()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_1
    iget-boolean v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->reuseExistingSignatureAppearanceStream:Z

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->reuseExistingSignatureAppearanceStream()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showWatermark:Z

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->showWatermark()Z

    move-result p1

    if-ne v1, p1, :cond_3

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    return v0

    :cond_4
    return v2
.end method

.method public getSignatureAppearanceMode()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureAppearanceMode:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

    return-object v0
.end method

.method public getSignatureGraphic()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureGraphic:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    return-object v0
.end method

.method public getSignatureWatermark()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureWatermark:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureAppearanceMode:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0xf4243

    xor-int/2addr v0, v1

    mul-int v0, v0, v1

    .line 3
    iget-boolean v2, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignerName:Z

    const/16 v3, 0x4cf

    const/16 v4, 0x4d5

    if-eqz v2, :cond_0

    const/16 v2, 0x4cf

    goto :goto_0

    :cond_0
    const/16 v2, 0x4d5

    :goto_0
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 5
    iget-boolean v2, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignDate:Z

    if-eqz v2, :cond_1

    const/16 v2, 0x4cf

    goto :goto_1

    :cond_1
    const/16 v2, 0x4d5

    :goto_1
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 7
    iget-boolean v2, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignatureReason:Z

    if-eqz v2, :cond_2

    const/16 v2, 0x4cf

    goto :goto_2

    :cond_2
    const/16 v2, 0x4d5

    :goto_2
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 9
    iget-boolean v2, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignatureLocation:Z

    if-eqz v2, :cond_3

    const/16 v2, 0x4cf

    goto :goto_3

    :cond_3
    const/16 v2, 0x4d5

    :goto_3
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureGraphic:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    const/4 v5, 0x0

    if-nez v2, :cond_4

    const/4 v2, 0x0

    goto :goto_4

    :cond_4
    invoke-virtual {v2}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->hashCode()I

    move-result v2

    :goto_4
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 13
    iget-object v2, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureWatermark:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    if-nez v2, :cond_5

    goto :goto_5

    :cond_5
    invoke-virtual {v2}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->hashCode()I

    move-result v5

    :goto_5
    xor-int/2addr v0, v5

    mul-int v0, v0, v1

    .line 15
    iget-boolean v2, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->reuseExistingSignatureAppearanceStream:Z

    if-eqz v2, :cond_6

    const/16 v2, 0x4cf

    goto :goto_6

    :cond_6
    const/16 v2, 0x4d5

    :goto_6
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 17
    iget-boolean v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showWatermark:Z

    if-eqz v1, :cond_7

    goto :goto_7

    :cond_7
    const/16 v3, 0x4d5

    :goto_7
    xor-int/2addr v0, v3

    return v0
.end method

.method public reuseExistingSignatureAppearanceStream()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->reuseExistingSignatureAppearanceStream:Z

    return v0
.end method

.method public showSignDate()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignDate:Z

    return v0
.end method

.method public showSignatureLocation()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignatureLocation:Z

    return v0
.end method

.method public showSignatureReason()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignatureReason:Z

    return v0
.end method

.method public showSignerName()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignerName:Z

    return v0
.end method

.method public showWatermark()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showWatermark:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SignatureAppearance{getSignatureAppearanceMode="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureAppearanceMode:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showSignerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignerName:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showSignDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignDate:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showSignatureReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignatureReason:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showSignatureLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showSignatureLocation:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getSignatureGraphic="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureGraphic:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getSignatureWatermark="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->getSignatureWatermark:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", reuseExistingSignatureAppearanceStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->reuseExistingSignatureAppearanceStream:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showWatermark="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/signatures/$AutoValue_SignatureAppearance;->showWatermark:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class abstract Lcom/pspdfkit/signatures/$AutoValue_Signature;
.super Lcom/pspdfkit/signatures/Signature;
.source "SourceFile"


# instance fields
.field private final annotationType:Lcom/pspdfkit/annotations/AnnotationType;

.field private final biometricData:Lcom/pspdfkit/signatures/BiometricSignatureData;

.field private final bitmapIdentifier:I

.field private final id:J

.field private final inkColor:I

.field private final lineWidth:F

.field private final lines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation
.end field

.field private final signatureDrawWidthRatio:F

.field private final signerIdentifier:Ljava/lang/String;

.field private final stampRect:Landroid/graphics/RectF;


# direct methods
.method constructor <init>(Lcom/pspdfkit/annotations/AnnotationType;JIFLjava/util/List;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;FILandroid/graphics/RectF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            "JIF",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/signatures/BiometricSignatureData;",
            "FI",
            "Landroid/graphics/RectF;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/signatures/Signature;-><init>()V

    if-eqz p1, :cond_1

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->annotationType:Lcom/pspdfkit/annotations/AnnotationType;

    .line 6
    iput-wide p2, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->id:J

    .line 7
    iput p4, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->inkColor:I

    .line 8
    iput p5, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->lineWidth:F

    if-eqz p6, :cond_0

    .line 12
    iput-object p6, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->lines:Ljava/util/List;

    .line 13
    iput-object p7, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->signerIdentifier:Ljava/lang/String;

    .line 14
    iput-object p8, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->biometricData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    .line 15
    iput p9, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->signatureDrawWidthRatio:F

    .line 16
    iput p10, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->bitmapIdentifier:I

    .line 17
    iput-object p11, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->stampRect:Landroid/graphics/RectF;

    return-void

    .line 18
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null lines"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 19
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null annotationType"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/signatures/Signature;

    const/4 v2, 0x0

    if-eqz v1, :cond_5

    .line 2
    check-cast p1, Lcom/pspdfkit/signatures/Signature;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->annotationType:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-wide v3, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->id:J

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getId()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-nez v1, :cond_4

    iget v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->inkColor:I

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getInkColor()I

    move-result v3

    if-ne v1, v3, :cond_4

    iget v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->lineWidth:F

    .line 6
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getLineWidth()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->lines:Ljava/util/List;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getLines()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->signerIdentifier:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getSignerIdentifier()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getSignerIdentifier()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->biometricData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    if-nez v1, :cond_2

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getBiometricData()Lcom/pspdfkit/signatures/BiometricSignatureData;

    move-result-object v1

    if-nez v1, :cond_4

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getBiometricData()Lcom/pspdfkit/signatures/BiometricSignatureData;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :goto_1
    iget v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->signatureDrawWidthRatio:F

    .line 10
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getSignatureDrawWidthRatio()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v1, v3, :cond_4

    iget v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->bitmapIdentifier:I

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getBitmapIdentifier()I

    move-result v3

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->stampRect:Landroid/graphics/RectF;

    if-nez v1, :cond_3

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getStampRect()Landroid/graphics/RectF;

    move-result-object p1

    if-nez p1, :cond_4

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getStampRect()Landroid/graphics/RectF;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    return v0

    :cond_5
    return v2
.end method

.method public getAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->annotationType:Lcom/pspdfkit/annotations/AnnotationType;

    return-object v0
.end method

.method public getBiometricData()Lcom/pspdfkit/signatures/BiometricSignatureData;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->biometricData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    return-object v0
.end method

.method public getBitmapIdentifier()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->bitmapIdentifier:I

    return v0
.end method

.method public getId()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->id:J

    return-wide v0
.end method

.method public getInkColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->inkColor:I

    return v0
.end method

.method public getLineWidth()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->lineWidth:F

    return v0
.end method

.method public getLines()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->lines:Ljava/util/List;

    return-object v0
.end method

.method public getSignatureDrawWidthRatio()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->signatureDrawWidthRatio:F

    return v0
.end method

.method public getSignerIdentifier()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->signerIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method public getStampRect()Landroid/graphics/RectF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->stampRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->annotationType:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0xf4243

    xor-int/2addr v0, v1

    mul-int v0, v0, v1

    .line 3
    iget-wide v2, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->id:J

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v3, v2

    xor-int/2addr v0, v3

    mul-int v0, v0, v1

    .line 5
    iget v2, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->inkColor:I

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 7
    iget v2, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->lineWidth:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->lines:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->signerIdentifier:Ljava/lang/String;

    const/4 v3, 0x0

    if-nez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_0
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 13
    iget-object v2, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->biometricData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_1
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 15
    iget v2, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->signatureDrawWidthRatio:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 17
    iget v2, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->bitmapIdentifier:I

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 19
    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->stampRect:Landroid/graphics/RectF;

    if-nez v1, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {v1}, Landroid/graphics/RectF;->hashCode()I

    move-result v3

    :goto_2
    xor-int/2addr v0, v3

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Signature{annotationType="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->annotationType:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", inkColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->inkColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", lineWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->lineWidth:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", lines="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->lines:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signerIdentifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->signerIdentifier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", biometricData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->biometricData:Lcom/pspdfkit/signatures/BiometricSignatureData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signatureDrawWidthRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->signatureDrawWidthRatio:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", bitmapIdentifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->bitmapIdentifier:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", stampRect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/signatures/$AutoValue_Signature;->stampRect:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

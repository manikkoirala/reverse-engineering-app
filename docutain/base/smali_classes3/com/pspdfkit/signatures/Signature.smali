.class public abstract Lcom/pspdfkit/signatures/Signature;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/signatures/Signature$HorizontalInset;
    }
.end annotation


# static fields
.field public static final BITMAP_NOT_SET:I = -0x1

.field public static final ID_NOT_SET:J = -0x1L

.field private static final JSON_KEY_BIOMETRIC_DATA:Ljava/lang/String; = "biometricData"

.field private static final JSON_KEY_BITMAP:Ljava/lang/String; = "bitmap"

.field private static final JSON_KEY_DRAW_WIDTH_RATIO:Ljava/lang/String; = "drawWidthRatio"

.field private static final JSON_KEY_INK_COLOR:Ljava/lang/String; = "inkColor"

.field private static final JSON_KEY_LINES:Ljava/lang/String; = "lines"

.field private static final JSON_KEY_LINE_WIDTH_PDF:Ljava/lang/String; = "lineWidthPdf"

.field private static final JSON_KEY_SIGNER_IDENTIFIER:Ljava/lang/String; = "signerIdentifier"

.field private static final JSON_KEY_STAMP_RECT:Ljava/lang/String; = "stampRect"

.field private static final JSON_KEY_X:Ljava/lang/String; = "x"

.field private static final JSON_KEY_Y:Ljava/lang/String; = "y"

.field private static final MIN_SIGNATURE_ANNOTATION_PDF_SIZE:F = 32.0f


# instance fields
.field private inkLinesBoundingBox:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(IFLjava/util/List;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;F)Lcom/pspdfkit/signatures/Signature;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IF",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/signatures/BiometricSignatureData;",
            "F)",
            "Lcom/pspdfkit/signatures/Signature;"
        }
    .end annotation

    .line 5
    new-instance v12, Lcom/pspdfkit/signatures/AutoValue_Signature;

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    const-wide/16 v2, -0x1

    const/4 v10, -0x1

    const/4 v11, 0x0

    move-object v0, v12

    move v4, p0

    move v5, p1

    move-object v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move/from16 v9, p5

    invoke-direct/range {v0 .. v11}, Lcom/pspdfkit/signatures/AutoValue_Signature;-><init>(Lcom/pspdfkit/annotations/AnnotationType;JIFLjava/util/List;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;FILandroid/graphics/RectF;)V

    return-object v12
.end method

.method private static create(JIFLjava/util/List;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;F)Lcom/pspdfkit/signatures/Signature;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JIF",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/signatures/BiometricSignatureData;",
            "F)",
            "Lcom/pspdfkit/signatures/Signature;"
        }
    .end annotation

    .line 6
    new-instance v12, Lcom/pspdfkit/signatures/AutoValue_Signature;

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v10, -0x1

    const/4 v11, 0x0

    move-object v0, v12

    move-wide v2, p0

    move v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v0 .. v11}, Lcom/pspdfkit/signatures/AutoValue_Signature;-><init>(Lcom/pspdfkit/annotations/AnnotationType;JIFLjava/util/List;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;FILandroid/graphics/RectF;)V

    return-object v12
.end method

.method private static create(JLandroid/graphics/Bitmap;Landroid/graphics/RectF;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;F)Lcom/pspdfkit/signatures/Signature;
    .locals 13

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->s()Lcom/pspdfkit/signatures/SignatureBitmapStorage;

    move-result-object v0

    move-object v1, p2

    invoke-virtual {v0, p2}, Lcom/pspdfkit/signatures/SignatureBitmapStorage;->putBitmap(Landroid/graphics/Bitmap;)I

    move-result v11

    .line 4
    new-instance v0, Lcom/pspdfkit/signatures/AutoValue_Signature;

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, v0

    move-wide v3, p0

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move/from16 v10, p6

    move-object/from16 v12, p3

    invoke-direct/range {v1 .. v12}, Lcom/pspdfkit/signatures/AutoValue_Signature;-><init>(Lcom/pspdfkit/annotations/AnnotationType;JIFLjava/util/List;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;FILandroid/graphics/RectF;)V

    return-object v0
.end method

.method public static create(Landroid/graphics/Bitmap;Landroid/graphics/RectF;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;F)Lcom/pspdfkit/signatures/Signature;
    .locals 13

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->s()Lcom/pspdfkit/signatures/SignatureBitmapStorage;

    move-result-object v0

    move-object v1, p0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/signatures/SignatureBitmapStorage;->putBitmap(Landroid/graphics/Bitmap;)I

    move-result v11

    .line 2
    new-instance v0, Lcom/pspdfkit/signatures/AutoValue_Signature;

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v3, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, v0

    move-object v8, p2

    move-object/from16 v9, p3

    move/from16 v10, p4

    move-object v12, p1

    invoke-direct/range {v1 .. v12}, Lcom/pspdfkit/signatures/AutoValue_Signature;-><init>(Lcom/pspdfkit/annotations/AnnotationType;JIFLjava/util/List;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;FILandroid/graphics/RectF;)V

    return-object v0
.end method

.method private static detectHorizontalImageInset(Landroid/graphics/Bitmap;)Lcom/pspdfkit/signatures/Signature$HorizontalInset;
    .locals 11

    .line 1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    .line 2
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    mul-int v0, v8, v9

    .line 4
    new-array v10, v0, [I

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, v10

    move v3, v8

    move v6, v8

    move v7, v9

    .line 5
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    const/4 p0, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 14
    :goto_0
    div-int/lit8 v5, v8, 0x2

    const/4 v6, 0x1

    if-ge v0, v5, :cond_5

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v9, :cond_3

    if-eqz v3, :cond_0

    if-nez v4, :cond_3

    :cond_0
    if-nez v3, :cond_1

    mul-int v7, v5, v8

    add-int/2addr v7, v0

    .line 16
    aget v7, v10, v7

    if-eqz v7, :cond_1

    move v1, v0

    const/4 v3, 0x1

    :cond_1
    if-nez v4, :cond_2

    add-int/lit8 v7, v5, 0x1

    mul-int v7, v7, v8

    sub-int/2addr v7, v0

    sub-int/2addr v7, v6

    .line 20
    aget v7, v10, v7

    if-eqz v7, :cond_2

    move v2, v0

    const/4 v4, 0x1

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    if-eqz v3, :cond_4

    if-eqz v4, :cond_4

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    :goto_2
    const/4 v0, 0x2

    new-array v3, v0, [I

    aput v1, v3, p0

    aput v2, v3, v6

    const v4, 0x7fffffff

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v0, :cond_7

    .line 34
    aget v7, v3, v5

    if-ge v7, v4, :cond_6

    move v4, v7

    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 35
    :cond_7
    div-int/lit8 v4, v4, 0x4

    new-array v3, v0, [I

    aput p0, v3, p0

    sub-int/2addr v1, v4

    aput v1, v3, v6

    .line 37
    invoke-static {v3}, Lcom/pspdfkit/internal/di;->a([I)I

    move-result v1

    new-array v0, v0, [I

    aput p0, v0, p0

    sub-int/2addr v2, v4

    aput v2, v0, v6

    .line 38
    invoke-static {v0}, Lcom/pspdfkit/internal/di;->a([I)I

    move-result p0

    .line 39
    new-instance v0, Lcom/pspdfkit/signatures/Signature$HorizontalInset;

    invoke-direct {v0, v1, p0}, Lcom/pspdfkit/signatures/Signature$HorizontalInset;-><init>(II)V

    return-object v0
.end method

.method public static fromJson(JLorg/json/JSONObject;)Lcom/pspdfkit/signatures/Signature;
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    move-object/from16 v0, p2

    const-string v1, "touchRadius"

    const-string v2, "inputMethod"

    const-string v3, "timePoints"

    const-string v4, "pressurePoints"

    const-string v5, "signatureJson"

    const-string v6, "argumentName"

    .line 3
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    .line 54
    invoke-static {v0, v5, v6}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v5, "signerIdentifier"

    .line 55
    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 56
    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v5, v6

    :goto_0
    const-string v7, "biometricData"

    .line 60
    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    const/4 v9, 0x0

    if-eqz v8, :cond_a

    .line 64
    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    if-nez v7, :cond_1

    goto/16 :goto_5

    .line 65
    :cond_1
    :try_start_0
    new-instance v8, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;

    invoke-direct {v8}, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;-><init>()V

    .line 66
    invoke-virtual {v7, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 68
    invoke-virtual {v7, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    if-nez v4, :cond_2

    move-object v10, v6

    goto :goto_2

    .line 69
    :cond_2
    new-instance v10, Ljava/util/ArrayList;

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v11

    invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v11, 0x0

    .line 70
    :goto_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_3

    .line 71
    invoke-virtual {v4, v11}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v12

    double-to-float v12, v12

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 72
    :cond_3
    :goto_2
    invoke-virtual {v8, v10}, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->setPressurePoints(Ljava/util/List;)Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;

    .line 74
    :cond_4
    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 75
    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    if-nez v3, :cond_5

    goto :goto_4

    .line 76
    :cond_5
    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    invoke-direct {v6, v4}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v4, 0x0

    .line 77
    :goto_3
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v4, v10, :cond_6

    .line 78
    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 79
    :cond_6
    :goto_4
    invoke-virtual {v8, v6}, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->setTimePoints(Ljava/util/List;)Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;

    .line 80
    :cond_7
    invoke-virtual {v7, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 81
    invoke-virtual {v7, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->setInputMethod(Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;)Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;

    .line 82
    :cond_8
    invoke-virtual {v7, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v7, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    double-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->setTouchRadius(Ljava/lang/Float;)Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;

    .line 83
    :cond_9
    invoke-virtual {v8}, Lcom/pspdfkit/signatures/BiometricSignatureData$Builder;->build()Lcom/pspdfkit/signatures/BiometricSignatureData;

    move-result-object v6
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception v0

    new-array v1, v9, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Signatures"

    const-string v3, "Error while deserializing biometric signature data."

    .line 85
    invoke-static {v2, v0, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    invoke-static {v0}, Lio/reactivex/rxjava3/exceptions/Exceptions;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_a
    :goto_5
    const/high16 v1, 0x3f800000    # 1.0f

    const-string v2, "drawWidthRatio"

    .line 87
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 88
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    double-to-float v1, v1

    :cond_b
    const-string v2, "bitmap"

    .line 91
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x1

    xor-int/2addr v3, v4

    if-eqz v3, :cond_e

    const-string v2, "inkColor"

    .line 94
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "lineWidthPdf"

    .line 95
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v3

    double-to-float v10, v3

    .line 97
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    const-string v3, "lines"

    .line 98
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    const/4 v3, 0x0

    .line 99
    :goto_6
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_d

    .line 100
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 101
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v7

    const/4 v8, 0x0

    .line 102
    :goto_7
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v8, v12, :cond_c

    .line 103
    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v12

    const-string v13, "x"

    .line 104
    invoke-virtual {v12, v13}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v13

    double-to-float v13, v13

    const-string v14, "y"

    .line 105
    invoke-virtual {v12, v14}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v14

    double-to-float v12, v14

    .line 106
    new-instance v14, Landroid/graphics/PointF;

    invoke-direct {v14, v13, v12}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v4, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    .line 108
    :cond_c
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_d
    move-wide/from16 v7, p0

    move v9, v2

    move-object v12, v5

    move-object v13, v6

    move v14, v1

    .line 111
    invoke-static/range {v7 .. v14}, Lcom/pspdfkit/signatures/Signature;->create(JIFLjava/util/List;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;F)Lcom/pspdfkit/signatures/Signature;

    move-result-object v0

    return-object v0

    .line 114
    :cond_e
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/pspdfkit/internal/ga;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    const-string v3, "stampRect"

    .line 117
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 118
    new-instance v10, Landroid/graphics/RectF;

    .line 119
    invoke-virtual {v0, v9}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v7

    double-to-float v3, v7

    .line 120
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v7

    double-to-float v4, v7

    const/4 v7, 0x2

    .line 121
    invoke-virtual {v0, v7}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v7

    double-to-float v7, v7

    const/4 v8, 0x3

    .line 122
    invoke-virtual {v0, v8}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v8

    double-to-float v0, v8

    invoke-direct {v10, v3, v4, v7, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-wide/from16 v7, p0

    move-object v9, v2

    move-object v11, v5

    move-object v12, v6

    move v13, v1

    .line 124
    invoke-static/range {v7 .. v13}, Lcom/pspdfkit/signatures/Signature;->create(JLandroid/graphics/Bitmap;Landroid/graphics/RectF;Ljava/lang/String;Lcom/pspdfkit/signatures/BiometricSignatureData;F)Lcom/pspdfkit/signatures/Signature;

    move-result-object v0

    return-object v0
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lcom/pspdfkit/signatures/Signature;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const-wide/16 v0, -0x1

    .line 1
    invoke-static {v0, v1, p0}, Lcom/pspdfkit/signatures/Signature;->fromJson(JLorg/json/JSONObject;)Lcom/pspdfkit/signatures/Signature;

    move-result-object p0

    return-object p0
.end method

.method public static textToBitmap(Ljava/lang/String;Lcom/pspdfkit/ui/fonts/Font;IFLandroid/util/DisplayMetrics;)Landroid/graphics/Bitmap;
    .locals 5

    const/4 v0, 0x0

    cmpg-float v1, p3, v0

    if-lez v1, :cond_1

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    .line 6
    new-instance v1, Landroid/graphics/Paint;

    const/4 v4, 0x2

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 8
    invoke-virtual {v1, p2}, Landroid/graphics/Paint;->setColor(I)V

    const/4 p1, 0x3

    const/high16 p2, 0x42c80000    # 100.0f

    .line 12
    invoke-static {p1, p2, p4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p1

    mul-float p1, p1, p3

    .line 13
    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 14
    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 16
    invoke-virtual {v1}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object p1

    .line 17
    iget p2, p1, Landroid/graphics/Paint$FontMetrics;->ascent:F

    neg-float p2, p2

    .line 24
    new-instance p3, Ljava/lang/StringBuilder;

    const-string p4, " "

    invoke-direct {p3, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p0, 0x20

    invoke-virtual {p3, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 25
    invoke-virtual {v1, p0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result p3

    float-to-int p3, p3

    .line 26
    iget p1, p1, Landroid/graphics/Paint$FontMetrics;->descent:F

    add-float/2addr p1, p2

    float-to-int p1, p1

    .line 28
    sget-object p4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p3, p1, p4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p4

    .line 29
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, p4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 30
    invoke-virtual {v3, p0, v0, p2, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 32
    invoke-static {p4}, Lcom/pspdfkit/signatures/Signature;->detectHorizontalImageInset(Landroid/graphics/Bitmap;)Lcom/pspdfkit/signatures/Signature$HorizontalInset;

    move-result-object p0

    .line 35
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature$HorizontalInset;->getInsetLeft()I

    move-result p2

    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature$HorizontalInset;->getTotalInset()I

    move-result p0

    sub-int/2addr p3, p0

    invoke-static {p4, p2, v2, p3, p1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0

    .line 36
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-array p2, v3, [Ljava/lang/Object;

    .line 37
    invoke-virtual {p1}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, p2, v2

    const-string p1, "Font %s is not available on this device."

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 38
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "scaleFactor must be a positive value, it was: "

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private toAnnotation(I)Lcom/pspdfkit/annotations/Annotation;
    .locals 3

    .line 158
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_0

    .line 159
    new-instance v0, Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-direct {v0, p1}, Lcom/pspdfkit/annotations/InkAnnotation;-><init>(I)V

    .line 160
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getLines()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/InkAnnotation;->setLines(Ljava/util/List;)V

    .line 161
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getInkColor()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    .line 162
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getLineWidth()F

    move-result p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/InkAnnotation;->setLineWidth(F)V

    goto :goto_0

    .line 164
    :cond_0
    new-instance v0, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-direct {v0, p1, v1, v2}, Lcom/pspdfkit/annotations/StampAnnotation;-><init>(ILandroid/graphics/RectF;Landroid/graphics/Bitmap;)V

    .line 167
    :goto_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    const/4 v1, 0x1

    invoke-interface {p1, v1}, Lcom/pspdfkit/internal/pf;->setIsSignature(Z)V

    return-object v0
.end method

.method private toAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/PointF;)Lcom/pspdfkit/annotations/Annotation;
    .locals 7

    .line 118
    invoke-interface {p1, p2}, Lcom/pspdfkit/document/PdfDocument;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v0

    .line 119
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    .line 125
    invoke-interface {p1, p2}, Lcom/pspdfkit/document/PdfDocument;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    const/high16 v2, 0x40800000    # 4.0f

    div-float/2addr p1, v2

    .line 126
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getSignatureDrawWidthRatio()F

    move-result v2

    mul-float v2, v2, p1

    .line 129
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result p1

    .line 130
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v3

    neg-float v3, v3

    cmpl-float v4, p1, v3

    if-lez v4, :cond_0

    div-float p1, v2, p1

    mul-float p1, p1, v3

    goto :goto_0

    :cond_0
    div-float v3, v2, v3

    mul-float v3, v3, p1

    move p1, v2

    move v2, v3

    :goto_0
    const/high16 v3, 0x42000000    # 32.0f

    cmpg-float v4, v2, p1

    if-gez v4, :cond_1

    div-float/2addr p1, v2

    .line 147
    iget v4, v0, Lcom/pspdfkit/utils/Size;->width:F

    .line 148
    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    mul-float p1, p1, v2

    goto :goto_1

    :cond_1
    div-float/2addr v2, p1

    .line 149
    iget v4, v0, Lcom/pspdfkit/utils/Size;->height:F

    .line 150
    invoke-static {p1, v4}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {v3, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    mul-float v2, v2, p1

    .line 151
    :goto_1
    iget v3, p3, Landroid/graphics/PointF;->x:F

    iget p3, p3, Landroid/graphics/PointF;->y:F

    .line 152
    new-instance v4, Landroid/graphics/RectF;

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v2, v5

    sub-float v6, v3, v2

    div-float/2addr p1, v5

    add-float v5, p3, p1

    add-float/2addr v3, v2

    sub-float/2addr p3, p1

    invoke-direct {v4, v6, v5, v3, p3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 153
    new-instance p1, Landroid/graphics/RectF;

    iget p3, v0, Lcom/pspdfkit/utils/Size;->height:F

    iget v0, v0, Lcom/pspdfkit/utils/Size;->width:F

    const/4 v2, 0x0

    invoke-direct {p1, v2, p3, v0, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-static {v4, p1}, Lcom/pspdfkit/internal/ga;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 155
    invoke-direct {p0, p2}, Lcom/pspdfkit/signatures/Signature;->toAnnotation(I)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    .line 156
    invoke-virtual {p1, v4, v1}, Lcom/pspdfkit/annotations/Annotation;->updateTransformationProperties(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 157
    invoke-virtual {p1, v4}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    return-object p1
.end method

.method private toAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/RectF;)Lcom/pspdfkit/annotations/Annotation;
    .locals 5

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 52
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "targetRect"

    .line 53
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-static {p3, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 105
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p3}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    invoke-virtual {p3}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 106
    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/signatures/Signature;->toAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/PointF;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    .line 107
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p2

    .line 108
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v0

    .line 109
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result p2

    neg-float p2, p2

    .line 110
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v1

    .line 111
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v2

    neg-float v2, v2

    div-float v3, v0, p2

    div-float v4, v1, v2

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    div-float v3, v1, v0

    goto :goto_0

    :cond_0
    div-float v3, v2, p2

    :goto_0
    mul-float v0, v0, v3

    mul-float p2, p2, v3

    .line 112
    iget v3, p3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v1, v4

    add-float/2addr v1, v3

    .line 113
    iget p3, p3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, p2

    div-float/2addr v2, v4

    sub-float/2addr p3, v2

    .line 115
    new-instance v2, Landroid/graphics/RectF;

    add-float/2addr v0, v1

    sub-float p2, p3, p2

    invoke-direct {v2, v1, p3, v0, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 116
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/pspdfkit/annotations/Annotation;->updateTransformationProperties(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 117
    invoke-virtual {p1, v2}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    return-object p1
.end method

.method private toInkAnnotation(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/annotations/InkAnnotation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassCastException;
        }
    .end annotation

    .line 4
    instance-of v0, p1, Lcom/pspdfkit/annotations/InkAnnotation;

    if-eqz v0, :cond_0

    .line 5
    check-cast p1, Lcom/pspdfkit/annotations/InkAnnotation;

    return-object p1

    .line 7
    :cond_0
    new-instance p1, Ljava/lang/ClassCastException;

    const-string v0, "This is a stamp image signature. Please use Signature#toStampAnnotation instead."

    invoke-direct {p1, v0}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private toStampAnnotation(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/annotations/StampAnnotation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassCastException;
        }
    .end annotation

    .line 4
    instance-of v0, p1, Lcom/pspdfkit/annotations/StampAnnotation;

    if-eqz v0, :cond_0

    .line 5
    check-cast p1, Lcom/pspdfkit/annotations/StampAnnotation;

    return-object p1

    .line 7
    :cond_0
    new-instance p1, Ljava/lang/ClassCastException;

    const-string v0, "This is an ink signature. Please use Signature#toInkAnnotation instead."

    invoke-direct {p1, v0}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public abstract getAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;
.end method

.method public abstract getBiometricData()Lcom/pspdfkit/signatures/BiometricSignatureData;
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getBitmapIdentifier()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 5
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->s()Lcom/pspdfkit/signatures/SignatureBitmapStorage;

    move-result-object v0

    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getBitmapIdentifier()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/signatures/SignatureBitmapStorage;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public abstract getBitmapIdentifier()I
.end method

.method public getBoundingBox()Landroid/graphics/RectF;
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getStampRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    return-object v0

    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_9

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/signatures/Signature;->inkLinesBoundingBox:Landroid/graphics/RectF;

    if-eqz v0, :cond_1

    return-object v0

    .line 10
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getLines()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    .line 11
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 14
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 15
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    .line 16
    iget v6, v5, Landroid/graphics/PointF;->x:F

    cmpl-float v7, v6, v3

    if-lez v7, :cond_4

    move v3, v6

    .line 17
    :cond_4
    iget v5, v5, Landroid/graphics/PointF;->y:F

    cmpl-float v6, v5, v2

    if-lez v6, :cond_3

    move v2, v5

    goto :goto_0

    .line 20
    :cond_5
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v2, v3, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_1

    .line 22
    :cond_6
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 27
    :goto_1
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v1

    if-nez v2, :cond_7

    .line 28
    iput v3, v0, Landroid/graphics/RectF;->right:F

    .line 30
    :cond_7
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    cmpl-float v1, v2, v1

    if-nez v1, :cond_8

    .line 31
    iput v3, v0, Landroid/graphics/RectF;->top:F

    .line 32
    :cond_8
    iput-object v0, p0, Lcom/pspdfkit/signatures/Signature;->inkLinesBoundingBox:Landroid/graphics/RectF;

    .line 38
    iget v1, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getLineWidth()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v2, v1

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/signatures/Signature;->inkLinesBoundingBox:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getLineWidth()F

    move-result v2

    div-float/2addr v2, v3

    add-float/2addr v2, v1

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 40
    iget-object v0, p0, Lcom/pspdfkit/signatures/Signature;->inkLinesBoundingBox:Landroid/graphics/RectF;

    return-object v0

    .line 44
    :cond_9
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid annotation type. Please make sure your Signature was properly created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract getId()J
.end method

.method public abstract getInkColor()I
.end method

.method public abstract getLineWidth()F
.end method

.method public abstract getLines()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getSignatureDrawWidthRatio()F
.end method

.method public abstract getSignerIdentifier()Ljava/lang/String;
.end method

.method public abstract getStampRect()Landroid/graphics/RectF;
.end method

.method public toInkAnnotation(I)Lcom/pspdfkit/annotations/InkAnnotation;
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/Signature;->toAnnotation(I)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/Signature;->toInkAnnotation(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/annotations/InkAnnotation;

    move-result-object p1

    return-object p1
.end method

.method public toInkAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/PointF;)Lcom/pspdfkit/annotations/InkAnnotation;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/signatures/Signature;->toAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/PointF;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/Signature;->toInkAnnotation(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/annotations/InkAnnotation;

    move-result-object p1

    return-object p1
.end method

.method public toInkAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/RectF;)Lcom/pspdfkit/annotations/InkAnnotation;
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/signatures/Signature;->toAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/RectF;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/Signature;->toInkAnnotation(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/annotations/InkAnnotation;

    move-result-object p1

    return-object p1
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v3, 0x0

    if-ne v1, v2, :cond_2

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getInkColor()I

    move-result v1

    const-string v2, "inkColor"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getLineWidth()F

    move-result v1

    float-to-double v1, v1

    const-string v4, "lineWidthPdf"

    invoke-virtual {v0, v4, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 7
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getLines()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 9
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 10
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    .line 11
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 12
    iget v8, v6, Landroid/graphics/PointF;->x:F

    float-to-double v8, v8

    const-string v10, "x"

    invoke-virtual {v7, v10, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 13
    iget v6, v6, Landroid/graphics/PointF;->y:F

    float-to-double v8, v6

    const-string v6, "y"

    invoke-virtual {v7, v6, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 14
    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 16
    :cond_0
    invoke-virtual {v1, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    :cond_1
    const-string v2, "lines"

    .line 18
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 20
    :cond_2
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    .line 21
    new-instance v2, Lorg/json/JSONArray;

    const/4 v4, 0x4

    new-array v4, v4, [F

    iget v5, v1, Landroid/graphics/RectF;->left:F

    aput v5, v4, v3

    iget v5, v1, Landroid/graphics/RectF;->top:F

    const/4 v6, 0x1

    aput v5, v4, v6

    iget v5, v1, Landroid/graphics/RectF;->right:F

    const/4 v6, 0x2

    aput v5, v4, v6

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    const/4 v5, 0x3

    aput v1, v4, v5

    invoke-direct {v2, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/Object;)V

    const-string v1, "stampRect"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 24
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 25
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 26
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {v1, v4, v5, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 27
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-static {v1, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    const-string v2, "bitmap"

    .line 28
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 31
    :goto_2
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getSignerIdentifier()Ljava/lang/String;

    move-result-object v1

    const-string v2, "signerIdentifier"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 33
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getSignatureDrawWidthRatio()F

    move-result v1

    float-to-double v1, v1

    const-string v4, "drawWidthRatio"

    invoke-virtual {v0, v4, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 36
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/Signature;->getBiometricData()Lcom/pspdfkit/signatures/BiometricSignatureData;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_3

    goto/16 :goto_7

    .line 37
    :cond_3
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v5, "pressurePoints"

    .line 39
    invoke-virtual {v1}, Lcom/pspdfkit/signatures/BiometricSignatureData;->getPressurePoints()Ljava/util/List;

    move-result-object v6

    if-nez v6, :cond_4

    move-object v7, v2

    goto :goto_4

    .line 40
    :cond_4
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 41
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 42
    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_3

    .line 43
    :cond_5
    :goto_4
    invoke-virtual {v4, v5, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "timePoints"

    .line 44
    invoke-virtual {v1}, Lcom/pspdfkit/signatures/BiometricSignatureData;->getTimePoints()Ljava/util/List;

    move-result-object v6

    if-nez v6, :cond_6

    move-object v7, v2

    goto :goto_6

    .line 45
    :cond_6
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 46
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 47
    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_5

    .line 48
    :cond_7
    :goto_6
    invoke-virtual {v4, v5, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "inputMethod"

    .line 51
    invoke-virtual {v1}, Lcom/pspdfkit/signatures/BiometricSignatureData;->getInputMethod()Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 52
    invoke-virtual {v1}, Lcom/pspdfkit/signatures/BiometricSignatureData;->getInputMethod()Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    .line 53
    :cond_8
    invoke-virtual {v4, v5, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "touchRadius"

    .line 58
    invoke-virtual {v1}, Lcom/pspdfkit/signatures/BiometricSignatureData;->getTouchRadius()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v4

    :goto_7
    const-string v1, "biometricData"

    .line 59
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-object v0

    :catch_0
    move-exception v0

    new-array v1, v3, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Signatures"

    const-string v3, "Error while serializing biometric signature data."

    .line 60
    invoke-static {v2, v0, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    invoke-static {v0}, Lio/reactivex/rxjava3/exceptions/Exceptions;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public toStampAnnotation(I)Lcom/pspdfkit/annotations/StampAnnotation;
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/Signature;->toAnnotation(I)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/Signature;->toStampAnnotation(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/annotations/StampAnnotation;

    move-result-object p1

    return-object p1
.end method

.method public toStampAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/PointF;)Lcom/pspdfkit/annotations/StampAnnotation;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/signatures/Signature;->toAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/PointF;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/Signature;->toStampAnnotation(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/annotations/StampAnnotation;

    move-result-object p1

    return-object p1
.end method

.method public toStampAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/RectF;)Lcom/pspdfkit/annotations/StampAnnotation;
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/signatures/Signature;->toAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/RectF;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/signatures/Signature;->toStampAnnotation(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/annotations/StampAnnotation;

    move-result-object p1

    return-object p1
.end method

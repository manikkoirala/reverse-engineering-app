.class public Lcom/pspdfkit/signatures/contents/PKCS7;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final nativePKCS7:Lcom/pspdfkit/internal/jni/NativePlatformPKCS7;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/jni/NativePlatformPKCS7;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/signatures/contents/PKCS7;->nativePKCS7:Lcom/pspdfkit/internal/jni/NativePlatformPKCS7;

    return-void
.end method

.method public static create([BLjava/security/KeyStore$PrivateKeyEntry;Lcom/pspdfkit/signatures/HashAlgorithm;Lcom/pspdfkit/signatures/FilterSubtype;)Lcom/pspdfkit/signatures/contents/PKCS7;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateEncodingException;
        }
    .end annotation

    const-string v0, "digest"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 52
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "privateKey"

    .line 53
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "hashAlgorithm"

    .line 105
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 157
    invoke-virtual {p1}, Ljava/security/KeyStore$PrivateKeyEntry;->getCertificate()Ljava/security/cert/Certificate;

    move-result-object v0

    instance-of v0, v0, Ljava/security/cert/X509Certificate;

    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {p1}, Ljava/security/KeyStore$PrivateKeyEntry;->getCertificate()Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    const/4 v1, 0x0

    .line 164
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/vr;->a(Ljava/security/cert/X509Certificate;Z)Lcom/pspdfkit/internal/jni/NativeX509Certificate;

    move-result-object v0

    .line 165
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->a(Ljava/security/KeyStore$PrivateKeyEntry;)Lcom/pspdfkit/internal/jni/NativePrivateKey;

    move-result-object v3

    .line 166
    const-class p1, Lcom/pspdfkit/internal/jni/NativeFilterSubtype;

    invoke-static {p1, p3}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/pspdfkit/internal/jni/NativeFilterSubtype;

    .line 167
    new-instance v4, Ljava/util/ArrayList;

    .line 170
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v4, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 171
    invoke-static {p2}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/signatures/HashAlgorithm;)Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;

    move-result-object v5

    .line 172
    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativePrivateKey;->encryptionAlgorithm()Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;

    move-result-object v6

    move-object v2, p0

    .line 173
    invoke-static/range {v1 .. v6}, Lcom/pspdfkit/internal/jni/NativePlatformPKCS7;->create(Lcom/pspdfkit/internal/jni/NativeFilterSubtype;[BLcom/pspdfkit/internal/jni/NativePrivateKey;Ljava/util/ArrayList;Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;)Lcom/pspdfkit/internal/jni/NativePlatformPKCS7;

    move-result-object p0

    .line 180
    new-instance p1, Lcom/pspdfkit/signatures/contents/PKCS7;

    invoke-direct {p1, p0}, Lcom/pspdfkit/signatures/contents/PKCS7;-><init>(Lcom/pspdfkit/internal/jni/NativePlatformPKCS7;)V

    return-object p1

    .line 181
    :cond_0
    new-instance p0, Ljava/security/cert/CertificateEncodingException;

    const-string p1, "The certificate inside the private key must be a X.509 certificate."

    invoke-direct {p0, p1}, Ljava/security/cert/CertificateEncodingException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public getPKCS7EncodedData()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/signatures/contents/PKCS7;->nativePKCS7:Lcom/pspdfkit/internal/jni/NativePlatformPKCS7;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativePlatformPKCS7;->data()[B

    move-result-object v0

    return-object v0
.end method

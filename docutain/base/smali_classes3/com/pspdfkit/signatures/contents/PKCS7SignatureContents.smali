.class public Lcom/pspdfkit/signatures/contents/PKCS7SignatureContents;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/signatures/contents/SignatureContents;


# instance fields
.field private final pkcs7:Lcom/pspdfkit/signatures/contents/PKCS7;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/forms/SignatureFormField;Ljava/security/KeyStore$PrivateKeyEntry;Lcom/pspdfkit/signatures/HashAlgorithm;Lcom/pspdfkit/signatures/FilterSubtype;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateEncodingException;
        }
    .end annotation

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "signatureFormField"

    .line 4
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signingKey"

    .line 5
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hashAlgorithm"

    .line 6
    invoke-static {p3, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    .line 12
    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    .line 13
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Document for form field with FQN "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getFullyQualifiedName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " was null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 16
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getDocumentSources()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    const-string v1, "This constructor can\'t be used with compound documents!"

    .line 21
    invoke-static {v1, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    .line 27
    invoke-virtual {p1}, Lcom/pspdfkit/forms/SignatureFormField;->getSignatureInfo()Lcom/pspdfkit/signatures/DigitalSignatureInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/signatures/DigitalSignatureInfo;->getByteRange()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 33
    invoke-virtual {v0, v2, p1, p3}, Lcom/pspdfkit/internal/zf;->getHashForDocumentRange(ILjava/util/List;Lcom/pspdfkit/signatures/HashAlgorithm;)[B

    move-result-object p1

    .line 35
    invoke-static {p1, p2, p3, p4}, Lcom/pspdfkit/signatures/contents/PKCS7;->create([BLjava/security/KeyStore$PrivateKeyEntry;Lcom/pspdfkit/signatures/HashAlgorithm;Lcom/pspdfkit/signatures/FilterSubtype;)Lcom/pspdfkit/signatures/contents/PKCS7;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/signatures/contents/PKCS7SignatureContents;->pkcs7:Lcom/pspdfkit/signatures/contents/PKCS7;

    return-void

    .line 36
    :cond_1
    new-instance p1, Lcom/pspdfkit/signatures/SigningFailedException;

    const-string p2, "Can\'t retrieve signature byte range."

    invoke-direct {p1, p2}, Lcom/pspdfkit/signatures/SigningFailedException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>([BLjava/security/KeyStore$PrivateKeyEntry;Lcom/pspdfkit/signatures/HashAlgorithm;Lcom/pspdfkit/signatures/FilterSubtype;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateEncodingException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1, p2, p3, p4}, Lcom/pspdfkit/signatures/contents/PKCS7;->create([BLjava/security/KeyStore$PrivateKeyEntry;Lcom/pspdfkit/signatures/HashAlgorithm;Lcom/pspdfkit/signatures/FilterSubtype;)Lcom/pspdfkit/signatures/contents/PKCS7;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/signatures/contents/PKCS7SignatureContents;->pkcs7:Lcom/pspdfkit/signatures/contents/PKCS7;

    return-void
.end method


# virtual methods
.method public signData([B)[B
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/signatures/contents/PKCS7SignatureContents;->pkcs7:Lcom/pspdfkit/signatures/contents/PKCS7;

    invoke-virtual {p1}, Lcom/pspdfkit/signatures/contents/PKCS7;->getPKCS7EncodedData()[B

    move-result-object p1

    return-object p1
.end method

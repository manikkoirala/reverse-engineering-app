.class public final Lcom/pspdfkit/signatures/DigitalSignatureValidator;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$validateSignatureAsync$0(Lcom/pspdfkit/signatures/DigitalSignatureInfo;)Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/DigitalSignatureInfo;->getFormField()Lcom/pspdfkit/internal/jni/NativeFormField;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentSignatureValidator;->create(Lcom/pspdfkit/internal/jni/NativeFormField;)Lcom/pspdfkit/internal/jni/NativeDocumentSignatureValidator;

    move-result-object v0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->t()Lcom/pspdfkit/internal/tr;

    move-result-object v1

    .line 5
    invoke-virtual {v1}, Lcom/pspdfkit/internal/tr;->e()Lcom/pspdfkit/internal/jni/NativeKeyStore;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentSignatureValidator;->verifyDocument(Lcom/pspdfkit/internal/jni/NativeKeyStore;)Lcom/pspdfkit/internal/jni/NativeSignatureValidationResult;

    move-result-object v0

    .line 6
    new-instance v1, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;

    invoke-static {p0}, Lcom/pspdfkit/signatures/DigitalSignatureValidator;->wasDocumentModified(Lcom/pspdfkit/signatures/DigitalSignatureInfo;)Z

    move-result p0

    invoke-direct {v1, v0, p0}, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;-><init>(Lcom/pspdfkit/internal/jni/NativeSignatureValidationResult;Z)V

    return-object v1
.end method

.method public static validateSignature(Lcom/pspdfkit/signatures/DigitalSignatureInfo;)Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/signatures/DigitalSignatureValidator;->validateSignatureAsync(Lcom/pspdfkit/signatures/DigitalSignatureInfo;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    invoke-virtual {p0}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;

    return-object p0
.end method

.method public static validateSignatureAsync(Lcom/pspdfkit/signatures/DigitalSignatureInfo;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/signatures/DigitalSignatureInfo;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/signatures/DigitalSignatureValidationResult;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_1

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DIGITAL_SIGNATURES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    new-instance v0, Lcom/pspdfkit/signatures/DigitalSignatureValidator$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/signatures/DigitalSignatureValidator$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/signatures/DigitalSignatureInfo;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0

    .line 6
    :cond_0
    new-instance p0, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Validating signatures of a PDF document requires the digital signature feature in your license."

    invoke-direct {p0, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 7
    :cond_1
    new-instance p0, Ljava/lang/NullPointerException;

    const-string v0, "digitalSignatureInfo may not be null."

    invoke-direct {p0, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static wasDocumentModified(Lcom/pspdfkit/signatures/DigitalSignatureInfo;)Z
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/DigitalSignatureInfo;->getByteRange()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x4

    if-lt v2, v3, :cond_1

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/DigitalSignatureInfo;->getDocumentInternal()Lcom/pspdfkit/internal/zf;

    move-result-object v2

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/DigitalSignatureInfo;->getDocumentSourceIndex()I

    move-result p0

    .line 6
    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getDocumentSources()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge p0, v3, :cond_1

    .line 8
    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getDocumentSources()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/document/DocumentSource;

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSource;->isFileSource()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 10
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object p0

    invoke-interface {p0}, Lcom/pspdfkit/document/providers/DataProvider;->getSize()J

    move-result-wide v2

    :goto_0
    const/4 p0, 0x2

    .line 12
    invoke-interface {v0, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 p0, 0x3

    invoke-interface {v0, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v6, v4

    cmp-long p0, v6, v2

    if-eqz p0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

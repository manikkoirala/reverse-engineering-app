.class public interface abstract Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onDismiss()V
.end method

.method public abstract onSignatureCreated(Lcom/pspdfkit/signatures/Signature;Z)V
.end method

.method public abstract onSignaturePicked(Lcom/pspdfkit/signatures/Signature;)V
.end method

.method public abstract onSignatureUiDataCollected(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V
.end method

.class public Lcom/pspdfkit/signatures/SignatureAppearance$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/signatures/SignatureAppearance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private reuseExistingSignatureAppearanceStream:Z

.field private showSignDate:Z

.field private showSignatureLocation:Z

.field private showSignatureReason:Z

.field private showSignerName:Z

.field private showWatermark:Z

.field private signatureAppearanceMode:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

.field private signatureGraphic:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

.field private signatureWatermark:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    sget-object v0, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;->SIGNATURE_AND_DESCRIPTION:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

    iput-object v0, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->signatureAppearanceMode:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showSignerName:Z

    .line 8
    iput-boolean v0, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showSignDate:Z

    const/4 v1, 0x0

    .line 9
    iput-boolean v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showSignatureReason:Z

    .line 10
    iput-boolean v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showSignatureLocation:Z

    const/4 v1, 0x0

    .line 12
    iput-object v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->signatureGraphic:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    .line 15
    iput-object v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->signatureWatermark:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    .line 18
    iput-boolean v0, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->reuseExistingSignatureAppearanceStream:Z

    .line 19
    iput-boolean v0, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showWatermark:Z

    return-void
.end method


# virtual methods
.method public build()Lcom/pspdfkit/signatures/SignatureAppearance;
    .locals 11

    .line 1
    new-instance v10, Lcom/pspdfkit/signatures/AutoValue_SignatureAppearance;

    iget-object v1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->signatureAppearanceMode:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

    iget-boolean v2, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showSignerName:Z

    iget-boolean v3, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showSignDate:Z

    iget-boolean v4, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showSignatureReason:Z

    iget-boolean v5, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showSignatureLocation:Z

    iget-object v6, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->signatureGraphic:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    iget-object v7, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->signatureWatermark:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    iget-boolean v8, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->reuseExistingSignatureAppearanceStream:Z

    iget-boolean v9, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showWatermark:Z

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/pspdfkit/signatures/AutoValue_SignatureAppearance;-><init>(Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;ZZZZLcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;ZZ)V

    return-object v10
.end method

.method public setReuseExistingSignatureAppearanceStream(Z)Lcom/pspdfkit/signatures/SignatureAppearance$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->reuseExistingSignatureAppearanceStream:Z

    return-object p0
.end method

.method public setShowSignDate(Z)Lcom/pspdfkit/signatures/SignatureAppearance$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showSignDate:Z

    return-object p0
.end method

.method public setShowSignatureLocation(Z)Lcom/pspdfkit/signatures/SignatureAppearance$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showSignatureLocation:Z

    return-object p0
.end method

.method public setShowSignatureReason(Z)Lcom/pspdfkit/signatures/SignatureAppearance$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showSignatureReason:Z

    return-object p0
.end method

.method public setShowSignerName(Z)Lcom/pspdfkit/signatures/SignatureAppearance$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showSignerName:Z

    return-object p0
.end method

.method public setShowWatermark(Z)Lcom/pspdfkit/signatures/SignatureAppearance$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->showWatermark:Z

    return-object p0
.end method

.method public setSignatureAppearanceMode(Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;)Lcom/pspdfkit/signatures/SignatureAppearance$Builder;
    .locals 2

    const-string v0, "signatureAppearanceMode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->signatureAppearanceMode:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

    return-object p0
.end method

.method public setSignatureGraphic(Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;)Lcom/pspdfkit/signatures/SignatureAppearance$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->signatureGraphic:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    return-object p0
.end method

.method public setSignatureWatermark(Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;)Lcom/pspdfkit/signatures/SignatureAppearance$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/signatures/SignatureAppearance$Builder;->signatureWatermark:Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    return-object p0
.end method

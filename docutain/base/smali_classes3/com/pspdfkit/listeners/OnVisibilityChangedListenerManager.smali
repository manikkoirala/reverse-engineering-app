.class public Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/listeners/OnVisibilityChangedListener;


# instance fields
.field private final listeners:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/listeners/OnVisibilityChangedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->listeners:Lcom/pspdfkit/internal/nh;

    return-void
.end method


# virtual methods
.method public addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->listeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public clear()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->listeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    return-void
.end method

.method public onHide(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->listeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/listeners/OnVisibilityChangedListener;

    .line 2
    invoke-interface {v1, p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListener;->onHide(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onShow(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->listeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/listeners/OnVisibilityChangedListener;

    .line 2
    invoke-interface {v1, p1}, Lcom/pspdfkit/listeners/OnVisibilityChangedListener;->onShow(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/listeners/OnVisibilityChangedListenerManager;->listeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

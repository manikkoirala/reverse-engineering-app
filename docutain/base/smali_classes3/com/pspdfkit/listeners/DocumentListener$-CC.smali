.class public final synthetic Lcom/pspdfkit/listeners/DocumentListener$-CC;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public static $default$onDocumentClick(Lcom/pspdfkit/listeners/DocumentListener;)Z
    .locals 1
    .param p0, "_this"    # Lcom/pspdfkit/listeners/DocumentListener;

    .line 0
    const/4 v0, 0x0

    return v0
.end method

.method public static $default$onDocumentLoadFailed(Lcom/pspdfkit/listeners/DocumentListener;Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/listeners/DocumentListener;

    .line 0
    return-void
.end method

.method public static $default$onDocumentLoaded(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/listeners/DocumentListener;

    .line 0
    return-void
.end method

.method public static $default$onDocumentSave(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/listeners/DocumentListener;

    .line 0
    const/4 p1, 0x1

    return p1
.end method

.method public static $default$onDocumentSaveCancelled(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/listeners/DocumentListener;

    .line 0
    return-void
.end method

.method public static $default$onDocumentSaveFailed(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/listeners/DocumentListener;

    .line 0
    return-void
.end method

.method public static $default$onDocumentSaved(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/listeners/DocumentListener;

    .line 0
    return-void
.end method

.method public static $default$onDocumentZoomed(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;IF)V
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/listeners/DocumentListener;

    .line 0
    return-void
.end method

.method public static $default$onPageChanged(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/listeners/DocumentListener;

    .line 0
    return-void
.end method

.method public static $default$onPageClick(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/listeners/DocumentListener;

    .line 0
    const/4 p1, 0x0

    return p1
.end method

.method public static $default$onPageUpdated(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0
    .param p0, "_this"    # Lcom/pspdfkit/listeners/DocumentListener;

    .line 0
    return-void
.end method

.class public interface abstract Lcom/pspdfkit/listeners/DocumentListener;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onDocumentClick()Z
.end method

.method public abstract onDocumentLoadFailed(Ljava/lang/Throwable;)V
.end method

.method public abstract onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
.end method

.method public abstract onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
.end method

.method public abstract onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
.end method

.method public abstract onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
.end method

.method public abstract onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
.end method

.method public abstract onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V
.end method

.method public abstract onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
.end method

.method public abstract onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
.end method

.method public abstract onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
.end method

.class public Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;
.super Ljava/lang/Object;
.source "CameraXPageRenderer_a.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Ljava/util/Comparator;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_compare:(Ljava/lang/Object;Ljava/lang/Object;)I:GetCompare_Ljava_lang_Object_Ljava_lang_Object_Handler:Java.Util.IComparatorInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_equals:(Ljava/lang/Object;)Z:GetEquals_Ljava_lang_Object_Handler:Java.Util.IComparatorInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_reversed:()Ljava/util/Comparator;:GetReversedHandler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_thenComparing:(Ljava/util/Comparator;)Ljava/util/Comparator;:GetThenComparing_Ljava_util_Comparator_Handler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_thenComparing:(Ljava/util/function/Function;)Ljava/util/Comparator;:GetThenComparing_Ljava_util_function_Function_Handler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_thenComparing:(Ljava/util/function/Function;Ljava/util/Comparator;)Ljava/util/Comparator;:GetThenComparing_Ljava_util_function_Function_Ljava_util_Comparator_Handler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_thenComparingDouble:(Ljava/util/function/ToDoubleFunction;)Ljava/util/Comparator;:GetThenComparingDouble_Ljava_util_function_ToDoubleFunction_Handler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_thenComparingInt:(Ljava/util/function/ToIntFunction;)Ljava/util/Comparator;:GetThenComparingInt_Ljava_util_function_ToIntFunction_Handler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_thenComparingLong:(Ljava/util/function/ToLongFunction;)Ljava/util/Comparator;:GetThenComparingLong_Ljava_util_function_ToLongFunction_Handler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 24
    const-class v0, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;

    const-string v1, "Droid.CameraXPageRenderer+a, UInterface.Android"

    const-string v2, "n_compare:(Ljava/lang/Object;Ljava/lang/Object;)I:GetCompare_Ljava_lang_Object_Ljava_lang_Object_Handler:Java.Util.IComparatorInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_equals:(Ljava/lang/Object;)Z:GetEquals_Ljava_lang_Object_Handler:Java.Util.IComparatorInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_reversed:()Ljava/util/Comparator;:GetReversedHandler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_thenComparing:(Ljava/util/Comparator;)Ljava/util/Comparator;:GetThenComparing_Ljava_util_Comparator_Handler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_thenComparing:(Ljava/util/function/Function;)Ljava/util/Comparator;:GetThenComparing_Ljava_util_function_Function_Handler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_thenComparing:(Ljava/util/function/Function;Ljava/util/Comparator;)Ljava/util/Comparator;:GetThenComparing_Ljava_util_function_Function_Ljava_util_Comparator_Handler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_thenComparingDouble:(Ljava/util/function/ToDoubleFunction;)Ljava/util/Comparator;:GetThenComparingDouble_Ljava_util_function_ToDoubleFunction_Handler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_thenComparingInt:(Ljava/util/function/ToIntFunction;)Ljava/util/Comparator;:GetThenComparingInt_Ljava_util_function_ToIntFunction_Handler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\nn_thenComparingLong:(Ljava/util/function/ToLongFunction;)Ljava/util/Comparator;:GetThenComparingLong_Ljava_util_function_ToLongFunction_Handler:Java.Util.IComparator, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Droid.CameraXPageRenderer+a, UInterface.Android"

    const-string v2, ""

    .line 32
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_compare(Ljava/lang/Object;Ljava/lang/Object;)I
.end method

.method private native n_equals(Ljava/lang/Object;)Z
.end method

.method private native n_reversed()Ljava/util/Comparator;
.end method

.method private native n_thenComparing(Ljava/util/Comparator;)Ljava/util/Comparator;
.end method

.method private native n_thenComparing(Ljava/util/function/Function;)Ljava/util/Comparator;
.end method

.method private native n_thenComparing(Ljava/util/function/Function;Ljava/util/Comparator;)Ljava/util/Comparator;
.end method

.method private native n_thenComparingDouble(Ljava/util/function/ToDoubleFunction;)Ljava/util/Comparator;
.end method

.method private native n_thenComparingInt(Ljava/util/function/ToIntFunction;)Ljava/util/Comparator;
.end method

.method private native n_thenComparingLong(Ljava/util/function/ToLongFunction;)Ljava/util/Comparator;
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;->n_compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;->n_equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;->refList:Ljava/util/ArrayList;

    .line 113
    :cond_0
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 118
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public reversed()Ljava/util/Comparator;
    .locals 1

    .line 55
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;->n_reversed()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public thenComparing(Ljava/util/Comparator;)Ljava/util/Comparator;
    .locals 0

    .line 63
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;->n_thenComparing(Ljava/util/Comparator;)Ljava/util/Comparator;

    move-result-object p1

    return-object p1
.end method

.method public thenComparing(Ljava/util/function/Function;)Ljava/util/Comparator;
    .locals 0

    .line 71
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;->n_thenComparing(Ljava/util/function/Function;)Ljava/util/Comparator;

    move-result-object p1

    return-object p1
.end method

.method public thenComparing(Ljava/util/function/Function;Ljava/util/Comparator;)Ljava/util/Comparator;
    .locals 0

    .line 79
    invoke-direct {p0, p1, p2}, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;->n_thenComparing(Ljava/util/function/Function;Ljava/util/Comparator;)Ljava/util/Comparator;

    move-result-object p1

    return-object p1
.end method

.method public thenComparingDouble(Ljava/util/function/ToDoubleFunction;)Ljava/util/Comparator;
    .locals 0

    .line 87
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;->n_thenComparingDouble(Ljava/util/function/ToDoubleFunction;)Ljava/util/Comparator;

    move-result-object p1

    return-object p1
.end method

.method public thenComparingInt(Ljava/util/function/ToIntFunction;)Ljava/util/Comparator;
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;->n_thenComparingInt(Ljava/util/function/ToIntFunction;)Ljava/util/Comparator;

    move-result-object p1

    return-object p1
.end method

.method public thenComparingLong(Ljava/util/function/ToLongFunction;)Ljava/util/Comparator;
    .locals 0

    .line 103
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/CameraXPageRenderer_a;->n_thenComparingLong(Ljava/util/function/ToLongFunction;)Ljava/util/Comparator;

    move-result-object p1

    return-object p1
.end method

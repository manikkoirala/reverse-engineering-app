.class public Lcrc64d90d81a035ad11fb/f;
.super Landroidx/camera/core/ImageCapture$OnImageCapturedCallback;
.source "f.java"

# interfaces
.implements Lmono/android/IGCUserPeer;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onCaptureSuccess:(Landroidx/camera/core/ImageProxy;)V:GetOnCaptureSuccess_Landroidx_camera_core_ImageProxy_Handler\nn_onError:(Landroidx/camera/core/ImageCaptureException;)V:GetOnError_Landroidx_camera_core_ImageCaptureException_Handler\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 16
    const-class v0, Lcrc64d90d81a035ad11fb/f;

    const-string v1, "Droid.f, UInterface.Android"

    const-string v2, "n_onCaptureSuccess:(Landroidx/camera/core/ImageProxy;)V:GetOnCaptureSuccess_Landroidx_camera_core_ImageProxy_Handler\nn_onError:(Landroidx/camera/core/ImageCaptureException;)V:GetOnError_Landroidx_camera_core_ImageCaptureException_Handler\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 22
    invoke-direct {p0}, Landroidx/camera/core/ImageCapture$OnImageCapturedCallback;-><init>()V

    .line 23
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/f;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Droid.f, UInterface.Android"

    const-string v2, ""

    .line 24
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Lcrc64d90d81a035ad11fb/CameraXPageRenderer;)V
    .locals 2

    .line 30
    invoke-direct {p0}, Landroidx/camera/core/ImageCapture$OnImageCapturedCallback;-><init>()V

    .line 31
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/f;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Droid.f, UInterface.Android"

    const-string v1, "Droid.CameraXPageRenderer, UInterface.Android"

    .line 32
    invoke-static {p1, v1, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onCaptureSuccess(Landroidx/camera/core/ImageProxy;)V
.end method

.method private native n_onError(Landroidx/camera/core/ImageCaptureException;)V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 55
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/f;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcrc64d90d81a035ad11fb/f;->refList:Ljava/util/ArrayList;

    .line 57
    :cond_0
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/f;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 62
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/f;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onCaptureSuccess(Landroidx/camera/core/ImageProxy;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/f;->n_onCaptureSuccess(Landroidx/camera/core/ImageProxy;)V

    return-void
.end method

.method public onError(Landroidx/camera/core/ImageCaptureException;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/f;->n_onError(Landroidx/camera/core/ImageCaptureException;)V

    return-void
.end method

.class public Lcrc64d90d81a035ad11fb/PDFViewerActivity;
.super Lcom/pspdfkit/ui/PdfActivity;
.source "PDFViewerActivity.java"

# interfaces
.implements Lmono/android/IGCUserPeer;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\nn_onDestroy:()V:GetOnDestroyHandler\nn_onPrepareOptionsMenu:(Landroid/view/Menu;)Z:GetOnPrepareOptionsMenu_Landroid_view_Menu_Handler\nn_onGenerateMenuItemIds:(Ljava/util/List;)Ljava/util/List;:GetOnGenerateMenuItemIds_Ljava_util_List_Handler\nn_onOptionsItemSelected:(Landroid/view/MenuItem;)Z:GetOnOptionsItemSelected_Landroid_view_MenuItem_Handler\nn_onBackPressed:()V:GetOnBackPressedHandler\nn_onDocumentLoadFailed:(Ljava/lang/Throwable;)V:GetOnDocumentLoadFailed_Ljava_lang_Throwable_Handler\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 21
    const-class v0, Lcrc64d90d81a035ad11fb/PDFViewerActivity;

    const-string v1, "Droid.PDFViewerActivity, UInterface.Android"

    const-string v2, "n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\nn_onDestroy:()V:GetOnDestroyHandler\nn_onPrepareOptionsMenu:(Landroid/view/Menu;)Z:GetOnPrepareOptionsMenu_Landroid_view_Menu_Handler\nn_onGenerateMenuItemIds:(Ljava/util/List;)Ljava/util/List;:GetOnGenerateMenuItemIds_Ljava_util_List_Handler\nn_onOptionsItemSelected:(Landroid/view/MenuItem;)Z:GetOnOptionsItemSelected_Landroid_view_MenuItem_Handler\nn_onBackPressed:()V:GetOnBackPressedHandler\nn_onDocumentLoadFailed:(Ljava/lang/Throwable;)V:GetOnDocumentLoadFailed_Ljava_lang_Throwable_Handler\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 27
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfActivity;-><init>()V

    .line 28
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/PDFViewerActivity;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Droid.PDFViewerActivity, UInterface.Android"

    const-string v2, ""

    .line 29
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onBackPressed()V
.end method

.method private native n_onCreate(Landroid/os/Bundle;)V
.end method

.method private native n_onDestroy()V
.end method

.method private native n_onDocumentLoadFailed(Ljava/lang/Throwable;)V
.end method

.method private native n_onGenerateMenuItemIds(Ljava/util/List;)Ljava/util/List;
.end method

.method private native n_onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end method

.method private native n_onPrepareOptionsMenu(Landroid/view/Menu;)Z
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/PDFViewerActivity;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcrc64d90d81a035ad11fb/PDFViewerActivity;->refList:Ljava/util/ArrayList;

    .line 94
    :cond_0
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/PDFViewerActivity;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 99
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/PDFViewerActivity;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .line 76
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/PDFViewerActivity;->n_onBackPressed()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/PDFViewerActivity;->n_onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/PDFViewerActivity;->n_onDestroy()V

    return-void
.end method

.method public onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/PDFViewerActivity;->n_onDocumentLoadFailed(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onGenerateMenuItemIds(Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/PDFViewerActivity;->n_onGenerateMenuItemIds(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/PDFViewerActivity;->n_onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 0

    .line 52
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/PDFViewerActivity;->n_onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

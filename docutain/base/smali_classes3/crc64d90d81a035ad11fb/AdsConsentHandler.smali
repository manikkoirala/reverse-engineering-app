.class public Lcrc64d90d81a035ad11fb/AdsConsentHandler;
.super Ljava/lang/Object;
.source "AdsConsentHandler.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/google/android/ump/ConsentInformation$OnConsentInfoUpdateSuccessListener;
.implements Lcom/google/android/ump/ConsentInformation$OnConsentInfoUpdateFailureListener;
.implements Lcom/google/android/ump/UserMessagingPlatform$OnConsentFormLoadFailureListener;
.implements Lcom/google/android/ump/UserMessagingPlatform$OnConsentFormLoadSuccessListener;
.implements Lcom/google/android/ump/ConsentForm$OnConsentFormDismissedListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onConsentInfoUpdateSuccess:()V:GetOnConsentInfoUpdateSuccessHandler:Xamarin.Google.UserMesssagingPlatform.IConsentInformationOnConsentInfoUpdateSuccessListenerInvoker, Xamarin.Google.UserMessagingPlatform\nn_onConsentInfoUpdateFailure:(Lcom/google/android/ump/FormError;)V:GetOnConsentInfoUpdateFailure_Lcom_google_android_ump_FormError_Handler:Xamarin.Google.UserMesssagingPlatform.IConsentInformationOnConsentInfoUpdateFailureListenerInvoker, Xamarin.Google.UserMessagingPlatform\nn_onConsentFormLoadFailure:(Lcom/google/android/ump/FormError;)V:GetOnConsentFormLoadFailure_Lcom_google_android_ump_FormError_Handler:Xamarin.Google.UserMesssagingPlatform.UserMessagingPlatform/IOnConsentFormLoadFailureListenerInvoker, Xamarin.Google.UserMessagingPlatform\nn_onConsentFormLoadSuccess:(Lcom/google/android/ump/ConsentForm;)V:GetOnConsentFormLoadSuccess_Lcom_google_android_ump_ConsentForm_Handler:Xamarin.Google.UserMesssagingPlatform.UserMessagingPlatform/IOnConsentFormLoadSuccessListenerInvoker, Xamarin.Google.UserMessagingPlatform\nn_onConsentFormDismissed:(Lcom/google/android/ump/FormError;)V:GetOnConsentFormDismissed_Lcom_google_android_ump_FormError_Handler:Xamarin.Google.UserMesssagingPlatform.IConsentFormOnConsentFormDismissedListenerInvoker, Xamarin.Google.UserMessagingPlatform\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 24
    const-class v0, Lcrc64d90d81a035ad11fb/AdsConsentHandler;

    const-string v1, "Droid.AdsConsentHandler, UInterface.Android"

    const-string v2, "n_onConsentInfoUpdateSuccess:()V:GetOnConsentInfoUpdateSuccessHandler:Xamarin.Google.UserMesssagingPlatform.IConsentInformationOnConsentInfoUpdateSuccessListenerInvoker, Xamarin.Google.UserMessagingPlatform\nn_onConsentInfoUpdateFailure:(Lcom/google/android/ump/FormError;)V:GetOnConsentInfoUpdateFailure_Lcom_google_android_ump_FormError_Handler:Xamarin.Google.UserMesssagingPlatform.IConsentInformationOnConsentInfoUpdateFailureListenerInvoker, Xamarin.Google.UserMessagingPlatform\nn_onConsentFormLoadFailure:(Lcom/google/android/ump/FormError;)V:GetOnConsentFormLoadFailure_Lcom_google_android_ump_FormError_Handler:Xamarin.Google.UserMesssagingPlatform.UserMessagingPlatform/IOnConsentFormLoadFailureListenerInvoker, Xamarin.Google.UserMessagingPlatform\nn_onConsentFormLoadSuccess:(Lcom/google/android/ump/ConsentForm;)V:GetOnConsentFormLoadSuccess_Lcom_google_android_ump_ConsentForm_Handler:Xamarin.Google.UserMesssagingPlatform.UserMessagingPlatform/IOnConsentFormLoadSuccessListenerInvoker, Xamarin.Google.UserMessagingPlatform\nn_onConsentFormDismissed:(Lcom/google/android/ump/FormError;)V:GetOnConsentFormDismissed_Lcom_google_android_ump_FormError_Handler:Xamarin.Google.UserMesssagingPlatform.IConsentFormOnConsentFormDismissedListenerInvoker, Xamarin.Google.UserMessagingPlatform\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/AdsConsentHandler;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Droid.AdsConsentHandler, UInterface.Android"

    const-string v2, ""

    .line 32
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onConsentFormDismissed(Lcom/google/android/ump/FormError;)V
.end method

.method private native n_onConsentFormLoadFailure(Lcom/google/android/ump/FormError;)V
.end method

.method private native n_onConsentFormLoadSuccess(Lcom/google/android/ump/ConsentForm;)V
.end method

.method private native n_onConsentInfoUpdateFailure(Lcom/google/android/ump/FormError;)V
.end method

.method private native n_onConsentInfoUpdateSuccess()V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/AdsConsentHandler;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcrc64d90d81a035ad11fb/AdsConsentHandler;->refList:Ljava/util/ArrayList;

    .line 81
    :cond_0
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/AdsConsentHandler;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 86
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/AdsConsentHandler;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onConsentFormDismissed(Lcom/google/android/ump/FormError;)V
    .locals 0

    .line 71
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/AdsConsentHandler;->n_onConsentFormDismissed(Lcom/google/android/ump/FormError;)V

    return-void
.end method

.method public onConsentFormLoadFailure(Lcom/google/android/ump/FormError;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/AdsConsentHandler;->n_onConsentFormLoadFailure(Lcom/google/android/ump/FormError;)V

    return-void
.end method

.method public onConsentFormLoadSuccess(Lcom/google/android/ump/ConsentForm;)V
    .locals 0

    .line 63
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/AdsConsentHandler;->n_onConsentFormLoadSuccess(Lcom/google/android/ump/ConsentForm;)V

    return-void
.end method

.method public onConsentInfoUpdateFailure(Lcom/google/android/ump/FormError;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/AdsConsentHandler;->n_onConsentInfoUpdateFailure(Lcom/google/android/ump/FormError;)V

    return-void
.end method

.method public onConsentInfoUpdateSuccess()V
    .locals 0

    .line 39
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/AdsConsentHandler;->n_onConsentInfoUpdateSuccess()V

    return-void
.end method

.class public Lcrc64d90d81a035ad11fb/MyAdListener;
.super Lcom/google/android/gms/ads/AdListener;
.source "MyAdListener.java"

# interfaces
.implements Lmono/android/IGCUserPeer;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onAdFailedToLoad:(Lcom/google/android/gms/ads/LoadAdError;)V:GetOnAdFailedToLoad_Lcom_google_android_gms_ads_LoadAdError_Handler\nn_onAdLoaded:()V:GetOnAdLoadedHandler\nn_onAdClicked:()V:GetOnAdClickedHandler\nn_onAdOpened:()V:GetOnAdOpenedHandler\nn_onAdClosed:()V:GetOnAdClosedHandler\nn_onAdImpression:()V:GetOnAdImpressionHandler\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 20
    const-class v0, Lcrc64d90d81a035ad11fb/MyAdListener;

    const-string v1, "Droid.MyAdListener, UInterface.Android"

    const-string v2, "n_onAdFailedToLoad:(Lcom/google/android/gms/ads/LoadAdError;)V:GetOnAdFailedToLoad_Lcom_google_android_gms_ads_LoadAdError_Handler\nn_onAdLoaded:()V:GetOnAdLoadedHandler\nn_onAdClicked:()V:GetOnAdClickedHandler\nn_onAdOpened:()V:GetOnAdOpenedHandler\nn_onAdClosed:()V:GetOnAdClosedHandler\nn_onAdImpression:()V:GetOnAdImpressionHandler\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/ads/AdListener;-><init>()V

    .line 27
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/MyAdListener;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Droid.MyAdListener, UInterface.Android"

    const-string v2, ""

    .line 28
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/ads/AdView;I)V
    .locals 2

    .line 34
    invoke-direct {p0}, Lcom/google/android/gms/ads/AdListener;-><init>()V

    .line 35
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/MyAdListener;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    .line 36
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v0, p1

    const-string p1, "Droid.MyAdListener, UInterface.Android"

    const-string p2, "Android.Gms.Ads.AdView, Xamarin.GooglePlayServices.Ads.Lite:UInterface.AdService+enAdType, UInterface.Android"

    invoke-static {p1, p2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onAdClicked()V
.end method

.method private native n_onAdClosed()V
.end method

.method private native n_onAdFailedToLoad(Lcom/google/android/gms/ads/LoadAdError;)V
.end method

.method private native n_onAdImpression()V
.end method

.method private native n_onAdLoaded()V
.end method

.method private native n_onAdOpened()V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 91
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/MyAdListener;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcrc64d90d81a035ad11fb/MyAdListener;->refList:Ljava/util/ArrayList;

    .line 93
    :cond_0
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/MyAdListener;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 98
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/MyAdListener;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onAdClicked()V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/MyAdListener;->n_onAdClicked()V

    return-void
.end method

.method public onAdClosed()V
    .locals 0

    .line 75
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/MyAdListener;->n_onAdClosed()V

    return-void
.end method

.method public onAdFailedToLoad(Lcom/google/android/gms/ads/LoadAdError;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/MyAdListener;->n_onAdFailedToLoad(Lcom/google/android/gms/ads/LoadAdError;)V

    return-void
.end method

.method public onAdImpression()V
    .locals 0

    .line 83
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/MyAdListener;->n_onAdImpression()V

    return-void
.end method

.method public onAdLoaded()V
    .locals 0

    .line 51
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/MyAdListener;->n_onAdLoaded()V

    return-void
.end method

.method public onAdOpened()V
    .locals 0

    .line 67
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/MyAdListener;->n_onAdOpened()V

    return-void
.end method

.class public Lcrc64d90d81a035ad11fb/FirebaseService;
.super Lcom/google/firebase/messaging/FirebaseMessagingService;
.source "FirebaseService.java"

# interfaces
.implements Lmono/android/IGCUserPeer;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onCreate:()V:GetOnCreateHandler\nn_onNewToken:(Ljava/lang/String;)V:GetOnNewToken_Ljava_lang_String_Handler\nn_onMessageReceived:(Lcom/google/firebase/messaging/RemoteMessage;)V:GetOnMessageReceived_Lcom_google_firebase_messaging_RemoteMessage_Handler\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 17
    const-class v0, Lcrc64d90d81a035ad11fb/FirebaseService;

    const-string v1, "Droid.FirebaseService, UInterface.Android"

    const-string v2, "n_onCreate:()V:GetOnCreateHandler\nn_onNewToken:(Ljava/lang/String;)V:GetOnNewToken_Ljava_lang_String_Handler\nn_onMessageReceived:(Lcom/google/firebase/messaging/RemoteMessage;)V:GetOnMessageReceived_Lcom_google_firebase_messaging_RemoteMessage_Handler\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 23
    invoke-direct {p0}, Lcom/google/firebase/messaging/FirebaseMessagingService;-><init>()V

    .line 24
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/FirebaseService;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Droid.FirebaseService, UInterface.Android"

    const-string v2, ""

    .line 25
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onCreate()V
.end method

.method private native n_onMessageReceived(Lcom/google/firebase/messaging/RemoteMessage;)V
.end method

.method private native n_onNewToken(Ljava/lang/String;)V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/FirebaseService;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcrc64d90d81a035ad11fb/FirebaseService;->refList:Ljava/util/ArrayList;

    .line 58
    :cond_0
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/FirebaseService;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/FirebaseService;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/FirebaseService;->n_onCreate()V

    return-void
.end method

.method public onMessageReceived(Lcom/google/firebase/messaging/RemoteMessage;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/FirebaseService;->n_onMessageReceived(Lcom/google/firebase/messaging/RemoteMessage;)V

    return-void
.end method

.method public onNewToken(Ljava/lang/String;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/FirebaseService;->n_onNewToken(Ljava/lang/String;)V

    return-void
.end method

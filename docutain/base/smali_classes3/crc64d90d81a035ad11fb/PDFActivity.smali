.class public Lcrc64d90d81a035ad11fb/PDFActivity;
.super Lcom/pspdfkit/ui/PdfUiFragment;
.source "PDFActivity.java"

# interfaces
.implements Lmono/android/IGCUserPeer;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onPageChanged:(Lcom/pspdfkit/document/PdfDocument;I)V:GetOnPageChanged_Lcom_pspdfkit_document_PdfDocument_IHandler\nn_onGenerateMenuItemIds:(Ljava/util/List;)Ljava/util/List;:GetOnGenerateMenuItemIds_Ljava_util_List_Handler\nn_onDestroyView:()V:GetOnDestroyViewHandler\nn_onCreateView:(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;:GetOnCreateView_Landroid_view_LayoutInflater_Landroid_view_ViewGroup_Landroid_os_Bundle_Handler\nn_onCreateOptionsMenu:(Landroid/view/Menu;Landroid/view/MenuInflater;)V:GetOnCreateOptionsMenu_Landroid_view_Menu_Landroid_view_MenuInflater_Handler\nn_onBackPressed:()Z:GetOnBackPressedHandler\nn_onOptionsItemSelected:(Landroid/view/MenuItem;)Z:GetOnOptionsItemSelected_Landroid_view_MenuItem_Handler\nn_onSetActivityTitle:(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/PdfDocument;)V:GetOnSetActivityTitle_Lcom_pspdfkit_configuration_activity_PdfActivityConfiguration_Lcom_pspdfkit_document_PdfDocument_Handler\nn_onDocumentLoaded:(Lcom/pspdfkit/document/PdfDocument;)V:GetOnDocumentLoaded_Lcom_pspdfkit_document_PdfDocument_Handler\nn_onDocumentLoadFailed:(Ljava/lang/Throwable;)V:GetOnDocumentLoadFailed_Ljava_lang_Throwable_Handler\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 24
    const-class v0, Lcrc64d90d81a035ad11fb/PDFActivity;

    const-string v1, "Droid.PDFActivity, UInterface.Android"

    const-string v2, "n_onPageChanged:(Lcom/pspdfkit/document/PdfDocument;I)V:GetOnPageChanged_Lcom_pspdfkit_document_PdfDocument_IHandler\nn_onGenerateMenuItemIds:(Ljava/util/List;)Ljava/util/List;:GetOnGenerateMenuItemIds_Ljava_util_List_Handler\nn_onDestroyView:()V:GetOnDestroyViewHandler\nn_onCreateView:(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;:GetOnCreateView_Landroid_view_LayoutInflater_Landroid_view_ViewGroup_Landroid_os_Bundle_Handler\nn_onCreateOptionsMenu:(Landroid/view/Menu;Landroid/view/MenuInflater;)V:GetOnCreateOptionsMenu_Landroid_view_Menu_Landroid_view_MenuInflater_Handler\nn_onBackPressed:()Z:GetOnBackPressedHandler\nn_onOptionsItemSelected:(Landroid/view/MenuItem;)Z:GetOnOptionsItemSelected_Landroid_view_MenuItem_Handler\nn_onSetActivityTitle:(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/PdfDocument;)V:GetOnSetActivityTitle_Lcom_pspdfkit_configuration_activity_PdfActivityConfiguration_Lcom_pspdfkit_document_PdfDocument_Handler\nn_onDocumentLoaded:(Lcom/pspdfkit/document/PdfDocument;)V:GetOnDocumentLoaded_Lcom_pspdfkit_document_PdfDocument_Handler\nn_onDocumentLoadFailed:(Ljava/lang/Throwable;)V:GetOnDocumentLoadFailed_Ljava_lang_Throwable_Handler\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 30
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfUiFragment;-><init>()V

    .line 31
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/PDFActivity;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Droid.PDFActivity, UInterface.Android"

    const-string v2, ""

    .line 32
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onBackPressed()Z
.end method

.method private native n_onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
.end method

.method private native n_onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end method

.method private native n_onDestroyView()V
.end method

.method private native n_onDocumentLoadFailed(Ljava/lang/Throwable;)V
.end method

.method private native n_onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
.end method

.method private native n_onGenerateMenuItemIds(Ljava/util/List;)Ljava/util/List;
.end method

.method private native n_onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end method

.method private native n_onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
.end method

.method private native n_onSetActivityTitle(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/PdfDocument;)V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 119
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/PDFActivity;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcrc64d90d81a035ad11fb/PDFActivity;->refList:Ljava/util/ArrayList;

    .line 121
    :cond_0
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/PDFActivity;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 126
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/PDFActivity;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 79
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/PDFActivity;->n_onBackPressed()Z

    move-result v0

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .line 71
    invoke-direct {p0, p1, p2}, Lcrc64d90d81a035ad11fb/PDFActivity;->n_onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 0

    .line 63
    invoke-direct {p0, p1, p2, p3}, Lcrc64d90d81a035ad11fb/PDFActivity;->n_onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onDestroyView()V
    .locals 0

    .line 55
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/PDFActivity;->n_onDestroyView()V

    return-void
.end method

.method public onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 0

    .line 111
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/PDFActivity;->n_onDocumentLoadFailed(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 103
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/PDFActivity;->n_onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public onGenerateMenuItemIds(Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/PDFActivity;->n_onGenerateMenuItemIds(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 0

    .line 87
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/PDFActivity;->n_onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Lcrc64d90d81a035ad11fb/PDFActivity;->n_onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V

    return-void
.end method

.method public onSetActivityTitle(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2}, Lcrc64d90d81a035ad11fb/PDFActivity;->n_onSetActivityTitle(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

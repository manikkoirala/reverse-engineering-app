.class public Lcrc64d90d81a035ad11fb/AdService;
.super Ljava/lang/Object;
.source "AdService.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/google/android/gms/ads/nativead/NativeAd$OnNativeAdLoadedListener;
.implements Lcom/google/android/gms/ads/OnUserEarnedRewardListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onNativeAdLoaded:(Lcom/google/android/gms/ads/nativead/NativeAd;)V:GetOnNativeAdLoaded_Lcom_google_android_gms_ads_nativead_NativeAd_Handler:Android.Gms.Ads.NativeAd.NativeAd/IOnNativeAdLoadedListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\nn_onUserEarnedReward:(Lcom/google/android/gms/ads/rewarded/RewardItem;)V:GetOnUserEarnedReward_Lcom_google_android_gms_ads_rewarded_RewardItem_Handler:Android.Gms.Ads.IOnUserEarnedRewardListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 18
    const-class v0, Lcrc64d90d81a035ad11fb/AdService;

    const-string v1, "Droid.AdService, UInterface.Android"

    const-string v2, "n_onNativeAdLoaded:(Lcom/google/android/gms/ads/nativead/NativeAd;)V:GetOnNativeAdLoaded_Lcom_google_android_gms_ads_nativead_NativeAd_Handler:Android.Gms.Ads.NativeAd.NativeAd/IOnNativeAdLoadedListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\nn_onUserEarnedReward:(Lcom/google/android/gms/ads/rewarded/RewardItem;)V:GetOnUserEarnedReward_Lcom_google_android_gms_ads_rewarded_RewardItem_Handler:Android.Gms.Ads.IOnUserEarnedRewardListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/AdService;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Droid.AdService, UInterface.Android"

    const-string v2, ""

    .line 26
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onNativeAdLoaded(Lcom/google/android/gms/ads/nativead/NativeAd;)V
.end method

.method private native n_onUserEarnedReward(Lcom/google/android/gms/ads/rewarded/RewardItem;)V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 49
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/AdService;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcrc64d90d81a035ad11fb/AdService;->refList:Ljava/util/ArrayList;

    .line 51
    :cond_0
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/AdService;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 56
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/AdService;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onNativeAdLoaded(Lcom/google/android/gms/ads/nativead/NativeAd;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/AdService;->n_onNativeAdLoaded(Lcom/google/android/gms/ads/nativead/NativeAd;)V

    return-void
.end method

.method public onUserEarnedReward(Lcom/google/android/gms/ads/rewarded/RewardItem;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/AdService;->n_onUserEarnedReward(Lcom/google/android/gms/ads/rewarded/RewardItem;)V

    return-void
.end method

.class public Lcrc64d90d81a035ad11fb/m;
.super Ljava/lang/Object;
.source "m.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/android/installreferrer/api/InstallReferrerStateListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onInstallReferrerServiceDisconnected:()V:GetOnInstallReferrerServiceDisconnectedHandler:Com.Android.Installreferrer.Api.IInstallReferrerStateListenerInvoker, AndroidInstallReferrerBinding\nn_onInstallReferrerSetupFinished:(I)V:GetOnInstallReferrerSetupFinished_IHandler:Com.Android.Installreferrer.Api.IInstallReferrerStateListenerInvoker, AndroidInstallReferrerBinding\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 17
    const-class v0, Lcrc64d90d81a035ad11fb/m;

    const-string v1, "Droid.m, UInterface.Android"

    const-string v2, "n_onInstallReferrerServiceDisconnected:()V:GetOnInstallReferrerServiceDisconnectedHandler:Com.Android.Installreferrer.Api.IInstallReferrerStateListenerInvoker, AndroidInstallReferrerBinding\nn_onInstallReferrerSetupFinished:(I)V:GetOnInstallReferrerSetupFinished_IHandler:Com.Android.Installreferrer.Api.IInstallReferrerStateListenerInvoker, AndroidInstallReferrerBinding\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/m;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Droid.m, UInterface.Android"

    const-string v2, ""

    .line 25
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/android/installreferrer/api/InstallReferrerClient;)V
    .locals 2

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/m;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Droid.m, UInterface.Android"

    const-string v1, "Com.Android.Installreferrer.Api.InstallReferrerClient, AndroidInstallReferrerBinding"

    .line 33
    invoke-static {p1, v1, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onInstallReferrerServiceDisconnected()V
.end method

.method private native n_onInstallReferrerSetupFinished(I)V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/m;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcrc64d90d81a035ad11fb/m;->refList:Ljava/util/ArrayList;

    .line 58
    :cond_0
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/m;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/m;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onInstallReferrerServiceDisconnected()V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/m;->n_onInstallReferrerServiceDisconnected()V

    return-void
.end method

.method public onInstallReferrerSetupFinished(I)V
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcrc64d90d81a035ad11fb/m;->n_onInstallReferrerSetupFinished(I)V

    return-void
.end method

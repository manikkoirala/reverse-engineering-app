.class public Lcrc64d90d81a035ad11fb/EditImagePageRenderer_PDFDataProvider;
.super Lcom/pspdfkit/document/providers/InputStreamDataProvider;
.source "EditImagePageRenderer_PDFDataProvider.java"

# interfaces
.implements Lmono/android/IGCUserPeer;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_getSize:()J:GetGetSizeHandler\nn_getTitle:()Ljava/lang/String;:GetGetTitleHandler\nn_getUid:()Ljava/lang/String;:GetGetUidHandler\nn_openInputStream:()Ljava/io/InputStream;:GetOpenInputStreamHandler\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 18
    const-class v0, Lcrc64d90d81a035ad11fb/EditImagePageRenderer_PDFDataProvider;

    const-string v1, "Droid.EditImagePageRenderer+PDFDataProvider, UInterface.Android"

    const-string v2, "n_getSize:()J:GetGetSizeHandler\nn_getTitle:()Ljava/lang/String;:GetGetTitleHandler\nn_getUid:()Ljava/lang/String;:GetGetUidHandler\nn_openInputStream:()Ljava/io/InputStream;:GetOpenInputStreamHandler\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 24
    invoke-direct {p0}, Lcom/pspdfkit/document/providers/InputStreamDataProvider;-><init>()V

    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/EditImagePageRenderer_PDFDataProvider;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Droid.EditImagePageRenderer+PDFDataProvider, UInterface.Android"

    const-string v2, ""

    .line 26
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2

    .line 32
    invoke-direct {p0}, Lcom/pspdfkit/document/providers/InputStreamDataProvider;-><init>()V

    .line 33
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcrc64d90d81a035ad11fb/EditImagePageRenderer_PDFDataProvider;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Droid.EditImagePageRenderer+PDFDataProvider, UInterface.Android"

    const-string v1, "System.Byte[], mscorlib"

    .line 34
    invoke-static {p1, v1, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_getSize()J
.end method

.method private native n_getTitle()Ljava/lang/String;
.end method

.method private native n_getUid()Ljava/lang/String;
.end method

.method private native n_openInputStream()Ljava/io/InputStream;
.end method


# virtual methods
.method public getSize()J
    .locals 2

    .line 41
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/EditImagePageRenderer_PDFDataProvider;->n_getSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 49
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/EditImagePageRenderer_PDFDataProvider;->n_getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUid()Ljava/lang/String;
    .locals 1

    .line 57
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/EditImagePageRenderer_PDFDataProvider;->n_getUid()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 73
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/EditImagePageRenderer_PDFDataProvider;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcrc64d90d81a035ad11fb/EditImagePageRenderer_PDFDataProvider;->refList:Ljava/util/ArrayList;

    .line 75
    :cond_0
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/EditImagePageRenderer_PDFDataProvider;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 80
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/EditImagePageRenderer_PDFDataProvider;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public openInputStream()Ljava/io/InputStream;
    .locals 1

    .line 65
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/EditImagePageRenderer_PDFDataProvider;->n_openInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

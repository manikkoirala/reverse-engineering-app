.class public Lcrc64d90d81a035ad11fb/MainApplication;
.super Landroid/app/Application;
.source "MainApplication.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Landroidx/camera/core/CameraXConfig$Provider;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onCreate:()V:GetOnCreateHandler\nn_getCameraXConfig:()Landroidx/camera/core/CameraXConfig;:GetGetCameraXConfigHandler:AndroidX.Camera.Core.CameraXConfig/IProviderInvoker, Xamarin.AndroidX.Camera.Core\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 21
    invoke-static {p0}, Lmono/MonoPackageManager;->setContext(Landroid/content/Context;)V

    return-void
.end method

.method private native n_getCameraXConfig()Landroidx/camera/core/CameraXConfig;
.end method

.method private native n_onCreate()V
.end method


# virtual methods
.method public getCameraXConfig()Landroidx/camera/core/CameraXConfig;
    .locals 1

    .line 35
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/MainApplication;->n_getCameraXConfig()Landroidx/camera/core/CameraXConfig;

    move-result-object v0

    return-object v0
.end method

.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/MainApplication;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcrc64d90d81a035ad11fb/MainApplication;->refList:Ljava/util/ArrayList;

    .line 45
    :cond_0
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/MainApplication;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 50
    iget-object v0, p0, Lcrc64d90d81a035ad11fb/MainApplication;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcrc64d90d81a035ad11fb/MainApplication;->n_onCreate()V

    return-void
.end method

.class public Lmono/com/pspdfkit/annotations/AnnotationProvider_OnAnnotationUpdatedListenerImplementor;
.super Ljava/lang/Object;
.source "AnnotationProvider_OnAnnotationUpdatedListenerImplementor.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onAnnotationCreated:(Lcom/pspdfkit/annotations/Annotation;)V:GetOnAnnotationCreated_Lcom_pspdfkit_annotations_Annotation_Handler:PSPDFKit.Annotations.IAnnotationProvider/IOnAnnotationUpdatedListenerInvoker, PSPDFKit.Android\nn_onAnnotationRemoved:(Lcom/pspdfkit/annotations/Annotation;)V:GetOnAnnotationRemoved_Lcom_pspdfkit_annotations_Annotation_Handler:PSPDFKit.Annotations.IAnnotationProvider/IOnAnnotationUpdatedListenerInvoker, PSPDFKit.Android\nn_onAnnotationUpdated:(Lcom/pspdfkit/annotations/Annotation;)V:GetOnAnnotationUpdated_Lcom_pspdfkit_annotations_Annotation_Handler:PSPDFKit.Annotations.IAnnotationProvider/IOnAnnotationUpdatedListenerInvoker, PSPDFKit.Android\nn_onAnnotationZOrderChanged:(ILjava/util/List;Ljava/util/List;)V:GetOnAnnotationZOrderChanged_ILjava_util_List_Ljava_util_List_Handler:PSPDFKit.Annotations.IAnnotationProvider/IOnAnnotationUpdatedListenerInvoker, PSPDFKit.Android\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 19
    const-class v0, Lmono/com/pspdfkit/annotations/AnnotationProvider_OnAnnotationUpdatedListenerImplementor;

    const-string v1, "PSPDFKit.Annotations.IAnnotationProvider+IOnAnnotationUpdatedListenerImplementor, PSPDFKit.Android"

    const-string v2, "n_onAnnotationCreated:(Lcom/pspdfkit/annotations/Annotation;)V:GetOnAnnotationCreated_Lcom_pspdfkit_annotations_Annotation_Handler:PSPDFKit.Annotations.IAnnotationProvider/IOnAnnotationUpdatedListenerInvoker, PSPDFKit.Android\nn_onAnnotationRemoved:(Lcom/pspdfkit/annotations/Annotation;)V:GetOnAnnotationRemoved_Lcom_pspdfkit_annotations_Annotation_Handler:PSPDFKit.Annotations.IAnnotationProvider/IOnAnnotationUpdatedListenerInvoker, PSPDFKit.Android\nn_onAnnotationUpdated:(Lcom/pspdfkit/annotations/Annotation;)V:GetOnAnnotationUpdated_Lcom_pspdfkit_annotations_Annotation_Handler:PSPDFKit.Annotations.IAnnotationProvider/IOnAnnotationUpdatedListenerInvoker, PSPDFKit.Android\nn_onAnnotationZOrderChanged:(ILjava/util/List;Ljava/util/List;)V:GetOnAnnotationZOrderChanged_ILjava_util_List_Ljava_util_List_Handler:PSPDFKit.Annotations.IAnnotationProvider/IOnAnnotationUpdatedListenerInvoker, PSPDFKit.Android\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lmono/com/pspdfkit/annotations/AnnotationProvider_OnAnnotationUpdatedListenerImplementor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Annotations.IAnnotationProvider+IOnAnnotationUpdatedListenerImplementor, PSPDFKit.Android"

    const-string v2, ""

    .line 27
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
.end method

.method private native n_onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
.end method

.method private native n_onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
.end method

.method private native n_onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 66
    iget-object v0, p0, Lmono/com/pspdfkit/annotations/AnnotationProvider_OnAnnotationUpdatedListenerImplementor;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmono/com/pspdfkit/annotations/AnnotationProvider_OnAnnotationUpdatedListenerImplementor;->refList:Ljava/util/ArrayList;

    .line 68
    :cond_0
    iget-object v0, p0, Lmono/com/pspdfkit/annotations/AnnotationProvider_OnAnnotationUpdatedListenerImplementor;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 73
    iget-object v0, p0, Lmono/com/pspdfkit/annotations/AnnotationProvider_OnAnnotationUpdatedListenerImplementor;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/annotations/AnnotationProvider_OnAnnotationUpdatedListenerImplementor;->n_onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/annotations/AnnotationProvider_OnAnnotationUpdatedListenerImplementor;->n_onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/annotations/AnnotationProvider_OnAnnotationUpdatedListenerImplementor;->n_onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0

    .line 58
    invoke-direct {p0, p1, p2, p3}, Lmono/com/pspdfkit/annotations/AnnotationProvider_OnAnnotationUpdatedListenerImplementor;->n_onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V

    return-void
.end method

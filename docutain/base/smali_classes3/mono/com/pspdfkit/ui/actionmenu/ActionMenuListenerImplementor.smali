.class public Lmono/com/pspdfkit/ui/actionmenu/ActionMenuListenerImplementor;
.super Ljava/lang/Object;
.source "ActionMenuListenerImplementor.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onActionMenuItemClicked:(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z:GetOnActionMenuItemClicked_Lcom_pspdfkit_ui_actionmenu_ActionMenu_Lcom_pspdfkit_ui_actionmenu_ActionMenuItem_Handler:PSPDFKit.UI.ActionMenuSdk.IActionMenuListenerInvoker, PSPDFKit.Android\nn_onActionMenuItemLongClicked:(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z:GetOnActionMenuItemLongClicked_Lcom_pspdfkit_ui_actionmenu_ActionMenu_Lcom_pspdfkit_ui_actionmenu_ActionMenuItem_Handler:PSPDFKit.UI.ActionMenuSdk.IActionMenuListenerInvoker, PSPDFKit.Android\nn_onDisplayActionMenu:(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V:GetOnDisplayActionMenu_Lcom_pspdfkit_ui_actionmenu_ActionMenu_Handler:PSPDFKit.UI.ActionMenuSdk.IActionMenuListenerInvoker, PSPDFKit.Android\nn_onPrepareActionMenu:(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)Z:GetOnPrepareActionMenu_Lcom_pspdfkit_ui_actionmenu_ActionMenu_Handler:PSPDFKit.UI.ActionMenuSdk.IActionMenuListenerInvoker, PSPDFKit.Android\nn_onRemoveActionMenu:(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V:GetOnRemoveActionMenu_Lcom_pspdfkit_ui_actionmenu_ActionMenu_Handler:PSPDFKit.UI.ActionMenuSdk.IActionMenuListenerInvoker, PSPDFKit.Android\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 20
    const-class v0, Lmono/com/pspdfkit/ui/actionmenu/ActionMenuListenerImplementor;

    const-string v1, "PSPDFKit.UI.ActionMenuSdk.IActionMenuListenerImplementor, PSPDFKit.Android"

    const-string v2, "n_onActionMenuItemClicked:(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z:GetOnActionMenuItemClicked_Lcom_pspdfkit_ui_actionmenu_ActionMenu_Lcom_pspdfkit_ui_actionmenu_ActionMenuItem_Handler:PSPDFKit.UI.ActionMenuSdk.IActionMenuListenerInvoker, PSPDFKit.Android\nn_onActionMenuItemLongClicked:(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z:GetOnActionMenuItemLongClicked_Lcom_pspdfkit_ui_actionmenu_ActionMenu_Lcom_pspdfkit_ui_actionmenu_ActionMenuItem_Handler:PSPDFKit.UI.ActionMenuSdk.IActionMenuListenerInvoker, PSPDFKit.Android\nn_onDisplayActionMenu:(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V:GetOnDisplayActionMenu_Lcom_pspdfkit_ui_actionmenu_ActionMenu_Handler:PSPDFKit.UI.ActionMenuSdk.IActionMenuListenerInvoker, PSPDFKit.Android\nn_onPrepareActionMenu:(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)Z:GetOnPrepareActionMenu_Lcom_pspdfkit_ui_actionmenu_ActionMenu_Handler:PSPDFKit.UI.ActionMenuSdk.IActionMenuListenerInvoker, PSPDFKit.Android\nn_onRemoveActionMenu:(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V:GetOnRemoveActionMenu_Lcom_pspdfkit_ui_actionmenu_ActionMenu_Handler:PSPDFKit.UI.ActionMenuSdk.IActionMenuListenerInvoker, PSPDFKit.Android\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lmono/com/pspdfkit/ui/actionmenu/ActionMenuListenerImplementor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.UI.ActionMenuSdk.IActionMenuListenerImplementor, PSPDFKit.Android"

    const-string v2, ""

    .line 28
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onActionMenuItemClicked(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z
.end method

.method private native n_onActionMenuItemLongClicked(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z
.end method

.method private native n_onDisplayActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V
.end method

.method private native n_onPrepareActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)Z
.end method

.method private native n_onRemoveActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 75
    iget-object v0, p0, Lmono/com/pspdfkit/ui/actionmenu/ActionMenuListenerImplementor;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmono/com/pspdfkit/ui/actionmenu/ActionMenuListenerImplementor;->refList:Ljava/util/ArrayList;

    .line 77
    :cond_0
    iget-object v0, p0, Lmono/com/pspdfkit/ui/actionmenu/ActionMenuListenerImplementor;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 82
    iget-object v0, p0, Lmono/com/pspdfkit/ui/actionmenu/ActionMenuListenerImplementor;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onActionMenuItemClicked(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Lmono/com/pspdfkit/ui/actionmenu/ActionMenuListenerImplementor;->n_onActionMenuItemClicked(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z

    move-result p1

    return p1
.end method

.method public onActionMenuItemLongClicked(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2}, Lmono/com/pspdfkit/ui/actionmenu/ActionMenuListenerImplementor;->n_onActionMenuItemLongClicked(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z

    move-result p1

    return p1
.end method

.method public onDisplayActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/ui/actionmenu/ActionMenuListenerImplementor;->n_onDisplayActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V

    return-void
.end method

.method public onPrepareActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)Z
    .locals 0

    .line 59
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/ui/actionmenu/ActionMenuListenerImplementor;->n_onPrepareActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)Z

    move-result p1

    return p1
.end method

.method public onRemoveActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V
    .locals 0

    .line 67
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/ui/actionmenu/ActionMenuListenerImplementor;->n_onRemoveActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V

    return-void
.end method

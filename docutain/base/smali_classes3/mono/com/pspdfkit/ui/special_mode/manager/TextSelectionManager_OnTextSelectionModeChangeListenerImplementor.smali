.class public Lmono/com/pspdfkit/ui/special_mode/manager/TextSelectionManager_OnTextSelectionModeChangeListenerImplementor;
.super Ljava/lang/Object;
.source "TextSelectionManager_OnTextSelectionModeChangeListenerImplementor.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/pspdfkit/ui/special_mode/manager/TextSelectionManager$OnTextSelectionModeChangeListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onEnterTextSelectionMode:(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V:GetOnEnterTextSelectionMode_Lcom_pspdfkit_ui_special_mode_controller_TextSelectionController_Handler:PSPDFKit.UI.SpecialMode.Manager.ITextSelectionManager/IOnTextSelectionModeChangeListenerInvoker, PSPDFKit.Android\nn_onExitTextSelectionMode:(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V:GetOnExitTextSelectionMode_Lcom_pspdfkit_ui_special_mode_controller_TextSelectionController_Handler:PSPDFKit.UI.SpecialMode.Manager.ITextSelectionManager/IOnTextSelectionModeChangeListenerInvoker, PSPDFKit.Android\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 17
    const-class v0, Lmono/com/pspdfkit/ui/special_mode/manager/TextSelectionManager_OnTextSelectionModeChangeListenerImplementor;

    const-string v1, "PSPDFKit.UI.SpecialMode.Manager.ITextSelectionManager+IOnTextSelectionModeChangeListenerImplementor, PSPDFKit.Android"

    const-string v2, "n_onEnterTextSelectionMode:(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V:GetOnEnterTextSelectionMode_Lcom_pspdfkit_ui_special_mode_controller_TextSelectionController_Handler:PSPDFKit.UI.SpecialMode.Manager.ITextSelectionManager/IOnTextSelectionModeChangeListenerInvoker, PSPDFKit.Android\nn_onExitTextSelectionMode:(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V:GetOnExitTextSelectionMode_Lcom_pspdfkit_ui_special_mode_controller_TextSelectionController_Handler:PSPDFKit.UI.SpecialMode.Manager.ITextSelectionManager/IOnTextSelectionModeChangeListenerInvoker, PSPDFKit.Android\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lmono/com/pspdfkit/ui/special_mode/manager/TextSelectionManager_OnTextSelectionModeChangeListenerImplementor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.UI.SpecialMode.Manager.ITextSelectionManager+IOnTextSelectionModeChangeListenerImplementor, PSPDFKit.Android"

    const-string v2, ""

    .line 25
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onEnterTextSelectionMode(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V
.end method

.method private native n_onExitTextSelectionMode(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 48
    iget-object v0, p0, Lmono/com/pspdfkit/ui/special_mode/manager/TextSelectionManager_OnTextSelectionModeChangeListenerImplementor;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmono/com/pspdfkit/ui/special_mode/manager/TextSelectionManager_OnTextSelectionModeChangeListenerImplementor;->refList:Ljava/util/ArrayList;

    .line 50
    :cond_0
    iget-object v0, p0, Lmono/com/pspdfkit/ui/special_mode/manager/TextSelectionManager_OnTextSelectionModeChangeListenerImplementor;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 55
    iget-object v0, p0, Lmono/com/pspdfkit/ui/special_mode/manager/TextSelectionManager_OnTextSelectionModeChangeListenerImplementor;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onEnterTextSelectionMode(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/ui/special_mode/manager/TextSelectionManager_OnTextSelectionModeChangeListenerImplementor;->n_onEnterTextSelectionMode(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V

    return-void
.end method

.method public onExitTextSelectionMode(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/ui/special_mode/manager/TextSelectionManager_OnTextSelectionModeChangeListenerImplementor;->n_onExitTextSelectionMode(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V

    return-void
.end method

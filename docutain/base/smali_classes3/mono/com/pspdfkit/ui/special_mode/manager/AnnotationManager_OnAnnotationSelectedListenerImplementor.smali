.class public Lmono/com/pspdfkit/ui/special_mode/manager/AnnotationManager_OnAnnotationSelectedListenerImplementor;
.super Ljava/lang/Object;
.source "AnnotationManager_OnAnnotationSelectedListenerImplementor.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onAnnotationSelected:(Lcom/pspdfkit/annotations/Annotation;Z)V:GetOnAnnotationSelected_Lcom_pspdfkit_annotations_Annotation_ZHandler:PSPDFKit.UI.SpecialMode.Manager.IAnnotationManager/IOnAnnotationSelectedListenerInvoker, PSPDFKit.Android\nn_onPrepareAnnotationSelection:(Lcom/pspdfkit/ui/special_mode/controller/AnnotationSelectionController;Lcom/pspdfkit/annotations/Annotation;Z)Z:GetOnPrepareAnnotationSelection_Lcom_pspdfkit_ui_special_mode_controller_AnnotationSelectionController_Lcom_pspdfkit_annotations_Annotation_ZHandler:PSPDFKit.UI.SpecialMode.Manager.IAnnotationManager/IOnAnnotationSelectedListenerInvoker, PSPDFKit.Android\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 17
    const-class v0, Lmono/com/pspdfkit/ui/special_mode/manager/AnnotationManager_OnAnnotationSelectedListenerImplementor;

    const-string v1, "PSPDFKit.UI.SpecialMode.Manager.IAnnotationManager+IOnAnnotationSelectedListenerImplementor, PSPDFKit.Android"

    const-string v2, "n_onAnnotationSelected:(Lcom/pspdfkit/annotations/Annotation;Z)V:GetOnAnnotationSelected_Lcom_pspdfkit_annotations_Annotation_ZHandler:PSPDFKit.UI.SpecialMode.Manager.IAnnotationManager/IOnAnnotationSelectedListenerInvoker, PSPDFKit.Android\nn_onPrepareAnnotationSelection:(Lcom/pspdfkit/ui/special_mode/controller/AnnotationSelectionController;Lcom/pspdfkit/annotations/Annotation;Z)Z:GetOnPrepareAnnotationSelection_Lcom_pspdfkit_ui_special_mode_controller_AnnotationSelectionController_Lcom_pspdfkit_annotations_Annotation_ZHandler:PSPDFKit.UI.SpecialMode.Manager.IAnnotationManager/IOnAnnotationSelectedListenerInvoker, PSPDFKit.Android\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lmono/com/pspdfkit/ui/special_mode/manager/AnnotationManager_OnAnnotationSelectedListenerImplementor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.UI.SpecialMode.Manager.IAnnotationManager+IOnAnnotationSelectedListenerImplementor, PSPDFKit.Android"

    const-string v2, ""

    .line 25
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onAnnotationSelected(Lcom/pspdfkit/annotations/Annotation;Z)V
.end method

.method private native n_onPrepareAnnotationSelection(Lcom/pspdfkit/ui/special_mode/controller/AnnotationSelectionController;Lcom/pspdfkit/annotations/Annotation;Z)Z
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 48
    iget-object v0, p0, Lmono/com/pspdfkit/ui/special_mode/manager/AnnotationManager_OnAnnotationSelectedListenerImplementor;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmono/com/pspdfkit/ui/special_mode/manager/AnnotationManager_OnAnnotationSelectedListenerImplementor;->refList:Ljava/util/ArrayList;

    .line 50
    :cond_0
    iget-object v0, p0, Lmono/com/pspdfkit/ui/special_mode/manager/AnnotationManager_OnAnnotationSelectedListenerImplementor;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 55
    iget-object v0, p0, Lmono/com/pspdfkit/ui/special_mode/manager/AnnotationManager_OnAnnotationSelectedListenerImplementor;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onAnnotationSelected(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lmono/com/pspdfkit/ui/special_mode/manager/AnnotationManager_OnAnnotationSelectedListenerImplementor;->n_onAnnotationSelected(Lcom/pspdfkit/annotations/Annotation;Z)V

    return-void
.end method

.method public onPrepareAnnotationSelection(Lcom/pspdfkit/ui/special_mode/controller/AnnotationSelectionController;Lcom/pspdfkit/annotations/Annotation;Z)Z
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2, p3}, Lmono/com/pspdfkit/ui/special_mode/manager/AnnotationManager_OnAnnotationSelectedListenerImplementor;->n_onPrepareAnnotationSelection(Lcom/pspdfkit/ui/special_mode/controller/AnnotationSelectionController;Lcom/pspdfkit/annotations/Annotation;Z)Z

    move-result p1

    return p1
.end method

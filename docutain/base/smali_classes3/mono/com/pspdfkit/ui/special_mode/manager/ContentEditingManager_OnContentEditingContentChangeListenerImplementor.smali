.class public Lmono/com/pspdfkit/ui/special_mode/manager/ContentEditingManager_OnContentEditingContentChangeListenerImplementor;
.super Ljava/lang/Object;
.source "ContentEditingManager_OnContentEditingContentChangeListenerImplementor.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onContentChange:(Ljava/util/UUID;)V:GetOnContentChange_Ljava_util_UUID_Handler:PSPDFKit.UI.SpecialMode.Manager.IContentEditingManager/IOnContentEditingContentChangeListener, PSPDFKit.Android\nn_onContentSelectionChange:(Ljava/util/UUID;IIZ)V:GetOnContentSelectionChange_Ljava_util_UUID_IIZHandler:PSPDFKit.UI.SpecialMode.Manager.IContentEditingManager/IOnContentEditingContentChangeListener, PSPDFKit.Android\nn_onFinishEditingContentBlock:(Ljava/util/UUID;)V:GetOnFinishEditingContentBlock_Ljava_util_UUID_Handler:PSPDFKit.UI.SpecialMode.Manager.IContentEditingManager/IOnContentEditingContentChangeListener, PSPDFKit.Android\nn_onStartEditingContentBlock:(Ljava/util/UUID;)V:GetOnStartEditingContentBlock_Ljava_util_UUID_Handler:PSPDFKit.UI.SpecialMode.Manager.IContentEditingManager/IOnContentEditingContentChangeListener, PSPDFKit.Android\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 19
    const-class v0, Lmono/com/pspdfkit/ui/special_mode/manager/ContentEditingManager_OnContentEditingContentChangeListenerImplementor;

    const-string v1, "PSPDFKit.UI.SpecialMode.Manager.IContentEditingManager+IOnContentEditingContentChangeListenerImplementor, PSPDFKit.Android"

    const-string v2, "n_onContentChange:(Ljava/util/UUID;)V:GetOnContentChange_Ljava_util_UUID_Handler:PSPDFKit.UI.SpecialMode.Manager.IContentEditingManager/IOnContentEditingContentChangeListener, PSPDFKit.Android\nn_onContentSelectionChange:(Ljava/util/UUID;IIZ)V:GetOnContentSelectionChange_Ljava_util_UUID_IIZHandler:PSPDFKit.UI.SpecialMode.Manager.IContentEditingManager/IOnContentEditingContentChangeListener, PSPDFKit.Android\nn_onFinishEditingContentBlock:(Ljava/util/UUID;)V:GetOnFinishEditingContentBlock_Ljava_util_UUID_Handler:PSPDFKit.UI.SpecialMode.Manager.IContentEditingManager/IOnContentEditingContentChangeListener, PSPDFKit.Android\nn_onStartEditingContentBlock:(Ljava/util/UUID;)V:GetOnStartEditingContentBlock_Ljava_util_UUID_Handler:PSPDFKit.UI.SpecialMode.Manager.IContentEditingManager/IOnContentEditingContentChangeListener, PSPDFKit.Android\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lmono/com/pspdfkit/ui/special_mode/manager/ContentEditingManager_OnContentEditingContentChangeListenerImplementor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.UI.SpecialMode.Manager.IContentEditingManager+IOnContentEditingContentChangeListenerImplementor, PSPDFKit.Android"

    const-string v2, ""

    .line 27
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onContentChange(Ljava/util/UUID;)V
.end method

.method private native n_onContentSelectionChange(Ljava/util/UUID;IIZ)V
.end method

.method private native n_onFinishEditingContentBlock(Ljava/util/UUID;)V
.end method

.method private native n_onStartEditingContentBlock(Ljava/util/UUID;)V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 66
    iget-object v0, p0, Lmono/com/pspdfkit/ui/special_mode/manager/ContentEditingManager_OnContentEditingContentChangeListenerImplementor;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmono/com/pspdfkit/ui/special_mode/manager/ContentEditingManager_OnContentEditingContentChangeListenerImplementor;->refList:Ljava/util/ArrayList;

    .line 68
    :cond_0
    iget-object v0, p0, Lmono/com/pspdfkit/ui/special_mode/manager/ContentEditingManager_OnContentEditingContentChangeListenerImplementor;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 73
    iget-object v0, p0, Lmono/com/pspdfkit/ui/special_mode/manager/ContentEditingManager_OnContentEditingContentChangeListenerImplementor;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onContentChange(Ljava/util/UUID;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/ui/special_mode/manager/ContentEditingManager_OnContentEditingContentChangeListenerImplementor;->n_onContentChange(Ljava/util/UUID;)V

    return-void
.end method

.method public synthetic onContentSelectionChange(Ljava/util/UUID;IILcom/pspdfkit/internal/jt;Z)V
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener$-CC;->$default$onContentSelectionChange(Lcom/pspdfkit/ui/special_mode/manager/ContentEditingManager$OnContentEditingContentChangeListener;Ljava/util/UUID;IILcom/pspdfkit/internal/jt;Z)V

    return-void
.end method

.method public onContentSelectionChange(Ljava/util/UUID;IIZ)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Lmono/com/pspdfkit/ui/special_mode/manager/ContentEditingManager_OnContentEditingContentChangeListenerImplementor;->n_onContentSelectionChange(Ljava/util/UUID;IIZ)V

    return-void
.end method

.method public onFinishEditingContentBlock(Ljava/util/UUID;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/ui/special_mode/manager/ContentEditingManager_OnContentEditingContentChangeListenerImplementor;->n_onFinishEditingContentBlock(Ljava/util/UUID;)V

    return-void
.end method

.method public onStartEditingContentBlock(Ljava/util/UUID;)V
    .locals 0

    .line 58
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/ui/special_mode/manager/ContentEditingManager_OnContentEditingContentChangeListenerImplementor;->n_onStartEditingContentBlock(Ljava/util/UUID;)V

    return-void
.end method

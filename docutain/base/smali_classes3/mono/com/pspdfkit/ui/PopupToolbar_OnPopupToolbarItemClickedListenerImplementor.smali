.class public Lmono/com/pspdfkit/ui/PopupToolbar_OnPopupToolbarItemClickedListenerImplementor;
.super Ljava/lang/Object;
.source "PopupToolbar_OnPopupToolbarItemClickedListenerImplementor.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarItemClickedListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onItemClicked:(Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)Z:GetOnItemClicked_Lcom_pspdfkit_ui_toolbar_popup_PopupToolbarMenuItem_Handler:PSPDFKit.UI.PopupToolbar/IOnPopupToolbarItemClickedListenerInvoker, PSPDFKit.Android\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 16
    const-class v0, Lmono/com/pspdfkit/ui/PopupToolbar_OnPopupToolbarItemClickedListenerImplementor;

    const-string v1, "PSPDFKit.UI.PopupToolbar+IOnPopupToolbarItemClickedListenerImplementor, PSPDFKit.Android"

    const-string v2, "n_onItemClicked:(Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)Z:GetOnItemClicked_Lcom_pspdfkit_ui_toolbar_popup_PopupToolbarMenuItem_Handler:PSPDFKit.UI.PopupToolbar/IOnPopupToolbarItemClickedListenerInvoker, PSPDFKit.Android\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lmono/com/pspdfkit/ui/PopupToolbar_OnPopupToolbarItemClickedListenerImplementor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.UI.PopupToolbar+IOnPopupToolbarItemClickedListenerImplementor, PSPDFKit.Android"

    const-string v2, ""

    .line 24
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onItemClicked(Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)Z
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lmono/com/pspdfkit/ui/PopupToolbar_OnPopupToolbarItemClickedListenerImplementor;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmono/com/pspdfkit/ui/PopupToolbar_OnPopupToolbarItemClickedListenerImplementor;->refList:Ljava/util/ArrayList;

    .line 41
    :cond_0
    iget-object v0, p0, Lmono/com/pspdfkit/ui/PopupToolbar_OnPopupToolbarItemClickedListenerImplementor;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 46
    iget-object v0, p0, Lmono/com/pspdfkit/ui/PopupToolbar_OnPopupToolbarItemClickedListenerImplementor;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onItemClicked(Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)Z
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/ui/PopupToolbar_OnPopupToolbarItemClickedListenerImplementor;->n_onItemClicked(Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)Z

    move-result p1

    return p1
.end method

.class public Lmono/com/pspdfkit/ui/contentediting/ContentEditingStylingBar_OnContentEditingBarLifecycleListenerImplementor;
.super Ljava/lang/Object;
.source "ContentEditingStylingBar_OnContentEditingBarLifecycleListenerImplementor.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar$OnContentEditingBarLifecycleListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onDisplayContentEditingBar:(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V:GetOnDisplayContentEditingBar_Lcom_pspdfkit_ui_contentediting_ContentEditingStylingBar_Handler:Com.Pspdfkit.UI.Contentediting.ContentEditingStylingBar/IOnContentEditingBarLifecycleListenerInvoker, PSPDFKit.Android\nn_onPrepareContentEditingBar:(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V:GetOnPrepareContentEditingBar_Lcom_pspdfkit_ui_contentediting_ContentEditingStylingBar_Handler:Com.Pspdfkit.UI.Contentediting.ContentEditingStylingBar/IOnContentEditingBarLifecycleListenerInvoker, PSPDFKit.Android\nn_onRemoveContentEditingBar:(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V:GetOnRemoveContentEditingBar_Lcom_pspdfkit_ui_contentediting_ContentEditingStylingBar_Handler:Com.Pspdfkit.UI.Contentediting.ContentEditingStylingBar/IOnContentEditingBarLifecycleListenerInvoker, PSPDFKit.Android\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 18
    const-class v0, Lmono/com/pspdfkit/ui/contentediting/ContentEditingStylingBar_OnContentEditingBarLifecycleListenerImplementor;

    const-string v1, "Com.Pspdfkit.UI.Contentediting.ContentEditingStylingBar+IOnContentEditingBarLifecycleListenerImplementor, PSPDFKit.Android"

    const-string v2, "n_onDisplayContentEditingBar:(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V:GetOnDisplayContentEditingBar_Lcom_pspdfkit_ui_contentediting_ContentEditingStylingBar_Handler:Com.Pspdfkit.UI.Contentediting.ContentEditingStylingBar/IOnContentEditingBarLifecycleListenerInvoker, PSPDFKit.Android\nn_onPrepareContentEditingBar:(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V:GetOnPrepareContentEditingBar_Lcom_pspdfkit_ui_contentediting_ContentEditingStylingBar_Handler:Com.Pspdfkit.UI.Contentediting.ContentEditingStylingBar/IOnContentEditingBarLifecycleListenerInvoker, PSPDFKit.Android\nn_onRemoveContentEditingBar:(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V:GetOnRemoveContentEditingBar_Lcom_pspdfkit_ui_contentediting_ContentEditingStylingBar_Handler:Com.Pspdfkit.UI.Contentediting.ContentEditingStylingBar/IOnContentEditingBarLifecycleListenerInvoker, PSPDFKit.Android\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lmono/com/pspdfkit/ui/contentediting/ContentEditingStylingBar_OnContentEditingBarLifecycleListenerImplementor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Com.Pspdfkit.UI.Contentediting.ContentEditingStylingBar+IOnContentEditingBarLifecycleListenerImplementor, PSPDFKit.Android"

    const-string v2, ""

    .line 26
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onDisplayContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V
.end method

.method private native n_onPrepareContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V
.end method

.method private native n_onRemoveContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lmono/com/pspdfkit/ui/contentediting/ContentEditingStylingBar_OnContentEditingBarLifecycleListenerImplementor;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmono/com/pspdfkit/ui/contentediting/ContentEditingStylingBar_OnContentEditingBarLifecycleListenerImplementor;->refList:Ljava/util/ArrayList;

    .line 59
    :cond_0
    iget-object v0, p0, Lmono/com/pspdfkit/ui/contentediting/ContentEditingStylingBar_OnContentEditingBarLifecycleListenerImplementor;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 64
    iget-object v0, p0, Lmono/com/pspdfkit/ui/contentediting/ContentEditingStylingBar_OnContentEditingBarLifecycleListenerImplementor;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onDisplayContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/ui/contentediting/ContentEditingStylingBar_OnContentEditingBarLifecycleListenerImplementor;->n_onDisplayContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V

    return-void
.end method

.method public onPrepareContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/ui/contentediting/ContentEditingStylingBar_OnContentEditingBarLifecycleListenerImplementor;->n_onPrepareContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V

    return-void
.end method

.method public onRemoveContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/ui/contentediting/ContentEditingStylingBar_OnContentEditingBarLifecycleListenerImplementor;->n_onRemoveContentEditingBar(Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;)V

    return-void
.end method

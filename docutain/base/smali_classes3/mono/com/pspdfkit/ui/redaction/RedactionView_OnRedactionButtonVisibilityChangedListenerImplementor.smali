.class public Lmono/com/pspdfkit/ui/redaction/RedactionView_OnRedactionButtonVisibilityChangedListenerImplementor;
.super Ljava/lang/Object;
.source "RedactionView_OnRedactionButtonVisibilityChangedListenerImplementor.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/pspdfkit/ui/redaction/RedactionView$OnRedactionButtonVisibilityChangedListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onRedactionButtonCollapsing:()V:GetOnRedactionButtonCollapsingHandler:PSPDFKit.UI.Redaction.RedactionView/IOnRedactionButtonVisibilityChangedListenerInvoker, PSPDFKit.Android\nn_onRedactionButtonExpanding:()V:GetOnRedactionButtonExpandingHandler:PSPDFKit.UI.Redaction.RedactionView/IOnRedactionButtonVisibilityChangedListenerInvoker, PSPDFKit.Android\nn_onRedactionButtonSlidingInside:()V:GetOnRedactionButtonSlidingInsideHandler:PSPDFKit.UI.Redaction.RedactionView/IOnRedactionButtonVisibilityChangedListenerInvoker, PSPDFKit.Android\nn_onRedactionButtonSlidingOutside:()V:GetOnRedactionButtonSlidingOutsideHandler:PSPDFKit.UI.Redaction.RedactionView/IOnRedactionButtonVisibilityChangedListenerInvoker, PSPDFKit.Android\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 19
    const-class v0, Lmono/com/pspdfkit/ui/redaction/RedactionView_OnRedactionButtonVisibilityChangedListenerImplementor;

    const-string v1, "PSPDFKit.UI.Redaction.RedactionView+IOnRedactionButtonVisibilityChangedListenerImplementor, PSPDFKit.Android"

    const-string v2, "n_onRedactionButtonCollapsing:()V:GetOnRedactionButtonCollapsingHandler:PSPDFKit.UI.Redaction.RedactionView/IOnRedactionButtonVisibilityChangedListenerInvoker, PSPDFKit.Android\nn_onRedactionButtonExpanding:()V:GetOnRedactionButtonExpandingHandler:PSPDFKit.UI.Redaction.RedactionView/IOnRedactionButtonVisibilityChangedListenerInvoker, PSPDFKit.Android\nn_onRedactionButtonSlidingInside:()V:GetOnRedactionButtonSlidingInsideHandler:PSPDFKit.UI.Redaction.RedactionView/IOnRedactionButtonVisibilityChangedListenerInvoker, PSPDFKit.Android\nn_onRedactionButtonSlidingOutside:()V:GetOnRedactionButtonSlidingOutsideHandler:PSPDFKit.UI.Redaction.RedactionView/IOnRedactionButtonVisibilityChangedListenerInvoker, PSPDFKit.Android\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lmono/com/pspdfkit/ui/redaction/RedactionView_OnRedactionButtonVisibilityChangedListenerImplementor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.UI.Redaction.RedactionView+IOnRedactionButtonVisibilityChangedListenerImplementor, PSPDFKit.Android"

    const-string v2, ""

    .line 27
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onRedactionButtonCollapsing()V
.end method

.method private native n_onRedactionButtonExpanding()V
.end method

.method private native n_onRedactionButtonSlidingInside()V
.end method

.method private native n_onRedactionButtonSlidingOutside()V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 66
    iget-object v0, p0, Lmono/com/pspdfkit/ui/redaction/RedactionView_OnRedactionButtonVisibilityChangedListenerImplementor;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmono/com/pspdfkit/ui/redaction/RedactionView_OnRedactionButtonVisibilityChangedListenerImplementor;->refList:Ljava/util/ArrayList;

    .line 68
    :cond_0
    iget-object v0, p0, Lmono/com/pspdfkit/ui/redaction/RedactionView_OnRedactionButtonVisibilityChangedListenerImplementor;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 73
    iget-object v0, p0, Lmono/com/pspdfkit/ui/redaction/RedactionView_OnRedactionButtonVisibilityChangedListenerImplementor;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onRedactionButtonCollapsing()V
    .locals 0

    .line 34
    invoke-direct {p0}, Lmono/com/pspdfkit/ui/redaction/RedactionView_OnRedactionButtonVisibilityChangedListenerImplementor;->n_onRedactionButtonCollapsing()V

    return-void
.end method

.method public onRedactionButtonExpanding()V
    .locals 0

    .line 42
    invoke-direct {p0}, Lmono/com/pspdfkit/ui/redaction/RedactionView_OnRedactionButtonVisibilityChangedListenerImplementor;->n_onRedactionButtonExpanding()V

    return-void
.end method

.method public onRedactionButtonSlidingInside()V
    .locals 0

    .line 50
    invoke-direct {p0}, Lmono/com/pspdfkit/ui/redaction/RedactionView_OnRedactionButtonVisibilityChangedListenerImplementor;->n_onRedactionButtonSlidingInside()V

    return-void
.end method

.method public onRedactionButtonSlidingOutside()V
    .locals 0

    .line 58
    invoke-direct {p0}, Lmono/com/pspdfkit/ui/redaction/RedactionView_OnRedactionButtonVisibilityChangedListenerImplementor;->n_onRedactionButtonSlidingOutside()V

    return-void
.end method

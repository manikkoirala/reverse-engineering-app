.class public Lmono/com/pspdfkit/listeners/LocalizationListenerImplementor;
.super Ljava/lang/Object;
.source "LocalizationListenerImplementor.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/pspdfkit/listeners/LocalizationListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_getLocalizedQuantityString:(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;:GetGetLocalizedQuantityString_Landroid_content_Context_ILjava_util_Locale_Landroid_view_View_IarrayLjava_lang_Object_Handler:PSPDFKit.Listeners.ILocalizationListenerInvoker, PSPDFKit.Android\nn_getLocalizedString:(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;)Ljava/lang/String;:GetGetLocalizedString_Landroid_content_Context_ILjava_util_Locale_Landroid_view_View_Handler:PSPDFKit.Listeners.ILocalizationListenerInvoker, PSPDFKit.Android\nn_getLocalizedString:(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;:GetGetLocalizedStr_Landroid_content_Context_ILjava_util_Locale_Landroid_view_View_arrayLjava_lang_Object_Handler:PSPDFKit.Listeners.ILocalizationListenerInvoker, PSPDFKit.Android\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 18
    const-class v0, Lmono/com/pspdfkit/listeners/LocalizationListenerImplementor;

    const-string v1, "PSPDFKit.Listeners.ILocalizationListenerImplementor, PSPDFKit.Android"

    const-string v2, "n_getLocalizedQuantityString:(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;:GetGetLocalizedQuantityString_Landroid_content_Context_ILjava_util_Locale_Landroid_view_View_IarrayLjava_lang_Object_Handler:PSPDFKit.Listeners.ILocalizationListenerInvoker, PSPDFKit.Android\nn_getLocalizedString:(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;)Ljava/lang/String;:GetGetLocalizedString_Landroid_content_Context_ILjava_util_Locale_Landroid_view_View_Handler:PSPDFKit.Listeners.ILocalizationListenerInvoker, PSPDFKit.Android\nn_getLocalizedString:(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;:GetGetLocalizedStr_Landroid_content_Context_ILjava_util_Locale_Landroid_view_View_arrayLjava_lang_Object_Handler:PSPDFKit.Listeners.ILocalizationListenerInvoker, PSPDFKit.Android\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lmono/com/pspdfkit/listeners/LocalizationListenerImplementor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Listeners.ILocalizationListenerImplementor, PSPDFKit.Android"

    const-string v2, ""

    .line 26
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_getLocalizedQuantityString(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;
.end method

.method private native n_getLocalizedString(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;)Ljava/lang/String;
.end method

.method private native n_getLocalizedString(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;
.end method


# virtual methods
.method public getLocalizedQuantityString(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 33
    invoke-direct/range {p0 .. p6}, Lmono/com/pspdfkit/listeners/LocalizationListenerImplementor;->n_getLocalizedQuantityString(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getLocalizedString(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;)Ljava/lang/String;
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lmono/com/pspdfkit/listeners/LocalizationListenerImplementor;->n_getLocalizedString(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getLocalizedString(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 49
    invoke-direct/range {p0 .. p5}, Lmono/com/pspdfkit/listeners/LocalizationListenerImplementor;->n_getLocalizedString(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lmono/com/pspdfkit/listeners/LocalizationListenerImplementor;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmono/com/pspdfkit/listeners/LocalizationListenerImplementor;->refList:Ljava/util/ArrayList;

    .line 59
    :cond_0
    iget-object v0, p0, Lmono/com/pspdfkit/listeners/LocalizationListenerImplementor;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 64
    iget-object v0, p0, Lmono/com/pspdfkit/listeners/LocalizationListenerImplementor;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.class public Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;
.super Ljava/lang/Object;
.source "DocumentListenerImplementor.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/pspdfkit/listeners/DocumentListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onDocumentClick:()Z:GetOnDocumentClickHandler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentLoadFailed:(Ljava/lang/Throwable;)V:GetOnDocumentLoadFailed_Ljava_lang_Throwable_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentLoaded:(Lcom/pspdfkit/document/PdfDocument;)V:GetOnDocumentLoaded_Lcom_pspdfkit_document_PdfDocument_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentSave:(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z:GetOnDocumentSave_Lcom_pspdfkit_document_PdfDocument_Lcom_pspdfkit_document_DocumentSaveOptions_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentSaveCancelled:(Lcom/pspdfkit/document/PdfDocument;)V:GetOnDocumentSaveCancelled_Lcom_pspdfkit_document_PdfDocument_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentSaveFailed:(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V:GetOnDocumentSaveFailed_Lcom_pspdfkit_document_PdfDocument_Ljava_lang_Throwable_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentSaved:(Lcom/pspdfkit/document/PdfDocument;)V:GetOnDocumentSaved_Lcom_pspdfkit_document_PdfDocument_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentZoomed:(Lcom/pspdfkit/document/PdfDocument;IF)V:GetOnDocumentZoomed_Lcom_pspdfkit_document_PdfDocument_IFHandler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onPageChanged:(Lcom/pspdfkit/document/PdfDocument;I)V:GetOnPageChanged_Lcom_pspdfkit_document_PdfDocument_IHandler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onPageClick:(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z:GetOnPageClick_Lcom_pspdfkit_document_PdfDocument_ILandroid_view_MotionEvent_Landroid_graphics_PointF_Lcom_pspdfkit_annotations_Annotation_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onPageUpdated:(Lcom/pspdfkit/document/PdfDocument;I)V:GetOnPageUpdated_Lcom_pspdfkit_document_PdfDocument_IHandler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 26
    const-class v0, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;

    const-string v1, "PSPDFKit.Listeners.IDocumentListenerImplementor, PSPDFKit.Android"

    const-string v2, "n_onDocumentClick:()Z:GetOnDocumentClickHandler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentLoadFailed:(Ljava/lang/Throwable;)V:GetOnDocumentLoadFailed_Ljava_lang_Throwable_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentLoaded:(Lcom/pspdfkit/document/PdfDocument;)V:GetOnDocumentLoaded_Lcom_pspdfkit_document_PdfDocument_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentSave:(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z:GetOnDocumentSave_Lcom_pspdfkit_document_PdfDocument_Lcom_pspdfkit_document_DocumentSaveOptions_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentSaveCancelled:(Lcom/pspdfkit/document/PdfDocument;)V:GetOnDocumentSaveCancelled_Lcom_pspdfkit_document_PdfDocument_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentSaveFailed:(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V:GetOnDocumentSaveFailed_Lcom_pspdfkit_document_PdfDocument_Ljava_lang_Throwable_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentSaved:(Lcom/pspdfkit/document/PdfDocument;)V:GetOnDocumentSaved_Lcom_pspdfkit_document_PdfDocument_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onDocumentZoomed:(Lcom/pspdfkit/document/PdfDocument;IF)V:GetOnDocumentZoomed_Lcom_pspdfkit_document_PdfDocument_IFHandler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onPageChanged:(Lcom/pspdfkit/document/PdfDocument;I)V:GetOnPageChanged_Lcom_pspdfkit_document_PdfDocument_IHandler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onPageClick:(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z:GetOnPageClick_Lcom_pspdfkit_document_PdfDocument_ILandroid_view_MotionEvent_Landroid_graphics_PointF_Lcom_pspdfkit_annotations_Annotation_Handler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\nn_onPageUpdated:(Lcom/pspdfkit/document/PdfDocument;I)V:GetOnPageUpdated_Lcom_pspdfkit_document_PdfDocument_IHandler:PSPDFKit.Listeners.IDocumentListener, PSPDFKit.Android\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Listeners.IDocumentListenerImplementor, PSPDFKit.Android"

    const-string v2, ""

    .line 34
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onDocumentClick()Z
.end method

.method private native n_onDocumentLoadFailed(Ljava/lang/Throwable;)V
.end method

.method private native n_onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
.end method

.method private native n_onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
.end method

.method private native n_onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
.end method

.method private native n_onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
.end method

.method private native n_onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
.end method

.method private native n_onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V
.end method

.method private native n_onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
.end method

.method private native n_onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
.end method

.method private native n_onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 129
    iget-object v0, p0, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->refList:Ljava/util/ArrayList;

    .line 131
    :cond_0
    iget-object v0, p0, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 136
    iget-object v0, p0, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onDocumentClick()Z
    .locals 1

    .line 41
    invoke-direct {p0}, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->n_onDocumentClick()Z

    move-result v0

    return v0
.end method

.method public onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->n_onDocumentLoadFailed(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->n_onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 0

    .line 65
    invoke-direct {p0, p1, p2}, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->n_onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result p1

    return p1
.end method

.method public onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 73
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->n_onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
    .locals 0

    .line 81
    invoke-direct {p0, p1, p2}, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->n_onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V

    return-void
.end method

.method public onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 89
    invoke-direct {p0, p1}, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->n_onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V
    .locals 0

    .line 97
    invoke-direct {p0, p1, p2, p3}, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->n_onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V

    return-void
.end method

.method public onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    .line 105
    invoke-direct {p0, p1, p2}, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->n_onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V

    return-void
.end method

.method public onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    .line 113
    invoke-direct/range {p0 .. p5}, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->n_onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    return p1
.end method

.method public onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    .line 121
    invoke-direct {p0, p1, p2}, Lmono/com/pspdfkit/listeners/DocumentListenerImplementor;->n_onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V

    return-void
.end method

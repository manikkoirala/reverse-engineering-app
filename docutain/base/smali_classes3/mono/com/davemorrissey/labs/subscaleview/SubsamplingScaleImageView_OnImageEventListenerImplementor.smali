.class public Lmono/com/davemorrissey/labs/subscaleview/SubsamplingScaleImageView_OnImageEventListenerImplementor;
.super Ljava/lang/Object;
.source "SubsamplingScaleImageView_OnImageEventListenerImplementor.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/davemorrissey/labs/subscaleview/SubsamplingScaleImageView$OnImageEventListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onImageLoadError:(Ljava/lang/Exception;)V:GetOnImageLoadError_Ljava_lang_Exception_Handler:Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView/IOnImageEventListenerInvoker, SubsamplingScaleImageViewBinding\nn_onImageLoaded:()V:GetOnImageLoadedHandler:Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView/IOnImageEventListenerInvoker, SubsamplingScaleImageViewBinding\nn_onPreviewLoadError:(Ljava/lang/Exception;)V:GetOnPreviewLoadError_Ljava_lang_Exception_Handler:Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView/IOnImageEventListenerInvoker, SubsamplingScaleImageViewBinding\nn_onPreviewReleased:()V:GetOnPreviewReleasedHandler:Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView/IOnImageEventListenerInvoker, SubsamplingScaleImageViewBinding\nn_onReady:()V:GetOnReadyHandler:Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView/IOnImageEventListenerInvoker, SubsamplingScaleImageViewBinding\nn_onTileLoadError:(Ljava/lang/Exception;)V:GetOnTileLoadError_Ljava_lang_Exception_Handler:Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView/IOnImageEventListenerInvoker, SubsamplingScaleImageViewBinding\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 21
    const-class v0, Lmono/com/davemorrissey/labs/subscaleview/SubsamplingScaleImageView_OnImageEventListenerImplementor;

    const-string v1, "Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView+IOnImageEventListenerImplementor, SubsamplingScaleImageViewBinding"

    const-string v2, "n_onImageLoadError:(Ljava/lang/Exception;)V:GetOnImageLoadError_Ljava_lang_Exception_Handler:Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView/IOnImageEventListenerInvoker, SubsamplingScaleImageViewBinding\nn_onImageLoaded:()V:GetOnImageLoadedHandler:Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView/IOnImageEventListenerInvoker, SubsamplingScaleImageViewBinding\nn_onPreviewLoadError:(Ljava/lang/Exception;)V:GetOnPreviewLoadError_Ljava_lang_Exception_Handler:Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView/IOnImageEventListenerInvoker, SubsamplingScaleImageViewBinding\nn_onPreviewReleased:()V:GetOnPreviewReleasedHandler:Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView/IOnImageEventListenerInvoker, SubsamplingScaleImageViewBinding\nn_onReady:()V:GetOnReadyHandler:Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView/IOnImageEventListenerInvoker, SubsamplingScaleImageViewBinding\nn_onTileLoadError:(Ljava/lang/Exception;)V:GetOnTileLoadError_Ljava_lang_Exception_Handler:Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView/IOnImageEventListenerInvoker, SubsamplingScaleImageViewBinding\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lmono/com/davemorrissey/labs/subscaleview/SubsamplingScaleImageView_OnImageEventListenerImplementor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Com.Davemorrissey.Labs.Subscaleview.SubsamplingScaleImageView+IOnImageEventListenerImplementor, SubsamplingScaleImageViewBinding"

    const-string v2, ""

    .line 29
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onImageLoadError(Ljava/lang/Exception;)V
.end method

.method private native n_onImageLoaded()V
.end method

.method private native n_onPreviewLoadError(Ljava/lang/Exception;)V
.end method

.method private native n_onPreviewReleased()V
.end method

.method private native n_onReady()V
.end method

.method private native n_onTileLoadError(Ljava/lang/Exception;)V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lmono/com/davemorrissey/labs/subscaleview/SubsamplingScaleImageView_OnImageEventListenerImplementor;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmono/com/davemorrissey/labs/subscaleview/SubsamplingScaleImageView_OnImageEventListenerImplementor;->refList:Ljava/util/ArrayList;

    .line 86
    :cond_0
    iget-object v0, p0, Lmono/com/davemorrissey/labs/subscaleview/SubsamplingScaleImageView_OnImageEventListenerImplementor;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 91
    iget-object v0, p0, Lmono/com/davemorrissey/labs/subscaleview/SubsamplingScaleImageView_OnImageEventListenerImplementor;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onImageLoadError(Ljava/lang/Exception;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lmono/com/davemorrissey/labs/subscaleview/SubsamplingScaleImageView_OnImageEventListenerImplementor;->n_onImageLoadError(Ljava/lang/Exception;)V

    return-void
.end method

.method public onImageLoaded()V
    .locals 0

    .line 44
    invoke-direct {p0}, Lmono/com/davemorrissey/labs/subscaleview/SubsamplingScaleImageView_OnImageEventListenerImplementor;->n_onImageLoaded()V

    return-void
.end method

.method public onPreviewLoadError(Ljava/lang/Exception;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1}, Lmono/com/davemorrissey/labs/subscaleview/SubsamplingScaleImageView_OnImageEventListenerImplementor;->n_onPreviewLoadError(Ljava/lang/Exception;)V

    return-void
.end method

.method public onPreviewReleased()V
    .locals 0

    .line 60
    invoke-direct {p0}, Lmono/com/davemorrissey/labs/subscaleview/SubsamplingScaleImageView_OnImageEventListenerImplementor;->n_onPreviewReleased()V

    return-void
.end method

.method public onReady()V
    .locals 0

    .line 68
    invoke-direct {p0}, Lmono/com/davemorrissey/labs/subscaleview/SubsamplingScaleImageView_OnImageEventListenerImplementor;->n_onReady()V

    return-void
.end method

.method public onTileLoadError(Ljava/lang/Exception;)V
    .locals 0

    .line 76
    invoke-direct {p0, p1}, Lmono/com/davemorrissey/labs/subscaleview/SubsamplingScaleImageView_OnImageEventListenerImplementor;->n_onTileLoadError(Ljava/lang/Exception;)V

    return-void
.end method

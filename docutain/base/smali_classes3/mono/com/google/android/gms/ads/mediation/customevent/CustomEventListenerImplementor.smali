.class public Lmono/com/google/android/gms/ads/mediation/customevent/CustomEventListenerImplementor;
.super Ljava/lang/Object;
.source "CustomEventListenerImplementor.java"

# interfaces
.implements Lmono/android/IGCUserPeer;
.implements Lcom/google/android/gms/ads/mediation/customevent/CustomEventListener;


# static fields
.field public static final __md_methods:Ljava/lang/String; = "n_onAdClicked:()V:GetOnAdClickedHandler:Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\nn_onAdClosed:()V:GetOnAdClosedHandler:Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\nn_onAdFailedToLoad:(Lcom/google/android/gms/ads/AdError;)V:GetOnAdFailedToLoad2_Lcom_google_android_gms_ads_AdError_Handler:Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\nn_onAdFailedToLoad:(I)V:GetOnAdFailedToLoad_IHandler:Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\nn_onAdLeftApplication:()V:GetOnAdLeftApplicationHandler:Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\nn_onAdOpened:()V:GetOnAdOpenedHandler:Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\n"


# instance fields
.field private refList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 21
    const-class v0, Lmono/com/google/android/gms/ads/mediation/customevent/CustomEventListenerImplementor;

    const-string v1, "Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerImplementor, Xamarin.GooglePlayServices.Ads.Lite"

    const-string v2, "n_onAdClicked:()V:GetOnAdClickedHandler:Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\nn_onAdClosed:()V:GetOnAdClosedHandler:Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\nn_onAdFailedToLoad:(Lcom/google/android/gms/ads/AdError;)V:GetOnAdFailedToLoad2_Lcom_google_android_gms_ads_AdError_Handler:Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\nn_onAdFailedToLoad:(I)V:GetOnAdFailedToLoad_IHandler:Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\nn_onAdLeftApplication:()V:GetOnAdLeftApplicationHandler:Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\nn_onAdOpened:()V:GetOnAdOpenedHandler:Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerInvoker, Xamarin.GooglePlayServices.Ads.Lite\n"

    invoke-static {v1, v0, v2}, Lmono/android/Runtime;->register(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lmono/com/google/android/gms/ads/mediation/customevent/CustomEventListenerImplementor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Android.Gms.Ads.Mediation.CustomEvent.ICustomEventListenerImplementor, Xamarin.GooglePlayServices.Ads.Lite"

    const-string v2, ""

    .line 29
    invoke-static {v1, v2, p0, v0}, Lmono/android/TypeManager;->Activate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private native n_onAdClicked()V
.end method

.method private native n_onAdClosed()V
.end method

.method private native n_onAdFailedToLoad(I)V
.end method

.method private native n_onAdFailedToLoad(Lcom/google/android/gms/ads/AdError;)V
.end method

.method private native n_onAdLeftApplication()V
.end method

.method private native n_onAdOpened()V
.end method


# virtual methods
.method public monodroidAddReference(Ljava/lang/Object;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lmono/com/google/android/gms/ads/mediation/customevent/CustomEventListenerImplementor;->refList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmono/com/google/android/gms/ads/mediation/customevent/CustomEventListenerImplementor;->refList:Ljava/util/ArrayList;

    .line 86
    :cond_0
    iget-object v0, p0, Lmono/com/google/android/gms/ads/mediation/customevent/CustomEventListenerImplementor;->refList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public monodroidClearReferences()V
    .locals 1

    .line 91
    iget-object v0, p0, Lmono/com/google/android/gms/ads/mediation/customevent/CustomEventListenerImplementor;->refList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onAdClicked()V
    .locals 0

    .line 36
    invoke-direct {p0}, Lmono/com/google/android/gms/ads/mediation/customevent/CustomEventListenerImplementor;->n_onAdClicked()V

    return-void
.end method

.method public onAdClosed()V
    .locals 0

    .line 44
    invoke-direct {p0}, Lmono/com/google/android/gms/ads/mediation/customevent/CustomEventListenerImplementor;->n_onAdClosed()V

    return-void
.end method

.method public onAdFailedToLoad(I)V
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lmono/com/google/android/gms/ads/mediation/customevent/CustomEventListenerImplementor;->n_onAdFailedToLoad(I)V

    return-void
.end method

.method public onAdFailedToLoad(Lcom/google/android/gms/ads/AdError;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1}, Lmono/com/google/android/gms/ads/mediation/customevent/CustomEventListenerImplementor;->n_onAdFailedToLoad(Lcom/google/android/gms/ads/AdError;)V

    return-void
.end method

.method public onAdLeftApplication()V
    .locals 0

    .line 68
    invoke-direct {p0}, Lmono/com/google/android/gms/ads/mediation/customevent/CustomEventListenerImplementor;->n_onAdLeftApplication()V

    return-void
.end method

.method public onAdOpened()V
    .locals 0

    .line 76
    invoke-direct {p0}, Lmono/com/google/android/gms/ads/mediation/customevent/CustomEventListenerImplementor;->n_onAdOpened()V

    return-void
.end method

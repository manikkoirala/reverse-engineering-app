.class public Lmono/MonoPackageManager_Resources;
.super Ljava/lang/Object;
.source "MonoPackageManager_Resources.java"


# static fields
.field public static Assemblies:[Ljava/lang/String;

.field public static Dependencies:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 90

    const-string v0, "UInterface.Android.dll"

    const-string v1, "Xamarin.Android.ReactiveX.RxJava3.RxJava.dll"

    const-string v2, "Xamarin.AndroidX.AppCompat.AppCompatResources.dll"

    const-string v3, "Xamarin.AndroidX.AppCompat.dll"

    const-string v4, "Xamarin.AndroidX.Browser.dll"

    const-string v5, "Xamarin.AndroidX.CardView.dll"

    const-string v6, "Xamarin.AndroidX.ConstraintLayout.dll"

    const-string v7, "Xamarin.AndroidX.CoordinatorLayout.dll"

    const-string v8, "Xamarin.AndroidX.Core.dll"

    const-string v9, "Xamarin.AndroidX.CursorAdapter.dll"

    const-string v10, "Xamarin.AndroidX.CustomView.dll"

    const-string v11, "Xamarin.AndroidX.DrawerLayout.dll"

    const-string v12, "Xamarin.AndroidX.ExifInterface.dll"

    const-string v13, "Xamarin.AndroidX.Fragment.dll"

    const-string v14, "Xamarin.AndroidX.GridLayout.dll"

    const-string v15, "Xamarin.AndroidX.Legacy.Support.Core.UI.dll"

    const-string v16, "Xamarin.AndroidX.Legacy.Support.Core.Utils.dll"

    const-string v17, "Xamarin.AndroidX.Lifecycle.Common.dll"

    const-string v18, "Xamarin.AndroidX.Lifecycle.LiveData.Core.dll"

    const-string v19, "Xamarin.AndroidX.Lifecycle.ViewModel.dll"

    const-string v20, "Xamarin.AndroidX.Loader.dll"

    const-string v21, "Xamarin.AndroidX.Preference.dll"

    const-string v22, "Xamarin.AndroidX.RecyclerView.dll"

    const-string v23, "Xamarin.AndroidX.SwipeRefreshLayout.dll"

    const-string v24, "Xamarin.AndroidX.Transition.dll"

    const-string v25, "Xamarin.AndroidX.VersionedParcelable.dll"

    const-string v26, "Xamarin.AndroidX.ViewPager.dll"

    const-string v27, "Xamarin.Google.Android.Material.dll"

    const-string v28, "Xamarin.Kotlin.StdLib.dll"

    const-string v29, "AndroidInstallReferrerBinding.dll"

    const-string v30, "FormsViewGroup.dll"

    const-string v31, "KeepSafe.TapTargetView.dll"

    const-string v32, "Microsoft.AppCenter.Analytics.dll"

    const-string v33, "Microsoft.AppCenter.Crashes.dll"

    const-string v34, "Microsoft.AppCenter.dll"

    const-string v35, "Microsoft.Azure.NotificationHubs.dll"

    const-string v36, "Microsoft.Extensions.Caching.Abstractions.dll"

    const-string v37, "Microsoft.Extensions.Caching.Memory.dll"

    const-string v38, "Microsoft.Extensions.Logging.Abstractions.dll"

    const-string v39, "Microsoft.Extensions.Options.dll"

    const-string v40, "Microsoft.Extensions.Primitives.dll"

    const-string v41, "MPAndroidChart.dll"

    const-string v42, "Newtonsoft.Json.dll"

    const-string v43, "Plugin.InAppBilling.dll"

    const-string v44, "PSPDFKit.Android.dll"

    const-string v45, "SubsamplingScaleImageViewBinding.dll"

    const-string v46, "Translation.dll"

    const-string v47, "Xamarin.Android.Glide.dll"

    const-string v48, "Xamarin.Android.Google.BillingClient.dll"

    const-string v49, "Xamarin.Android.ReactiveStreams.dll"

    const-string v50, "Xamarin.AndroidX.Activity.dll"

    const-string v51, "Xamarin.AndroidX.Biometric.dll"

    const-string v52, "Xamarin.AndroidX.Camera.Camera2.dll"

    const-string v53, "Xamarin.AndroidX.Camera.Core.dll"

    const-string v54, "Xamarin.AndroidX.Camera.Lifecycle.dll"

    const-string v55, "Xamarin.AndroidX.Camera.View.dll"

    const-string v56, "Xamarin.AndroidX.ConstraintLayout.Core.dll"

    const-string v57, "Xamarin.AndroidX.SavedState.dll"

    const-string v58, "Xamarin.AndroidX.ViewPager2.dll"

    const-string v59, "Xamarin.Azure.NotificationHubs.Android.dll"

    const-string v60, "Xamarin.CommunityToolkit.dll"

    const-string v61, "Xamarin.Essentials.dll"

    const-string v62, "Xamarin.Firebase.Analytics.dll"

    const-string v63, "Xamarin.Firebase.Common.dll"

    const-string v64, "Xamarin.Firebase.Config.dll"

    const-string v65, "Xamarin.Firebase.Messaging.dll"

    const-string v66, "Xamarin.Forms.Core.dll"

    const-string v67, "Xamarin.Forms.Material.dll"

    const-string v68, "Xamarin.Forms.Platform.Android.dll"

    const-string v69, "Xamarin.Forms.Platform.dll"

    const-string v70, "Xamarin.Forms.Xaml.dll"

    const-string v71, "Xamarin.Google.Android.DataTransport.TransportApi.dll"

    const-string v72, "Xamarin.Google.Android.ODML.Image.dll"

    const-string v73, "Xamarin.Google.Android.Play.Review.dll"

    const-string v74, "Xamarin.Google.Guava.ListenableFuture.dll"

    const-string v75, "Xamarin.Google.MLKit.BarcodeScanning.Common.dll"

    const-string v76, "Xamarin.Google.MLKit.Common.dll"

    const-string v77, "Xamarin.Google.MLKit.Vision.Common.dll"

    const-string v78, "Xamarin.Google.MLKit.Vision.Interfaces.dll"

    const-string v79, "Xamarin.Google.UserMessagingPlatform.dll"

    const-string v80, "Xamarin.GooglePlayServices.Ads.Identifier.dll"

    const-string v81, "Xamarin.GooglePlayServices.Ads.Lite.dll"

    const-string v82, "Xamarin.GooglePlayServices.Base.dll"

    const-string v83, "Xamarin.GooglePlayServices.Basement.dll"

    const-string v84, "Xamarin.GooglePlayServices.Measurement.Api.dll"

    const-string v85, "Xamarin.GooglePlayServices.Measurement.dll"

    const-string v86, "Xamarin.GooglePlayServices.MLKit.BarcodeScanning.dll"

    const-string v87, "Xamarin.GooglePlayServices.MLKit.Text.Recognition.Common.dll"

    const-string v88, "Xamarin.GooglePlayServices.MLKit.Text.Recognition.dll"

    const-string v89, "Xamarin.GooglePlayServices.Tasks.dll"

    .line 3
    filled-new-array/range {v0 .. v89}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmono/MonoPackageManager_Resources;->Assemblies:[Ljava/lang/String;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    .line 96
    sput-object v0, Lmono/MonoPackageManager_Resources;->Dependencies:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

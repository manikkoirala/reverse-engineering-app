.class public final Landroidx/window/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/window/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final activityAction:I = 0x7f040027

.field public static final activityName:I = 0x7f040029

.field public static final alwaysExpand:I = 0x7f04003b

.field public static final animationBackgroundColor:I = 0x7f040040

.field public static final clearTop:I = 0x7f0400e5

.field public static final finishPrimaryWithPlaceholder:I = 0x7f0401f9

.field public static final finishPrimaryWithSecondary:I = 0x7f0401fa

.field public static final finishSecondaryWithPrimary:I = 0x7f0401fb

.field public static final placeholderActivityName:I = 0x7f0403b6

.field public static final primaryActivityName:I = 0x7f0403d4

.field public static final secondaryActivityAction:I = 0x7f04057c

.field public static final secondaryActivityName:I = 0x7f04057d

.field public static final splitLayoutDirection:I = 0x7f0405b2

.field public static final splitMaxAspectRatioInLandscape:I = 0x7f0405b3

.field public static final splitMaxAspectRatioInPortrait:I = 0x7f0405b4

.field public static final splitMinHeightDp:I = 0x7f0405b5

.field public static final splitMinSmallestWidthDp:I = 0x7f0405b6

.field public static final splitMinWidthDp:I = 0x7f0405b7

.field public static final splitRatio:I = 0x7f0405b8

.field public static final stickyPlaceholder:I = 0x7f0405d7

.field public static final tag:I = 0x7f040612


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

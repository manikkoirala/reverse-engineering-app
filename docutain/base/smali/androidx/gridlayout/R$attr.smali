.class public final Landroidx/gridlayout/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/gridlayout/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final alignmentMode:I = 0x7f040033

.field public static final alpha:I = 0x7f040038

.field public static final columnCount:I = 0x7f04013e

.field public static final columnOrderPreserved:I = 0x7f04013f

.field public static final coordinatorLayoutStyle:I = 0x7f04015b

.field public static final font:I = 0x7f04021f

.field public static final fontProviderAuthority:I = 0x7f040221

.field public static final fontProviderCerts:I = 0x7f040222

.field public static final fontProviderFetchStrategy:I = 0x7f040223

.field public static final fontProviderFetchTimeout:I = 0x7f040224

.field public static final fontProviderPackage:I = 0x7f040225

.field public static final fontProviderQuery:I = 0x7f040226

.field public static final fontStyle:I = 0x7f040228

.field public static final fontVariationSettings:I = 0x7f040229

.field public static final fontWeight:I = 0x7f04022a

.field public static final keylines:I = 0x7f04028f

.field public static final layout_anchor:I = 0x7f04029b

.field public static final layout_anchorGravity:I = 0x7f04029c

.field public static final layout_behavior:I = 0x7f04029d

.field public static final layout_column:I = 0x7f0402a0

.field public static final layout_columnSpan:I = 0x7f0402a1

.field public static final layout_columnWeight:I = 0x7f0402a2

.field public static final layout_dodgeInsetEdges:I = 0x7f0402d1

.field public static final layout_gravity:I = 0x7f0402db

.field public static final layout_insetEdge:I = 0x7f0402dc

.field public static final layout_keyline:I = 0x7f0402dd

.field public static final layout_row:I = 0x7f0402e0

.field public static final layout_rowSpan:I = 0x7f0402e1

.field public static final layout_rowWeight:I = 0x7f0402e2

.field public static final orientation:I = 0x7f040399

.field public static final rowCount:I = 0x7f04056e

.field public static final rowOrderPreserved:I = 0x7f04056f

.field public static final statusBarBackground:I = 0x7f0405d4

.field public static final ttcIndex:I = 0x7f0406a4

.field public static final useDefaultMargins:I = 0x7f0406a9


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

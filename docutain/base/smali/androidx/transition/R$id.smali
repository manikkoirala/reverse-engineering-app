.class public final Landroidx/transition/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/transition/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accessibility_action_clickable_span:I = 0x7f0a0026

.field public static final accessibility_custom_action_0:I = 0x7f0a0027

.field public static final accessibility_custom_action_1:I = 0x7f0a0028

.field public static final accessibility_custom_action_10:I = 0x7f0a0029

.field public static final accessibility_custom_action_11:I = 0x7f0a002a

.field public static final accessibility_custom_action_12:I = 0x7f0a002b

.field public static final accessibility_custom_action_13:I = 0x7f0a002c

.field public static final accessibility_custom_action_14:I = 0x7f0a002d

.field public static final accessibility_custom_action_15:I = 0x7f0a002e

.field public static final accessibility_custom_action_16:I = 0x7f0a002f

.field public static final accessibility_custom_action_17:I = 0x7f0a0030

.field public static final accessibility_custom_action_18:I = 0x7f0a0031

.field public static final accessibility_custom_action_19:I = 0x7f0a0032

.field public static final accessibility_custom_action_2:I = 0x7f0a0033

.field public static final accessibility_custom_action_20:I = 0x7f0a0034

.field public static final accessibility_custom_action_21:I = 0x7f0a0035

.field public static final accessibility_custom_action_22:I = 0x7f0a0036

.field public static final accessibility_custom_action_23:I = 0x7f0a0037

.field public static final accessibility_custom_action_24:I = 0x7f0a0038

.field public static final accessibility_custom_action_25:I = 0x7f0a0039

.field public static final accessibility_custom_action_26:I = 0x7f0a003a

.field public static final accessibility_custom_action_27:I = 0x7f0a003b

.field public static final accessibility_custom_action_28:I = 0x7f0a003c

.field public static final accessibility_custom_action_29:I = 0x7f0a003d

.field public static final accessibility_custom_action_3:I = 0x7f0a003e

.field public static final accessibility_custom_action_30:I = 0x7f0a003f

.field public static final accessibility_custom_action_31:I = 0x7f0a0040

.field public static final accessibility_custom_action_4:I = 0x7f0a0041

.field public static final accessibility_custom_action_5:I = 0x7f0a0042

.field public static final accessibility_custom_action_6:I = 0x7f0a0043

.field public static final accessibility_custom_action_7:I = 0x7f0a0044

.field public static final accessibility_custom_action_8:I = 0x7f0a0045

.field public static final accessibility_custom_action_9:I = 0x7f0a0046

.field public static final action_bar:I = 0x7f0a004c

.field public static final action_bar_activity_content:I = 0x7f0a004d

.field public static final action_bar_container:I = 0x7f0a004e

.field public static final action_bar_root:I = 0x7f0a004f

.field public static final action_bar_spinner:I = 0x7f0a0050

.field public static final action_bar_subtitle:I = 0x7f0a0051

.field public static final action_bar_title:I = 0x7f0a0052

.field public static final action_container:I = 0x7f0a0053

.field public static final action_context_bar:I = 0x7f0a0054

.field public static final action_divider:I = 0x7f0a0057

.field public static final action_image:I = 0x7f0a005d

.field public static final action_menu_divider:I = 0x7f0a005f

.field public static final action_menu_presenter:I = 0x7f0a0060

.field public static final action_mode_bar:I = 0x7f0a0061

.field public static final action_mode_bar_stub:I = 0x7f0a0062

.field public static final action_mode_close_button:I = 0x7f0a0063

.field public static final action_text:I = 0x7f0a0069

.field public static final actions:I = 0x7f0a006a

.field public static final activity_chooser_view_content:I = 0x7f0a006b

.field public static final add:I = 0x7f0a006d

.field public static final alertTitle:I = 0x7f0a0077

.field public static final async:I = 0x7f0a0088

.field public static final blocking:I = 0x7f0a00a0

.field public static final bottom:I = 0x7f0a00a2

.field public static final buttonPanel:I = 0x7f0a00b9

.field public static final checkbox:I = 0x7f0a00d0

.field public static final chronometer:I = 0x7f0a00d2

.field public static final content:I = 0x7f0a00e3

.field public static final contentPanel:I = 0x7f0a00e4

.field public static final custom:I = 0x7f0a00f0

.field public static final customPanel:I = 0x7f0a00f1

.field public static final decor_content_parent:I = 0x7f0a00f8

.field public static final default_activity_button:I = 0x7f0a00f9

.field public static final dialog_button:I = 0x7f0a0104

.field public static final edit_query:I = 0x7f0a0120

.field public static final end:I = 0x7f0a0125

.field public static final expand_activities_button:I = 0x7f0a012e

.field public static final expanded_menu:I = 0x7f0a012f

.field public static final forever:I = 0x7f0a014b

.field public static final fragment_container_view_tag:I = 0x7f0a014d

.field public static final ghost_view:I = 0x7f0a0150

.field public static final ghost_view_holder:I = 0x7f0a0151

.field public static final group_divider:I = 0x7f0a0159

.field public static final home:I = 0x7f0a0160

.field public static final icon:I = 0x7f0a0167

.field public static final icon_group:I = 0x7f0a0169

.field public static final image:I = 0x7f0a0170

.field public static final info:I = 0x7f0a017a

.field public static final italic:I = 0x7f0a017e

.field public static final left:I = 0x7f0a0184

.field public static final line1:I = 0x7f0a0188

.field public static final line3:I = 0x7f0a0189

.field public static final listMode:I = 0x7f0a018d

.field public static final list_item:I = 0x7f0a018e

.field public static final message:I = 0x7f0a01b7

.field public static final multiply:I = 0x7f0a01d9

.field public static final none:I = 0x7f0a01ed

.field public static final normal:I = 0x7f0a01ee

.field public static final notification_background:I = 0x7f0a01f0

.field public static final notification_main_column:I = 0x7f0a01f5

.field public static final notification_main_column_container:I = 0x7f0a01f6

.field public static final parentPanel:I = 0x7f0a0203

.field public static final parent_matrix:I = 0x7f0a0205

.field public static final progress_circular:I = 0x7f0a021d

.field public static final progress_horizontal:I = 0x7f0a021e

.field public static final radio:I = 0x7f0a0474

.field public static final right:I = 0x7f0a0480

.field public static final right_icon:I = 0x7f0a0482

.field public static final right_side:I = 0x7f0a0483

.field public static final save_non_transition_alpha:I = 0x7f0a048a

.field public static final save_overlay_view:I = 0x7f0a048b

.field public static final screen:I = 0x7f0a048e

.field public static final scrollIndicatorDown:I = 0x7f0a0490

.field public static final scrollIndicatorUp:I = 0x7f0a0491

.field public static final scrollView:I = 0x7f0a0492

.field public static final search_badge:I = 0x7f0a0499

.field public static final search_bar:I = 0x7f0a049a

.field public static final search_button:I = 0x7f0a049c

.field public static final search_close_btn:I = 0x7f0a049d

.field public static final search_edit_frame:I = 0x7f0a049e

.field public static final search_go_btn:I = 0x7f0a049f

.field public static final search_mag_icon:I = 0x7f0a04a0

.field public static final search_plate:I = 0x7f0a04a1

.field public static final search_src_text:I = 0x7f0a04a2

.field public static final search_voice_btn:I = 0x7f0a04b0

.field public static final select_dialog_listview:I = 0x7f0a04b5

.field public static final shortcut:I = 0x7f0a04be

.field public static final spacer:I = 0x7f0a04cf

.field public static final split_action_bar:I = 0x7f0a04d3

.field public static final src_atop:I = 0x7f0a04d8

.field public static final src_in:I = 0x7f0a04d9

.field public static final src_over:I = 0x7f0a04da

.field public static final start:I = 0x7f0a04dd

.field public static final submenuarrow:I = 0x7f0a04ed

.field public static final submit_area:I = 0x7f0a04ee

.field public static final tabMode:I = 0x7f0a04f2

.field public static final tag_accessibility_actions:I = 0x7f0a04f3

.field public static final tag_accessibility_clickable_spans:I = 0x7f0a04f4

.field public static final tag_accessibility_heading:I = 0x7f0a04f5

.field public static final tag_accessibility_pane_title:I = 0x7f0a04f6

.field public static final tag_screen_reader_focusable:I = 0x7f0a04fa

.field public static final tag_transition_group:I = 0x7f0a04fc

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a04fd

.field public static final tag_unhandled_key_listeners:I = 0x7f0a04fe

.field public static final text:I = 0x7f0a0502

.field public static final text2:I = 0x7f0a0503

.field public static final textSpacerNoButtons:I = 0x7f0a0505

.field public static final textSpacerNoTitle:I = 0x7f0a0506

.field public static final time:I = 0x7f0a0519

.field public static final title:I = 0x7f0a051a

.field public static final titleDividerNoCustom:I = 0x7f0a051b

.field public static final title_template:I = 0x7f0a051d

.field public static final top:I = 0x7f0a0522

.field public static final topPanel:I = 0x7f0a0525

.field public static final transition_current_scene:I = 0x7f0a052c

.field public static final transition_layout_save:I = 0x7f0a052d

.field public static final transition_position:I = 0x7f0a052e

.field public static final transition_scene_layoutid_cache:I = 0x7f0a052f

.field public static final transition_transform:I = 0x7f0a0530

.field public static final uniform:I = 0x7f0a0535

.field public static final up:I = 0x7f0a0537

.field public static final visible_removing_fragment_view_tag:I = 0x7f0a0542

.field public static final wrap_content:I = 0x7f0a054a


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

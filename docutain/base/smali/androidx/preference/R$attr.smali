.class public final Landroidx/preference/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/preference/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final adjustable:I = 0x7f04002e

.field public static final allowDividerAbove:I = 0x7f040034

.field public static final allowDividerAfterLastItem:I = 0x7f040035

.field public static final allowDividerBelow:I = 0x7f040036

.field public static final checkBoxPreferenceStyle:I = 0x7f0400b9

.field public static final defaultValue:I = 0x7f040186

.field public static final dependency:I = 0x7f040189

.field public static final dialogIcon:I = 0x7f04018d

.field public static final dialogLayout:I = 0x7f04018e

.field public static final dialogMessage:I = 0x7f04018f

.field public static final dialogPreferenceStyle:I = 0x7f040190

.field public static final dialogTitle:I = 0x7f040193

.field public static final disableDependentsState:I = 0x7f040194

.field public static final dropdownPreferenceStyle:I = 0x7f0401b0

.field public static final editTextPreferenceStyle:I = 0x7f0401b5

.field public static final enableCopying:I = 0x7f0401bc

.field public static final enabled:I = 0x7f0401be

.field public static final entries:I = 0x7f0401cb

.field public static final entryValues:I = 0x7f0401cc

.field public static final fragment:I = 0x7f04022e

.field public static final icon:I = 0x7f04024c

.field public static final iconSpaceReserved:I = 0x7f040251

.field public static final initialExpandedChildrenCount:I = 0x7f040268

.field public static final isPreferenceVisible:I = 0x7f04026e

.field public static final key:I = 0x7f04028c

.field public static final layout:I = 0x7f040297

.field public static final maxHeight:I = 0x7f04033b

.field public static final maxWidth:I = 0x7f04033f

.field public static final min:I = 0x7f040346

.field public static final negativeButtonText:I = 0x7f040388

.field public static final order:I = 0x7f040397

.field public static final orderingFromXml:I = 0x7f040398

.field public static final persistent:I = 0x7f0403b4

.field public static final positiveButtonText:I = 0x7f0403c4

.field public static final preferenceCategoryStyle:I = 0x7f0403c5

.field public static final preferenceCategoryTitleTextAppearance:I = 0x7f0403c6

.field public static final preferenceCategoryTitleTextColor:I = 0x7f0403c7

.field public static final preferenceFragmentCompatStyle:I = 0x7f0403c8

.field public static final preferenceFragmentListStyle:I = 0x7f0403c9

.field public static final preferenceFragmentStyle:I = 0x7f0403ca

.field public static final preferenceInformationStyle:I = 0x7f0403cb

.field public static final preferenceScreenStyle:I = 0x7f0403cc

.field public static final preferenceStyle:I = 0x7f0403cd

.field public static final preferenceTheme:I = 0x7f0403ce

.field public static final seekBarIncrement:I = 0x7f04057e

.field public static final seekBarPreferenceStyle:I = 0x7f04057f

.field public static final selectable:I = 0x7f040581

.field public static final selectableItemBackground:I = 0x7f040582

.field public static final shouldDisableView:I = 0x7f040593

.field public static final showSeekBarValue:I = 0x7f04059b

.field public static final singleLineTitle:I = 0x7f0405a7

.field public static final summary:I = 0x7f0405e9

.field public static final summaryOff:I = 0x7f0405ea

.field public static final summaryOn:I = 0x7f0405eb

.field public static final switchPreferenceCompatStyle:I = 0x7f0405ef

.field public static final switchPreferenceStyle:I = 0x7f0405f0

.field public static final switchTextOff:I = 0x7f0405f3

.field public static final switchTextOn:I = 0x7f0405f4

.field public static final title:I = 0x7f040673

.field public static final updatesContinuously:I = 0x7f0406a6

.field public static final useSimpleSummaryProvider:I = 0x7f0406ac

.field public static final widgetLayout:I = 0x7f0406be


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

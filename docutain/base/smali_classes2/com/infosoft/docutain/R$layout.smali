.class public final Lcom/infosoft/docutain/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infosoft/docutain/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f0d0000

.field public static final abc_action_bar_up_container:I = 0x7f0d0001

.field public static final abc_action_menu_item_layout:I = 0x7f0d0002

.field public static final abc_action_menu_layout:I = 0x7f0d0003

.field public static final abc_action_mode_bar:I = 0x7f0d0004

.field public static final abc_action_mode_close_item_material:I = 0x7f0d0005

.field public static final abc_activity_chooser_view:I = 0x7f0d0006

.field public static final abc_activity_chooser_view_list_item:I = 0x7f0d0007

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f0d0008

.field public static final abc_alert_dialog_material:I = 0x7f0d0009

.field public static final abc_alert_dialog_title_material:I = 0x7f0d000a

.field public static final abc_cascading_menu_item_layout:I = 0x7f0d000b

.field public static final abc_dialog_title_material:I = 0x7f0d000c

.field public static final abc_expanded_menu_layout:I = 0x7f0d000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f0d000e

.field public static final abc_list_menu_item_icon:I = 0x7f0d000f

.field public static final abc_list_menu_item_layout:I = 0x7f0d0010

.field public static final abc_list_menu_item_radio:I = 0x7f0d0011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f0d0012

.field public static final abc_popup_menu_item_layout:I = 0x7f0d0013

.field public static final abc_screen_content_include:I = 0x7f0d0014

.field public static final abc_screen_simple:I = 0x7f0d0015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f0d0016

.field public static final abc_screen_toolbar:I = 0x7f0d0017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f0d0018

.field public static final abc_search_view:I = 0x7f0d0019

.field public static final abc_select_dialog_material:I = 0x7f0d001a

.field public static final abc_tooltip:I = 0x7f0d001b

.field public static final addialog:I = 0x7f0d001c

.field public static final addpagesheet:I = 0x7f0d001d

.field public static final aditemcell:I = 0x7f0d001e

.field public static final aditemcellcompact:I = 0x7f0d001f

.field public static final admob_empty_layout:I = 0x7f0d0020

.field public static final bottomtablayout:I = 0x7f0d0021

.field public static final browser_actions_context_menu_page:I = 0x7f0d0022

.field public static final browser_actions_context_menu_row:I = 0x7f0d0023

.field public static final browserlayout:I = 0x7f0d0024

.field public static final camerafragment:I = 0x7f0d0025

.field public static final cameraprogressdialog:I = 0x7f0d0026

.field public static final cameraxactivity:I = 0x7f0d0027

.field public static final collectionbottomsheet:I = 0x7f0d0028

.field public static final contactlistitem:I = 0x7f0d0029

.field public static final croppingpage:I = 0x7f0d002a

.field public static final custom_dialog:I = 0x7f0d002b

.field public static final customalertdialog:I = 0x7f0d002c

.field public static final design_bottom_navigation_item:I = 0x7f0d002d

.field public static final design_bottom_sheet_dialog:I = 0x7f0d002e

.field public static final design_layout_snackbar:I = 0x7f0d002f

.field public static final design_layout_snackbar_include:I = 0x7f0d0030

.field public static final design_layout_tab_icon:I = 0x7f0d0031

.field public static final design_layout_tab_text:I = 0x7f0d0032

.field public static final design_menu_item_action_area:I = 0x7f0d0033

.field public static final design_navigation_item:I = 0x7f0d0034

.field public static final design_navigation_item_header:I = 0x7f0d0035

.field public static final design_navigation_item_separator:I = 0x7f0d0036

.field public static final design_navigation_item_subheader:I = 0x7f0d0037

.field public static final design_navigation_menu:I = 0x7f0d0038

.field public static final design_navigation_menu_item:I = 0x7f0d0039

.field public static final design_text_input_end_icon:I = 0x7f0d003a

.field public static final design_text_input_start_icon:I = 0x7f0d003b

.field public static final docviewer:I = 0x7f0d003c

.field public static final dokitemcell:I = 0x7f0d003d

.field public static final dokitemcellcompact:I = 0x7f0d003e

.field public static final dropdownitem1line:I = 0x7f0d003f

.field public static final dyncontentpopup:I = 0x7f0d0040

.field public static final edgedetectiondialog:I = 0x7f0d0041

.field public static final edit_horizontal_pager:I = 0x7f0d0042

.field public static final edit_titletextview:I = 0x7f0d0043

.field public static final edit_toolbar_bottom:I = 0x7f0d0044

.field public static final editbottomsheet:I = 0x7f0d0045

.field public static final editimageactivity:I = 0x7f0d0046

.field public static final editimagemenu:I = 0x7f0d0047

.field public static final entryalert:I = 0x7f0d0048

.field public static final expand_button:I = 0x7f0d0049

.field public static final fallbacktabbardonotuse:I = 0x7f0d004a

.field public static final fallbacktoolbardonotuse:I = 0x7f0d004b

.field public static final fingerprint_dialog_layout:I = 0x7f0d004c

.field public static final flyoutcontent:I = 0x7f0d004d

.field public static final folderview:I = 0x7f0d004e

.field public static final gnt_medium_template_view:I = 0x7f0d004f

.field public static final gnt_small_template_view:I = 0x7f0d0050

.field public static final image_frame:I = 0x7f0d0051

.field public static final ime_base_split_test_activity:I = 0x7f0d0052

.field public static final ime_secondary_split_test_activity:I = 0x7f0d0053

.field public static final importsheet:I = 0x7f0d0054

.field public static final inboxbottombar:I = 0x7f0d0055

.field public static final index_bottomsheet_textfields:I = 0x7f0d0056

.field public static final indexinfosbottomsheet:I = 0x7f0d0057

.field public static final item_show_case_content:I = 0x7f0d0058

.field public static final launchscreen:I = 0x7f0d0059

.field public static final linearprogressbar:I = 0x7f0d005a

.field public static final listviewsheet:I = 0x7f0d005b

.field public static final m3_alert_dialog:I = 0x7f0d005c

.field public static final m3_alert_dialog_actions:I = 0x7f0d005d

.field public static final m3_alert_dialog_title:I = 0x7f0d005e

.field public static final m3_auto_complete_simple_item:I = 0x7f0d005f

.field public static final m3_side_sheet_dialog:I = 0x7f0d0060

.field public static final main:I = 0x7f0d0061

.field public static final material_auto_complete:I = 0x7f0d0062

.field public static final material_autocomplete_dense:I = 0x7f0d0063

.field public static final material_chip_input_combo:I = 0x7f0d0064

.field public static final material_clock_display:I = 0x7f0d0065

.field public static final material_clock_display_divider:I = 0x7f0d0066

.field public static final material_clock_period_toggle:I = 0x7f0d0067

.field public static final material_clock_period_toggle_land:I = 0x7f0d0068

.field public static final material_clockface_textview:I = 0x7f0d0069

.field public static final material_clockface_view:I = 0x7f0d006a

.field public static final material_radial_view_group:I = 0x7f0d006b

.field public static final material_textinput_timepicker:I = 0x7f0d006c

.field public static final material_time_chip:I = 0x7f0d006d

.field public static final material_time_input:I = 0x7f0d006e

.field public static final material_timepicker:I = 0x7f0d006f

.field public static final material_timepicker_dialog:I = 0x7f0d0070

.field public static final material_timepicker_textinput_display:I = 0x7f0d0071

.field public static final materialbutton:I = 0x7f0d0072

.field public static final materialbuttontogglegroup:I = 0x7f0d0073

.field public static final materialeditor:I = 0x7f0d0074

.field public static final materialentry:I = 0x7f0d0075

.field public static final materialentrydense:I = 0x7f0d0076

.field public static final materialpasswordentry:I = 0x7f0d0077

.field public static final materialpickertextinput:I = 0x7f0d0078

.field public static final mtrl_alert_dialog:I = 0x7f0d0079

.field public static final mtrl_alert_dialog_actions:I = 0x7f0d007a

.field public static final mtrl_alert_dialog_title:I = 0x7f0d007b

.field public static final mtrl_alert_select_dialog_item:I = 0x7f0d007c

.field public static final mtrl_alert_select_dialog_multichoice:I = 0x7f0d007d

.field public static final mtrl_alert_select_dialog_singlechoice:I = 0x7f0d007e

.field public static final mtrl_auto_complete_simple_item:I = 0x7f0d007f

.field public static final mtrl_calendar_day:I = 0x7f0d0080

.field public static final mtrl_calendar_day_of_week:I = 0x7f0d0081

.field public static final mtrl_calendar_days_of_week:I = 0x7f0d0082

.field public static final mtrl_calendar_horizontal:I = 0x7f0d0083

.field public static final mtrl_calendar_month:I = 0x7f0d0084

.field public static final mtrl_calendar_month_labeled:I = 0x7f0d0085

.field public static final mtrl_calendar_month_navigation:I = 0x7f0d0086

.field public static final mtrl_calendar_months:I = 0x7f0d0087

.field public static final mtrl_calendar_vertical:I = 0x7f0d0088

.field public static final mtrl_calendar_year:I = 0x7f0d0089

.field public static final mtrl_layout_snackbar:I = 0x7f0d008a

.field public static final mtrl_layout_snackbar_include:I = 0x7f0d008b

.field public static final mtrl_navigation_rail_item:I = 0x7f0d008c

.field public static final mtrl_picker_actions:I = 0x7f0d008d

.field public static final mtrl_picker_dialog:I = 0x7f0d008e

.field public static final mtrl_picker_fullscreen:I = 0x7f0d008f

.field public static final mtrl_picker_header_dialog:I = 0x7f0d0090

.field public static final mtrl_picker_header_fullscreen:I = 0x7f0d0091

.field public static final mtrl_picker_header_selection_text:I = 0x7f0d0092

.field public static final mtrl_picker_header_title_text:I = 0x7f0d0093

.field public static final mtrl_picker_header_toggle:I = 0x7f0d0094

.field public static final mtrl_picker_text_input_date:I = 0x7f0d0095

.field public static final mtrl_picker_text_input_date_range:I = 0x7f0d0096

.field public static final mtrl_search_bar:I = 0x7f0d0097

.field public static final mtrl_search_view:I = 0x7f0d0098

.field public static final mxmp_activity_picker:I = 0x7f0d0099

.field public static final mxmp_item_image:I = 0x7f0d009a

.field public static final mxmp_item_unknown:I = 0x7f0d009b

.field public static final notification_action:I = 0x7f0d009c

.field public static final notification_action_tombstone:I = 0x7f0d009d

.field public static final notification_media_action:I = 0x7f0d009e

.field public static final notification_media_cancel_action:I = 0x7f0d009f

.field public static final notification_template_big_media:I = 0x7f0d00a0

.field public static final notification_template_big_media_custom:I = 0x7f0d00a1

.field public static final notification_template_big_media_narrow:I = 0x7f0d00a2

.field public static final notification_template_big_media_narrow_custom:I = 0x7f0d00a3

.field public static final notification_template_custom_big:I = 0x7f0d00a4

.field public static final notification_template_icon_group:I = 0x7f0d00a5

.field public static final notification_template_lines_media:I = 0x7f0d00a6

.field public static final notification_template_media:I = 0x7f0d00a7

.field public static final notification_template_media_custom:I = 0x7f0d00a8

.field public static final notification_template_part_chronometer:I = 0x7f0d00a9

.field public static final notification_template_part_time:I = 0x7f0d00aa

.field public static final notificationbottomsheet:I = 0x7f0d00ab

.field public static final partnercell:I = 0x7f0d00ac

.field public static final pdfviewer:I = 0x7f0d00ad

.field public static final preference:I = 0x7f0d00ae

.field public static final preference_category:I = 0x7f0d00af

.field public static final preference_category_material:I = 0x7f0d00b0

.field public static final preference_dialog_edittext:I = 0x7f0d00b1

.field public static final preference_dropdown:I = 0x7f0d00b2

.field public static final preference_dropdown_material:I = 0x7f0d00b3

.field public static final preference_information:I = 0x7f0d00b4

.field public static final preference_information_material:I = 0x7f0d00b5

.field public static final preference_list_fragment:I = 0x7f0d00b6

.field public static final preference_material:I = 0x7f0d00b7

.field public static final preference_recyclerview:I = 0x7f0d00b8

.field public static final preference_widget_checkbox:I = 0x7f0d00b9

.field public static final preference_widget_seekbar:I = 0x7f0d00ba

.field public static final preference_widget_seekbar_material:I = 0x7f0d00bb

.field public static final preference_widget_switch:I = 0x7f0d00bc

.field public static final preference_widget_switch_compat:I = 0x7f0d00bd

.field public static final progressdialoghorizontal:I = 0x7f0d00be

.field public static final pspdf__action_menu_layout:I = 0x7f0d00bf

.field public static final pspdf__alert_dialog_progress:I = 0x7f0d00c0

.field public static final pspdf__annotation_creator_input_dialog:I = 0x7f0d00c1

.field public static final pspdf__audio_inspector_bar:I = 0x7f0d00c2

.field public static final pspdf__color_palette_view:I = 0x7f0d00c3

.field public static final pspdf__color_picker_detail:I = 0x7f0d00c4

.field public static final pspdf__color_preview_view:I = 0x7f0d00c5

.field public static final pspdf__compare_documents_dialog:I = 0x7f0d00c6

.field public static final pspdf__comparison_hint_view:I = 0x7f0d00c7

.field public static final pspdf__content_editing_bar:I = 0x7f0d00c8

.field public static final pspdf__custom_color_picker:I = 0x7f0d00c9

.field public static final pspdf__custom_stamp_creator_layout:I = 0x7f0d00ca

.field public static final pspdf__document_editor_alert_dialog:I = 0x7f0d00cb

.field public static final pspdf__document_info_group:I = 0x7f0d00cc

.field public static final pspdf__document_info_item:I = 0x7f0d00cd

.field public static final pspdf__document_info_page_binding_item:I = 0x7f0d00ce

.field public static final pspdf__document_info_view:I = 0x7f0d00cf

.field public static final pspdf__document_view:I = 0x7f0d00d0

.field public static final pspdf__draw_electronic_signature_dialog_layout:I = 0x7f0d00d1

.field public static final pspdf__draw_electronic_signature_layout:I = 0x7f0d00d2

.field public static final pspdf__electronic_signature_typing_font_view_holder:I = 0x7f0d00d3

.field public static final pspdf__form_editing_bar:I = 0x7f0d00d4

.field public static final pspdf__gallery_item:I = 0x7f0d00d5

.field public static final pspdf__grid_list_item:I = 0x7f0d00d6

.field public static final pspdf__image_electronic_signature_dialog_layout:I = 0x7f0d00d7

.field public static final pspdf__image_electronic_signature_layout:I = 0x7f0d00d8

.field public static final pspdf__inspector_precision_spinner_item:I = 0x7f0d00d9

.field public static final pspdf__inspector_scale_unit_spinner_item:I = 0x7f0d00da

.field public static final pspdf__list_item_checked:I = 0x7f0d00db

.field public static final pspdf__loading_view:I = 0x7f0d00dc

.field public static final pspdf__loading_view_progress:I = 0x7f0d00dd

.field public static final pspdf__measurement_value_popup:I = 0x7f0d00de

.field public static final pspdf__media_dialog:I = 0x7f0d00df

.field public static final pspdf__note_editor_item_card_layout:I = 0x7f0d00e0

.field public static final pspdf__note_editor_layout:I = 0x7f0d00e1

.field public static final pspdf__note_editor_set_status_dialog_layout:I = 0x7f0d00e2

.field public static final pspdf__note_editor_set_status_dialog_layout_item:I = 0x7f0d00e3

.field public static final pspdf__note_editor_style_box_card_layout:I = 0x7f0d00e4

.field public static final pspdf__note_editor_style_box_header:I = 0x7f0d00e5

.field public static final pspdf__option_picker_custom_value_view:I = 0x7f0d00e6

.field public static final pspdf__option_picker_inspector_view:I = 0x7f0d00e7

.field public static final pspdf__outline_annotation_view:I = 0x7f0d00e8

.field public static final pspdf__outline_bookmarks_list_item:I = 0x7f0d00e9

.field public static final pspdf__outline_bookmarks_name_dialog:I = 0x7f0d00ea

.field public static final pspdf__outline_bookmarks_view:I = 0x7f0d00eb

.field public static final pspdf__outline_list_divider:I = 0x7f0d00ec

.field public static final pspdf__outline_list_view:I = 0x7f0d00ed

.field public static final pspdf__outline_pager_annotation_list_item:I = 0x7f0d00ee

.field public static final pspdf__outline_pager_annotation_page_item:I = 0x7f0d00ef

.field public static final pspdf__outline_pager_list_footer:I = 0x7f0d00f0

.field public static final pspdf__outline_pager_outline_list_item:I = 0x7f0d00f1

.field public static final pspdf__outline_pager_outline_list_no_match:I = 0x7f0d00f2

.field public static final pspdf__outline_view:I = 0x7f0d00f3

.field public static final pspdf__overflow_menu_view:I = 0x7f0d00f4

.field public static final pspdf__page_binding_view:I = 0x7f0d00f5

.field public static final pspdf__page_creator_dialog:I = 0x7f0d00f6

.field public static final pspdf__page_creator_page_color_view:I = 0x7f0d00f7

.field public static final pspdf__page_creator_page_pattern_item:I = 0x7f0d00f8

.field public static final pspdf__password_view:I = 0x7f0d00f9

.field public static final pspdf__pdf_activity:I = 0x7f0d00fa

.field public static final pspdf__pdf_fragment_error_view:I = 0x7f0d00fb

.field public static final pspdf__preview_uri_dialog:I = 0x7f0d00fc

.field public static final pspdf__progress_dialog:I = 0x7f0d00fd

.field public static final pspdf__reader_view:I = 0x7f0d00fe

.field public static final pspdf__recycler_view_with_empty_message:I = 0x7f0d00ff

.field public static final pspdf__redaction_view:I = 0x7f0d0100

.field public static final pspdf__search_footer:I = 0x7f0d0101

.field public static final pspdf__search_item:I = 0x7f0d0102

.field public static final pspdf__search_view_inline:I = 0x7f0d0103

.field public static final pspdf__search_view_modular:I = 0x7f0d0104

.field public static final pspdf__settings_layout:I = 0x7f0d0105

.field public static final pspdf__share_dialog:I = 0x7f0d0106

.field public static final pspdf__signature_info_dialog:I = 0x7f0d0107

.field public static final pspdf__signature_layout_add_new_signature:I = 0x7f0d0108

.field public static final pspdf__signature_list_dialog:I = 0x7f0d0109

.field public static final pspdf__signature_list_item:I = 0x7f0d010a

.field public static final pspdf__signer_list_item:I = 0x7f0d010b

.field public static final pspdf__signer_list_selected_item:I = 0x7f0d010c

.field public static final pspdf__signer_list_view_popup:I = 0x7f0d010d

.field public static final pspdf__stamps_picker_custom_section:I = 0x7f0d010e

.field public static final pspdf__stamps_picker_list_item:I = 0x7f0d010f

.field public static final pspdf__stamps_selection_grid:I = 0x7f0d0110

.field public static final pspdf__tab_item:I = 0x7f0d0111

.field public static final pspdf__thumbnail_grid_item_view:I = 0x7f0d0112

.field public static final pspdf__thumbnail_grid_view:I = 0x7f0d0113

.field public static final pspdf__toolbar_main:I = 0x7f0d0114

.field public static final pspdf__typing_electronic_signature_dialog_layout:I = 0x7f0d0115

.field public static final pspdf__typing_electronic_signature_layout:I = 0x7f0d0116

.field public static final pspdf__uvv_on_error_layout:I = 0x7f0d0117

.field public static final pspdf__uvv_on_loading_layout:I = 0x7f0d0118

.field public static final pspdf__uvv_player_controller:I = 0x7f0d0119

.field public static final pspdf__value_slider:I = 0x7f0d011a

.field public static final pspdf__vertical_scrollbar_indicator:I = 0x7f0d011b

.field public static final pspdf__view_empty_activity:I = 0x7f0d011c

.field public static final pspdf__view_inspector_color_picker:I = 0x7f0d011d

.field public static final pspdf__view_inspector_divider:I = 0x7f0d011e

.field public static final pspdf__view_inspector_font_list_item:I = 0x7f0d011f

.field public static final pspdf__view_inspector_font_picker:I = 0x7f0d0120

.field public static final pspdf__view_inspector_measurement_value:I = 0x7f0d0121

.field public static final pspdf__view_inspector_options_picker:I = 0x7f0d0122

.field public static final pspdf__view_inspector_precision_picker:I = 0x7f0d0123

.field public static final pspdf__view_inspector_scale_calibration_picker:I = 0x7f0d0124

.field public static final pspdf__view_inspector_scale_picker:I = 0x7f0d0125

.field public static final pspdf__view_inspector_slider_picker:I = 0x7f0d0126

.field public static final pspdf__view_inspector_snapping_picker:I = 0x7f0d0127

.field public static final pspdf__view_inspector_text:I = 0x7f0d0128

.field public static final pspdf__view_inspector_toggle_picker:I = 0x7f0d0129

.field public static final pspdf__view_inspector_z_index_picker:I = 0x7f0d012a

.field public static final pspdf__view_options_picker_item:I = 0x7f0d012b

.field public static final pspdf__view_pager_tab_view:I = 0x7f0d012c

.field public static final pspdf__view_settings_mode_picker:I = 0x7f0d012d

.field public static final pspdf__view_settings_mode_picker_item:I = 0x7f0d012e

.field public static final pspdf__you_tube_activity:I = 0x7f0d012f

.field public static final radiobutton:I = 0x7f0d0130

.field public static final radiogroup:I = 0x7f0d0131

.field public static final rootlayout:I = 0x7f0d0132

.field public static final scanpositiondialog:I = 0x7f0d0133

.field public static final scanqualityquestionview:I = 0x7f0d0134

.field public static final scanqualityquestionviewlayout:I = 0x7f0d0135

.field public static final searchbar:I = 0x7f0d0136

.field public static final select_dialog_item_material:I = 0x7f0d0137

.field public static final select_dialog_multichoice_material:I = 0x7f0d0138

.field public static final select_dialog_singlechoice_material:I = 0x7f0d0139

.field public static final sharesheet:I = 0x7f0d013a

.field public static final shellcontent:I = 0x7f0d013b

.field public static final simple_text_toast:I = 0x7f0d013c

.field public static final sortsheet:I = 0x7f0d013d

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0d013e

.field public static final tabbar:I = 0x7f0d013f

.field public static final tags_input_layout:I = 0x7f0d0140

.field public static final textinputlayoutfilledbox:I = 0x7f0d0141

.field public static final toolbar:I = 0x7f0d0142

.field public static final view_pager_page:I = 0x7f0d0143


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

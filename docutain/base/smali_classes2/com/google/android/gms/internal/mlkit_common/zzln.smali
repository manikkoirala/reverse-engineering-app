.class public final Lcom/google/android/gms/internal/mlkit_common/zzln;
.super Ljava/lang/Object;
.source "com.google.mlkit:common@@18.8.0"


# instance fields
.field private zza:Ljava/lang/String;

.field private zzb:Lcom/google/android/gms/internal/mlkit_common/zzlp;

.field private zzc:Ljava/lang/String;

.field private zzd:Lcom/google/android/gms/internal/mlkit_common/zzlo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static bridge synthetic zze(Lcom/google/android/gms/internal/mlkit_common/zzln;)Lcom/google/android/gms/internal/mlkit_common/zzlo;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_common/zzln;->zzd:Lcom/google/android/gms/internal/mlkit_common/zzlo;

    return-object p0
.end method

.method static bridge synthetic zzf(Lcom/google/android/gms/internal/mlkit_common/zzln;)Lcom/google/android/gms/internal/mlkit_common/zzlp;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_common/zzln;->zzb:Lcom/google/android/gms/internal/mlkit_common/zzlp;

    return-object p0
.end method

.method static bridge synthetic zzh(Lcom/google/android/gms/internal/mlkit_common/zzln;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_common/zzln;->zzc:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic zzi(Lcom/google/android/gms/internal/mlkit_common/zzln;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_common/zzln;->zza:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public final zza(Ljava/lang/String;)Lcom/google/android/gms/internal/mlkit_common/zzln;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzln;->zzc:Ljava/lang/String;

    return-object p0
.end method

.method public final zzb(Lcom/google/android/gms/internal/mlkit_common/zzlo;)Lcom/google/android/gms/internal/mlkit_common/zzln;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzln;->zzd:Lcom/google/android/gms/internal/mlkit_common/zzlo;

    return-object p0
.end method

.method public final zzc(Ljava/lang/String;)Lcom/google/android/gms/internal/mlkit_common/zzln;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzln;->zza:Ljava/lang/String;

    return-object p0
.end method

.method public final zzd(Lcom/google/android/gms/internal/mlkit_common/zzlp;)Lcom/google/android/gms/internal/mlkit_common/zzln;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzln;->zzb:Lcom/google/android/gms/internal/mlkit_common/zzlp;

    return-object p0
.end method

.method public final zzg()Lcom/google/android/gms/internal/mlkit_common/zzlr;
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/mlkit_common/zzlr;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/mlkit_common/zzlr;-><init>(Lcom/google/android/gms/internal/mlkit_common/zzln;Lcom/google/android/gms/internal/mlkit_common/zzlq;)V

    return-object v0
.end method

.class public final Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"


# instance fields
.field private final zza:Ljava/lang/Long;

.field private final zzb:Ljava/lang/Long;

.field private final zzc:Ljava/lang/Long;

.field private final zzd:Ljava/lang/Long;

.field private final zze:Ljava/lang/Long;

.field private final zzf:Ljava/lang/Long;


# direct methods
.method synthetic constructor <init>(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlz;Lcom/google/android/gms/internal/mlkit_vision_text_common/zzma;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlz;->zzj(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlz;)Ljava/lang/Long;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;->zza:Ljava/lang/Long;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlz;->zzl(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlz;)Ljava/lang/Long;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;->zzb:Ljava/lang/Long;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlz;->zzh(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlz;)Ljava/lang/Long;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;->zzc:Ljava/lang/Long;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlz;->zzi(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlz;)Ljava/lang/Long;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;->zzd:Ljava/lang/Long;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlz;->zzk(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlz;)Ljava/lang/Long;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;->zze:Ljava/lang/Long;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlz;->zzm(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlz;)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;->zzf:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public final zza()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;->zzc:Ljava/lang/Long;

    return-object v0
.end method

.method public final zzb()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;->zzd:Ljava/lang/Long;

    return-object v0
.end method

.method public final zzc()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;->zza:Ljava/lang/Long;

    return-object v0
.end method

.method public final zzd()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;->zze:Ljava/lang/Long;

    return-object v0
.end method

.method public final zze()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;->zzb:Ljava/lang/Long;

    return-object v0
.end method

.method public final zzf()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;->zzf:Ljava/lang/Long;

    return-object v0
.end method

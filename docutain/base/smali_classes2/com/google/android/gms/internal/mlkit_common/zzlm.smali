.class public final Lcom/google/android/gms/internal/mlkit_common/zzlm;
.super Ljava/lang/Object;
.source "com.google.mlkit:common@@18.8.0"


# instance fields
.field private final zza:Lcom/google/android/gms/internal/mlkit_common/zzlv;

.field private final zzb:Ljava/lang/Long;

.field private final zzc:Lcom/google/android/gms/internal/mlkit_common/zzle;

.field private final zzd:Ljava/lang/Long;

.field private final zze:Lcom/google/android/gms/internal/mlkit_common/zzlk;

.field private final zzf:Ljava/lang/Long;


# direct methods
.method synthetic constructor <init>(Lcom/google/android/gms/internal/mlkit_common/zzlj;Lcom/google/android/gms/internal/mlkit_common/zzll;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_common/zzlj;->zzj(Lcom/google/android/gms/internal/mlkit_common/zzlj;)Lcom/google/android/gms/internal/mlkit_common/zzlv;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_common/zzlm;->zza:Lcom/google/android/gms/internal/mlkit_common/zzlv;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_common/zzlj;->zzm(Lcom/google/android/gms/internal/mlkit_common/zzlj;)Ljava/lang/Long;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_common/zzlm;->zzb:Ljava/lang/Long;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_common/zzlj;->zza(Lcom/google/android/gms/internal/mlkit_common/zzlj;)Lcom/google/android/gms/internal/mlkit_common/zzle;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_common/zzlm;->zzc:Lcom/google/android/gms/internal/mlkit_common/zzle;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_common/zzlj;->zzl(Lcom/google/android/gms/internal/mlkit_common/zzlj;)Ljava/lang/Long;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_common/zzlm;->zzd:Ljava/lang/Long;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_common/zzlj;->zzh(Lcom/google/android/gms/internal/mlkit_common/zzlj;)Lcom/google/android/gms/internal/mlkit_common/zzlk;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_common/zzlm;->zze:Lcom/google/android/gms/internal/mlkit_common/zzlk;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_common/zzlj;->zzk(Lcom/google/android/gms/internal/mlkit_common/zzlj;)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzlm;->zzf:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public final zza()Lcom/google/android/gms/internal/mlkit_common/zzle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_common/zzlm;->zzc:Lcom/google/android/gms/internal/mlkit_common/zzle;

    return-object v0
.end method

.method public final zzb()Lcom/google/android/gms/internal/mlkit_common/zzlk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_common/zzlm;->zze:Lcom/google/android/gms/internal/mlkit_common/zzlk;

    return-object v0
.end method

.method public final zzc()Lcom/google/android/gms/internal/mlkit_common/zzlv;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_common/zzlm;->zza:Lcom/google/android/gms/internal/mlkit_common/zzlv;

    return-object v0
.end method

.method public final zzd()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_common/zzlm;->zzf:Ljava/lang/Long;

    return-object v0
.end method

.method public final zze()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_common/zzlm;->zzd:Ljava/lang/Long;

    return-object v0
.end method

.method public final zzf()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_common/zzlm;->zzb:Ljava/lang/Long;

    return-object v0
.end method

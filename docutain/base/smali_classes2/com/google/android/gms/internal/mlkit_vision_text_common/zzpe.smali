.class public final Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"


# instance fields
.field private zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmo;

.field private zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmi;

.field private zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static bridge synthetic zza(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmi;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmi;

    return-object p0
.end method

.method static bridge synthetic zzb(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmo;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmo;

    return-object p0
.end method

.method static bridge synthetic zzg(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;

    return-object p0
.end method


# virtual methods
.method public final zzc(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmi;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmi;

    return-object p0
.end method

.method public final zzd(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmo;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmo;

    return-object p0
.end method

.method public final zze(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;

    return-object p0
.end method

.method public final zzf()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;-><init>(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpf;)V

    return-object v0
.end method

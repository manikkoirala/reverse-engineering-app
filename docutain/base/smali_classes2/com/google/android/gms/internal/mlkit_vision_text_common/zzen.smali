.class public final Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"


# instance fields
.field private zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;

.field private zzb:Ljava/lang/Boolean;

.field private zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static bridge synthetic zze(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;

    return-object p0
.end method

.method static bridge synthetic zzf(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;

    return-object p0
.end method

.method static bridge synthetic zzg(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;)Ljava/lang/Boolean;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;->zzb:Ljava/lang/Boolean;

    return-object p0
.end method


# virtual methods
.method public final zza(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;

    return-object p0
.end method

.method public final zzb(Ljava/lang/Boolean;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;->zzb:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final zzc(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;

    return-object p0
.end method

.method public final zzd()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzep;
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzep;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzep;-><init>(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;Lcom/google/android/gms/internal/mlkit_vision_text_common/zzeo;)V

    return-object v0
.end method

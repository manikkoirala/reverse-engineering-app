.class public final synthetic Lcom/google/android/gms/internal/mlkit_vision_text_common/zzra;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;

.field public final synthetic zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

.field public final synthetic zzc:Ljava/lang/Object;

.field public final synthetic zzd:J

.field public final synthetic zze:Lcom/google/mlkit/vision/text/internal/zzp;


# direct methods
.method public synthetic constructor <init>(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;Ljava/lang/Object;JLcom/google/mlkit/vision/text/internal/zzp;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzra;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzra;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

    iput-object p3, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzra;->zzc:Ljava/lang/Object;

    iput-wide p4, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzra;->zzd:J

    iput-object p6, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzra;->zze:Lcom/google/mlkit/vision/text/internal/zzp;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzra;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;

    iget-object v1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzra;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

    iget-object v2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzra;->zzc:Ljava/lang/Object;

    iget-wide v3, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzra;->zzd:J

    iget-object v5, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzra;->zze:Lcom/google/mlkit/vision/text/internal/zzp;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;->zzh(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;Ljava/lang/Object;JLcom/google/mlkit/vision/text/internal/zzp;)V

    return-void
.end method

.class public final Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"


# instance fields
.field private zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpu;

.field private zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

.field private zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

.field private zzd:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;

.field private zze:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpd;

.field private zzf:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static bridge synthetic zza(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzf:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;

    return-object p0
.end method

.method static bridge synthetic zzb(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

    return-object p0
.end method

.method static bridge synthetic zzc(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

    return-object p0
.end method

.method static bridge synthetic zzk(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpd;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zze:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpd;

    return-object p0
.end method

.method static bridge synthetic zzl(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzd:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;

    return-object p0
.end method

.method static bridge synthetic zzm(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpu;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpu;

    return-object p0
.end method


# virtual methods
.method public final zzd(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzf:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;

    return-object p0
.end method

.method public final zze(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

    return-object p0
.end method

.method public final zzf(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

    return-object p0
.end method

.method public final zzg(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpd;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zze:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpd;

    return-object p0
.end method

.method public final zzh(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzd:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;

    return-object p0
.end method

.method public final zzi(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpu;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpu;

    return-object p0
.end method

.method public final zzj()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;-><init>(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmy;)V

    return-object v0
.end method

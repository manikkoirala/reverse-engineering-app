.class public abstract Lcom/google/android/gms/internal/mlkit_common/zzpt;
.super Ljava/lang/Object;
.source "com.google.mlkit:common@@18.8.0"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static zzh()Lcom/google/android/gms/internal/mlkit_common/zzps;
    .locals 3

    .line 1
    new-instance v0, Lcom/google/android/gms/internal/mlkit_common/zzpf;

    invoke-direct {v0}, Lcom/google/android/gms/internal/mlkit_common/zzpf;-><init>()V

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/mlkit_common/zzpf;->zzg(Ljava/lang/String;)Lcom/google/android/gms/internal/mlkit_common/zzps;

    const/4 v1, 0x0

    .line 2
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/mlkit_common/zzps;->zzf(Z)Lcom/google/android/gms/internal/mlkit_common/zzps;

    .line 3
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/mlkit_common/zzps;->zze(Z)Lcom/google/android/gms/internal/mlkit_common/zzps;

    sget-object v2, Lcom/google/mlkit/common/sdkinternal/ModelType;->UNKNOWN:Lcom/google/mlkit/common/sdkinternal/ModelType;

    .line 4
    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/mlkit_common/zzps;->zzd(Lcom/google/mlkit/common/sdkinternal/ModelType;)Lcom/google/android/gms/internal/mlkit_common/zzps;

    sget-object v2, Lcom/google/android/gms/internal/mlkit_common/zzle;->zza:Lcom/google/android/gms/internal/mlkit_common/zzle;

    .line 5
    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/mlkit_common/zzps;->zzb(Lcom/google/android/gms/internal/mlkit_common/zzle;)Lcom/google/android/gms/internal/mlkit_common/zzps;

    sget-object v2, Lcom/google/android/gms/internal/mlkit_common/zzlk;->zza:Lcom/google/android/gms/internal/mlkit_common/zzlk;

    .line 6
    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/mlkit_common/zzps;->zza(Lcom/google/android/gms/internal/mlkit_common/zzlk;)Lcom/google/android/gms/internal/mlkit_common/zzps;

    .line 7
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/mlkit_common/zzps;->zzc(I)Lcom/google/android/gms/internal/mlkit_common/zzps;

    return-object v0
.end method


# virtual methods
.method public abstract zza()I
.end method

.method public abstract zzb()Lcom/google/mlkit/common/sdkinternal/ModelType;
.end method

.method public abstract zzc()Lcom/google/android/gms/internal/mlkit_common/zzle;
.end method

.method public abstract zzd()Lcom/google/android/gms/internal/mlkit_common/zzlk;
.end method

.method public abstract zze()Ljava/lang/String;
.end method

.method public abstract zzf()Z
.end method

.method public abstract zzg()Z
.end method

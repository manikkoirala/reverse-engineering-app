.class public final Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"


# instance fields
.field private final zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzep;

.field private final zzb:Ljava/lang/Integer;

.field private final zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;


# direct methods
.method synthetic constructor <init>(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;Lcom/google/android/gms/internal/mlkit_vision_text_common/zzeq;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;->zzd(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzep;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzep;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;->zzg(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;)Ljava/lang/Integer;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;->zzb:Ljava/lang/Integer;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;->zzf(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;

    return-void
.end method


# virtual methods
.method public final zza()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzep;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzep;

    return-object v0
.end method

.method public final zzb()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;

    return-object v0
.end method

.method public final zzc()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;->zzb:Ljava/lang/Integer;

    return-object v0
.end method

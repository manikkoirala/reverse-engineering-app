.class public final Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkz;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"

# interfaces
.implements Lcom/google/firebase/encoders/config/Configurator;


# static fields
.field public static final zza:Lcom/google/firebase/encoders/config/Configurator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkz;

    invoke-direct {v0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkz;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkz;->zza:Lcom/google/firebase/encoders/config/Configurator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final configure(Lcom/google/firebase/encoders/config/EncoderConfig;)V
    .locals 2

    .line 1
    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;

    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhx;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhx;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpu;

    .line 2
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkp;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkp;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzna;

    .line 3
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhy;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhy;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznd;

    .line 4
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzia;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzia;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznb;

    .line 5
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhz;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhz;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznc;

    .line 6
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzib;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzib;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlt;

    .line 7
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgz;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgz;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzls;

    .line 8
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgy;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgy;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmo;

    .line 9
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhq;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhq;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpm;

    .line 10
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkf;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkf;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlr;

    .line 11
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgx;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgx;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlq;

    .line 12
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgw;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgw;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznw;

    .line 13
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zziw;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zziw;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqo;

    .line 14
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhj;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhj;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmi;

    .line 15
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhm;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhm;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmd;

    .line 16
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhi;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhi;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznx;

    .line 17
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzix;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzix;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;

    .line 18
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkc;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkc;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;

    .line 19
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkd;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkd;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpd;

    .line 20
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkb;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkb;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznh;

    .line 21
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzih;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzih;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqn;

    .line 22
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgg;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgg;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzni;

    .line 23
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzii;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzii;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzof;

    .line 24
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjf;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjf;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzoi;

    .line 25
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzji;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzji;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzoh;

    .line 26
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjh;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjh;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzog;

    .line 27
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjg;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjg;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzor;

    .line 28
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjr;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjr;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzos;

    .line 29
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjs;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjs;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzou;

    .line 30
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzju;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzju;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzot;

    .line 31
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjt;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjt;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzng;

    .line 32
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzig;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzig;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzov;

    .line 33
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjv;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjv;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    .line 34
    sget-object v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjw;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjw;

    const-class v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzow;

    invoke-interface {p1, v1, v0}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzox;

    .line 35
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjx;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjx;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzoy;

    .line 36
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjy;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjy;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpa;

    .line 37
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjz;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjz;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzoz;

    .line 38
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzka;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzka;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzoq;

    .line 39
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjn;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjn;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzms;

    .line 40
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhv;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhv;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzoo;

    .line 41
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjp;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjp;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzon;

    .line 42
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjo;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjo;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzop;

    .line 43
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjq;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjq;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpl;

    .line 44
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzke;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzke;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqa;

    .line 45
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkv;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkv;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlf;

    .line 46
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgl;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgl;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzld;

    .line 47
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgj;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgj;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlc;

    .line 48
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgi;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgi;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzle;

    .line 49
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgk;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgk;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlh;

    .line 50
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgn;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgn;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlg;

    .line 51
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgm;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgm;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzli;

    .line 52
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgo;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgo;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlj;

    .line 53
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgp;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgp;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlk;

    .line 54
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgq;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgq;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzll;

    .line 55
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgr;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgr;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlm;

    .line 56
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgs;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgs;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzes;

    .line 57
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgc;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgc;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzeu;

    .line 58
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzge;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzge;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzet;

    .line 59
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgd;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgd;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmq;

    .line 60
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzht;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzht;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlu;

    .line 61
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzha;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzha;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdj;

    .line 62
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzew;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzew;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdi;

    .line 63
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzex;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzex;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;

    .line 64
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhg;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhg;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdl;

    .line 65
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzey;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzey;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdk;

    .line 66
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzez;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzez;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdx;

    .line 67
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfk;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfk;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    .line 68
    sget-object v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfl;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfl;

    const-class v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdw;

    invoke-interface {p1, v1, v0}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdn;

    .line 69
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfa;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfa;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdm;

    .line 70
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfb;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfb;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzed;

    .line 71
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfq;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfq;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzec;

    .line 72
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfr;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfr;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzeh;

    .line 73
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfu;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfu;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzeg;

    .line 74
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfv;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfv;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;

    .line 75
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzga;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzga;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzep;

    .line 76
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgb;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgb;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzej;

    .line 77
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfw;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfw;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzei;

    .line 78
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfx;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfx;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzel;

    .line 79
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfy;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfy;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzek;

    .line 80
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfz;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfz;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqi;

    .line 81
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzki;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzki;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqb;

    .line 82
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhb;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhb;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqf;

    .line 83
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzif;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzif;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqe;

    .line 84
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzie;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzie;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqc;

    .line 85
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhk;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhk;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqh;

    .line 86
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkh;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkh;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqg;

    .line 87
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkg;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkg;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqj;

    .line 88
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkj;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkj;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqd;

    .line 89
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhr;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhr;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqm;

    .line 90
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkx;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkx;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzql;

    .line 91
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzky;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzky;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqk;

    .line 92
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkw;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkw;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpn;

    .line 93
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkk;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkk;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmp;

    .line 94
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhs;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhs;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmt;

    .line 95
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhw;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhw;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlb;

    .line 96
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgh;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgh;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmj;

    .line 97
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhn;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhn;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmr;

    .line 98
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhu;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhu;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmc;

    .line 99
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhh;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhh;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlw;

    .line 100
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhd;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhd;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlx;

    .line 101
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhe;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhe;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    .line 102
    sget-object v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhc;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhc;

    const-class v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlv;

    invoke-interface {p1, v1, v0}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzly;

    .line 103
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhf;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhf;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznf;

    .line 104
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzid;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzid;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzne;

    .line 105
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzic;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzic;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdh;

    .line 106
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzev;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzev;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpx;

    .line 107
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzks;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzks;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpz;

    .line 108
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzku;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzku;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpy;

    .line 109
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkt;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkt;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzla;

    .line 110
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgf;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgf;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlp;

    .line 111
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgv;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgv;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzlo;

    .line 112
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgu;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgu;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzln;

    .line 113
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgt;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzgt;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznt;

    .line 114
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzit;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzit;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznv;

    .line 115
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zziv;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zziv;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznu;

    .line 116
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zziu;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zziu;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdv;

    .line 117
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfi;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfi;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdu;

    .line 118
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfj;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfj;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzny;

    .line 119
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zziy;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zziy;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzob;

    .line 120
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjb;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjb;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznz;

    .line 121
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zziz;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zziz;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzoa;

    .line 122
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzja;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzja;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdz;

    .line 123
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfm;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfm;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdy;

    .line 124
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfn;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfn;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpr;

    .line 125
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzko;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzko;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpq;

    .line 126
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkn;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkn;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpv;

    .line 127
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkq;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkq;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpw;

    .line 128
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkr;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkr;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzoj;

    .line 129
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjj;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjj;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzom;

    .line 130
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjm;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjm;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzok;

    .line 131
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjk;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjk;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzol;

    .line 132
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjl;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjl;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzml;

    .line 133
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhp;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhp;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzef;

    .line 134
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfs;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfs;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzee;

    .line 135
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzft;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzft;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    .line 136
    sget-object v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzho;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzho;

    const-class v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmk;

    invoke-interface {p1, v1, v0}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzme;

    .line 137
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhl;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzhl;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzoc;

    .line 138
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjc;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjc;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzoe;

    .line 139
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzje;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzje;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzod;

    .line 140
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjd;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzjd;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzeb;

    .line 141
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfo;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfo;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzea;

    .line 142
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfp;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfp;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznm;

    .line 143
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzim;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzim;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznn;

    .line 144
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzin;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzin;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzno;

    .line 145
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzio;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzio;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdr;

    .line 146
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfe;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfe;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdq;

    .line 147
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzff;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzff;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznj;

    .line 148
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzij;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzij;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznk;

    .line 149
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzik;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzik;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznl;

    .line 150
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzil;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzil;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdp;

    .line 151
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfc;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfc;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdo;

    .line 152
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfd;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfd;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznp;

    .line 153
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzip;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzip;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznq;

    .line 154
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zziq;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zziq;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zznr;

    .line 155
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzir;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzir;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzns;

    .line 156
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzis;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzis;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzdt;

    .line 157
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfg;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfg;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzds;

    .line 158
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfh;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzfh;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpp;

    .line 159
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkl;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkl;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpo;

    .line 160
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkm;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzkm;

    invoke-interface {p1, v0, v1}, Lcom/google/firebase/encoders/config/EncoderConfig;->registerEncoder(Ljava/lang/Class;Lcom/google/firebase/encoders/ObjectEncoder;)Lcom/google/firebase/encoders/config/EncoderConfig;

    return-void
.end method

.class public final Lcom/google/android/gms/internal/mlkit_vision_text_common/zzro;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"


# static fields
.field private static zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrn;


# direct methods
.method public static declared-synchronized zza(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqv;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;
    .locals 3

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzro;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzro;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrn;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrn;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrn;-><init>(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrm;)V

    sput-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzro;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrn;

    :cond_0
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzro;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrn;

    .line 2
    invoke-virtual {v1, p0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrn;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized zzb(Ljava/lang/String;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;
    .locals 1

    const-class v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzro;

    monitor-enter v0

    .line 1
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqv;->zzd(Ljava/lang/String;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqu;

    move-result-object p0

    .line 2
    invoke-virtual {p0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqu;->zzd()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqv;

    move-result-object p0

    .line 3
    invoke-static {p0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzro;->zza(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqv;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;

    move-result-object p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

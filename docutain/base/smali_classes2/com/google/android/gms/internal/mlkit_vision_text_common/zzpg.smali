.class public final Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"


# instance fields
.field private final zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmo;

.field private final zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmi;

.field private final zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;


# direct methods
.method synthetic constructor <init>(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpf;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zzb(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmo;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmo;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zza(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmi;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmi;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zzg(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;

    return-void
.end method


# virtual methods
.method public final zza()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmi;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmi;

    return-object v0
.end method

.method public final zzb()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmo;

    return-object v0
.end method

.method public final zzc()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;

    return-object v0
.end method

.class public final Lcom/google/android/gms/internal/mlkit_common/zzli;
.super Ljava/lang/Object;
.source "com.google.mlkit:common@@18.8.0"


# annotations
.annotation runtime Lcom/google/firebase/encoders/annotations/Encodable;
.end annotation


# instance fields
.field private final zza:Lcom/google/android/gms/internal/mlkit_common/zzof;

.field private final zzb:Lcom/google/android/gms/internal/mlkit_common/zzlf;

.field private final zzc:Lcom/google/android/gms/internal/mlkit_common/zzlm;

.field private final zzd:Lcom/google/android/gms/internal/mlkit_common/zzkx;

.field private final zze:Lcom/google/android/gms/internal/mlkit_common/zzkg;

.field private final zzf:Lcom/google/android/gms/internal/mlkit_common/zzld;


# direct methods
.method synthetic constructor <init>(Lcom/google/android/gms/internal/mlkit_common/zzlg;Lcom/google/android/gms/internal/mlkit_common/zzlh;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zzm(Lcom/google/android/gms/internal/mlkit_common/zzlg;)Lcom/google/android/gms/internal/mlkit_common/zzof;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_common/zzli;->zza:Lcom/google/android/gms/internal/mlkit_common/zzof;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zzd(Lcom/google/android/gms/internal/mlkit_common/zzlg;)Lcom/google/android/gms/internal/mlkit_common/zzlf;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_common/zzli;->zzb:Lcom/google/android/gms/internal/mlkit_common/zzlf;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zzl(Lcom/google/android/gms/internal/mlkit_common/zzlg;)Lcom/google/android/gms/internal/mlkit_common/zzlm;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_common/zzli;->zzc:Lcom/google/android/gms/internal/mlkit_common/zzlm;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zzb(Lcom/google/android/gms/internal/mlkit_common/zzlg;)Lcom/google/android/gms/internal/mlkit_common/zzkx;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_common/zzli;->zzd:Lcom/google/android/gms/internal/mlkit_common/zzkx;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zza(Lcom/google/android/gms/internal/mlkit_common/zzlg;)Lcom/google/android/gms/internal/mlkit_common/zzkg;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_common/zzli;->zze:Lcom/google/android/gms/internal/mlkit_common/zzkg;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zzc(Lcom/google/android/gms/internal/mlkit_common/zzlg;)Lcom/google/android/gms/internal/mlkit_common/zzld;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzli;->zzf:Lcom/google/android/gms/internal/mlkit_common/zzld;

    return-void
.end method


# virtual methods
.method public final zza()Lcom/google/android/gms/internal/mlkit_common/zzkg;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_common/zzli;->zze:Lcom/google/android/gms/internal/mlkit_common/zzkg;

    return-object v0
.end method

.method public final zzb()Lcom/google/android/gms/internal/mlkit_common/zzkx;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_common/zzli;->zzd:Lcom/google/android/gms/internal/mlkit_common/zzkx;

    return-object v0
.end method

.method public final zzc()Lcom/google/android/gms/internal/mlkit_common/zzld;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_common/zzli;->zzf:Lcom/google/android/gms/internal/mlkit_common/zzld;

    return-object v0
.end method

.method public final zzd()Lcom/google/android/gms/internal/mlkit_common/zzlf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_common/zzli;->zzb:Lcom/google/android/gms/internal/mlkit_common/zzlf;

    return-object v0
.end method

.method public final zze()Lcom/google/android/gms/internal/mlkit_common/zzlm;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_common/zzli;->zzc:Lcom/google/android/gms/internal/mlkit_common/zzlm;

    return-object v0
.end method

.method public final zzf()Lcom/google/android/gms/internal/mlkit_common/zzof;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_common/zzli;->zza:Lcom/google/android/gms/internal/mlkit_common/zzof;

    return-object v0
.end method

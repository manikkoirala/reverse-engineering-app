.class public final Lcom/google/android/gms/internal/mlkit_common/zzqc;
.super Ljava/lang/Object;
.source "com.google.mlkit:common@@18.8.0"


# static fields
.field private static zza:Lcom/google/android/gms/internal/mlkit_common/zzqb;


# direct methods
.method public static declared-synchronized zza(Lcom/google/android/gms/internal/mlkit_common/zzpl;)Lcom/google/android/gms/internal/mlkit_common/zzpr;
    .locals 3

    const-class v0, Lcom/google/android/gms/internal/mlkit_common/zzqc;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/google/android/gms/internal/mlkit_common/zzqc;->zza:Lcom/google/android/gms/internal/mlkit_common/zzqb;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/gms/internal/mlkit_common/zzqb;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/mlkit_common/zzqb;-><init>(Lcom/google/android/gms/internal/mlkit_common/zzqa;)V

    sput-object v1, Lcom/google/android/gms/internal/mlkit_common/zzqc;->zza:Lcom/google/android/gms/internal/mlkit_common/zzqb;

    :cond_0
    sget-object v1, Lcom/google/android/gms/internal/mlkit_common/zzqc;->zza:Lcom/google/android/gms/internal/mlkit_common/zzqb;

    .line 2
    invoke-virtual {v1, p0}, Lcom/google/mlkit/common/sdkinternal/LazyInstanceMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/internal/mlkit_common/zzpr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized zzb(Ljava/lang/String;)Lcom/google/android/gms/internal/mlkit_common/zzpr;
    .locals 1

    const-class p0, Lcom/google/android/gms/internal/mlkit_common/zzqc;

    monitor-enter p0

    :try_start_0
    const-string v0, "common"

    .line 1
    invoke-static {v0}, Lcom/google/android/gms/internal/mlkit_common/zzpl;->zzd(Ljava/lang/String;)Lcom/google/android/gms/internal/mlkit_common/zzpk;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/google/android/gms/internal/mlkit_common/zzpk;->zzd()Lcom/google/android/gms/internal/mlkit_common/zzpl;

    move-result-object v0

    .line 3
    invoke-static {v0}, Lcom/google/android/gms/internal/mlkit_common/zzqc;->zza(Lcom/google/android/gms/internal/mlkit_common/zzpl;)Lcom/google/android/gms/internal/mlkit_common/zzpr;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

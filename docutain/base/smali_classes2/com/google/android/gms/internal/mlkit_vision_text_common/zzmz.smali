.class public final Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"


# annotations
.annotation runtime Lcom/google/firebase/encoders/annotations/Encodable;
.end annotation


# instance fields
.field private final zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpu;

.field private final zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

.field private final zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

.field private final zzd:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;

.field private final zze:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpd;

.field private final zzf:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;


# direct methods
.method synthetic constructor <init>(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzm(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpu;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpu;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzc(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzb(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzl(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;->zzd:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzk(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpd;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;->zze:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpd;

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zza(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;->zzf:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;

    return-void
.end method


# virtual methods
.method public final zza()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;->zzf:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;

    return-object v0
.end method

.method public final zzb()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

    return-object v0
.end method

.method public final zzc()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

    return-object v0
.end method

.method public final zzd()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;->zze:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpd;

    return-object v0
.end method

.method public final zze()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;->zzd:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;

    return-object v0
.end method

.method public final zzf()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmz;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpu;

    return-object v0
.end method

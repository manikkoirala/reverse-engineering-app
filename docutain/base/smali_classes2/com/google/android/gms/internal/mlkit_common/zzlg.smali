.class public final Lcom/google/android/gms/internal/mlkit_common/zzlg;
.super Ljava/lang/Object;
.source "com.google.mlkit:common@@18.8.0"


# instance fields
.field private zza:Lcom/google/android/gms/internal/mlkit_common/zzof;

.field private zzb:Lcom/google/android/gms/internal/mlkit_common/zzlf;

.field private zzc:Lcom/google/android/gms/internal/mlkit_common/zzlm;

.field private zzd:Lcom/google/android/gms/internal/mlkit_common/zzkx;

.field private zze:Lcom/google/android/gms/internal/mlkit_common/zzkg;

.field private zzf:Lcom/google/android/gms/internal/mlkit_common/zzld;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static bridge synthetic zza(Lcom/google/android/gms/internal/mlkit_common/zzlg;)Lcom/google/android/gms/internal/mlkit_common/zzkg;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zze:Lcom/google/android/gms/internal/mlkit_common/zzkg;

    return-object p0
.end method

.method static bridge synthetic zzb(Lcom/google/android/gms/internal/mlkit_common/zzlg;)Lcom/google/android/gms/internal/mlkit_common/zzkx;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zzd:Lcom/google/android/gms/internal/mlkit_common/zzkx;

    return-object p0
.end method

.method static bridge synthetic zzc(Lcom/google/android/gms/internal/mlkit_common/zzlg;)Lcom/google/android/gms/internal/mlkit_common/zzld;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zzf:Lcom/google/android/gms/internal/mlkit_common/zzld;

    return-object p0
.end method

.method static bridge synthetic zzd(Lcom/google/android/gms/internal/mlkit_common/zzlg;)Lcom/google/android/gms/internal/mlkit_common/zzlf;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zzb:Lcom/google/android/gms/internal/mlkit_common/zzlf;

    return-object p0
.end method

.method static bridge synthetic zzl(Lcom/google/android/gms/internal/mlkit_common/zzlg;)Lcom/google/android/gms/internal/mlkit_common/zzlm;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zzc:Lcom/google/android/gms/internal/mlkit_common/zzlm;

    return-object p0
.end method

.method static bridge synthetic zzm(Lcom/google/android/gms/internal/mlkit_common/zzlg;)Lcom/google/android/gms/internal/mlkit_common/zzof;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zza:Lcom/google/android/gms/internal/mlkit_common/zzof;

    return-object p0
.end method


# virtual methods
.method public final zze(Lcom/google/android/gms/internal/mlkit_common/zzkg;)Lcom/google/android/gms/internal/mlkit_common/zzlg;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zze:Lcom/google/android/gms/internal/mlkit_common/zzkg;

    return-object p0
.end method

.method public final zzf(Lcom/google/android/gms/internal/mlkit_common/zzlf;)Lcom/google/android/gms/internal/mlkit_common/zzlg;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zzb:Lcom/google/android/gms/internal/mlkit_common/zzlf;

    return-object p0
.end method

.method public final zzg(Lcom/google/android/gms/internal/mlkit_common/zzkx;)Lcom/google/android/gms/internal/mlkit_common/zzlg;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zzd:Lcom/google/android/gms/internal/mlkit_common/zzkx;

    return-object p0
.end method

.method public final zzh(Lcom/google/android/gms/internal/mlkit_common/zzld;)Lcom/google/android/gms/internal/mlkit_common/zzlg;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zzf:Lcom/google/android/gms/internal/mlkit_common/zzld;

    return-object p0
.end method

.method public final zzi(Lcom/google/android/gms/internal/mlkit_common/zzlm;)Lcom/google/android/gms/internal/mlkit_common/zzlg;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zzc:Lcom/google/android/gms/internal/mlkit_common/zzlm;

    return-object p0
.end method

.method public final zzj(Lcom/google/android/gms/internal/mlkit_common/zzof;)Lcom/google/android/gms/internal/mlkit_common/zzlg;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzlg;->zza:Lcom/google/android/gms/internal/mlkit_common/zzof;

    return-object p0
.end method

.method public final zzk()Lcom/google/android/gms/internal/mlkit_common/zzli;
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/mlkit_common/zzli;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/mlkit_common/zzli;-><init>(Lcom/google/android/gms/internal/mlkit_common/zzlg;Lcom/google/android/gms/internal/mlkit_common/zzlh;)V

    return-object v0
.end method

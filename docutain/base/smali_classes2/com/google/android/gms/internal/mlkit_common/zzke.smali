.class public final Lcom/google/android/gms/internal/mlkit_common/zzke;
.super Ljava/lang/Object;
.source "com.google.mlkit:common@@18.8.0"


# instance fields
.field private zza:Lcom/google/android/gms/internal/mlkit_common/zzlo;

.field private zzb:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static bridge synthetic zzd(Lcom/google/android/gms/internal/mlkit_common/zzke;)Lcom/google/android/gms/internal/mlkit_common/zzlo;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_common/zzke;->zza:Lcom/google/android/gms/internal/mlkit_common/zzlo;

    return-object p0
.end method

.method static bridge synthetic zze(Lcom/google/android/gms/internal/mlkit_common/zzke;)Ljava/lang/Boolean;
    .locals 0

    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_common/zzke;->zzb:Ljava/lang/Boolean;

    return-object p0
.end method


# virtual methods
.method public final zza(Ljava/lang/Boolean;)Lcom/google/android/gms/internal/mlkit_common/zzke;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzke;->zzb:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final zzb(Lcom/google/android/gms/internal/mlkit_common/zzlo;)Lcom/google/android/gms/internal/mlkit_common/zzke;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_common/zzke;->zza:Lcom/google/android/gms/internal/mlkit_common/zzlo;

    return-object p0
.end method

.method public final zzc()Lcom/google/android/gms/internal/mlkit_common/zzkg;
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/mlkit_common/zzkg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/mlkit_common/zzkg;-><init>(Lcom/google/android/gms/internal/mlkit_common/zzke;Lcom/google/android/gms/internal/mlkit_common/zzkf;)V

    return-object v0
.end method

.class public final Lcom/google/android/material/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/material/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f120000

.field public static final abc_action_bar_up_description:I = 0x7f120001

.field public static final abc_action_menu_overflow_description:I = 0x7f120002

.field public static final abc_action_mode_done:I = 0x7f120003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f120004

.field public static final abc_activitychooserview_choose_application:I = 0x7f120005

.field public static final abc_capital_off:I = 0x7f120006

.field public static final abc_capital_on:I = 0x7f120007

.field public static final abc_menu_alt_shortcut_label:I = 0x7f120008

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f120009

.field public static final abc_menu_delete_shortcut_label:I = 0x7f12000a

.field public static final abc_menu_enter_shortcut_label:I = 0x7f12000b

.field public static final abc_menu_function_shortcut_label:I = 0x7f12000c

.field public static final abc_menu_meta_shortcut_label:I = 0x7f12000d

.field public static final abc_menu_shift_shortcut_label:I = 0x7f12000e

.field public static final abc_menu_space_shortcut_label:I = 0x7f12000f

.field public static final abc_menu_sym_shortcut_label:I = 0x7f120010

.field public static final abc_prepend_shortcut_label:I = 0x7f120011

.field public static final abc_search_hint:I = 0x7f120012

.field public static final abc_searchview_description_clear:I = 0x7f120013

.field public static final abc_searchview_description_query:I = 0x7f120014

.field public static final abc_searchview_description_search:I = 0x7f120015

.field public static final abc_searchview_description_submit:I = 0x7f120016

.field public static final abc_searchview_description_voice:I = 0x7f120017

.field public static final abc_shareactionprovider_share_with:I = 0x7f120018

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f120019

.field public static final abc_toolbar_collapse_description:I = 0x7f12001a

.field public static final appbar_scrolling_view_behavior:I = 0x7f12001d

.field public static final bottom_sheet_behavior:I = 0x7f12001e

.field public static final bottomsheet_action_collapse:I = 0x7f12001f

.field public static final bottomsheet_action_expand:I = 0x7f120020

.field public static final bottomsheet_action_expand_halfway:I = 0x7f120021

.field public static final bottomsheet_drag_handle_clicked:I = 0x7f120022

.field public static final bottomsheet_drag_handle_content_description:I = 0x7f120023

.field public static final character_counter_content_description:I = 0x7f12002b

.field public static final character_counter_overflowed_content_description:I = 0x7f12002c

.field public static final character_counter_pattern:I = 0x7f12002d

.field public static final clear_text_end_icon_content_description:I = 0x7f12002e

.field public static final error_a11y_label:I = 0x7f120046

.field public static final error_icon_content_description:I = 0x7f120047

.field public static final exposed_dropdown_menu_content_description:I = 0x7f120049

.field public static final fab_transformation_scrim_behavior:I = 0x7f12004a

.field public static final fab_transformation_sheet_behavior:I = 0x7f12004b

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f120062

.field public static final icon_content_description:I = 0x7f120063

.field public static final item_view_role_description:I = 0x7f120065

.field public static final m3_ref_typeface_brand_medium:I = 0x7f120066

.field public static final m3_ref_typeface_brand_regular:I = 0x7f120067

.field public static final m3_ref_typeface_plain_medium:I = 0x7f120068

.field public static final m3_ref_typeface_plain_regular:I = 0x7f120069

.field public static final m3_sys_motion_easing_emphasized:I = 0x7f12006a

.field public static final m3_sys_motion_easing_emphasized_accelerate:I = 0x7f12006b

.field public static final m3_sys_motion_easing_emphasized_decelerate:I = 0x7f12006c

.field public static final m3_sys_motion_easing_emphasized_path_data:I = 0x7f12006d

.field public static final m3_sys_motion_easing_legacy:I = 0x7f12006e

.field public static final m3_sys_motion_easing_legacy_accelerate:I = 0x7f12006f

.field public static final m3_sys_motion_easing_legacy_decelerate:I = 0x7f120070

.field public static final m3_sys_motion_easing_linear:I = 0x7f120071

.field public static final m3_sys_motion_easing_standard:I = 0x7f120072

.field public static final m3_sys_motion_easing_standard_accelerate:I = 0x7f120073

.field public static final m3_sys_motion_easing_standard_decelerate:I = 0x7f120074

.field public static final material_clock_display_divider:I = 0x7f120075

.field public static final material_clock_toggle_content_description:I = 0x7f120076

.field public static final material_hour_24h_suffix:I = 0x7f120077

.field public static final material_hour_selection:I = 0x7f120078

.field public static final material_hour_suffix:I = 0x7f120079

.field public static final material_minute_selection:I = 0x7f12007a

.field public static final material_minute_suffix:I = 0x7f12007b

.field public static final material_motion_easing_accelerated:I = 0x7f12007c

.field public static final material_motion_easing_decelerated:I = 0x7f12007d

.field public static final material_motion_easing_emphasized:I = 0x7f12007e

.field public static final material_motion_easing_linear:I = 0x7f12007f

.field public static final material_motion_easing_standard:I = 0x7f120080

.field public static final material_slider_range_end:I = 0x7f120081

.field public static final material_slider_range_start:I = 0x7f120082

.field public static final material_slider_value:I = 0x7f120083

.field public static final material_timepicker_am:I = 0x7f120084

.field public static final material_timepicker_clock_mode_description:I = 0x7f120085

.field public static final material_timepicker_hour:I = 0x7f120086

.field public static final material_timepicker_minute:I = 0x7f120087

.field public static final material_timepicker_pm:I = 0x7f120088

.field public static final material_timepicker_select_time:I = 0x7f120089

.field public static final material_timepicker_text_input_mode_description:I = 0x7f12008a

.field public static final mtrl_badge_numberless_content_description:I = 0x7f12008b

.field public static final mtrl_checkbox_button_icon_path_checked:I = 0x7f12008c

.field public static final mtrl_checkbox_button_icon_path_group_name:I = 0x7f12008d

.field public static final mtrl_checkbox_button_icon_path_indeterminate:I = 0x7f12008e

.field public static final mtrl_checkbox_button_icon_path_name:I = 0x7f12008f

.field public static final mtrl_checkbox_button_path_checked:I = 0x7f120090

.field public static final mtrl_checkbox_button_path_group_name:I = 0x7f120091

.field public static final mtrl_checkbox_button_path_name:I = 0x7f120092

.field public static final mtrl_checkbox_button_path_unchecked:I = 0x7f120093

.field public static final mtrl_checkbox_state_description_checked:I = 0x7f120094

.field public static final mtrl_checkbox_state_description_indeterminate:I = 0x7f120095

.field public static final mtrl_checkbox_state_description_unchecked:I = 0x7f120096

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f120097

.field public static final mtrl_exceed_max_badge_number_content_description:I = 0x7f120098

.field public static final mtrl_exceed_max_badge_number_suffix:I = 0x7f120099

.field public static final mtrl_picker_a11y_next_month:I = 0x7f12009a

.field public static final mtrl_picker_a11y_prev_month:I = 0x7f12009b

.field public static final mtrl_picker_announce_current_range_selection:I = 0x7f12009c

.field public static final mtrl_picker_announce_current_selection:I = 0x7f12009d

.field public static final mtrl_picker_announce_current_selection_none:I = 0x7f12009e

.field public static final mtrl_picker_cancel:I = 0x7f12009f

.field public static final mtrl_picker_confirm:I = 0x7f1200a0

.field public static final mtrl_picker_date_header_selected:I = 0x7f1200a1

.field public static final mtrl_picker_date_header_title:I = 0x7f1200a2

.field public static final mtrl_picker_date_header_unselected:I = 0x7f1200a3

.field public static final mtrl_picker_day_of_week_column_header:I = 0x7f1200a4

.field public static final mtrl_picker_end_date_description:I = 0x7f1200a5

.field public static final mtrl_picker_invalid_format:I = 0x7f1200a6

.field public static final mtrl_picker_invalid_format_example:I = 0x7f1200a7

.field public static final mtrl_picker_invalid_format_use:I = 0x7f1200a8

.field public static final mtrl_picker_invalid_range:I = 0x7f1200a9

.field public static final mtrl_picker_navigate_to_current_year_description:I = 0x7f1200aa

.field public static final mtrl_picker_navigate_to_year_description:I = 0x7f1200ab

.field public static final mtrl_picker_out_of_range:I = 0x7f1200ac

.field public static final mtrl_picker_range_header_only_end_selected:I = 0x7f1200ad

.field public static final mtrl_picker_range_header_only_start_selected:I = 0x7f1200ae

.field public static final mtrl_picker_range_header_selected:I = 0x7f1200af

.field public static final mtrl_picker_range_header_title:I = 0x7f1200b0

.field public static final mtrl_picker_range_header_unselected:I = 0x7f1200b1

.field public static final mtrl_picker_save:I = 0x7f1200b2

.field public static final mtrl_picker_start_date_description:I = 0x7f1200b3

.field public static final mtrl_picker_text_input_date_hint:I = 0x7f1200b5

.field public static final mtrl_picker_text_input_date_range_end_hint:I = 0x7f1200b6

.field public static final mtrl_picker_text_input_date_range_start_hint:I = 0x7f1200b7

.field public static final mtrl_picker_text_input_day_abbr:I = 0x7f1200b8

.field public static final mtrl_picker_text_input_month_abbr:I = 0x7f1200b9

.field public static final mtrl_picker_text_input_year_abbr:I = 0x7f1200ba

.field public static final mtrl_picker_today_description:I = 0x7f1200bb

.field public static final mtrl_picker_toggle_to_calendar_input_mode:I = 0x7f1200bc

.field public static final mtrl_picker_toggle_to_day_selection:I = 0x7f1200bd

.field public static final mtrl_picker_toggle_to_text_input_mode:I = 0x7f1200be

.field public static final mtrl_picker_toggle_to_year_selection:I = 0x7f1200bf

.field public static final mtrl_switch_thumb_group_name:I = 0x7f1200c0

.field public static final mtrl_switch_thumb_path_checked:I = 0x7f1200c1

.field public static final mtrl_switch_thumb_path_morphing:I = 0x7f1200c2

.field public static final mtrl_switch_thumb_path_name:I = 0x7f1200c3

.field public static final mtrl_switch_thumb_path_pressed:I = 0x7f1200c4

.field public static final mtrl_switch_thumb_path_unchecked:I = 0x7f1200c5

.field public static final mtrl_switch_track_decoration_path:I = 0x7f1200c6

.field public static final mtrl_switch_track_path:I = 0x7f1200c7

.field public static final mtrl_timepicker_cancel:I = 0x7f1200c8

.field public static final mtrl_timepicker_confirm:I = 0x7f1200c9

.field public static final password_toggle_content_description:I = 0x7f1200ce

.field public static final path_password_eye:I = 0x7f1200cf

.field public static final path_password_eye_mask_strike_through:I = 0x7f1200d0

.field public static final path_password_eye_mask_visible:I = 0x7f1200d1

.field public static final path_password_strike_through:I = 0x7f1200d2

.field public static final search_menu_title:I = 0x7f120271

.field public static final searchbar_scrolling_view_behavior:I = 0x7f120272

.field public static final searchview_clear_text_content_description:I = 0x7f120273

.field public static final searchview_navigation_content_description:I = 0x7f120274

.field public static final side_sheet_accessibility_pane_title:I = 0x7f120275

.field public static final side_sheet_behavior:I = 0x7f120276

.field public static final status_bar_notification_info_overflow:I = 0x7f120277


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

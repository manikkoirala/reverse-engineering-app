.class public Lcom/google/mlkit/vision/text/internal/TextRecognizerImpl;
.super Lcom/google/mlkit/vision/common/internal/MobileVisionBase;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"

# interfaces
.implements Lcom/google/mlkit/vision/text/TextRecognizer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/mlkit/vision/common/internal/MobileVisionBase<",
        "Lcom/google/mlkit/vision/text/Text;",
        ">;",
        "Lcom/google/mlkit/vision/text/TextRecognizer;"
    }
.end annotation


# instance fields
.field private final zzb:Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;


# direct methods
.method constructor <init>(Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;Ljava/util/concurrent/Executor;Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/mlkit/vision/common/internal/MobileVisionBase;-><init>(Lcom/google/mlkit/common/sdkinternal/MLTask;Ljava/util/concurrent/Executor;)V

    iput-object p4, p0, Lcom/google/mlkit/vision/text/internal/TextRecognizerImpl;->zzb:Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;

    new-instance p1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;

    invoke-direct {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;-><init>()V

    .line 2
    invoke-interface {p4}, Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;->getIsThickClient()Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

    .line 3
    :goto_0
    invoke-virtual {p1, p2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zze(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;

    new-instance p2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;

    invoke-direct {p2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;

    invoke-direct {v0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;-><init>()V

    .line 4
    invoke-interface {p4}, Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;->getLoggingLanguageOption()I

    move-result p4

    .line 5
    invoke-static {p4}, Lcom/google/mlkit/vision/text/internal/LoggingUtils;->zza(I)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;

    move-result-object p4

    .line 6
    invoke-virtual {v0, p4}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;->zza(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;->zzc()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;

    move-result-object p4

    .line 7
    invoke-virtual {p2, p4}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zze(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;

    invoke-virtual {p2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zzf()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;

    move-result-object p2

    .line 8
    invoke-virtual {p1, p2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzh(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;

    const/4 p2, 0x1

    .line 9
    invoke-static {p1, p2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrg;->zzg(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;I)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqs;

    move-result-object p1

    sget-object p2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;->zzg:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

    .line 10
    invoke-virtual {p3, p1, p2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;->zzd(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqs;Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;)V

    return-void
.end method


# virtual methods
.method public final getDetectorType()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public final getOptionalFeatures()[Lcom/google/android/gms/common/Feature;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/mlkit/vision/text/internal/TextRecognizerImpl;->zzb:Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;

    invoke-static {v0}, Lcom/google/mlkit/vision/text/internal/TextOptionalModuleUtils;->zza(Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;)[Lcom/google/android/gms/common/Feature;

    move-result-object v0

    return-object v0
.end method

.method public final process(Lcom/google/android/odml/image/MlImage;)Lcom/google/android/gms/tasks/Task;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/odml/image/MlImage;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Lcom/google/mlkit/vision/text/Text;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-super {p0, p1}, Lcom/google/mlkit/vision/common/internal/MobileVisionBase;->processBase(Lcom/google/android/odml/image/MlImage;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final process(Lcom/google/mlkit/vision/common/InputImage;)Lcom/google/android/gms/tasks/Task;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/mlkit/vision/common/InputImage;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Lcom/google/mlkit/vision/text/Text;",
            ">;"
        }
    .end annotation

    .line 2
    invoke-super {p0, p1}, Lcom/google/mlkit/vision/common/internal/MobileVisionBase;->processBase(Lcom/google/mlkit/vision/common/InputImage;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.class public Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;
.super Lcom/google/mlkit/common/sdkinternal/MLTask;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/mlkit/common/sdkinternal/MLTask<",
        "Lcom/google/mlkit/vision/text/Text;",
        "Lcom/google/mlkit/vision/common/InputImage;",
        ">;"
    }
.end annotation


# static fields
.field private static final taskQueue:Lcom/google/mlkit/common/sdkinternal/TaskQueue;

.field static zza:Z = true

.field private static final zzb:Lcom/google/mlkit/vision/common/internal/ImageUtils;


# instance fields
.field private final zzc:Lcom/google/mlkit/vision/text/internal/zzl;

.field private final zzd:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;

.field private final zze:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrf;

.field private final zzf:Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/mlkit/vision/common/internal/ImageUtils;->getInstance()Lcom/google/mlkit/vision/common/internal/ImageUtils;

    move-result-object v0

    sput-object v0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzb:Lcom/google/mlkit/vision/common/internal/ImageUtils;

    .line 2
    new-instance v0, Lcom/google/mlkit/common/sdkinternal/TaskQueue;

    invoke-direct {v0}, Lcom/google/mlkit/common/sdkinternal/TaskQueue;-><init>()V

    sput-object v0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->taskQueue:Lcom/google/mlkit/common/sdkinternal/TaskQueue;

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;Lcom/google/mlkit/vision/text/internal/zzl;Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->taskQueue:Lcom/google/mlkit/common/sdkinternal/TaskQueue;

    invoke-direct {p0, v0}, Lcom/google/mlkit/common/sdkinternal/MLTask;-><init>(Lcom/google/mlkit/common/sdkinternal/TaskQueue;)V

    iput-object p1, p0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzd:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;

    iput-object p2, p0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzc:Lcom/google/mlkit/vision/text/internal/zzl;

    .line 2
    invoke-static {}, Lcom/google/mlkit/common/sdkinternal/MlKitContext;->getInstance()Lcom/google/mlkit/common/sdkinternal/MlKitContext;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/mlkit/common/sdkinternal/MlKitContext;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrf;->zza(Landroid/content/Context;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrf;

    move-result-object p1

    iput-object p1, p0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zze:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrf;

    iput-object p3, p0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzf:Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;

    return-void
.end method

.method private final zzf(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;JLcom/google/mlkit/vision/common/InputImage;)V
    .locals 23

    move-object/from16 v6, p0

    .line 1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long v14, v0, p2

    new-instance v7, Lcom/google/mlkit/vision/text/internal/zzo;

    move-object v0, v7

    move-object/from16 v1, p0

    move-wide v2, v14

    move-object/from16 v4, p1

    move-object/from16 v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/mlkit/vision/text/internal/zzo;-><init>(Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;JLcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;Lcom/google/mlkit/vision/common/InputImage;)V

    iget-object v0, v6, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzd:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;

    .line 2
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;->zzf:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

    invoke-virtual {v0, v7, v1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;->zzf(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrc;Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;)V

    new-instance v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;

    invoke-direct {v0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;-><init>()V

    move-object/from16 v1, p1

    .line 3
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;->zza(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;

    sget-boolean v2, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zza:Z

    .line 4
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;->zzb(Ljava/lang/Boolean;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;

    new-instance v2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;

    invoke-direct {v2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;-><init>()V

    iget-object v3, v6, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzf:Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;

    .line 5
    invoke-interface {v3}, Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;->getLoggingLanguageOption()I

    move-result v3

    invoke-static {v3}, Lcom/google/mlkit/vision/text/internal/LoggingUtils;->zza(I)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;

    move-result-object v3

    .line 6
    invoke-virtual {v2, v3}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;->zza(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;->zzc()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;

    move-result-object v2

    .line 7
    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;->zzc(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzen;->zzd()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzep;

    move-result-object v10

    new-instance v13, Lcom/google/mlkit/vision/text/internal/zzp;

    invoke-direct {v13, v6}, Lcom/google/mlkit/vision/text/internal/zzp;-><init>(Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;)V

    iget-object v8, v6, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzd:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;

    sget-object v9, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;->zzbi:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

    .line 8
    invoke-static {}, Lcom/google/mlkit/common/sdkinternal/MLTaskExecutor;->workerThreadExecutor()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzra;

    move-object v7, v2

    move-wide v11, v14

    invoke-direct/range {v7 .. v13}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzra;-><init>(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;Ljava/lang/Object;JLcom/google/mlkit/vision/text/internal/zzp;)V

    .line 9
    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    iget-object v0, v6, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zze:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrf;

    iget-object v2, v6, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzf:Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;

    .line 11
    invoke-interface {v2}, Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;->getLoggingEventId()I

    move-result v17

    sub-long v19, v21, v14

    .line 12
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;->zza()I

    move-result v18

    move-object/from16 v16, v0

    .line 13
    invoke-virtual/range {v16 .. v22}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrf;->zzc(IIJJ)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized load()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/mlkit/common/MlKitException;
        }
    .end annotation

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzc:Lcom/google/mlkit/vision/text/internal/zzl;

    invoke-interface {v0}, Lcom/google/mlkit/vision/text/internal/zzl;->zzb()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized release()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    .line 1
    :try_start_0
    sput-boolean v0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zza:Z

    iget-object v0, p0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzc:Lcom/google/mlkit/vision/text/internal/zzl;

    invoke-interface {v0}, Lcom/google/mlkit/vision/text/internal/zzl;->zzc()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final bridge synthetic run(Lcom/google/mlkit/common/sdkinternal/MLTaskInput;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/mlkit/common/MlKitException;
        }
    .end annotation

    .line 1
    check-cast p1, Lcom/google/mlkit/vision/common/InputImage;

    invoke-virtual {p0, p1}, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zze(Lcom/google/mlkit/vision/common/InputImage;)Lcom/google/mlkit/vision/text/Text;

    move-result-object p1

    return-object p1
.end method

.method final synthetic zzc(JLcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;Lcom/google/mlkit/vision/common/InputImage;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqs;
    .locals 2

    .line 1
    new-instance v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;

    invoke-direct {v0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;-><init>()V

    new-instance v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmm;

    invoke-direct {v1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmm;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmm;->zzc(Ljava/lang/Long;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmm;

    .line 2
    invoke-virtual {v1, p3}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmm;->zzd(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmm;

    sget-boolean p1, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zza:Z

    .line 3
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmm;->zze(Ljava/lang/Boolean;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmm;

    const/4 p1, 0x1

    .line 4
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmm;->zza(Ljava/lang/Boolean;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmm;

    .line 5
    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmm;->zzb(Ljava/lang/Boolean;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmm;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmm;->zzf()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmo;

    move-result-object p1

    .line 6
    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zzd(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmo;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;

    sget-object p1, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzb:Lcom/google/mlkit/vision/common/internal/ImageUtils;

    .line 7
    invoke-virtual {p1, p4}, Lcom/google/mlkit/vision/common/internal/ImageUtils;->getMobileVisionImageFormat(Lcom/google/mlkit/vision/common/InputImage;)I

    move-result p2

    .line 8
    invoke-virtual {p1, p4}, Lcom/google/mlkit/vision/common/internal/ImageUtils;->getMobileVisionImageSize(Lcom/google/mlkit/vision/common/InputImage;)I

    move-result p1

    new-instance p3, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmf;

    invoke-direct {p3}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmf;-><init>()V

    const/4 p4, -0x1

    if-eq p2, p4, :cond_4

    const/16 p4, 0x23

    if-eq p2, p4, :cond_3

    const p4, 0x32315659

    if-eq p2, p4, :cond_2

    const/16 p4, 0x10

    if-eq p2, p4, :cond_1

    const/16 p4, 0x11

    if-eq p2, p4, :cond_0

    .line 14
    sget-object p2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmg;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmg;

    goto :goto_0

    .line 11
    :cond_0
    sget-object p2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmg;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmg;

    goto :goto_0

    .line 12
    :cond_1
    sget-object p2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmg;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmg;

    goto :goto_0

    .line 9
    :cond_2
    sget-object p2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmg;->zzd:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmg;

    goto :goto_0

    .line 10
    :cond_3
    sget-object p2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmg;->zze:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmg;

    goto :goto_0

    .line 13
    :cond_4
    sget-object p2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmg;->zzg:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmg;

    .line 15
    :goto_0
    invoke-virtual {p3, p2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmf;->zza(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmg;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmf;

    .line 16
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmf;->zzb(Ljava/lang/Integer;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmf;

    invoke-virtual {p3}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmf;->zzd()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmi;

    move-result-object p1

    .line 17
    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zzc(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmi;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;

    new-instance p1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;

    invoke-direct {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;-><init>()V

    iget-object p2, p0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzf:Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;

    .line 18
    invoke-interface {p2}, Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;->getLoggingLanguageOption()I

    move-result p2

    invoke-static {p2}, Lcom/google/mlkit/vision/text/internal/LoggingUtils;->zza(I)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;

    move-result-object p2

    .line 19
    invoke-virtual {p1, p2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;->zza(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;

    invoke-virtual {p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzph;->zzc()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;

    move-result-object p1

    .line 20
    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zze(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpk;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpe;->zzf()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;

    move-result-object p1

    new-instance p2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;

    invoke-direct {p2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;-><init>()V

    iget-object p3, p0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzf:Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;

    .line 21
    invoke-interface {p3}, Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;->getIsThickClient()Z

    move-result p3

    if-eqz p3, :cond_5

    .line 22
    sget-object p3, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

    goto :goto_1

    .line 23
    :cond_5
    sget-object p3, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

    .line 24
    :goto_1
    invoke-virtual {p2, p3}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zze(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;

    .line 25
    invoke-virtual {p2, p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzh(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpg;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;

    .line 26
    invoke-static {p2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrg;->zzf(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqs;

    move-result-object p1

    return-object p1
.end method

.method final synthetic zzd(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzep;ILcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqs;
    .locals 2

    .line 1
    new-instance v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;

    invoke-direct {v0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;-><init>()V

    iget-object v1, p0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzf:Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;

    invoke-interface {v1}, Lcom/google/mlkit/vision/text/TextRecognizerOptionsInterface;->getIsThickClient()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

    goto :goto_0

    .line 3
    :cond_0
    sget-object v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

    .line 4
    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zze(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;

    new-instance v1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;

    invoke-direct {v1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;-><init>()V

    .line 5
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;->zza(Ljava/lang/Integer;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;

    .line 6
    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;->zzc(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzep;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;

    .line 7
    invoke-virtual {v1, p3}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;->zzb(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmb;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzem;->zze()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;

    move-result-object p1

    .line 8
    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzd(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzer;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;

    .line 9
    invoke-static {v0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrg;->zzf(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqs;

    move-result-object p1

    return-object p1
.end method

.method public final declared-synchronized zze(Lcom/google/mlkit/vision/common/InputImage;)Lcom/google/mlkit/vision/text/Text;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/mlkit/common/MlKitException;
        }
    .end annotation

    monitor-enter p0

    .line 1
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v2, p0, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzc:Lcom/google/mlkit/vision/text/internal/zzl;

    .line 2
    invoke-interface {v2, p1}, Lcom/google/mlkit/vision/text/internal/zzl;->zza(Lcom/google/mlkit/vision/common/InputImage;)Lcom/google/mlkit/vision/text/Text;

    move-result-object v2

    .line 3
    sget-object v3, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;

    invoke-direct {p0, v3, v0, v1, p1}, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzf(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;JLcom/google/mlkit/vision/common/InputImage;)V

    const/4 v3, 0x0

    sput-boolean v3, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zza:Z
    :try_end_1
    .catch Lcom/google/mlkit/common/MlKitException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v2

    :catch_0
    move-exception v2

    .line 4
    :try_start_2
    invoke-virtual {v2}, Lcom/google/mlkit/common/MlKitException;->getErrorCode()I

    move-result v3

    const/16 v4, 0xe

    if-ne v3, v4, :cond_0

    .line 5
    sget-object v3, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;->zzk:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;

    goto :goto_0

    .line 6
    :cond_0
    sget-object v3, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;->zzV:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;

    .line 7
    :goto_0
    invoke-direct {p0, v3, v0, v1, p1}, Lcom/google/mlkit/vision/text/internal/TextRecognizerTaskWithResource;->zzf(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;JLcom/google/mlkit/vision/common/InputImage;)V

    .line 8
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

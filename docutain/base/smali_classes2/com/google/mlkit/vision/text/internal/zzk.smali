.class public final synthetic Lcom/google/mlkit/vision/text/internal/zzk;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"

# interfaces
.implements Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrc;


# instance fields
.field public final synthetic zza:Z

.field public final synthetic zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;


# direct methods
.method public synthetic constructor <init>(ZLcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/mlkit/vision/text/internal/zzk;->zza:Z

    iput-object p2, p0, Lcom/google/mlkit/vision/text/internal/zzk;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;

    return-void
.end method


# virtual methods
.method public final zza()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqs;
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/google/mlkit/vision/text/internal/zzk;->zza:Z

    iget-object v1, p0, Lcom/google/mlkit/vision/text/internal/zzk;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;

    new-instance v2, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;

    invoke-direct {v2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;-><init>()V

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zze(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmu;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;

    new-instance v0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpb;

    invoke-direct {v0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpb;-><init>()V

    .line 2
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpb;->zzb(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpb;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpb;->zzc()Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpd;

    move-result-object v0

    .line 3
    invoke-virtual {v2, v0}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;->zzg(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpd;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;

    .line 4
    invoke-static {v2}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrg;->zzf(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmx;)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzqs;

    move-result-object v0

    return-object v0
.end method

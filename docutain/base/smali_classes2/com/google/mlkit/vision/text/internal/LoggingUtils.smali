.class public final Lcom/google/mlkit/vision/text/internal/LoggingUtils;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-mlkit-text-recognition-common@@19.0.0"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static zza(I)Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;
    .locals 0

    packed-switch p0, :pswitch_data_0

    .line 8
    sget-object p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;->zza:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;

    return-object p0

    .line 1
    :pswitch_0
    sget-object p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;->zzh:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;

    return-object p0

    .line 2
    :pswitch_1
    sget-object p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;->zzg:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;

    return-object p0

    .line 3
    :pswitch_2
    sget-object p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;->zzf:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;

    return-object p0

    .line 4
    :pswitch_3
    sget-object p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;->zze:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;

    return-object p0

    .line 5
    :pswitch_4
    sget-object p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;->zzd:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;

    return-object p0

    .line 6
    :pswitch_5
    sget-object p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;->zzc:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;

    return-object p0

    .line 7
    :pswitch_6
    sget-object p0, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;->zzb:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzpi;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static zzb(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;ZLcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/google/mlkit/vision/text/internal/zzk;

    invoke-direct {v0, p1, p2}, Lcom/google/mlkit/vision/text/internal/zzk;-><init>(ZLcom/google/android/gms/internal/mlkit_vision_text_common/zzmv;)V

    sget-object p1, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;->zzi:Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrd;->zzf(Lcom/google/android/gms/internal/mlkit_vision_text_common/zzrc;Lcom/google/android/gms/internal/mlkit_vision_text_common/zzmw;)V

    return-void
.end method

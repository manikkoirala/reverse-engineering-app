.class final Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;
.super Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbk;
.source "com.google.mlkit:barcode-scanning@@17.1.0"


# static fields
.field private static final zza:[I

.field private static final zzb:[[D


# instance fields
.field private final zzc:Landroid/content/Context;

.field private final zzd:Lcom/google/android/libraries/barhopper/RecognitionOptions;

.field private zze:Lcom/google/android/libraries/barhopper/BarhopperV3;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x6

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    sput-object v1, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zza:[I

    const/16 v1, 0x24

    new-array v1, v1, [[D

    const/4 v2, 0x2

    new-array v3, v2, [D

    fill-array-data v3, :array_1

    const/4 v4, 0x0

    aput-object v3, v1, v4

    new-array v3, v2, [D

    fill-array-data v3, :array_2

    const/4 v4, 0x1

    aput-object v3, v1, v4

    new-array v3, v2, [D

    fill-array-data v3, :array_3

    aput-object v3, v1, v2

    new-array v3, v2, [D

    fill-array-data v3, :array_4

    const/4 v4, 0x3

    aput-object v3, v1, v4

    new-array v3, v2, [D

    fill-array-data v3, :array_5

    const/4 v4, 0x4

    aput-object v3, v1, v4

    new-array v3, v2, [D

    fill-array-data v3, :array_6

    const/4 v4, 0x5

    aput-object v3, v1, v4

    new-array v3, v2, [D

    fill-array-data v3, :array_7

    aput-object v3, v1, v0

    new-array v0, v2, [D

    fill-array-data v0, :array_8

    const/4 v3, 0x7

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_9

    const/16 v3, 0x8

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_a

    const/16 v3, 0x9

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_b

    const/16 v3, 0xa

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_c

    const/16 v3, 0xb

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_d

    const/16 v3, 0xc

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_e

    const/16 v3, 0xd

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_f

    const/16 v3, 0xe

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_10

    const/16 v3, 0xf

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_11

    const/16 v3, 0x10

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_12

    const/16 v3, 0x11

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_13

    const/16 v3, 0x12

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_14

    const/16 v3, 0x13

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_15

    const/16 v3, 0x14

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_16

    const/16 v3, 0x15

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_17

    const/16 v3, 0x16

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_18

    const/16 v3, 0x17

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_19

    const/16 v3, 0x18

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_1a

    const/16 v3, 0x19

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_1b

    const/16 v3, 0x1a

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_1c

    const/16 v3, 0x1b

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_1d

    const/16 v3, 0x1c

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_1e

    const/16 v3, 0x1d

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_1f

    const/16 v3, 0x1e

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_20

    const/16 v3, 0x1f

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_21

    const/16 v3, 0x20

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_22

    const/16 v3, 0x21

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_23

    const/16 v3, 0x22

    aput-object v0, v1, v3

    new-array v0, v2, [D

    fill-array-data v0, :array_24

    const/16 v2, 0x23

    aput-object v0, v1, v2

    sput-object v1, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zzb:[[D

    return-void

    :array_0
    .array-data 4
        0x5
        0x7
        0x7
        0x7
        0x5
        0x5
    .end array-data

    :array_1
    .array-data 8
        0x3fb3333333333333L    # 0.075
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_2
    .array-data 8
        0x3fb999999999999aL    # 0.1
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_3
    .array-data 8
        0x3fc0000000000000L    # 0.125
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_4
    .array-data 8
        0x3fc999999999999aL    # 0.2
        0x4000000000000000L    # 2.0
    .end array-data

    :array_5
    .array-data 8
        0x3fc999999999999aL    # 0.2
        0x3fe0000000000000L    # 0.5
    .end array-data

    :array_6
    .array-data 8
        0x3fc3333333333333L    # 0.15
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_7
    .array-data 8
        0x3fc999999999999aL    # 0.2
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_8
    .array-data 8
        0x3fd0000000000000L    # 0.25
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_9
    .array-data 8
        0x3fd6666666666666L    # 0.35
        0x4000000000000000L    # 2.0
    .end array-data

    :array_a
    .array-data 8
        0x3fd6666666666666L    # 0.35
        0x3fe0000000000000L    # 0.5
    .end array-data

    :array_b
    .array-data 8
        0x3fd6666666666666L    # 0.35
        0x4008000000000000L    # 3.0
    .end array-data

    :array_c
    .array-data 8
        0x3fd6666666666666L    # 0.35
        0x3fd554c985f06f69L    # 0.3333
    .end array-data

    :array_d
    .array-data 8
        0x3fd3333333333333L    # 0.3
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_e
    .array-data 8
        0x3fd999999999999aL    # 0.4
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_f
    .array-data 8
        0x3fe0000000000000L    # 0.5
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_10
    .array-data 8
        0x3fe0000000000000L    # 0.5
        0x4000000000000000L    # 2.0
    .end array-data

    :array_11
    .array-data 8
        0x3fe0000000000000L    # 0.5
        0x3fe0000000000000L    # 0.5
    .end array-data

    :array_12
    .array-data 8
        0x3fe0000000000000L    # 0.5
        0x4008000000000000L    # 3.0
    .end array-data

    :array_13
    .array-data 8
        0x3fe0000000000000L    # 0.5
        0x3fd554c985f06f69L    # 0.3333
    .end array-data

    :array_14
    .array-data 8
        0x3fe3333333333333L    # 0.6
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_15
    .array-data 8
        0x3fe999999999999aL    # 0.8
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_16
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_17
    .array-data 8
        0x3fe4cccccccccccdL    # 0.65
        0x4000000000000000L    # 2.0
    .end array-data

    :array_18
    .array-data 8
        0x3fe4cccccccccccdL    # 0.65
        0x3fe0000000000000L    # 0.5
    .end array-data

    :array_19
    .array-data 8
        0x3fe4cccccccccccdL    # 0.65
        0x4008000000000000L    # 3.0
    .end array-data

    :array_1a
    .array-data 8
        0x3fe4cccccccccccdL    # 0.65
        0x3fd554c985f06f69L    # 0.3333
    .end array-data

    :array_1b
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_1c
    .array-data 8
        0x3fe999999999999aL    # 0.8
        0x4000000000000000L    # 2.0
    .end array-data

    :array_1d
    .array-data 8
        0x3fe999999999999aL    # 0.8
        0x3fe0000000000000L    # 0.5
    .end array-data

    :array_1e
    .array-data 8
        0x3fe999999999999aL    # 0.8
        0x4008000000000000L    # 3.0
    .end array-data

    :array_1f
    .array-data 8
        0x3fe999999999999aL    # 0.8
        0x3fd554c985f06f69L    # 0.3333
    .end array-data

    :array_20
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_21
    .array-data 8
        0x3fee666666666666L    # 0.95
        0x4000000000000000L    # 2.0
    .end array-data

    :array_22
    .array-data 8
        0x3fee666666666666L    # 0.95
        0x3fe0000000000000L    # 0.5
    .end array-data

    :array_23
    .array-data 8
        0x3fee666666666666L    # 0.95
        0x4008000000000000L    # 3.0
    .end array-data

    :array_24
    .array-data 8
        0x3fee666666666666L    # 0.95
        0x3fd554c985f06f69L    # 0.3333
    .end array-data
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbc;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbk;-><init>()V

    new-instance v0, Lcom/google/android/libraries/barhopper/RecognitionOptions;

    invoke-direct {v0}, Lcom/google/android/libraries/barhopper/RecognitionOptions;-><init>()V

    iput-object v0, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zzd:Lcom/google/android/libraries/barhopper/RecognitionOptions;

    iput-object p1, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zzc:Landroid/content/Context;

    .line 2
    invoke-virtual {p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbc;->zza()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/barhopper/RecognitionOptions;->setBarcodeFormats(I)V

    .line 3
    invoke-virtual {p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbc;->zzb()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/barhopper/RecognitionOptions;->setOutputUnrecognizedBarcodes(Z)V

    return-void
.end method

.method private static zze(Lcom/google/photos/vision/barhopper/zzn;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzap;
    .locals 10

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    if-nez p1, :cond_0

    goto :goto_0

    .line 1
    :cond_0
    invoke-static {p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    .line 2
    new-instance p2, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzap;

    invoke-virtual {p0}, Lcom/google/photos/vision/barhopper/zzn;->zzf()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/photos/vision/barhopper/zzn;->zzd()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/photos/vision/barhopper/zzn;->zza()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/photos/vision/barhopper/zzn;->zzb()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/photos/vision/barhopper/zzn;->zzc()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/photos/vision/barhopper/zzn;->zze()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/photos/vision/barhopper/zzn;->zzj()Z

    move-result v8

    .line 3
    invoke-virtual {p1}, Ljava/util/regex/Matcher;->find()Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    invoke-virtual {p1, p0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    move-object v9, v0

    move-object v1, p2

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzap;-><init>(IIIIIIZLjava/lang/String;)V

    return-object p2

    :cond_2
    :goto_0
    return-object v0
.end method

.method private final zzf(Ljava/nio/ByteBuffer;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;)Lcom/google/photos/vision/barhopper/BarhopperProto$BarhopperResponse;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zze:Lcom/google/android/libraries/barhopper/BarhopperV3;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/barhopper/BarhopperV3;

    .line 2
    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3
    invoke-virtual {p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;->zzd()I

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;->zza()I

    move-result p2

    iget-object v2, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zzd:Lcom/google/android/libraries/barhopper/RecognitionOptions;

    .line 4
    invoke-virtual {v0, v1, p2, p1, v2}, Lcom/google/android/libraries/barhopper/BarhopperV3;->recognize(IILjava/nio/ByteBuffer;Lcom/google/android/libraries/barhopper/RecognitionOptions;)Lcom/google/photos/vision/barhopper/BarhopperProto$BarhopperResponse;

    move-result-object p1

    goto :goto_0

    .line 5
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v1

    if-nez v1, :cond_1

    .line 9
    invoke-virtual {p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;->zzd()I

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;->zza()I

    move-result p2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object p1

    iget-object v2, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zzd:Lcom/google/android/libraries/barhopper/RecognitionOptions;

    .line 10
    invoke-virtual {v0, v1, p2, p1, v2}, Lcom/google/android/libraries/barhopper/BarhopperV3;->recognize(II[BLcom/google/android/libraries/barhopper/RecognitionOptions;)Lcom/google/photos/vision/barhopper/BarhopperProto$BarhopperResponse;

    move-result-object p1

    goto :goto_0

    .line 6
    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    new-array v1, v1, [B

    .line 7
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 8
    invoke-virtual {p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;->zzd()I

    move-result p1

    invoke-virtual {p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;->zza()I

    move-result p2

    iget-object v2, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zzd:Lcom/google/android/libraries/barhopper/RecognitionOptions;

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/google/android/libraries/barhopper/BarhopperV3;->recognize(II[BLcom/google/android/libraries/barhopper/RecognitionOptions;)Lcom/google/photos/vision/barhopper/BarhopperProto$BarhopperResponse;

    move-result-object p1

    :goto_0
    return-object p1
.end method


# virtual methods
.method public final zzb(Lcom/google/android/gms/dynamic/IObjectWrapper;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;)Ljava/util/List;
    .locals 54

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    .line 1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;->zzb()I

    move-result v2

    const/4 v3, -0x1

    const/4 v4, 0x0

    if-eq v2, v3, :cond_3

    const/16 v5, 0x11

    if-eq v2, v5, :cond_2

    const/16 v5, 0x23

    if-eq v2, v5, :cond_1

    const v5, 0x32315659

    if-ne v2, v5, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    .line 123
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;->zzb()I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported image format: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/Image;

    .line 3
    invoke-static {v2}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/Image;

    invoke-virtual {v2}, Landroid/media/Image;->getPlanes()[Landroid/media/Image$Plane;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/media/Image$Plane;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zzf(Ljava/nio/ByteBuffer;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;)Lcom/google/photos/vision/barhopper/BarhopperProto$BarhopperResponse;

    move-result-object v2

    goto :goto_1

    .line 4
    :cond_2
    :goto_0
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/nio/ByteBuffer;

    invoke-direct {v0, v2, v1}, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zzf(Ljava/nio/ByteBuffer;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;)Lcom/google/photos/vision/barhopper/BarhopperProto$BarhopperResponse;

    move-result-object v2

    goto :goto_1

    .line 123
    :cond_3
    iget-object v2, v0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zze:Lcom/google/android/libraries/barhopper/BarhopperV3;

    .line 5
    invoke-static {v2}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/barhopper/BarhopperV3;

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    iget-object v6, v0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zzd:Lcom/google/android/libraries/barhopper/RecognitionOptions;

    invoke-virtual {v2, v5, v6}, Lcom/google/android/libraries/barhopper/BarhopperV3;->recognize(Landroid/graphics/Bitmap;Lcom/google/android/libraries/barhopper/RecognitionOptions;)Lcom/google/photos/vision/barhopper/BarhopperProto$BarhopperResponse;

    move-result-object v2

    .line 3
    :goto_1
    new-instance v5, Ljava/util/ArrayList;

    .line 6
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 7
    invoke-static {}, Lcom/google/mlkit/vision/common/internal/ImageUtils;->getInstance()Lcom/google/mlkit/vision/common/internal/ImageUtils;

    move-result-object v6

    .line 8
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;->zzd()I

    move-result v7

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;->zza()I

    move-result v8

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;->zzc()I

    move-result v9

    .line 9
    invoke-virtual {v6, v7, v8, v9}, Lcom/google/mlkit/vision/common/internal/ImageUtils;->getUprightRotationMatrix(III)Landroid/graphics/Matrix;

    move-result-object v6

    .line 10
    invoke-virtual {v2}, Lcom/google/photos/vision/barhopper/BarhopperProto$BarhopperResponse;->zzc()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/photos/vision/barhopper/zzc;

    .line 11
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zza()I

    move-result v8

    const/16 v9, 0x8

    const/4 v10, 0x1

    if-lez v8, :cond_5

    if-eqz v6, :cond_5

    new-array v8, v9, [F

    .line 12
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzo()Ljava/util/List;

    move-result-object v11

    .line 13
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zza()I

    move-result v12

    const/4 v13, 0x0

    :goto_3
    if-ge v13, v12, :cond_4

    .line 14
    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/photos/vision/barhopper/zzae;

    invoke-virtual {v14}, Lcom/google/photos/vision/barhopper/zzae;->zza()I

    move-result v14

    int-to-float v14, v14

    add-int v15, v13, v13

    aput v14, v8, v15

    add-int/2addr v15, v10

    .line 15
    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/photos/vision/barhopper/zzae;

    invoke-virtual {v14}, Lcom/google/photos/vision/barhopper/zzae;->zzb()I

    move-result v14

    int-to-float v14, v14

    aput v14, v8, v15

    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 16
    :cond_4
    invoke-virtual {v6, v8}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 17
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzbu;->zzc()I

    move-result v11

    const/4 v13, 0x0

    :goto_4
    if-ge v13, v12, :cond_5

    .line 18
    invoke-virtual {v7}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzed;->zzG()Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdx;

    move-result-object v7

    check-cast v7, Lcom/google/photos/vision/barhopper/zzb;

    add-int v14, v13, v11

    rem-int/2addr v14, v12

    add-int v15, v13, v13

    .line 19
    invoke-static {}, Lcom/google/photos/vision/barhopper/zzae;->zzc()Lcom/google/photos/vision/barhopper/zzad;

    move-result-object v9

    aget v4, v8, v15

    float-to-int v4, v4

    .line 20
    invoke-virtual {v9, v4}, Lcom/google/photos/vision/barhopper/zzad;->zza(I)Lcom/google/photos/vision/barhopper/zzad;

    add-int/2addr v15, v10

    aget v4, v8, v15

    float-to-int v4, v4

    .line 21
    invoke-virtual {v9, v4}, Lcom/google/photos/vision/barhopper/zzad;->zzb(I)Lcom/google/photos/vision/barhopper/zzad;

    .line 22
    invoke-virtual {v9}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdx;->zzh()Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzed;

    move-result-object v4

    check-cast v4, Lcom/google/photos/vision/barhopper/zzae;

    .line 23
    invoke-virtual {v7, v14, v4}, Lcom/google/photos/vision/barhopper/zzb;->zza(ILcom/google/photos/vision/barhopper/zzae;)Lcom/google/photos/vision/barhopper/zzb;

    .line 24
    invoke-virtual {v7}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdx;->zzh()Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzed;

    move-result-object v4

    move-object v7, v4

    check-cast v7, Lcom/google/photos/vision/barhopper/zzc;

    add-int/lit8 v13, v13, 0x1

    const/4 v4, 0x0

    const/16 v9, 0x8

    goto :goto_4

    .line 25
    :cond_5
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzt()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 26
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzh()Lcom/google/photos/vision/barhopper/zzy;

    move-result-object v4

    .line 27
    new-instance v9, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzat;

    .line 28
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzy;->zzf()I

    move-result v11

    add-int/2addr v11, v3

    .line 29
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzy;->zzc()Ljava/lang/String;

    move-result-object v12

    .line 30
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzy;->zze()Ljava/lang/String;

    move-result-object v13

    .line 31
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzy;->zzd()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v9, v11, v12, v13, v4}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzat;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v24, v9

    goto :goto_5

    :cond_6
    const/16 v24, 0x0

    .line 32
    :goto_5
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzv()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 33
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzb()Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzci;

    move-result-object v4

    .line 34
    new-instance v9, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzaw;

    invoke-virtual {v4}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzci;->zzd()I

    move-result v11

    add-int/2addr v11, v3

    invoke-virtual {v4}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzci;->zzc()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v9, v11, v4}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzaw;-><init>(ILjava/lang/String;)V

    move-object/from16 v25, v9

    goto :goto_6

    :cond_7
    const/16 v25, 0x0

    .line 35
    :goto_6
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzw()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 36
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzj()Lcom/google/photos/vision/barhopper/zzag;

    move-result-object v4

    .line 37
    new-instance v9, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzax;

    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzag;->zzc()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzag;->zzd()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v9, v11, v4}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzax;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v26, v9

    goto :goto_7

    :cond_8
    const/16 v26, 0x0

    .line 38
    :goto_7
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzy()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 39
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzl()Lcom/google/photos/vision/barhopper/zzao;

    move-result-object v4

    .line 40
    new-instance v9, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzaz;

    .line 41
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzao;->zzd()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzao;->zzc()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzao;->zze()I

    move-result v4

    add-int/2addr v4, v3

    invoke-direct {v9, v11, v12, v4}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzaz;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v27, v9

    goto :goto_8

    :cond_9
    const/16 v27, 0x0

    .line 42
    :goto_8
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzx()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 43
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzk()Lcom/google/photos/vision/barhopper/zzaj;

    move-result-object v4

    .line 44
    new-instance v9, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzay;

    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzaj;->zzc()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzaj;->zzd()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v9, v11, v4}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzay;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v28, v9

    goto :goto_9

    :cond_a
    const/16 v28, 0x0

    .line 45
    :goto_9
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzu()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 46
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzi()Lcom/google/photos/vision/barhopper/zzac;

    move-result-object v4

    .line 47
    new-instance v9, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzau;

    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzac;->zza()D

    move-result-wide v11

    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzac;->zzb()D

    move-result-wide v13

    invoke-direct {v9, v11, v12, v13, v14}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzau;-><init>(DD)V

    move-object/from16 v29, v9

    goto :goto_a

    :cond_b
    const/16 v29, 0x0

    .line 48
    :goto_a
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzq()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 49
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzd()Lcom/google/photos/vision/barhopper/zzp;

    move-result-object v4

    .line 50
    new-instance v9, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzaq;

    .line 51
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzp;->zzj()Ljava/lang/String;

    move-result-object v31

    .line 52
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzp;->zze()Ljava/lang/String;

    move-result-object v32

    .line 53
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzp;->zzf()Ljava/lang/String;

    move-result-object v33

    .line 54
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzp;->zzh()Ljava/lang/String;

    move-result-object v34

    .line 55
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzp;->zzi()Ljava/lang/String;

    move-result-object v35

    .line 56
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzp;->zzb()Lcom/google/photos/vision/barhopper/zzn;

    move-result-object v11

    .line 57
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzm()Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;->zzn()Z

    move-result v12

    if-eqz v12, :cond_c

    .line 58
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzm()Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;->zzu()Ljava/lang/String;

    move-result-object v12

    goto :goto_b

    :cond_c
    const/4 v12, 0x0

    :goto_b
    const-string v13, "DTSTART:([0-9TZ]*)"

    .line 59
    invoke-static {v11, v12, v13}, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zze(Lcom/google/photos/vision/barhopper/zzn;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzap;

    move-result-object v36

    .line 60
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzp;->zza()Lcom/google/photos/vision/barhopper/zzn;

    move-result-object v4

    .line 61
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzm()Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;->zzn()Z

    move-result v11

    if-eqz v11, :cond_d

    .line 62
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzm()Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;->zzu()Ljava/lang/String;

    move-result-object v11

    goto :goto_c

    :cond_d
    const/4 v11, 0x0

    :goto_c
    const-string v12, "DTEND:([0-9TZ]*)"

    .line 63
    invoke-static {v4, v11, v12}, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zze(Lcom/google/photos/vision/barhopper/zzn;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzap;

    move-result-object v37

    move-object/from16 v30, v9

    invoke-direct/range {v30 .. v37}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzaq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzap;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzap;)V

    goto :goto_d

    :cond_e
    const/16 v30, 0x0

    .line 64
    :goto_d
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzr()Z

    move-result v4

    if-eqz v4, :cond_16

    .line 65
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zze()Lcom/google/photos/vision/barhopper/zzr;

    move-result-object v4

    .line 66
    new-instance v9, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzar;

    .line 67
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzr;->zza()Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzcd;

    move-result-object v11

    if-eqz v11, :cond_f

    .line 68
    new-instance v12, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzav;

    invoke-virtual {v11}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzcd;->zzd()Ljava/lang/String;

    move-result-object v32

    invoke-virtual {v11}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzcd;->zzi()Ljava/lang/String;

    move-result-object v33

    invoke-virtual {v11}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzcd;->zzh()Ljava/lang/String;

    move-result-object v34

    invoke-virtual {v11}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzcd;->zzc()Ljava/lang/String;

    move-result-object v35

    invoke-virtual {v11}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzcd;->zzf()Ljava/lang/String;

    move-result-object v36

    invoke-virtual {v11}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzcd;->zze()Ljava/lang/String;

    move-result-object v37

    invoke-virtual {v11}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzcd;->zzj()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v31, v12

    .line 69
    invoke-direct/range {v31 .. v38}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzav;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v32, v12

    goto :goto_e

    :cond_f
    const/16 v32, 0x0

    .line 70
    :goto_e
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzr;->zzd()Ljava/lang/String;

    move-result-object v33

    .line 71
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzr;->zze()Ljava/lang/String;

    move-result-object v34

    .line 72
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzr;->zzi()Ljava/util/List;

    move-result-object v11

    .line 73
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_10

    const/16 v35, 0x0

    goto :goto_10

    .line 74
    :cond_10
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v12

    new-array v12, v12, [Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzaw;

    const/4 v13, 0x0

    .line 75
    :goto_f
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v14

    if-ge v13, v14, :cond_11

    .line 76
    new-instance v14, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzaw;

    .line 77
    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzci;

    invoke-virtual {v15}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzci;->zzd()I

    move-result v15

    add-int/2addr v15, v3

    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzci;

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzci;->zzc()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v14, v15, v8}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzaw;-><init>(ILjava/lang/String;)V

    aput-object v14, v12, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_f

    :cond_11
    move-object/from16 v35, v12

    .line 78
    :goto_10
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzr;->zzh()Ljava/util/List;

    move-result-object v8

    .line 79
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_12

    const/16 v36, 0x0

    goto :goto_12

    .line 80
    :cond_12
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v11

    new-array v11, v11, [Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzat;

    const/4 v12, 0x0

    .line 81
    :goto_11
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v13

    if-ge v12, v13, :cond_13

    .line 82
    new-instance v13, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzat;

    .line 83
    invoke-interface {v8, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/photos/vision/barhopper/zzy;

    invoke-virtual {v14}, Lcom/google/photos/vision/barhopper/zzy;->zzf()I

    move-result v14

    add-int/2addr v14, v3

    .line 84
    invoke-interface {v8, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/photos/vision/barhopper/zzy;

    invoke-virtual {v15}, Lcom/google/photos/vision/barhopper/zzy;->zzc()Ljava/lang/String;

    move-result-object v15

    .line 85
    invoke-interface {v8, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/photos/vision/barhopper/zzy;

    invoke-virtual/range {v17 .. v17}, Lcom/google/photos/vision/barhopper/zzy;->zze()Ljava/lang/String;

    move-result-object v10

    .line 86
    invoke-interface {v8, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/photos/vision/barhopper/zzy;

    invoke-virtual/range {v17 .. v17}, Lcom/google/photos/vision/barhopper/zzy;->zzd()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v13, v14, v15, v10, v3}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzat;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v13, v11, v12

    add-int/lit8 v12, v12, 0x1

    const/4 v3, -0x1

    const/4 v10, 0x1

    goto :goto_11

    :cond_13
    move-object/from16 v36, v11

    .line 87
    :goto_12
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzr;->zzj()Ljava/util/List;

    move-result-object v3

    const/4 v8, 0x0

    new-array v10, v8, [Ljava/lang/String;

    invoke-interface {v3, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v37, v3

    check-cast v37, [Ljava/lang/String;

    .line 88
    invoke-virtual {v4}, Lcom/google/photos/vision/barhopper/zzr;->zzf()Ljava/util/List;

    move-result-object v3

    .line 89
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_14

    const/4 v13, 0x0

    const/16 v38, 0x0

    goto :goto_14

    .line 90
    :cond_14
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzao;

    const/4 v8, 0x0

    .line 91
    :goto_13
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    if-ge v8, v10, :cond_15

    .line 92
    new-instance v10, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzao;

    .line 93
    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzcb;

    invoke-virtual {v11}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzcb;->zzc()I

    move-result v11

    const/4 v12, -0x1

    add-int/2addr v11, v12

    .line 94
    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzcb;

    invoke-virtual {v12}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzcb;->zzb()Ljava/util/List;

    move-result-object v12

    const/4 v13, 0x0

    new-array v14, v13, [Ljava/lang/String;

    invoke-interface {v12, v14}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [Ljava/lang/String;

    invoke-direct {v10, v11, v12}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzao;-><init>(I[Ljava/lang/String;)V

    aput-object v10, v4, v8

    add-int/lit8 v8, v8, 0x1

    goto :goto_13

    :cond_15
    const/4 v13, 0x0

    move-object/from16 v38, v4

    :goto_14
    move-object/from16 v31, v9

    .line 88
    invoke-direct/range {v31 .. v38}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzar;-><init>(Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzav;Ljava/lang/String;Ljava/lang/String;[Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzaw;[Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzat;[Ljava/lang/String;[Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzao;)V

    goto :goto_15

    :cond_16
    const/4 v13, 0x0

    const/16 v31, 0x0

    .line 95
    :goto_15
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzs()Z

    move-result v3

    if-eqz v3, :cond_17

    .line 96
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzf()Lcom/google/photos/vision/barhopper/zzt;

    move-result-object v3

    .line 97
    new-instance v4, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzas;

    .line 98
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zzi()Ljava/lang/String;

    move-result-object v40

    .line 99
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zzk()Ljava/lang/String;

    move-result-object v41

    .line 100
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zzq()Ljava/lang/String;

    move-result-object v42

    .line 101
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zzo()Ljava/lang/String;

    move-result-object v43

    .line 102
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zzl()Ljava/lang/String;

    move-result-object v44

    .line 103
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zze()Ljava/lang/String;

    move-result-object v45

    .line 104
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zzc()Ljava/lang/String;

    move-result-object v46

    .line 105
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zzd()Ljava/lang/String;

    move-result-object v47

    .line 106
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zzf()Ljava/lang/String;

    move-result-object v48

    .line 107
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zzp()Ljava/lang/String;

    move-result-object v49

    .line 108
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zzm()Ljava/lang/String;

    move-result-object v50

    .line 109
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zzj()Ljava/lang/String;

    move-result-object v51

    .line 110
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zzh()Ljava/lang/String;

    move-result-object v52

    .line 111
    invoke-virtual {v3}, Lcom/google/photos/vision/barhopper/zzt;->zzn()Ljava/lang/String;

    move-result-object v53

    move-object/from16 v39, v4

    invoke-direct/range {v39 .. v53}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzas;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v32, v4

    goto :goto_16

    :cond_17
    const/16 v32, 0x0

    .line 112
    :goto_16
    new-instance v3, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzba;

    .line 113
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzz()I

    move-result v4

    const/4 v8, -0x1

    add-int/2addr v4, v8

    packed-switch v4, :pswitch_data_0

    const/4 v4, -0x1

    goto :goto_17

    :pswitch_0
    const/16 v4, 0x1000

    goto :goto_17

    :pswitch_1
    const/16 v4, 0x800

    goto :goto_17

    :pswitch_2
    const/16 v4, 0x400

    goto :goto_17

    :pswitch_3
    const/16 v4, 0x200

    goto :goto_17

    :pswitch_4
    const/16 v4, 0x100

    goto :goto_17

    :pswitch_5
    const/16 v4, 0x80

    goto :goto_17

    :pswitch_6
    const/16 v4, 0x40

    goto :goto_17

    :pswitch_7
    const/16 v4, 0x20

    goto :goto_17

    :pswitch_8
    const/16 v4, 0x10

    goto :goto_17

    :pswitch_9
    const/16 v4, 0x8

    goto :goto_17

    :pswitch_a
    const/4 v4, 0x4

    goto :goto_17

    :pswitch_b
    const/4 v4, 0x2

    goto :goto_17

    :pswitch_c
    const/4 v4, 0x1

    goto :goto_17

    :pswitch_d
    const/4 v4, 0x0

    .line 114
    :goto_17
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzn()Ljava/lang/String;

    move-result-object v10

    .line 115
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzm()Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;->zzn()Z

    move-result v11

    if-eqz v11, :cond_18

    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzm()Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;->zzu()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v20, v11

    goto :goto_18

    :cond_18
    const/16 v20, 0x0

    .line 116
    :goto_18
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzm()Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;->zzx()[B

    move-result-object v21

    .line 117
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzo()Ljava/util/List;

    move-result-object v11

    .line 118
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_19

    const/16 v22, 0x0

    goto :goto_1a

    .line 119
    :cond_19
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v12

    new-array v12, v12, [Landroid/graphics/Point;

    const/4 v14, 0x0

    .line 120
    :goto_19
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v15

    if-ge v14, v15, :cond_1a

    new-instance v15, Landroid/graphics/Point;

    .line 121
    invoke-interface {v11, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/photos/vision/barhopper/zzae;

    invoke-virtual/range {v16 .. v16}, Lcom/google/photos/vision/barhopper/zzae;->zza()I

    move-result v8

    invoke-interface {v11, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/photos/vision/barhopper/zzae;

    invoke-virtual/range {v16 .. v16}, Lcom/google/photos/vision/barhopper/zzae;->zzb()I

    move-result v9

    invoke-direct {v15, v8, v9}, Landroid/graphics/Point;-><init>(II)V

    aput-object v15, v12, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_19

    :cond_1a
    move-object/from16 v22, v12

    .line 122
    :goto_1a
    invoke-virtual {v7}, Lcom/google/photos/vision/barhopper/zzc;->zzA()I

    move-result v7

    const/4 v8, -0x1

    add-int/2addr v7, v8

    packed-switch v7, :pswitch_data_1

    const/16 v23, 0x0

    goto :goto_1b

    :pswitch_e
    const/16 v9, 0xc

    const/16 v23, 0xc

    goto :goto_1b

    :pswitch_f
    const/16 v9, 0xb

    const/16 v23, 0xb

    goto :goto_1b

    :pswitch_10
    const/16 v9, 0xa

    const/16 v23, 0xa

    goto :goto_1b

    :pswitch_11
    const/16 v9, 0x9

    const/16 v23, 0x9

    goto :goto_1b

    :pswitch_12
    const/16 v23, 0x8

    goto :goto_1b

    :pswitch_13
    const/4 v9, 0x7

    const/16 v23, 0x7

    goto :goto_1b

    :pswitch_14
    const/4 v9, 0x6

    const/16 v23, 0x6

    goto :goto_1b

    :pswitch_15
    const/4 v9, 0x5

    const/16 v23, 0x5

    goto :goto_1b

    :pswitch_16
    const/16 v23, 0x4

    goto :goto_1b

    :pswitch_17
    const/4 v9, 0x3

    const/16 v23, 0x3

    goto :goto_1b

    :pswitch_18
    const/16 v23, 0x2

    goto :goto_1b

    :pswitch_19
    const/16 v23, 0x1

    :goto_1b
    move-object/from16 v17, v3

    move/from16 v18, v4

    move-object/from16 v19, v10

    invoke-direct/range {v17 .. v32}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzba;-><init>(ILjava/lang/String;Ljava/lang/String;[B[Landroid/graphics/Point;ILcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzat;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzaw;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzax;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzaz;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzay;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzau;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzaq;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzar;Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzas;)V

    .line 112
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, -0x1

    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_1b
    return-object v5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
    .end packed-switch
.end method

.method public final zzc()V
    .locals 13

    .line 1
    iget-object v0, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zze:Lcom/google/android/libraries/barhopper/BarhopperV3;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/libraries/barhopper/BarhopperV3;

    invoke-direct {v0}, Lcom/google/android/libraries/barhopper/BarhopperV3;-><init>()V

    iput-object v0, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zze:Lcom/google/android/libraries/barhopper/BarhopperV3;

    .line 2
    invoke-static {}, Lcom/google/barhopper/deeplearning/zzi;->zza()Lcom/google/barhopper/deeplearning/zzh;

    move-result-object v0

    .line 3
    invoke-static {}, Lcom/google/barhopper/deeplearning/zzf;->zza()Lcom/google/barhopper/deeplearning/zze;

    move-result-object v1

    const/16 v2, 0x10

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x6

    if-ge v4, v6, :cond_2

    .line 4
    invoke-static {}, Lcom/google/barhopper/deeplearning/zzc;->zza()Lcom/google/barhopper/deeplearning/zzb;

    move-result-object v6

    .line 5
    invoke-virtual {v6, v2}, Lcom/google/barhopper/deeplearning/zzb;->zzc(I)Lcom/google/barhopper/deeplearning/zzb;

    .line 6
    invoke-virtual {v6, v2}, Lcom/google/barhopper/deeplearning/zzb;->zzd(I)Lcom/google/barhopper/deeplearning/zzb;

    const/4 v7, 0x0

    :goto_1
    sget-object v8, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zza:[I

    .line 7
    aget v8, v8, v4

    if-ge v7, v8, :cond_1

    sget-object v8, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zzb:[[D

    .line 8
    aget-object v8, v8, v5

    aget-wide v9, v8, v3

    const-wide/high16 v11, 0x4074000000000000L    # 320.0

    mul-double v9, v9, v11

    const/4 v11, 0x1

    .line 9
    aget-wide v11, v8, v11

    .line 10
    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v11

    double-to-float v8, v11

    double-to-float v9, v9

    div-float v10, v9, v8

    .line 11
    invoke-virtual {v6, v10}, Lcom/google/barhopper/deeplearning/zzb;->zza(F)Lcom/google/barhopper/deeplearning/zzb;

    mul-float v9, v9, v8

    .line 12
    invoke-virtual {v6, v9}, Lcom/google/barhopper/deeplearning/zzb;->zzb(F)Lcom/google/barhopper/deeplearning/zzb;

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_1
    add-int/2addr v2, v2

    .line 13
    invoke-virtual {v1, v6}, Lcom/google/barhopper/deeplearning/zze;->zza(Lcom/google/barhopper/deeplearning/zzb;)Lcom/google/barhopper/deeplearning/zze;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 14
    :cond_2
    invoke-virtual {v0, v1}, Lcom/google/barhopper/deeplearning/zzh;->zza(Lcom/google/barhopper/deeplearning/zze;)Lcom/google/barhopper/deeplearning/zzh;

    :try_start_0
    iget-object v1, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zzc:Landroid/content/Context;

    .line 15
    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "mlkit_barcode_models/barcode_ssd_mobilenet_v1_dmp25_quant.tflite"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v2, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zzc:Landroid/content/Context;

    .line 16
    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "mlkit_barcode_models/oned_auto_regressor_mobile.tflite"

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    :try_start_2
    iget-object v3, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zzc:Landroid/content/Context;

    .line 17
    invoke-virtual {v3}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    const-string v4, "mlkit_barcode_models/oned_feature_extractor_mobile.tflite"

    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    iget-object v4, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zze:Lcom/google/android/libraries/barhopper/BarhopperV3;

    .line 18
    invoke-static {v4}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/barhopper/BarhopperV3;

    .line 19
    invoke-static {}, Lcom/google/barhopper/deeplearning/BarhopperV3Options;->zza()Lcom/google/barhopper/deeplearning/zzk;

    move-result-object v5

    .line 20
    invoke-static {v1}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;->zzs(Ljava/io/InputStream;)Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/barhopper/deeplearning/zzh;->zzb(Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;)Lcom/google/barhopper/deeplearning/zzh;

    .line 21
    invoke-virtual {v5, v0}, Lcom/google/barhopper/deeplearning/zzk;->zza(Lcom/google/barhopper/deeplearning/zzh;)Lcom/google/barhopper/deeplearning/zzk;

    .line 22
    invoke-static {}, Lcom/google/barhopper/deeplearning/zzn;->zza()Lcom/google/barhopper/deeplearning/zzm;

    move-result-object v0

    .line 23
    invoke-static {v2}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;->zzs(Ljava/io/InputStream;)Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/barhopper/deeplearning/zzm;->zza(Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;)Lcom/google/barhopper/deeplearning/zzm;

    .line 24
    invoke-static {v3}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;->zzs(Ljava/io/InputStream;)Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/barhopper/deeplearning/zzm;->zzb(Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdb;)Lcom/google/barhopper/deeplearning/zzm;

    .line 25
    invoke-virtual {v5, v0}, Lcom/google/barhopper/deeplearning/zzk;->zzb(Lcom/google/barhopper/deeplearning/zzm;)Lcom/google/barhopper/deeplearning/zzk;

    .line 26
    invoke-virtual {v5}, Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzdx;->zzh()Lcom/google/android/gms/internal/mlkit_vision_barcode_bundled/zzed;

    move-result-object v0

    check-cast v0, Lcom/google/barhopper/deeplearning/BarhopperV3Options;

    .line 27
    invoke-virtual {v4, v0}, Lcom/google/android/libraries/barhopper/BarhopperV3;->create(Lcom/google/barhopper/deeplearning/BarhopperV3Options;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v3, :cond_3

    .line 28
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_3
    if-eqz v2, :cond_4

    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    :cond_4
    if-eqz v1, :cond_5

    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    :cond_5
    return-void

    :catchall_0
    move-exception v0

    if-eqz v3, :cond_6

    .line 15
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v3

    :try_start_8
    invoke-static {v0, v3}, Lcom/google/mlkit/vision/barcode/bundled/internal/zza;->zza(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    :cond_6
    :goto_2
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :catchall_2
    move-exception v0

    if-eqz v2, :cond_7

    :try_start_9
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    goto :goto_3

    :catchall_3
    move-exception v2

    :try_start_a
    invoke-static {v0, v2}, Lcom/google/mlkit/vision/barcode/bundled/internal/zza;->zza(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    :cond_7
    :goto_3
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :catchall_4
    move-exception v0

    if-eqz v1, :cond_8

    :try_start_b
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    goto :goto_4

    :catchall_5
    move-exception v1

    :try_start_c
    invoke-static {v0, v1}, Lcom/google/mlkit/vision/barcode/bundled/internal/zza;->zza(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    :cond_8
    :goto_4
    throw v0
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0

    :catch_0
    move-exception v0

    .line 28
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Failed to open Barcode models"

    .line 29
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final zzd()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zze:Lcom/google/android/libraries/barhopper/BarhopperV3;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/libraries/barhopper/BarhopperV3;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/mlkit/vision/barcode/bundled/internal/zzb;->zze:Lcom/google/android/libraries/barhopper/BarhopperV3;

    :cond_0
    return-void
.end method

.class public final Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;
.super Ljava/lang/Object;
.source "PnsSpecificRegistrationFactory.java"


# static fields
.field private static final mInstance:Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

.field private static mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    sget-object v0, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->fcm:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    sput-object v0, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    .line 38
    new-instance v0, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    invoke-direct {v0}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;-><init>()V

    sput-object v0, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->mInstance:Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Amazon"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 49
    sget-object v0, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->adm:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    sput-object v0, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    :cond_1
    return-void
.end method

.method public static getInstance()Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;
    .locals 1

    .line 58
    sget-object v0, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->mInstance:Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    return-object v0
.end method


# virtual methods
.method public createNativeRegistration(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/Registration;
    .locals 2

    .line 71
    sget-object v0, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory$1;->$SwitchMap$com$microsoft$windowsazure$messaging$Registration$RegistrationType:[I

    sget-object v1, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    invoke-virtual {v1}, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 82
    new-instance v0, Lcom/microsoft/windowsazure/messaging/AdmNativeRegistration;

    invoke-direct {v0, p1}, Lcom/microsoft/windowsazure/messaging/AdmNativeRegistration;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 85
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Ivalid registration type!"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 79
    :cond_1
    new-instance v0, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;

    invoke-direct {v0, p1}, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 76
    :cond_2
    new-instance v0, Lcom/microsoft/windowsazure/messaging/FcmNativeRegistration;

    invoke-direct {v0, p1}, Lcom/microsoft/windowsazure/messaging/FcmNativeRegistration;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 73
    :cond_3
    new-instance v0, Lcom/microsoft/windowsazure/messaging/GcmNativeRegistration;

    invoke-direct {v0, p1}, Lcom/microsoft/windowsazure/messaging/GcmNativeRegistration;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public createTemplateRegistration(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/TemplateRegistration;
    .locals 2

    .line 97
    sget-object v0, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory$1;->$SwitchMap$com$microsoft$windowsazure$messaging$Registration$RegistrationType:[I

    sget-object v1, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    invoke-virtual {v1}, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 105
    new-instance v0, Lcom/microsoft/windowsazure/messaging/AdmTemplateRegistration;

    invoke-direct {v0, p1}, Lcom/microsoft/windowsazure/messaging/AdmTemplateRegistration;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 107
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Invalid registration type!"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 103
    :cond_1
    new-instance v0, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;

    invoke-direct {v0, p1}, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 101
    :cond_2
    new-instance v0, Lcom/microsoft/windowsazure/messaging/FcmTemplateRegistration;

    invoke-direct {v0, p1}, Lcom/microsoft/windowsazure/messaging/FcmTemplateRegistration;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 99
    :cond_3
    new-instance v0, Lcom/microsoft/windowsazure/messaging/GcmTemplateRegistration;

    invoke-direct {v0, p1}, Lcom/microsoft/windowsazure/messaging/GcmTemplateRegistration;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public getAPIOrigin()Ljava/lang/String;
    .locals 2

    .line 181
    sget-object v0, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory$1;->$SwitchMap$com$microsoft$windowsazure$messaging$Registration$RegistrationType:[I

    sget-object v1, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    invoke-virtual {v1}, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const-string v0, "AndroidSdkAdm"

    return-object v0

    .line 196
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Invalid registration type!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    const-string v0, "AndroidSdkBaidu"

    return-object v0

    :cond_2
    const-string v0, "AndroidSdkFcm"

    return-object v0

    :cond_3
    const-string v0, "AndroidSdkGcm"

    return-object v0
.end method

.method public getPNSHandleFieldName()Ljava/lang/String;
    .locals 3

    .line 155
    sget-object v0, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory$1;->$SwitchMap$com$microsoft$windowsazure$messaging$Registration$RegistrationType:[I

    sget-object v1, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    invoke-virtual {v1}, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const-string v2, "GcmRegistrationId"

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const-string v0, "AdmRegistrationId"

    return-object v0

    .line 170
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Invalid registration type!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    const-string v0, "BaiduUserId-BaiduChannelId"

    return-object v0

    :cond_2
    return-object v2
.end method

.method public isTemplateRegistration(Ljava/lang/String;)Z
    .locals 3

    .line 120
    sget-object v0, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory$1;->$SwitchMap$com$microsoft$windowsazure$messaging$Registration$RegistrationType:[I

    sget-object v1, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    invoke-virtual {v1}, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const-string v2, "GcmTemplateRegistrationDescription"

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const-string v2, "AdmTemplateRegistrationDescription"

    goto :goto_0

    .line 143
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Invalid registration type!"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    const-string v2, "BaiduTemplateRegistrationDescription"

    .line 147
    :cond_2
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method

.method public setRegistrationType(Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;)V
    .locals 0

    .line 62
    sput-object p1, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    return-void
.end method

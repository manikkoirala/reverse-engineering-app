.class public Lcom/microsoft/windowsazure/messaging/NotificationHubUnauthorizedException;
.super Lcom/microsoft/windowsazure/messaging/NotificationHubException;
.source "NotificationHubUnauthorizedException.java"


# static fields
.field private static final serialVersionUID:J = -0x523f74a6a4477bd8L


# direct methods
.method constructor <init>()V
    .locals 2

    const-string v0, "Unauthorized"

    const/16 v1, 0x191

    .line 37
    invoke-direct {p0, v0, v1}, Lcom/microsoft/windowsazure/messaging/NotificationHubException;-><init>(Ljava/lang/String;I)V

    return-void
.end method

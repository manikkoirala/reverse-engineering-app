.class Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;
.super Ljava/lang/Object;
.source "ConnectionStringParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;
    }
.end annotation


# instance fields
.field private _pos:I

.field private _state:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

.field private _value:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    const/4 p1, 0x0

    .line 51
    iput p1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    .line 52
    sget-object p1, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->EXPECT_KEY:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_state:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    return-void
.end method

.method private varargs createException(ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;
    .locals 1

    .line 99
    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object p1, p2, p3

    .line 100
    iget p1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v0, 0x1

    aput-object p1, p2, v0

    const-string p1, "Error parsing connection string: %s. Position %s"

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-array p2, v0, [Ljava/lang/Object;

    aput-object p1, p2, p3

    const-string p1, "Invalid connection string: %s."

    .line 102
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 104
    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    return-object p2
.end method

.method private extractKey()Ljava/lang/String;
    .locals 5

    .line 114
    iget v0, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    .line 115
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/4 v2, 0x0

    const/16 v3, 0x22

    if-eq v1, v3, :cond_4

    const/16 v3, 0x27

    if-ne v1, v3, :cond_0

    goto :goto_2

    :cond_0
    const/16 v3, 0x3b

    if-eq v1, v3, :cond_3

    const/16 v3, 0x3d

    if-eq v1, v3, :cond_3

    .line 124
    :goto_0
    iget v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    iget-object v4, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 125
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v4, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_1

    goto :goto_1

    .line 129
    :cond_1
    iget v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    goto :goto_0

    .line 131
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v3, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    invoke-virtual {v1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_3
    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "Missing key"

    .line 122
    invoke-direct {p0, v0, v2, v1}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->createException(ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 119
    :cond_4
    :goto_2
    invoke-direct {p0, v1}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->extractString(C)Ljava/lang/String;

    move-result-object v1

    .line 133
    :goto_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    return-object v1

    :cond_5
    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "Empty key"

    .line 134
    invoke-direct {p0, v0, v2, v1}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->createException(ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
.end method

.method private extractString(C)Ljava/lang/String;
    .locals 4

    .line 140
    iget v0, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    .line 141
    :goto_0
    iget v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-eq v1, p1, :cond_0

    .line 142
    iget v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/2addr v1, v3

    iput v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    goto :goto_0

    .line 145
    :cond_0
    iget v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 149
    iget-object p1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 146
    :cond_1
    iget v0, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object p1

    aput-object p1, v1, v2

    const-string p1, "Missing character"

    invoke-direct {p0, v0, p1, v1}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->createException(ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

.method private extractValue()Ljava/lang/String;
    .locals 5

    .line 163
    iget v0, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 164
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x27

    const/4 v2, 0x1

    if-eq v0, v1, :cond_4

    const/16 v1, 0x22

    if-ne v0, v1, :cond_0

    goto :goto_1

    .line 170
    :cond_0
    iget v0, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    const/4 v1, 0x0

    .line 172
    :goto_0
    iget v3, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    iget-object v4, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_3

    if-nez v1, :cond_3

    .line 173
    iget-object v3, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v4, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x3b

    if-ne v3, v4, :cond_2

    .line 176
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->isStartWithKnownKey()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    .line 179
    :cond_1
    iget v3, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/2addr v3, v2

    iput v3, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    goto :goto_0

    .line 182
    :cond_2
    iget v3, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/2addr v3, v2

    iput v3, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    goto :goto_0

    .line 185
    :cond_3
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 167
    :cond_4
    :goto_1
    iget v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    .line 168
    invoke-direct {p0, v0}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->extractString(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    const-string v0, ""

    :goto_2
    return-object v0
.end method

.method private isStartWithKnownKey()Z
    .locals 5

    .line 192
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 194
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget v2, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/lit8 v3, v2, 0x1

    const/4 v4, 0x1

    if-le v1, v3, :cond_1

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    add-int/2addr v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "endpoint"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/2addr v2, v4

    .line 195
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "stsendpoint"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/2addr v2, v4

    .line 196
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "sharedsecretissuer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/2addr v2, v4

    .line 197
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "sharedsecretvalue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/2addr v2, v4

    .line 198
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "sharedaccesskeyname"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/2addr v2, v4

    .line 199
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "sharedaccesskey"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :cond_1
    :goto_0
    return v4
.end method

.method private parse()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x0

    :goto_0
    move-object v2, v1

    .line 60
    :goto_1
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->skipWhitespaces()V

    .line 62
    iget v3, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    iget-object v4, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_state:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    sget-object v4, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->EXPECT_VALUE:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    if-eq v3, v4, :cond_1

    .line 91
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_state:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    sget-object v2, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->EXPECT_ASSIGNMENT:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    if-eq v1, v2, :cond_0

    return-object v0

    .line 92
    :cond_0
    iget v0, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "="

    aput-object v3, v1, v2

    const-string v2, "Missing character %s"

    invoke-direct {p0, v0, v2, v1}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->createException(ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 66
    :cond_1
    sget-object v3, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$1;->$SwitchMap$com$microsoft$windowsazure$messaging$ConnectionStringParser$ParserState:[I

    iget-object v4, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_state:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    invoke-virtual {v4}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    if-eq v3, v5, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/16 v3, 0x3b

    .line 85
    invoke-direct {p0, v3}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->skipOperator(C)V

    .line 86
    sget-object v3, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->EXPECT_KEY:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    iput-object v3, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_state:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    goto :goto_1

    .line 78
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->extractValue()Ljava/lang/String;

    move-result-object v3

    .line 79
    sget-object v4, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->EXPECT_SEPARATOR:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    iput-object v4, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_state:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    .line 80
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    const/16 v3, 0x3d

    .line 73
    invoke-direct {p0, v3}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->skipOperator(C)V

    .line 74
    sget-object v3, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->EXPECT_VALUE:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    iput-object v3, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_state:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    goto :goto_1

    .line 68
    :cond_4
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->extractKey()Ljava/lang/String;

    move-result-object v2

    .line 69
    sget-object v3, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->EXPECT_ASSIGNMENT:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    iput-object v3, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_state:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    goto :goto_1
.end method

.method public static parse(Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;

    invoke-direct {v0, p0}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;-><init>(Ljava/lang/String;)V

    .line 46
    invoke-direct {v0}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->parse()Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private skipOperator(C)V
    .locals 3

    .line 153
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/4 v1, 0x1

    if-ne v0, p1, :cond_0

    .line 157
    iget p1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/2addr p1, v1

    iput p1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    return-void

    .line 154
    :cond_0
    iget v0, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object p1

    aput-object p1, v1, v2

    const-string p1, "Missing character"

    invoke-direct {p0, v0, p1, v1}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->createException(ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

.method private skipWhitespaces()V
    .locals 2

    .line 108
    :goto_0
    iget v0, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_value:Ljava/lang/String;

    iget v1, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget v0, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->_pos:I

    goto :goto_0

    :cond_0
    return-void
.end method

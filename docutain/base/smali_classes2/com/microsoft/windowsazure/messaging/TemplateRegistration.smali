.class public abstract Lcom/microsoft/windowsazure/messaging/TemplateRegistration;
.super Lcom/microsoft/windowsazure/messaging/Registration;
.source "TemplateRegistration.java"


# instance fields
.field private mBodyTemplate:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/Registration;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private appendBodyTemplateNode(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
    .locals 1

    .line 61
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->getBodyTemplate()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "BodyTemplate"

    .line 62
    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 63
    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 65
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->getBodyTemplate()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lorg/w3c/dom/Document;->createCDATASection(Ljava/lang/String;)Lorg/w3c/dom/CDATASection;

    move-result-object p1

    .line 66
    invoke-interface {v0, p1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    :cond_0
    return-void
.end method


# virtual methods
.method protected appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
    .locals 2

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->appendBodyTemplateNode(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V

    .line 51
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TemplateName"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getBodyTemplate()Ljava/lang/String;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->mBodyTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getTemplateName()Ljava/lang/String;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected loadCustomXmlData(Lorg/w3c/dom/Element;)V
    .locals 3

    const-string v0, "BodyTemplate"

    .line 72
    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 73
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x0

    .line 74
    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 75
    :goto_0
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 76
    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    instance-of v2, v2, Lorg/w3c/dom/CharacterData;

    if-eqz v2, :cond_0

    .line 77
    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/CharacterData;

    .line 78
    invoke-interface {v0}, Lorg/w3c/dom/CharacterData;->getData()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->mBodyTemplate:Ljava/lang/String;

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    const-string v0, "TemplateName"

    .line 84
    invoke-static {p1, v0}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->getNodeValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->setName(Ljava/lang/String;)V

    return-void
.end method

.method setBodyTemplate(Ljava/lang/String;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->mBodyTemplate:Ljava/lang/String;

    return-void
.end method

.class public final enum Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;
.super Ljava/lang/Enum;
.source "Registration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/windowsazure/messaging/Registration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RegistrationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

.field public static final enum adm:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

.field public static final enum baidu:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

.field public static final enum fcm:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

.field public static final enum gcm:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum unknown:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 51
    new-instance v0, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    const-string v1, "unknown"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->unknown:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    .line 55
    new-instance v1, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    const-string v3, "gcm"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->gcm:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    .line 57
    new-instance v3, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    const-string v5, "fcm"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->fcm:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    .line 58
    new-instance v5, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    const-string v7, "adm"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->adm:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    .line 59
    new-instance v7, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    const-string v9, "baidu"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->baidu:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    const/4 v9, 0x5

    new-array v9, v9, [Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    aput-object v0, v9, v2

    aput-object v1, v9, v4

    aput-object v3, v9, v6

    aput-object v5, v9, v8

    aput-object v7, v9, v10

    .line 49
    sput-object v9, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->$VALUES:[Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;
    .locals 1

    .line 49
    const-class v0, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    return-object p0
.end method

.method public static values()[Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;
    .locals 1

    .line 49
    sget-object v0, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->$VALUES:[Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    invoke-virtual {v0}, [Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    return-object v0
.end method

.class Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub$1;
.super Ljava/lang/Object;
.source "NotificationHub.java"

# interfaces
.implements Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;


# direct methods
.method constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub$1;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInstallationSaveError(Ljava/lang/Exception;)V
    .locals 2

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unable to save installation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ANH"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

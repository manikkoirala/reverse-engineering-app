.class final Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$2;
.super Lorg/json/JSONObject;
.source "InstallationPutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->getBody(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)Lorg/json/JSONObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$installation:Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

.field final synthetic val$serializedTemplates:Lorg/json/JSONObject;

.field final synthetic val$tagList:Lorg/json/JSONArray;


# direct methods
.method constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;Lorg/json/JSONArray;Lorg/json/JSONObject;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 96
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$2;->val$installation:Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

    iput-object p2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$2;->val$tagList:Lorg/json/JSONArray;

    iput-object p3, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$2;->val$serializedTemplates:Lorg/json/JSONObject;

    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 97
    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->getInstallationId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "installationId"

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$2;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "platform"

    const-string v1, "GCM"

    .line 98
    invoke-virtual {p0, v0, v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$2;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 99
    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->getPushChannel()Ljava/lang/String;

    move-result-object v0

    const-string v1, "pushChannel"

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$2;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "tags"

    .line 100
    invoke-virtual {p0, v0, p2}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$2;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p2, "templates"

    .line 101
    invoke-virtual {p0, p2, p3}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$2;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 102
    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->getUserId()Ljava/lang/String;

    move-result-object p1

    const-string p2, "userId"

    invoke-virtual {p0, p2, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$2;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.class interface abstract Lcom/microsoft/windowsazure/messaging/notificationhubs/Taggable;
.super Ljava/lang/Object;
.source "Taggable.java"


# virtual methods
.method public abstract addTag(Ljava/lang/String;)Z
.end method

.method public abstract addTags(Ljava/util/Collection;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract clearTags()V
.end method

.method public abstract getTags()Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract removeTag(Ljava/lang/String;)Z
.end method

.method public abstract removeTags(Ljava/util/Collection;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation
.end method

.class final Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$3;
.super Ljava/lang/Object;
.source "InstallationPutRequest.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->wrapListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)Lcom/android/volley/Response$Listener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic val$installation:Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

.field final synthetic val$subject:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;


# direct methods
.method constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$3;->val$subject:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;

    iput-object p2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$3;->val$installation:Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 121
    iget-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$3;->val$subject:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;

    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$3;->val$installation:Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

    invoke-interface {p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;->onInstallationSaved(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)V

    return-void
.end method

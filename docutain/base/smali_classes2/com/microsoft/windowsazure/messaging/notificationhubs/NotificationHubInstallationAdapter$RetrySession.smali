.class Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;
.super Ljava/lang/Object;
.source "NotificationHubInstallationAdapter.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RetrySession"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$ErrorListener;",
        "Lcom/android/volley/Response$Listener<",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# instance fields
.field private final mDefaultWaitTime:J

.field private final mInstallation:Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

.field private final mMaxRetries:I

.field private final mOnFailure:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;

.field private final mOnSuccess:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;

.field private mRetry:I

.field final synthetic this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;ILcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    iput p3, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mMaxRetries:I

    .line 136
    iput-object p2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mInstallation:Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

    .line 137
    iput-object p4, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mOnSuccess:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;

    .line 138
    iput-object p5, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mOnFailure:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;

    const/4 p1, 0x0

    .line 139
    iput p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mRetry:I

    const-wide/16 p1, 0x3e8

    .line 140
    iput-wide p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mDefaultWaitTime:J

    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;)V
    .locals 0

    .line 126
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->submit()V

    return-void
.end method

.method private submit()V
    .locals 7

    .line 147
    new-instance v6, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;

    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;

    .line 148
    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->access$100(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;)Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;

    move-result-object v1

    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;

    .line 149
    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->access$200(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mInstallation:Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

    move-object v0, v6

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;Ljava/lang/String;Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    const-string v0, "installationPutRequest"

    .line 153
    invoke-virtual {v6, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->addMarker(Ljava/lang/String;)V

    .line 154
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->access$300()Lcom/android/volley/RetryPolicy;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 155
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;

    monitor-enter v0

    .line 156
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->access$402(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;Ljava/util/concurrent/ScheduledFuture;)Ljava/util/concurrent/ScheduledFuture;

    .line 157
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;

    invoke-static {v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->access$500(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;)Lcom/android/volley/RequestQueue;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 158
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 6

    .line 179
    iget v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mRetry:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mRetry:I

    .line 180
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->isRetriable(Lcom/android/volley/VolleyError;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mRetry:I

    iget v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mMaxRetries:I

    if-le v0, v1, :cond_0

    goto :goto_2

    .line 186
    :cond_0
    iget-object p1, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 190
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->getRetryAfter(Lcom/android/volley/NetworkResponse;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    if-nez p1, :cond_2

    .line 194
    iget-wide v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mDefaultWaitTime:J

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    .line 196
    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->parseRetryAfterValue(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_1

    .line 197
    :cond_3
    iget v0, p1, Lcom/android/volley/NetworkResponse;->statusCode:I

    const/16 v1, 0x1ad

    if-eq v0, v1, :cond_5

    iget p1, p1, Lcom/android/volley/NetworkResponse;->statusCode:I

    const/16 v0, 0x193

    if-ne p1, v0, :cond_4

    goto :goto_0

    .line 200
    :cond_4
    iget-wide v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mDefaultWaitTime:J

    goto :goto_1

    :cond_5
    :goto_0
    const-wide/16 v0, 0x2710

    .line 202
    :goto_1
    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;

    monitor-enter v2

    .line 203
    :try_start_0
    iget-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;

    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->access$600(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    new-instance v4, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession$1;

    invoke-direct {v4, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession$1;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;)V

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v0, v1, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->access$402(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;Ljava/util/concurrent/ScheduledFuture;)Ljava/util/concurrent/ScheduledFuture;

    .line 209
    monitor-exit v2

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 181
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mOnFailure:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;

    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->convertVolleyException(Lcom/android/volley/VolleyError;)Ljava/lang/Exception;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;->onInstallationSaveError(Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0

    .line 126
    check-cast p1, Lorg/json/JSONObject;

    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 1

    .line 168
    iget-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mOnSuccess:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;

    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->mInstallation:Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

    invoke-interface {p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;->onInstallationSaved(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)V

    return-void
.end method

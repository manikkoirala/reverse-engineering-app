.class public Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;
.super Ljava/lang/Object;
.source "NotificationHubInstallationAdapter.java"

# interfaces
.implements Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;
    }
.end annotation


# static fields
.field private static final DEFAULT_INSTALLATION_EXPIRATION_MILLIS:J = 0x1cf7c5800L

.field private static final INSTALLATION_PUT_TAG:Ljava/lang/String; = "installationPutRequest"

.field private static final sDoNotRetry:Lcom/android/volley/RetryPolicy;

.field private static final sRetriableStatusCodes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mConnectionString:Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;

.field private final mHubName:Ljava/lang/String;

.field private final mInstallationExpirationWindow:J

.field private mOutstandingRetry:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture<",
            "*>;"
        }
    .end annotation
.end field

.field private final mRequestQueue:Lcom/android/volley/RequestQueue;

.field private final mScheduler:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 61
    new-instance v0, Lcom/android/volley/DefaultRetryPolicy;

    const/16 v1, 0x3e8

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3}, Lcom/android/volley/DefaultRetryPolicy;-><init>(IIF)V

    sput-object v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->sDoNotRetry:Lcom/android/volley/RetryPolicy;

    .line 62
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->sRetriableStatusCodes:Ljava/util/Set;

    const/16 v1, 0x1f4

    .line 63
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x1f7

    .line 64
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x1f8

    .line 65
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x193

    .line 66
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x198

    .line 67
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x1ad

    .line 68
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const-wide v4, 0x1cf7c5800L

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 50
    invoke-direct/range {v0 .. v5}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->mScheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 54
    iput-object p2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->mHubName:Ljava/lang/String;

    .line 55
    invoke-static {p3}, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->parse(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;

    move-result-object p2

    iput-object p2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->mConnectionString:Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/volley/toolbox/Volley;->newRequestQueue(Landroid/content/Context;)Lcom/android/volley/RequestQueue;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->mRequestQueue:Lcom/android/volley/RequestQueue;

    .line 57
    iput-wide p4, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->mInstallationExpirationWindow:J

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;)Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->mConnectionString:Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;

    return-object p0
.end method

.method static synthetic access$200(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;)Ljava/lang/String;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->mHubName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300()Lcom/android/volley/RetryPolicy;
    .locals 1

    .line 35
    sget-object v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->sDoNotRetry:Lcom/android/volley/RetryPolicy;

    return-object v0
.end method

.method static synthetic access$402(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;Ljava/util/concurrent/ScheduledFuture;)Ljava/util/concurrent/ScheduledFuture;
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->mOutstandingRetry:Ljava/util/concurrent/ScheduledFuture;

    return-object p1
.end method

.method static synthetic access$500(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;)Lcom/android/volley/RequestQueue;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->mRequestQueue:Lcom/android/volley/RequestQueue;

    return-object p0
.end method

.method static synthetic access$600(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->mScheduler:Ljava/util/concurrent/ScheduledExecutorService;

    return-object p0
.end method

.method static convertVolleyException(Lcom/android/volley/VolleyError;)Ljava/lang/Exception;
    .locals 2

    .line 222
    instance-of v0, p0, Lcom/android/volley/AuthFailureError;

    if-eqz v0, :cond_0

    .line 223
    new-instance v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/AuthorizationException;

    check-cast p0, Lcom/android/volley/AuthFailureError;

    invoke-direct {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/AuthorizationException;-><init>(Lcom/android/volley/AuthFailureError;)V

    return-object v0

    .line 224
    :cond_0
    instance-of v0, p0, Lcom/android/volley/ClientError;

    if-eqz v0, :cond_1

    .line 225
    new-instance v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ClientException;

    check-cast p0, Lcom/android/volley/ClientError;

    invoke-direct {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/ClientException;-><init>(Lcom/android/volley/ClientError;)V

    return-object v0

    .line 226
    :cond_1
    instance-of v0, p0, Lcom/android/volley/ServerError;

    if-eqz v0, :cond_2

    .line 227
    new-instance v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ServerException;

    check-cast p0, Lcom/android/volley/ServerError;

    invoke-direct {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/ServerException;-><init>(Lcom/android/volley/ServerError;)V

    return-object v0

    .line 228
    :cond_2
    instance-of v0, p0, Lcom/android/volley/NetworkError;

    if-eqz v0, :cond_3

    .line 229
    new-instance v0, Ljava/io/IOException;

    invoke-virtual {p0}, Lcom/android/volley/VolleyError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/volley/VolleyError;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0

    .line 230
    :cond_3
    instance-of v0, p0, Lcom/android/volley/ParseError;

    if-eqz v0, :cond_4

    .line 231
    new-instance v0, Ljava/io/IOException;

    invoke-virtual {p0}, Lcom/android/volley/VolleyError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/volley/VolleyError;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0

    .line 232
    :cond_4
    instance-of v0, p0, Lcom/android/volley/TimeoutError;

    if-eqz v0, :cond_5

    .line 233
    new-instance v0, Ljava/io/IOException;

    invoke-virtual {p0}, Lcom/android/volley/VolleyError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/volley/VolleyError;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0

    .line 235
    :cond_5
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, p0}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method static getRetryAfter(Lcom/android/volley/NetworkResponse;)Ljava/lang/String;
    .locals 3

    .line 244
    iget-object p0, p0, Lcom/android/volley/NetworkResponse;->allHeaders:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/Header;

    .line 245
    invoke-virtual {v0}, Lcom/android/volley/Header;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Retry-After"

    .line 246
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 247
    invoke-virtual {v0}, Lcom/android/volley/Header;->getValue()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method static isRetriable(Lcom/android/volley/VolleyError;)Z
    .locals 2

    .line 108
    instance-of v0, p0, Lcom/android/volley/NetworkError;

    const/4 v1, 0x1

    if-nez v0, :cond_2

    instance-of v0, p0, Lcom/android/volley/TimeoutError;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    if-eqz v0, :cond_1

    .line 113
    sget-object v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->sRetriableStatusCodes:Ljava/util/Set;

    iget-object p0, p0, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    iget p0, p0, Lcom/android/volley/NetworkResponse;->statusCode:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    return v1

    :cond_1
    const/4 p0, 0x0

    return p0

    :cond_2
    :goto_0
    return v1
.end method

.method static parseRetryAfterValue(Ljava/lang/String;)J
    .locals 4

    const-wide/16 v0, 0x3e8

    .line 264
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    mul-long v2, v2, v0

    return-wide v2

    :catch_0
    move-exception p0

    .line 266
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Retry-After must be communicated as a number of seconds"

    invoke-direct {v0, v1, p0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method


# virtual methods
.method addExpiration(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)V
    .locals 6

    .line 88
    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->getExpiration()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    return-void

    .line 92
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 93
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->mInstallationExpirationWindow:J

    add-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 95
    invoke-virtual {p1, v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->setExpiration(Ljava/util/Date;)V

    return-void
.end method

.method cancelOutstandingUpdates()V
    .locals 2

    .line 99
    monitor-enter p0

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->mOutstandingRetry:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 101
    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->mRequestQueue:Lcom/android/volley/RequestQueue;

    const-string v1, "installationPutRequest"

    invoke-virtual {v0, v1}, Lcom/android/volley/RequestQueue;->cancelAll(Ljava/lang/Object;)V

    .line 104
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public saveInstallation(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V
    .locals 7

    .line 78
    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->addExpiration(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)V

    .line 79
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;->cancelOutstandingUpdates()V

    .line 80
    new-instance v6, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;

    const/4 v3, 0x3

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;ILcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V

    invoke-static {v6}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;->access$000(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter$RetrySession;)V

    return-void
.end method

.class Lcom/microsoft/windowsazure/messaging/notificationhubs/BasicNotificationMessage;
.super Ljava/lang/Object;
.source "BasicNotificationMessage.java"

# interfaces
.implements Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationMessage;


# instance fields
.field private final mData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mMessage:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/BasicNotificationMessage;->mTitle:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/BasicNotificationMessage;->mMessage:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/BasicNotificationMessage;->mData:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public getBody()Ljava/lang/String;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/BasicNotificationMessage;->mMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/BasicNotificationMessage;->mData:Ljava/util/Map;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/BasicNotificationMessage;->mTitle:Ljava/lang/String;

    return-object v0
.end method

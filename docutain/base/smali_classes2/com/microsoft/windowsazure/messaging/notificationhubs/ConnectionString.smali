.class Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;
.super Ljava/lang/Object;
.source "ConnectionString.java"


# static fields
.field private static final ENDPOINT_KEY:Ljava/lang/String; = "Endpoint"

.field private static final PORTAL_FORMAT_PATTERN:Ljava/util/regex/Pattern;

.field private static final SHARED_ACCESS_KEY:Ljava/lang/String; = "SharedAccessKey"

.field private static final SHARED_ACCESS_KEY_NAME_KEY:Ljava/lang/String; = "SharedAccessKeyName"


# instance fields
.field private mEndpoint:Ljava/lang/String;

.field private mSharedAccessKey:Ljava/lang/String;

.field private mSharedAccessKeyName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "([^=]+)=([^;]+);?"

    .line 7
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->PORTAL_FORMAT_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 21
    invoke-static {p2}, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 25
    invoke-static {p3}, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mEndpoint:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mSharedAccessKeyName:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mSharedAccessKey:Ljava/lang/String;

    return-void

    .line 26
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "SharedAccessKey parameter can not be null or empty"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 22
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "SharedAccessKeyName parameter can not be null or empty"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 18
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Endpoint parameter can not be null or empty"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static isNullOrWhiteSpace(Ljava/lang/String;)Z
    .locals 1

    if-eqz p0, :cond_1

    .line 106
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static parse(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;
    .locals 8

    .line 35
    sget-object v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->PORTAL_FORMAT_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    const-string v0, ""

    move-object v1, v0

    move-object v2, v1

    .line 40
    :goto_0
    invoke-virtual {p0}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    .line 41
    invoke-virtual {p0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    const/4 v5, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v6

    const/4 v7, 0x2

    sparse-switch v6, :sswitch_data_0

    :goto_1
    const/4 v3, -0x1

    goto :goto_2

    :sswitch_0
    const-string v3, "Endpoint"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    const/4 v3, 0x2

    goto :goto_2

    :sswitch_1
    const-string v6, "SharedAccessKeyName"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_1

    :sswitch_2
    const-string v3, "SharedAccessKey"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :cond_2
    :goto_2
    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 43
    :pswitch_0
    invoke-virtual {p0, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 46
    :pswitch_1
    invoke-virtual {p0, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 49
    :pswitch_2
    invoke-virtual {p0, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 54
    :cond_3
    new-instance p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;

    invoke-direct {p0, v0, v1, v2}, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p0

    :sswitch_data_0
    .sparse-switch
        -0x2ab5ecaa -> :sswitch_2
        0x30e96701 -> :sswitch_1
        0x6ba181b5 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 72
    instance-of v0, p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 76
    :cond_0
    check-cast p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;

    .line 78
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mSharedAccessKey:Ljava/lang/String;

    iget-object v2, p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mSharedAccessKey:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mSharedAccessKeyName:Ljava/lang/String;

    iget-object v2, p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mSharedAccessKeyName:Ljava/lang/String;

    .line 79
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mEndpoint:Ljava/lang/String;

    iget-object p1, p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mEndpoint:Ljava/lang/String;

    .line 80
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public getEndpoint()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mEndpoint:Ljava/lang/String;

    return-object v0
.end method

.method public getSharedAccessKey()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mSharedAccessKey:Ljava/lang/String;

    return-object v0
.end method

.method public getSharedAccessKeyName()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mSharedAccessKeyName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Endpoint"

    .line 87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3d

    .line 88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 89
    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mEndpoint:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x3b

    .line 90
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v3, "SharedAccessKeyName"

    .line 92
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 94
    iget-object v3, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mSharedAccessKeyName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v3, "SharedAccessKey"

    .line 97
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 99
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->mSharedAccessKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 102
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

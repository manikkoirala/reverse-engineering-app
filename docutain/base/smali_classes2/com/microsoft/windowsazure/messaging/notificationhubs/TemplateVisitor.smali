.class Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;
.super Ljava/lang/Object;
.source "TemplateVisitor.java"

# interfaces
.implements Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;


# static fields
.field private static final PREFERENCE_KEY:Ljava/lang/String; = "templates"


# instance fields
.field private final mPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 31
    sget v0, Lcom/microsoft/windowsazure/messaging/R$string;->installation_enrichment_file_key:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;-><init>(Landroid/content/SharedPreferences;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;-><init>(Landroid/content/Context;)V

    .line 45
    invoke-virtual {p0, p2}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->setTemplates(Ljava/util/Map;)V

    return-void
.end method

.method constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->mPreferences:Landroid/content/SharedPreferences;

    return-void
.end method

.method private static deserializeInstallationTemplateFromJson(Ljava/util/Set;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 171
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 173
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 174
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "name"

    .line 175
    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 176
    invoke-static {v2}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;->deserialize(Lorg/json/JSONObject;)Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;

    move-result-object v2

    .line 177
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private getSharedPreferenceTemplates()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;",
            ">;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->mPreferences:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    const-string v2, "templates"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0

    .line 65
    :cond_0
    :try_start_0
    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->deserializeInstallationTemplateFromJson(Ljava/util/Set;)Ljava/util/Map;

    move-result-object v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 68
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to deserialize installation template"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static serializeInstallationTemplateToJson(Ljava/lang/String;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;)Ljava/lang/String;
    .locals 0

    .line 159
    invoke-static {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;->serialize(Ljava/lang/String;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public clearTemplates()V
    .locals 2

    .line 148
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "templates"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public getTemplate(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;
    .locals 1

    .line 131
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->getSharedPreferenceTemplates()Ljava/util/Map;

    move-result-object v0

    .line 132
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;

    return-object p1
.end method

.method public getTemplates()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Ljava/util/Map$Entry<",
            "Ljava/lang/String;",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;",
            ">;>;"
        }
    .end annotation

    .line 141
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->getSharedPreferenceTemplates()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public removeTemplate(Ljava/lang/String;)Z
    .locals 0

    .line 105
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->removeTemplates(Ljava/util/List;)Z

    move-result p1

    return p1
.end method

.method public removeTemplates(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 115
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->getSharedPreferenceTemplates()Ljava/util/Map;

    move-result-object v0

    .line 116
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 117
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 118
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 119
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;

    invoke-static {v2, v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->serializeInstallationTemplateToJson(Ljava/lang/String;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "templates"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    const/4 p1, 0x1

    return p1
.end method

.method public setTemplate(Ljava/lang/String;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;)V
    .locals 0

    .line 79
    invoke-static {p1, p2}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->setTemplates(Ljava/util/Map;)V

    return-void
.end method

.method public setTemplates(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;",
            ">;)V"
        }
    .end annotation

    .line 89
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->getSharedPreferenceTemplates()Ljava/util/Map;

    move-result-object v0

    .line 90
    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 91
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 92
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 93
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;

    invoke-static {v2, v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->serializeInstallationTemplateToJson(Ljava/lang/String;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "templates"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public visitInstallation(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)V
    .locals 1

    .line 50
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->getSharedPreferenceTemplates()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->setTemplates(Ljava/util/Map;)V

    return-void
.end method

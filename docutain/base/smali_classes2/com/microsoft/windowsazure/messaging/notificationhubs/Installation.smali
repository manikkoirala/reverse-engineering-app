.class public Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;
.super Ljava/lang/Object;
.source "Installation.java"

# interfaces
.implements Lcom/microsoft/windowsazure/messaging/notificationhubs/Taggable;


# instance fields
.field private mExpiration:Ljava/util/Date;

.field private mInstallationId:Ljava/lang/String;

.field private mPushChannel:Ljava/lang/String;

.field private mTags:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTemplates:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;",
            ">;"
        }
    .end annotation
.end field

.field private mUserId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTags:Ljava/util/Set;

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTemplates:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addTag(Ljava/lang/String;)Z
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTags:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public addTags(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTags:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    move-result p1

    return p1
.end method

.method public clearTags()V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTags:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 158
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 159
    :cond_1
    check-cast p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

    .line 160
    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mPushChannel:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mPushChannel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTags:Ljava/util/Set;

    iget-object v3, p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTags:Ljava/util/Set;

    .line 161
    invoke-interface {v2, v3}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTemplates:Ljava/util/Map;

    iget-object v3, p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTemplates:Ljava/util/Map;

    .line 162
    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mInstallationId:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mInstallationId:Ljava/lang/String;

    .line 163
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mUserId:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mUserId:Ljava/lang/String;

    .line 164
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mExpiration:Ljava/util/Date;

    iget-object p1, p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mExpiration:Ljava/util/Date;

    .line 165
    invoke-virtual {v2, p1}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public getExpiration()Ljava/util/Date;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mExpiration:Ljava/util/Date;

    return-object v0
.end method

.method public getInstallationId()Ljava/lang/String;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mInstallationId:Ljava/lang/String;

    return-object v0
.end method

.method public getPushChannel()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mPushChannel:Ljava/lang/String;

    return-object v0
.end method

.method public getTags()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTags:Ljava/util/Set;

    return-object v0
.end method

.method public getTemplates()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;",
            ">;"
        }
    .end annotation

    .line 137
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTemplates:Ljava/util/Map;

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mUserId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 170
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mPushChannel:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTags:Ljava/util/Set;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTemplates:Ljava/util/Map;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mInstallationId:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mUserId:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mExpiration:Ljava/util/Date;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public removeTag(Ljava/lang/String;)Z
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTags:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public removeTags(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTags:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    move-result p1

    return p1
.end method

.method public setExpiration(Ljava/util/Date;)V
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mExpiration:Ljava/util/Date;

    return-void
.end method

.method public setInstallationId(Ljava/lang/String;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mInstallationId:Ljava/lang/String;

    return-void
.end method

.method public setPushChannel(Ljava/lang/String;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mPushChannel:Ljava/lang/String;

    return-void
.end method

.method public setTemplates(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;",
            ">;)V"
        }
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mTemplates:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public setUserId(Ljava/lang/String;)V
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->mUserId:Ljava/lang/String;

    return-void
.end method

.class Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;
.super Ljava/lang/Object;
.source "TagVisitor.java"

# interfaces
.implements Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;
.implements Lcom/microsoft/windowsazure/messaging/notificationhubs/Taggable;


# static fields
.field private static final PREFERENCE_KEY:Ljava/lang/String; = "tags"


# instance fields
.field private mPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 27
    sget v0, Lcom/microsoft/windowsazure/messaging/R$string;->installation_enrichment_file_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;-><init>(Landroid/content/SharedPreferences;)V

    return-void
.end method

.method constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->mPreferences:Landroid/content/SharedPreferences;

    return-void
.end method

.method private getTagsSet()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 35
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->mPreferences:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    const-string v3, "tags"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method


# virtual methods
.method public addTag(Ljava/lang/String;)Z
    .locals 0

    .line 51
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->addTags(Ljava/util/Collection;)Z

    move-result p1

    return p1
.end method

.method public addTags(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 63
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->getTagsSet()Ljava/util/Set;

    move-result-object v0

    .line 64
    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 65
    iget-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v1, "tags"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    const/4 p1, 0x1

    return p1
.end method

.method public clearTags()V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "tags"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public getTags()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 103
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->getTagsSet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public removeTag(Ljava/lang/String;)Z
    .locals 0

    .line 77
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->removeTags(Ljava/util/Collection;)Z

    move-result p1

    return p1
.end method

.method public removeTags(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 88
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->getTagsSet()Ljava/util/Set;

    move-result-object v0

    .line 89
    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 90
    iget-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v1, "tags"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public visitInstallation(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)V
    .locals 1

    .line 40
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->getTagsSet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->addTags(Ljava/util/Collection;)Z

    return-void
.end method

.class public Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubException;
.super Ljava/lang/Exception;
.source "NotificationHubException.java"


# instance fields
.field private final mResponseBody:[B

.field private final mResponseHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mResponseStatusCode:I


# direct methods
.method constructor <init>(Lcom/android/volley/NetworkResponse;)V
    .locals 3

    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Azure Notification Hub request failed with status "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Lcom/android/volley/NetworkResponse;->statusCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p1, Lcom/android/volley/NetworkResponse;->data:[B

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 17
    iget-object v0, p1, Lcom/android/volley/NetworkResponse;->data:[B

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubException;->mResponseBody:[B

    .line 18
    iget v0, p1, Lcom/android/volley/NetworkResponse;->statusCode:I

    iput v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubException;->mResponseStatusCode:I

    .line 19
    iget-object p1, p1, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubException;->mResponseHeaders:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public getResponseBody()Ljava/lang/String;
    .locals 2

    .line 35
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubException;->mResponseBody:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method

.method public getResponseHeaders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubException;->mResponseHeaders:Ljava/util/Map;

    return-object v0
.end method

.method public getStatusCode()I
    .locals 1

    .line 27
    iget v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubException;->mResponseStatusCode:I

    return v0
.end method

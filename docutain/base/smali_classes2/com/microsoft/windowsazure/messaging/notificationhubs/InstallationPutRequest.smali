.class Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;
.super Lcom/android/volley/toolbox/JsonObjectRequest;
.source "InstallationPutRequest.java"


# static fields
.field private static final API_VERSION:Ljava/lang/String; = "2020-06"

.field private static final TOKEN_EXPIRE_SECONDS:J = 0x12cL

.field private static final sIso8601Format:Ljava/text/DateFormat;


# instance fields
.field private final mConnectionString:Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 40
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v2, "yyyy-MM-dd\'T\'HH:mmZ"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->sIso8601Format:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;Ljava/lang/String;Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;",
            "Ljava/lang/String;",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;",
            "Lcom/android/volley/Response$Listener<",
            "Lorg/json/JSONObject;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .line 47
    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->getEndpoint()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->getInstallationId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p2, v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->getInstallationUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 48
    invoke-static {p3}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->getBody(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)Lorg/json/JSONObject;

    move-result-object v5

    const/4 v3, 0x2

    move-object v2, p0

    move-object v6, p4

    move-object v7, p5

    .line 45
    invoke-direct/range {v2 .. v7}, Lcom/android/volley/toolbox/JsonObjectRequest;-><init>(ILjava/lang/String;Lorg/json/JSONObject;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 51
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->mConnectionString:Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;

    return-void
.end method

.method static synthetic access$001(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;)Ljava/lang/String;
    .locals 0

    .line 37
    invoke-super {p0}, Lcom/android/volley/toolbox/JsonObjectRequest;->getUrl()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;)Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->mConnectionString:Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;

    return-object p0
.end method

.method static synthetic access$200(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 37
    invoke-static {p0, p1, p2}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->generateAuthToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .line 37
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static generateAuthToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    const-string v0, "UTF-8"

    .line 162
    :try_start_0
    invoke-static {p0, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :catch_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    const-wide/16 v3, 0x12c

    add-long/2addr v1, v3

    .line 170
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 173
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const/4 v4, 0x0

    :try_start_1
    const-string v5, "HmacSHA256"

    .line 176
    invoke-static {v5}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v4
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    .line 181
    :catch_1
    new-instance v5, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object p2

    invoke-virtual {v4}, Ljavax/crypto/Mac;->getAlgorithm()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 182
    invoke-virtual {v4, v5}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 183
    invoke-virtual {v4, v3}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object p2

    const/4 v3, 0x0

    .line 184
    invoke-static {p2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p2

    .line 185
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    .line 187
    :try_start_2
    invoke-static {p2, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    .line 193
    :catch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SharedAccessSignature sr="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "&sig="

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "&se="

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p0, "&skn="

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getBody(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)Lorg/json/JSONObject;
    .locals 5

    .line 84
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 85
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->getTags()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 86
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 90
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 91
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->getTemplates()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 92
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 93
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;

    invoke-static {v4, v3}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;->serialize(Ljava/lang/String;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 96
    :cond_1
    new-instance v2, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$2;

    invoke-direct {v2, p0, v0, v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$2;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;Lorg/json/JSONArray;Lorg/json/JSONObject;)V

    .line 105
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->getExpiration()Ljava/util/Date;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 107
    sget-object v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->sIso8601Format:Ljava/text/DateFormat;

    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "expirationTime"

    .line 108
    invoke-virtual {v2, v0, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    return-object v2

    :catch_0
    move-exception p0

    .line 113
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, ""

    invoke-direct {v0, v1, p0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method static getInstallationUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sb://"

    .line 149
    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    .line 150
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    :cond_0
    const-string v1, "/"

    .line 152
    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    .line 153
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_1
    const-string v2, "https://"

    .line 155
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/installations/"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "?api-version="

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "2020-06"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static getUserAgent()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "2020-06"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "AndroidSdkV1FcmV1.1.4"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Android"

    aput-object v2, v0, v1

    .line 140
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "NOTIFICATIONHUBS/%s (api-origin=%s; os=%s; os_version=%s;)"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static wrapErrorListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)Lcom/android/volley/Response$ErrorListener;
    .locals 1

    .line 127
    new-instance v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$4;

    invoke-direct {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$4;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V

    return-object v0
.end method

.method public static wrapListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)Lcom/android/volley/Response$Listener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;",
            ")",
            "Lcom/android/volley/Response$Listener<",
            "TT;>;"
        }
    .end annotation

    .line 118
    new-instance v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$3;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$3;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)V

    return-object v0
.end method


# virtual methods
.method public getHeaders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 57
    :try_start_0
    new-instance v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$1;

    invoke-direct {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$1;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 68
    invoke-virtual {v0}, Ljava/security/InvalidKeyException;->printStackTrace()V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation

    .line 76
    iget v0, p1, Lcom/android/volley/NetworkResponse;->statusCode:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 77
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-static {p1}, Lcom/android/volley/toolbox/HttpHeaderParser;->parseCacheHeaders(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Cache$Entry;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/android/volley/Response;->success(Ljava/lang/Object;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;

    move-result-object p1

    return-object p1

    .line 80
    :cond_0
    invoke-super {p0, p1}, Lcom/android/volley/toolbox/JsonObjectRequest;->parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;

    move-result-object p1

    return-object p1
.end method

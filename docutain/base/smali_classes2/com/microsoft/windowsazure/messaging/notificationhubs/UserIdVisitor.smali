.class Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;
.super Ljava/lang/Object;
.source "UserIdVisitor.java"

# interfaces
.implements Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;


# static fields
.field private static final PREFERENCE_KEY:Ljava/lang/String; = "userId"


# instance fields
.field private final mPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    sget v0, Lcom/microsoft/windowsazure/messaging/R$string;->installation_enrichment_file_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;->mPreferences:Landroid/content/SharedPreferences;

    return-void
.end method


# virtual methods
.method public getUserId()Ljava/lang/String;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "userId"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setUserId(Ljava/lang/String;)Z
    .locals 2

    if-eqz p1, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;->getUserId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-nez p1, :cond_2

    .line 42
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;->getUserId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "userId"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    const/4 p1, 0x1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public visitInstallation(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)V
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;->getUserId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->setUserId(Ljava/lang/String;)V

    return-void
.end method

.class Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$1;
.super Ljava/util/HashMap;
.source "InstallationPutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->getHeaders()Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;


# direct methods
.method constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 57
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$1;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    const-string v0, "Content-Type"

    const-string v1, "application/json"

    .line 58
    invoke-virtual {p0, v0, v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "x-ms-version"

    const-string v1, "2020-06"

    .line 59
    invoke-virtual {p0, v0, v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->access$001(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;)Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->access$100(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;)Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->getSharedAccessKeyName()Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->access$100(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;)Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;

    move-result-object p1

    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/ConnectionString;->getSharedAccessKey()Ljava/lang/String;

    move-result-object p1

    .line 60
    invoke-static {v0, v1, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->access$200(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "Authorization"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->access$300()Ljava/lang/String;

    move-result-object p1

    const-string v0, "User-Agent"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

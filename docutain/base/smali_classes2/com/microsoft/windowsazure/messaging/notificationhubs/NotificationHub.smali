.class public final Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;
.super Ljava/lang/Object;
.source "NotificationHub.java"


# static fields
.field private static final IS_ENABLED_PREFERENCE_KEY:Ljava/lang/String; = "isEnabled"

.field private static sInstance:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;


# instance fields
.field private mAdapter:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;

.field private mApplication:Landroid/app/Application;

.field private mIdAssignmentVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;

.field private mListener:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationListener;

.field private mOnInstallationFailure:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;

.field private mOnSavedInstallation:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;

.field private mPreferences:Landroid/content/SharedPreferences;

.field private mPushChannelVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelVisitor;

.field private mTagVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;

.field private mTemplateVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;

.field private mUserIdVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;

.field private final mVisitors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mVisitors:Ljava/util/List;

    .line 45
    new-instance v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub$1;

    invoke-direct {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub$1;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;)V

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mOnInstallationFailure:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;

    .line 51
    new-instance v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub$2;

    invoke-direct {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub$2;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;)V

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mOnSavedInstallation:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;

    return-void
.end method

.method public static addTag(Ljava/lang/String;)Z
    .locals 1

    .line 323
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->addInstanceTag(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static addTags(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 348
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->addInstanceTags(Ljava/util/Collection;)Z

    move-result p0

    return p0
.end method

.method public static beginInstallationUpdate()V
    .locals 1

    .line 228
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->beginInstanceInstallationUpdate()V

    return-void
.end method

.method static checkLaunchedFromNotification(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method public static clearTags()V
    .locals 1

    .line 436
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->clearInstanceTags()V

    return-void
.end method

.method public static getInstallationId()Ljava/lang/String;
    .locals 1

    .line 284
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstanceInstallationId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;
    .locals 2

    const-class v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    monitor-enter v0

    .line 65
    :try_start_0
    sget-object v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->sInstance:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    if-nez v1, :cond_0

    .line 66
    new-instance v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    invoke-direct {v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;-><init>()V

    sput-object v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->sInstance:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    .line 68
    :cond_0
    sget-object v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->sInstance:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getPushChannel()Ljava/lang/String;
    .locals 1

    .line 259
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstancePushChannel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTags()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 420
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstanceTags()Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public static getTemplate(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;
    .locals 1

    .line 515
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstanceTemplate(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;

    move-result-object p0

    return-object p0
.end method

.method public static getUserId()Ljava/lang/String;
    .locals 1

    .line 552
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstanceUserId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isEnabled()Z
    .locals 1

    .line 469
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->isInstanceEnabled()Z

    move-result v0

    return v0
.end method

.method public static removeTag(Ljava/lang/String;)Z
    .locals 1

    .line 373
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->removeInstanceTag(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static removeTags(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 397
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->removeInstanceTags(Ljava/util/Collection;)Z

    move-result p0

    return p0
.end method

.method public static removeTemplate(Ljava/lang/String;)Z
    .locals 1

    .line 498
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->removeInstanceTemplate(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static setEnabled(Z)V
    .locals 1

    .line 454
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->setInstanceEnabled(Z)V

    return-void
.end method

.method public static setInstallationId(Ljava/lang/String;)V
    .locals 1

    .line 300
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->setInstanceInstallationId(Ljava/lang/String;)V

    return-void
.end method

.method public static setInstallationSaveFailureListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V
    .locals 1

    .line 181
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->setInstanceInstallationSaveFailureListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V

    return-void
.end method

.method public static setInstallationSavedListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;)V
    .locals 1

    .line 173
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->setInstanceInstallationSavedListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;)V

    return-void
.end method

.method public static setListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationListener;)V
    .locals 1

    .line 156
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->setInstanceListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationListener;)V

    return-void
.end method

.method static setPushChannel(Ljava/lang/String;)V
    .locals 1

    .line 250
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->setInstancePushChannel(Ljava/lang/String;)V

    return-void
.end method

.method public static setTemplate(Ljava/lang/String;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;)V
    .locals 1

    .line 483
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->setInstanceTemplate(Ljava/lang/String;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;)V

    return-void
.end method

.method public static setUserId(Ljava/lang/String;)Z
    .locals 1

    .line 529
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->setInstanceUserId(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static start(Landroid/app/Application;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;)V
    .locals 2

    .line 129
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    .line 130
    iput-object p1, v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mAdapter:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;

    .line 132
    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->registerApplication(Landroid/app/Application;)V

    .line 138
    new-instance p1, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {p1, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    .line 139
    invoke-virtual {p1, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 140
    invoke-static {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->getSharedInstance(Landroid/content/Context;)Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;

    move-result-object p0

    new-instance p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub$3;

    invoke-direct {p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub$3;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;)V

    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->addListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$Listener;)V

    return-void
.end method

.method public static start(Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 108
    new-instance v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubInstallationAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    new-instance p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;

    invoke-direct {p1, p0, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;)V

    .line 113
    new-instance p2, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;

    invoke-direct {p2, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;)V

    .line 115
    invoke-static {p0, p2}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->start(Landroid/app/Application;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;)V

    return-void
.end method

.method public static useVisitor(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;)V
    .locals 1

    .line 208
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->useInstanceVisitor(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;)V

    return-void
.end method


# virtual methods
.method public addInstanceTag(Ljava/lang/String;)Z
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mTagVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;

    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->addTag(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 334
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->beginInstanceInstallationUpdate()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public addInstanceTags(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 359
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mTagVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;

    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->addTags(Ljava/util/Collection;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 360
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->beginInstanceInstallationUpdate()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public beginInstanceInstallationUpdate()V
    .locals 4

    .line 235
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->isInstanceEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 239
    :cond_0
    new-instance v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

    invoke-direct {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;-><init>()V

    .line 240
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mVisitors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;

    .line 241
    invoke-interface {v2, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;->visitInstallation(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)V

    goto :goto_0

    .line 244
    :cond_1
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mAdapter:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;

    if-eqz v1, :cond_2

    .line 245
    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mOnSavedInstallation:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;

    iget-object v3, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mOnInstallationFailure:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;

    invoke-interface {v1, v0, v2, v3}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;->saveInstallation(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V

    :cond_2
    return-void
.end method

.method public clearInstanceTags()V
    .locals 1

    .line 443
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mTagVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->getTags()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mTagVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->clearTags()V

    .line 445
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->beginInstanceInstallationUpdate()V

    :cond_0
    return-void
.end method

.method public getInstanceInstallationId()Ljava/lang/String;
    .locals 1

    .line 292
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mIdAssignmentVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;->getInstallationId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getInstanceListener()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationListener;
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mListener:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationListener;

    return-object v0
.end method

.method public getInstancePushChannel()Ljava/lang/String;
    .locals 1

    .line 276
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mPushChannelVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelVisitor;

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelVisitor;->getPushChannel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInstanceTags()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 429
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mTagVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->getTags()Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public getInstanceTemplate(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;
    .locals 1

    .line 519
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mTemplateVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;

    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->getTemplate(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;

    move-result-object p1

    return-object p1
.end method

.method public getInstanceUserId()Ljava/lang/String;
    .locals 1

    .line 561
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mUserIdVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;->getUserId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isInstanceEnabled()Z
    .locals 3

    .line 473
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "isEnabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method declared-synchronized registerApplication(Landroid/app/Application;)V
    .locals 2

    monitor-enter p0

    .line 72
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mApplication:Landroid/app/Application;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_0

    .line 73
    monitor-exit p0

    return-void

    .line 76
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mApplication:Landroid/app/Application;

    .line 78
    sget v0, Lcom/microsoft/windowsazure/messaging/R$string;->installation_enrichment_file_key:I

    invoke-virtual {p1, v0}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mPreferences:Landroid/content/SharedPreferences;

    .line 80
    new-instance p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;

    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mApplication:Landroid/app/Application;

    invoke-direct {p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mIdAssignmentVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;

    .line 81
    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->useInstanceVisitor(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;)V

    .line 83
    new-instance p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;

    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mApplication:Landroid/app/Application;

    invoke-direct {p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mTagVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;

    .line 84
    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->useInstanceVisitor(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;)V

    .line 86
    new-instance p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;

    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mApplication:Landroid/app/Application;

    invoke-direct {p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mTemplateVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;

    .line 87
    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->useInstanceVisitor(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;)V

    .line 89
    new-instance p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelVisitor;

    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mApplication:Landroid/app/Application;

    invoke-direct {p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelVisitor;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mPushChannelVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelVisitor;

    .line 90
    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->useInstanceVisitor(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;)V

    .line 92
    new-instance p1, Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;

    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mApplication:Landroid/app/Application;

    invoke-direct {p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mUserIdVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;

    .line 93
    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->useInstanceVisitor(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;)V

    .line 95
    invoke-static {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubExtension;->fetchPushChannel(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public removeInstanceTag(Ljava/lang/String;)Z
    .locals 1

    .line 383
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mTagVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;

    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->removeTag(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 384
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->beginInstanceInstallationUpdate()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public removeInstanceTags(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 407
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mTagVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;

    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TagVisitor;->removeTags(Ljava/util/Collection;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 408
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->beginInstanceInstallationUpdate()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public removeInstanceTemplate(Ljava/lang/String;)Z
    .locals 1

    .line 502
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mTemplateVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;

    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->removeTemplate(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 503
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->beginInstanceInstallationUpdate()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public setInstanceEnabled(Z)V
    .locals 2

    .line 462
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "isEnabled"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    if-eqz p1, :cond_0

    .line 464
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->beginInstanceInstallationUpdate()V

    :cond_0
    return-void
.end method

.method public setInstanceInstallationId(Ljava/lang/String;)V
    .locals 1

    .line 308
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mIdAssignmentVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;->getInstallationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mIdAssignmentVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;

    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;->setInstallationId(Ljava/lang/String;)V

    .line 313
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->beginInstanceInstallationUpdate()V

    return-void
.end method

.method public setInstanceInstallationSaveFailureListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mOnInstallationFailure:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;

    return-void
.end method

.method public setInstanceInstallationSavedListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;)V
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mOnSavedInstallation:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;

    return-void
.end method

.method public setInstanceListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationListener;)V
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mListener:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationListener;

    return-void
.end method

.method setInstancePushChannel(Ljava/lang/String;)V
    .locals 1

    .line 263
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mPushChannelVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelVisitor;

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelVisitor;->getPushChannel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mPushChannelVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelVisitor;

    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelVisitor;->setPushChannel(Ljava/lang/String;)V

    .line 267
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->beginInstanceInstallationUpdate()V

    return-void
.end method

.method public setInstanceTemplate(Ljava/lang/String;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;)V
    .locals 1

    .line 487
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mTemplateVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/windowsazure/messaging/notificationhubs/TemplateVisitor;->setTemplate(Ljava/lang/String;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationTemplate;)V

    .line 488
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->beginInstanceInstallationUpdate()V

    return-void
.end method

.method public setInstanceUserId(Ljava/lang/String;)Z
    .locals 1

    .line 539
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mUserIdVisitor:Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;

    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/UserIdVisitor;->setUserId(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 540
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->beginInstanceInstallationUpdate()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public useInstanceVisitor(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;)V
    .locals 1

    .line 221
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->mVisitors:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.class Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;
.super Ljava/lang/Object;
.source "IdAssignmentVisitor.java"

# interfaces
.implements Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationVisitor;


# static fields
.field private static final PREFERENCE_KEY:Ljava/lang/String; = "installationId"


# instance fields
.field private final mPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    sget v0, Lcom/microsoft/windowsazure/messaging/R$string;->installation_enrichment_file_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;->mPreferences:Landroid/content/SharedPreferences;

    return-void
.end method


# virtual methods
.method public getInstallationId()Ljava/lang/String;
    .locals 3

    .line 46
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "installationId"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setInstallationId(Ljava/lang/String;)V
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "installationId"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public visitInstallation(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)V
    .locals 1

    .line 31
    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->getInstallationId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 32
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;->getInstallationId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 34
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 35
    invoke-virtual {p0, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/IdAssignmentVisitor;->setInstallationId(Ljava/lang/String;)V

    .line 37
    :cond_0
    invoke-virtual {p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->setInstallationId(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.class Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;
.super Ljava/lang/Object;
.source "NetworkStateHelper.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$ConnectivityReceiver;,
        Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$Listener;
    }
.end annotation


# static fields
.field private static sSharedInstance:Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;


# instance fields
.field private final mConnected:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mConnectivityReceiver:Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$ConnectivityReceiver;

.field private final mContext:Landroid/content/Context;

.field private final mListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mListeners:Ljava/util/Set;

    .line 72
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnected:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 81
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mContext:Landroid/content/Context;

    const-string v0, "connectivity"

    .line 82
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/ConnectivityManager;

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 83
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->reopen()V

    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;Landroid/net/Network;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->onNetworkAvailable(Landroid/net/Network;)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;Landroid/net/Network;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->onNetworkLost(Landroid/net/Network;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->handleNetworkStateUpdate()V

    return-void
.end method

.method private getOldIntentFilter()Landroid/content/IntentFilter;
    .locals 2

    .line 153
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static declared-synchronized getSharedInstance(Landroid/content/Context;)Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;
    .locals 2

    const-class v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;

    monitor-enter v0

    .line 97
    :try_start_0
    sget-object v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->sSharedInstance:Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;

    if-nez v1, :cond_0

    .line 98
    new-instance v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;

    invoke-direct {v1, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->sSharedInstance:Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;

    .line 100
    :cond_0
    sget-object p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->sSharedInstance:Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private handleNetworkStateUpdate()V
    .locals 3

    .line 227
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->isAnyNetworkConnected()Z

    move-result v0

    .line 228
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnected:Ljava/util/concurrent/atomic/AtomicBoolean;

    xor-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 229
    invoke-direct {p0, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->notifyNetworkStateUpdated(Z)V

    :cond_0
    return-void
.end method

.method private isAnyNetworkConnected()Z
    .locals 7

    .line 171
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x15

    if-lt v0, v3, :cond_2

    .line 172
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getAllNetworks()[Landroid/net/Network;

    move-result-object v0

    if-nez v0, :cond_0

    return v2

    .line 176
    :cond_0
    array-length v3, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_5

    aget-object v5, v0, v4

    .line 177
    iget-object v6, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v6, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(Landroid/net/Network;)Landroid/net/NetworkInfo;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 178
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_1

    return v1

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 185
    :cond_2
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v0

    if-nez v0, :cond_3

    return v2

    .line 189
    :cond_3
    array-length v3, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v3, :cond_5

    aget-object v5, v0, v4

    if-eqz v5, :cond_4

    .line 190
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_4

    return v1

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_5
    return v2
.end method

.method private notifyNetworkStateUpdated(Z)V
    .locals 2

    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Network has been "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_0

    const-string v1, "connected."

    goto :goto_0

    :cond_0
    const-string v1, "disconnected."

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ANH"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$Listener;

    .line 241
    invoke-interface {v1, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$Listener;->onNetworkStateUpdated(Z)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private onNetworkAvailable(Landroid/net/Network;)V
    .locals 2

    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Network "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " is available."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ANH"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnected:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 205
    invoke-direct {p0, v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->notifyNetworkStateUpdated(Z)V

    :cond_0
    return-void
.end method

.method private onNetworkLost(Landroid/net/Network;)V
    .locals 4

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Network "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " is lost."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ANH"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getAllNetworks()[Landroid/net/Network;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 216
    array-length v3, v0

    if-eqz v3, :cond_1

    new-array v3, v1, [Landroid/net/Network;

    aput-object p1, v3, v2

    .line 217
    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-eqz p1, :cond_2

    .line 218
    iget-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnected:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 219
    invoke-direct {p0, v2}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->notifyNetworkStateUpdated(Z)V

    :cond_2
    return-void
.end method

.method public static declared-synchronized unsetInstance()V
    .locals 2

    const-class v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;

    monitor-enter v0

    const/4 v1, 0x0

    .line 87
    :try_start_0
    sput-object v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->sSharedInstance:Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public addListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$Listener;)V
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public close()V
    .locals 2

    .line 247
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnected:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 248
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 249
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    goto :goto_0

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnectivityReceiver:Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$ConnectivityReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :goto_0
    return-void
.end method

.method public isNetworkConnected()Z
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnected:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->isAnyNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public removeListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$Listener;)V
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public reopen()V
    .locals 3

    .line 108
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 117
    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    const/16 v1, 0xc

    .line 118
    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    .line 119
    new-instance v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$1;

    invoke-direct {v1, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$1;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;)V

    iput-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 131
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v1, v0, v2}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    goto :goto_0

    .line 133
    :cond_0
    new-instance v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$ConnectivityReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$ConnectivityReceiver;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$1;)V

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnectivityReceiver:Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper$ConnectivityReceiver;

    .line 134
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->getOldIntentFilter()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 135
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->handleNetworkStateUpdate()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "ANH"

    const-string v2, "Cannot access network state information."

    .line 143
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 146
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NetworkStateHelper;->mConnected:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :goto_0
    return-void
.end method

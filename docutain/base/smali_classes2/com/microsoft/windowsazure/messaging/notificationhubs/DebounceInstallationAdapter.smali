.class public Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;
.super Ljava/lang/Object;
.source "DebounceInstallationAdapter.java"

# interfaces
.implements Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;


# static fields
.field private static final DEFAULT_DEBOUNCE_INTERVAL:J = 0x7d0L

.field private static final DEFAULT_INSTALLATION_STALE_MILLIS:J = 0x5265c00L

.field static final LAST_ACCEPTED_HASH_KEY:Ljava/lang/String; = "lastAcceptedHash"

.field static final LAST_ACCEPTED_TIMESTAMP_KEY:Ljava/lang/String; = "lastAcceptedTimestamp"


# instance fields
.field private mInstallationAdapter:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;

.field private mInstallationStaleMillis:J

.field private mInterval:J

.field private mPreferences:Landroid/content/SharedPreferences;

.field private mSchedFuture:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture<",
            "*>;"
        }
    .end annotation
.end field

.field private final mScheduler:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;)V
    .locals 2

    const-wide/16 v0, 0x7d0

    .line 41
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;J)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;J)V
    .locals 2

    .line 56
    sget v0, Lcom/microsoft/windowsazure/messaging/R$string;->installation_enrichment_file_key:I

    .line 59
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 56
    invoke-direct {p0, p2, p3, p4, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;JLandroid/content/SharedPreferences;)V

    return-void
.end method

.method constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;JLandroid/content/SharedPreferences;)V
    .locals 1

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 25
    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mScheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 65
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mInstallationAdapter:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;

    .line 66
    iput-wide p2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mInterval:J

    const-wide/32 p1, 0x5265c00

    .line 67
    iput-wide p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mInstallationStaleMillis:J

    .line 68
    iput-object p4, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mPreferences:Landroid/content/SharedPreferences;

    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;)Landroid/content/SharedPreferences;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mPreferences:Landroid/content/SharedPreferences;

    return-object p0
.end method

.method static synthetic access$100(Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;)Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mInstallationAdapter:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;

    return-object p0
.end method

.method private getLastAcceptedHash()I
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "lastAcceptedHash"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getLastAcceptedTimestamp()J
    .locals 4

    .line 118
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "lastAcceptedTimestamp"

    const-wide/high16 v2, -0x8000000000000000L

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public declared-synchronized saveInstallation(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V
    .locals 10

    monitor-enter p0

    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mSchedFuture:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mSchedFuture:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 86
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->hashCode()I

    move-result v2

    .line 87
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->getLastAcceptedHash()I

    move-result v0

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 90
    :goto_0
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 91
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->getLastAcceptedTimestamp()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mInstallationStaleMillis:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-long/2addr v6, v8

    cmp-long v8, v4, v6

    if-gez v8, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 94
    monitor-exit p0

    return-void

    .line 97
    :cond_3
    :try_start_1
    iget-object v8, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mScheduler:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v9, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;

    move-object v0, v9

    move-object v1, p0

    move-wide v3, v4

    move-object v5, p2

    move-object v6, p1

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;IJLcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V

    iget-wide p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mInterval:J

    sget-object p3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v8, v9, p1, p2, p3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mSchedFuture:Ljava/util/concurrent/ScheduledFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method setInstallationStaleWindow(J)V
    .locals 0

    .line 77
    iput-wide p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->mInstallationStaleMillis:J

    return-void
.end method

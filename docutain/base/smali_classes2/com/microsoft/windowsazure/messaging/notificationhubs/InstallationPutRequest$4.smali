.class final Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$4;
.super Ljava/lang/Object;
.source "InstallationPutRequest.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest;->wrapErrorListener(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)Lcom/android/volley/Response$ErrorListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$errorListener:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;


# direct methods
.method constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$4;->val$errorListener:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationPutRequest$4;->val$errorListener:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;

    invoke-interface {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;->onInstallationSaveError(Ljava/lang/Exception;)V

    return-void
.end method

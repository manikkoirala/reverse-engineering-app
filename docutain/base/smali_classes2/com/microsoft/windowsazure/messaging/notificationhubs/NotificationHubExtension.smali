.class Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubExtension;
.super Ljava/lang/Object;
.source "NotificationHubExtension.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fetchPushChannel(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;)V
    .locals 2

    .line 33
    invoke-static {}, Lcom/google/firebase/messaging/FirebaseMessaging;->getInstance()Lcom/google/firebase/messaging/FirebaseMessaging;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/messaging/FirebaseMessaging;->getToken()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubExtension$1;

    invoke-direct {v1, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubExtension$1;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

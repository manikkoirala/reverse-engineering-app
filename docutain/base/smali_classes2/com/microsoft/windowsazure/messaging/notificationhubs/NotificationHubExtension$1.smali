.class final Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubExtension$1;
.super Ljava/lang/Object;
.source "NotificationHubExtension.java"

# interfaces
.implements Lcom/google/android/gms/tasks/OnCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubExtension;->fetchPushChannel(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/tasks/OnCompleteListener<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$hub:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;


# direct methods
.method constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubExtension$1;->val$hub:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/google/android/gms/tasks/Task;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->isSuccessful()Z

    move-result v0

    if-nez v0, :cond_0

    const-string p1, "ANH"

    const-string v0, "unable to fetch FirebaseInstanceId"

    .line 37
    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHubExtension$1;->val$hub:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->setInstancePushChannel(Ljava/lang/String;)V

    return-void
.end method

.class Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;
.super Ljava/lang/Object;
.source "DebounceInstallationAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->saveInstallation(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;

.field final synthetic val$currentHash:I

.field final synthetic val$currentTime:J

.field final synthetic val$installation:Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

.field final synthetic val$onInstallationSaveError:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;

.field final synthetic val$onInstallationSaved:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;


# direct methods
.method constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;IJLcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;

    iput p2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->val$currentHash:I

    iput-wide p3, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->val$currentTime:J

    iput-object p5, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->val$onInstallationSaved:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;

    iput-object p6, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->val$installation:Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

    iput-object p7, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->val$onInstallationSaveError:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 101
    :try_start_0
    new-instance v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1$1;

    invoke-direct {v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1$1;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;)V

    .line 109
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;

    invoke-static {v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->access$100(Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;)Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->val$installation:Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;

    iget-object v3, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->val$onInstallationSaveError:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;

    invoke-interface {v1, v2, v0, v3}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;->saveInstallation(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 111
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->val$onInstallationSaveError:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;

    invoke-interface {v1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;->onInstallationSaveError(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method

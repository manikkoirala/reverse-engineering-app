.class public final Lcom/microsoft/windowsazure/messaging/notificationhubs/FirebaseReceiver;
.super Lcom/google/firebase/messaging/FirebaseMessagingService;
.source "FirebaseReceiver.java"


# instance fields
.field private final mHub:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 24
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstance()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/FirebaseReceiver;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;)V

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/google/firebase/messaging/FirebaseMessagingService;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/FirebaseReceiver;->mHub:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    return-void
.end method

.method static getNotificationMessage(Lcom/google/firebase/messaging/RemoteMessage;)Lcom/microsoft/windowsazure/messaging/notificationhubs/BasicNotificationMessage;
    .locals 3

    .line 71
    invoke-virtual {p0}, Lcom/google/firebase/messaging/RemoteMessage;->getNotification()Lcom/google/firebase/messaging/RemoteMessage$Notification;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Lcom/google/firebase/messaging/RemoteMessage$Notification;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 76
    invoke-virtual {v0}, Lcom/google/firebase/messaging/RemoteMessage$Notification;->getBody()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 78
    :goto_0
    new-instance v2, Lcom/microsoft/windowsazure/messaging/notificationhubs/BasicNotificationMessage;

    .line 81
    invoke-virtual {p0}, Lcom/google/firebase/messaging/RemoteMessage;->getData()Ljava/util/Map;

    move-result-object p0

    invoke-direct {v2, v1, v0, p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/BasicNotificationMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-object v2
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    .line 41
    invoke-super {p0}, Lcom/google/firebase/messaging/FirebaseMessagingService;->onCreate()V

    .line 43
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/FirebaseReceiver;->mHub:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/FirebaseReceiver;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->registerApplication(Landroid/app/Application;)V

    return-void
.end method

.method public onMessageReceived(Lcom/google/firebase/messaging/RemoteMessage;)V
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/FirebaseReceiver;->mHub:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    invoke-virtual {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->getInstanceListener()Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationListener;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/FirebaseReceiver;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/FirebaseReceiver;->getNotificationMessage(Lcom/google/firebase/messaging/RemoteMessage;)Lcom/microsoft/windowsazure/messaging/notificationhubs/BasicNotificationMessage;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationListener;->onPushNotificationReceived(Landroid/content/Context;Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationMessage;)V

    return-void
.end method

.method public onNewToken(Ljava/lang/String;)V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/FirebaseReceiver;->mHub:Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;

    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/NotificationHub;->setInstancePushChannel(Ljava/lang/String;)V

    return-void
.end method

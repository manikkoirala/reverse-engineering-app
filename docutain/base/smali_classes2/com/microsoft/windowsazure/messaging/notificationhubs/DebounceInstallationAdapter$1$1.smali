.class Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1$1;
.super Ljava/lang/Object;
.source "DebounceInstallationAdapter.java"

# interfaces
.implements Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;


# direct methods
.method constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1$1;->this$1:Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInstallationSaved(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)V
    .locals 4

    .line 104
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1$1;->this$1:Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;

    iget-object v0, v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;

    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->access$000(Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1$1;->this$1:Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;

    iget v1, v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->val$currentHash:I

    const-string v2, "lastAcceptedHash"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 105
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1$1;->this$1:Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;

    iget-object v0, v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->this$0:Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;

    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;->access$000(Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1$1;->this$1:Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;

    iget-wide v1, v1, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->val$currentTime:J

    const-string v3, "lastAcceptedTimestamp"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 106
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1$1;->this$1:Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;

    iget-object v0, v0, Lcom/microsoft/windowsazure/messaging/notificationhubs/DebounceInstallationAdapter$1;->val$onInstallationSaved:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;

    invoke-interface {v0, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;->onInstallationSaved(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;)V

    return-void
.end method

.class public Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;
.super Ljava/lang/Object;
.source "PushChannelValidationAdapter.java"

# interfaces
.implements Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;


# static fields
.field public static final DEFAULT_MAX_RETRIES:I = 0x14


# instance fields
.field private final mDecoratedAdapter:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;

.field private final mMaxRetries:I

.field private mRetryCount:I


# direct methods
.method public constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;)V
    .locals 1

    const/16 v0, 0x14

    .line 23
    invoke-direct {p0, p1, v0}, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;-><init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;I)V

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;I)V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;->mMaxRetries:I

    const/4 p2, 0x0

    .line 35
    iput p2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;->mRetryCount:I

    .line 36
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;->mDecoratedAdapter:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;

    return-void
.end method


# virtual methods
.method public saveInstallation(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V
    .locals 2

    .line 48
    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;->getPushChannel()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 49
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 61
    :cond_0
    iput v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;->mRetryCount:I

    .line 62
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;->mDecoratedAdapter:Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;

    invoke-interface {v0, p1, p2, p3}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter;->saveInstallation(Lcom/microsoft/windowsazure/messaging/notificationhubs/Installation;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$Listener;Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;)V

    return-void

    .line 51
    :cond_1
    :goto_0
    iget p1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;->mRetryCount:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;->mRetryCount:I

    iget p2, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;->mMaxRetries:I

    if-lt p1, p2, :cond_2

    .line 55
    iput v1, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;->mRetryCount:I

    .line 56
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "After "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/microsoft/windowsazure/messaging/notificationhubs/PushChannelValidationAdapter;->mRetryCount:I

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " retry attempts, Installation does not have a PushChannel."

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, p1}, Lcom/microsoft/windowsazure/messaging/notificationhubs/InstallationAdapter$ErrorListener;->onInstallationSaveError(Ljava/lang/Exception;)V

    :cond_2
    return-void
.end method

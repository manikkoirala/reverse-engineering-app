.class public Lcom/microsoft/windowsazure/messaging/ConnectionString;
.super Ljava/lang/Object;
.source "ConnectionString.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createUsingSharedAccessKey(Ljava/net/URI;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_2

    .line 43
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 47
    invoke-static {p2}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 51
    invoke-virtual {p0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v0, v1

    const/4 p0, 0x1

    aput-object p1, v0, p0

    const/4 p0, 0x2

    aput-object p2, v0, p0

    const-string p0, "Endpoint=%s;SharedAccessKeyName=%s;SharedAccessKey=%s"

    invoke-static {p0, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 48
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "accessSecret"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 44
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "keyName"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 40
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "endPoint"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static createUsingSharedAccessKeyWithFullAccess(Ljava/net/URI;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 61
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DefaultFullSharedAccessSignature"

    .line 65
    invoke-static {p0, v0, p1}, Lcom/microsoft/windowsazure/messaging/ConnectionString;->createUsingSharedAccessKey(Ljava/net/URI;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 62
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "fullAccessSecret"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static createUsingSharedAccessKeyWithListenAccess(Ljava/net/URI;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 75
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DefaultListenSharedAccessSignature"

    .line 79
    invoke-static {p0, v0, p1}, Lcom/microsoft/windowsazure/messaging/ConnectionString;->createUsingSharedAccessKey(Ljava/net/URI;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 76
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "listenAccessSecret"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.class public Lcom/microsoft/windowsazure/messaging/AdmTemplateRegistration;
.super Lcom/microsoft/windowsazure/messaging/TemplateRegistration;
.source "AdmTemplateRegistration.java"


# static fields
.field private static final ADM_HANDLE_NODE:Ljava/lang/String; = "AdmRegistrationId"

.field static final ADM_TEMPLATE_REGISTRATION_CUSTOM_NODE:Ljava/lang/String; = "AdmTemplateRegistrationDescription"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;-><init>(Ljava/lang/String;)V

    .line 47
    sget-object p1, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->adm:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/AdmTemplateRegistration;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    return-void
.end method


# virtual methods
.method protected appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
    .locals 2

    .line 57
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/AdmTemplateRegistration;->getPNSHandle()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdmRegistrationId"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/microsoft/windowsazure/messaging/AdmTemplateRegistration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-super {p0, p1, p2}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V

    return-void
.end method

.method protected getSpecificPayloadNodeName()Ljava/lang/String;
    .locals 1

    const-string v0, "AdmTemplateRegistrationDescription"

    return-object v0
.end method

.method protected loadCustomXmlData(Lorg/w3c/dom/Element;)V
    .locals 1

    const-string v0, "AdmRegistrationId"

    .line 63
    invoke-static {p1, v0}, Lcom/microsoft/windowsazure/messaging/AdmTemplateRegistration;->getNodeValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/windowsazure/messaging/AdmTemplateRegistration;->setPNSHandle(Ljava/lang/String;)V

    .line 64
    invoke-super {p0, p1}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->loadCustomXmlData(Lorg/w3c/dom/Element;)V

    return-void
.end method

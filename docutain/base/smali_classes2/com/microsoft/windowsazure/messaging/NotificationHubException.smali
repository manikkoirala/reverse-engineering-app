.class public Lcom/microsoft/windowsazure/messaging/NotificationHubException;
.super Ljava/lang/Exception;
.source "NotificationHubException.java"


# static fields
.field private static final serialVersionUID:J = -0x218cae631cbcfe7eL


# instance fields
.field private mStatusCode:I


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 38
    iput p2, p0, Lcom/microsoft/windowsazure/messaging/NotificationHubException;->mStatusCode:I

    return-void
.end method


# virtual methods
.method public getStatusCode()I
    .locals 1

    .line 46
    iget v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHubException;->mStatusCode:I

    return v0
.end method

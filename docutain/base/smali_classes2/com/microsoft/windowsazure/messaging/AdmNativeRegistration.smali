.class public Lcom/microsoft/windowsazure/messaging/AdmNativeRegistration;
.super Lcom/microsoft/windowsazure/messaging/Registration;
.source "AdmNativeRegistration.java"


# static fields
.field static final ADM_HANDLE_NODE:Ljava/lang/String; = "AdmRegistrationId"

.field private static final ADM_NATIVE_REGISTRATION_CUSTOM_NODE:Ljava/lang/String; = "AdmRegistrationDescription"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/Registration;-><init>(Ljava/lang/String;)V

    .line 47
    sget-object p1, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->adm:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/AdmNativeRegistration;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    return-void
.end method


# virtual methods
.method protected appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
    .locals 2

    .line 57
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/AdmNativeRegistration;->getPNSHandle()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdmRegistrationId"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/microsoft/windowsazure/messaging/AdmNativeRegistration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected getSpecificPayloadNodeName()Ljava/lang/String;
    .locals 1

    const-string v0, "AdmRegistrationDescription"

    return-object v0
.end method

.method protected loadCustomXmlData(Lorg/w3c/dom/Element;)V
    .locals 1

    const-string v0, "AdmRegistrationId"

    .line 62
    invoke-static {p1, v0}, Lcom/microsoft/windowsazure/messaging/AdmNativeRegistration;->getNodeValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/AdmNativeRegistration;->setPNSHandle(Ljava/lang/String;)V

    const-string p1, "$Default"

    .line 63
    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/AdmNativeRegistration;->setName(Ljava/lang/String;)V

    return-void
.end method

.class public Lcom/microsoft/windowsazure/messaging/GcmNativeRegistration;
.super Lcom/microsoft/windowsazure/messaging/Registration;
.source "GcmNativeRegistration.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final GCM_HANDLE_NODE:Ljava/lang/String; = "GcmRegistrationId"

.field private static final GCM_NATIVE_REGISTRATION_CUSTOM_NODE:Ljava/lang/String; = "GcmRegistrationDescription"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/Registration;-><init>(Ljava/lang/String;)V

    .line 50
    sget-object p1, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->gcm:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/GcmNativeRegistration;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    return-void
.end method


# virtual methods
.method protected appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
    .locals 2

    .line 60
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/GcmNativeRegistration;->getPNSHandle()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GcmRegistrationId"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/microsoft/windowsazure/messaging/GcmNativeRegistration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected getSpecificPayloadNodeName()Ljava/lang/String;
    .locals 1

    const-string v0, "GcmRegistrationDescription"

    return-object v0
.end method

.method protected loadCustomXmlData(Lorg/w3c/dom/Element;)V
    .locals 1

    const-string v0, "GcmRegistrationId"

    .line 65
    invoke-static {p1, v0}, Lcom/microsoft/windowsazure/messaging/GcmNativeRegistration;->getNodeValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/GcmNativeRegistration;->setPNSHandle(Ljava/lang/String;)V

    const-string p1, "$Default"

    .line 66
    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/GcmNativeRegistration;->setName(Ljava/lang/String;)V

    return-void
.end method

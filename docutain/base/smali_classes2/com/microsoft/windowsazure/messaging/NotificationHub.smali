.class public Lcom/microsoft/windowsazure/messaging/NotificationHub;
.super Ljava/lang/Object;
.source "NotificationHub.java"


# static fields
.field private static final NEW_REGISTRATION_LOCATION_HEADER:Ljava/lang/String; = "Location"

.field private static final PNS_HANDLE_KEY:Ljava/lang/String; = "PNS_HANDLE"

.field private static final REGISTRATION_NAME_STORAGE_KEY:Ljava/lang/String; = "REG_NAME_"

.field private static final STORAGE_PREFIX:Ljava/lang/String; = "__NH_"

.field private static final STORAGE_VERSION:Ljava/lang/String; = "1.0.0"

.field private static final STORAGE_VERSION_KEY:Ljava/lang/String; = "STORAGE_VERSION"

.field private static final XML_CONTENT_TYPE:Ljava/lang/String; = "application/atom+xml"


# instance fields
.field private mConnectionString:Ljava/lang/String;

.field private mIsRefreshNeeded:Z

.field private mNotificationHubPath:Ljava/lang/String;

.field private mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .line 115
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {p3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/windowsazure/messaging/NotificationHub;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences;)V
    .locals 1

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 106
    iput-boolean v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mIsRefreshNeeded:Z

    .line 119
    invoke-virtual {p0, p2}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->setConnectionString(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->setNotificationHubPath(Ljava/lang/String;)V

    if-eqz p3, :cond_0

    .line 126
    iput-object p3, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 128
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->verifyStorageVersion()V

    return-void

    .line 123
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "sharedPreferences cannot be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private createRegistrationId()Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 468
    new-instance v0, Lcom/microsoft/windowsazure/messaging/Connection;

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mConnectionString:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/microsoft/windowsazure/messaging/Connection;-><init>(Ljava/lang/String;)V

    .line 470
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mNotificationHubPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/registrationids/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v6, v2, [Ljava/util/AbstractMap$SimpleEntry;

    const/4 v2, 0x0

    const-string v3, "application/atom+xml"

    const-string v4, "POST"

    const-string v5, "Location"

    .line 471
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/windowsazure/messaging/Connection;->executeRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/util/AbstractMap$SimpleEntry;)Ljava/lang/String;

    move-result-object v0

    .line 473
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 474
    invoke-virtual {v1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 475
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method private deleteRegistrationInternal(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 486
    new-instance v0, Lcom/microsoft/windowsazure/messaging/Connection;

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mConnectionString:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/microsoft/windowsazure/messaging/Connection;-><init>(Ljava/lang/String;)V

    .line 487
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mNotificationHubPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/Registrations/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 p2, 0x1

    new-array v5, p2, [Ljava/util/AbstractMap$SimpleEntry;

    .line 489
    new-instance p2, Ljava/util/AbstractMap$SimpleEntry;

    const-string v2, "If-Match"

    const-string v3, "*"

    invoke-direct {p2, v2, v3}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x0

    aput-object p2, v5, v2

    const/4 v2, 0x0

    const-string v3, "application/atom+xml"

    const-string v4, "DELETE"

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/windowsazure/messaging/Connection;->executeRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/util/AbstractMap$SimpleEntry;)Ljava/lang/String;

    .line 490
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->removeRegistrationId(Ljava/lang/String;)V

    return-void
.end method

.method private refreshRegistrationInformation(Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 279
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 284
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 285
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 286
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "__NH_REG_NAME_"

    .line 287
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 288
    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 292
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 295
    new-instance v4, Lcom/microsoft/windowsazure/messaging/Connection;

    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mConnectionString:Ljava/lang/String;

    invoke-direct {v4, v0}, Lcom/microsoft/windowsazure/messaging/Connection;-><init>(Ljava/lang/String;)V

    .line 297
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->getInstance()Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->getPNSHandleFieldName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " eq \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 299
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mNotificationHubPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/Registrations/?$filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "UTF-8"

    invoke-static {p1, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 p1, 0x0

    new-array v9, p1, [Ljava/util/AbstractMap$SimpleEntry;

    const-string v7, "application/atom+xml"

    const-string v8, "GET"

    .line 301
    invoke-virtual/range {v4 .. v9}, Lcom/microsoft/windowsazure/messaging/Connection;->executeRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/util/AbstractMap$SimpleEntry;)Ljava/lang/String;

    move-result-object v0

    .line 303
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v1

    .line 304
    new-instance v2, Lcom/microsoft/windowsazure/messaging/NotificationHub$1;

    invoke-direct {v2, p0}, Lcom/microsoft/windowsazure/messaging/NotificationHub$1;-><init>(Lcom/microsoft/windowsazure/messaging/NotificationHub;)V

    invoke-virtual {v1, v2}, Ljavax/xml/parsers/DocumentBuilder;->setEntityResolver(Lorg/xml/sax/EntityResolver;)V

    .line 311
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v1, v2}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 313
    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v1

    invoke-interface {v1}, Lorg/w3c/dom/Element;->normalize()V

    .line 314
    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    const-string v1, "entry"

    .line 317
    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    const/4 v1, 0x0

    .line 318
    :goto_1
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 320
    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    check-cast v2, Lorg/w3c/dom/Element;

    .line 321
    invoke-static {v2}, Lcom/microsoft/windowsazure/messaging/Utils;->getXmlString(Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v2

    .line 322
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->getInstance()Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->isTemplateRegistration(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 323
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->getInstance()Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mNotificationHubPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->createTemplateRegistration(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/TemplateRegistration;

    move-result-object v3

    goto :goto_2

    .line 325
    :cond_2
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->getInstance()Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mNotificationHubPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->createNativeRegistration(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/Registration;

    move-result-object v3

    .line 328
    :goto_2
    iget-object v4, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mNotificationHubPath:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/microsoft/windowsazure/messaging/Registration;->loadXml(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    invoke-virtual {v3}, Lcom/microsoft/windowsazure/messaging/Registration;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/microsoft/windowsazure/messaging/Registration;->getRegistrationId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/microsoft/windowsazure/messaging/Registration;->getPNSHandle()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v4, v3}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->storeRegistrationId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 333
    :cond_3
    iput-boolean p1, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mIsRefreshNeeded:Z

    return-void

    .line 280
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "pnsHandle"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private registerInternal(Lcom/microsoft/windowsazure/messaging/Registration;)Lcom/microsoft/windowsazure/messaging/Registration;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 392
    iget-boolean v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mIsRefreshNeeded:Z

    if-eqz v0, :cond_1

    .line 393
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "__NH_PNS_HANDLE"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 395
    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 396
    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/Registration;->getPNSHandle()Ljava/lang/String;

    move-result-object v0

    .line 399
    :cond_0
    invoke-direct {p0, v0}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->refreshRegistrationInformation(Ljava/lang/String;)V

    .line 402
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/Registration;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->retrieveRegistrationId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 403
    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 404
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->createRegistrationId()Ljava/lang/String;

    move-result-object v0

    .line 407
    :cond_2
    invoke-virtual {p1, v0}, Lcom/microsoft/windowsazure/messaging/Registration;->setRegistrationId(Ljava/lang/String;)V

    .line 410
    :try_start_0
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->upsertRegistrationInternal(Lcom/microsoft/windowsazure/messaging/Registration;)Lcom/microsoft/windowsazure/messaging/Registration;

    move-result-object p1
    :try_end_0
    .catch Lcom/microsoft/windowsazure/messaging/RegistrationGoneException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 416
    :catch_0
    invoke-direct {p0}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->createRegistrationId()Ljava/lang/String;

    move-result-object v0

    .line 417
    invoke-virtual {p1, v0}, Lcom/microsoft/windowsazure/messaging/Registration;->setRegistrationId(Ljava/lang/String;)V

    .line 418
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->upsertRegistrationInternal(Lcom/microsoft/windowsazure/messaging/Registration;)Lcom/microsoft/windowsazure/messaging/Registration;

    move-result-object p1

    return-object p1
.end method

.method private removeRegistrationId(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 528
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 530
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "__NH_REG_NAME_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 532
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private retrieveRegistrationId(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 500
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mSharedPreferences:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "__NH_REG_NAME_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private storeRegistrationId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 510
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 512
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "__NH_REG_NAME_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string p1, "__NH_PNS_HANDLE"

    .line 514
    invoke-interface {v0, p1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string p1, "__NH_STORAGE_VERSION"

    const-string p2, "1.0.0"

    .line 517
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 519
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private unregisterInternal(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 427
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->retrieveRegistrationId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 429
    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 430
    invoke-direct {p0, p1, v0}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->deleteRegistrationInternal(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private upsertRegistrationInternal(Lcom/microsoft/windowsazure/messaging/Registration;)Lcom/microsoft/windowsazure/messaging/Registration;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 441
    new-instance v0, Lcom/microsoft/windowsazure/messaging/Connection;

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mConnectionString:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/microsoft/windowsazure/messaging/Connection;-><init>(Ljava/lang/String;)V

    .line 443
    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/Registration;->getURI()Ljava/lang/String;

    move-result-object v1

    .line 444
    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/Registration;->toXml()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v5, v3, [Ljava/util/AbstractMap$SimpleEntry;

    const-string v3, "application/atom+xml"

    const-string v4, "PUT"

    .line 446
    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/windowsazure/messaging/Connection;->executeRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/util/AbstractMap$SimpleEntry;)Ljava/lang/String;

    move-result-object v0

    .line 449
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->getInstance()Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->isTemplateRegistration(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 453
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->getInstance()Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mNotificationHubPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->createTemplateRegistration(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/TemplateRegistration;

    move-result-object v1

    goto :goto_0

    .line 457
    :cond_0
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->getInstance()Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mNotificationHubPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->createNativeRegistration(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/Registration;

    move-result-object v1

    .line 460
    :goto_0
    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mNotificationHubPath:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/microsoft/windowsazure/messaging/Registration;->loadXml(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    invoke-virtual {v1}, Lcom/microsoft/windowsazure/messaging/Registration;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/microsoft/windowsazure/messaging/Registration;->getRegistrationId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/windowsazure/messaging/Registration;->getPNSHandle()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, v2, p1}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->storeRegistrationId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private verifyStorageVersion()V
    .locals 4

    .line 536
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "__NH_STORAGE_VERSION"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 538
    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "1.0.0"

    .line 540
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 541
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 543
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "__NH_"

    .line 544
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 545
    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 550
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v0, 0x1

    .line 552
    iput-boolean v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mIsRefreshNeeded:Z

    return-void
.end method


# virtual methods
.method public getConnectionString()Ljava/lang/String;
    .locals 1

    .line 341
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mConnectionString:Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationHubPath()Ljava/lang/String;
    .locals 1

    .line 368
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mNotificationHubPath:Ljava/lang/String;

    return-object v0
.end method

.method public varargs register(Ljava/lang/String;[Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/Registration;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 139
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->getInstance()Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mNotificationHubPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->createNativeRegistration(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/Registration;

    move-result-object v0

    .line 144
    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/Registration;->setPNSHandle(Ljava/lang/String;)V

    const-string p1, "$Default"

    .line 145
    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/Registration;->setName(Ljava/lang/String;)V

    .line 146
    invoke-virtual {v0, p2}, Lcom/microsoft/windowsazure/messaging/Registration;->addTags([Ljava/lang/String;)V

    .line 148
    invoke-direct {p0, v0}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->registerInternal(Lcom/microsoft/windowsazure/messaging/Registration;)Lcom/microsoft/windowsazure/messaging/Registration;

    move-result-object p1

    return-object p1

    .line 140
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "pnsHandle"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public varargs registerBaidu(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/Registration;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 160
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 163
    invoke-static {p2}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->getInstance()Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    move-result-object v0

    sget-object v1, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->baidu:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    invoke-virtual {v0, v1}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->setRegistrationType(Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;)V

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "-"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, p3}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->register(Ljava/lang/String;[Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/Registration;

    move-result-object p1

    return-object p1

    .line 164
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "channelId"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 161
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "userId"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public varargs registerBaiduTemplate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/TemplateRegistration;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 215
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 219
    invoke-static {p2}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 223
    invoke-static {p3}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 227
    invoke-static {p4}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->getInstance()Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    move-result-object v0

    sget-object v1, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->baidu:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    invoke-virtual {v0, v1}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->setRegistrationType(Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;)V

    .line 234
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "-"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, p3, p4, p5}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->registerTemplate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/TemplateRegistration;

    move-result-object p1

    return-object p1

    .line 228
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "template"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 224
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "templateName"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 220
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "channelId"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 216
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "userId"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public varargs registerTemplate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/TemplateRegistration;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 183
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 187
    invoke-static {p2}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 191
    invoke-static {p3}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    invoke-static {}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->getInstance()Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mNotificationHubPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/windowsazure/messaging/PnsSpecificRegistrationFactory;->createTemplateRegistration(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/TemplateRegistration;

    move-result-object v0

    .line 196
    invoke-virtual {v0, p1}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->setPNSHandle(Ljava/lang/String;)V

    .line 197
    invoke-virtual {v0, p2}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->setName(Ljava/lang/String;)V

    .line 198
    invoke-virtual {v0, p3}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->setBodyTemplate(Ljava/lang/String;)V

    .line 199
    invoke-virtual {v0, p4}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->addTags([Ljava/lang/String;)V

    .line 201
    invoke-direct {p0, v0}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->registerInternal(Lcom/microsoft/windowsazure/messaging/Registration;)Lcom/microsoft/windowsazure/messaging/Registration;

    move-result-object p1

    check-cast p1, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;

    return-object p1

    .line 192
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "template"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 188
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "templateName"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 184
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "pnsHandle"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setConnectionString(Ljava/lang/String;)V
    .locals 2

    .line 350
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "connectionString"

    if-nez v0, :cond_0

    .line 355
    :try_start_0
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;->parse(Ljava/lang/String;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mConnectionString:Ljava/lang/String;

    return-void

    :catch_0
    move-exception p1

    .line 357
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 351
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setNotificationHubPath(Ljava/lang/String;)V
    .locals 1

    .line 377
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 381
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mNotificationHubPath:Ljava/lang/String;

    return-void

    .line 378
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "notificationHubPath"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public unregister()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "$Default"

    .line 242
    invoke-direct {p0, v0}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->unregisterInternal(Ljava/lang/String;)V

    return-void
.end method

.method public unregisterAll(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 264
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->refreshRegistrationInformation(Ljava/lang/String;)V

    .line 266
    iget-object p1, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    .line 268
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "__NH_REG_NAME_"

    .line 269
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xe

    .line 270
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 271
    iget-object v2, p0, Lcom/microsoft/windowsazure/messaging/NotificationHub;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, ""

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 273
    invoke-direct {p0, v1, v0}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->deleteRegistrationInternal(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public unregisterTemplate(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 251
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/NotificationHub;->unregisterInternal(Ljava/lang/String;)V

    return-void

    .line 252
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "templateName"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.class public Lcom/microsoft/windowsazure/messaging/RegistrationGoneException;
.super Ljava/lang/Exception;
.source "RegistrationGoneException.java"


# static fields
.field private static final serialVersionUID:J = -0x22aef6bb886a607L


# direct methods
.method constructor <init>()V
    .locals 1

    const-string v0, "Registration is gone"

    .line 34
    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    return-void
.end method

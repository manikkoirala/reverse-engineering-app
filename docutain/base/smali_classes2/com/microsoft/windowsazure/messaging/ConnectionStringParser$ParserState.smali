.class final enum Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;
.super Ljava/lang/Enum;
.source "ConnectionStringParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/windowsazure/messaging/ConnectionStringParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ParserState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

.field public static final enum EXPECT_ASSIGNMENT:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

.field public static final enum EXPECT_KEY:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

.field public static final enum EXPECT_SEPARATOR:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

.field public static final enum EXPECT_VALUE:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 32
    new-instance v0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    const-string v1, "EXPECT_KEY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->EXPECT_KEY:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    new-instance v1, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    const-string v3, "EXPECT_ASSIGNMENT"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->EXPECT_ASSIGNMENT:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    new-instance v3, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    const-string v5, "EXPECT_VALUE"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->EXPECT_VALUE:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    new-instance v5, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    const-string v7, "EXPECT_SEPARATOR"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->EXPECT_SEPARATOR:Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    const/4 v7, 0x4

    new-array v7, v7, [Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    .line 31
    sput-object v7, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->$VALUES:[Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;
    .locals 1

    .line 31
    const-class v0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    return-object p0
.end method

.method public static values()[Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;
    .locals 1

    .line 31
    sget-object v0, Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->$VALUES:[Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    invoke-virtual {v0}, [Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/windowsazure/messaging/ConnectionStringParser$ParserState;

    return-object v0
.end method

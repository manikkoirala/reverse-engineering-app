.class public Lcom/microsoft/windowsazure/messaging/FcmTemplateRegistration;
.super Lcom/microsoft/windowsazure/messaging/TemplateRegistration;
.source "FcmTemplateRegistration.java"


# static fields
.field private static final FCM_HANDLE_NODE:Ljava/lang/String; = "GcmRegistrationId"

.field static final FCM_TEMPLATE_REGISTRATION_CUSTOM_NODE:Ljava/lang/String; = "GcmTemplateRegistrationDescription"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;-><init>(Ljava/lang/String;)V

    .line 47
    sget-object p1, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->fcm:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/FcmTemplateRegistration;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    return-void
.end method


# virtual methods
.method protected appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
    .locals 2

    .line 57
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/FcmTemplateRegistration;->getPNSHandle()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GcmRegistrationId"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/microsoft/windowsazure/messaging/FcmTemplateRegistration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-super {p0, p1, p2}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V

    return-void
.end method

.method protected getSpecificPayloadNodeName()Ljava/lang/String;
    .locals 1

    const-string v0, "GcmTemplateRegistrationDescription"

    return-object v0
.end method

.method protected loadCustomXmlData(Lorg/w3c/dom/Element;)V
    .locals 1

    const-string v0, "GcmRegistrationId"

    .line 63
    invoke-static {p1, v0}, Lcom/microsoft/windowsazure/messaging/FcmTemplateRegistration;->getNodeValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/windowsazure/messaging/FcmTemplateRegistration;->setPNSHandle(Ljava/lang/String;)V

    .line 64
    invoke-super {p0, p1}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->loadCustomXmlData(Lorg/w3c/dom/Element;)V

    return-void
.end method

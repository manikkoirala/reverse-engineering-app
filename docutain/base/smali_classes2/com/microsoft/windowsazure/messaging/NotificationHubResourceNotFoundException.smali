.class public Lcom/microsoft/windowsazure/messaging/NotificationHubResourceNotFoundException;
.super Lcom/microsoft/windowsazure/messaging/NotificationHubException;
.source "NotificationHubResourceNotFoundException.java"


# static fields
.field private static final serialVersionUID:J = -0x10bb348b4e01dd17L


# direct methods
.method constructor <init>()V
    .locals 2

    const-string v0, "Resource not found"

    const/16 v1, 0x194

    .line 34
    invoke-direct {p0, v0, v1}, Lcom/microsoft/windowsazure/messaging/NotificationHubException;-><init>(Ljava/lang/String;I)V

    return-void
.end method

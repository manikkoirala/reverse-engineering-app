.class public Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;
.super Lcom/microsoft/windowsazure/messaging/Registration;
.source "BaiduNativeRegistration.java"


# static fields
.field private static final BAIDU_CHANNEL_ID:Ljava/lang/String; = "BaiduChannelId"

.field static final BAIDU_HANDLE_NODE:Ljava/lang/String; = "BaiduUserId-BaiduChannelId"

.field private static final BAIDU_NATIVE_REGISTRATION_CUSTOM_NODE:Ljava/lang/String; = "BaiduRegistrationDescription"

.field private static final BAIDU_USER_ID:Ljava/lang/String; = "BaiduUserId"


# instance fields
.field protected mChannelId:Ljava/lang/String;

.field protected mUserId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 67
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/Registration;-><init>(Ljava/lang/String;)V

    .line 68
    sget-object p1, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->baidu:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    return-void
.end method


# virtual methods
.method protected appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
    .locals 2

    .line 135
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;->getUserId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BaiduUserId"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;->getChannelId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BaiduChannelId"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getChannelId()Ljava/lang/String;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;->mChannelId:Ljava/lang/String;

    return-object v0
.end method

.method protected getSpecificPayloadNodeName()Ljava/lang/String;
    .locals 1

    const-string v0, "BaiduRegistrationDescription"

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;->mUserId:Ljava/lang/String;

    return-object v0
.end method

.method protected loadCustomXmlData(Lorg/w3c/dom/Element;)V
    .locals 0

    const-string p1, "$Default"

    .line 141
    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;->setName(Ljava/lang/String;)V

    return-void
.end method

.method setChannelId(Ljava/lang/String;)V
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;->mChannelId:Ljava/lang/String;

    return-void
.end method

.method setPNSHandle(Ljava/lang/String;)V
    .locals 2

    .line 78
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 81
    :cond_0
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;->mPNSHandle:Ljava/lang/String;

    const-string v0, "-"

    .line 82
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    .line 84
    aget-object v0, p1, v0

    .line 86
    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 89
    invoke-virtual {p0, v0}, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;->setUserId(Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 91
    aget-object p1, p1, v1

    .line 92
    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 95
    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;->setChannelId(Ljava/lang/String;)V

    return-void

    .line 93
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Baidu channelId is inalid!"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 87
    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Baidu userId is inalid!"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method setUserId(Ljava/lang/String;)V
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/BaiduNativeRegistration;->mUserId:Ljava/lang/String;

    return-void
.end method

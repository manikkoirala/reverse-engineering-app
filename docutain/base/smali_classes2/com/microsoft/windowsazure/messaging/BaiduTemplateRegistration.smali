.class public Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;
.super Lcom/microsoft/windowsazure/messaging/TemplateRegistration;
.source "BaiduTemplateRegistration.java"


# static fields
.field private static final BAIDU_CHANNEL_ID:Ljava/lang/String; = "BaiduChannelId"

.field private static final BAIDU_HANDLE_NODE:Ljava/lang/String; = "BaiduUserId-BaiduChannelId"

.field static final BAIDU_TEMPLATE_REGISTRATION_CUSTOM_NODE:Ljava/lang/String; = "BaiduTemplateRegistrationDescription"

.field private static final BAIDU_USER_ID:Ljava/lang/String; = "BaiduUserId"


# instance fields
.field protected mChannelId:Ljava/lang/String;

.field protected mUserId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;-><init>(Ljava/lang/String;)V

    .line 69
    sget-object p1, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->baidu:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    return-void
.end method


# virtual methods
.method protected appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
    .locals 2

    .line 136
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->getUserId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BaiduUserId"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->getChannelId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BaiduChannelId"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-super {p0, p1, p2}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V

    return-void
.end method

.method public getChannelId()Ljava/lang/String;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->mChannelId:Ljava/lang/String;

    return-object v0
.end method

.method protected getSpecificPayloadNodeName()Ljava/lang/String;
    .locals 1

    const-string v0, "BaiduTemplateRegistrationDescription"

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->mUserId:Ljava/lang/String;

    return-object v0
.end method

.method protected loadCustomXmlData(Lorg/w3c/dom/Element;)V
    .locals 1

    const-string v0, "BaiduUserId-BaiduChannelId"

    .line 143
    invoke-static {p1, v0}, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->getNodeValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->setPNSHandle(Ljava/lang/String;)V

    .line 144
    invoke-super {p0, p1}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->loadCustomXmlData(Lorg/w3c/dom/Element;)V

    return-void
.end method

.method setChannelId(Ljava/lang/String;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->mChannelId:Ljava/lang/String;

    return-void
.end method

.method setPNSHandle(Ljava/lang/String;)V
    .locals 2

    .line 109
    invoke-static {p1}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 112
    :cond_0
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->mPNSHandle:Ljava/lang/String;

    const-string v0, "-"

    .line 113
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    .line 115
    aget-object v0, p1, v0

    .line 117
    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 120
    invoke-virtual {p0, v0}, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->setUserId(Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 122
    aget-object p1, p1, v1

    .line 123
    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 126
    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->setChannelId(Ljava/lang/String;)V

    return-void

    .line 124
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Baidu channelId is inalid!"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 118
    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Baidu userId is inalid!"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method setUserId(Ljava/lang/String;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/BaiduTemplateRegistration;->mUserId:Ljava/lang/String;

    return-void
.end method

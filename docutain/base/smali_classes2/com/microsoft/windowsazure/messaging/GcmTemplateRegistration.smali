.class public Lcom/microsoft/windowsazure/messaging/GcmTemplateRegistration;
.super Lcom/microsoft/windowsazure/messaging/TemplateRegistration;
.source "GcmTemplateRegistration.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final GCM_HANDLE_NODE:Ljava/lang/String; = "GcmRegistrationId"

.field static final GCM_TEMPLATE_REGISTRATION_CUSTOM_NODE:Ljava/lang/String; = "GcmTemplateRegistrationDescription"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;-><init>(Ljava/lang/String;)V

    .line 50
    sget-object p1, Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;->gcm:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/GcmTemplateRegistration;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    return-void
.end method


# virtual methods
.method protected appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
    .locals 2

    .line 60
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/GcmTemplateRegistration;->getPNSHandle()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GcmRegistrationId"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/microsoft/windowsazure/messaging/GcmTemplateRegistration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-super {p0, p1, p2}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V

    return-void
.end method

.method protected getSpecificPayloadNodeName()Ljava/lang/String;
    .locals 1

    const-string v0, "GcmTemplateRegistrationDescription"

    return-object v0
.end method

.method protected loadCustomXmlData(Lorg/w3c/dom/Element;)V
    .locals 1

    const-string v0, "GcmRegistrationId"

    .line 66
    invoke-static {p1, v0}, Lcom/microsoft/windowsazure/messaging/GcmTemplateRegistration;->getNodeValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/windowsazure/messaging/GcmTemplateRegistration;->setPNSHandle(Ljava/lang/String;)V

    .line 67
    invoke-super {p0, p1}, Lcom/microsoft/windowsazure/messaging/TemplateRegistration;->loadCustomXmlData(Lorg/w3c/dom/Element;)V

    return-void
.end method

.class public abstract Lcom/microsoft/windowsazure/messaging/Registration;
.super Ljava/lang/Object;
.source "Registration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;
    }
.end annotation


# static fields
.field static final DEFAULT_REGISTRATION_NAME:Ljava/lang/String; = "$Default"

.field static final REGISTRATIONID_JSON_PROPERTY:Ljava/lang/String; = "registrationid"

.field static final REGISTRATION_NAME_JSON_PROPERTY:Ljava/lang/String; = "registrationName"


# instance fields
.field protected mETag:Ljava/lang/String;

.field protected mExpirationTime:Ljava/lang/String;

.field protected mName:Ljava/lang/String;

.field protected mNotificationHubPath:Ljava/lang/String;

.field protected mPNSHandle:Ljava/lang/String;

.field protected mRegistrationId:Ljava/lang/String;

.field public mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

.field protected mTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mURI:Ljava/lang/String;

.field protected mUpdated:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mTags:Ljava/util/List;

    .line 287
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mNotificationHubPath:Ljava/lang/String;

    return-void
.end method

.method private static UTCDateStringToDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "Z"

    const-string v1, "+00:00"

    .line 389
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    const/16 v0, 0x1a

    .line 393
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x1b

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 399
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "yyyy-MM-dd\'T\'HH:mm:ss\'.\'SSSZ"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 400
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 401
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    return-object p0

    .line 395
    :catch_0
    new-instance p0, Ljava/text/ParseException;

    const-string v1, "The \'updated\' value has an invalid format"

    invoke-direct {p0, v1, v0}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw p0
.end method

.method private appendContentNode(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
    .locals 3

    const-string v0, "content"

    .line 157
    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    const-string v1, "type"

    const-string v2, "application/xml"

    .line 158
    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 161
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/Registration;->getSpecificPayloadNodeName()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object p2

    const-string v1, "xmlns:i"

    const-string v2, "http://www.w3.org/2001/XMLSchema-instance"

    .line 162
    invoke-interface {p2, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "xmlns"

    const-string v2, "http://schemas.microsoft.com/netservices/2010/10/servicebus/connect"

    .line 163
    invoke-interface {p2, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-interface {v0, p2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 166
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/Registration;->getETag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ETag"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/microsoft/windowsazure/messaging/Registration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/Registration;->getExpirationTimeString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExpirationTime"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/microsoft/windowsazure/messaging/Registration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/Registration;->getRegistrationId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RegistrationId"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/microsoft/windowsazure/messaging/Registration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/windowsazure/messaging/Registration;->appendTagsNode(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V

    .line 171
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/windowsazure/messaging/Registration;->appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V

    return-void
.end method

.method protected static getNodeValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 273
    invoke-interface {p0, p1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object p0

    .line 274
    invoke-interface {p0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x0

    .line 275
    invoke-interface {p0, p1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object p0

    invoke-interface {p0}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method addTags([Ljava/lang/String;)V
    .locals 4

    if-eqz p1, :cond_1

    .line 472
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p1, v1

    .line 473
    invoke-static {v2}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 474
    iget-object v3, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mTags:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected abstract appendCustomPayload(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
.end method

.method protected appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 209
    invoke-static {p4}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    invoke-interface {p1, p3}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object p3

    .line 211
    invoke-interface {p1, p4}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object p1

    invoke-interface {p3, p1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 212
    invoke-interface {p2, p3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    :cond_0
    return-void
.end method

.method protected appendTagsNode(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
    .locals 4

    .line 187
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/Registration;->getTags()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 188
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x0

    .line 189
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x1

    .line 191
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 192
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "Tags"

    .line 195
    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 196
    invoke-interface {p1, v1}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object p1

    invoke-interface {v0, p1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 197
    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    :cond_1
    return-void
.end method

.method getETag()Ljava/lang/String;
    .locals 1

    .line 370
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mETag:Ljava/lang/String;

    return-object v0
.end method

.method public getExpirationTime()Ljava/util/Date;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 450
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mExpirationTime:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/Registration;->UTCDateStringToDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method getExpirationTimeString()Ljava/lang/String;
    .locals 1

    .line 457
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mExpirationTime:Ljava/lang/String;

    return-object v0
.end method

.method getName()Ljava/lang/String;
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationHubPath()Ljava/lang/String;
    .locals 1

    .line 325
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mNotificationHubPath:Ljava/lang/String;

    return-object v0
.end method

.method public getPNSHandle()Ljava/lang/String;
    .locals 1

    .line 434
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mPNSHandle:Ljava/lang/String;

    return-object v0
.end method

.method public getRegistrationId()Ljava/lang/String;
    .locals 1

    .line 310
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mRegistrationId:Ljava/lang/String;

    return-object v0
.end method

.method getRegistrationInformation()Lorg/json/JSONObject;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 485
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 486
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/Registration;->getRegistrationId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "registrationid"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 487
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/Registration;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "registrationName"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-object v0
.end method

.method public getRegistrationType()Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;
    .locals 1

    .line 295
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    return-object v0
.end method

.method protected abstract getSpecificPayloadNodeName()Ljava/lang/String;
.end method

.method public getTags()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 355
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mTags:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getURI()Ljava/lang/String;
    .locals 2

    .line 363
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/Registration;->getNotificationHubPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/Registrations/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mRegistrationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getUpdated()Ljava/util/Date;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 412
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mUpdated:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/Registration;->UTCDateStringToDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method getUpdatedString()Ljava/lang/String;
    .locals 1

    .line 419
    iget-object v0, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mUpdated:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract loadCustomXmlData(Lorg/w3c/dom/Element;)V
.end method

.method loadXml(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 223
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 224
    new-instance v1, Lorg/xml/sax/InputSource;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0, v1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;

    move-result-object p1

    .line 226
    invoke-interface {p1}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Element;->normalize()V

    .line 227
    invoke-interface {p1}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 229
    iput-object p2, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mNotificationHubPath:Ljava/lang/String;

    const-string v1, "updated"

    .line 230
    invoke-static {v0, v1}, Lcom/microsoft/windowsazure/messaging/Registration;->getNodeValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mUpdated:Ljava/lang/String;

    .line 232
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/Registration;->getSpecificPayloadNodeName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object p1

    .line 233
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    .line 234
    invoke-interface {p1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object p1

    check-cast p1, Lorg/w3c/dom/Element;

    const-string v1, "ETag"

    .line 235
    invoke-static {p1, v1}, Lcom/microsoft/windowsazure/messaging/Registration;->getNodeValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mETag:Ljava/lang/String;

    const-string v1, "ExpirationTime"

    .line 236
    invoke-static {p1, v1}, Lcom/microsoft/windowsazure/messaging/Registration;->getNodeValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mExpirationTime:Ljava/lang/String;

    const-string v1, "RegistrationId"

    .line 237
    invoke-static {p1, v1}, Lcom/microsoft/windowsazure/messaging/Registration;->getNodeValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mRegistrationId:Ljava/lang/String;

    .line 238
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "/Registrations/"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mRegistrationId:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mURI:Ljava/lang/String;

    const-string p2, "Tags"

    .line 240
    invoke-static {p1, p2}, Lcom/microsoft/windowsazure/messaging/Registration;->getNodeValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 241
    invoke-static {p2}, Lcom/microsoft/windowsazure/messaging/Utils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 242
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    const-string v1, ","

    .line 243
    invoke-virtual {p2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p2

    .line 245
    array-length v1, p2

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p2, v0

    .line 246
    iget-object v3, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mTags:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 250
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/windowsazure/messaging/Registration;->loadCustomXmlData(Lorg/w3c/dom/Element;)V

    :cond_1
    return-void
.end method

.method setETag(Ljava/lang/String;)V
    .locals 0

    .line 377
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mETag:Ljava/lang/String;

    return-void
.end method

.method setExpirationTimeString(Ljava/lang/String;)V
    .locals 0

    .line 464
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mExpirationTime:Ljava/lang/String;

    return-void
.end method

.method setName(Ljava/lang/String;)V
    .locals 0

    .line 347
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mName:Ljava/lang/String;

    return-void
.end method

.method setNotificationHubPath(Ljava/lang/String;)V
    .locals 0

    .line 333
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mNotificationHubPath:Ljava/lang/String;

    return-void
.end method

.method setPNSHandle(Ljava/lang/String;)V
    .locals 0

    .line 441
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mPNSHandle:Ljava/lang/String;

    return-void
.end method

.method setRegistrationId(Ljava/lang/String;)V
    .locals 0

    .line 317
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mRegistrationId:Ljava/lang/String;

    return-void
.end method

.method setRegistrationType(Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;)V
    .locals 0

    .line 302
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mRegistrationType:Lcom/microsoft/windowsazure/messaging/Registration$RegistrationType;

    return-void
.end method

.method setUpdatedString(Ljava/lang/String;)V
    .locals 0

    .line 426
    iput-object p1, p0, Lcom/microsoft/windowsazure/messaging/Registration;->mUpdated:Ljava/lang/String;

    return-void
.end method

.method toXml()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 129
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 130
    new-instance v1, Lcom/microsoft/windowsazure/messaging/Registration$1;

    invoke-direct {v1, p0}, Lcom/microsoft/windowsazure/messaging/Registration$1;-><init>(Lcom/microsoft/windowsazure/messaging/Registration;)V

    invoke-virtual {v0, v1}, Ljavax/xml/parsers/DocumentBuilder;->setEntityResolver(Lorg/xml/sax/EntityResolver;)V

    .line 138
    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    const-string v1, "entry"

    .line 140
    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    const-string v2, "xmlns"

    const-string v3, "http://www.w3.org/2005/Atom"

    .line 141
    invoke-interface {v1, v2, v3}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 144
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/Registration;->getURI()Ljava/lang/String;

    move-result-object v2

    const-string v3, "id"

    invoke-virtual {p0, v0, v1, v3, v2}, Lcom/microsoft/windowsazure/messaging/Registration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p0}, Lcom/microsoft/windowsazure/messaging/Registration;->getUpdatedString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "updated"

    invoke-virtual {p0, v0, v1, v3, v2}, Lcom/microsoft/windowsazure/messaging/Registration;->appendNodeWithValue(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-direct {p0, v0, v1}, Lcom/microsoft/windowsazure/messaging/Registration;->appendContentNode(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V

    .line 148
    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/windowsazure/messaging/Utils;->getXmlString(Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/pspdfkit/instant/internal/jni/NativeLayerDocumentContainer;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final mDocument:Lcom/pspdfkit/internal/jni/NativeDocument;

.field final mLayerCapabilities:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/jni/NativeDocument;Ljava/util/EnumSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/jni/NativeDocument;",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/instant/internal/jni/NativeLayerDocumentContainer;->mDocument:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/instant/internal/jni/NativeLayerDocumentContainer;->mLayerCapabilities:Ljava/util/EnumSet;

    return-void
.end method


# virtual methods
.method public getDocument()Lcom/pspdfkit/internal/jni/NativeDocument;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/internal/jni/NativeLayerDocumentContainer;->mDocument:Lcom/pspdfkit/internal/jni/NativeDocument;

    return-object v0
.end method

.method public getLayerCapabilities()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/internal/jni/NativeLayerDocumentContainer;->mLayerCapabilities:Ljava/util/EnumSet;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NativeLayerDocumentContainer{mDocument="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/instant/internal/jni/NativeLayerDocumentContainer;->mDocument:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ",mLayerCapabilities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/instant/internal/jni/NativeLayerDocumentContainer;->mLayerCapabilities:Ljava/util/EnumSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

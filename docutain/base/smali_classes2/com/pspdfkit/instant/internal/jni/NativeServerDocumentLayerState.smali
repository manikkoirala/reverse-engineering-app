.class public final enum Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

.field public static final enum CLEAN:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

.field public static final enum FETCHING_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

.field public static final enum INVALID:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

.field public static final enum MIGRATING_RECORD_CONTENT:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

.field public static final enum NEEDS_RECORD_CONTENT_MIGRATION:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

.field public static final enum NEEDS_RESET_FOR_DATABASE_MIGRATION:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

.field public static final enum PENDING_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

.field public static final enum PUSHING_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

.field public static final enum RESETTING_FOR_DATABASE_MIGRATION:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

.field public static final enum UNKNOWN:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;->UNKNOWN:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    .line 8
    new-instance v1, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    const-string v3, "NEEDS_RECORD_CONTENT_MIGRATION"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;->NEEDS_RECORD_CONTENT_MIGRATION:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    .line 14
    new-instance v3, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    const-string v5, "MIGRATING_RECORD_CONTENT"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;->MIGRATING_RECORD_CONTENT:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    .line 23
    new-instance v5, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    const-string v7, "NEEDS_RESET_FOR_DATABASE_MIGRATION"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;->NEEDS_RESET_FOR_DATABASE_MIGRATION:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    .line 31
    new-instance v7, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    const-string v9, "RESETTING_FOR_DATABASE_MIGRATION"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;->RESETTING_FOR_DATABASE_MIGRATION:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    .line 33
    new-instance v9, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    const-string v11, "CLEAN"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;->CLEAN:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    .line 35
    new-instance v11, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    const-string v13, "PENDING_CHANGES"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;->PENDING_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    .line 43
    new-instance v13, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    const-string v15, "PUSHING_CHANGES"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;->PUSHING_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    .line 53
    new-instance v15, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    const-string v14, "FETCHING_CHANGES"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;->FETCHING_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    .line 55
    new-instance v14, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    const-string v12, "INVALID"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;->INVALID:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    const/16 v12, 0xa

    new-array v12, v12, [Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    aput-object v0, v12, v2

    aput-object v1, v12, v4

    aput-object v3, v12, v6

    aput-object v5, v12, v8

    const/4 v0, 0x4

    aput-object v7, v12, v0

    const/4 v0, 0x5

    aput-object v9, v12, v0

    const/4 v0, 0x6

    aput-object v11, v12, v0

    const/4 v0, 0x7

    aput-object v13, v12, v0

    const/16 v0, 0x8

    aput-object v15, v12, v0

    aput-object v14, v12, v10

    .line 56
    sput-object v12, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;->$VALUES:[Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;->$VALUES:[Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    invoke-virtual {v0}, [Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    return-object v0
.end method

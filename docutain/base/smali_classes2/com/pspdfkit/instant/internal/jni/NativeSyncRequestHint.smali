.class public final enum Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

.field public static final enum FETCH_UPDATES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

.field public static final enum LISTEN_FOR_UPDATES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

.field public static final enum PUSH_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1
    new-instance v0, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    const-string v1, "PUSH_CHANGES"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;->PUSH_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    .line 10
    new-instance v1, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    const-string v3, "FETCH_UPDATES"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;->FETCH_UPDATES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    .line 21
    new-instance v3, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    const-string v5, "LISTEN_FOR_UPDATES"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;->LISTEN_FOR_UPDATES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    const/4 v5, 0x3

    new-array v5, v5, [Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    aput-object v0, v5, v2

    aput-object v1, v5, v4

    aput-object v3, v5, v6

    .line 22
    sput-object v5, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;->$VALUES:[Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;->$VALUES:[Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    invoke-virtual {v0}, [Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    return-object v0
.end method

.class public final enum Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum ALREADY_AUTHENTICATING:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum ALREADY_SYNCING:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum ATTACHMENT_ALREADY_TRANSFERRED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum ATTACHMENT_NOT_LOADED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum ATTACHMENT_TRANSFER_IN_PROGRESS:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum AUTHENTICATION_FAILED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum DATABASE_ERROR:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum DATABASE_IS_PERFORMING_CONTENT_MIGRATION:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum DATABASE_NEEDS_CONTENT_MIGRATION:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum INVALID_CUSTOM_DATA:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum INVALID_JSON_STRUCTURE:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum INVALID_JWT:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum INVALID_REQUEST:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum INVALID_SERVER_DATA:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum NO_SUCH_ATTACHMENT:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum NO_SUCH_ATTACHMENT_TRANSFER:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum OLD_CLIENT:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum OLD_SERVER:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum PAYLOAD_SIZE_LIMIT_EXCEEDED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum READ_FAILED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum REQUEST_FAILED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum SERVER_UUID_PENDING:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum UNKNOWN:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum UNMANAGED_ANNOTATION:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum USER_CANCELLED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum USER_MISMATCH:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

.field public static final enum WRITE_FAILED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;


# direct methods
.method static constructor <clinit>()V
    .locals 29

    .line 1
    new-instance v0, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->UNKNOWN:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 7
    new-instance v1, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v3, "USER_CANCELLED"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->USER_CANCELLED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 14
    new-instance v3, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v5, "AUTHENTICATION_FAILED"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->AUTHENTICATION_FAILED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 20
    new-instance v5, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v7, "ALREADY_AUTHENTICATING"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->ALREADY_AUTHENTICATING:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 22
    new-instance v7, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v9, "ALREADY_SYNCING"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->ALREADY_SYNCING:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 29
    new-instance v9, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v11, "REQUEST_FAILED"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->REQUEST_FAILED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 35
    new-instance v11, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v13, "OLD_CLIENT"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->OLD_CLIENT:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 41
    new-instance v13, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v15, "OLD_SERVER"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->OLD_SERVER:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 47
    new-instance v15, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v14, "INVALID_REQUEST"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->INVALID_REQUEST:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 54
    new-instance v14, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v12, "PAYLOAD_SIZE_LIMIT_EXCEEDED"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->PAYLOAD_SIZE_LIMIT_EXCEEDED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 60
    new-instance v12, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v10, "INVALID_SERVER_DATA"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->INVALID_SERVER_DATA:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 66
    new-instance v10, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v8, "WRITE_FAILED"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->WRITE_FAILED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 73
    new-instance v8, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v6, "READ_FAILED"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->READ_FAILED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 83
    new-instance v6, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v4, "DATABASE_ERROR"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->DATABASE_ERROR:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 89
    new-instance v4, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v2, "DATABASE_NEEDS_CONTENT_MIGRATION"

    move-object/from16 v16, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->DATABASE_NEEDS_CONTENT_MIGRATION:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 97
    new-instance v2, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v6, "DATABASE_IS_PERFORMING_CONTENT_MIGRATION"

    move-object/from16 v17, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->DATABASE_IS_PERFORMING_CONTENT_MIGRATION:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 103
    new-instance v6, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v4, "SERVER_UUID_PENDING"

    move-object/from16 v18, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->SERVER_UUID_PENDING:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 105
    new-instance v4, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v2, "INVALID_JWT"

    move-object/from16 v19, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->INVALID_JWT:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 107
    new-instance v2, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v6, "USER_MISMATCH"

    move-object/from16 v20, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->USER_MISMATCH:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 113
    new-instance v6, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v4, "ATTACHMENT_NOT_LOADED"

    move-object/from16 v21, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->ATTACHMENT_NOT_LOADED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 115
    new-instance v4, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v2, "NO_SUCH_ATTACHMENT"

    move-object/from16 v22, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->NO_SUCH_ATTACHMENT:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 121
    new-instance v2, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v6, "ATTACHMENT_TRANSFER_IN_PROGRESS"

    move-object/from16 v23, v4

    const/16 v4, 0x15

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->ATTACHMENT_TRANSFER_IN_PROGRESS:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 127
    new-instance v6, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v4, "ATTACHMENT_ALREADY_TRANSFERRED"

    move-object/from16 v24, v2

    const/16 v2, 0x16

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->ATTACHMENT_ALREADY_TRANSFERRED:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 129
    new-instance v2, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v4, "NO_SUCH_ATTACHMENT_TRANSFER"

    move-object/from16 v25, v6

    const/16 v6, 0x17

    invoke-direct {v2, v4, v6}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->NO_SUCH_ATTACHMENT_TRANSFER:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 131
    new-instance v4, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v6, "UNMANAGED_ANNOTATION"

    move-object/from16 v26, v2

    const/16 v2, 0x18

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->UNMANAGED_ANNOTATION:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 133
    new-instance v2, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v6, "INVALID_CUSTOM_DATA"

    move-object/from16 v27, v4

    const/16 v4, 0x19

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->INVALID_CUSTOM_DATA:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    .line 135
    new-instance v4, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const-string v6, "INVALID_JSON_STRUCTURE"

    move-object/from16 v28, v2

    const/16 v2, 0x1a

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->INVALID_JSON_STRUCTURE:Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const/16 v2, 0x1b

    new-array v2, v2, [Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    const/4 v6, 0x0

    aput-object v0, v2, v6

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object v5, v2, v0

    const/4 v0, 0x4

    aput-object v7, v2, v0

    const/4 v0, 0x5

    aput-object v9, v2, v0

    const/4 v0, 0x6

    aput-object v11, v2, v0

    const/4 v0, 0x7

    aput-object v13, v2, v0

    const/16 v0, 0x8

    aput-object v15, v2, v0

    const/16 v0, 0x9

    aput-object v14, v2, v0

    const/16 v0, 0xa

    aput-object v12, v2, v0

    const/16 v0, 0xb

    aput-object v10, v2, v0

    const/16 v0, 0xc

    aput-object v8, v2, v0

    const/16 v0, 0xd

    aput-object v16, v2, v0

    const/16 v0, 0xe

    aput-object v17, v2, v0

    const/16 v0, 0xf

    aput-object v18, v2, v0

    const/16 v0, 0x10

    aput-object v19, v2, v0

    const/16 v0, 0x11

    aput-object v20, v2, v0

    const/16 v0, 0x12

    aput-object v21, v2, v0

    const/16 v0, 0x13

    aput-object v22, v2, v0

    const/16 v0, 0x14

    aput-object v23, v2, v0

    const/16 v0, 0x15

    aput-object v24, v2, v0

    const/16 v0, 0x16

    aput-object v25, v2, v0

    const/16 v0, 0x17

    aput-object v26, v2, v0

    const/16 v0, 0x18

    aput-object v27, v2, v0

    const/16 v0, 0x19

    aput-object v28, v2, v0

    const/16 v0, 0x1a

    aput-object v4, v2, v0

    .line 136
    sput-object v2, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->$VALUES:[Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->$VALUES:[Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    invoke-virtual {v0}, [Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    return-object v0
.end method

.class public final enum Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

.field public static final enum FETCH_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

.field public static final enum LISTEN_FOR_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

.field public static final enum PUSH_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

.field public static final enum PUSH_CHANGES_AND_ASSETS:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 1
    new-instance v0, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    const-string v1, "FETCH_CHANGES"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;->FETCH_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    .line 3
    new-instance v1, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    const-string v3, "PUSH_CHANGES"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;->PUSH_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    .line 5
    new-instance v3, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    const-string v5, "PUSH_CHANGES_AND_ASSETS"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;->PUSH_CHANGES_AND_ASSETS:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    .line 10
    new-instance v5, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    const-string v7, "LISTEN_FOR_CHANGES"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;->LISTEN_FOR_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    const/4 v7, 0x4

    new-array v7, v7, [Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    .line 11
    sput-object v7, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;->$VALUES:[Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;->$VALUES:[Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    invoke-virtual {v0}, [Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    return-object v0
.end method

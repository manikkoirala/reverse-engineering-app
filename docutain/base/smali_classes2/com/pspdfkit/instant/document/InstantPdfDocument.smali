.class public interface abstract Lcom/pspdfkit/instant/document/InstantPdfDocument;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/PdfDocument;


# static fields
.field public static final SYNC_LOCAL_CHANGES_DISABLED:J = 0x7fffffffffffffffL


# virtual methods
.method public abstract addInstantDocumentListener(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V
.end method

.method public abstract getAnnotationProvider()Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider;
.end method

.method public abstract getDelayForSyncingLocalChanges()J
.end method

.method public abstract getDocumentState()Lcom/pspdfkit/instant/document/InstantDocumentState;
.end method

.method public abstract getInstantClient()Lcom/pspdfkit/instant/client/InstantClient;
.end method

.method public abstract getInstantDocumentDescriptor()Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;
.end method

.method public abstract isListeningToServerChanges()Z
.end method

.method public abstract notifyConnectivityChanged(Z)V
.end method

.method public abstract reauthenticateWithJwt(Ljava/lang/String;)V
.end method

.method public abstract reauthenticateWithJwtAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Completable;
.end method

.method public abstract removeInstantDocumentListener(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V
.end method

.method public abstract removeLocalStorage()V
.end method

.method public abstract setDelayForSyncingLocalChanges(J)V
.end method

.method public abstract setListenToServerChanges(Z)V
.end method

.method public abstract syncAnnotations()V
.end method

.method public abstract syncAnnotationsAsync()Lio/reactivex/rxjava3/core/Flowable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Lcom/pspdfkit/instant/client/InstantProgress;",
            ">;"
        }
    .end annotation
.end method

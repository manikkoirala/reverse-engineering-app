.class public Lcom/pspdfkit/instant/ui/InstantPdfFragment;
.super Lcom/pspdfkit/ui/PdfFragment;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/instant/listeners/InstantDocumentListener;
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# static fields
.field public static final PARAM_INSTANT_DOCUMENT_SOURCE:Ljava/lang/String; = "Instant.InstantDocumentSource"


# instance fields
.field private final connectivityReceiver:Landroid/content/BroadcastReceiver;

.field private documentSource:Lcom/pspdfkit/internal/if;

.field private errorDialog:Landroid/app/AlertDialog;

.field private handleCriticalErrors:Z

.field private instantDocumentListeners:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/instant/listeners/InstantDocumentListener;",
            ">;"
        }
    .end annotation
.end field

.field private listenToServerChangesWhenVisible:Z

.field private loadingProgressSubject:Lio/reactivex/rxjava3/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/subjects/PublishSubject<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private weakInstantDocumentListeners:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/instant/listeners/InstantDocumentListener;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfFragment;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->instantDocumentListeners:Lcom/pspdfkit/internal/nh;

    .line 5
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->instantDocumentListeners:Lcom/pspdfkit/internal/nh;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->weakInstantDocumentListeners:Ljava/lang/ref/WeakReference;

    const/4 v0, 0x1

    .line 10
    iput-boolean v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->listenToServerChangesWhenVisible:Z

    .line 12
    new-instance v1, Lcom/pspdfkit/instant/ui/InstantPdfFragment$1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment$1;-><init>(Lcom/pspdfkit/instant/ui/InstantPdfFragment;)V

    iput-object v1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->connectivityReceiver:Landroid/content/BroadcastReceiver;

    .line 27
    iput-boolean v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->handleCriticalErrors:Z

    .line 36
    invoke-super {p0}, Lcom/pspdfkit/ui/PdfFragment;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/vu;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/vu;->a(I)V

    return-void
.end method

.method private static filterList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "TT;>;",
            "Ljava/util/List<",
            "TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    .line 4
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 5
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 6
    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 7
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private handleInstantError(Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 6

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->handleCriticalErrors:Z

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/instant/exceptions/InstantException;->getErrorCode()Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->OLD_CLIENT:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    if-eq v0, v1, :cond_1

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/instant/exceptions/InstantException;->getErrorCode()Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    move-result-object v0

    sget-object v2, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->OLD_SERVER:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    if-ne v0, v2, :cond_3

    .line 5
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->getDocument()Lcom/pspdfkit/instant/document/InstantPdfDocument;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 7
    invoke-interface {v0, v2}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->setListenToServerChanges(Z)V

    const-wide v3, 0x7fffffffffffffffL

    .line 8
    invoke-interface {v0, v3, v4}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->setDelayForSyncingLocalChanges(J)V

    .line 12
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/instant/exceptions/InstantException;->getErrorCode()Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    move-result-object p1

    if-ne p1, v1, :cond_3

    iget-object p1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->errorDialog:Landroid/app/AlertDialog;

    if-nez p1, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isResumed()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 13
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v0, Lcom/pspdfkit/R$string;->pspdf__update_required:I

    .line 14
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 16
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__update_required_description:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 19
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 20
    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/content/pm/PackageItemInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x0

    .line 22
    invoke-static {v0, v1, v2, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/pspdfkit/R$string;->pspdf__ok:I

    .line 27
    invoke-virtual {p1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/instant/ui/InstantPdfFragment$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/instant/ui/InstantPdfFragment;)V

    .line 28
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 29
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->errorDialog:Landroid/app/AlertDialog;

    :cond_3
    return-void
.end method

.method public static newInstance(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/instant/ui/InstantPdfFragment;
    .locals 3

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 274
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 325
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 327
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 378
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 379
    invoke-interface {p0}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->getInstantDocumentDescriptor()Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getJwt()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 385
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 386
    invoke-static {p1}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->validatedPdfConfiguration(Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    const-string v2, "PSPDFKit.Configuration"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 387
    new-instance p1, Lcom/pspdfkit/internal/if;

    .line 389
    invoke-interface {p0}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->getInstantClient()Lcom/pspdfkit/instant/client/InstantClient;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/instant/client/InstantClient;->getServerUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p1, v2, v0}, Lcom/pspdfkit/internal/if;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Instant.InstantDocumentSource"

    .line 390
    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 394
    new-instance p1, Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    invoke-direct {p1}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;-><init>()V

    .line 395
    invoke-virtual {p1, v1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 396
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/ag;->setDocument(Lcom/pspdfkit/document/PdfDocument;)V

    return-object p1

    .line 397
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Document JWT is not available."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method static newInstance(Lcom/pspdfkit/internal/if;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/instant/ui/InstantPdfFragment;
    .locals 3

    const-string v0, "documentSource"

    const-string v1, "argumentName"

    .line 162
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 213
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 215
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 267
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "Instant.InstantDocumentSource"

    .line 268
    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 269
    invoke-static {p1}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->validatedPdfConfiguration(Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p0

    const-string p1, "PSPDFKit.Configuration"

    invoke-virtual {v0, p1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 271
    new-instance p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    invoke-direct {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;-><init>()V

    .line 272
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object p0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/instant/ui/InstantPdfFragment;
    .locals 3

    const-string v0, "serverUrl"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jwt"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 108
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 160
    new-instance v0, Lcom/pspdfkit/internal/if;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/if;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->newInstance(Lcom/pspdfkit/internal/if;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    move-result-object p0

    return-object p0
.end method

.method private refreshListenToServerChangesWhenVisible()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->getDocument()Lcom/pspdfkit/instant/document/InstantPdfDocument;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget-boolean v1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->listenToServerChangesWhenVisible:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->connectivityReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v1, 0x1

    .line 7
    invoke-interface {v0, v1}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->setListenToServerChanges(Z)V

    goto :goto_0

    .line 10
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->connectivityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    const/4 v1, 0x0

    .line 14
    invoke-interface {v0, v1}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->setListenToServerChanges(Z)V

    :goto_0
    return-void
.end method

.method static validatedPdfConfiguration(Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 18

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/PdfConfiguration;)V

    const/4 v2, 0x1

    .line 3
    invoke-virtual {v0, v2}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->autosaveEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    const/4 v3, 0x0

    .line 6
    invoke-virtual {v0, v3}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->automaticallyGenerateLinks(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    .line 9
    sget-object v4, Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;->DISABLED:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    invoke-virtual {v0, v4}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->annotationReplyFeatures(Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    const/16 v4, 0xd

    new-array v5, v4, [Lcom/pspdfkit/annotations/AnnotationType;

    .line 12
    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    aput-object v6, v5, v3

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    aput-object v6, v5, v2

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v7, 0x2

    aput-object v6, v5, v7

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->LINE:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v8, 0x3

    aput-object v6, v5, v8

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->SQUARE:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v9, 0x4

    aput-object v6, v5, v9

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->CIRCLE:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v10, 0x5

    aput-object v6, v5, v10

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->POLYLINE:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v11, 0x6

    aput-object v6, v5, v11

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->POLYGON:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v12, 0x7

    aput-object v6, v5, v12

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->HIGHLIGHT:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v13, 0x8

    aput-object v6, v5, v13

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->SQUIGGLY:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v14, 0x9

    aput-object v6, v5, v14

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->STRIKEOUT:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v15, 0xa

    aput-object v6, v5, v15

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->UNDERLINE:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v16, 0xb

    aput-object v6, v5, v16

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v17, 0xc

    aput-object v6, v5, v17

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 26
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEditableAnnotationTypes()Ljava/util/List;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->filterList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->editableAnnotationTypes(Ljava/util/List;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    const/16 v5, 0x1a

    new-array v5, v5, [Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 29
    sget-object v6, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->FREETEXT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    aput-object v6, v5, v3

    sget-object v6, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    aput-object v6, v5, v2

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    aput-object v2, v5, v7

    aput-object v2, v5, v8

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MAGIC_INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    aput-object v2, v5, v9

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SIGNATURE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    aput-object v2, v5, v10

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->LINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    aput-object v2, v5, v11

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SQUARE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    aput-object v2, v5, v12

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->CIRCLE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    aput-object v2, v5, v13

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->POLYLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    aput-object v2, v5, v14

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->POLYGON:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    aput-object v2, v5, v15

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->ERASER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    aput-object v2, v5, v16

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->HIGHLIGHT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    aput-object v2, v5, v17

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SQUIGGLY:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    aput-object v2, v5, v4

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->STRIKEOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/16 v4, 0xe

    aput-object v2, v5, v4

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->UNDERLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/16 v4, 0xf

    aput-object v2, v5, v4

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->IMAGE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/16 v4, 0x10

    aput-object v2, v5, v4

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->CAMERA:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/16 v4, 0x11

    aput-object v2, v5, v4

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->STAMP:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/16 v4, 0x12

    aput-object v2, v5, v4

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INSTANT_COMMENT_MARKER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/16 v4, 0x13

    aput-object v2, v5, v4

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INSTANT_HIGHLIGHT_COMMENT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/16 v4, 0x14

    aput-object v2, v5, v4

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_DISTANCE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/16 v4, 0x15

    aput-object v2, v5, v4

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_PERIMETER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/16 v4, 0x16

    aput-object v2, v5, v4

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_ELLIPSE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/16 v4, 0x17

    aput-object v2, v5, v4

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_POLYGON:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/16 v4, 0x18

    aput-object v2, v5, v4

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_RECT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/16 v4, 0x19

    aput-object v2, v5, v4

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledAnnotationTools()Ljava/util/List;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->filterList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->enabledAnnotationTools(Ljava/util/List;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    .line 59
    invoke-virtual {v0, v3}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->undoEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    .line 62
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->disableAnnotationRotation()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    .line 64
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->build()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addInstantDocumentListener(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->instantDocumentListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;
    .locals 3

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->getDocument()Lcom/pspdfkit/instant/document/InstantPdfDocument;

    move-result-object v1

    .line 3
    new-instance v2, Lcom/pspdfkit/internal/pe;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    .line 4
    :cond_0
    invoke-interface {v1}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->getInstantDocumentDescriptor()Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    move-result-object v1

    :goto_0
    invoke-direct {v2, v0, v1}, Lcom/pspdfkit/internal/pe;-><init>(Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;)V

    return-object v2
.end method

.method public bridge synthetic getDocument()Lcom/pspdfkit/document/PdfDocument;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->getDocument()Lcom/pspdfkit/instant/document/InstantPdfDocument;

    move-result-object v0

    return-object v0
.end method

.method public getDocument()Lcom/pspdfkit/instant/document/InstantPdfDocument;
    .locals 2

    .line 2
    invoke-super {p0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 4
    :cond_0
    instance-of v1, v0, Lcom/pspdfkit/instant/document/InstantPdfDocument;

    if-eqz v1, :cond_1

    .line 8
    check-cast v0, Lcom/pspdfkit/instant/document/InstantPdfDocument;

    return-object v0

    .line 9
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Wrong document type loaded in instant fragment. InstantPdfDocument was expected!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected getDocumentLoadingProgressObservables()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Ljava/lang/Double;",
            ">;>;"
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-static {}, Lio/reactivex/rxjava3/subjects/PublishSubject;->create()Lio/reactivex/rxjava3/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->loadingProgressSubject:Lio/reactivex/rxjava3/subjects/PublishSubject;

    .line 3
    sget-object v1, Lio/reactivex/rxjava3/core/BackpressureStrategy;->LATEST:Lio/reactivex/rxjava3/core/BackpressureStrategy;

    .line 4
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/PublishSubject;->toFlowable(Lio/reactivex/rxjava3/core/BackpressureStrategy;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    const-wide/16 v1, 0x0

    .line 5
    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Flowable;->startWithItem(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    .line 6
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 9
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getUndoManager()Lcom/pspdfkit/undo/UndoManager;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Instant does not support undo!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method synthetic lambda$handleInstantError$3$com-pspdfkit-instant-ui-InstantPdfFragment(Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->errorDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method synthetic lambda$onCreateView$0$com-pspdfkit-instant-ui-InstantPdfFragment(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getOverlaidAnnotationTypes()Ljava/util/EnumSet;

    move-result-object p1

    .line 2
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3
    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->setOverlaidAnnotationTypes(Ljava/util/EnumSet;)V

    :cond_0
    return-void
.end method

.method synthetic lambda$openDocumentAsync$1$com-pspdfkit-instant-ui-InstantPdfFragment(Lcom/pspdfkit/instant/client/InstantProgress;)Lcom/pspdfkit/instant/client/InstantProgress;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->loadingProgressSubject:Lio/reactivex/rxjava3/subjects/PublishSubject;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/instant/client/InstantProgress;->getCurrentProgress()I

    move-result v1

    int-to-double v1, v1

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 5
    :cond_0
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method synthetic lambda$openDocumentAsync$2$com-pspdfkit-instant-ui-InstantPdfFragment()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->loadingProgressSubject:Lio/reactivex/rxjava3/subjects/PublishSubject;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lio/reactivex/rxjava3/subjects/PublishSubject;->onComplete()V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->loadingProgressSubject:Lio/reactivex/rxjava3/subjects/PublishSubject;

    .line 6
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2
    invoke-virtual {p0, p0}, Lcom/pspdfkit/ui/PdfFragment;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    return-void
.end method

.method public onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isModified()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    :cond_0
    return-void
.end method

.method public onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->onAttach(Landroid/content/Context;)V

    .line 3
    new-instance p1, Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->instantDocumentListeners:Lcom/pspdfkit/internal/nh;

    invoke-direct {p1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->weakInstantDocumentListeners:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public onAuthenticationFailed(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 2

    .line 1
    invoke-direct {p0, p2}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->handleInstantError(Lcom/pspdfkit/instant/exceptions/InstantException;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->weakInstantDocumentListeners:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nh;

    if-nez v0, :cond_0

    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 7
    invoke-interface {v1, p1, p2}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onAuthenticationFailed(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onAuthenticationFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->weakInstantDocumentListeners:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nh;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 4
    invoke-interface {v1, p1, p2}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onAuthenticationFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->onCreate(Landroid/os/Bundle;)V

    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    sget v0, Lcom/pspdfkit/internal/wl;->b:I

    const-string v0, "context"

    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 60
    invoke-static {p1}, Lcom/pspdfkit/internal/wl;->a(Landroid/content/Context;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->onErrorComplete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 62
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V

    .line 63
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->isInitialized()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 64
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;

    const-string v0, "PSPDFKit is not initialized!"

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 65
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->documentSource:Lcom/pspdfkit/internal/if;

    if-nez p1, :cond_3

    .line 66
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "Instant.InstantDocumentSource"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/if;

    iput-object p1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->documentSource:Lcom/pspdfkit/internal/if;

    if-eqz p1, :cond_2

    goto :goto_1

    .line 68
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Document descriptor is missing."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/internal/ag;->getViewCoordinator()Lcom/pspdfkit/internal/xm;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/se;

    .line 4
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v3

    invoke-direct {v1, v2, p0, v3}, Lcom/pspdfkit/internal/se;-><init>(Landroid/content/Context;Lcom/pspdfkit/instant/ui/InstantPdfFragment;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    .line 5
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/se;)V

    .line 9
    invoke-super {p0, p1, p2, p3}, Lcom/pspdfkit/ui/PdfFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object p2

    invoke-interface {p2}, Lcom/pspdfkit/internal/ag;->getViewCoordinator()Lcom/pspdfkit/internal/xm;

    move-result-object p2

    new-instance p3, Lcom/pspdfkit/instant/ui/InstantPdfFragment$$ExternalSyntheticLambda0;

    invoke-direct {p3, p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/instant/ui/InstantPdfFragment;)V

    const/4 v0, 0x0

    .line 13
    invoke-virtual {p2, p3, v0}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-object p1
.end method

.method public onDestroy()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->getDocument()Lcom/pspdfkit/instant/document/InstantPdfDocument;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0, p0}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->removeInstantDocumentListener(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V

    .line 5
    :cond_0
    invoke-virtual {p0, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 6
    invoke-super {p0}, Lcom/pspdfkit/ui/PdfFragment;->onDestroy()V

    return-void
.end method

.method public onDetach()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/PdfFragment;->onDetach()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->instantDocumentListeners:Lcom/pspdfkit/internal/nh;

    return-void
.end method

.method public onDocumentCorrupted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->weakInstantDocumentListeners:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nh;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 4
    invoke-interface {v1, p1}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onDocumentCorrupted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onDocumentInvalidated(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->weakInstantDocumentListeners:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nh;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 4
    invoke-interface {v1, p1}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onDocumentInvalidated(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->getDocument()Lcom/pspdfkit/instant/document/InstantPdfDocument;

    move-result-object p1

    .line 5
    invoke-interface {p1, p0}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->addInstantDocumentListener(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V

    .line 8
    invoke-direct {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->refreshListenToServerChangesWhenVisible()V

    return-void
.end method

.method public onDocumentStateChanged(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/document/InstantDocumentState;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->weakInstantDocumentListeners:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nh;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 4
    invoke-interface {v1, p1, p2}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onDocumentStateChanged(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/document/InstantDocumentState;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/PdfFragment;->onResume()V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->refreshListenToServerChangesWhenVisible()V

    return-void
.end method

.method public onStop()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/ui/PdfFragment;->onStop()V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->refreshListenToServerChangesWhenVisible()V

    return-void
.end method

.method public onSyncError(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 2

    .line 1
    invoke-direct {p0, p2}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->handleInstantError(Lcom/pspdfkit/instant/exceptions/InstantException;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->weakInstantDocumentListeners:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nh;

    if-nez v0, :cond_0

    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 7
    invoke-interface {v1, p1, p2}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onSyncError(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onSyncFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->weakInstantDocumentListeners:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nh;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 4
    invoke-interface {v1, p1}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onSyncFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onSyncStarted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->weakInstantDocumentListeners:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nh;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 4
    invoke-interface {v1, p1}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onSyncStarted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected openDocumentAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "+",
            "Lcom/pspdfkit/document/PdfDocument;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->documentSource:Lcom/pspdfkit/internal/if;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Document source was not initialized!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0

    .line 5
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->documentSource:Lcom/pspdfkit/internal/if;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/if;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pspdfkit/instant/client/InstantClient;->create(Landroid/content/Context;Ljava/lang/String;)Lcom/pspdfkit/instant/client/InstantClient;

    move-result-object v0

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->documentSource:Lcom/pspdfkit/internal/if;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/if;->b()Ljava/lang/String;

    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Lcom/pspdfkit/instant/client/InstantClient;->getInstantDocumentDescriptorForJwt(Ljava/lang/String;)Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    move-result-object v0

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->documentSource:Lcom/pspdfkit/internal/if;

    .line 10
    invoke-virtual {v1}, Lcom/pspdfkit/internal/if;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->downloadDocumentAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/instant/ui/InstantPdfFragment$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/instant/ui/InstantPdfFragment;)V

    .line 11
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Flowable;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v1

    .line 19
    invoke-virtual {v1}, Lio/reactivex/rxjava3/core/Flowable;->ignoreElements()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/instant/ui/InstantPdfFragment$$ExternalSyntheticLambda3;

    invoke-direct {v2, p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/instant/ui/InstantPdfFragment;)V

    .line 20
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Completable;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->documentSource:Lcom/pspdfkit/internal/if;

    .line 28
    invoke-virtual {v2}, Lcom/pspdfkit/internal/if;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->openDocumentAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/SingleSource;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public removeInstantDocumentListener(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->instantDocumentListeners:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public save()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->getDocument()Lcom/pspdfkit/instant/document/InstantPdfDocument;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->weakInstantDocumentListeners:Ljava/lang/ref/WeakReference;

    .line 4
    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/nh;

    .line 6
    invoke-interface {v0}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->syncAnnotationsAsync()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Flowable;->ignoreElements()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 8
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->onErrorComplete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v2, Lcom/pspdfkit/instant/ui/InstantPdfFragment$2;

    invoke-direct {v2, p0, v1}, Lcom/pspdfkit/instant/ui/InstantPdfFragment$2;-><init>(Lcom/pspdfkit/instant/ui/InstantPdfFragment;Lcom/pspdfkit/internal/nh;)V

    .line 9
    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/core/CompletableObserver;)V

    :cond_0
    return-void
.end method

.method public setHandleCriticalInstantErrors(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->handleCriticalErrors:Z

    return-void
.end method

.method public setListenToServerChangesWhenVisible(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->listenToServerChangesWhenVisible:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->listenToServerChangesWhenVisible:Z

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->refreshListenToServerChangesWhenVisible()V

    return-void
.end method

.method public setOverlaidAnnotationTypes(Ljava/util/EnumSet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3
    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Instant"

    const-string v2, "Forcing overlay for stamp annotations in InstantPdfFragment"

    .line 4
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    :cond_0
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setOverlaidAnnotationTypes(Ljava/util/EnumSet;)V

    return-void
.end method

.method protected shouldReloadDocument()Z
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->getDocument()Lcom/pspdfkit/instant/document/InstantPdfDocument;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->documentSource:Lcom/pspdfkit/internal/if;

    if-nez v2, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    invoke-virtual {v2}, Lcom/pspdfkit/internal/if;->d()Ljava/lang/String;

    move-result-object v2

    .line 6
    invoke-interface {v0}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->getInstantClient()Lcom/pspdfkit/instant/client/InstantClient;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/instant/client/InstantClient;->getServerUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->documentSource:Lcom/pspdfkit/internal/if;

    .line 8
    invoke-virtual {v2}, Lcom/pspdfkit/internal/if;->a()Ljava/lang/String;

    move-result-object v2

    .line 9
    invoke-interface {v0}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->getInstantDocumentDescriptor()Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getDocumentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->documentSource:Lcom/pspdfkit/internal/if;

    .line 11
    invoke-virtual {v2}, Lcom/pspdfkit/internal/if;->c()Ljava/lang/String;

    move-result-object v2

    .line 12
    invoke-interface {v0}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->getInstantDocumentDescriptor()Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getLayerName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    return v1
.end method

.method public syncAnnotations()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->getDocument()Lcom/pspdfkit/instant/document/InstantPdfDocument;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->syncAnnotationsAsync()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Flowable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_0
    return-void
.end method

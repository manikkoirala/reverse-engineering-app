.class final Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;
.super Lcom/pspdfkit/internal/ui/f;
.source "SourceFile"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final activityListener:Lcom/pspdfkit/instant/ui/InstantPdfActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/instant/ui/InstantPdfActivity;Lcom/pspdfkit/internal/bg;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p1, p2}, Lcom/pspdfkit/internal/ui/f;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/internal/bg;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;->activityListener:Lcom/pspdfkit/instant/ui/InstantPdfActivity;

    return-void
.end method

.method private sanitizePdfActivityConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    invoke-direct {v0, p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->disableDocumentEditor()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->disableBookmarkEditing()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->disableBookmarkList()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->setRedactionUiEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object v0

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->validatedPdfConfiguration(Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->configuration(Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object p1

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method protected removeListeners(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/ui/f;->removeListeners(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 2
    check-cast p1, Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;->activityListener:Lcom/pspdfkit/instant/ui/InstantPdfActivity;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->removeInstantDocumentListener(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V

    return-void
.end method

.method protected requirePdfParameters()Landroid/os/Bundle;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ui/f;->activity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PSPDF.InternalExtras"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "Instant.InstantDocumentSource"

    const-string v2, "PSPDF.Configuration"

    if-eqz v0, :cond_2

    .line 3
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 4
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    .line 21
    :cond_0
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    if-eqz v1, :cond_1

    .line 23
    invoke-direct {p0, v1}, Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;->sanitizePdfActivityConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    return-object v0

    .line 24
    :cond_2
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v0, :cond_4

    .line 28
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "- Document source was not set.\n"

    .line 29
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    :cond_3
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "- No configuration was passed.\n"

    .line 32
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    const-string v0, "- Extras bundle was missing entirely.\n"

    .line 33
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    :cond_5
    :goto_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InstantPdfActivity was not initialized with proper arguments:\n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;->sanitizePdfActivityConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p1

    invoke-super {p0, p1}, Lcom/pspdfkit/internal/ui/f;->setConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    return-void
.end method

.method protected setDocument(Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "Instant.InstantDocumentSource"

    .line 12
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/if;

    .line 14
    invoke-virtual {p0, p1}, Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;->setDocument(Lcom/pspdfkit/internal/if;)V

    return-void
.end method

.method protected setDocument(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "setDocument() may only be called from the UI thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 2
    instance-of v0, p1, Lcom/pspdfkit/instant/document/InstantPdfDocument;

    if-eqz v0, :cond_0

    .line 6
    check-cast p1, Lcom/pspdfkit/instant/document/InstantPdfDocument;

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    .line 8
    invoke-static {p1, v0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->newInstance(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    move-result-object p1

    .line 10
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/f;->setFragment(Lcom/pspdfkit/ui/PdfFragment;)V

    return-void

    .line 11
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Only InstantPdfDocument can be set to instant fragment!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setDocument(Lcom/pspdfkit/internal/if;)V
    .locals 2

    .line 15
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "setDocument() may only be called from the UI thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ui/f;->getConfiguration()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    .line 19
    invoke-static {p1, v0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->newInstance(Lcom/pspdfkit/internal/if;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    move-result-object p1

    .line 21
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ui/f;->setFragment(Lcom/pspdfkit/ui/PdfFragment;)V

    return-void
.end method

.method protected setupListeners(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/ui/f;->setupListeners(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 2
    check-cast p1, Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;->activityListener:Lcom/pspdfkit/instant/ui/InstantPdfActivity;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->addInstantDocumentListener(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V

    return-void
.end method

.class public Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final PARAM_INSTANT_DOCUMENT_SOURCE:Ljava/lang/String; = "Instant.InstantDocumentSource"


# instance fields
.field private activityClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lcom/pspdfkit/ui/PdfActivity;",
            ">;"
        }
    .end annotation
.end field

.field private configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

.field private final context:Landroid/content/Context;

.field private final documentSource:Lcom/pspdfkit/internal/if;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/if;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->context:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->documentSource:Lcom/pspdfkit/internal/if;

    return-void
.end method

.method static checkAndAdjustConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
    .locals 2

    if-eqz p0, :cond_1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isContentEditingEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 6
    :cond_0
    new-instance v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    invoke-direct {v0, p0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    new-instance v1, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/PdfConfiguration;)V

    .line 8
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->disableContentEditing()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    move-result-object p0

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->build()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p0

    .line 10
    invoke-virtual {v0, p0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->configuration(Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object p0

    .line 13
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p0

    :cond_1
    :goto_0
    return-object p0
.end method

.method public static fromInstantDocument(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;
    .locals 3

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "instantServerUrl"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jwt"

    .line 108
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 160
    new-instance v0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;

    new-instance v1, Lcom/pspdfkit/internal/if;

    invoke-direct {v1, p1, p2}, Lcom/pspdfkit/internal/if;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/if;)V

    return-object v0
.end method


# virtual methods
.method public activityClass(Ljava/lang/Class;)Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/pspdfkit/ui/PdfActivity;",
            ">;)",
            "Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 1
    const-class v0, Lcom/pspdfkit/instant/ui/InstantPdfActivity;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Passed activity class must extend InstantPdfActivity!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 4
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->activityClass:Ljava/lang/Class;

    return-object p0
.end method

.method public activityClass(Lkotlin/reflect/KClass;)Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/KClass<",
            "+",
            "Lcom/pspdfkit/ui/PdfActivity;",
            ">;)",
            "Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 5
    invoke-static {p1}, Lkotlin/jvm/JvmClassMappingKt;->getJavaClass(Lkotlin/reflect/KClass;)Ljava/lang/Class;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->activityClass(Ljava/lang/Class;)Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;

    move-result-object p1

    return-object p1
.end method

.method public build()Landroid/content/Intent;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->activityClass:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-class v0, Lcom/pspdfkit/instant/ui/InstantPdfActivity;

    iput-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->activityClass:Ljava/lang/Class;

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    if-nez v0, :cond_1

    .line 4
    new-instance v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    iget-object v1, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Landroid/content/Context;)V

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->disableContentEditing()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 9
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->activityClass:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 12
    iget-object v2, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    const-string v3, "PSPDF.Configuration"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 13
    iget-object v2, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->documentSource:Lcom/pspdfkit/internal/if;

    const-string v3, "Instant.InstantDocumentSource"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "PSPDF.InternalExtras"

    .line 19
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    return-object v0
.end method

.method public configuration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->checkAndAdjustConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->configuration:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    return-object p0
.end method

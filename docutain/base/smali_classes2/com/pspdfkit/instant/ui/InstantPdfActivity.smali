.class public Lcom/pspdfkit/instant/ui/InstantPdfActivity;
.super Lcom/pspdfkit/ui/PdfActivity;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/instant/listeners/InstantPdfActivityListener;


# instance fields
.field private implementation:Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/PdfActivity;-><init>()V

    return-void
.end method

.method public static showInstantDocument(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 3

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "serverUrl"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jwt"

    .line 108
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 160
    invoke-static {p0, p1, p2}, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->fromInstantDocument(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;

    move-result-object p1

    .line 161
    invoke-virtual {p1, p3}, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->configuration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;

    move-result-object p1

    .line 162
    invoke-virtual {p1}, Lcom/pspdfkit/instant/ui/InstantPdfActivityIntentBuilder;->build()Landroid/content/Intent;

    move-result-object p1

    .line 164
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected createImplementation()Lcom/pspdfkit/internal/ui/f;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivity;->implementation:Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;

    iget-object v1, p0, Lcom/pspdfkit/ui/PdfActivity;->internalPdfUi:Lcom/pspdfkit/ui/PdfActivity$InternalPdfUiImpl;

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;-><init>(Lcom/pspdfkit/instant/ui/InstantPdfActivity;Lcom/pspdfkit/internal/bg;)V

    iput-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivity;->implementation:Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivity;->implementation:Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;

    return-object v0
.end method

.method public bridge synthetic getDocument()Lcom/pspdfkit/document/PdfDocument;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfActivity;->getDocument()Lcom/pspdfkit/instant/document/InstantPdfDocument;

    move-result-object v0

    return-object v0
.end method

.method public getDocument()Lcom/pspdfkit/instant/document/InstantPdfDocument;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfActivity;->getPdfFragment()Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/instant/ui/InstantPdfFragment;->getDocument()Lcom/pspdfkit/instant/document/InstantPdfDocument;

    move-result-object v0

    return-object v0
.end method

.method public getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string v1, "DocumentCoordinator is not supported when using InstantPdfActivity, use PdfActivity instead!"

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getPdfFragment()Lcom/pspdfkit/instant/ui/InstantPdfFragment;
    .locals 2

    .line 2
    invoke-super {p0}, Lcom/pspdfkit/ui/PdfActivity;->getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    .line 3
    instance-of v1, v0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    if-eqz v1, :cond_0

    .line 7
    check-cast v0, Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    return-object v0

    .line 8
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Instant activity has wrong fragment type. InstantPdfFragment was expected!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfActivity;->getPdfFragment()Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    move-result-object v0

    return-object v0
.end method

.method public onAuthenticationFailed(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 0

    return-void
.end method

.method public onAuthenticationFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onDocumentCorrupted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentInvalidated(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentStateChanged(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/document/InstantDocumentState;)V
    .locals 0

    return-void
.end method

.method public onSyncError(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 0

    return-void
.end method

.method public onSyncFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 0

    return-void
.end method

.method public onSyncStarted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 0

    return-void
.end method

.method public requirePdfFragment()Lcom/pspdfkit/instant/ui/InstantPdfFragment;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfActivity;->getPdfFragment()Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic requirePdfFragment()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/instant/ui/InstantPdfActivity;->requirePdfFragment()Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    move-result-object v0

    return-object v0
.end method

.method public setDocument(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/ui/InstantPdfActivity;->implementation:Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;

    new-instance v1, Lcom/pspdfkit/internal/if;

    invoke-direct {v1, p1, p2}, Lcom/pspdfkit/internal/if;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/instant/ui/InstantPdfUiImpl;->setDocument(Lcom/pspdfkit/internal/if;)V

    return-void
.end method

.method public setDocumentFromDataProvider(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)V
    .locals 0

    .line 1
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string p2, "setDocumentFromDataProvider() may not be called when using InstantPdfActivity, use PdfActivity instead!"

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setDocumentFromDataProviders(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/providers/DataProvider;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string p2, "setDocumentFromDataProviders() may not be called when using InstantPdfActivity, use PdfActivity instead!"

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setDocumentFromUri(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string p2, "setDocumentFromUri() may not be called when using InstantPdfActivity, use PdfActivity instead!"

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setDocumentFromUris(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string p2, "setDocumentFromUris() may not be called when using InstantPdfActivity, use PdfActivity instead!"

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

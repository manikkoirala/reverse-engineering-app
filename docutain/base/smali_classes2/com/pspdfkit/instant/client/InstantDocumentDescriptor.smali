.class public final Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/vf;


# direct methods
.method constructor <init>(Lcom/pspdfkit/instant/client/InstantClient;Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/vf;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/vf;-><init>(Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;Lcom/pspdfkit/instant/client/InstantClient;Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;)V

    iput-object v0, p0, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->a:Lcom/pspdfkit/internal/vf;

    return-void
.end method


# virtual methods
.method public declared-synchronized downloadDocumentAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Lcom/pspdfkit/instant/client/InstantProgress;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->a:Lcom/pspdfkit/internal/vf;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vf;->a(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getCreatorName()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getInternal()Lcom/pspdfkit/internal/vf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDocumentId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->a:Lcom/pspdfkit/internal/vf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInternal()Lcom/pspdfkit/internal/vf;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->a:Lcom/pspdfkit/internal/vf;

    return-object v0
.end method

.method public getJwt()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->a:Lcom/pspdfkit/internal/vf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLayerName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->a:Lcom/pspdfkit/internal/vf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->a:Lcom/pspdfkit/internal/vf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isDownloaded()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->a:Lcom/pspdfkit/internal/vf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->l()Z

    move-result v0

    return v0
.end method

.method public openDocument(Ljava/lang/String;)Lcom/pspdfkit/instant/document/InstantPdfDocument;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->openDocumentAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/instant/document/InstantPdfDocument;

    return-object p1
.end method

.method public openDocumentAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/instant/document/InstantPdfDocument;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->a:Lcom/pspdfkit/internal/vf;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vf;->c(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public removeLocalStorage()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->a:Lcom/pspdfkit/internal/vf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->m()V

    return-void
.end method

.class public final Lcom/pspdfkit/instant/client/InstantClient;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:Ljava/lang/String;

.field private static final f:Ljava/util/HashMap;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/pspdfkit/instant/internal/jni/NativeServerClient;

.field private final d:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/instant/internal/jni/NativeServerClient;->getProtocolVersion()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/instant/client/InstantClient;->e:Ljava/lang/String;

    .line 5
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/pspdfkit/instant/client/InstantClient;->f:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/instant/client/InstantClient;->d:Ljava/util/HashMap;

    .line 14
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "context"

    .line 16
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serverUrl"

    .line 17
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    new-instance v0, Lcom/pspdfkit/internal/ud;

    invoke-static {p1}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/ud;-><init>(Ljava/io/File;)V

    .line 20
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "PSPDFKit-Platform"

    const-string v3, "android"

    .line 21
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    sget-object v2, Lcom/pspdfkit/instant/client/InstantClient;->e:Ljava/lang/String;

    const-string v3, "PSPDFKit-Version"

    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ud;->b(Ljava/util/HashMap;)V

    .line 25
    invoke-static {p2}, Lcom/pspdfkit/internal/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/instant/client/InstantClient;->a:Ljava/lang/String;

    .line 26
    invoke-static {p1}, Lcom/pspdfkit/instant/client/InstantClient;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/instant/client/InstantClient;->b:Ljava/lang/String;

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p2, p1, v0, v2}, Lcom/pspdfkit/instant/internal/jni/NativeServerClient;->create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeHTTPClient;Ljava/lang/String;)Lcom/pspdfkit/instant/internal/jni/NativeServerClientResult;

    move-result-object p1

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeServerClientResult;->isError()Z

    move-result p2

    if-nez p2, :cond_0

    .line 32
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeServerClientResult;->value()Lcom/pspdfkit/instant/internal/jni/NativeServerClient;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/instant/client/InstantClient;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerClient;

    return-void

    .line 33
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeServerClientResult;->error()Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)Lcom/pspdfkit/instant/exceptions/InstantException;

    move-result-object p1

    throw p1
.end method

.method static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object p0

    const-string v1, "pspdfkit-instant"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static declared-synchronized create(Landroid/content/Context;Ljava/lang/String;)Lcom/pspdfkit/instant/client/InstantClient;
    .locals 5

    const-class v0, Lcom/pspdfkit/instant/client/InstantClient;

    monitor-enter v0

    :try_start_0
    const-string v1, "Context may not be null."

    const-string v2, "argumentName"

    .line 1
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "Context may not be null."

    const/4 v2, 0x0

    .line 52
    invoke-static {p0, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v1, "Server URL may not be null."

    const-string v3, "argumentName"

    .line 53
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "Server URL may not be null."

    .line 104
    invoke-static {p1, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 105
    invoke-static {p1}, Lcom/pspdfkit/internal/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 106
    sget-object v3, Lcom/pspdfkit/instant/client/InstantClient;->f:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 107
    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/instant/client/InstantClient;

    :cond_0
    if-nez v2, :cond_1

    .line 111
    new-instance v2, Lcom/pspdfkit/instant/client/InstantClient;

    invoke-direct {v2, p0, p1}, Lcom/pspdfkit/instant/client/InstantClient;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 112
    new-instance p0, Ljava/lang/ref/WeakReference;

    invoke-direct {p0, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v1, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit v0

    return-object v2

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static create(Landroid/content/Context;Ljava/net/URL;)Lcom/pspdfkit/instant/client/InstantClient;
    .locals 0

    .line 113
    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/pspdfkit/instant/client/InstantClient;->create(Landroid/content/Context;Ljava/lang/String;)Lcom/pspdfkit/instant/client/InstantClient;

    move-result-object p0

    return-object p0
.end method

.method public static create(Landroid/content/Context;Lokhttp3/HttpUrl;)Lcom/pspdfkit/instant/client/InstantClient;
    .locals 0

    .line 114
    invoke-virtual {p1}, Lokhttp3/HttpUrl;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/pspdfkit/instant/client/InstantClient;->create(Landroid/content/Context;Ljava/lang/String;)Lcom/pspdfkit/instant/client/InstantClient;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getDataPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantClient;->b:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getInstantDocumentDescriptorForJwt(Ljava/lang/String;)Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;
    .locals 5

    monitor-enter p0

    :try_start_0
    const-string v0, "jwt may not be null."

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jwt may not be null."

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    invoke-static {p1}, Lcom/pspdfkit/internal/kf;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/kf;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Lcom/pspdfkit/internal/kf;->a()Ljava/lang/String;

    move-result-object v0

    .line 55
    invoke-virtual {p1}, Lcom/pspdfkit/internal/kf;->b()Ljava/lang/String;

    move-result-object v2

    .line 56
    iget-object v3, p0, Lcom/pspdfkit/instant/client/InstantClient;->d:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 57
    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 58
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    :cond_0
    if-eqz v1, :cond_3

    .line 59
    invoke-virtual {p1}, Lcom/pspdfkit/internal/kf;->d()Ljava/lang/String;

    move-result-object p1

    .line 60
    invoke-virtual {v1}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getUserId()Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_1

    .line 61
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    if-eqz v0, :cond_5

    .line 62
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 63
    :cond_2
    new-instance v1, Lcom/pspdfkit/instant/exceptions/InstantException;

    sget-object v2, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->USER_MISMATCH:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 p1, 0x1

    aput-object v0, v3, p1

    const-string p1, "Attempted to obtain a document descriptor for a JWT with the `user_id` claim \'%s\' but the one we have belongs to \'%s\'"

    invoke-direct {v1, v2, p1, v3}, Lcom/pspdfkit/instant/exceptions/InstantException;-><init>(Lcom/pspdfkit/instant/exceptions/InstantErrorCode;Ljava/lang/String;[Ljava/lang/Object;)V

    throw v1

    .line 70
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantClient;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerClient;

    .line 71
    invoke-virtual {p1}, Lcom/pspdfkit/internal/kf;->c()Lcom/pspdfkit/instant/internal/jni/NativeInstantJWT;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/instant/internal/jni/NativeServerClient;->getLayerForJwt(Lcom/pspdfkit/instant/internal/jni/NativeInstantJWT;)Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerResult;

    move-result-object p1

    .line 72
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerResult;->isError()Z

    move-result v0

    if-nez v0, :cond_6

    .line 76
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerResult;->value()Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    move-result-object p1

    .line 77
    new-instance v1, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;-><init>(Lcom/pspdfkit/instant/client/InstantClient;Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;)V

    .line 78
    invoke-virtual {v1}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getDocumentId()Ljava/lang/String;

    move-result-object p1

    .line 79
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantClient;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-nez v0, :cond_4

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 82
    iget-object v2, p0, Lcom/pspdfkit/instant/client/InstantClient;->d:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    :cond_4
    invoke-virtual {v1}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getLayerName()Ljava/lang/String;

    move-result-object p1

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    :goto_0
    monitor-exit p0

    return-object v1

    .line 84
    :cond_6
    :try_start_1
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerResult;->error()Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)Lcom/pspdfkit/instant/exceptions/InstantException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getLocalDocumentDescriptors()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantClient;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerClient;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerClient;->listLocalDocuments()Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentListResult;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentListResult;->isError()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 4
    :cond_0
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentListResult;->value()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/instant/internal/jni/NativeLocalServerDocumentLayers;

    .line 9
    invoke-virtual {v2}, Lcom/pspdfkit/instant/internal/jni/NativeLocalServerDocumentLayers;->getDocumentId()Ljava/lang/String;

    move-result-object v3

    .line 10
    iget-object v4, p0, Lcom/pspdfkit/instant/client/InstantClient;->d:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    if-nez v4, :cond_2

    .line 12
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 13
    iget-object v5, p0, Lcom/pspdfkit/instant/client/InstantClient;->d:Ljava/util/HashMap;

    invoke-virtual {v5, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    :cond_2
    invoke-virtual {v2}, Lcom/pspdfkit/instant/internal/jni/NativeLocalServerDocumentLayers;->getLoadedLayers()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    .line 15
    invoke-virtual {v3}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->getLayerName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 19
    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 20
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    :cond_3
    if-nez v6, :cond_4

    .line 24
    new-instance v6, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    invoke-direct {v6, p0, v3}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;-><init>(Lcom/pspdfkit/instant/client/InstantClient;Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;)V

    .line 25
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v6}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    :cond_4
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_5
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getServerUrl()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantClient;->a:Ljava/lang/String;

    return-object v0
.end method

.method public openDocument(Ljava/lang/String;)Lcom/pspdfkit/instant/document/InstantPdfDocument;
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/instant/client/InstantClient;->getInstantDocumentDescriptorForJwt(Ljava/lang/String;)Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->openDocument(Ljava/lang/String;)Lcom/pspdfkit/instant/document/InstantPdfDocument;

    move-result-object p1

    return-object p1
.end method

.method public openDocumentAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/instant/document/InstantPdfDocument;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/instant/client/InstantClient;->getInstantDocumentDescriptorForJwt(Ljava/lang/String;)Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->openDocumentAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public declared-synchronized removeLocalStorage()V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantClient;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantClient;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerClient;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerClient;->removeLocalStorage()Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    .line 4
    :cond_0
    :try_start_1
    invoke-static {v0}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)Lcom/pspdfkit/instant/exceptions/InstantException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeLocalStorageForDocument(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantClient;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/instant/client/InstantClient;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerClient;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/instant/internal/jni/NativeServerClient;->purgeDocumentWithId(Ljava/lang/String;)Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    monitor-exit p0

    return-void

    .line 4
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)Lcom/pspdfkit/instant/exceptions/InstantException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

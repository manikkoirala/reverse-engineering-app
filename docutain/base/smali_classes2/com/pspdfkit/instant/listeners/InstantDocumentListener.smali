.class public interface abstract Lcom/pspdfkit/instant/listeners/InstantDocumentListener;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onAuthenticationFailed(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
.end method

.method public abstract onAuthenticationFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;Ljava/lang/String;)V
.end method

.method public abstract onDocumentCorrupted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
.end method

.method public abstract onDocumentInvalidated(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
.end method

.method public abstract onDocumentStateChanged(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/document/InstantDocumentState;)V
.end method

.method public abstract onSyncError(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
.end method

.method public abstract onSyncFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
.end method

.method public abstract onSyncStarted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
.end method

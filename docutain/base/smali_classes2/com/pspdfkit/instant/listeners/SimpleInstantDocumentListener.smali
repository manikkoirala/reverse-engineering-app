.class public Lcom/pspdfkit/instant/listeners/SimpleInstantDocumentListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/instant/listeners/InstantDocumentListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAuthenticationFailed(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 0

    return-void
.end method

.method public onAuthenticationFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onDocumentCorrupted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentInvalidated(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 0

    return-void
.end method

.method public onDocumentStateChanged(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/document/InstantDocumentState;)V
    .locals 0

    return-void
.end method

.method public onSyncError(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 0

    return-void
.end method

.method public onSyncFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 0

    return-void
.end method

.method public onSyncStarted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 0

    return-void
.end method

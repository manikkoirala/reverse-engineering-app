.class public interface abstract Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/AnnotationProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$OnNonAnnotationChangeListener;,
        Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$NonAnnotationChange;
    }
.end annotation


# virtual methods
.method public abstract addNonAnnotationChangeListener(Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$OnNonAnnotationChangeListener;)V
.end method

.method public abstract getAnnotationForIdentifier(Ljava/lang/String;)Lcom/pspdfkit/annotations/Annotation;
.end method

.method public abstract getIdentifierForAnnotation(Lcom/pspdfkit/annotations/Annotation;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract hasUnsavedChanges()Z
.end method

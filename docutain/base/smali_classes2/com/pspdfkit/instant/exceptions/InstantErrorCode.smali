.class public final enum Lcom/pspdfkit/instant/exceptions/InstantErrorCode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/instant/exceptions/InstantErrorCode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ALREADY_AUTHENTICATING:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum ALREADY_SYNCING:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum ATTACHMENT_ALREADY_TRANSFERRED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum ATTACHMENT_NOT_LOADED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum ATTACHMENT_TRANSFER_IN_PROGRESS:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum AUTHENTICATION_FAILED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum DATABASE_ERROR:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum INVALID_CUSTOM_DATA:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum INVALID_JSON_STRUCTURE:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum INVALID_JWT:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum INVALID_REQUEST:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum INVALID_SERVER_DATA:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum NO_SUCH_ATTACHMENT:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum NO_SUCH_ATTACHMENT_TRANSFER:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum OLD_CLIENT:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum OLD_SERVER:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum PAYLOAD_SIZE_LIMIT_EXCEEDED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum READ_FAILED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum REQUEST_FAILED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum SERVER_UUID_PENDING:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum UNKNOWN:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum UNMANAGED_ANNOTATION:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum USER_CANCELLED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum USER_MISMATCH:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field public static final enum WRITE_FAILED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

.field private static final synthetic a:[Lcom/pspdfkit/instant/exceptions/InstantErrorCode;


# direct methods
.method static constructor <clinit>()V
    .locals 27

    .line 1
    new-instance v0, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->UNKNOWN:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 4
    new-instance v1, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v3, "USER_CANCELLED"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->USER_CANCELLED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 21
    new-instance v3, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v5, "AUTHENTICATION_FAILED"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->AUTHENTICATION_FAILED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 31
    new-instance v5, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v7, "ALREADY_AUTHENTICATING"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->ALREADY_AUTHENTICATING:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 34
    new-instance v7, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v9, "ALREADY_SYNCING"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->ALREADY_SYNCING:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 44
    new-instance v9, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v11, "REQUEST_FAILED"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->REQUEST_FAILED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 52
    new-instance v11, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v13, "OLD_CLIENT"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->OLD_CLIENT:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 60
    new-instance v13, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v15, "OLD_SERVER"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->OLD_SERVER:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 68
    new-instance v15, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v14, "INVALID_REQUEST"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->INVALID_REQUEST:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 74
    new-instance v14, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v12, "PAYLOAD_SIZE_LIMIT_EXCEEDED"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->PAYLOAD_SIZE_LIMIT_EXCEEDED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 82
    new-instance v12, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v10, "INVALID_SERVER_DATA"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->INVALID_SERVER_DATA:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 88
    new-instance v10, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v8, "WRITE_FAILED"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->WRITE_FAILED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 94
    new-instance v8, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v6, "READ_FAILED"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->READ_FAILED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 104
    new-instance v6, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v4, "DATABASE_ERROR"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->DATABASE_ERROR:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 111
    new-instance v4, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v2, "SERVER_UUID_PENDING"

    move-object/from16 v16, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->SERVER_UUID_PENDING:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 117
    new-instance v2, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v6, "INVALID_JWT"

    move-object/from16 v17, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->INVALID_JWT:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 128
    new-instance v6, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v4, "USER_MISMATCH"

    move-object/from16 v18, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->USER_MISMATCH:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 136
    new-instance v4, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v2, "ATTACHMENT_NOT_LOADED"

    move-object/from16 v19, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->ATTACHMENT_NOT_LOADED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 144
    new-instance v2, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v6, "NO_SUCH_ATTACHMENT"

    move-object/from16 v20, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->NO_SUCH_ATTACHMENT:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 147
    new-instance v6, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v4, "ATTACHMENT_TRANSFER_IN_PROGRESS"

    move-object/from16 v21, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->ATTACHMENT_TRANSFER_IN_PROGRESS:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 150
    new-instance v4, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v2, "ATTACHMENT_ALREADY_TRANSFERRED"

    move-object/from16 v22, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->ATTACHMENT_ALREADY_TRANSFERRED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 153
    new-instance v2, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v6, "NO_SUCH_ATTACHMENT_TRANSFER"

    move-object/from16 v23, v4

    const/16 v4, 0x15

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->NO_SUCH_ATTACHMENT_TRANSFER:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 158
    new-instance v6, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v4, "UNMANAGED_ANNOTATION"

    move-object/from16 v24, v2

    const/16 v2, 0x16

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->UNMANAGED_ANNOTATION:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 168
    new-instance v2, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v4, "INVALID_JSON_STRUCTURE"

    move-object/from16 v25, v6

    const/16 v6, 0x17

    invoke-direct {v2, v4, v6}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->INVALID_JSON_STRUCTURE:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    .line 176
    new-instance v4, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const-string v6, "INVALID_CUSTOM_DATA"

    move-object/from16 v26, v2

    const/16 v2, 0x18

    invoke-direct {v4, v6, v2}, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->INVALID_CUSTOM_DATA:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const/16 v2, 0x19

    new-array v2, v2, [Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    const/4 v6, 0x0

    aput-object v0, v2, v6

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object v5, v2, v0

    const/4 v0, 0x4

    aput-object v7, v2, v0

    const/4 v0, 0x5

    aput-object v9, v2, v0

    const/4 v0, 0x6

    aput-object v11, v2, v0

    const/4 v0, 0x7

    aput-object v13, v2, v0

    const/16 v0, 0x8

    aput-object v15, v2, v0

    const/16 v0, 0x9

    aput-object v14, v2, v0

    const/16 v0, 0xa

    aput-object v12, v2, v0

    const/16 v0, 0xb

    aput-object v10, v2, v0

    const/16 v0, 0xc

    aput-object v8, v2, v0

    const/16 v0, 0xd

    aput-object v16, v2, v0

    const/16 v0, 0xe

    aput-object v17, v2, v0

    const/16 v0, 0xf

    aput-object v18, v2, v0

    const/16 v0, 0x10

    aput-object v19, v2, v0

    const/16 v0, 0x11

    aput-object v20, v2, v0

    const/16 v0, 0x12

    aput-object v21, v2, v0

    const/16 v0, 0x13

    aput-object v22, v2, v0

    const/16 v0, 0x14

    aput-object v23, v2, v0

    const/16 v0, 0x15

    aput-object v24, v2, v0

    const/16 v0, 0x16

    aput-object v25, v2, v0

    const/16 v0, 0x17

    aput-object v26, v2, v0

    const/16 v0, 0x18

    aput-object v4, v2, v0

    .line 177
    sput-object v2, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->a:[Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/instant/exceptions/InstantErrorCode;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/instant/exceptions/InstantErrorCode;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->a:[Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    invoke-virtual {v0}, [Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    return-object v0
.end method

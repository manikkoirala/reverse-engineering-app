.class public final Lcom/pspdfkit/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f0d0000

.field public static final abc_action_bar_up_container:I = 0x7f0d0001

.field public static final abc_action_menu_item_layout:I = 0x7f0d0002

.field public static final abc_action_menu_layout:I = 0x7f0d0003

.field public static final abc_action_mode_bar:I = 0x7f0d0004

.field public static final abc_action_mode_close_item_material:I = 0x7f0d0005

.field public static final abc_activity_chooser_view:I = 0x7f0d0006

.field public static final abc_activity_chooser_view_list_item:I = 0x7f0d0007

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f0d0008

.field public static final abc_alert_dialog_material:I = 0x7f0d0009

.field public static final abc_alert_dialog_title_material:I = 0x7f0d000a

.field public static final abc_cascading_menu_item_layout:I = 0x7f0d000b

.field public static final abc_dialog_title_material:I = 0x7f0d000c

.field public static final abc_expanded_menu_layout:I = 0x7f0d000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f0d000e

.field public static final abc_list_menu_item_icon:I = 0x7f0d000f

.field public static final abc_list_menu_item_layout:I = 0x7f0d0010

.field public static final abc_list_menu_item_radio:I = 0x7f0d0011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f0d0012

.field public static final abc_popup_menu_item_layout:I = 0x7f0d0013

.field public static final abc_screen_content_include:I = 0x7f0d0014

.field public static final abc_screen_simple:I = 0x7f0d0015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f0d0016

.field public static final abc_screen_toolbar:I = 0x7f0d0017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f0d0018

.field public static final abc_search_view:I = 0x7f0d0019

.field public static final abc_select_dialog_material:I = 0x7f0d001a

.field public static final abc_tooltip:I = 0x7f0d001b

.field public static final custom_dialog:I = 0x7f0d002b

.field public static final design_bottom_navigation_item:I = 0x7f0d002d

.field public static final design_bottom_sheet_dialog:I = 0x7f0d002e

.field public static final design_layout_snackbar:I = 0x7f0d002f

.field public static final design_layout_snackbar_include:I = 0x7f0d0030

.field public static final design_layout_tab_icon:I = 0x7f0d0031

.field public static final design_layout_tab_text:I = 0x7f0d0032

.field public static final design_menu_item_action_area:I = 0x7f0d0033

.field public static final design_navigation_item:I = 0x7f0d0034

.field public static final design_navigation_item_header:I = 0x7f0d0035

.field public static final design_navigation_item_separator:I = 0x7f0d0036

.field public static final design_navigation_item_subheader:I = 0x7f0d0037

.field public static final design_navigation_menu:I = 0x7f0d0038

.field public static final design_navigation_menu_item:I = 0x7f0d0039

.field public static final design_text_input_end_icon:I = 0x7f0d003a

.field public static final design_text_input_start_icon:I = 0x7f0d003b

.field public static final expand_button:I = 0x7f0d0049

.field public static final image_frame:I = 0x7f0d0051

.field public static final m3_alert_dialog:I = 0x7f0d005c

.field public static final m3_alert_dialog_actions:I = 0x7f0d005d

.field public static final m3_alert_dialog_title:I = 0x7f0d005e

.field public static final m3_auto_complete_simple_item:I = 0x7f0d005f

.field public static final m3_side_sheet_dialog:I = 0x7f0d0060

.field public static final material_chip_input_combo:I = 0x7f0d0064

.field public static final material_clock_display:I = 0x7f0d0065

.field public static final material_clock_display_divider:I = 0x7f0d0066

.field public static final material_clock_period_toggle:I = 0x7f0d0067

.field public static final material_clock_period_toggle_land:I = 0x7f0d0068

.field public static final material_clockface_textview:I = 0x7f0d0069

.field public static final material_clockface_view:I = 0x7f0d006a

.field public static final material_radial_view_group:I = 0x7f0d006b

.field public static final material_textinput_timepicker:I = 0x7f0d006c

.field public static final material_time_chip:I = 0x7f0d006d

.field public static final material_time_input:I = 0x7f0d006e

.field public static final material_timepicker:I = 0x7f0d006f

.field public static final material_timepicker_dialog:I = 0x7f0d0070

.field public static final material_timepicker_textinput_display:I = 0x7f0d0071

.field public static final mtrl_alert_dialog:I = 0x7f0d0079

.field public static final mtrl_alert_dialog_actions:I = 0x7f0d007a

.field public static final mtrl_alert_dialog_title:I = 0x7f0d007b

.field public static final mtrl_alert_select_dialog_item:I = 0x7f0d007c

.field public static final mtrl_alert_select_dialog_multichoice:I = 0x7f0d007d

.field public static final mtrl_alert_select_dialog_singlechoice:I = 0x7f0d007e

.field public static final mtrl_auto_complete_simple_item:I = 0x7f0d007f

.field public static final mtrl_calendar_day:I = 0x7f0d0080

.field public static final mtrl_calendar_day_of_week:I = 0x7f0d0081

.field public static final mtrl_calendar_days_of_week:I = 0x7f0d0082

.field public static final mtrl_calendar_horizontal:I = 0x7f0d0083

.field public static final mtrl_calendar_month:I = 0x7f0d0084

.field public static final mtrl_calendar_month_labeled:I = 0x7f0d0085

.field public static final mtrl_calendar_month_navigation:I = 0x7f0d0086

.field public static final mtrl_calendar_months:I = 0x7f0d0087

.field public static final mtrl_calendar_vertical:I = 0x7f0d0088

.field public static final mtrl_calendar_year:I = 0x7f0d0089

.field public static final mtrl_layout_snackbar:I = 0x7f0d008a

.field public static final mtrl_layout_snackbar_include:I = 0x7f0d008b

.field public static final mtrl_navigation_rail_item:I = 0x7f0d008c

.field public static final mtrl_picker_actions:I = 0x7f0d008d

.field public static final mtrl_picker_dialog:I = 0x7f0d008e

.field public static final mtrl_picker_fullscreen:I = 0x7f0d008f

.field public static final mtrl_picker_header_dialog:I = 0x7f0d0090

.field public static final mtrl_picker_header_fullscreen:I = 0x7f0d0091

.field public static final mtrl_picker_header_selection_text:I = 0x7f0d0092

.field public static final mtrl_picker_header_title_text:I = 0x7f0d0093

.field public static final mtrl_picker_header_toggle:I = 0x7f0d0094

.field public static final mtrl_picker_text_input_date:I = 0x7f0d0095

.field public static final mtrl_picker_text_input_date_range:I = 0x7f0d0096

.field public static final mtrl_search_bar:I = 0x7f0d0097

.field public static final mtrl_search_view:I = 0x7f0d0098

.field public static final notification_action:I = 0x7f0d009c

.field public static final notification_action_tombstone:I = 0x7f0d009d

.field public static final notification_media_action:I = 0x7f0d009e

.field public static final notification_media_cancel_action:I = 0x7f0d009f

.field public static final notification_template_big_media:I = 0x7f0d00a0

.field public static final notification_template_big_media_custom:I = 0x7f0d00a1

.field public static final notification_template_big_media_narrow:I = 0x7f0d00a2

.field public static final notification_template_big_media_narrow_custom:I = 0x7f0d00a3

.field public static final notification_template_custom_big:I = 0x7f0d00a4

.field public static final notification_template_icon_group:I = 0x7f0d00a5

.field public static final notification_template_lines_media:I = 0x7f0d00a6

.field public static final notification_template_media:I = 0x7f0d00a7

.field public static final notification_template_media_custom:I = 0x7f0d00a8

.field public static final notification_template_part_chronometer:I = 0x7f0d00a9

.field public static final notification_template_part_time:I = 0x7f0d00aa

.field public static final preference:I = 0x7f0d00ae

.field public static final preference_category:I = 0x7f0d00af

.field public static final preference_category_material:I = 0x7f0d00b0

.field public static final preference_dialog_edittext:I = 0x7f0d00b1

.field public static final preference_dropdown:I = 0x7f0d00b2

.field public static final preference_dropdown_material:I = 0x7f0d00b3

.field public static final preference_information:I = 0x7f0d00b4

.field public static final preference_information_material:I = 0x7f0d00b5

.field public static final preference_list_fragment:I = 0x7f0d00b6

.field public static final preference_material:I = 0x7f0d00b7

.field public static final preference_recyclerview:I = 0x7f0d00b8

.field public static final preference_widget_checkbox:I = 0x7f0d00b9

.field public static final preference_widget_seekbar:I = 0x7f0d00ba

.field public static final preference_widget_seekbar_material:I = 0x7f0d00bb

.field public static final preference_widget_switch:I = 0x7f0d00bc

.field public static final preference_widget_switch_compat:I = 0x7f0d00bd

.field public static final pspdf__action_menu_layout:I = 0x7f0d00bf

.field public static final pspdf__alert_dialog_progress:I = 0x7f0d00c0

.field public static final pspdf__annotation_creator_input_dialog:I = 0x7f0d00c1

.field public static final pspdf__audio_inspector_bar:I = 0x7f0d00c2

.field public static final pspdf__color_palette_view:I = 0x7f0d00c3

.field public static final pspdf__color_picker_detail:I = 0x7f0d00c4

.field public static final pspdf__color_preview_view:I = 0x7f0d00c5

.field public static final pspdf__compare_documents_dialog:I = 0x7f0d00c6

.field public static final pspdf__comparison_hint_view:I = 0x7f0d00c7

.field public static final pspdf__content_editing_bar:I = 0x7f0d00c8

.field public static final pspdf__custom_color_picker:I = 0x7f0d00c9

.field public static final pspdf__custom_stamp_creator_layout:I = 0x7f0d00ca

.field public static final pspdf__document_editor_alert_dialog:I = 0x7f0d00cb

.field public static final pspdf__document_info_group:I = 0x7f0d00cc

.field public static final pspdf__document_info_item:I = 0x7f0d00cd

.field public static final pspdf__document_info_page_binding_item:I = 0x7f0d00ce

.field public static final pspdf__document_info_view:I = 0x7f0d00cf

.field public static final pspdf__document_view:I = 0x7f0d00d0

.field public static final pspdf__draw_electronic_signature_dialog_layout:I = 0x7f0d00d1

.field public static final pspdf__draw_electronic_signature_layout:I = 0x7f0d00d2

.field public static final pspdf__electronic_signature_typing_font_view_holder:I = 0x7f0d00d3

.field public static final pspdf__form_editing_bar:I = 0x7f0d00d4

.field public static final pspdf__gallery_item:I = 0x7f0d00d5

.field public static final pspdf__grid_list_item:I = 0x7f0d00d6

.field public static final pspdf__image_electronic_signature_dialog_layout:I = 0x7f0d00d7

.field public static final pspdf__image_electronic_signature_layout:I = 0x7f0d00d8

.field public static final pspdf__inspector_precision_spinner_item:I = 0x7f0d00d9

.field public static final pspdf__inspector_scale_unit_spinner_item:I = 0x7f0d00da

.field public static final pspdf__list_item_checked:I = 0x7f0d00db

.field public static final pspdf__loading_view:I = 0x7f0d00dc

.field public static final pspdf__loading_view_progress:I = 0x7f0d00dd

.field public static final pspdf__measurement_value_popup:I = 0x7f0d00de

.field public static final pspdf__media_dialog:I = 0x7f0d00df

.field public static final pspdf__note_editor_item_card_layout:I = 0x7f0d00e0

.field public static final pspdf__note_editor_layout:I = 0x7f0d00e1

.field public static final pspdf__note_editor_set_status_dialog_layout:I = 0x7f0d00e2

.field public static final pspdf__note_editor_set_status_dialog_layout_item:I = 0x7f0d00e3

.field public static final pspdf__note_editor_style_box_card_layout:I = 0x7f0d00e4

.field public static final pspdf__note_editor_style_box_header:I = 0x7f0d00e5

.field public static final pspdf__option_picker_custom_value_view:I = 0x7f0d00e6

.field public static final pspdf__option_picker_inspector_view:I = 0x7f0d00e7

.field public static final pspdf__outline_annotation_view:I = 0x7f0d00e8

.field public static final pspdf__outline_bookmarks_list_item:I = 0x7f0d00e9

.field public static final pspdf__outline_bookmarks_name_dialog:I = 0x7f0d00ea

.field public static final pspdf__outline_bookmarks_view:I = 0x7f0d00eb

.field public static final pspdf__outline_list_divider:I = 0x7f0d00ec

.field public static final pspdf__outline_list_view:I = 0x7f0d00ed

.field public static final pspdf__outline_pager_annotation_list_item:I = 0x7f0d00ee

.field public static final pspdf__outline_pager_annotation_page_item:I = 0x7f0d00ef

.field public static final pspdf__outline_pager_list_footer:I = 0x7f0d00f0

.field public static final pspdf__outline_pager_outline_list_item:I = 0x7f0d00f1

.field public static final pspdf__outline_pager_outline_list_no_match:I = 0x7f0d00f2

.field public static final pspdf__outline_view:I = 0x7f0d00f3

.field public static final pspdf__overflow_menu_view:I = 0x7f0d00f4

.field public static final pspdf__page_binding_view:I = 0x7f0d00f5

.field public static final pspdf__page_creator_dialog:I = 0x7f0d00f6

.field public static final pspdf__page_creator_page_color_view:I = 0x7f0d00f7

.field public static final pspdf__page_creator_page_pattern_item:I = 0x7f0d00f8

.field public static final pspdf__password_view:I = 0x7f0d00f9

.field public static final pspdf__pdf_activity:I = 0x7f0d00fa

.field public static final pspdf__pdf_fragment_error_view:I = 0x7f0d00fb

.field public static final pspdf__preview_uri_dialog:I = 0x7f0d00fc

.field public static final pspdf__progress_dialog:I = 0x7f0d00fd

.field public static final pspdf__reader_view:I = 0x7f0d00fe

.field public static final pspdf__recycler_view_with_empty_message:I = 0x7f0d00ff

.field public static final pspdf__redaction_view:I = 0x7f0d0100

.field public static final pspdf__search_footer:I = 0x7f0d0101

.field public static final pspdf__search_item:I = 0x7f0d0102

.field public static final pspdf__search_view_inline:I = 0x7f0d0103

.field public static final pspdf__search_view_modular:I = 0x7f0d0104

.field public static final pspdf__settings_layout:I = 0x7f0d0105

.field public static final pspdf__share_dialog:I = 0x7f0d0106

.field public static final pspdf__signature_info_dialog:I = 0x7f0d0107

.field public static final pspdf__signature_layout_add_new_signature:I = 0x7f0d0108

.field public static final pspdf__signature_list_dialog:I = 0x7f0d0109

.field public static final pspdf__signature_list_item:I = 0x7f0d010a

.field public static final pspdf__signer_list_item:I = 0x7f0d010b

.field public static final pspdf__signer_list_selected_item:I = 0x7f0d010c

.field public static final pspdf__signer_list_view_popup:I = 0x7f0d010d

.field public static final pspdf__stamps_picker_custom_section:I = 0x7f0d010e

.field public static final pspdf__stamps_picker_list_item:I = 0x7f0d010f

.field public static final pspdf__stamps_selection_grid:I = 0x7f0d0110

.field public static final pspdf__tab_item:I = 0x7f0d0111

.field public static final pspdf__thumbnail_grid_item_view:I = 0x7f0d0112

.field public static final pspdf__thumbnail_grid_view:I = 0x7f0d0113

.field public static final pspdf__toolbar_main:I = 0x7f0d0114

.field public static final pspdf__typing_electronic_signature_dialog_layout:I = 0x7f0d0115

.field public static final pspdf__typing_electronic_signature_layout:I = 0x7f0d0116

.field public static final pspdf__uvv_on_error_layout:I = 0x7f0d0117

.field public static final pspdf__uvv_on_loading_layout:I = 0x7f0d0118

.field public static final pspdf__uvv_player_controller:I = 0x7f0d0119

.field public static final pspdf__value_slider:I = 0x7f0d011a

.field public static final pspdf__vertical_scrollbar_indicator:I = 0x7f0d011b

.field public static final pspdf__view_empty_activity:I = 0x7f0d011c

.field public static final pspdf__view_inspector_color_picker:I = 0x7f0d011d

.field public static final pspdf__view_inspector_divider:I = 0x7f0d011e

.field public static final pspdf__view_inspector_font_list_item:I = 0x7f0d011f

.field public static final pspdf__view_inspector_font_picker:I = 0x7f0d0120

.field public static final pspdf__view_inspector_measurement_value:I = 0x7f0d0121

.field public static final pspdf__view_inspector_options_picker:I = 0x7f0d0122

.field public static final pspdf__view_inspector_precision_picker:I = 0x7f0d0123

.field public static final pspdf__view_inspector_scale_calibration_picker:I = 0x7f0d0124

.field public static final pspdf__view_inspector_scale_picker:I = 0x7f0d0125

.field public static final pspdf__view_inspector_slider_picker:I = 0x7f0d0126

.field public static final pspdf__view_inspector_snapping_picker:I = 0x7f0d0127

.field public static final pspdf__view_inspector_text:I = 0x7f0d0128

.field public static final pspdf__view_inspector_toggle_picker:I = 0x7f0d0129

.field public static final pspdf__view_inspector_z_index_picker:I = 0x7f0d012a

.field public static final pspdf__view_options_picker_item:I = 0x7f0d012b

.field public static final pspdf__view_pager_tab_view:I = 0x7f0d012c

.field public static final pspdf__view_settings_mode_picker:I = 0x7f0d012d

.field public static final pspdf__view_settings_mode_picker_item:I = 0x7f0d012e

.field public static final pspdf__you_tube_activity:I = 0x7f0d012f

.field public static final select_dialog_item_material:I = 0x7f0d0137

.field public static final select_dialog_multichoice_material:I = 0x7f0d0138

.field public static final select_dialog_singlechoice_material:I = 0x7f0d0139

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0d013e


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public abstract Lcom/pspdfkit/annotations/ShapeAnnotation;
.super Lcom/pspdfkit/annotations/Annotation;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/ResizableAnnotation;


# direct methods
.method constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/Annotation;-><init>(I)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/p1;Z)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/Annotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/ShapeAnnotation;->setMeasurementScale(Lcom/pspdfkit/annotations/measurements/Scale;)V

    .line 2
    invoke-virtual {p0, p2}, Lcom/pspdfkit/annotations/ShapeAnnotation;->setMeasurementPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    .line 3
    sget p1, Lcom/pspdfkit/internal/ao;->a:I

    const-string p1, "annotation"

    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1308
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object p1

    .line 1310
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/mt;->a()Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    invoke-virtual {p2}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/ui/fonts/Font;

    invoke-virtual {p2}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object p2

    const/16 v0, 0x3e9

    .line 1311
    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 1315
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object p1

    const/high16 p2, 0x41900000    # 18.0f

    .line 1317
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    const/16 v0, 0x3ea

    .line 1318
    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 1322
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object p1

    .line 1324
    sget-object p2, Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextTextJustification;->CENTER:Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextTextJustification;

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    int-to-byte p2, p2

    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p2

    const/16 v0, 0x3ed

    .line 1325
    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 1326
    sget p1, Lcom/pspdfkit/internal/ao;->a:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    const/high16 p1, 0x40000000    # 2.0f

    .line 1327
    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/Annotation;->setBorderWidth(F)V

    return-void
.end method

.method public getMeasurementPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getMeasurementPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v0

    return-object v0
.end method

.method public getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v0

    return-object v0
.end method

.method public getMinimumSize()Lcom/pspdfkit/utils/Size;
    .locals 5

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/internal/er;->a(Lcom/pspdfkit/annotations/Annotation;)F

    move-result v0

    .line 2
    new-instance v1, Lcom/pspdfkit/utils/Size;

    sget-object v2, Lcom/pspdfkit/annotations/Annotation;->o:Lcom/pspdfkit/utils/Size;

    iget v3, v2, Lcom/pspdfkit/utils/Size;->width:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float v0, v0, v4

    .line 3
    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iget v2, v2, Lcom/pspdfkit/utils/Size;->height:F

    .line 4
    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-direct {v1, v3, v0}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    return-object v1
.end method

.method public isMeasurement()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const-class v1, Lcom/pspdfkit/annotations/measurements/Scale;

    const/16 v2, 0x2afa

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setMeasurementPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->MEASUREMENT_TOOLS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "precision"

    const-string v1, "argumentName"

    .line 6
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 57
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 58
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/pf;->setMeasurementPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    return-void

    .line 59
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Your current license doesn\'t allow for measurement annotations."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setMeasurementScale(Lcom/pspdfkit/annotations/measurements/Scale;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->MEASUREMENT_TOOLS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "scale"

    const-string v1, "argumentName"

    .line 6
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 57
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 58
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/pf;->setMeasurementScale(Lcom/pspdfkit/annotations/measurements/Scale;)V

    return-void

    .line 59
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Your current license doesn\'t allow for measurement annotations."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public updateTransformationProperties(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 3

    const-string v0, "newBoundingBox"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string p1, "oldBoundingBox"

    .line 55
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, p1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method

.class public interface abstract Lcom/pspdfkit/annotations/AnnotationProvider;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;
    }
.end annotation


# static fields
.field public static final ALL_ANNOTATION_TYPES:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/annotations/AnnotationProvider;->ALL_ANNOTATION_TYPES:Ljava/util/EnumSet;

    return-void
.end method


# virtual methods
.method public abstract addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;)V
.end method

.method public abstract addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;I)V
.end method

.method public abstract addAnnotationToPageAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;
.end method

.method public abstract addAnnotationToPageAsync(Lcom/pspdfkit/annotations/Annotation;I)Lio/reactivex/rxjava3/core/Completable;
.end method

.method public abstract addAppearanceStreamGenerator(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;)V
.end method

.method public abstract addAppearanceStreamGenerator(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;Z)V
.end method

.method public abstract addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
.end method

.method public abstract appendAnnotationState(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)V
.end method

.method public abstract appendAnnotationStateAsync(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)Lio/reactivex/rxjava3/core/Completable;
.end method

.method public abstract createAnnotationFromInstantJson(Ljava/lang/String;)Lcom/pspdfkit/annotations/Annotation;
.end method

.method public abstract createAnnotationFromInstantJsonAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAllAnnotationsOfType(Ljava/util/EnumSet;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAllAnnotationsOfType(Ljava/util/EnumSet;II)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;II)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAllAnnotationsOfTypeAsync(Ljava/util/EnumSet;)Lio/reactivex/rxjava3/core/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAllAnnotationsOfTypeAsync(Ljava/util/EnumSet;II)Lio/reactivex/rxjava3/core/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;II)",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAnnotation(II)Lcom/pspdfkit/annotations/Annotation;
.end method

.method public abstract getAnnotationAsync(II)Lio/reactivex/rxjava3/core/Maybe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAnnotationReplies(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAnnotationRepliesAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getAnnotations(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAnnotations(Ljava/util/Collection;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAnnotationsAsync(I)Lio/reactivex/rxjava3/core/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getAnnotationsAsync(Ljava/util/Collection;)Lio/reactivex/rxjava3/core/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getFlattenedAnnotationReplies(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFlattenedAnnotationRepliesAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getReviewHistory(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/note/AnnotationStateChange;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getReviewHistoryAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/note/AnnotationStateChange;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getReviewSummary(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;
.end method

.method public abstract getReviewSummaryAsync(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getZIndex(Lcom/pspdfkit/annotations/Annotation;)I
.end method

.method public abstract getZIndexAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasUnsavedChanges()Z
.end method

.method public abstract moveAnnotation(III)V
.end method

.method public abstract moveAnnotation(Lcom/pspdfkit/annotations/Annotation;I)V
.end method

.method public abstract moveAnnotation(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V
.end method

.method public abstract moveAnnotationAsync(III)Lio/reactivex/rxjava3/core/Completable;
.end method

.method public abstract moveAnnotationAsync(Lcom/pspdfkit/annotations/Annotation;I)Lio/reactivex/rxjava3/core/Completable;
.end method

.method public abstract moveAnnotationAsync(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)Lio/reactivex/rxjava3/core/Completable;
.end method

.method public abstract removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V
.end method

.method public abstract removeAnnotationFromPageAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;
.end method

.method public abstract removeAppearanceStreamGenerator(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;)V
.end method

.method public abstract removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
.end method

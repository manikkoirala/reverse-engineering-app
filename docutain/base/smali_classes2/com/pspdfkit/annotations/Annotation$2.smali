.class Lcom/pspdfkit/annotations/Annotation$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/pf;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/annotations/Annotation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private a:Lio/reactivex/rxjava3/disposables/Disposable;

.field private final b:Lio/reactivex/rxjava3/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/subjects/PublishSubject<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/pspdfkit/internal/ls;

.field final synthetic d:Lcom/pspdfkit/annotations/Annotation;


# direct methods
.method public static synthetic $r8$lambda$iRHBl0h1V8PRsQA68H0ObprIeto(Lcom/pspdfkit/annotations/Annotation$2;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/Annotation$2;->a(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {}, Lio/reactivex/rxjava3/subjects/PublishSubject;->create()Lio/reactivex/rxjava3/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->b:Lio/reactivex/rxjava3/subjects/PublishSubject;

    return-void
.end method

.method private a()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->requireNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object v0

    const-string v1, "The bound native annotation was not attached to a document."

    const-string v2, "message"

    .line 4
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v1, v0

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/high16 v2, -0x80000000

    const/4 v3, 0x0

    .line 7
    invoke-virtual {v0, v3, v2}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    :cond_0
    return-void

    .line 11
    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private synthetic a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object p1, p1, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    .line 14
    check-cast p1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/r1;->j(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method private b()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    monitor-enter v0

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v1}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetl(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x1;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/pspdfkit/internal/x1;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 5
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private c()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    monitor-enter v0

    .line 5
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v1, v1, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-interface {v1, v2}, Lcom/pspdfkit/internal/qf;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v1

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v2}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetl(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x1;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 11
    invoke-virtual {v2}, Lcom/pspdfkit/internal/x1;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    or-int/2addr v1, v2

    .line 12
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 13
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public addOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgeta(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetb(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public adjustBoundsForRotation(F)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->getContentSize(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->needsFlippedContentSize()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    neg-float v2, v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/4 v3, 0x0

    invoke-direct {v1, v3, v3, v2, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object v0, v1

    .line 11
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/RectF;->sort()V

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    .line 13
    invoke-virtual {v1}, Landroid/graphics/RectF;->sort()V

    .line 14
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getRotation()I

    move-result v2

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    .line 17
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-double v4, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double v6, v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    .line 18
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v6

    float-to-double v6, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double v8, v8, v6

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    add-double/2addr v6, v4

    double-to-float v4, v6

    .line 19
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v5

    float-to-double v5, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v7

    mul-double v7, v7, v5

    invoke-static {v7, v8}, Ljava/lang/Math;->abs(D)D

    move-result-wide v5

    .line 20
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    float-to-double v7, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double v2, v2, v7

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    add-double/2addr v2, v5

    double-to-float v0, v2

    mul-float v4, v4, p1

    mul-float v0, v0, p1

    .line 25
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    new-instance v2, Landroid/graphics/RectF;

    .line 26
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    .line 27
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v6

    div-float/2addr v0, v5

    add-float/2addr v6, v0

    .line 28
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v5

    add-float/2addr v5, v4

    .line 29
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    sub-float/2addr v1, v0

    invoke-direct {v2, v3, v6, v5, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 30
    invoke-virtual {p1, v2}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    :cond_1
    return-void
.end method

.method public clearModified()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/p1;->a()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetl(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x1;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/x1;->c()V

    :cond_0
    return-void
.end method

.method public clearTextShouldFit()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string v3, "web/freetext/isFitting"

    .line 6
    invoke-virtual {v0, v3, v1, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->setAdditionalDataBoolean(Ljava/lang/String;Ljava/lang/Boolean;Z)V

    return-void

    .line 7
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t clear shouldFit flag on an annotation that is not attached to a document."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public ensureAnnotationCanBeAttachedToDocument(Lcom/pspdfkit/internal/zf;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0, p1}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$ma(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/zf;)V

    return-void

    .line 8
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Can\'t add an annotation that is already attached to a document."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getAction()Lcom/pspdfkit/annotations/actions/Action;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const-class v1, Lcom/pspdfkit/annotations/actions/Action;

    const/16 v2, 0xbb8

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/actions/Action;

    return-object v0
.end method

.method public getAdditionalAction(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)Lcom/pspdfkit/annotations/actions/Action;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getAdditionalActions()Lcom/pspdfkit/internal/o;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/o;->a(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)Lcom/pspdfkit/annotations/actions/Action;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public getAdditionalActions()Lcom/pspdfkit/internal/o;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const-class v1, Lcom/pspdfkit/internal/o;

    const/16 v2, 0xbb9

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/o;

    return-object v0
.end method

.method public getAdditionalData(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAdditionalDataString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 7
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Can\'t get additional data on an annotation that is not attached to a document."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getAnnotationResource()Lcom/pspdfkit/internal/x1;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetl(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x1;

    move-result-object v0

    return-object v0
.end method

.method public getContentSize()Landroid/graphics/RectF;
    .locals 1

    const/4 v0, 0x0

    .line 7
    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation$2;->getContentSize(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getContentSize(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const-class v1, Landroid/graphics/RectF;

    const/16 v2, 0x16

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    .line 4
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 6
    :cond_0
    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getCopy()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->a()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public getDetachedAnnotationLookupKey()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetg(Lcom/pspdfkit/annotations/Annotation;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getEdgeInsets()Lcom/pspdfkit/utils/EdgeInsets;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    new-instance v1, Lcom/pspdfkit/utils/EdgeInsets;

    invoke-direct {v1}, Lcom/pspdfkit/utils/EdgeInsets;-><init>()V

    const-class v2, Lcom/pspdfkit/utils/EdgeInsets;

    const/16 v3, 0x3ef

    invoke-virtual {v0, v3, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/EdgeInsets;

    return-object v0
.end method

.method public getInReplyToUuid()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInternalDocument()Lcom/pspdfkit/internal/zf;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    return-object v0
.end method

.method public getMeasurementPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x2af9

    const-class v2, Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMeasurementProperties()Lcom/pspdfkit/internal/ri;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetn(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getMeasurementPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v0

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v2}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetn(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v2

    .line 5
    iget-object v3, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    const-string v4, "annotationType"

    .line 6
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    sget-object v4, Lcom/pspdfkit/internal/li$a;->b:[I

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget v3, v4, v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x2

    if-eq v3, v4, :cond_4

    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_2

    const/4 v4, 0x5

    if-eq v3, v4, :cond_1

    move-object v3, v1

    goto :goto_0

    .line 222
    :cond_1
    sget-object v3, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->AREA:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    goto :goto_0

    .line 223
    :cond_2
    sget-object v3, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->AREA:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    goto :goto_0

    .line 224
    :cond_3
    sget-object v3, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->AREA:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    goto :goto_0

    .line 225
    :cond_4
    sget-object v3, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->PERIMETER:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    goto :goto_0

    .line 226
    :cond_5
    sget-object v3, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->DISTANCE:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    :goto_0
    if-eqz v3, :cond_7

    if-eqz v0, :cond_7

    if-nez v2, :cond_6

    goto :goto_1

    .line 227
    :cond_6
    new-instance v1, Lcom/pspdfkit/internal/ri;

    invoke-direct {v1, v2, v0, v3}, Lcom/pspdfkit/internal/ri;-><init>(Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;Lcom/pspdfkit/annotations/measurements/MeasurementMode;)V

    :cond_7
    :goto_1
    return-object v1
.end method

.method public getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const-class v1, Lcom/pspdfkit/annotations/measurements/Scale;

    const/16 v2, 0x2afa

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/measurements/Scale;

    return-object v0
.end method

.method public getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetf(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/oj;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 5
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/internal/oj;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    return-object v0
.end method

.method public getNativeAnnotationManager()Lcom/pspdfkit/internal/jni/NativeAnnotationManager;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/r1;->c()Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    move-result-object v0

    return-object v0

    .line 6
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access NativeAnnotationManager when annotation is not attached!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getNativeResourceManager()Lcom/pspdfkit/internal/jni/NativeResourceManager;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/r1;->d()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v0

    return-object v0

    .line 5
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access NativeResourceManager when annotation is not attached!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getPageRotation()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->getPageRotation(I)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getProperties()Lcom/pspdfkit/internal/p1;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    return-object v0
.end method

.method public getQuadrilaterals()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ho;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    .line 2
    const-class v1, Ljava/util/List;

    const/16 v2, 0x1389

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    return-object v0
.end method

.method public getRotation()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x12

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    rsub-int v0, v0, 0x168

    return v0
.end method

.method public getSoundAnnotationState()Lcom/pspdfkit/internal/ls;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->c:Lcom/pspdfkit/internal/ls;

    return-object v0
.end method

.method public getTextShouldFit()Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const-string v2, "web/freetext/isFitting"

    .line 6
    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAdditionalDataBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 7
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public getUuid()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v1, v1, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->w()Lcom/pspdfkit/internal/gv;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/jo;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/jo;->a()Ljava/lang/String;

    move-result-object v1

    .line 4
    invoke-virtual {p0, v1}, Lcom/pspdfkit/annotations/Annotation$2;->setUuid(Ljava/lang/String;)V

    .line 6
    :cond_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 7
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x1a

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromName(Ljava/lang/String;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    goto :goto_0

    .line 4
    :cond_0
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public hasInstantComments()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetd(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    return v0
.end method

.method public markAsInstantCommentRoot()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fputd(Lcom/pspdfkit/annotations/Annotation;Z)V

    return-void
.end method

.method public needsFlippedContentSize()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getPageRotation()I

    move-result v0

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_1

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public needsSyncingWithCore()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetl(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x1;

    move-result-object v1

    .line 2
    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/p1;->c()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/pspdfkit/internal/x1;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public notifyAnnotationCreated()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetb(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-interface {v1, v2}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public notifyAnnotationRemoved()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetb(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-interface {v1, v2}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public notifyAnnotationUpdated()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetb(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-interface {v1, v2}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final onAttachToDocument(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/oj;Z)V
    .locals 5

    .line 1
    iget-object p3, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    monitor-enter p3

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iput-object p1, v0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    .line 3
    invoke-static {v0, p2}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fputf(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/oj;)V

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/annotations/Annotation$2;->a()V

    .line 11
    iget-object p2, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {p2}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetm(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->needsFlippedContentSize()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 12
    iget-object p2, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object p2, p2, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const-class v0, Landroid/graphics/RectF;

    const/16 v1, 0x16

    invoke-virtual {p2, v1, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/graphics/RectF;

    if-eqz p2, :cond_0

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    new-instance v2, Landroid/graphics/RectF;

    .line 16
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result p2

    const/4 v4, 0x0

    invoke-direct {v2, v4, v3, p2, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 17
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/p1;->a(ILandroid/graphics/RectF;)V

    .line 22
    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fputm(Lcom/pspdfkit/annotations/Annotation;Z)V

    .line 26
    invoke-static {p2}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgeth(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 28
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p1

    .line 29
    check-cast p1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/r1;->b()Lcom/pspdfkit/internal/o2;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    .line 30
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/o2;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 34
    :cond_1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation$2;->synchronizeToNativeObjectIfAttached(Z)Z

    .line 35
    monitor-exit p3

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public onDetachedFromDocument()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    .line 2
    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/high16 v2, -0x80000000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/p1;->d(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetf(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/oj;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/oj;->release()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fputf(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/oj;)V

    .line 6
    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetl(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x1;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/internal/x1;->d()V

    :cond_0
    return-void
.end method

.method public prepareForCopy()V
    .locals 2

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation$2;->setPageIndex(I)V

    const/high16 v0, -0x80000000

    .line 2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation$2;->setObjectNumber(I)V

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->w()Lcom/pspdfkit/internal/gv;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/jo;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jo;->a()Ljava/lang/String;

    move-result-object v0

    .line 4
    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation$2;->setUuid(Ljava/lang/String;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->setName(Ljava/lang/String;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->d(I)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->d(I)V

    return-void
.end method

.method public removeOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgeta(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetb(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public requireNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetf(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/oj;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6
    invoke-interface {v0}, Lcom/pspdfkit/internal/oj;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    return-object v0

    .line 7
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t retrieve native annotation since nativeAnnotationHolder was null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setAction(Lcom/pspdfkit/annotations/actions/Action;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->synchronizeToNativeObjectIfAttached()Z

    return-void
.end method

.method public setAdditionalAction(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;Lcom/pspdfkit/annotations/actions/Action;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getAdditionalActions()Lcom/pspdfkit/internal/o;

    move-result-object v0

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    return-void

    :cond_0
    const/16 v1, 0xbb9

    if-nez v0, :cond_1

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/o;

    invoke-direct {v0}, Lcom/pspdfkit/internal/o;-><init>()V

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v2, v2, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v2, v1, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 8
    :cond_1
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/o;->a(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;Lcom/pspdfkit/annotations/actions/Action;)V

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/o;->c()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object p1, p1, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 p2, 0x0

    invoke-virtual {p1, v1, p2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 15
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object p1, p1, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/p1;->d()V

    .line 18
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->synchronizeToNativeObjectIfAttached()Z

    return-void
.end method

.method public setAdditionalData(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6
    invoke-virtual {v0, p1, p2, p3}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->setAdditionalDataString(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void

    .line 7
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Can\'t set additional data on an annotation that is not attached to a document."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setAnnotationResource(Lcom/pspdfkit/internal/x1;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetl(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x1;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/x1;->d()V

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0, p1}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fputl(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/x1;)V

    return-void
.end method

.method public setContentSize(Landroid/graphics/RectF;Z)V
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 2
    iget p1, v0, Landroid/graphics/RectF;->left:F

    iget v1, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v2, p1, v1

    if-lez v2, :cond_0

    .line 4
    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 5
    iput p1, v0, Landroid/graphics/RectF;->right:F

    .line 7
    :cond_0
    iget p1, v0, Landroid/graphics/RectF;->bottom:F

    iget v1, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v2, p1, v1

    if-lez v2, :cond_1

    .line 9
    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 10
    iput p1, v0, Landroid/graphics/RectF;->top:F

    .line 11
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result p1

    const/16 v1, 0x16

    if-eqz p1, :cond_3

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->needsFlippedContentSize()Z

    move-result p1

    if-eqz p1, :cond_2

    if-nez p2, :cond_2

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object p1, p1, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    new-instance p2, Landroid/graphics/RectF;

    .line 15
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    const/4 v3, 0x0

    invoke-direct {p2, v3, v2, v0, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 16
    invoke-virtual {p1, v1, p2}, Lcom/pspdfkit/internal/p1;->a(ILandroid/graphics/RectF;)V

    goto :goto_0

    .line 20
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object p1, p1, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/internal/p1;->a(ILandroid/graphics/RectF;)V

    .line 22
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fputm(Lcom/pspdfkit/annotations/Annotation;Z)V

    goto :goto_1

    .line 27
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object p1, p1, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/internal/p1;->a(ILandroid/graphics/RectF;)V

    if-nez p2, :cond_4

    .line 29
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fputm(Lcom/pspdfkit/annotations/Annotation;Z)V

    :cond_4
    :goto_1
    return-void
.end method

.method public setDetachedAnnotationLookupKey(Ljava/lang/Integer;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0, p1}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fputg(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/Integer;)V

    .line 2
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    return-void
.end method

.method public setEdgeInsets(Lcom/pspdfkit/utils/EdgeInsets;)V
    .locals 2

    const-string v0, "edgeInsets"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x3ef

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public setFontName(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method public setInReplyToUuid(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x15

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method public setIsSignature(Z)V
    .locals 2

    if-eqz p1, :cond_1

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Your current license doesn\'t allow creating signature annotations."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 5
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const/16 v1, 0x7d0

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Boolean;)V

    return-void
.end method

.method public setMeasurementPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x2af9

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {p1}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$me(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public setMeasurementScale(Lcom/pspdfkit/annotations/measurements/Scale;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x2afa

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {p1}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$me(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public setObjectNumber(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    return-void
.end method

.method public setPageIndex(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    return-void
.end method

.method public setPointsWithoutCoreSync(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    const-string v1, "type"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    sget-object v1, Lcom/pspdfkit/internal/jr;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v1, v3, :cond_0

    const/4 v4, 0x2

    if-eq v1, v4, :cond_0

    const/4 v4, 0x3

    if-eq v1, v4, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    return-void

    .line 23
    :cond_1
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->LINE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_2

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    .line 26
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/PointF;

    invoke-static {v1, p1}, Lcom/pspdfkit/annotations/LineAnnotation;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)Ljava/util/ArrayList;

    move-result-object p1

    const/16 v1, 0x64

    .line 27
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    goto :goto_1

    .line 31
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x67

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 37
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {p1}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$me(Lcom/pspdfkit/annotations/Annotation;)V

    .line 38
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    check-cast p1, Lcom/pspdfkit/annotations/BaseLineAnnotation;

    .line 39
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/BaseLineAnnotation;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, v0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    sget-object v2, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    if-ne v1, v2, :cond_5

    iget-object v0, v0, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    if-eq v0, v2, :cond_3

    goto :goto_3

    .line 52
    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/BaseLineAnnotation;->f()Ljava/util/List;

    move-result-object p1

    .line 53
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x1

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v1, 0x1

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    const v3, 0x7f7fffff    # Float.MAX_VALUE

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 54
    iget v5, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v5, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 55
    iget v5, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 56
    iget v5, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v5, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 57
    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v4, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto :goto_2

    .line 60
    :cond_4
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1, v2, v3, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 61
    invoke-virtual {p1}, Landroid/graphics/RectF;->sort()V

    .line 62
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    .line 63
    invoke-static {v0}, Lcom/pspdfkit/internal/er;->a(Lcom/pspdfkit/annotations/Annotation;)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    neg-float v0, v0

    .line 64
    invoke-virtual {p1, v0, v0}, Landroid/graphics/RectF;->inset(FF)V

    .line 66
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    goto :goto_4

    .line 67
    :cond_5
    :goto_3
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->synchronizeToNativeObjectIfAttached()Z

    :goto_4
    return-void
.end method

.method public setProperties(Lcom/pspdfkit/internal/p1;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    new-instance v1, Lcom/pspdfkit/internal/p1;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/p1;-><init>(Lcom/pspdfkit/internal/p1;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->a(Lcom/pspdfkit/internal/p1;)V

    return-void
.end method

.method public setQuadrilaterals(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ho;",
            ">;)V"
        }
    .end annotation

    const-string v0, "Annotation quadrilaterals"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "Annotation quadrilaterals may not contain null elements."

    .line 54
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Ljava/util/Collection;)V

    .line 57
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/16 p1, 0x1389

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 59
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    return-void
.end method

.method public setRotation(I)V
    .locals 2

    .line 1
    rem-int/lit16 p1, p1, 0x168

    rsub-int p1, p1, 0x168

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/16 v1, 0x12

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    return-void
.end method

.method public setSoundAnnotationState(Lcom/pspdfkit/internal/ls;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->c:Lcom/pspdfkit/internal/ls;

    return-void
.end method

.method public setTextShouldFit(Z)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v1, v2, :cond_0

    .line 9
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const/4 v1, 0x0

    const-string v2, "web/freetext/isFitting"

    invoke-virtual {v0, v2, p1, v1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->setAdditionalDataBoolean(Ljava/lang/String;Ljava/lang/Boolean;Z)V

    return-void

    .line 10
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "shouldFit flag can be set only on free-text annotations."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 11
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Can\'t set shouldFit flag on an annotation that is not attached to a document."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setUuid(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x14

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method public setVariant(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 2

    const/16 v0, 0x1a

    if-nez p1, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object p1, p1, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/p1;->d(I)V

    goto :goto_0

    .line 3
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v1, v1, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v0, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    :goto_0
    return-void
.end method

.method public synchronizeFromNativeObjectIfAttached()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getNativeAnnotationManager()Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)V

    :cond_0
    return-void
.end method

.method public final synchronizeToNativeObjectIfAttached()Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1
    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/annotations/Annotation$2;->synchronizeToNativeObjectIfAttached(ZZ)Z

    move-result v0

    return v0
.end method

.method public final synchronizeToNativeObjectIfAttached(Z)Z
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/annotations/Annotation$2;->synchronizeToNativeObjectIfAttached(ZZ)Z

    move-result p1

    return p1
.end method

.method public synchronizeToNativeObjectIfAttached(ZZ)Z
    .locals 4

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/annotations/Annotation$2;->c()Z

    move-result v0

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object v2, v1, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    iget-object v1, v1, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    .line 9
    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v1

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation$2;->requireNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v3

    .line 10
    invoke-virtual {v2, v1, v3}, Lcom/pspdfkit/internal/p1;->a(Lcom/pspdfkit/internal/qf;Lcom/pspdfkit/internal/jni/NativeAnnotation;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 14
    invoke-direct {p0}, Lcom/pspdfkit/annotations/Annotation$2;->b()Z

    move-result v1

    or-int/2addr v0, v1

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    if-eqz p2, :cond_1

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->a:Lio/reactivex/rxjava3/disposables/Disposable;

    if-nez p1, :cond_0

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->b:Lio/reactivex/rxjava3/subjects/PublishSubject;

    sget-object p2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1f4

    .line 21
    invoke-virtual {p1, v1, v2, p2}, Lio/reactivex/rxjava3/subjects/PublishSubject;->throttleLatest(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 22
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/annotations/Annotation$2$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/pspdfkit/annotations/Annotation$2$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/annotations/Annotation$2;)V

    .line 23
    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->a:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 27
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->b:Lio/reactivex/rxjava3/subjects/PublishSubject;

    iget-object p2, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 29
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->a:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 30
    invoke-static {p1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 p1, 0x0

    .line 31
    iput-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->a:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 32
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    iget-object p1, p1, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/annotations/Annotation$2;->d:Lcom/pspdfkit/annotations/Annotation;

    check-cast p1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/r1;->j(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :cond_3
    :goto_0
    return v0
.end method

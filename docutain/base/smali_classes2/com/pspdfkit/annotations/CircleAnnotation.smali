.class public Lcom/pspdfkit/annotations/CircleAnnotation;
.super Lcom/pspdfkit/annotations/ShapeAnnotation;
.source "SourceFile"


# direct methods
.method public constructor <init>(ILandroid/graphics/RectF;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/ShapeAnnotation;-><init>(I)V

    const-string p1, "rect"

    .line 2
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v0, 0x9

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/p1;->a(ILandroid/graphics/RectF;)V

    return-void
.end method

.method public constructor <init>(ILandroid/graphics/RectF;Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 0

    .line 4
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/CircleAnnotation;-><init>(ILandroid/graphics/RectF;)V

    .line 6
    invoke-virtual {p0, p3, p4}, Lcom/pspdfkit/annotations/ShapeAnnotation;->a(Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/p1;Z)V
    .locals 0

    .line 7
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/ShapeAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    return-void
.end method


# virtual methods
.method final a()Lcom/pspdfkit/annotations/Annotation;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/CircleAnnotation;

    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/annotations/CircleAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->prepareForCopy()V

    return-object v0
.end method

.method protected final a(Lcom/pspdfkit/internal/ri;)Lcom/pspdfkit/internal/mi;
    .locals 2

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-static {p1, v1, v0}, Lcom/pspdfkit/internal/li;->a(Lcom/pspdfkit/internal/ri;FF)Lcom/pspdfkit/internal/mi;

    move-result-object p1

    return-object p1
.end method

.method public getType()Lcom/pspdfkit/annotations/AnnotationType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->CIRCLE:Lcom/pspdfkit/annotations/AnnotationType;

    return-object v0
.end method

.class Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;
.super Lcom/pspdfkit/ui/drawable/PdfDrawable;
.source "SourceFile"


# static fields
.field private static final l:Landroid/graphics/RectF;

.field private static final m:Landroid/graphics/Paint;

.field private static final n:Landroid/graphics/Paint;


# instance fields
.field private final b:Lcom/pspdfkit/annotations/LinkAnnotation;

.field private final c:Landroid/graphics/RectF;

.field private d:I

.field private e:I

.field private f:F

.field private g:I

.field private h:I

.field private i:F

.field private j:Z

.field private k:F


# direct methods
.method public static synthetic $r8$lambda$dj2sMUcGPNLdG-9RmGPV555B0ts(Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->a(Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DARKEN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 13
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    sput-object v1, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->l:Landroid/graphics/RectF;

    .line 23
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->m:Landroid/graphics/Paint;

    .line 24
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 25
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    const/16 v2, 0x78

    .line 26
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 28
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->n:Landroid/graphics/Paint;

    .line 29
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 30
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 31
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/annotations/LinkAnnotation;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/drawable/PdfDrawable;-><init>()V

    const-string v0, "linkAnnotation"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->b:Lcom/pspdfkit/annotations/LinkAnnotation;

    .line 4
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->c:Landroid/graphics/RectF;

    const/4 p1, 0x1

    .line 5
    iput-boolean p1, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->j:Z

    return-void
.end method

.method private synthetic a(Landroid/animation/ValueAnimator;)V
    .locals 1

    .line 15
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 16
    sget-object v0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->n:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 17
    sget-object v0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->m:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 18
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    return-void
.end method

.method private startPopOutAnimation()V
    .locals 7

    const/4 v0, 0x2

    new-array v1, v0, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v2, v1, v3

    .line 1
    iget v2, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->e:I

    int-to-float v2, v2

    const/4 v4, 0x1

    aput v2, v1, v4

    invoke-static {v1}, Landroid/animation/ObjectAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 2
    new-instance v2, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const-wide/16 v5, 0x96

    .line 7
    invoke-virtual {v1, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 8
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 9
    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 10
    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    new-array v2, v0, [I

    .line 12
    sget-object v5, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->m:Landroid/graphics/Paint;

    invoke-virtual {v5}, Landroid/graphics/Paint;->getAlpha()I

    move-result v5

    aput v5, v2, v3

    aput v3, v2, v4

    invoke-static {v2}, Landroid/animation/ObjectAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 13
    new-instance v5, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable$$ExternalSyntheticLambda1;

    invoke-direct {v5, p0}, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;)V

    invoke-virtual {v2, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const-wide/16 v5, 0x5dc

    .line 19
    invoke-virtual {v2, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 20
    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 22
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    new-array v0, v0, [Landroid/animation/Animator;

    aput-object v1, v0, v3

    aput-object v2, v0, v4

    .line 23
    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 24
    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    .line 26
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    return-void
.end method


# virtual methods
.method final a()Lcom/pspdfkit/annotations/LinkAnnotation;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->b:Lcom/pspdfkit/annotations/LinkAnnotation;

    return-object v0
.end method

.method final a(Lcom/pspdfkit/internal/lh;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->m:Landroid/graphics/Paint;

    iget v1, p1, Lcom/pspdfkit/internal/lh;->a:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2
    sget-object v1, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->n:Landroid/graphics/Paint;

    iget v2, p1, Lcom/pspdfkit/internal/lh;->b:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 3
    iget v2, p1, Lcom/pspdfkit/internal/lh;->c:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4
    iget v2, p1, Lcom/pspdfkit/internal/lh;->d:I

    iput v2, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->d:I

    .line 5
    iget v2, p1, Lcom/pspdfkit/internal/lh;->e:I

    iput v2, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->e:I

    .line 6
    iget v2, p1, Lcom/pspdfkit/internal/lh;->f:F

    iput v2, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->f:F

    .line 8
    iget v2, p1, Lcom/pspdfkit/internal/lh;->g:I

    iput v2, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->g:I

    .line 9
    iget p1, p1, Lcom/pspdfkit/internal/lh;->h:I

    iput p1, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->h:I

    const/16 p1, 0x78

    .line 10
    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 11
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 14
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;)V

    check-cast p1, Lcom/pspdfkit/internal/u;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->j:Z

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->startPopOutAnimation()V

    .line 6
    :cond_0
    sget-object v0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->l:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->c:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 7
    iget v1, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->k:F

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_1

    neg-float v1, v1

    invoke-virtual {v0, v1, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 8
    :cond_1
    iget v1, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->i:F

    sget-object v2, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 9
    iget v1, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->i:F

    sget-object v2, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method synthetic lambda$startPopOutAnimation$0$com-pspdfkit-annotations-links-HighlightedLinkAnnotationDrawable(Landroid/animation/ValueAnimator;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    iput p1, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->k:F

    .line 2
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    return-void
.end method

.method public setAlpha(I)V
    .locals 0

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method

.method public updatePdfToViewTransformation(Landroid/graphics/Matrix;)V
    .locals 5

    const-string v0, "matrix"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-super {p0, p1}, Lcom/pspdfkit/ui/drawable/PdfDrawable;->updatePdfToViewTransformation(Landroid/graphics/Matrix;)V

    .line 56
    iget v0, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->g:I

    int-to-float v0, v0

    iput v0, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->i:F

    .line 57
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 58
    iget-object v1, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->c:Landroid/graphics/RectF;

    .line 59
    iget-object v2, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->b:Lcom/pspdfkit/annotations/LinkAnnotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 61
    iget v2, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->d:I

    neg-int v3, v2

    int-to-float v3, v3

    int-to-float v2, v2

    invoke-virtual {v0, v3, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 62
    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 63
    invoke-virtual {p1, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 64
    iget p1, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->i:F

    .line 67
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget v2, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->f:F

    mul-float v0, v0, v2

    iget v2, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->g:I

    int-to-float v2, v2

    iget v3, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->h:I

    int-to-float v3, v3

    .line 68
    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 69
    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iput p1, p0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->i:F

    .line 77
    iget p1, v1, Landroid/graphics/RectF;->left:F

    float-to-int p1, p1

    .line 78
    iget v0, v1, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    .line 79
    iget v2, v1, Landroid/graphics/RectF;->right:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 80
    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-double v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v1, v3

    .line 81
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3, p1, v0, v2, v1}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

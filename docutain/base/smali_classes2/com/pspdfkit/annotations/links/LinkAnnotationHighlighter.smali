.class public Lcom/pspdfkit/annotations/links/LinkAnnotationHighlighter;
.super Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;
.source "SourceFile"


# instance fields
.field private final b:Lcom/pspdfkit/internal/lh;

.field private c:Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;-><init>()V

    const-string v0, "context"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/lh;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/lh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/annotations/links/LinkAnnotationHighlighter;->b:Lcom/pspdfkit/internal/lh;

    return-void
.end method


# virtual methods
.method public getDrawablesForPage(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/document/PdfDocument;",
            "I)",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/ui/drawable/PdfDrawable;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/annotations/links/LinkAnnotationHighlighter;->c:Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->a()Lcom/pspdfkit/annotations/LinkAnnotation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result p1

    if-ne p1, p3, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/annotations/links/LinkAnnotationHighlighter;->c:Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 4
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public setLinkAnnotation(Lcom/pspdfkit/annotations/LinkAnnotation;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/annotations/links/LinkAnnotationHighlighter;->c:Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;

    goto :goto_0

    .line 3
    :cond_0
    new-instance v0, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;

    invoke-direct {v0, p1}, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;-><init>(Lcom/pspdfkit/annotations/LinkAnnotation;)V

    iput-object v0, p0, Lcom/pspdfkit/annotations/links/LinkAnnotationHighlighter;->c:Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/annotations/links/LinkAnnotationHighlighter;->b:Lcom/pspdfkit/internal/lh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/links/HighlightedLinkAnnotationDrawable;->a(Lcom/pspdfkit/internal/lh;)V

    .line 6
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->notifyDrawablesChanged()V

    return-void
.end method

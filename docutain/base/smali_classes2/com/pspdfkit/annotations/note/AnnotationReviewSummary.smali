.class public final Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/pspdfkit/annotations/note/AuthorState;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/pspdfkit/annotations/note/AuthorState;


# direct methods
.method public constructor <init>(Ljava/util/Map;Lcom/pspdfkit/annotations/note/AuthorState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/pspdfkit/annotations/note/AuthorState;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/pspdfkit/annotations/note/AuthorState;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;->a:Ljava/util/Map;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;->b:Lcom/pspdfkit/annotations/note/AuthorState;

    return-void
.end method


# virtual methods
.method public getCurrentUserState()Lcom/pspdfkit/annotations/note/AuthorState;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;->b:Lcom/pspdfkit/annotations/note/AuthorState;

    return-object v0
.end method

.method public getReviewNames()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/pspdfkit/annotations/note/AuthorState;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;->a:Ljava/util/Map;

    return-object v0
.end method

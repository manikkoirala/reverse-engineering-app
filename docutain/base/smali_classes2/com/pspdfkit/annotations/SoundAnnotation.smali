.class public Lcom/pspdfkit/annotations/SoundAnnotation;
.super Lcom/pspdfkit/annotations/Annotation;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/SoundAnnotation$IconName;
    }
.end annotation


# static fields
.field public static final ICON_NAME_MIC:Ljava/lang/String; = "Mic"

.field public static final ICON_NAME_SPEAKER:Ljava/lang/String; = "Speaker"


# instance fields
.field private q:Lcom/pspdfkit/internal/z;


# direct methods
.method public static synthetic $r8$lambda$5GtHMY2qtjFNZvQbmn1v7aGwMmE(Lcom/pspdfkit/annotations/SoundAnnotation;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/annotations/SoundAnnotation;->f()Lio/reactivex/rxjava3/core/MaybeSource;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(ILandroid/graphics/RectF;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/Annotation;-><init>(I)V

    const-string p1, "boundingBox"

    .line 9
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-virtual {p0, p2}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    const-string p1, "Speaker"

    .line 12
    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->setIconName(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(ILandroid/graphics/RectF;Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/SoundAnnotation;-><init>(ILandroid/graphics/RectF;)V

    const-string p1, "audioSource"

    .line 3
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-virtual {p3}, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->getDescription()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/Annotation;->setContents(Ljava/lang/String;)V

    .line 6
    new-instance p1, Lcom/pspdfkit/internal/z;

    invoke-direct {p1, p0, p3}, Lcom/pspdfkit/internal/z;-><init>(Lcom/pspdfkit/annotations/SoundAnnotation;Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;)V

    iput-object p1, p0, Lcom/pspdfkit/annotations/SoundAnnotation;->q:Lcom/pspdfkit/internal/z;

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/annotations/SoundAnnotation;->q:Lcom/pspdfkit/internal/z;

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/p1;ZLcom/pspdfkit/annotations/sound/EmbeddedAudioSource;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/Annotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    if-eqz p3, :cond_0

    .line 19
    new-instance p1, Lcom/pspdfkit/internal/z;

    invoke-direct {p1, p0, p3}, Lcom/pspdfkit/internal/z;-><init>(Lcom/pspdfkit/annotations/SoundAnnotation;Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;)V

    iput-object p1, p0, Lcom/pspdfkit/annotations/SoundAnnotation;->q:Lcom/pspdfkit/internal/z;

    .line 20
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/annotations/SoundAnnotation;->q:Lcom/pspdfkit/internal/z;

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/p1;ZLjava/lang/String;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/Annotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    if-eqz p3, :cond_0

    .line 15
    new-instance p1, Lcom/pspdfkit/internal/z;

    invoke-direct {p1, p0, p3}, Lcom/pspdfkit/internal/z;-><init>(Lcom/pspdfkit/annotations/SoundAnnotation;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/annotations/SoundAnnotation;->q:Lcom/pspdfkit/internal/z;

    .line 16
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/annotations/SoundAnnotation;->q:Lcom/pspdfkit/internal/z;

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    :cond_0
    return-void
.end method

.method private synthetic f()Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/SoundAnnotation;->q:Lcom/pspdfkit/internal/z;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z;->i()[B

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 2
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    :goto_1
    return-object v0
.end method


# virtual methods
.method public getAudioData()[B
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/SoundAnnotation;->hasAudioData()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 4
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/SoundAnnotation;->q:Lcom/pspdfkit/internal/z;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z;->i()[B

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.Annotations"

    const-string v4, "Can\'t retrieve audio data."

    .line 6
    invoke-static {v3, v0, v4, v2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v1
.end method

.method public getAudioDataAsync()Lio/reactivex/rxjava3/core/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "[B>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/SoundAnnotation$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/annotations/SoundAnnotation$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/annotations/SoundAnnotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    return-object v0
.end method

.method public getAudioEncoding()Lcom/pspdfkit/annotations/sound/AudioEncoding;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    sget-object v1, Lcom/pspdfkit/annotations/sound/AudioEncoding;->SIGNED:Lcom/pspdfkit/annotations/sound/AudioEncoding;

    const-class v2, Lcom/pspdfkit/annotations/sound/AudioEncoding;

    const/16 v3, 0x2714

    invoke-virtual {v0, v3, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/sound/AudioEncoding;

    return-object v0
.end method

.method public getChannels()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x2713

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getIconName()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0xfa0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "Speaker"

    :cond_0
    return-object v0
.end method

.method public getSampleRate()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x2712

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getSampleSize()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x2711

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getType()Lcom/pspdfkit/annotations/AnnotationType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->SOUND:Lcom/pspdfkit/annotations/AnnotationType;

    return-object v0
.end method

.method public hasAudioData()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/SoundAnnotation;->q:Lcom/pspdfkit/internal/z;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isLocked()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isResizable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setAudioSource(Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;)V
    .locals 2

    .line 1
    monitor-enter p0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 3
    :try_start_0
    iput-object p1, p0, Lcom/pspdfkit/annotations/SoundAnnotation;->q:Lcom/pspdfkit/internal/z;

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    goto :goto_0

    .line 6
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/z;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/z;-><init>(Lcom/pspdfkit/annotations/SoundAnnotation;Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;)V

    iput-object v0, p0, Lcom/pspdfkit/annotations/SoundAnnotation;->q:Lcom/pspdfkit/internal/z;

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/annotations/SoundAnnotation;->q:Lcom/pspdfkit/internal/z;

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->getDescription()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->getDescription()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/Annotation;->setContents(Ljava/lang/String;)V

    .line 12
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public setIconName(Ljava/lang/String;)V
    .locals 2

    const-string v0, "Annotation icon name must not be null."

    .line 1
    invoke-static {p1, p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0xfa0

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method public updateTransformationProperties(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 0

    return-void
.end method

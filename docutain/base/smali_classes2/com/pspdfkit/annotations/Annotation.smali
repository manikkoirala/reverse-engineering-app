.class public abstract Lcom/pspdfkit/annotations/Annotation;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final DEFAULT_BORDER_WIDTH:F = 1.0f

.field public static final DEFAULT_CLOUDY_BORDER_EFFECT_INTENSITY:F = 2.0f

.field public static final OBJECT_NUMBER_NOT_SET:I = -0x80000000

.field public static final PAGE_NUMBER_NOT_SET:I = -0x80000000

.field protected static final o:Lcom/pspdfkit/utils/Size;

.field static final synthetic p:Z = true


# instance fields
.field private final a:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/internal/el;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/pspdfkit/internal/p1;

.field private d:Z

.field e:Lcom/pspdfkit/internal/zf;

.field private f:Lcom/pspdfkit/internal/oj;

.field private g:Ljava/lang/Integer;

.field private h:Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

.field private i:Z

.field private j:Lcom/pspdfkit/annotations/Annotation;

.field private k:Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

.field private l:Lcom/pspdfkit/internal/x1;

.field private m:Z

.field private final n:Lcom/pspdfkit/internal/pf;


# direct methods
.method public static synthetic $r8$lambda$xVZn7r2-fl4vD0R8xsv_rRGdxIg(Lcom/pspdfkit/annotations/Annotation;ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/annotations/Annotation;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/nh;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/annotations/Annotation;->a:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/nh;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/annotations/Annotation;->b:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/annotations/Annotation;->d:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/oj;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/annotations/Annotation;->f:Lcom/pspdfkit/internal/oj;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetg(Lcom/pspdfkit/annotations/Annotation;)Ljava/lang/Integer;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/annotations/Annotation;->g:Ljava/lang/Integer;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/annotations/Annotation;->h:Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetj(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/annotations/Annotation;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/annotations/Annotation;->j:Lcom/pspdfkit/annotations/Annotation;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetl(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x1;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/annotations/Annotation;->l:Lcom/pspdfkit/internal/x1;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetm(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/annotations/Annotation;->m:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetn(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/pf;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/annotations/Annotation;->n:Lcom/pspdfkit/internal/pf;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputd(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/annotations/Annotation;->d:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputf(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/oj;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->f:Lcom/pspdfkit/internal/oj;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputg(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->g:Ljava/lang/Integer;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputl(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/x1;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->l:Lcom/pspdfkit/internal/x1;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputm(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/annotations/Annotation;->m:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$ma(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/zf;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/Annotation;->a(Lcom/pspdfkit/internal/zf;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$md(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/annotations/Annotation;->d()V

    return-void
.end method

.method static bridge synthetic -$$Nest$me(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/annotations/Annotation;->e()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 10
    new-instance v0, Lcom/pspdfkit/utils/Size;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-direct {v0, v1, v1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    sput-object v0, Lcom/pspdfkit/annotations/Annotation;->o:Lcom/pspdfkit/utils/Size;

    return-void
.end method

.method constructor <init>(I)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->a:Lcom/pspdfkit/internal/nh;

    .line 11
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->b:Lcom/pspdfkit/internal/nh;

    .line 19
    new-instance v0, Lcom/pspdfkit/annotations/Annotation$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/annotations/Annotation$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    .line 31
    new-instance v1, Lcom/pspdfkit/internal/p1;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/p1;-><init>(Lcom/pspdfkit/internal/p1$a;)V

    iput-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v0, 0x0

    .line 34
    iput-boolean v0, p0, Lcom/pspdfkit/annotations/Annotation;->d:Z

    const/4 v2, 0x0

    .line 41
    iput-object v2, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    .line 48
    iput-object v2, p0, Lcom/pspdfkit/annotations/Annotation;->f:Lcom/pspdfkit/internal/oj;

    .line 60
    iput-object v2, p0, Lcom/pspdfkit/annotations/Annotation;->g:Ljava/lang/Integer;

    .line 82
    iput-boolean v0, p0, Lcom/pspdfkit/annotations/Annotation;->i:Z

    .line 1346
    new-instance v0, Lcom/pspdfkit/annotations/Annotation$2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/annotations/Annotation$2;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    iput-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->n:Lcom/pspdfkit/internal/pf;

    .line 1347
    invoke-direct {p0}, Lcom/pspdfkit/annotations/Annotation;->c()V

    .line 1348
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {v1, v0, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/p1;Z)V
    .locals 3

    .line 1349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1350
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->a:Lcom/pspdfkit/internal/nh;

    .line 1359
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->b:Lcom/pspdfkit/internal/nh;

    .line 1367
    new-instance v0, Lcom/pspdfkit/annotations/Annotation$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/annotations/Annotation$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    .line 1379
    new-instance v1, Lcom/pspdfkit/internal/p1;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/p1;-><init>(Lcom/pspdfkit/internal/p1$a;)V

    iput-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v0, 0x0

    .line 1382
    iput-boolean v0, p0, Lcom/pspdfkit/annotations/Annotation;->d:Z

    const/4 v2, 0x0

    .line 1389
    iput-object v2, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    .line 1396
    iput-object v2, p0, Lcom/pspdfkit/annotations/Annotation;->f:Lcom/pspdfkit/internal/oj;

    .line 1408
    iput-object v2, p0, Lcom/pspdfkit/annotations/Annotation;->g:Ljava/lang/Integer;

    .line 1430
    iput-boolean v0, p0, Lcom/pspdfkit/annotations/Annotation;->i:Z

    .line 2694
    new-instance v0, Lcom/pspdfkit/annotations/Annotation$2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/annotations/Annotation$2;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    iput-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->n:Lcom/pspdfkit/internal/pf;

    .line 2695
    invoke-direct {p0}, Lcom/pspdfkit/annotations/Annotation;->c()V

    if-eqz p2, :cond_0

    .line 2701
    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/p1;->b(Lcom/pspdfkit/internal/p1;)V

    goto :goto_0

    .line 2703
    :cond_0
    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/p1;->a(Lcom/pspdfkit/internal/p1;)V

    .line 2704
    invoke-virtual {v1}, Lcom/pspdfkit/internal/p1;->a()V

    :goto_0
    return-void
.end method

.method private synthetic a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/el;

    .line 2
    invoke-interface {v1, p0, p1, p2, p3}, Lcom/pspdfkit/internal/el;->onAnnotationPropertyChange(Lcom/pspdfkit/annotations/Annotation;ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Lcom/pspdfkit/internal/zf;)V
    .locals 2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->j:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->j:Lcom/pspdfkit/annotations/Annotation;

    iget-object v1, v0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    if-eq v1, p1, :cond_0

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v0, p1, v1

    const/4 v0, 0x1

    aput-object p0, p1, v0

    const-string v0, "PSPDFKit.Annotations"

    const-string v1, "Annotation and its reply are attached to different documents. This can produce unexpected results. Annotation: %s Reply: %s"

    .line 4
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private b()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffectIntensity()F

    move-result v0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/BorderEffect;->CLOUDY:Lcom/pspdfkit/annotations/BorderEffect;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getEdgeInsets()Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object v2

    .line 6
    iget v3, v1, Landroid/graphics/RectF;->left:F

    iget v4, v2, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    add-float/2addr v3, v4

    iput v3, v1, Landroid/graphics/RectF;->left:F

    .line 7
    iget v3, v1, Landroid/graphics/RectF;->top:F

    iget v4, v2, Lcom/pspdfkit/utils/EdgeInsets;->top:F

    sub-float/2addr v3, v4

    iput v3, v1, Landroid/graphics/RectF;->top:F

    .line 8
    iget v3, v1, Landroid/graphics/RectF;->right:F

    iget v4, v2, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    sub-float/2addr v3, v4

    iput v3, v1, Landroid/graphics/RectF;->right:F

    .line 9
    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, v2, Lcom/pspdfkit/utils/EdgeInsets;->bottom:F

    add-float/2addr v3, v2

    iput v3, v1, Landroid/graphics/RectF;->bottom:F

    const/high16 v2, 0x40880000    # 4.25f

    mul-float v0, v0, v2

    neg-float v2, v0

    .line 10
    invoke-virtual {v1, v2, v0}, Landroid/graphics/RectF;->inset(FF)V

    .line 11
    invoke-virtual {p0, v1}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    .line 14
    new-instance v1, Lcom/pspdfkit/utils/EdgeInsets;

    invoke-direct {v1, v0, v0, v0, v0}, Lcom/pspdfkit/utils/EdgeInsets;-><init>(FFFF)V

    .line 16
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->setEdgeInsets(Lcom/pspdfkit/utils/EdgeInsets;)V

    .line 19
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    :cond_0
    return-void
.end method

.method private c()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getUuid()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method private d()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached(Z)Z

    move-result v0

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 11
    iget-boolean v4, p0, Lcom/pspdfkit/annotations/Annotation;->i:Z

    if-eqz v4, :cond_0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 12
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    .line 13
    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getNativeAnnotationManager()Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    move-result-object v0

    .line 14
    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->synchronizeAnnotationToBackend(Lcom/pspdfkit/internal/jni/NativeAnnotation;Z)V

    .line 16
    :cond_1
    iput-boolean v3, p0, Lcom/pspdfkit/annotations/Annotation;->i:Z

    :cond_2
    return-void
.end method

.method private e()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getMeasurementProperties()Lcom/pspdfkit/internal/ri;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 7
    :cond_1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation;->a(Lcom/pspdfkit/internal/ri;)Lcom/pspdfkit/internal/mi;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 15
    invoke-virtual {v0}, Lcom/pspdfkit/internal/mi;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 16
    invoke-virtual {v0}, Lcom/pspdfkit/internal/mi;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation;->setContents(Ljava/lang/String;)V

    :cond_2
    return-void

    .line 17
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t update measurement text for annotation type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method a()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Lcom/pspdfkit/internal/ri;)Lcom/pspdfkit/internal/mi;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public attachBinaryInstantJsonAttachment(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "dataProvider"

    const-string v2, "argumentName"

    .line 8
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 59
    invoke-static {p1, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 60
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0, p0, p1, p2}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)V

    return-void

    .line 61
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "The annotation needs to be attached to a document in order to attach a new binary instant JSON attachment."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 1
    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/annotations/Annotation;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 7
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const/16 v1, 0x14

    .line 8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x15

    .line 9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x8

    .line 10
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 12
    check-cast p1, Lcom/pspdfkit/annotations/Annotation;

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    iget-object p1, p1, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v1, p1, v0}, Lcom/pspdfkit/internal/p1;->a(Ljava/lang/Object;Ljava/util/HashSet;)Z

    move-result p1

    return p1
.end method

.method public fetchBinaryInstantJsonAttachment(Ljava/io/OutputStream;)Ljava/lang/String;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "outputStream"

    const-string v2, "argumentName"

    .line 8
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 59
    invoke-static {p1, v1, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 60
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "annotation"

    .line 61
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    invoke-static {p0, v0, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 113
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    invoke-static {p1, v1, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 165
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 171
    new-instance v1, Lcom/pspdfkit/internal/sl;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/sl;-><init>(Ljava/io/OutputStream;)V

    .line 173
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->writeBinaryInstantJsonAttachment(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeDataSink;)Lcom/pspdfkit/internal/jni/NativeAttachmentResult;

    move-result-object p1

    .line 174
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAttachmentResult;->getHasError()Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAttachmentResult;->getMimeType()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 177
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAttachmentResult;->getErrorString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The annotation needs to be attached to the document to fetch the attached binary instant JSON."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 179
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The annotation needs to be attached to a document to fetch its binary instant JSON attachment."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public generateAppearanceStream()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/annotations/Annotation;->i:Z

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/annotations/Annotation;->d()V

    return-void
.end method

.method public generateAppearanceStreamAsync()Lio/reactivex/rxjava3/core/Completable;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    invoke-static {}, Lio/reactivex/rxjava3/core/Completable;->complete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    return-object v0

    .line 2
    :cond_0
    new-instance v0, Lcom/pspdfkit/annotations/Annotation$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/annotations/Annotation$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    const/4 v2, 0x5

    .line 3
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    return-object v0
.end method

.method public getAlpha()F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0xc

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/p1;->a(IF)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public getAppearanceStreamGenerator()Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->h:Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    return-object v0
.end method

.method public getBlendMode()Lcom/pspdfkit/annotations/BlendMode;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    sget-object v1, Lcom/pspdfkit/annotations/BlendMode;->NORMAL:Lcom/pspdfkit/annotations/BlendMode;

    const-class v2, Lcom/pspdfkit/annotations/BlendMode;

    const/16 v3, 0x17

    invoke-virtual {v0, v3, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/BlendMode;

    return-object v0
.end method

.method public getBorderColor()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v0

    return v0
.end method

.method public getBorderDashArray()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const-class v1, Ljava/util/List;

    const/16 v2, 0xf

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    sget-object v1, Lcom/pspdfkit/annotations/BorderEffect;->NO_EFFECT:Lcom/pspdfkit/annotations/BorderEffect;

    const-class v2, Lcom/pspdfkit/annotations/BorderEffect;

    const/16 v3, 0x18

    invoke-virtual {v0, v3, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/BorderEffect;

    return-object v0
.end method

.method public getBorderEffectIntensity()F
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const-class v2, Ljava/lang/Float;

    const/16 v3, 0x19

    invoke-virtual {v0, v3, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    sget-object v1, Lcom/pspdfkit/annotations/BorderStyle;->NONE:Lcom/pspdfkit/annotations/BorderStyle;

    const-class v2, Lcom/pspdfkit/annotations/BorderStyle;

    const/16 v3, 0xe

    invoke-virtual {v0, v3, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/BorderStyle;

    return-object v0
.end method

.method public getBorderWidth()F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x65

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/p1;->a(IF)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public getBoundingBox()Landroid/graphics/RectF;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getBoundingBox(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const-class v1, Landroid/graphics/RectF;

    const/16 v2, 0x9

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    if-nez p1, :cond_0

    .line 4
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    :cond_0
    if-eqz v0, :cond_1

    .line 7
    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    :cond_1
    return-object p1
.end method

.method public getColor()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0xa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getContents()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCopy(I)Lcom/pspdfkit/annotations/Annotation;
    .locals 2

    if-ltz p1, :cond_1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getCopy()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/pf;->setPageIndex(I)V

    :cond_0
    return-object v0

    .line 4
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "pageIndex can\'t be smaller than 0."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getCreatedDate()Ljava/util/Date;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    .line 2
    const-class v1, Ljava/util/Date;

    const/4 v2, 0x7

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 3
    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public getCreator()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCustomData()Lorg/json/JSONObject;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const-class v1, Lorg/json/JSONObject;

    const/16 v2, 0x2329

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    return-object v0
.end method

.method public getFillColor()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getFlags()Ljava/util/EnumSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationFlags;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const-class v1, Ljava/util/EnumSet;

    const/16 v2, 0x10

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/EnumSet;

    if-nez v0, :cond_0

    .line 2
    const-class v0, Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getInReplyTo()Lcom/pspdfkit/annotations/Annotation;
    .locals 6

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_REPLIES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->j:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_1

    return-object v0

    .line 3
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInReplyToUuid()Ljava/lang/String;

    move-result-object v0

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    if-eqz v2, :cond_5

    if-nez v0, :cond_2

    goto :goto_1

    .line 5
    :cond_2
    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v2

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v3

    check-cast v2, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "uuid"

    const-string v5, "argumentName"

    .line 6
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "uuid"

    .line 57
    invoke-static {v0, v4, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 58
    monitor-enter v2

    .line 59
    :try_start_0
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/Annotation;

    .line 60
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v5

    invoke-interface {v5}, Lcom/pspdfkit/internal/pf;->getUuid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 61
    monitor-exit v2

    move-object v1, v4

    goto :goto_0

    .line 64
    :cond_4
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    :goto_0
    iput-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->j:Lcom/pspdfkit/annotations/Annotation;

    return-object v1

    :catchall_0
    move-exception v0

    .line 66
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    :goto_1
    return-object v1
.end method

.method public getInReplyToAsync()Lio/reactivex/rxjava3/core/Maybe;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isReply()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance v0, Lcom/pspdfkit/annotations/Annotation$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/annotations/Annotation$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    const/4 v2, 0x5

    .line 3
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    return-object v0

    .line 4
    :cond_1
    :goto_0
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    return-object v0
.end method

.method public getInstantRecordGroup()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getInstantRecordGroup()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInternal()Lcom/pspdfkit/internal/pf;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->n:Lcom/pspdfkit/internal/pf;

    return-object v0
.end method

.method public getMeasurementInfo()Lcom/pspdfkit/annotations/measurements/MeasurementInfo;
    .locals 9

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->MEASUREMENT_TOOLS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->n:Lcom/pspdfkit/internal/pf;

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getMeasurementProperties()Lcom/pspdfkit/internal/ri;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 12
    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation;->a(Lcom/pspdfkit/internal/ri;)Lcom/pspdfkit/internal/mi;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 18
    new-instance v8, Lcom/pspdfkit/annotations/measurements/MeasurementInfo;

    .line 19
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ri;->c()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v3

    .line 20
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ri;->b()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v4

    .line 21
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ri;->a()Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    move-result-object v5

    .line 22
    invoke-virtual {v1}, Lcom/pspdfkit/internal/mi;->b()F

    move-result v6

    .line 23
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v7

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/pspdfkit/annotations/measurements/MeasurementInfo;-><init>(Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;Lcom/pspdfkit/annotations/measurements/MeasurementMode;FLjava/lang/String;)V

    return-object v8

    .line 24
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot calculate measurement value for measurement annotation."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 25
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot get measurement info for measurement annotation as the measurement properties are null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_3
    new-instance v0, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v1, "Your current license doesn\'t allow for measurement tools."

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getMinimumSize()Lcom/pspdfkit/utils/Size;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/Annotation;->o:Lcom/pspdfkit/utils/Size;

    return-object v0
.end method

.method public getModifiedDate()Ljava/util/Date;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    .line 2
    const-class v1, Ljava/util/Date;

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 3
    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObjectNumber()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x0

    const/high16 v2, -0x80000000

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getPageIndex()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getRichText()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getType()Lcom/pspdfkit/annotations/AnnotationType;
.end method

.method public getUuid()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getUuid()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasBinaryInstantJsonAttachment()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 8
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 59
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 60
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 65
    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->hasBinaryInstantJsonAttachment(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Z

    move-result v0

    :goto_0
    return v0

    .line 66
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The annotation needs to be attached to a document to check if it has a binary instant JSON attachment."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z
    .locals 3

    const-string v0, "flag"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const-class v1, Ljava/util/EnumSet;

    const/16 v2, 0x10

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/EnumSet;

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public hasLockedContents()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationFlags;->LOCKEDCONTENTS:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/p1;->hashCode()I

    move-result v0

    return v0
.end method

.method public isAttached()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isLocked()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationFlags;->LOCKED:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v0

    return v0
.end method

.method public isMeasurement()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isModified()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->l:Lcom/pspdfkit/internal/x1;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/p1;->c()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/internal/p1;->e()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/x1;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isReply()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->j:Lcom/pspdfkit/annotations/Annotation;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->n:Lcom/pspdfkit/internal/pf;

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInReplyToUuid()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isResizable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isSignature()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->b(I)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public renderToBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;

    invoke-direct {v0}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;->build()Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/annotations/Annotation;->renderToBitmap(Landroid/graphics/Bitmap;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)V

    return-void
.end method

.method public renderToBitmap(Landroid/graphics/Bitmap;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)V
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/annotations/Annotation;->renderToBitmapAsync(Landroid/graphics/Bitmap;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->ignoreElement()Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V

    return-void
.end method

.method public renderToBitmapAsync(Landroid/graphics/Bitmap;)Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;

    invoke-direct {v0}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration$Builder;->build()Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/annotations/Annotation;->renderToBitmapAsync(Landroid/graphics/Bitmap;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public renderToBitmapAsync(Landroid/graphics/Bitmap;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    const-string v0, "bitmap"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 54
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 56
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 108
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 112
    sget-boolean v0, Lcom/pspdfkit/annotations/Annotation;->p:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 114
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    invoke-static {v0, p0, p1, p2}, Lcom/pspdfkit/internal/v1;->a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    .line 115
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Can\'t render annotations that aren\'t attached to a document page!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setAlpha(F)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public setAppearanceStreamGenerator(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->h:Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    invoke-static {p1, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->h:Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    const/4 v0, 0x1

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/annotations/Annotation;->i:Z

    .line 4
    instance-of v0, p0, Lcom/pspdfkit/annotations/StampAnnotation;

    if-eqz v0, :cond_2

    const/16 v0, 0xfa0

    if-eqz p1, :cond_1

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    sget-object v2, Lcom/pspdfkit/annotations/StampAnnotation;->r:Lcom/pspdfkit/annotations/stamps/StampType;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/stamps/StampType;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 8
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/p1;->d(I)V

    .line 12
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_4

    if-eqz p1, :cond_3

    .line 16
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p1

    .line 17
    check-cast p1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/r1;->b()Lcom/pspdfkit/internal/o2;

    move-result-object p1

    .line 18
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/o2;->a(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_1

    .line 21
    :cond_3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p1

    .line 22
    check-cast p1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/r1;->b()Lcom/pspdfkit/internal/o2;

    move-result-object p1

    .line 23
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/o2;->b(Lcom/pspdfkit/annotations/Annotation;)V

    :cond_4
    :goto_1
    return-void
.end method

.method public setBlendMode(Lcom/pspdfkit/annotations/BlendMode;)V
    .locals 2

    const-string v0, "blendMode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x17

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public setBorderColor(I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    return-void
.end method

.method public setBorderDashArray(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public setBorderEffect(Lcom/pspdfkit/annotations/BorderEffect;)V
    .locals 2

    const-string v0, "borderEffect"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x18

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 56
    invoke-direct {p0}, Lcom/pspdfkit/annotations/Annotation;->b()V

    :cond_0
    return-void
.end method

.method public setBorderEffectIntensity(F)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffectIntensity()F

    move-result v0

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    const/16 v1, 0x19

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/annotations/Annotation;->b()V

    return-void
.end method

.method public setBorderStyle(Lcom/pspdfkit/annotations/BorderStyle;)V
    .locals 2

    const-string v0, "borderStyle"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0xe

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public setBorderWidth(F)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    const/16 v1, 0x65

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public setBoundingBox(Landroid/graphics/RectF;)V
    .locals 2

    const-string v0, "newBoundingBox"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    const/16 p1, 0x9

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/p1;->a(ILandroid/graphics/RectF;)V

    .line 58
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p1

    const-string v0, "annotationType"

    .line 59
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->SQUARE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->CIRCLE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-eqz p1, :cond_2

    .line 285
    invoke-direct {p0}, Lcom/pspdfkit/annotations/Annotation;->e()V

    :cond_2
    return-void
.end method

.method public setColor(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0xff

    .line 2
    invoke-static {p1, v1}, Landroidx/core/graphics/ColorUtils;->setAlphaComponent(II)I

    move-result p1

    .line 3
    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/16 v1, 0xa

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    return-void
.end method

.method public setContents(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method public setCreatedDate(Ljava/util/Date;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/util/Date;)V

    return-void
.end method

.method public setCreator(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method public setCustomData(Lorg/json/JSONObject;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x2329

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public setFillColor(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0xff

    .line 2
    invoke-static {p1, v1}, Landroidx/core/graphics/ColorUtils;->setAlphaComponent(II)I

    move-result p1

    .line 3
    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    return-void
.end method

.method public setFlags(Ljava/util/EnumSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationFlags;",
            ">;)V"
        }
    .end annotation

    const-string v0, "flags"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public setInReplyTo(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_REPLIES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->j:Lcom/pspdfkit/annotations/Annotation;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_2

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    if-ne v0, v1, :cond_1

    goto :goto_0

    .line 7
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The annotation that this annotation replies to must have the same page index."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 11
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->j:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->k:Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    if-eqz v1, :cond_3

    .line 13
    iget-object v0, v0, Lcom/pspdfkit/annotations/Annotation;->n:Lcom/pspdfkit/internal/pf;

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    const/4 v0, 0x0

    .line 14
    iput-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->k:Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 17
    :cond_3
    iput-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->j:Lcom/pspdfkit/annotations/Annotation;

    if-eqz p1, :cond_4

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    invoke-direct {p0, v0}, Lcom/pspdfkit/annotations/Annotation;->a(Lcom/pspdfkit/internal/zf;)V

    .line 24
    new-instance v0, Lcom/pspdfkit/annotations/Annotation$1;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/annotations/Annotation$1;-><init>(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/Annotation;)V

    iput-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->k:Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 48
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->k:Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/pf;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 51
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->n:Lcom/pspdfkit/internal/pf;

    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->j:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getUuid()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/pf;->setInReplyToUuid(Ljava/lang/String;)V

    .line 52
    invoke-direct {p0}, Lcom/pspdfkit/annotations/Annotation;->d()V

    :cond_4
    return-void

    .line 53
    :cond_5
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Your current license doesn\'t allow creating annotation replies."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setModifiedDate(Ljava/util/Date;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/util/Date;)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method public setRichText(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method public toInstantJson()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached(Z)Z

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotationManager()Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->toInstantJson(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 8
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t create json from annotation when annotation is not attached!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Annotation["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "]{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/p1;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract updateTransformationProperties(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
.end method

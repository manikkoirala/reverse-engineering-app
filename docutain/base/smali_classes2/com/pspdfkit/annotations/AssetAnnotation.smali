.class public abstract Lcom/pspdfkit/annotations/AssetAnnotation;
.super Lcom/pspdfkit/annotations/LinkAnnotation;
.source "SourceFile"


# instance fields
.field private final q:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/p1;ZLjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/LinkAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    .line 2
    iput-object p3, p0, Lcom/pspdfkit/annotations/AssetAnnotation;->q:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAssetName()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x1b5a

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFileUri(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;)Landroid/net/Uri;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/annotations/AssetAnnotation;->q:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/AssetAnnotation;->getAssetName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 13
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Extracting temporary media file for annotation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "PSPDFKit.Annotations"

    invoke-static {v4, v1, v3}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "TEMP_"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 16
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p1

    invoke-direct {v1, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 19
    :try_start_0
    new-instance p1, Ljava/io/FileOutputStream;

    invoke-direct {p1, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 20
    new-instance v0, Lcom/pspdfkit/internal/sl;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/sl;-><init>(Ljava/io/OutputStream;)V

    .line 21
    check-cast p2, Lcom/pspdfkit/internal/zf;

    .line 22
    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p2

    .line 23
    check-cast p2, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/r1;->d()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object p2

    const/4 v3, 0x0

    .line 24
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v5

    invoke-interface {v5}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v5

    iget-object v6, p0, Lcom/pspdfkit/annotations/AssetAnnotation;->q:Ljava/lang/String;

    invoke-virtual {p2, v3, v5, v6, v0}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->getResource(Lcom/pspdfkit/internal/jni/NativeDocument;Lcom/pspdfkit/internal/jni/NativeAnnotation;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeDataSink;)Lcom/pspdfkit/internal/jni/NativeResult;

    .line 25
    invoke-virtual {v0}, Lcom/pspdfkit/internal/sl;->finish()Z

    .line 26
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    aput-object p0, p2, v2

    const-string v0, "Could not retrieve resource for asset annotation: %s"

    .line 28
    invoke-static {v4, p1, v0, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    :goto_0
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    return-object p1

    .line 32
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string p2, "The asset name has not been defined."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 33
    :cond_1
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string p2, "Trying to extract asset from the annotation, but it has no resource id."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 34
    :cond_2
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string p2, "Annotation is not attached to the document."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

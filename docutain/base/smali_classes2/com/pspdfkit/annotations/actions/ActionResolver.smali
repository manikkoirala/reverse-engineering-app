.class public interface abstract Lcom/pspdfkit/annotations/actions/ActionResolver;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract addDocumentActionListener(Lcom/pspdfkit/document/DocumentActionListener;)V
.end method

.method public abstract executeAction(Lcom/pspdfkit/annotations/actions/Action;)V
.end method

.method public abstract executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V
.end method

.method public abstract removeDocumentActionListener(Lcom/pspdfkit/document/DocumentActionListener;)V
.end method

.class public final Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;
.super Lcom/pspdfkit/annotations/actions/Action;
.source "SourceFile"


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/pspdfkit/document/files/EmbeddedFile;Z)V
    .locals 2

    .line 1
    invoke-interface {p1}, Lcom/pspdfkit/document/files/EmbeddedFile;->getFileName()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;-><init>(Ljava/lang/String;IZLjava/util/List;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/document/files/EmbeddedFile;ZLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/document/files/EmbeddedFile;",
            "Z",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/actions/Action;",
            ">;)V"
        }
    .end annotation

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/document/files/EmbeddedFile;->getFileName()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;-><init>(Ljava/lang/String;IZLjava/util/List;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;IZLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZ",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/actions/Action;",
            ">;)V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p4}, Lcom/pspdfkit/annotations/actions/Action;-><init>(Ljava/util/List;)V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->b:Ljava/lang/String;

    .line 5
    iput p2, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->c:I

    .line 6
    iput-boolean p3, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->d:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 3
    :cond_1
    check-cast p1, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;

    .line 4
    iget v1, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->c:I

    iget v3, p1, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->c:I

    if-eq v1, v3, :cond_2

    return v2

    .line 5
    :cond_2
    iget-boolean v1, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->d:Z

    iget-boolean v3, p1, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->d:Z

    if-eq v1, v3, :cond_3

    return v2

    .line 6
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->b:Ljava/lang/String;

    iget-object p1, p1, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->b:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_4
    if-nez p1, :cond_5

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getPageIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->c:I

    return v0
.end method

.method public getPdfPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/pspdfkit/annotations/actions/ActionType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/actions/ActionType;->GOTO_EMBEDDED:Lcom/pspdfkit/annotations/actions/ActionType;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 2
    iget v1, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->c:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 3
    iget-boolean v1, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->d:Z

    add-int/2addr v0, v1

    return v0
.end method

.method public isNewWindow()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->d:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GoToEmbeddedAction{pdfPath=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', pageIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", newWindow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

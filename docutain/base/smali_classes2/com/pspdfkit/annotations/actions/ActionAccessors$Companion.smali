.class public final Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/annotations/actions/ActionAccessors;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\t\u0008\u0002\u00a2\u0006\u0004\u0008!\u0010\"J,\u0010\n\u001a\u00020\t2\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0006\u001a\u00020\u00052\u000e\u0010\u0008\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0002J\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u000b\u001a\u00020\tJ0\u0010\u0013\u001a\u00020\u00122\u0008\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u00052\u000e\u0010\u0008\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0002J0\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u000f2\u0008\u0010\u0017\u001a\u0004\u0018\u00010\r2\u000e\u0010\u0008\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0002J&\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u000f2\u000e\u0010\u0008\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0002J\u0016\u0010 \u001a\u00020\u001f2\u000e\u0010\u0008\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0002\u00a8\u0006#"
    }
    d2 = {
        "Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;",
        "",
        "",
        "Lcom/pspdfkit/internal/u1;",
        "annotationReferences",
        "",
        "shouldHide",
        "Lcom/pspdfkit/annotations/actions/Action;",
        "subActions",
        "Lcom/pspdfkit/annotations/actions/HideAction;",
        "createHideAction",
        "hideAction",
        "getAnnotationReferences",
        "",
        "pdfPath",
        "",
        "pageIndex",
        "newWindow",
        "Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;",
        "createGoToEmbeddedAction",
        "Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;",
        "renditionActionType",
        "screenAnnotationObjectNumber",
        "javascript",
        "Lcom/pspdfkit/annotations/actions/RenditionAction;",
        "createRenditionAction",
        "Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction$RichMediaExecuteActionType;",
        "actionType",
        "richMediaAnnotationObjectNumber",
        "Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction;",
        "createRichMediaExecuteAction",
        "Lcom/pspdfkit/annotations/actions/ImportDataAction;",
        "createImportDataAction",
        "<init>",
        "()V",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final createGoToEmbeddedAction(Ljava/lang/String;IZLjava/util/List;)Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZ",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/actions/Action;",
            ">;)",
            "Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;-><init>(Ljava/lang/String;IZLjava/util/List;)V

    return-object v0
.end method

.method public final createHideAction(Ljava/util/List;ZLjava/util/List;)Lcom/pspdfkit/annotations/actions/HideAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/u1;",
            ">;Z",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/actions/Action;",
            ">;)",
            "Lcom/pspdfkit/annotations/actions/HideAction;"
        }
    .end annotation

    const-string v0, "annotationReferences"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/actions/HideAction;

    invoke-direct {v0, p1, p2, p3}, Lcom/pspdfkit/annotations/actions/HideAction;-><init>(Ljava/util/List;ZLjava/util/List;)V

    return-object v0
.end method

.method public final createImportDataAction(Ljava/util/List;)Lcom/pspdfkit/annotations/actions/ImportDataAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/actions/Action;",
            ">;)",
            "Lcom/pspdfkit/annotations/actions/ImportDataAction;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/actions/ImportDataAction;

    invoke-direct {v0, p1}, Lcom/pspdfkit/annotations/actions/ImportDataAction;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final createRenditionAction(Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;ILjava/lang/String;Ljava/util/List;)Lcom/pspdfkit/annotations/actions/RenditionAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/actions/Action;",
            ">;)",
            "Lcom/pspdfkit/annotations/actions/RenditionAction;"
        }
    .end annotation

    const-string v0, "renditionActionType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/actions/RenditionAction;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/pspdfkit/annotations/actions/RenditionAction;-><init>(Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;ILjava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public final createRichMediaExecuteAction(Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction$RichMediaExecuteActionType;ILjava/util/List;)Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction$RichMediaExecuteActionType;",
            "I",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/actions/Action;",
            ">;)",
            "Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction;"
        }
    .end annotation

    const-string v0, "actionType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction;

    invoke-direct {v0, p1, p2, p3}, Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction;-><init>(Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction$RichMediaExecuteActionType;ILjava/util/List;)V

    return-object v0
.end method

.method public final getAnnotationReferences(Lcom/pspdfkit/annotations/actions/HideAction;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/actions/HideAction;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/u1;",
            ">;"
        }
    .end annotation

    const-string v0, "hideAction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p1, Lcom/pspdfkit/annotations/actions/HideAction;->c:Ljava/util/List;

    const-string v0, "hideAction.annotationReferences"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.class public final Lcom/pspdfkit/annotations/actions/ActionSender;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/annotations/Annotation;

.field private final b:Lcom/pspdfkit/forms/FormElement;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "annotation"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/annotations/actions/ActionSender;->a:Lcom/pspdfkit/annotations/Annotation;

    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/annotations/actions/ActionSender;->b:Lcom/pspdfkit/forms/FormElement;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/forms/FormElement;)V
    .locals 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "formElement"

    .line 6
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    iput-object p1, p0, Lcom/pspdfkit/annotations/actions/ActionSender;->b:Lcom/pspdfkit/forms/FormElement;

    const/4 p1, 0x0

    .line 8
    iput-object p1, p0, Lcom/pspdfkit/annotations/actions/ActionSender;->a:Lcom/pspdfkit/annotations/Annotation;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/annotations/actions/ActionSender;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2
    :cond_1
    check-cast p1, Lcom/pspdfkit/annotations/actions/ActionSender;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/ActionSender;->a:Lcom/pspdfkit/annotations/Annotation;

    iget-object v3, p1, Lcom/pspdfkit/annotations/actions/ActionSender;->a:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/ActionSender;->b:Lcom/pspdfkit/forms/FormElement;

    iget-object p1, p1, Lcom/pspdfkit/annotations/actions/ActionSender;->b:Lcom/pspdfkit/forms/FormElement;

    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getAnnotation()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/ActionSender;->a:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method public getFormElement()Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/ActionSender;->b:Lcom/pspdfkit/forms/FormElement;

    return-object v0
.end method

.method public getPageIndex()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/ActionSender;->a:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    goto :goto_0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/ActionSender;->b:Lcom/pspdfkit/forms/FormElement;

    if-eqz v0, :cond_1

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    goto :goto_0

    :cond_1
    const/high16 v0, -0x80000000

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/ActionSender;->a:Lcom/pspdfkit/annotations/Annotation;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/ActionSender;->b:Lcom/pspdfkit/forms/FormElement;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

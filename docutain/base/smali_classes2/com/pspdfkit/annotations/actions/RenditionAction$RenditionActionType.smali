.class public final enum Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/annotations/actions/RenditionAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RenditionActionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum PAUSE:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

.field public static final enum PLAY:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

.field public static final enum PLAY_STOP:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

.field public static final enum RESUME:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

.field public static final enum STOP:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

.field public static final enum UNKNOWN:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

.field private static final a:[Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

.field private static final synthetic b:[Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    const-string v1, "PLAY_STOP"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->PLAY_STOP:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    .line 2
    new-instance v1, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    const-string v3, "STOP"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->STOP:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    .line 3
    new-instance v3, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    const-string v5, "PAUSE"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->PAUSE:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    .line 4
    new-instance v5, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    const-string v7, "RESUME"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->RESUME:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    .line 5
    new-instance v7, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    const-string v9, "PLAY"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->PLAY:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    .line 6
    new-instance v9, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    const-string v11, "UNKNOWN"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->UNKNOWN:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    const/4 v11, 0x6

    new-array v11, v11, [Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    aput-object v0, v11, v2

    aput-object v1, v11, v4

    aput-object v3, v11, v6

    aput-object v5, v11, v8

    aput-object v7, v11, v10

    aput-object v9, v11, v12

    .line 7
    sput-object v11, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->b:[Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    .line 16
    invoke-static {}, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->values()[Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->a:[Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromValue(I)Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;
    .locals 2

    if-ltz p0, :cond_1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->a:[Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    array-length v1, v0

    if-lt p0, v1, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    aget-object p0, v0, p0

    return-object p0

    .line 5
    :cond_1
    :goto_0
    sget-object p0, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->UNKNOWN:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->b:[Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    invoke-virtual {v0}, [Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    return-object v0
.end method

.class public abstract Lcom/pspdfkit/annotations/actions/AbstractFormAction;
.super Lcom/pspdfkit/annotations/actions/Action;
.source "SourceFile"


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$AQKhli5SFBzRpFT3117gPPfiD_o(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/String;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->a(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/String;)Lio/reactivex/rxjava3/core/MaybeSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$VFFCtB9CuTOUAAdzJAnEwbfrGiA(Lcom/pspdfkit/annotations/actions/AbstractFormAction;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->a(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$sHGfRjIdAhheSatLQVuZa89wO6M(Lcom/pspdfkit/annotations/actions/AbstractFormAction;Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/ObservableSource;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->a(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/ObservableSource;

    move-result-object p0

    return-object p0
.end method

.method protected constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/actions/Action;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p2}, Lcom/pspdfkit/annotations/actions/Action;-><init>(Ljava/util/List;)V

    const-string p2, "fieldNames"

    .line 2
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->b:Ljava/util/List;

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/String;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 22
    invoke-interface {p0}, Lcom/pspdfkit/document/PdfDocument;->getFormProvider()Lcom/pspdfkit/forms/FormProvider;

    move-result-object p0

    invoke-interface {p0, p1}, Lcom/pspdfkit/forms/FormProvider;->getFormFieldWithFullyQualifiedNameAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    return-object p0
.end method

.method private synthetic a(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/ObservableSource;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 3
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    monitor-exit p0

    return-object p1

    .line 5
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 10
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ACRO_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 12
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getFormProvider()Lcom/pspdfkit/forms/FormProvider;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/forms/FormProvider;->getFormFields()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/forms/FormField;

    .line 13
    iget-object v2, p0, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->b:Ljava/util/List;

    invoke-virtual {v1}, Lcom/pspdfkit/forms/FormField;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->shouldExcludeFormFields()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-ne v2, v3, :cond_1

    .line 14
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 19
    :cond_2
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Observable;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    .line 20
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method private synthetic a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 21
    iput-object p1, p0, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->c:Ljava/util/List;

    return-void
.end method

.method protected static b(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 2

    const-string v0, "formFields"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 54
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/forms/FormField;

    .line 55
    invoke-virtual {v1}, Lcom/pspdfkit/forms/FormField;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 1
    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/annotations/actions/AbstractFormAction;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 2
    :cond_1
    check-cast p1, Lcom/pspdfkit/annotations/actions/AbstractFormAction;

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->b:Ljava/util/List;

    iget-object p1, p1, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->b:Ljava/util/List;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->b:Ljava/util/List;

    return-object v0
.end method

.method public getFormFieldsAsync(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/document/PdfDocument;",
            ")",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormField;",
            ">;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/actions/AbstractFormAction$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/annotations/actions/AbstractFormAction$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/annotations/actions/AbstractFormAction;Lcom/pspdfkit/document/PdfDocument;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    check-cast p1, Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 22
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/annotations/actions/AbstractFormAction$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/annotations/actions/AbstractFormAction$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/annotations/actions/AbstractFormAction;)V

    .line 23
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->doOnNext(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getTargetFormFieldsAsync(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/document/PdfDocument;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormField;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->b:Ljava/util/List;

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/annotations/actions/AbstractFormAction$$ExternalSyntheticLambda2;

    invoke-direct {v1, p1}, Lcom/pspdfkit/annotations/actions/AbstractFormAction$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/document/PdfDocument;)V

    .line 2
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->flatMapMaybe(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Observable;->toList()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    check-cast p1, Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 5
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->b:Ljava/util/List;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public shouldExcludeFormFields()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "fieldNames="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final enum Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/annotations/actions/SubmitFormAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SubmitFormActionFlag"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CANONICAL_FORMAT:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

.field public static final enum EMBED_FORM:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

.field public static final enum EXCLUDE_NON_USER_ANNOTATIONS:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

.field public static final enum EXPORT_FORMAT:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

.field public static final enum GET_METHOD:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

.field public static final enum INCLUDE_ANNOTATIONS:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

.field public static final enum INCLUDE_APPEND_SAVES:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

.field public static final enum INCLUDE_EXCLUDE:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

.field public static final enum INCLUDE_NO_VALUE_FIELDS:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

.field public static final enum SUBMIT_COORDINATES:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

.field public static final enum SUBMIT_PDF:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

.field public static final enum XFDF:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

.field private static final synthetic a:[Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-string v1, "INCLUDE_EXCLUDE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->INCLUDE_EXCLUDE:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    .line 7
    new-instance v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-string v3, "INCLUDE_NO_VALUE_FIELDS"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->INCLUDE_NO_VALUE_FIELDS:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    .line 14
    new-instance v3, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-string v5, "EXPORT_FORMAT"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->EXPORT_FORMAT:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    .line 22
    new-instance v5, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-string v7, "GET_METHOD"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->GET_METHOD:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    .line 30
    new-instance v7, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-string v9, "SUBMIT_COORDINATES"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->SUBMIT_COORDINATES:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    .line 36
    new-instance v9, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-string v11, "XFDF"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->XFDF:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    .line 44
    new-instance v11, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-string v13, "INCLUDE_APPEND_SAVES"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->INCLUDE_APPEND_SAVES:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    .line 52
    new-instance v13, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-string v15, "INCLUDE_ANNOTATIONS"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->INCLUDE_ANNOTATIONS:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    .line 57
    new-instance v15, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-string v14, "SUBMIT_PDF"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->SUBMIT_PDF:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    .line 62
    new-instance v14, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-string v12, "CANONICAL_FORMAT"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->CANONICAL_FORMAT:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    .line 70
    new-instance v12, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-string v10, "EXCLUDE_NON_USER_ANNOTATIONS"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->EXCLUDE_NON_USER_ANNOTATIONS:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    .line 78
    new-instance v10, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-string v8, "EMBED_FORM"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->EMBED_FORM:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const/16 v8, 0xc

    new-array v8, v8, [Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    aput-object v0, v8, v2

    aput-object v1, v8, v4

    const/4 v0, 0x2

    aput-object v3, v8, v0

    const/4 v0, 0x3

    aput-object v5, v8, v0

    const/4 v0, 0x4

    aput-object v7, v8, v0

    const/4 v0, 0x5

    aput-object v9, v8, v0

    const/4 v0, 0x6

    aput-object v11, v8, v0

    const/4 v0, 0x7

    aput-object v13, v8, v0

    const/16 v0, 0x8

    aput-object v15, v8, v0

    const/16 v0, 0x9

    aput-object v14, v8, v0

    const/16 v0, 0xa

    aput-object v12, v8, v0

    aput-object v10, v8, v6

    .line 79
    sput-object v8, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->a:[Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->a:[Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    invoke-virtual {v0}, [Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    return-object v0
.end method

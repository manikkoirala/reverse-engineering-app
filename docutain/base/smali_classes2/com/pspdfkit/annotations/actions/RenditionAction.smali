.class public Lcom/pspdfkit/annotations/actions/RenditionAction;
.super Lcom/pspdfkit/annotations/actions/AbstractMediaAction;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;
    }
.end annotation


# instance fields
.field private final c:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

.field private final d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;ILjava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/actions/Action;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p2, p4}, Lcom/pspdfkit/annotations/actions/AbstractMediaAction;-><init>(ILjava/util/List;)V

    const-string p2, "renditionActionType"

    .line 2
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/annotations/actions/RenditionAction;->c:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/annotations/actions/RenditionAction;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/annotations/actions/RenditionAction;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2
    :cond_1
    invoke-super {p0, p1}, Lcom/pspdfkit/annotations/actions/AbstractMediaAction;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    .line 3
    :cond_2
    check-cast p1, Lcom/pspdfkit/annotations/actions/RenditionAction;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/RenditionAction;->c:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    iget-object v3, p1, Lcom/pspdfkit/annotations/actions/RenditionAction;->c:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/RenditionAction;->d:Ljava/lang/String;

    iget-object p1, p1, Lcom/pspdfkit/annotations/actions/RenditionAction;->d:Ljava/lang/String;

    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getJavascript()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/RenditionAction;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getRenditionActionType()Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/RenditionAction;->c:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    return-object v0
.end method

.method public getScreenAnnotationAsync(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/document/PdfDocument;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/ScreenAnnotation;",
            ">;"
        }
    .end annotation

    const-string v0, "pdfDocument"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/actions/AbstractMediaAction;->a(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    const-class v0, Lcom/pspdfkit/annotations/ScreenAnnotation;

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->cast(Ljava/lang/Class;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public getScreenAnnotationObjectNumber()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/actions/AbstractMediaAction;->a()I

    move-result v0

    return v0
.end method

.method public getType()Lcom/pspdfkit/annotations/actions/ActionType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/actions/ActionType;->RENDITION:Lcom/pspdfkit/annotations/actions/ActionType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/annotations/actions/AbstractMediaAction;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/RenditionAction;->c:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/RenditionAction;->d:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RenditionAction{renditionActionType="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/RenditionAction;->c:Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", screenAnnotationObjectNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/actions/RenditionAction;->getScreenAnnotationObjectNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", javascript=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/annotations/actions/RenditionAction;->d:Ljava/lang/String;

    const-string v2, "\'}"

    .line 6
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/rg;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

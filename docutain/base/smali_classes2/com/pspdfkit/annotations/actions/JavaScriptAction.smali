.class public final Lcom/pspdfkit/annotations/actions/JavaScriptAction;
.super Lcom/pspdfkit/annotations/actions/Action;
.source "SourceFile"


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0}, Lcom/pspdfkit/annotations/actions/Action;-><init>(Ljava/util/List;)V

    const-string v0, "script"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/annotations/actions/JavaScriptAction;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/actions/Action;",
            ">;)V"
        }
    .end annotation

    .line 4
    invoke-direct {p0, p2}, Lcom/pspdfkit/annotations/actions/Action;-><init>(Ljava/util/List;)V

    if-nez p1, :cond_0

    const-string p1, ""

    .line 5
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/annotations/actions/JavaScriptAction;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 1
    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/annotations/actions/JavaScriptAction;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 3
    :cond_1
    check-cast p1, Lcom/pspdfkit/annotations/actions/JavaScriptAction;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/JavaScriptAction;->b:Ljava/lang/String;

    iget-object p1, p1, Lcom/pspdfkit/annotations/actions/JavaScriptAction;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getScript()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/JavaScriptAction;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/pspdfkit/annotations/actions/ActionType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/actions/ActionType;->JAVASCRIPT:Lcom/pspdfkit/annotations/actions/ActionType;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/actions/JavaScriptAction;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.class public final enum Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CURSOR_ENTERS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

.field public static final enum CURSOR_EXITS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

.field public static final enum FIELD_FORMAT:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

.field public static final enum FORM_CALCULATE:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

.field public static final enum FORM_CHANGED:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

.field public static final enum FORM_VALIDATE:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

.field public static final enum LOOSE_FOCUS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

.field public static final enum MOUSE_DOWN:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

.field public static final enum MOUSE_UP:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

.field public static final enum PAGE_CLOSED:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

.field public static final enum PAGE_OPENED:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

.field public static final enum PAGE_VISIBLE:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

.field public static final enum RECEIVE_FOCUS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

.field private static final synthetic a:[Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const-string v1, "CURSOR_ENTERS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->CURSOR_ENTERS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 3
    new-instance v1, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const-string v3, "CURSOR_EXITS"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->CURSOR_EXITS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 5
    new-instance v3, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const-string v5, "MOUSE_DOWN"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->MOUSE_DOWN:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 13
    new-instance v5, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const-string v7, "MOUSE_UP"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->MOUSE_UP:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 15
    new-instance v7, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const-string v9, "RECEIVE_FOCUS"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->RECEIVE_FOCUS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 17
    new-instance v9, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const-string v11, "LOOSE_FOCUS"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->LOOSE_FOCUS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 20
    new-instance v11, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const-string v13, "PAGE_OPENED"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->PAGE_OPENED:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 22
    new-instance v13, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const-string v15, "PAGE_CLOSED"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->PAGE_CLOSED:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 24
    new-instance v15, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const-string v14, "PAGE_VISIBLE"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->PAGE_VISIBLE:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 30
    new-instance v14, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const-string v12, "FORM_CHANGED"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->FORM_CHANGED:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 35
    new-instance v12, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const-string v10, "FIELD_FORMAT"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->FIELD_FORMAT:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 40
    new-instance v10, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const-string v8, "FORM_VALIDATE"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->FORM_VALIDATE:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 45
    new-instance v8, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const-string v6, "FORM_CALCULATE"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->FORM_CALCULATE:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const/16 v6, 0xd

    new-array v6, v6, [Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    aput-object v0, v6, v2

    const/4 v0, 0x1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    aput-object v3, v6, v0

    const/4 v0, 0x3

    aput-object v5, v6, v0

    const/4 v0, 0x4

    aput-object v7, v6, v0

    const/4 v0, 0x5

    aput-object v9, v6, v0

    const/4 v0, 0x6

    aput-object v11, v6, v0

    const/4 v0, 0x7

    aput-object v13, v6, v0

    const/16 v0, 0x8

    aput-object v15, v6, v0

    const/16 v0, 0x9

    aput-object v14, v6, v0

    const/16 v0, 0xa

    aput-object v12, v6, v0

    const/16 v0, 0xb

    aput-object v10, v6, v0

    aput-object v8, v6, v4

    .line 46
    sput-object v6, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->a:[Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->a:[Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    invoke-virtual {v0}, [Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object v0
.end method

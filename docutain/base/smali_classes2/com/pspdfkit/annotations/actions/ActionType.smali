.class public final enum Lcom/pspdfkit/annotations/actions/ActionType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/annotations/actions/ActionType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum GOTO:Lcom/pspdfkit/annotations/actions/ActionType;

.field public static final enum GOTO_EMBEDDED:Lcom/pspdfkit/annotations/actions/ActionType;

.field public static final enum GOTO_REMOTE:Lcom/pspdfkit/annotations/actions/ActionType;

.field public static final enum HIDE:Lcom/pspdfkit/annotations/actions/ActionType;

.field public static final enum IMPORT_DATA:Lcom/pspdfkit/annotations/actions/ActionType;

.field public static final enum JAVASCRIPT:Lcom/pspdfkit/annotations/actions/ActionType;

.field public static final enum LAUNCH:Lcom/pspdfkit/annotations/actions/ActionType;

.field public static final enum NAMED:Lcom/pspdfkit/annotations/actions/ActionType;

.field public static final enum RENDITION:Lcom/pspdfkit/annotations/actions/ActionType;

.field public static final enum RESET_FORM:Lcom/pspdfkit/annotations/actions/ActionType;

.field public static final enum RICH_MEDIA_EXECUTE:Lcom/pspdfkit/annotations/actions/ActionType;

.field public static final enum SUBMIT_FORM:Lcom/pspdfkit/annotations/actions/ActionType;

.field public static final enum URI:Lcom/pspdfkit/annotations/actions/ActionType;

.field private static final synthetic a:[Lcom/pspdfkit/annotations/actions/ActionType;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/actions/ActionType;

    const-string v1, "GOTO"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/annotations/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/annotations/actions/ActionType;->GOTO:Lcom/pspdfkit/annotations/actions/ActionType;

    .line 2
    new-instance v1, Lcom/pspdfkit/annotations/actions/ActionType;

    const-string v3, "GOTO_REMOTE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/annotations/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/annotations/actions/ActionType;->GOTO_REMOTE:Lcom/pspdfkit/annotations/actions/ActionType;

    .line 3
    new-instance v3, Lcom/pspdfkit/annotations/actions/ActionType;

    const-string v5, "GOTO_EMBEDDED"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/annotations/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/annotations/actions/ActionType;->GOTO_EMBEDDED:Lcom/pspdfkit/annotations/actions/ActionType;

    .line 4
    new-instance v5, Lcom/pspdfkit/annotations/actions/ActionType;

    const-string v7, "LAUNCH"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/annotations/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/annotations/actions/ActionType;->LAUNCH:Lcom/pspdfkit/annotations/actions/ActionType;

    .line 6
    new-instance v7, Lcom/pspdfkit/annotations/actions/ActionType;

    const-string v9, "URI"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/annotations/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/annotations/actions/ActionType;->URI:Lcom/pspdfkit/annotations/actions/ActionType;

    .line 9
    new-instance v9, Lcom/pspdfkit/annotations/actions/ActionType;

    const-string v11, "HIDE"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/annotations/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/annotations/actions/ActionType;->HIDE:Lcom/pspdfkit/annotations/actions/ActionType;

    .line 10
    new-instance v11, Lcom/pspdfkit/annotations/actions/ActionType;

    const-string v13, "NAMED"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/annotations/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/annotations/actions/ActionType;->NAMED:Lcom/pspdfkit/annotations/actions/ActionType;

    .line 11
    new-instance v13, Lcom/pspdfkit/annotations/actions/ActionType;

    const-string v15, "SUBMIT_FORM"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/annotations/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/annotations/actions/ActionType;->SUBMIT_FORM:Lcom/pspdfkit/annotations/actions/ActionType;

    .line 12
    new-instance v15, Lcom/pspdfkit/annotations/actions/ActionType;

    const-string v14, "RESET_FORM"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/annotations/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/annotations/actions/ActionType;->RESET_FORM:Lcom/pspdfkit/annotations/actions/ActionType;

    .line 13
    new-instance v14, Lcom/pspdfkit/annotations/actions/ActionType;

    const-string v12, "RENDITION"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/annotations/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/annotations/actions/ActionType;->RENDITION:Lcom/pspdfkit/annotations/actions/ActionType;

    .line 14
    new-instance v12, Lcom/pspdfkit/annotations/actions/ActionType;

    const-string v10, "RICH_MEDIA_EXECUTE"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/annotations/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/annotations/actions/ActionType;->RICH_MEDIA_EXECUTE:Lcom/pspdfkit/annotations/actions/ActionType;

    .line 15
    new-instance v10, Lcom/pspdfkit/annotations/actions/ActionType;

    const-string v8, "IMPORT_DATA"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/annotations/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/annotations/actions/ActionType;->IMPORT_DATA:Lcom/pspdfkit/annotations/actions/ActionType;

    .line 16
    new-instance v8, Lcom/pspdfkit/annotations/actions/ActionType;

    const-string v6, "JAVASCRIPT"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/annotations/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/annotations/actions/ActionType;->JAVASCRIPT:Lcom/pspdfkit/annotations/actions/ActionType;

    const/16 v6, 0xd

    new-array v6, v6, [Lcom/pspdfkit/annotations/actions/ActionType;

    aput-object v0, v6, v2

    const/4 v0, 0x1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    aput-object v3, v6, v0

    const/4 v0, 0x3

    aput-object v5, v6, v0

    const/4 v0, 0x4

    aput-object v7, v6, v0

    const/4 v0, 0x5

    aput-object v9, v6, v0

    const/4 v0, 0x6

    aput-object v11, v6, v0

    const/4 v0, 0x7

    aput-object v13, v6, v0

    const/16 v0, 0x8

    aput-object v15, v6, v0

    const/16 v0, 0x9

    aput-object v14, v6, v0

    const/16 v0, 0xa

    aput-object v12, v6, v0

    const/16 v0, 0xb

    aput-object v10, v6, v0

    aput-object v8, v6, v4

    .line 17
    sput-object v6, Lcom/pspdfkit/annotations/actions/ActionType;->a:[Lcom/pspdfkit/annotations/actions/ActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/actions/ActionType;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/actions/ActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/actions/ActionType;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/annotations/actions/ActionType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/actions/ActionType;->a:[Lcom/pspdfkit/annotations/actions/ActionType;

    invoke-virtual {v0}, [Lcom/pspdfkit/annotations/actions/ActionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/annotations/actions/ActionType;

    return-object v0
.end method

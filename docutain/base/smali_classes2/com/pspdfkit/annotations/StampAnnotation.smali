.class public Lcom/pspdfkit/annotations/StampAnnotation;
.super Lcom/pspdfkit/annotations/Annotation;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/ResizableAnnotation;


# static fields
.field static final q:Lcom/pspdfkit/annotations/stamps/StampType;

.field static final r:Lcom/pspdfkit/annotations/stamps/StampType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/stamps/StampType;

    const-string v1, "#Image"

    invoke-direct {v0, v1}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/annotations/StampAnnotation;->q:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 3
    new-instance v0, Lcom/pspdfkit/annotations/stamps/StampType;

    const-string v1, "#CustomAp"

    invoke-direct {v0, v1}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/annotations/StampAnnotation;->r:Lcom/pspdfkit/annotations/stamps/StampType;

    return-void
.end method

.method public constructor <init>(ILandroid/graphics/RectF;Landroid/graphics/Bitmap;)V
    .locals 1

    .line 7
    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/Annotation;-><init>(I)V

    const-string p1, "rect"

    .line 8
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "bitmap"

    .line 9
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v0, 0x9

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/p1;->a(ILandroid/graphics/RectF;)V

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    sget-object p2, Lcom/pspdfkit/annotations/StampAnnotation;->q:Lcom/pspdfkit/annotations/stamps/StampType;

    invoke-virtual {p2}, Lcom/pspdfkit/annotations/stamps/StampType;->getName()Ljava/lang/String;

    move-result-object p2

    const/16 v0, 0xfa0

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/a0;

    invoke-direct {p2, p0, p3}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;)V

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    return-void
.end method

.method public constructor <init>(ILandroid/graphics/RectF;Lcom/pspdfkit/annotations/stamps/StampType;)V
    .locals 1

    .line 13
    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/Annotation;-><init>(I)V

    const-string p1, "rect"

    .line 14
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v0, 0x9

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/p1;->a(ILandroid/graphics/RectF;)V

    .line 16
    invoke-virtual {p0, p3}, Lcom/pspdfkit/annotations/StampAnnotation;->setStampType(Lcom/pspdfkit/annotations/stamps/StampType;)V

    return-void
.end method

.method public constructor <init>(ILandroid/graphics/RectF;Ljava/lang/String;)V
    .locals 1

    .line 17
    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/Annotation;-><init>(I)V

    const-string p1, "rect"

    .line 18
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "title"

    .line 19
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v0, 0x9

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/p1;->a(ILandroid/graphics/RectF;)V

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 p2, 0x1772

    invoke-virtual {p1, p2, p3}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(ILandroid/graphics/RectF;[B)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/Annotation;-><init>(I)V

    const-string p1, "rect"

    .line 2
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "compressedBitmap"

    .line 3
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v0, 0x9

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/p1;->a(ILandroid/graphics/RectF;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    sget-object p2, Lcom/pspdfkit/annotations/StampAnnotation;->q:Lcom/pspdfkit/annotations/stamps/StampType;

    invoke-virtual {p2}, Lcom/pspdfkit/annotations/stamps/StampType;->getName()Ljava/lang/String;

    move-result-object p2

    const/16 v0, 0xfa0

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/a0;

    invoke-direct {p2, p0, p3}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;[B)V

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/p1;ZLandroid/graphics/Bitmap;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/Annotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    if-eqz p3, :cond_0

    .line 27
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/a0;

    invoke-direct {p2, p0, p3}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;)V

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/p1;ZLjava/lang/String;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/Annotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    if-eqz p3, :cond_0

    .line 24
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/a0;

    invoke-direct {p2, p0, p3}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    :cond_0
    return-void
.end method


# virtual methods
.method final a()Lcom/pspdfkit/annotations/Annotation;
    .locals 4

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    new-instance v0, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/annotations/StampAnnotation;-><init>(Lcom/pspdfkit/internal/p1;ZLandroid/graphics/Bitmap;)V

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->prepareForCopy()V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getAppearanceStreamGenerator()Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 7
    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->setAppearanceStreamGenerator(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;)V

    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getAnnotationResource()Lcom/pspdfkit/internal/x1;

    move-result-object v1

    .line 9
    instance-of v2, v1, Lcom/pspdfkit/internal/a0;

    if-eqz v2, :cond_1

    .line 10
    move-object v3, v1

    check-cast v3, Lcom/pspdfkit/internal/a0;

    :cond_1
    if-eqz v3, :cond_2

    .line 11
    invoke-virtual {v3}, Lcom/pspdfkit/internal/a0;->j()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 13
    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/StampAnnotation;->setBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :goto_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public adjustBoundsForRotation()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->adjustBoundsForRotation(F)V

    return-void
.end method

.method public declared-synchronized getBitmap()Landroid/graphics/Bitmap;
    .locals 3

    monitor-enter p0

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getAnnotationResource()Lcom/pspdfkit/internal/x1;

    move-result-object v0

    .line 2
    instance-of v1, v0, Lcom/pspdfkit/internal/a0;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 3
    check-cast v0, Lcom/pspdfkit/internal/a0;

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/a0;->j()Landroid/graphics/Bitmap;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-object v2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getMinimumSize()Lcom/pspdfkit/utils/Size;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->getContentSize(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Landroid/graphics/RectF;->sort()V

    .line 5
    new-instance v1, Lcom/pspdfkit/utils/Size;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    const v3, 0x3e4ccccd    # 0.2f

    mul-float v2, v2, v3

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    mul-float v0, v0, v3

    invoke-direct {v1, v2, v0}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    return-object v1

    .line 7
    :cond_0
    invoke-super {p0}, Lcom/pspdfkit/annotations/Annotation;->getMinimumSize()Lcom/pspdfkit/utils/Size;

    move-result-object v0

    return-object v0
.end method

.method public getRotation()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result v0

    return v0
.end method

.method public getStampType()Lcom/pspdfkit/annotations/stamps/StampType;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0xfa0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 2
    :cond_0
    new-instance v1, Lcom/pspdfkit/annotations/stamps/StampType;

    invoke-direct {v1, v0}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    :goto_0
    return-object v0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x1771

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x1772

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/pspdfkit/annotations/AnnotationType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    return-object v0
.end method

.method public declared-synchronized hasBitmap()Z
    .locals 2

    monitor-enter p0

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getAnnotationResource()Lcom/pspdfkit/internal/x1;

    move-result-object v0

    .line 2
    instance-of v1, v0, Lcom/pspdfkit/internal/a0;

    if-eqz v1, :cond_0

    .line 3
    check-cast v0, Lcom/pspdfkit/internal/a0;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/a0;->o()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setBitmap(Landroid/graphics/Bitmap;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "bitmap"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bitmap"

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getAnnotationResource()Lcom/pspdfkit/internal/x1;

    move-result-object v0

    .line 54
    instance-of v2, v0, Lcom/pspdfkit/internal/a0;

    if-eqz v2, :cond_0

    .line 55
    check-cast v0, Lcom/pspdfkit/internal/a0;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    .line 56
    invoke-virtual {p0, v1}, Lcom/pspdfkit/annotations/StampAnnotation;->setTitle(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0, v1}, Lcom/pspdfkit/annotations/StampAnnotation;->setStampType(Lcom/pspdfkit/annotations/stamps/StampType;)V

    .line 58
    invoke-virtual {p0, v1}, Lcom/pspdfkit/annotations/StampAnnotation;->setSubtitle(Ljava/lang/String;)V

    .line 60
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/a0;

    const-string v2, "annotation"

    .line 61
    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "bitmap"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;I)V

    .line 62
    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    .line 63
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    sget-object v0, Lcom/pspdfkit/annotations/StampAnnotation;->q:Lcom/pspdfkit/annotations/stamps/StampType;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/stamps/StampType;->getName()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xfa0

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setBitmap([B)V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "compressedBitmap"

    const-string v1, "argumentName"

    .line 64
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "compressedBitmap"

    const/4 v1, 0x0

    .line 115
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 116
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getAnnotationResource()Lcom/pspdfkit/internal/x1;

    move-result-object v0

    .line 117
    instance-of v2, v0, Lcom/pspdfkit/internal/a0;

    if-eqz v2, :cond_0

    .line 118
    check-cast v0, Lcom/pspdfkit/internal/a0;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    .line 119
    invoke-virtual {p0, v1}, Lcom/pspdfkit/annotations/StampAnnotation;->setTitle(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0, v1}, Lcom/pspdfkit/annotations/StampAnnotation;->setStampType(Lcom/pspdfkit/annotations/stamps/StampType;)V

    .line 121
    invoke-virtual {p0, v1}, Lcom/pspdfkit/annotations/StampAnnotation;->setSubtitle(Ljava/lang/String;)V

    .line 123
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/a0;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;[B)V

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    .line 124
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    sget-object v0, Lcom/pspdfkit/annotations/StampAnnotation;->q:Lcom/pspdfkit/annotations/stamps/StampType;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/stamps/StampType;->getName()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xfa0

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setIsSignature(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/pf;->setIsSignature(Z)V

    return-void
.end method

.method public setRotation(I)V
    .locals 1

    const/4 v0, 0x1

    .line 62
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/annotations/StampAnnotation;->setRotation(IZ)V

    return-void
.end method

.method public setRotation(ILcom/pspdfkit/utils/Size;)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, p1, p2, v0}, Lcom/pspdfkit/annotations/StampAnnotation;->setRotation(ILcom/pspdfkit/utils/Size;Z)V

    return-void
.end method

.method public setRotation(ILcom/pspdfkit/utils/Size;Z)V
    .locals 2

    const-string v0, "contentSize"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/pf;->setRotation(I)V

    .line 57
    new-instance p1, Landroid/graphics/RectF;

    iget v0, p2, Lcom/pspdfkit/utils/Size;->height:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget p2, p2, Lcom/pspdfkit/utils/Size;->width:F

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p2

    const/4 v1, 0x0

    invoke-direct {p1, v1, v0, p2, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 58
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p2

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Lcom/pspdfkit/internal/pf;->setContentSize(Landroid/graphics/RectF;Z)V

    if-eqz p3, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/StampAnnotation;->adjustBoundsForRotation()V

    :cond_0
    return-void
.end method

.method public setRotation(IZ)V
    .locals 2

    .line 63
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/pf;->setRotation(I)V

    .line 64
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/pf;->getContentSize(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p1

    if-nez p1, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/pspdfkit/internal/pf;->setContentSize(Landroid/graphics/RectF;Z)V

    :cond_0
    if-eqz p2, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/StampAnnotation;->adjustBoundsForRotation()V

    :cond_1
    return-void
.end method

.method public declared-synchronized setStampType(Lcom/pspdfkit/annotations/stamps/StampType;)V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampType;->getName()Ljava/lang/String;

    move-result-object v0

    .line 4
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0xfa0

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :goto_1
    monitor-exit p0

    throw p1
.end method

.method public setSubtitle(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x1771

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x1772

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method public toInstantJson()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/StampAnnotation;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/StampAnnotation;->getStampType()Lcom/pspdfkit/annotations/stamps/StampType;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/StampAnnotation;->hasBitmap()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t create Instant JSON for stamp annotation that has no content - title, stamp icon or an image!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5
    :cond_1
    :goto_0
    invoke-super {p0}, Lcom/pspdfkit/annotations/Annotation;->toInstantJson()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateTransformationProperties(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 0

    return-void
.end method

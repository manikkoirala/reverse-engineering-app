.class Lcom/pspdfkit/annotations/Annotation$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/annotations/Annotation;->setInReplyTo(Lcom/pspdfkit/annotations/Annotation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/pspdfkit/annotations/Annotation;

.field final synthetic c:Lcom/pspdfkit/annotations/Annotation;


# direct methods
.method constructor <init>(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/annotations/Annotation$1;->c:Lcom/pspdfkit/annotations/Annotation;

    iput-object p2, p0, Lcom/pspdfkit/annotations/Annotation$1;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation$1;->b:Lcom/pspdfkit/annotations/Annotation;

    if-eq p1, v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$1;->c:Lcom/pspdfkit/annotations/Annotation;

    iget-object v0, p1, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    invoke-static {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$ma(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/zf;)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$1;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {p1}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetn(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-static {p1}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$fgetj(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getUuid()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/pf;->setInReplyToUuid(Ljava/lang/String;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation$1;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {p1}, Lcom/pspdfkit/annotations/Annotation;->-$$Nest$md(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

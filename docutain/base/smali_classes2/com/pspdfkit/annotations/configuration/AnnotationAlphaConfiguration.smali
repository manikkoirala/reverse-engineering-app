.class public interface abstract Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration$Builder;
    }
.end annotation


# virtual methods
.method public abstract getDefaultAlpha()F
.end method

.method public abstract getMaxAlpha()F
.end method

.method public abstract getMinAlpha()F
.end method

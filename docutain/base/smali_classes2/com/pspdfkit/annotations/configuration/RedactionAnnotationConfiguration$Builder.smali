.class public interface abstract Lcom/pspdfkit/annotations/configuration/RedactionAnnotationConfiguration$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationPreviewConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration$Builder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/annotations/configuration/RedactionAnnotationConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration$Builder<",
        "Lcom/pspdfkit/annotations/configuration/RedactionAnnotationConfiguration$Builder;",
        ">;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration$Builder<",
        "Lcom/pspdfkit/annotations/configuration/RedactionAnnotationConfiguration$Builder;",
        ">;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationPreviewConfiguration$Builder<",
        "Lcom/pspdfkit/annotations/configuration/RedactionAnnotationConfiguration$Builder;",
        ">;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration$Builder<",
        "Lcom/pspdfkit/annotations/configuration/RedactionAnnotationConfiguration$Builder;",
        ">;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration$Builder<",
        "Lcom/pspdfkit/annotations/configuration/RedactionAnnotationConfiguration$Builder;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract build()Lcom/pspdfkit/annotations/configuration/RedactionAnnotationConfiguration;
.end method

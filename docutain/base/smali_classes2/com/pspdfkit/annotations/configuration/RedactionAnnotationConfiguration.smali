.class public interface abstract Lcom/pspdfkit/annotations/configuration/RedactionAnnotationConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationPreviewConfiguration;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/configuration/RedactionAnnotationConfiguration$Builder;
    }
.end annotation

.class public interface abstract Lcom/pspdfkit/annotations/configuration/SoundAnnotationConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/configuration/SoundAnnotationConfiguration$Builder;
    }
.end annotation


# virtual methods
.method public abstract getAudioRecordingTimeLimit()I
.end method

.method public abstract getRecordingSampleRate()I
.end method

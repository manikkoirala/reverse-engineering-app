.class public interface abstract Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration$Builder;
    }
.end annotation


# virtual methods
.method public abstract getDefaultThickness()F
.end method

.method public abstract getMaxThickness()F
.end method

.method public abstract getMinThickness()F
.end method

.class public interface abstract Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$Builder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$Builder<",
        "Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration$Builder;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract build()Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration;
.end method

.method public abstract setAvailableStampPickerItems(Ljava/util/List;)Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/stamps/StampPickerItem;",
            ">;)",
            "Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration$Builder;"
        }
    .end annotation
.end method

.class public final enum Lcom/pspdfkit/annotations/configuration/AnnotationProperty;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/annotations/configuration/AnnotationProperty;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ANNOTATION_ALPHA:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum ANNOTATION_NOTE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum BORDER_STYLE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum FILL_COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum FLOAT_PRECISION:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum FONT:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum LINE_ENDS:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum LINE_ENDS_FILL_COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum NOTE_ICON:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum OUTLINE_COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum OVERLAY_TEXT:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum REPEAT_OVERLAY_TEXT:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum SCALE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum TEXT_SIZE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field public static final enum THICKNESS:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

.field private static final synthetic a:[Lcom/pspdfkit/annotations/configuration/AnnotationProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 19

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v1, "COLOR"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 7
    new-instance v1, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v3, "FILL_COLOR"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->FILL_COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 16
    new-instance v3, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v5, "THICKNESS"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->THICKNESS:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 22
    new-instance v5, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v7, "TEXT_SIZE"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->TEXT_SIZE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 29
    new-instance v7, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v9, "BORDER_STYLE"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->BORDER_STYLE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 35
    new-instance v9, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v11, "LINE_ENDS"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->LINE_ENDS:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 41
    new-instance v11, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v13, "LINE_ENDS_FILL_COLOR"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->LINE_ENDS_FILL_COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 43
    new-instance v13, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v15, "ANNOTATION_NOTE"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->ANNOTATION_NOTE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 49
    new-instance v15, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v14, "ANNOTATION_ALPHA"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->ANNOTATION_ALPHA:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 55
    new-instance v14, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v12, "FONT"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->FONT:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 62
    new-instance v12, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v10, "OUTLINE_COLOR"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->OUTLINE_COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 69
    new-instance v10, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v8, "REPEAT_OVERLAY_TEXT"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->REPEAT_OVERLAY_TEXT:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 76
    new-instance v8, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v6, "OVERLAY_TEXT"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->OVERLAY_TEXT:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 82
    new-instance v6, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v4, "NOTE_ICON"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->NOTE_ICON:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 88
    new-instance v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v2, "FLOAT_PRECISION"

    move-object/from16 v17, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->FLOAT_PRECISION:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 94
    new-instance v2, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const-string v6, "SCALE"

    move-object/from16 v18, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->SCALE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const/16 v6, 0x10

    new-array v6, v6, [Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const/16 v16, 0x0

    aput-object v0, v6, v16

    const/4 v0, 0x1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    aput-object v3, v6, v0

    const/4 v0, 0x3

    aput-object v5, v6, v0

    const/4 v0, 0x4

    aput-object v7, v6, v0

    const/4 v0, 0x5

    aput-object v9, v6, v0

    const/4 v0, 0x6

    aput-object v11, v6, v0

    const/4 v0, 0x7

    aput-object v13, v6, v0

    const/16 v0, 0x8

    aput-object v15, v6, v0

    const/16 v0, 0x9

    aput-object v14, v6, v0

    const/16 v0, 0xa

    aput-object v12, v6, v0

    const/16 v0, 0xb

    aput-object v10, v6, v0

    const/16 v0, 0xc

    aput-object v8, v6, v0

    const/16 v0, 0xd

    aput-object v17, v6, v0

    const/16 v0, 0xe

    aput-object v18, v6, v0

    aput-object v2, v6, v4

    .line 95
    sput-object v6, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->a:[Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/configuration/AnnotationProperty;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/annotations/configuration/AnnotationProperty;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->a:[Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-virtual {v0}, [Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    return-object v0
.end method

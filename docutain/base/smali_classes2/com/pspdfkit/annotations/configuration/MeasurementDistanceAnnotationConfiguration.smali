.class public interface abstract Lcom/pspdfkit/annotations/configuration/MeasurementDistanceAnnotationConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationPreviewConfiguration;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/configuration/MeasurementDistanceAnnotationConfiguration$Builder;
    }
.end annotation

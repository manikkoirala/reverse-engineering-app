.class public interface abstract Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$Builder;
    }
.end annotation


# virtual methods
.method public abstract getForceDefaults()Z
.end method

.method public abstract getSupportedProperties()Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/configuration/AnnotationProperty;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isZIndexEditingEnabled()Z
.end method

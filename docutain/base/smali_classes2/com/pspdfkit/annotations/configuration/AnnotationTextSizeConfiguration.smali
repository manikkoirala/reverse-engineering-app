.class public interface abstract Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration$Builder;
    }
.end annotation


# virtual methods
.method public abstract getDefaultTextSize()F
.end method

.method public abstract getMaxTextSize()F
.end method

.method public abstract getMinTextSize()F
.end method

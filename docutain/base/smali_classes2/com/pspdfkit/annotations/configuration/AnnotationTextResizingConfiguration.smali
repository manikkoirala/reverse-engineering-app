.class public interface abstract Lcom/pspdfkit/annotations/configuration/AnnotationTextResizingConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/configuration/AnnotationTextResizingConfiguration$Builder;
    }
.end annotation


# virtual methods
.method public abstract isHorizontalResizingEnabled()Z
.end method

.method public abstract isVerticalResizingEnabled()Z
.end method

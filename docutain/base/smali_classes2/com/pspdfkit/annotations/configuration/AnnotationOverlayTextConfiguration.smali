.class public interface abstract Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration$Builder;
    }
.end annotation


# virtual methods
.method public abstract getDefaultOverlayText()Ljava/lang/String;
.end method

.method public abstract getDefaultRepeatOverlayTextSetting()Z
.end method

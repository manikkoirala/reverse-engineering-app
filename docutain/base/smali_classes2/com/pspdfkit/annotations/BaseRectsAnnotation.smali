.class public abstract Lcom/pspdfkit/annotations/BaseRectsAnnotation;
.super Lcom/pspdfkit/annotations/Annotation;
.source "SourceFile"


# direct methods
.method constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/Annotation;-><init>(I)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/p1;Z)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/Annotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    return-void
.end method


# virtual methods
.method public getRects()Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getQuadrilaterals()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0

    :cond_0
    const-string v1, "quads"

    const-string v2, "argumentName"

    .line 5
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 56
    invoke-static {v0, v1, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 57
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 58
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/ho;

    const-string v5, "quad"

    .line 59
    invoke-static {v5, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    invoke-static {v4, v5, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 111
    new-instance v5, Landroid/graphics/RectF;

    const/4 v6, 0x4

    new-array v7, v6, [F

    iget v8, v4, Lcom/pspdfkit/internal/ho;->a:F

    const/4 v9, 0x0

    aput v8, v7, v9

    iget v8, v4, Lcom/pspdfkit/internal/ho;->c:F

    const/4 v10, 0x1

    aput v8, v7, v10

    iget v8, v4, Lcom/pspdfkit/internal/ho;->e:F

    const/4 v11, 0x2

    aput v8, v7, v11

    iget v8, v4, Lcom/pspdfkit/internal/ho;->g:F

    const/4 v12, 0x3

    aput v8, v7, v12

    .line 113
    invoke-static {v7}, Lcom/pspdfkit/internal/di;->b([F)F

    move-result v7

    new-array v8, v6, [F

    iget v13, v4, Lcom/pspdfkit/internal/ho;->b:F

    aput v13, v8, v9

    iget v13, v4, Lcom/pspdfkit/internal/ho;->d:F

    aput v13, v8, v10

    iget v13, v4, Lcom/pspdfkit/internal/ho;->f:F

    aput v13, v8, v11

    iget v13, v4, Lcom/pspdfkit/internal/ho;->h:F

    aput v13, v8, v12

    .line 115
    invoke-static {v8}, Lcom/pspdfkit/internal/di;->a([F)F

    move-result v8

    new-array v13, v6, [F

    iget v14, v4, Lcom/pspdfkit/internal/ho;->a:F

    aput v14, v13, v9

    iget v14, v4, Lcom/pspdfkit/internal/ho;->c:F

    aput v14, v13, v10

    iget v14, v4, Lcom/pspdfkit/internal/ho;->e:F

    aput v14, v13, v11

    iget v14, v4, Lcom/pspdfkit/internal/ho;->g:F

    aput v14, v13, v12

    .line 117
    invoke-static {v13}, Lcom/pspdfkit/internal/di;->a([F)F

    move-result v13

    new-array v6, v6, [F

    iget v14, v4, Lcom/pspdfkit/internal/ho;->b:F

    aput v14, v6, v9

    iget v9, v4, Lcom/pspdfkit/internal/ho;->d:F

    aput v9, v6, v10

    iget v9, v4, Lcom/pspdfkit/internal/ho;->f:F

    aput v9, v6, v11

    iget v4, v4, Lcom/pspdfkit/internal/ho;->h:F

    aput v4, v6, v12

    .line 119
    invoke-static {v6}, Lcom/pspdfkit/internal/di;->b([F)F

    move-result v4

    invoke-direct {v5, v7, v8, v13, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 120
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public isResizable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setRects(Ljava/util/List;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "rects"

    const-string v2, "Annotation rectangles may not be null."

    .line 1
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    const-string v4, "Annotation rectangles may not contain a null element"

    .line 3
    invoke-static {v3, v1, v4}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    goto :goto_0

    .line 5
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    const-string v3, "argumentName"

    .line 6
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    .line 57
    invoke-static {v0, v1, v4}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 59
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    const-string v6, "rect"

    .line 60
    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-static {v5, v6, v4}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 112
    new-instance v6, Lcom/pspdfkit/internal/ho;

    iget v12, v5, Landroid/graphics/RectF;->left:F

    iget v11, v5, Landroid/graphics/RectF;->top:F

    iget v14, v5, Landroid/graphics/RectF;->right:F

    iget v15, v5, Landroid/graphics/RectF;->bottom:F

    move-object v7, v6

    move v8, v12

    move v9, v11

    move v10, v14

    move v13, v15

    invoke-direct/range {v7 .. v15}, Lcom/pspdfkit/internal/ho;-><init>(FFFFFFFF)V

    .line 113
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 114
    :cond_1
    invoke-interface {v2, v1}, Lcom/pspdfkit/internal/pf;->setQuadrilaterals(Ljava/util/List;)V

    return-void
.end method

.class public Lcom/pspdfkit/annotations/PolylineAnnotation;
.super Lcom/pspdfkit/annotations/BaseLineAnnotation;
.source "SourceFile"


# direct methods
.method public constructor <init>(ILjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/BaseLineAnnotation;-><init>(I)V

    const-string p1, "points"

    .line 2
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v0, 0x67

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(ILjava/util/List;Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/pspdfkit/annotations/measurements/Scale;",
            "Lcom/pspdfkit/annotations/measurements/FloatPrecision;",
            ")V"
        }
    .end annotation

    .line 4
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/PolylineAnnotation;-><init>(ILjava/util/List;)V

    .line 6
    invoke-virtual {p0, p3, p4}, Lcom/pspdfkit/annotations/ShapeAnnotation;->a(Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    .line 8
    sget-object p1, Lcom/pspdfkit/annotations/BorderStyle;->SOLID:Lcom/pspdfkit/annotations/BorderStyle;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/BaseLineAnnotation;->setLineStyle(Lcom/pspdfkit/annotations/BorderStyle;)V

    .line 9
    sget-object p1, Lcom/pspdfkit/annotations/LineEndType;->BUTT:Lcom/pspdfkit/annotations/LineEndType;

    sget-object p2, Lcom/pspdfkit/annotations/LineEndType;->OPEN_ARROW:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/annotations/PolylineAnnotation;->setLineEnds(Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/p1;Z)V
    .locals 0

    .line 10
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/BaseLineAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    return-void
.end method


# virtual methods
.method final a()Lcom/pspdfkit/annotations/Annotation;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/PolylineAnnotation;

    new-instance v1, Lcom/pspdfkit/internal/p1;

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/pspdfkit/internal/p1;-><init>(Lcom/pspdfkit/internal/p1;)V

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/annotations/PolylineAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->prepareForCopy()V

    return-object v0
.end method

.method public getLineEnds()Landroidx/core/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/annotations/BaseLineAnnotation;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public getPoints()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/BaseLineAnnotation;->f()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/pspdfkit/annotations/AnnotationType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->POLYLINE:Lcom/pspdfkit/annotations/AnnotationType;

    return-object v0
.end method

.method public setLineEnds(Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V
    .locals 2

    const-string v0, "lineEnd1"

    const-string v1, "Line ends may not be null."

    .line 1
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "lineEnd2"

    .line 2
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 p2, 0x66

    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    return-void
.end method

.method public setPoints(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;)V"
        }
    .end annotation

    const-string v0, "points"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x67

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 55
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    const/4 v0, 0x1

    invoke-interface {p1, v0, v0}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached(ZZ)Z

    return-void
.end method

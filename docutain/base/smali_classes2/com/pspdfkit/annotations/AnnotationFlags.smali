.class public final enum Lcom/pspdfkit/annotations/AnnotationFlags;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/annotations/AnnotationFlags;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum HIDDEN:Lcom/pspdfkit/annotations/AnnotationFlags;

.field public static final enum INVISIBLE:Lcom/pspdfkit/annotations/AnnotationFlags;

.field public static final enum LOCKED:Lcom/pspdfkit/annotations/AnnotationFlags;

.field public static final enum LOCKEDCONTENTS:Lcom/pspdfkit/annotations/AnnotationFlags;

.field public static final enum NOROTATE:Lcom/pspdfkit/annotations/AnnotationFlags;

.field public static final enum NOVIEW:Lcom/pspdfkit/annotations/AnnotationFlags;

.field public static final enum NOZOOM:Lcom/pspdfkit/annotations/AnnotationFlags;

.field public static final enum PRINT:Lcom/pspdfkit/annotations/AnnotationFlags;

.field public static final enum READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

.field public static final enum TOGGLENOVIEW:Lcom/pspdfkit/annotations/AnnotationFlags;

.field private static final synthetic a:[Lcom/pspdfkit/annotations/AnnotationFlags;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/AnnotationFlags;

    const-string v1, "INVISIBLE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/annotations/AnnotationFlags;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/annotations/AnnotationFlags;->INVISIBLE:Lcom/pspdfkit/annotations/AnnotationFlags;

    .line 3
    new-instance v1, Lcom/pspdfkit/annotations/AnnotationFlags;

    const-string v3, "HIDDEN"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/annotations/AnnotationFlags;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/annotations/AnnotationFlags;->HIDDEN:Lcom/pspdfkit/annotations/AnnotationFlags;

    .line 5
    new-instance v3, Lcom/pspdfkit/annotations/AnnotationFlags;

    const-string v5, "PRINT"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/annotations/AnnotationFlags;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/annotations/AnnotationFlags;->PRINT:Lcom/pspdfkit/annotations/AnnotationFlags;

    .line 16
    new-instance v5, Lcom/pspdfkit/annotations/AnnotationFlags;

    const-string v7, "NOZOOM"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/annotations/AnnotationFlags;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/annotations/AnnotationFlags;->NOZOOM:Lcom/pspdfkit/annotations/AnnotationFlags;

    .line 21
    new-instance v7, Lcom/pspdfkit/annotations/AnnotationFlags;

    const-string v9, "NOROTATE"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/annotations/AnnotationFlags;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/annotations/AnnotationFlags;->NOROTATE:Lcom/pspdfkit/annotations/AnnotationFlags;

    .line 23
    new-instance v9, Lcom/pspdfkit/annotations/AnnotationFlags;

    const-string v11, "NOVIEW"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/annotations/AnnotationFlags;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/annotations/AnnotationFlags;->NOVIEW:Lcom/pspdfkit/annotations/AnnotationFlags;

    .line 25
    new-instance v11, Lcom/pspdfkit/annotations/AnnotationFlags;

    const-string v13, "READONLY"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/annotations/AnnotationFlags;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/annotations/AnnotationFlags;->READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

    .line 27
    new-instance v13, Lcom/pspdfkit/annotations/AnnotationFlags;

    const-string v15, "LOCKED"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/annotations/AnnotationFlags;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/annotations/AnnotationFlags;->LOCKED:Lcom/pspdfkit/annotations/AnnotationFlags;

    .line 29
    new-instance v15, Lcom/pspdfkit/annotations/AnnotationFlags;

    const-string v14, "TOGGLENOVIEW"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/annotations/AnnotationFlags;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/annotations/AnnotationFlags;->TOGGLENOVIEW:Lcom/pspdfkit/annotations/AnnotationFlags;

    .line 31
    new-instance v14, Lcom/pspdfkit/annotations/AnnotationFlags;

    const-string v12, "LOCKEDCONTENTS"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/annotations/AnnotationFlags;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/annotations/AnnotationFlags;->LOCKEDCONTENTS:Lcom/pspdfkit/annotations/AnnotationFlags;

    const/16 v12, 0xa

    new-array v12, v12, [Lcom/pspdfkit/annotations/AnnotationFlags;

    aput-object v0, v12, v2

    aput-object v1, v12, v4

    aput-object v3, v12, v6

    aput-object v5, v12, v8

    const/4 v0, 0x4

    aput-object v7, v12, v0

    const/4 v0, 0x5

    aput-object v9, v12, v0

    const/4 v0, 0x6

    aput-object v11, v12, v0

    const/4 v0, 0x7

    aput-object v13, v12, v0

    const/16 v0, 0x8

    aput-object v15, v12, v0

    aput-object v14, v12, v10

    .line 32
    sput-object v12, Lcom/pspdfkit/annotations/AnnotationFlags;->a:[Lcom/pspdfkit/annotations/AnnotationFlags;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/AnnotationFlags;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/AnnotationFlags;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/annotations/AnnotationFlags;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationFlags;->a:[Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {v0}, [Lcom/pspdfkit/annotations/AnnotationFlags;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/annotations/AnnotationFlags;

    return-object v0
.end method

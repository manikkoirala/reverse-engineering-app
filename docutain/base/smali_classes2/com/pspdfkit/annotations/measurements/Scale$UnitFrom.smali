.class public final enum Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/annotations/measurements/Scale;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UnitFrom"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CM:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

.field public static final enum IN:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

.field public static final enum MM:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

.field public static final enum PT:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

.field private static final synthetic b:[Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    const-string v1, "IN"

    const/4 v2, 0x0

    const-string v3, "in"

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->IN:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 3
    new-instance v1, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    const-string v3, "MM"

    const/4 v4, 0x1

    const-string v5, "mm"

    invoke-direct {v1, v3, v4, v5}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->MM:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 5
    new-instance v3, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    const-string v5, "CM"

    const/4 v6, 0x2

    const-string v7, "cm"

    invoke-direct {v3, v5, v6, v7}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->CM:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 7
    new-instance v5, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    const-string v7, "PT"

    const/4 v8, 0x3

    const-string v9, "pt"

    invoke-direct {v5, v7, v8, v9}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->PT:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    const/4 v7, 0x4

    new-array v7, v7, [Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    .line 8
    sput-object v7, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->b:[Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    iput-object p3, p0, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->a:Ljava/lang/String;

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;
    .locals 5

    .line 1
    invoke-static {}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->values()[Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 2
    iget-object v4, v3, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->b:[Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    invoke-virtual {v0}, [Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->a:Ljava/lang/String;

    return-object v0
.end method

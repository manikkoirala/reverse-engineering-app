.class public final Lcom/pspdfkit/annotations/measurements/Scale;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;,
        Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;
    }
.end annotation


# instance fields
.field public final unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

.field public final unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

.field public final valueFrom:F

.field public final valueTo:F


# direct methods
.method public constructor <init>(FLcom/pspdfkit/annotations/measurements/Scale$UnitFrom;FLcom/pspdfkit/annotations/measurements/Scale$UnitTo;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "unitFrom"

    .line 2
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unitTo"

    .line 3
    invoke-static {p4, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x3727c5ac    # 1.0E-5f

    .line 4
    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iput p1, p0, Lcom/pspdfkit/annotations/measurements/Scale;->valueFrom:F

    .line 5
    iput-object p2, p0, Lcom/pspdfkit/annotations/measurements/Scale;->unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 6
    invoke-static {v0, p3}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iput p1, p0, Lcom/pspdfkit/annotations/measurements/Scale;->valueTo:F

    .line 7
    iput-object p4, p0, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    return-void
.end method

.method public static defaultScale()Lcom/pspdfkit/annotations/measurements/Scale;
    .locals 4

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/measurements/Scale;

    sget-object v1, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->IN:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    sget-object v2, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->IN:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v1, v3, v2}, Lcom/pspdfkit/annotations/measurements/Scale;-><init>(FLcom/pspdfkit/annotations/measurements/Scale$UnitFrom;FLcom/pspdfkit/annotations/measurements/Scale$UnitTo;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/annotations/measurements/Scale;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 5
    :cond_0
    check-cast p1, Lcom/pspdfkit/annotations/measurements/Scale;

    .line 6
    iget v0, p0, Lcom/pspdfkit/annotations/measurements/Scale;->valueFrom:F

    iget v2, p1, Lcom/pspdfkit/annotations/measurements/Scale;->valueFrom:F

    invoke-static {v0, v2}, Lcom/pspdfkit/internal/di;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/pspdfkit/annotations/measurements/Scale;->valueTo:F

    iget v2, p1, Lcom/pspdfkit/annotations/measurements/Scale;->valueTo:F

    .line 7
    invoke-static {v0, v2}, Lcom/pspdfkit/internal/di;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/annotations/measurements/Scale;->unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    iget-object v2, p1, Lcom/pspdfkit/annotations/measurements/Scale;->unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 8
    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    iget-object p1, p1, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 9
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/annotations/measurements/Scale;->valueFrom:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    .line 2
    iget v1, p0, Lcom/pspdfkit/annotations/measurements/Scale;->valueTo:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/annotations/measurements/Scale;->unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

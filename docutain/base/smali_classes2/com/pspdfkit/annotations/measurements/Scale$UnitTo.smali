.class public final enum Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/annotations/measurements/Scale;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UnitTo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CM:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

.field public static final enum FT:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

.field public static final enum IN:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

.field public static final enum KM:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

.field public static final enum M:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

.field public static final enum MI:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

.field public static final enum MM:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

.field public static final enum PT:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

.field public static final enum YD:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

.field private static final synthetic b:[Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const-string v1, "IN"

    const/4 v2, 0x0

    const-string v3, "in"

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->IN:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 3
    new-instance v1, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const-string v3, "MM"

    const/4 v4, 0x1

    const-string v5, "mm"

    invoke-direct {v1, v3, v4, v5}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->MM:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 5
    new-instance v3, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const-string v5, "CM"

    const/4 v6, 0x2

    const-string v7, "cm"

    invoke-direct {v3, v5, v6, v7}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->CM:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 7
    new-instance v5, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const-string v7, "PT"

    const/4 v8, 0x3

    const-string v9, "pt"

    invoke-direct {v5, v7, v8, v9}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->PT:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 9
    new-instance v7, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const-string v9, "FT"

    const/4 v10, 0x4

    const-string v11, "ft"

    invoke-direct {v7, v9, v10, v11}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->FT:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 11
    new-instance v9, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const-string v11, "M"

    const/4 v12, 0x5

    const-string v13, "m"

    invoke-direct {v9, v11, v12, v13}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->M:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 13
    new-instance v11, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const-string v13, "YD"

    const/4 v14, 0x6

    const-string v15, "yd"

    invoke-direct {v11, v13, v14, v15}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v11, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->YD:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 15
    new-instance v13, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const-string v15, "KM"

    const/4 v14, 0x7

    const-string v12, "km"

    invoke-direct {v13, v15, v14, v12}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v13, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->KM:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 17
    new-instance v12, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const-string v15, "MI"

    const/16 v14, 0x8

    const-string v10, "mi"

    invoke-direct {v12, v15, v14, v10}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v12, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->MI:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const/16 v10, 0x9

    new-array v10, v10, [Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    aput-object v0, v10, v2

    aput-object v1, v10, v4

    aput-object v3, v10, v6

    aput-object v5, v10, v8

    const/4 v0, 0x4

    aput-object v7, v10, v0

    const/4 v0, 0x5

    aput-object v9, v10, v0

    const/4 v0, 0x6

    aput-object v11, v10, v0

    const/4 v0, 0x7

    aput-object v13, v10, v0

    aput-object v12, v10, v14

    .line 18
    sput-object v10, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->b:[Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    iput-object p3, p0, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->a:Ljava/lang/String;

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;
    .locals 5

    .line 1
    invoke-static {}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->values()[Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 2
    iget-object v4, v3, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->b:[Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    invoke-virtual {v0}, [Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->a:Ljava/lang/String;

    return-object v0
.end method

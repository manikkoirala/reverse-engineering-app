.class public final enum Lcom/pspdfkit/annotations/measurements/MeasurementMode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/annotations/measurements/MeasurementMode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AREA:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

.field public static final enum DISTANCE:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

.field public static final enum PERIMETER:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

.field private static final synthetic a:[Lcom/pspdfkit/annotations/measurements/MeasurementMode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    const-string v1, "DISTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/annotations/measurements/MeasurementMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->DISTANCE:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    .line 4
    new-instance v1, Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    const-string v3, "PERIMETER"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/annotations/measurements/MeasurementMode;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->PERIMETER:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    .line 7
    new-instance v3, Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    const-string v5, "AREA"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/annotations/measurements/MeasurementMode;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->AREA:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    const/4 v5, 0x3

    new-array v5, v5, [Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    aput-object v0, v5, v2

    aput-object v1, v5, v4

    aput-object v3, v5, v6

    .line 8
    sput-object v5, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->a:[Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/measurements/MeasurementMode;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/annotations/measurements/MeasurementMode;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->a:[Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    invoke-virtual {v0}, [Lcom/pspdfkit/annotations/measurements/MeasurementMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    return-object v0
.end method

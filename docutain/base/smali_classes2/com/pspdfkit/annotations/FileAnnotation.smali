.class public Lcom/pspdfkit/annotations/FileAnnotation;
.super Lcom/pspdfkit/annotations/Annotation;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/annotations/FileAnnotation$IconName;
    }
.end annotation


# static fields
.field public static final GRAPH:Ljava/lang/String; = "Graph"

.field public static final PAPERCLIP:Ljava/lang/String; = "Paperclip"

.field public static final PUSH_PIN:Ljava/lang/String; = "PushPin"

.field public static final TAG:Ljava/lang/String; = "Tag"


# instance fields
.field private q:Lcom/pspdfkit/internal/x0;


# direct methods
.method public constructor <init>(ILandroid/graphics/RectF;Lcom/pspdfkit/document/files/EmbeddedFileSource;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/Annotation;-><init>(I)V

    const-string p1, "boundingBox"

    .line 2
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "embeddedFileSource"

    .line 3
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-virtual {p0, p2}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    const-string p1, "PushPin"

    .line 6
    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/FileAnnotation;->setIconName(Ljava/lang/String;)V

    .line 7
    invoke-virtual {p3}, Lcom/pspdfkit/document/files/EmbeddedFileSource;->getFileDescription()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/Annotation;->setContents(Ljava/lang/String;)V

    .line 9
    new-instance p1, Lcom/pspdfkit/internal/x0;

    invoke-direct {p1, p0, p3}, Lcom/pspdfkit/internal/x0;-><init>(Lcom/pspdfkit/annotations/FileAnnotation;Lcom/pspdfkit/document/files/EmbeddedFileSource;)V

    iput-object p1, p0, Lcom/pspdfkit/annotations/FileAnnotation;->q:Lcom/pspdfkit/internal/x0;

    .line 10
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/annotations/FileAnnotation;->q:Lcom/pspdfkit/internal/x0;

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/p1;ZLjava/lang/String;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/Annotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    if-eqz p3, :cond_0

    .line 13
    new-instance p1, Lcom/pspdfkit/internal/x0;

    invoke-direct {p1, p0, p3}, Lcom/pspdfkit/internal/x0;-><init>(Lcom/pspdfkit/annotations/FileAnnotation;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/annotations/FileAnnotation;->q:Lcom/pspdfkit/internal/x0;

    .line 14
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/annotations/FileAnnotation;->q:Lcom/pspdfkit/internal/x0;

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getFile()Lcom/pspdfkit/document/files/EmbeddedFile;
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/FileAnnotation;->q:Lcom/pspdfkit/internal/x0;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/x0;->i()Lcom/pspdfkit/internal/sa;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getIconName()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0xfa0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "PushPin"

    :cond_0
    return-object v0
.end method

.method public getType()Lcom/pspdfkit/annotations/AnnotationType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->FILE:Lcom/pspdfkit/annotations/AnnotationType;

    return-object v0
.end method

.method public isLocked()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isResizable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setFileSource(Lcom/pspdfkit/document/files/EmbeddedFileSource;)V
    .locals 2

    const-string v0, "fileSource"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    monitor-enter p0

    .line 55
    :try_start_0
    new-instance v0, Lcom/pspdfkit/internal/x0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/x0;-><init>(Lcom/pspdfkit/annotations/FileAnnotation;Lcom/pspdfkit/document/files/EmbeddedFileSource;)V

    iput-object v0, p0, Lcom/pspdfkit/annotations/FileAnnotation;->q:Lcom/pspdfkit/internal/x0;

    .line 56
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/annotations/FileAnnotation;->q:Lcom/pspdfkit/internal/x0;

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    .line 62
    invoke-virtual {p1}, Lcom/pspdfkit/document/files/EmbeddedFileSource;->getFileDescription()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/Annotation;->setContents(Ljava/lang/String;)V

    .line 63
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public setIconName(Ljava/lang/String;)V
    .locals 2

    const-string v0, "File annotation icon name must not be null."

    .line 1
    invoke-static {p1, p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0xfa0

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    return-void
.end method

.method public updateTransformationProperties(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 0

    return-void
.end method

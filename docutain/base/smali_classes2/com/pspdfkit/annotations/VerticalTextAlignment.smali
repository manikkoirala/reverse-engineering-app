.class public final enum Lcom/pspdfkit/annotations/VerticalTextAlignment;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/annotations/VerticalTextAlignment;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BOTTOM:Lcom/pspdfkit/annotations/VerticalTextAlignment;

.field public static final enum CENTER:Lcom/pspdfkit/annotations/VerticalTextAlignment;

.field public static final enum TOP:Lcom/pspdfkit/annotations/VerticalTextAlignment;

.field private static final synthetic a:[Lcom/pspdfkit/annotations/VerticalTextAlignment;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/VerticalTextAlignment;

    const-string v1, "TOP"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/annotations/VerticalTextAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/annotations/VerticalTextAlignment;->TOP:Lcom/pspdfkit/annotations/VerticalTextAlignment;

    .line 3
    new-instance v1, Lcom/pspdfkit/annotations/VerticalTextAlignment;

    const-string v3, "CENTER"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/annotations/VerticalTextAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/annotations/VerticalTextAlignment;->CENTER:Lcom/pspdfkit/annotations/VerticalTextAlignment;

    .line 5
    new-instance v3, Lcom/pspdfkit/annotations/VerticalTextAlignment;

    const-string v5, "BOTTOM"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/annotations/VerticalTextAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/annotations/VerticalTextAlignment;->BOTTOM:Lcom/pspdfkit/annotations/VerticalTextAlignment;

    const/4 v5, 0x3

    new-array v5, v5, [Lcom/pspdfkit/annotations/VerticalTextAlignment;

    aput-object v0, v5, v2

    aput-object v1, v5, v4

    aput-object v3, v5, v6

    .line 6
    sput-object v5, Lcom/pspdfkit/annotations/VerticalTextAlignment;->a:[Lcom/pspdfkit/annotations/VerticalTextAlignment;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/VerticalTextAlignment;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/VerticalTextAlignment;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/VerticalTextAlignment;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/annotations/VerticalTextAlignment;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/VerticalTextAlignment;->a:[Lcom/pspdfkit/annotations/VerticalTextAlignment;

    invoke-virtual {v0}, [Lcom/pspdfkit/annotations/VerticalTextAlignment;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/annotations/VerticalTextAlignment;

    return-object v0
.end method

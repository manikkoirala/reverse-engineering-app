.class public Lcom/pspdfkit/annotations/LineAnnotation;
.super Lcom/pspdfkit/annotations/BaseLineAnnotation;
.source "SourceFile"


# direct methods
.method public constructor <init>(ILandroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/BaseLineAnnotation;-><init>(I)V

    const-string p1, "point1"

    .line 2
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "point2"

    .line 3
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-static {p2, p3}, Lcom/pspdfkit/annotations/LineAnnotation;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)Ljava/util/ArrayList;

    move-result-object p2

    const/16 p3, 0x64

    invoke-virtual {p1, p3, p2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(ILandroid/graphics/PointF;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 0

    .line 5
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/annotations/LineAnnotation;-><init>(ILandroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 7
    invoke-virtual {p0, p4, p5}, Lcom/pspdfkit/annotations/ShapeAnnotation;->a(Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    .line 9
    sget-object p1, Lcom/pspdfkit/annotations/BorderStyle;->SOLID:Lcom/pspdfkit/annotations/BorderStyle;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/BaseLineAnnotation;->setLineStyle(Lcom/pspdfkit/annotations/BorderStyle;)V

    .line 10
    sget-object p1, Lcom/pspdfkit/annotations/LineEndType;->BUTT:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {p0, p1, p1}, Lcom/pspdfkit/annotations/LineAnnotation;->setLineEnds(Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/p1;Z)V
    .locals 0

    .line 11
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/BaseLineAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    return-void
.end method

.method static a(Landroid/graphics/PointF;Landroid/graphics/PointF;)Ljava/util/ArrayList;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 3
    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method


# virtual methods
.method final a()Lcom/pspdfkit/annotations/Annotation;
    .locals 3

    .line 6
    new-instance v0, Lcom/pspdfkit/annotations/LineAnnotation;

    new-instance v1, Lcom/pspdfkit/internal/p1;

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/pspdfkit/internal/p1;-><init>(Lcom/pspdfkit/internal/p1;)V

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/annotations/LineAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->prepareForCopy()V

    return-object v0
.end method

.method final f()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/LineAnnotation;->getPoints()Landroidx/core/util/Pair;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/PointF;

    .line 2
    iget-object v2, v0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/graphics/PointF;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v0, v0, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/PointF;

    const/4 v2, 0x1

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLineEnds()Landroidx/core/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/annotations/BaseLineAnnotation;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public getPoints()Landroidx/core/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/core/util/Pair<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const-class v1, Ljava/util/ArrayList;

    const/16 v2, 0x64

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 3
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_1

    new-instance v0, Landroidx/core/util/Pair;

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    invoke-direct {v0, v1, v2}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 5
    :cond_1
    new-instance v2, Landroidx/core/util/Pair;

    new-instance v3, Landroid/graphics/PointF;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-direct {v3, v4, v1}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v1, Landroid/graphics/PointF;

    const/4 v4, 0x1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v5, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-direct {v2, v3, v1}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 6
    :cond_2
    :goto_0
    new-instance v0, Landroidx/core/util/Pair;

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    invoke-direct {v0, v1, v2}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public getType()Lcom/pspdfkit/annotations/AnnotationType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->LINE:Lcom/pspdfkit/annotations/AnnotationType;

    return-object v0
.end method

.method public setLineEnds(Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V
    .locals 2

    const-string v0, "lineEnd1"

    const-string v1, "Line ends may not be null."

    .line 1
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "lineEnd2"

    .line 2
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 p2, 0x66

    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    return-void
.end method

.method public setPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 2

    const-string v0, "point1"

    const-string v1, "Points may not be null."

    .line 1
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "point2"

    .line 2
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-static {p1, p2}, Lcom/pspdfkit/annotations/LineAnnotation;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)Ljava/util/ArrayList;

    move-result-object p1

    const/16 p2, 0x64

    invoke-virtual {v0, p2, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    const/4 p2, 0x1

    invoke-interface {p1, p2, p2}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached(ZZ)Z

    return-void
.end method

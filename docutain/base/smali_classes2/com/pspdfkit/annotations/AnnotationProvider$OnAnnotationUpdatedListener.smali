.class public interface abstract Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/annotations/AnnotationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnAnnotationUpdatedListener"
.end annotation


# virtual methods
.method public abstract onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
.end method

.method public abstract onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
.end method

.method public abstract onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
.end method

.method public abstract onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation
.end method

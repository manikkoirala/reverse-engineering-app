.class public abstract Lcom/pspdfkit/annotations/MediaAnnotation;
.super Lcom/pspdfkit/annotations/AssetAnnotation;
.source "SourceFile"


# static fields
.field private static final r:Lcom/pspdfkit/annotations/MediaWindowType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/MediaWindowType;->USE_ANNOTATION_RECTANGLE:Lcom/pspdfkit/annotations/MediaWindowType;

    sput-object v0, Lcom/pspdfkit/annotations/MediaAnnotation;->r:Lcom/pspdfkit/annotations/MediaWindowType;

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/p1;ZLjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/annotations/AssetAnnotation;-><init>(Lcom/pspdfkit/internal/p1;ZLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getMediaOptions()Ljava/util/EnumSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/actions/MediaOptions;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const-class v1, Ljava/util/EnumSet;

    const/16 v2, 0x1b59

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/EnumSet;

    if-nez v0, :cond_0

    .line 2
    const-class v0, Lcom/pspdfkit/annotations/actions/MediaOptions;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getWindowMediaType()Lcom/pspdfkit/annotations/MediaWindowType;
    .locals 4

    .line 1
    invoke-static {}, Lcom/pspdfkit/annotations/MediaWindowType;->values()[Lcom/pspdfkit/annotations/MediaWindowType;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    sget-object v2, Lcom/pspdfkit/annotations/MediaAnnotation;->r:Lcom/pspdfkit/annotations/MediaWindowType;

    .line 3
    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    const/16 v3, 0x1b58

    .line 4
    invoke-virtual {v1, v3, v2}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public setMediaOptions(Ljava/util/EnumSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/actions/MediaOptions;",
            ">;)V"
        }
    .end annotation

    const-string v0, "mediaOptions"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    const/16 v1, 0x1b59

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public setWindowMediaType(Lcom/pspdfkit/annotations/MediaWindowType;)V
    .locals 2

    const-string v0, "mediaWindowType"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/16 v1, 0x1b58

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    return-void
.end method

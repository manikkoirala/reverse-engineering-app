.class public Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final DURATION_UNKNOWN:I = -0x1


# instance fields
.field private final a:Lcom/pspdfkit/document/providers/DataProvider;

.field private final b:Lcom/pspdfkit/annotations/sound/AudioEncoding;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/document/providers/DataProvider;Lcom/pspdfkit/annotations/sound/AudioEncoding;IIILjava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "audioDataProvider"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioEncoding"

    .line 3
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-lez p3, :cond_2

    const/16 v0, 0x8

    if-lt p4, v0, :cond_1

    const/4 v0, 0x1

    if-lt p5, v0, :cond_0

    .line 14
    iput-object p1, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->a:Lcom/pspdfkit/document/providers/DataProvider;

    .line 15
    iput-object p2, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->b:Lcom/pspdfkit/annotations/sound/AudioEncoding;

    .line 16
    iput p3, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->c:I

    .line 17
    iput p4, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->d:I

    .line 18
    iput p5, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->e:I

    .line 19
    iput-object p6, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->f:Ljava/lang/String;

    return-void

    .line 20
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "Number of channels must be at least 1, was: "

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 21
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "Sample size must be at least 8 bits, was: "

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 22
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p4, "Sample rate must be larger than 0, was: "

    invoke-direct {p2, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>([BLcom/pspdfkit/annotations/sound/AudioEncoding;IIILjava/lang/String;)V
    .locals 7

    .line 23
    new-instance v1, Lcom/pspdfkit/internal/bj;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/bj;-><init>([B)V

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;-><init>(Lcom/pspdfkit/document/providers/DataProvider;Lcom/pspdfkit/annotations/sound/AudioEncoding;IIILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getAudioEncoding()Lcom/pspdfkit/annotations/sound/AudioEncoding;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->b:Lcom/pspdfkit/annotations/sound/AudioEncoding;

    return-object v0
.end method

.method public getChannels()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->e:I

    return v0
.end method

.method public getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->a:Lcom/pspdfkit/document/providers/DataProvider;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()J
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->a:Lcom/pspdfkit/document/providers/DataProvider;

    invoke-interface {v0}, Lcom/pspdfkit/document/providers/DataProvider;->getSize()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-wide v2

    .line 5
    :cond_0
    iget v2, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->c:I

    int-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    iget v3, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->e:I

    int-to-float v3, v3

    mul-float v2, v2, v3

    iget v3, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->d:I

    int-to-float v3, v3

    const/high16 v4, 0x41000000    # 8.0f

    div-float/2addr v3, v4

    mul-float v3, v3, v2

    long-to-float v0, v0

    div-float/2addr v0, v3

    float-to-long v0, v0

    return-wide v0
.end method

.method public getSampleRate()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->c:I

    return v0
.end method

.method public getSampleSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->d:I

    return v0
.end method

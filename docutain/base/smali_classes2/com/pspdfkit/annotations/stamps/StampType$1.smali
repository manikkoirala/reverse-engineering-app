.class Lcom/pspdfkit/annotations/stamps/StampType$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/annotations/stamps/StampType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/pspdfkit/annotations/stamps/StampType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/annotations/stamps/StampType;
    .locals 7

    .line 2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    .line 3
    :goto_0
    sget-object v0, Lcom/pspdfkit/annotations/stamps/StampType;->APPROVED:Lcom/pspdfkit/annotations/stamps/StampType;

    const-string v0, "name"

    const-string v1, "argumentName"

    .line 4
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 55
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 56
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeStampAnnotationHelper;->create()Lcom/pspdfkit/internal/jni/NativeStampAnnotationHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeStampAnnotationHelper;->getStampType(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeStampType;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v3, "nativeStampType"

    .line 57
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-static {v0, v3, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 109
    invoke-static {}, Lcom/pspdfkit/annotations/stamps/StampType;->-$$Nest$sfgetd()[Lcom/pspdfkit/annotations/stamps/StampType;

    move-result-object v1

    array-length v3, v1

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v3, :cond_2

    aget-object v5, v1, v4

    .line 110
    invoke-virtual {v5}, Lcom/pspdfkit/annotations/stamps/StampType;->a()Lcom/pspdfkit/internal/jni/NativeStampType;

    move-result-object v6

    if-ne v6, v0, :cond_1

    move-object v2, v5

    goto :goto_2

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    if-eqz v2, :cond_3

    goto :goto_3

    .line 111
    :cond_3
    new-instance v2, Lcom/pspdfkit/annotations/stamps/StampType;

    invoke-direct {v2, p1}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Ljava/lang/String;)V

    :goto_3
    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/stamps/StampType$1;->createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/annotations/stamps/StampType;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/pspdfkit/annotations/stamps/StampType;
    .locals 0

    .line 2
    new-array p1, p1, [Lcom/pspdfkit/annotations/stamps/StampType;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/stamps/StampType$1;->newArray(I)[Lcom/pspdfkit/annotations/stamps/StampType;

    move-result-object p1

    return-object p1
.end method

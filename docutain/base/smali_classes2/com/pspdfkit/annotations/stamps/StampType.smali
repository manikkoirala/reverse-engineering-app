.class public final Lcom/pspdfkit/annotations/stamps/StampType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lcom/google/errorprone/annotations/Immutable;
.end annotation


# static fields
.field public static final ACCEPTED:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final APPROVED:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final AS_IS:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final COMPLETED:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final CONFIDENTIAL:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/annotations/stamps/StampType;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEPARTMENTAL:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final DRAFT:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final EXPERIMENTAL:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final EXPIRED:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final FINAL:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final FOR_COMMENT:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final FOR_PUBLIC_RELEASE:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final INFORMATION_ONLY:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final INITIAL_HERE:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final NOT_APPROVED:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final NOT_FOR_PUBLIC_RELEASE:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final PRELIMINARY_RESULTS:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final REJECTED:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final REVISED:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final SIGN_HERE:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final SOLD:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final TOP_SECRET:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final VOID:Lcom/pspdfkit/annotations/stamps/StampType;

.field public static final WITNESS:Lcom/pspdfkit/annotations/stamps/StampType;

.field private static final d:[Lcom/pspdfkit/annotations/stamps/StampType;


# instance fields
.field private a:Lcom/pspdfkit/internal/jni/NativeStampType;

.field private b:Ljava/lang/String;

.field private c:Z


# direct methods
.method static bridge synthetic -$$Nest$sfgetd()[Lcom/pspdfkit/annotations/stamps/StampType;
    .locals 1

    sget-object v0, Lcom/pspdfkit/annotations/stamps/StampType;->d:[Lcom/pspdfkit/annotations/stamps/StampType;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 26

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeStampType;->APPROVED:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v0, v1}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v0, Lcom/pspdfkit/annotations/stamps/StampType;->APPROVED:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 2
    new-instance v1, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v2, Lcom/pspdfkit/internal/jni/NativeStampType;->EXPERIMENTAL:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v1, v2}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v1, Lcom/pspdfkit/annotations/stamps/StampType;->EXPERIMENTAL:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 3
    new-instance v2, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v3, Lcom/pspdfkit/internal/jni/NativeStampType;->NOTAPPROVED:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v2, v3}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v2, Lcom/pspdfkit/annotations/stamps/StampType;->NOT_APPROVED:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 4
    new-instance v3, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v4, Lcom/pspdfkit/internal/jni/NativeStampType;->ASIS:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v3, v4}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v3, Lcom/pspdfkit/annotations/stamps/StampType;->AS_IS:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 5
    new-instance v4, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v5, Lcom/pspdfkit/internal/jni/NativeStampType;->EXPIRED:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v4, v5}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v4, Lcom/pspdfkit/annotations/stamps/StampType;->EXPIRED:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 6
    new-instance v5, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v6, Lcom/pspdfkit/internal/jni/NativeStampType;->NOTFORPUBLICRELEASE:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v5, v6}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v5, Lcom/pspdfkit/annotations/stamps/StampType;->NOT_FOR_PUBLIC_RELEASE:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 7
    new-instance v6, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v7, Lcom/pspdfkit/internal/jni/NativeStampType;->CONFIDENTIAL:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v6, v7}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v6, Lcom/pspdfkit/annotations/stamps/StampType;->CONFIDENTIAL:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 8
    new-instance v7, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v8, Lcom/pspdfkit/internal/jni/NativeStampType;->FINAL:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v7, v8}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v7, Lcom/pspdfkit/annotations/stamps/StampType;->FINAL:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 9
    new-instance v8, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v9, Lcom/pspdfkit/internal/jni/NativeStampType;->SOLD:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v8, v9}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v8, Lcom/pspdfkit/annotations/stamps/StampType;->SOLD:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 10
    new-instance v9, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v10, Lcom/pspdfkit/internal/jni/NativeStampType;->DEPARTMENTAL:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v9, v10}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v9, Lcom/pspdfkit/annotations/stamps/StampType;->DEPARTMENTAL:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 11
    new-instance v10, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v11, Lcom/pspdfkit/internal/jni/NativeStampType;->FORCOMMENT:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v10, v11}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v10, Lcom/pspdfkit/annotations/stamps/StampType;->FOR_COMMENT:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 12
    new-instance v11, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v12, Lcom/pspdfkit/internal/jni/NativeStampType;->TOPSECRET:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v11, v12}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v11, Lcom/pspdfkit/annotations/stamps/StampType;->TOP_SECRET:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 13
    new-instance v12, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v13, Lcom/pspdfkit/internal/jni/NativeStampType;->DRAFT:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v12, v13}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v12, Lcom/pspdfkit/annotations/stamps/StampType;->DRAFT:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 14
    new-instance v13, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v14, Lcom/pspdfkit/internal/jni/NativeStampType;->FORPUBLICRELEASE:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v13, v14}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v13, Lcom/pspdfkit/annotations/stamps/StampType;->FOR_PUBLIC_RELEASE:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 17
    new-instance v14, Lcom/pspdfkit/annotations/stamps/StampType;

    sget-object v15, Lcom/pspdfkit/internal/jni/NativeStampType;->COMPLETED:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v14, v15}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->COMPLETED:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 18
    new-instance v15, Lcom/pspdfkit/annotations/stamps/StampType;

    move-object/from16 v16, v14

    sget-object v14, Lcom/pspdfkit/internal/jni/NativeStampType;->VOID:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v15, v14}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v15, Lcom/pspdfkit/annotations/stamps/StampType;->VOID:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 19
    new-instance v14, Lcom/pspdfkit/annotations/stamps/StampType;

    move-object/from16 v17, v15

    sget-object v15, Lcom/pspdfkit/internal/jni/NativeStampType;->PRELIMINARYRESULTS:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v14, v15}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->PRELIMINARY_RESULTS:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 20
    new-instance v15, Lcom/pspdfkit/annotations/stamps/StampType;

    move-object/from16 v18, v14

    sget-object v14, Lcom/pspdfkit/internal/jni/NativeStampType;->INFORMATIONONLY:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v15, v14}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v15, Lcom/pspdfkit/annotations/stamps/StampType;->INFORMATION_ONLY:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 21
    new-instance v14, Lcom/pspdfkit/annotations/stamps/StampType;

    move-object/from16 v19, v15

    sget-object v15, Lcom/pspdfkit/internal/jni/NativeStampType;->REVISED:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v14, v15}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->REVISED:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 24
    new-instance v15, Lcom/pspdfkit/annotations/stamps/StampType;

    move-object/from16 v20, v14

    sget-object v14, Lcom/pspdfkit/internal/jni/NativeStampType;->ACCEPTED:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v15, v14}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v15, Lcom/pspdfkit/annotations/stamps/StampType;->ACCEPTED:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 27
    new-instance v14, Lcom/pspdfkit/annotations/stamps/StampType;

    move-object/from16 v21, v15

    sget-object v15, Lcom/pspdfkit/internal/jni/NativeStampType;->REJECTED:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v14, v15}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->REJECTED:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 30
    new-instance v15, Lcom/pspdfkit/annotations/stamps/StampType;

    move-object/from16 v22, v14

    sget-object v14, Lcom/pspdfkit/internal/jni/NativeStampType;->INITIALHERE:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v15, v14}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v15, Lcom/pspdfkit/annotations/stamps/StampType;->INITIAL_HERE:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 31
    new-instance v14, Lcom/pspdfkit/annotations/stamps/StampType;

    move-object/from16 v23, v15

    sget-object v15, Lcom/pspdfkit/internal/jni/NativeStampType;->SIGNHERE:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v14, v15}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->SIGN_HERE:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 32
    new-instance v15, Lcom/pspdfkit/annotations/stamps/StampType;

    move-object/from16 v24, v14

    sget-object v14, Lcom/pspdfkit/internal/jni/NativeStampType;->WITNESS:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-direct {v15, v14}, Lcom/pspdfkit/annotations/stamps/StampType;-><init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V

    sput-object v15, Lcom/pspdfkit/annotations/stamps/StampType;->WITNESS:Lcom/pspdfkit/annotations/stamps/StampType;

    const/16 v14, 0x18

    new-array v14, v14, [Lcom/pspdfkit/annotations/stamps/StampType;

    const/16 v25, 0x0

    aput-object v0, v14, v25

    const/4 v0, 0x1

    aput-object v1, v14, v0

    const/4 v0, 0x2

    aput-object v2, v14, v0

    const/4 v0, 0x3

    aput-object v3, v14, v0

    const/4 v0, 0x4

    aput-object v4, v14, v0

    const/4 v0, 0x5

    aput-object v5, v14, v0

    const/4 v0, 0x6

    aput-object v6, v14, v0

    const/4 v0, 0x7

    aput-object v7, v14, v0

    const/16 v0, 0x8

    aput-object v8, v14, v0

    const/16 v0, 0x9

    aput-object v9, v14, v0

    const/16 v0, 0xa

    aput-object v10, v14, v0

    const/16 v0, 0xb

    aput-object v11, v14, v0

    const/16 v0, 0xc

    aput-object v12, v14, v0

    const/16 v0, 0xd

    aput-object v13, v14, v0

    const/16 v0, 0xe

    aput-object v16, v14, v0

    const/16 v0, 0xf

    aput-object v17, v14, v0

    const/16 v0, 0x10

    aput-object v18, v14, v0

    const/16 v0, 0x11

    aput-object v19, v14, v0

    const/16 v0, 0x12

    aput-object v20, v14, v0

    const/16 v0, 0x13

    aput-object v21, v14, v0

    const/16 v0, 0x14

    aput-object v22, v14, v0

    const/16 v0, 0x15

    aput-object v23, v14, v0

    const/16 v0, 0x16

    aput-object v24, v14, v0

    const/16 v0, 0x17

    aput-object v15, v14, v0

    .line 34
    sput-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->d:[Lcom/pspdfkit/annotations/stamps/StampType;

    .line 166
    new-instance v0, Lcom/pspdfkit/annotations/stamps/StampType$1;

    invoke-direct {v0}, Lcom/pspdfkit/annotations/stamps/StampType$1;-><init>()V

    sput-object v0, Lcom/pspdfkit/annotations/stamps/StampType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/pspdfkit/internal/jni/NativeStampType;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/annotations/stamps/StampType;->c:Z

    const-string v0, "nativeType"

    .line 6
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    iput-object p1, p0, Lcom/pspdfkit/annotations/stamps/StampType;->a:Lcom/pspdfkit/internal/jni/NativeStampType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 9
    iput-boolean v0, p0, Lcom/pspdfkit/annotations/stamps/StampType;->c:Z

    const-string v0, "name"

    .line 27
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iput-object p1, p0, Lcom/pspdfkit/annotations/stamps/StampType;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final a()Lcom/pspdfkit/internal/jni/NativeStampType;
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/stamps/StampType;->a:Lcom/pspdfkit/internal/jni/NativeStampType;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/annotations/stamps/StampType;->c:Z

    if-nez v0, :cond_0

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeStampAnnotationHelper;->create()Lcom/pspdfkit/internal/jni/NativeStampAnnotationHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/annotations/stamps/StampType;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeStampAnnotationHelper;->getStampType(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeStampType;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/annotations/stamps/StampType;->a:Lcom/pspdfkit/internal/jni/NativeStampType;

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/annotations/stamps/StampType;->c:Z

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/stamps/StampType;->a:Lcom/pspdfkit/internal/jni/NativeStampType;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 7
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 1
    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/annotations/stamps/StampType;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 2
    :cond_1
    check-cast p1, Lcom/pspdfkit/annotations/stamps/StampType;

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/stamps/StampType;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampType;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/stamps/StampType;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/annotations/stamps/StampType;->c:Z

    if-nez v0, :cond_0

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeStampAnnotationHelper;->create()Lcom/pspdfkit/internal/jni/NativeStampAnnotationHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/annotations/stamps/StampType;->a:Lcom/pspdfkit/internal/jni/NativeStampType;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeStampAnnotationHelper;->getPreferredIconName(Lcom/pspdfkit/internal/jni/NativeStampType;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/annotations/stamps/StampType;->b:Ljava/lang/String;

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/annotations/stamps/StampType;->c:Z

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/stamps/StampType;->b:Ljava/lang/String;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 8
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/stamps/StampType;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isStandard()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/stamps/StampType;->a()Lcom/pspdfkit/internal/jni/NativeStampType;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "StampType{name=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/stamps/StampType;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/stamps/StampType;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

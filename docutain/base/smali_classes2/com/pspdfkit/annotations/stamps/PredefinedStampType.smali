.class public final enum Lcom/pspdfkit/annotations/stamps/PredefinedStampType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/annotations/stamps/PredefinedStampType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACCEPTED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum APPROVED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum AS_IS:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum COMPLETED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum CONFIDENTIAL:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum CUSTOM:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum DEPARTMENTAL:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum DRAFT:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum EXPERIMENTAL:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum EXPIRED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum FINAL:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum FOR_COMMENT:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum FOR_PUBLIC_RELEASE:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum INFORMATION_ONLY:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum INITIAL_HERE:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum NOT_APPROVED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum NOT_FOR_PUBLIC_RELEASE:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum PRELIMINARY_RESULTS:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum REJECTED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum REVISED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum SIGN_HERE:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum SOLD:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum TOP_SECRET:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum VOID:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field public static final enum WITNESS:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

.field private static final synthetic c:[Lcom/pspdfkit/annotations/stamps/PredefinedStampType;


# instance fields
.field private final a:Lcom/pspdfkit/annotations/stamps/StampType;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 29

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v1, Lcom/pspdfkit/annotations/stamps/StampType;->APPROVED:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v2, Lcom/pspdfkit/R$string;->pspdf__stamp_approved:I

    const-string v3, "APPROVED"

    const/4 v4, 0x0

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v0, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->APPROVED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 2
    new-instance v1, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v2, Lcom/pspdfkit/annotations/stamps/StampType;->EXPERIMENTAL:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v3, Lcom/pspdfkit/R$string;->pspdf__stamp_experimental:I

    const-string v5, "EXPERIMENTAL"

    const/4 v6, 0x1

    invoke-direct {v1, v5, v6, v2, v3}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v1, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->EXPERIMENTAL:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 3
    new-instance v2, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v3, Lcom/pspdfkit/annotations/stamps/StampType;->NOT_APPROVED:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v5, Lcom/pspdfkit/R$string;->pspdf__stamp_not_approved:I

    const-string v7, "NOT_APPROVED"

    const/4 v8, 0x2

    invoke-direct {v2, v7, v8, v3, v5}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v2, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->NOT_APPROVED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 4
    new-instance v3, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v5, Lcom/pspdfkit/annotations/stamps/StampType;->AS_IS:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v7, Lcom/pspdfkit/R$string;->pspdf__stamp_as_is:I

    const-string v9, "AS_IS"

    const/4 v10, 0x3

    invoke-direct {v3, v9, v10, v5, v7}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v3, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->AS_IS:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 5
    new-instance v5, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v7, Lcom/pspdfkit/annotations/stamps/StampType;->EXPIRED:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v9, Lcom/pspdfkit/R$string;->pspdf__stamp_expired:I

    const-string v11, "EXPIRED"

    const/4 v12, 0x4

    invoke-direct {v5, v11, v12, v7, v9}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v5, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->EXPIRED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 6
    new-instance v7, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v9, Lcom/pspdfkit/annotations/stamps/StampType;->DRAFT:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v11, Lcom/pspdfkit/R$string;->pspdf__stamp_draft:I

    const-string v13, "DRAFT"

    const/4 v14, 0x5

    invoke-direct {v7, v13, v14, v9, v11}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v7, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->DRAFT:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 7
    new-instance v9, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v11, Lcom/pspdfkit/annotations/stamps/StampType;->FINAL:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v13, Lcom/pspdfkit/R$string;->pspdf__stamp_final:I

    const-string v15, "FINAL"

    const/4 v14, 0x6

    invoke-direct {v9, v15, v14, v11, v13}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v9, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->FINAL:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 8
    new-instance v11, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v13, Lcom/pspdfkit/annotations/stamps/StampType;->SOLD:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_sold:I

    const-string v14, "SOLD"

    const/4 v12, 0x7

    invoke-direct {v11, v14, v12, v13, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v11, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->SOLD:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 9
    new-instance v13, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->DEPARTMENTAL:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_departmental:I

    const-string v12, "DEPARTMENTAL"

    const/16 v10, 0x8

    invoke-direct {v13, v12, v10, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v13, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->DEPARTMENTAL:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 10
    new-instance v12, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->CONFIDENTIAL:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_confidential:I

    const-string v10, "CONFIDENTIAL"

    const/16 v8, 0x9

    invoke-direct {v12, v10, v8, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v12, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->CONFIDENTIAL:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 11
    new-instance v10, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->FOR_PUBLIC_RELEASE:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_for_public_release:I

    const-string v8, "FOR_PUBLIC_RELEASE"

    const/16 v6, 0xa

    invoke-direct {v10, v8, v6, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v10, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->FOR_PUBLIC_RELEASE:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 12
    new-instance v8, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->NOT_FOR_PUBLIC_RELEASE:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_not_for_public_release:I

    const-string v6, "NOT_FOR_PUBLIC_RELEASE"

    const/16 v4, 0xb

    invoke-direct {v8, v6, v4, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v8, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->NOT_FOR_PUBLIC_RELEASE:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 13
    new-instance v6, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->FOR_COMMENT:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_for_comment:I

    const-string v4, "FOR_COMMENT"

    move-object/from16 v16, v8

    const/16 v8, 0xc

    invoke-direct {v6, v4, v8, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v6, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->FOR_COMMENT:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 14
    new-instance v4, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->TOP_SECRET:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_top_secret:I

    const-string v8, "TOP_SECRET"

    move-object/from16 v17, v6

    const/16 v6, 0xd

    invoke-direct {v4, v8, v6, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v4, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->TOP_SECRET:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 17
    new-instance v8, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->COMPLETED:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_completed:I

    const-string v6, "COMPLETED"

    move-object/from16 v18, v4

    const/16 v4, 0xe

    invoke-direct {v8, v6, v4, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v8, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->COMPLETED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 18
    new-instance v6, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->VOID:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_void:I

    const-string v4, "VOID"

    move-object/from16 v19, v8

    const/16 v8, 0xf

    invoke-direct {v6, v4, v8, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v6, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->VOID:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 19
    new-instance v4, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->PRELIMINARY_RESULTS:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_preliminary_results:I

    const-string v8, "PRELIMINARY_RESULTS"

    move-object/from16 v20, v6

    const/16 v6, 0x10

    invoke-direct {v4, v8, v6, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v4, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->PRELIMINARY_RESULTS:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 20
    new-instance v8, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->INFORMATION_ONLY:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_information_only:I

    const-string v6, "INFORMATION_ONLY"

    move-object/from16 v21, v4

    const/16 v4, 0x11

    invoke-direct {v8, v6, v4, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v8, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->INFORMATION_ONLY:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 21
    new-instance v6, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->REVISED:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_revised:I

    const-string v4, "REVISED"

    move-object/from16 v22, v8

    const/16 v8, 0x12

    invoke-direct {v6, v4, v8, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v6, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->REVISED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 24
    new-instance v4, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->ACCEPTED:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_accepted:I

    const-string v8, "ACCEPTED"

    move-object/from16 v23, v6

    const/16 v6, 0x13

    invoke-direct {v4, v8, v6, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v4, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->ACCEPTED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 26
    new-instance v8, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->REJECTED:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_rejected:I

    const-string v6, "REJECTED"

    move-object/from16 v24, v4

    const/16 v4, 0x14

    invoke-direct {v8, v6, v4, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v8, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->REJECTED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 29
    new-instance v6, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->INITIAL_HERE:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_initial_here:I

    const-string v4, "INITIAL_HERE"

    move-object/from16 v25, v8

    const/16 v8, 0x15

    invoke-direct {v6, v4, v8, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v6, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->INITIAL_HERE:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 30
    new-instance v4, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v14, Lcom/pspdfkit/annotations/stamps/StampType;->SIGN_HERE:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v15, Lcom/pspdfkit/R$string;->pspdf__stamp_sign_here:I

    const-string v8, "SIGN_HERE"

    move-object/from16 v26, v6

    const/16 v6, 0x16

    invoke-direct {v4, v8, v6, v14, v15}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v4, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->SIGN_HERE:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 31
    new-instance v6, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v8, Lcom/pspdfkit/annotations/stamps/StampType;->WITNESS:Lcom/pspdfkit/annotations/stamps/StampType;

    sget v14, Lcom/pspdfkit/R$string;->pspdf__stamp_witness:I

    const-string v15, "WITNESS"

    move-object/from16 v27, v4

    const/16 v4, 0x17

    invoke-direct {v6, v15, v4, v8, v14}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v6, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->WITNESS:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    .line 34
    new-instance v4, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget v8, Lcom/pspdfkit/R$string;->pspdf__custom_stamp:I

    const-string v14, "CUSTOM"

    const/16 v15, 0x18

    move-object/from16 v28, v6

    const/4 v6, 0x0

    invoke-direct {v4, v14, v15, v6, v8}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;-><init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V

    sput-object v4, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->CUSTOM:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    const/16 v6, 0x19

    new-array v6, v6, [Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    const/4 v8, 0x0

    aput-object v0, v6, v8

    const/4 v0, 0x1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    aput-object v2, v6, v0

    const/4 v0, 0x3

    aput-object v3, v6, v0

    const/4 v0, 0x4

    aput-object v5, v6, v0

    const/4 v0, 0x5

    aput-object v7, v6, v0

    const/4 v0, 0x6

    aput-object v9, v6, v0

    const/4 v0, 0x7

    aput-object v11, v6, v0

    const/16 v0, 0x8

    aput-object v13, v6, v0

    const/16 v0, 0x9

    aput-object v12, v6, v0

    const/16 v0, 0xa

    aput-object v10, v6, v0

    const/16 v0, 0xb

    aput-object v16, v6, v0

    const/16 v0, 0xc

    aput-object v17, v6, v0

    const/16 v0, 0xd

    aput-object v18, v6, v0

    const/16 v0, 0xe

    aput-object v19, v6, v0

    const/16 v0, 0xf

    aput-object v20, v6, v0

    const/16 v0, 0x10

    aput-object v21, v6, v0

    const/16 v0, 0x11

    aput-object v22, v6, v0

    const/16 v0, 0x12

    aput-object v23, v6, v0

    const/16 v0, 0x13

    aput-object v24, v6, v0

    const/16 v0, 0x14

    aput-object v25, v6, v0

    const/16 v0, 0x15

    aput-object v26, v6, v0

    const/16 v0, 0x16

    aput-object v27, v6, v0

    const/16 v0, 0x17

    aput-object v28, v6, v0

    const/16 v0, 0x18

    aput-object v4, v6, v0

    .line 35
    sput-object v6, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->c:[Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/pspdfkit/annotations/stamps/StampType;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/stamps/StampType;",
            "I)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    iput-object p3, p0, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->a:Lcom/pspdfkit/annotations/stamps/StampType;

    .line 3
    iput p4, p0, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->b:I

    return-void
.end method

.method public static fromName(Ljava/lang/String;)Lcom/pspdfkit/annotations/stamps/PredefinedStampType;
    .locals 6

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 1
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeStampAnnotationHelper;->create()Lcom/pspdfkit/internal/jni/NativeStampAnnotationHelper;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/jni/NativeStampAnnotationHelper;->getStampType(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeStampType;

    move-result-object p0

    if-eqz p0, :cond_3

    .line 2
    invoke-static {}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->values()[Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_3

    aget-object v4, v1, v3

    .line 4
    iget-object v5, v4, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->a:Lcom/pspdfkit/annotations/stamps/StampType;

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/pspdfkit/annotations/stamps/StampType;->a()Lcom/pspdfkit/internal/jni/NativeStampType;

    move-result-object v5

    goto :goto_1

    :cond_1
    move-object v5, v0

    :goto_1
    if-ne v5, p0, :cond_2

    move-object v0, v4

    goto :goto_2

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    :goto_2
    return-object v0
.end method

.method public static fromStampType(Lcom/pspdfkit/annotations/stamps/StampType;)Lcom/pspdfkit/annotations/stamps/PredefinedStampType;
    .locals 6

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 1
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/stamps/StampType;->a()Lcom/pspdfkit/internal/jni/NativeStampType;

    move-result-object p0

    if-eqz p0, :cond_3

    .line 2
    invoke-static {}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->values()[Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_3

    aget-object v4, v1, v3

    .line 4
    iget-object v5, v4, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->a:Lcom/pspdfkit/annotations/stamps/StampType;

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/pspdfkit/annotations/stamps/StampType;->a()Lcom/pspdfkit/internal/jni/NativeStampType;

    move-result-object v5

    goto :goto_1

    :cond_1
    move-object v5, v0

    :goto_1
    if-ne v5, p0, :cond_2

    move-object v0, v4

    goto :goto_2

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    :goto_2
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/stamps/PredefinedStampType;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/annotations/stamps/PredefinedStampType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->c:[Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    invoke-virtual {v0}, [Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->a:Lcom/pspdfkit/annotations/stamps/StampType;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 4
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/stamps/StampType;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStampType()Lcom/pspdfkit/annotations/stamps/StampType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->a:Lcom/pspdfkit/annotations/stamps/StampType;

    return-object v0
.end method

.method public getTitleResId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->b:I

    return v0
.end method

.method public isStandard()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->CUSTOM:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

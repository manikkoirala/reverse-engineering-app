.class public abstract Lcom/pspdfkit/annotations/TextMarkupAnnotation;
.super Lcom/pspdfkit/annotations/BaseRectsAnnotation;
.source "SourceFile"


# direct methods
.method protected constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/annotations/BaseRectsAnnotation;-><init>(I)V

    return-void
.end method

.method protected constructor <init>(Lcom/pspdfkit/internal/p1;Z)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/annotations/BaseRectsAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    return-void
.end method


# virtual methods
.method public getHighlightedText()Ljava/lang/String;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/BaseRectsAnnotation;->getRects()Ljava/util/List;

    move-result-object v2

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v3

    const/high16 v4, -0x80000000

    if-eq v3, v4, :cond_2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 11
    :cond_1
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/zf;->a(ILjava/util/List;)Ljava/util/List;

    move-result-object v1

    goto :goto_1

    .line 12
    :cond_2
    :goto_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 13
    :goto_1
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHighlightedTextBlocks()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/datastructures/TextBlock;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/annotations/Annotation;->e:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_2

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/BaseRectsAnnotation;->getRects()Ljava/util/List;

    move-result-object v2

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v3

    const/high16 v4, -0x80000000

    if-eq v3, v4, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 9
    :cond_0
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/zf;->a(ILjava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 10
    :cond_1
    :goto_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 11
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public updateTransformationProperties(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 0

    return-void
.end method

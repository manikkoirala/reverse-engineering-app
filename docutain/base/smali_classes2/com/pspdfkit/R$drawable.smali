.class public final Lcom/pspdfkit/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f080028

.field public static final abc_action_bar_item_background_material:I = 0x7f080029

.field public static final abc_btn_borderless_material:I = 0x7f08002a

.field public static final abc_btn_check_material:I = 0x7f08002b

.field public static final abc_btn_check_material_anim:I = 0x7f08002c

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f08002d

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f08002e

.field public static final abc_btn_colored_material:I = 0x7f08002f

.field public static final abc_btn_default_mtrl_shape:I = 0x7f080030

.field public static final abc_btn_radio_material:I = 0x7f080031

.field public static final abc_btn_radio_material_anim:I = 0x7f080032

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f080033

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f080034

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f080035

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f080036

.field public static final abc_cab_background_internal_bg:I = 0x7f080037

.field public static final abc_cab_background_top_material:I = 0x7f080038

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f080039

.field public static final abc_control_background_material:I = 0x7f08003a

.field public static final abc_dialog_material_background:I = 0x7f08003b

.field public static final abc_edit_text_material:I = 0x7f08003c

.field public static final abc_ic_ab_back_material:I = 0x7f08003d

.field public static final abc_ic_arrow_drop_right_black_24dp:I = 0x7f08003e

.field public static final abc_ic_clear_material:I = 0x7f08003f

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f080040

.field public static final abc_ic_go_search_api_material:I = 0x7f080041

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f080042

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f080043

.field public static final abc_ic_menu_overflow_material:I = 0x7f080044

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f080045

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f080046

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f080047

.field public static final abc_ic_search_api_material:I = 0x7f080048

.field public static final abc_ic_voice_search_api_material:I = 0x7f080049

.field public static final abc_item_background_holo_dark:I = 0x7f08004a

.field public static final abc_item_background_holo_light:I = 0x7f08004b

.field public static final abc_list_divider_material:I = 0x7f08004c

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f08004d

.field public static final abc_list_focused_holo:I = 0x7f08004e

.field public static final abc_list_longpressed_holo:I = 0x7f08004f

.field public static final abc_list_pressed_holo_dark:I = 0x7f080050

.field public static final abc_list_pressed_holo_light:I = 0x7f080051

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f080052

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f080053

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f080054

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f080055

.field public static final abc_list_selector_holo_dark:I = 0x7f080056

.field public static final abc_list_selector_holo_light:I = 0x7f080057

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f080058

.field public static final abc_popup_background_mtrl_mult:I = 0x7f080059

.field public static final abc_ratingbar_indicator_material:I = 0x7f08005a

.field public static final abc_ratingbar_material:I = 0x7f08005b

.field public static final abc_ratingbar_small_material:I = 0x7f08005c

.field public static final abc_scrubber_control_off_mtrl_alpha:I = 0x7f08005d

.field public static final abc_scrubber_control_to_pressed_mtrl_000:I = 0x7f08005e

.field public static final abc_scrubber_control_to_pressed_mtrl_005:I = 0x7f08005f

.field public static final abc_scrubber_primary_mtrl_alpha:I = 0x7f080060

.field public static final abc_scrubber_track_mtrl_alpha:I = 0x7f080061

.field public static final abc_seekbar_thumb_material:I = 0x7f080062

.field public static final abc_seekbar_tick_mark_material:I = 0x7f080063

.field public static final abc_seekbar_track_material:I = 0x7f080064

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f080065

.field public static final abc_spinner_textfield_background_material:I = 0x7f080066

.field public static final abc_star_black_48dp:I = 0x7f080067

.field public static final abc_star_half_black_48dp:I = 0x7f080068

.field public static final abc_switch_thumb_material:I = 0x7f080069

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f08006a

.field public static final abc_tab_indicator_material:I = 0x7f08006b

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f08006c

.field public static final abc_text_cursor_material:I = 0x7f08006d

.field public static final abc_text_select_handle_left_mtrl:I = 0x7f08006e

.field public static final abc_text_select_handle_middle_mtrl:I = 0x7f08006f

.field public static final abc_text_select_handle_right_mtrl:I = 0x7f080070

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f080071

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f080072

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f080073

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f080074

.field public static final abc_textfield_search_material:I = 0x7f080075

.field public static final abc_vector_test:I = 0x7f080076

.field public static final avd_hide_password:I = 0x7f080081

.field public static final avd_show_password:I = 0x7f080082

.field public static final btn_checkbox_checked_mtrl:I = 0x7f080090

.field public static final btn_checkbox_checked_to_unchecked_mtrl_animation:I = 0x7f080091

.field public static final btn_checkbox_unchecked_mtrl:I = 0x7f080092

.field public static final btn_checkbox_unchecked_to_checked_mtrl_animation:I = 0x7f080093

.field public static final btn_radio_off_mtrl:I = 0x7f080094

.field public static final btn_radio_off_to_on_mtrl_animation:I = 0x7f080095

.field public static final btn_radio_on_mtrl:I = 0x7f080096

.field public static final btn_radio_on_to_off_mtrl_animation:I = 0x7f080097

.field public static final design_fab_background:I = 0x7f0800ce

.field public static final design_ic_visibility:I = 0x7f0800cf

.field public static final design_ic_visibility_off:I = 0x7f0800d0

.field public static final design_password_eye:I = 0x7f0800d1

.field public static final design_snackbar_background:I = 0x7f0800d2

.field public static final ic_arrow_back_black_24:I = 0x7f08010b

.field public static final ic_arrow_down_24dp:I = 0x7f08010c

.field public static final ic_clear_black_24:I = 0x7f080113

.field public static final ic_clock_black_24dp:I = 0x7f080114

.field public static final ic_keyboard_black_24dp:I = 0x7f080115

.field public static final ic_m3_chip_check:I = 0x7f08011a

.field public static final ic_m3_chip_checked_circle:I = 0x7f08011b

.field public static final ic_m3_chip_close:I = 0x7f08011c

.field public static final ic_mtrl_checked_circle:I = 0x7f08011d

.field public static final ic_mtrl_chip_checked_black:I = 0x7f08011e

.field public static final ic_mtrl_chip_checked_circle:I = 0x7f08011f

.field public static final ic_mtrl_chip_close_circle:I = 0x7f080120

.field public static final ic_search_black_24:I = 0x7f080121

.field public static final m3_appbar_background:I = 0x7f08013d

.field public static final m3_avd_hide_password:I = 0x7f08013e

.field public static final m3_avd_show_password:I = 0x7f08013f

.field public static final m3_password_eye:I = 0x7f080140

.field public static final m3_popupmenu_background_overlay:I = 0x7f080141

.field public static final m3_radiobutton_ripple:I = 0x7f080142

.field public static final m3_selection_control_ripple:I = 0x7f080143

.field public static final m3_tabs_background:I = 0x7f080144

.field public static final m3_tabs_line_indicator:I = 0x7f080145

.field public static final m3_tabs_rounded_line_indicator:I = 0x7f080146

.field public static final m3_tabs_transparent_background:I = 0x7f080147

.field public static final material_cursor_drawable:I = 0x7f080149

.field public static final material_ic_calendar_black_24dp:I = 0x7f08014a

.field public static final material_ic_clear_black_24dp:I = 0x7f08014b

.field public static final material_ic_edit_black_24dp:I = 0x7f08014c

.field public static final material_ic_keyboard_arrow_left_black_24dp:I = 0x7f08014d

.field public static final material_ic_keyboard_arrow_next_black_24dp:I = 0x7f08014e

.field public static final material_ic_keyboard_arrow_previous_black_24dp:I = 0x7f08014f

.field public static final material_ic_keyboard_arrow_right_black_24dp:I = 0x7f080150

.field public static final material_ic_menu_arrow_down_black_24dp:I = 0x7f080151

.field public static final material_ic_menu_arrow_up_black_24dp:I = 0x7f080152

.field public static final mtrl_bottomsheet_drag_handle:I = 0x7f080156

.field public static final mtrl_checkbox_button:I = 0x7f080157

.field public static final mtrl_checkbox_button_checked_unchecked:I = 0x7f080158

.field public static final mtrl_checkbox_button_icon:I = 0x7f080159

.field public static final mtrl_checkbox_button_icon_checked_indeterminate:I = 0x7f08015a

.field public static final mtrl_checkbox_button_icon_checked_unchecked:I = 0x7f08015b

.field public static final mtrl_checkbox_button_icon_indeterminate_checked:I = 0x7f08015c

.field public static final mtrl_checkbox_button_icon_indeterminate_unchecked:I = 0x7f08015d

.field public static final mtrl_checkbox_button_icon_unchecked_checked:I = 0x7f08015e

.field public static final mtrl_checkbox_button_icon_unchecked_indeterminate:I = 0x7f08015f

.field public static final mtrl_checkbox_button_unchecked_checked:I = 0x7f080160

.field public static final mtrl_dialog_background:I = 0x7f080161

.field public static final mtrl_dropdown_arrow:I = 0x7f080162

.field public static final mtrl_ic_arrow_drop_down:I = 0x7f080163

.field public static final mtrl_ic_arrow_drop_up:I = 0x7f080164

.field public static final mtrl_ic_cancel:I = 0x7f080165

.field public static final mtrl_ic_check_mark:I = 0x7f080166

.field public static final mtrl_ic_checkbox_checked:I = 0x7f080167

.field public static final mtrl_ic_checkbox_unchecked:I = 0x7f080168

.field public static final mtrl_ic_error:I = 0x7f080169

.field public static final mtrl_ic_indeterminate:I = 0x7f08016a

.field public static final mtrl_navigation_bar_item_background:I = 0x7f08016b

.field public static final mtrl_popupmenu_background:I = 0x7f08016c

.field public static final mtrl_popupmenu_background_overlay:I = 0x7f08016d

.field public static final mtrl_switch_thumb:I = 0x7f08016e

.field public static final mtrl_switch_thumb_checked:I = 0x7f08016f

.field public static final mtrl_switch_thumb_checked_pressed:I = 0x7f080170

.field public static final mtrl_switch_thumb_checked_unchecked:I = 0x7f080171

.field public static final mtrl_switch_thumb_pressed:I = 0x7f080172

.field public static final mtrl_switch_thumb_pressed_checked:I = 0x7f080173

.field public static final mtrl_switch_thumb_pressed_unchecked:I = 0x7f080174

.field public static final mtrl_switch_thumb_unchecked:I = 0x7f080175

.field public static final mtrl_switch_thumb_unchecked_checked:I = 0x7f080176

.field public static final mtrl_switch_thumb_unchecked_pressed:I = 0x7f080177

.field public static final mtrl_switch_track:I = 0x7f080178

.field public static final mtrl_switch_track_decoration:I = 0x7f080179

.field public static final mtrl_tabs_default_indicator:I = 0x7f08017a

.field public static final navigation_empty_icon:I = 0x7f08017d

.field public static final notification_action_background:I = 0x7f080183

.field public static final notification_bg:I = 0x7f080184

.field public static final notification_bg_low:I = 0x7f080185

.field public static final notification_bg_low_normal:I = 0x7f080186

.field public static final notification_bg_low_pressed:I = 0x7f080187

.field public static final notification_bg_normal:I = 0x7f080188

.field public static final notification_bg_normal_pressed:I = 0x7f080189

.field public static final notification_icon_background:I = 0x7f08018a

.field public static final notification_template_icon_bg:I = 0x7f08018c

.field public static final notification_template_icon_low_bg:I = 0x7f08018d

.field public static final notification_tile_bg:I = 0x7f08018e

.field public static final notify_panel_notification_icon_bg:I = 0x7f080190

.field public static final preference_list_divider_material:I = 0x7f0801ae

.field public static final pspdf__arrow_right:I = 0x7f0801b2

.field public static final pspdf__audio_view_background:I = 0x7f0801b3

.field public static final pspdf__bg_page_pattern_5mm_dot:I = 0x7f0801b4

.field public static final pspdf__bg_page_pattern_5mm_line:I = 0x7f0801b5

.field public static final pspdf__bg_page_pattern_5mm_square:I = 0x7f0801b6

.field public static final pspdf__bg_page_pattern_7mm_line:I = 0x7f0801b7

.field public static final pspdf__check_mark:I = 0x7f0801b8

.field public static final pspdf__circle_shape:I = 0x7f0801b9

.field public static final pspdf__circle_shape_transparent:I = 0x7f0801ba

.field public static final pspdf__content_editing_circle_gray_shape:I = 0x7f0801bb

.field public static final pspdf__content_editing_left_oval_gray_shape:I = 0x7f0801bc

.field public static final pspdf__content_editing_oval_gray_shape:I = 0x7f0801bd

.field public static final pspdf__content_editing_right_oval_gray_shape:I = 0x7f0801be

.field public static final pspdf__document_binding_left:I = 0x7f0801bf

.field public static final pspdf__document_binding_right:I = 0x7f0801c0

.field public static final pspdf__document_info_item_spacing:I = 0x7f0801c1

.field public static final pspdf__electronic_signature_controls_view_background:I = 0x7f0801c2

.field public static final pspdf__electronic_signature_controls_view_dialog_background:I = 0x7f0801c3

.field public static final pspdf__electronic_signature_font_text_color_selector:I = 0x7f0801c4

.field public static final pspdf__electronic_signature_save_signature_chip_background_selectable:I = 0x7f0801c5

.field public static final pspdf__electronic_signature_tt_icon:I = 0x7f0801c6

.field public static final pspdf__electronic_signature_tt_icon_selector:I = 0x7f0801c7

.field public static final pspdf__electronic_signature_tt_selected_icon:I = 0x7f0801c8

.field public static final pspdf__file_icon_graph:I = 0x7f0801c9

.field public static final pspdf__file_icon_paperclip:I = 0x7f0801ca

.field public static final pspdf__file_icon_push_pin:I = 0x7f0801cb

.field public static final pspdf__file_icon_tag:I = 0x7f0801cc

.field public static final pspdf__grid_list_label_background:I = 0x7f0801cd

.field public static final pspdf__ic_activity_empty:I = 0x7f0801ce

.field public static final pspdf__ic_add:I = 0x7f0801cf

.field public static final pspdf__ic_align:I = 0x7f0801d0

.field public static final pspdf__ic_arrow_back:I = 0x7f0801d1

.field public static final pspdf__ic_bold:I = 0x7f0801d2

.field public static final pspdf__ic_camera:I = 0x7f0801d3

.field public static final pspdf__ic_cancel:I = 0x7f0801d4

.field public static final pspdf__ic_caret:I = 0x7f0801d5

.field public static final pspdf__ic_certificate_add:I = 0x7f0801d6

.field public static final pspdf__ic_certificate_valid:I = 0x7f0801d7

.field public static final pspdf__ic_check:I = 0x7f0801d8

.field public static final pspdf__ic_chevron_left:I = 0x7f0801d9

.field public static final pspdf__ic_chevron_right:I = 0x7f0801da

.field public static final pspdf__ic_chevron_up:I = 0x7f0801db

.field public static final pspdf__ic_circle:I = 0x7f0801dc

.field public static final pspdf__ic_close:I = 0x7f0801dd

.field public static final pspdf__ic_close_circled:I = 0x7f0801de

.field public static final pspdf__ic_cloudy_circle:I = 0x7f0801df

.field public static final pspdf__ic_cloudy_polygon:I = 0x7f0801e0

.field public static final pspdf__ic_cloudy_square:I = 0x7f0801e1

.field public static final pspdf__ic_color_selected:I = 0x7f0801e2

.field public static final pspdf__ic_color_selected_bg:I = 0x7f0801e3

.field public static final pspdf__ic_compare_documents:I = 0x7f0801e4

.field public static final pspdf__ic_content_copy:I = 0x7f0801e5

.field public static final pspdf__ic_content_cut:I = 0x7f0801e6

.field public static final pspdf__ic_dashed_circle:I = 0x7f0801e7

.field public static final pspdf__ic_dashed_polygon:I = 0x7f0801e8

.field public static final pspdf__ic_dashed_square:I = 0x7f0801e9

.field public static final pspdf__ic_delete:I = 0x7f0801ea

.field public static final pspdf__ic_done:I = 0x7f0801eb

.field public static final pspdf__ic_drag_handle:I = 0x7f0801ec

.field public static final pspdf__ic_duplicate_page:I = 0x7f0801ed

.field public static final pspdf__ic_edit:I = 0x7f0801ee

.field public static final pspdf__ic_edit_annotations:I = 0x7f0801ef

.field public static final pspdf__ic_edit_content:I = 0x7f0801f0

.field public static final pspdf__ic_eraser:I = 0x7f0801f1

.field public static final pspdf__ic_export_pages:I = 0x7f0801f2

.field public static final pspdf__ic_file:I = 0x7f0801f3

.field public static final pspdf__ic_font_courier:I = 0x7f0801f4

.field public static final pspdf__ic_font_helvetica:I = 0x7f0801f5

.field public static final pspdf__ic_font_times:I = 0x7f0801f6

.field public static final pspdf__ic_form_button:I = 0x7f0801f7

.field public static final pspdf__ic_form_choice:I = 0x7f0801f8

.field public static final pspdf__ic_form_signature:I = 0x7f0801f9

.field public static final pspdf__ic_form_textfield:I = 0x7f0801fa

.field public static final pspdf__ic_freetext:I = 0x7f0801fb

.field public static final pspdf__ic_freetext_callout:I = 0x7f0801fc

.field public static final pspdf__ic_hearing:I = 0x7f0801fd

.field public static final pspdf__ic_highlight:I = 0x7f0801fe

.field public static final pspdf__ic_image:I = 0x7f0801ff

.field public static final pspdf__ic_import_document:I = 0x7f080200

.field public static final pspdf__ic_import_documents:I = 0x7f080201

.field public static final pspdf__ic_info:I = 0x7f080202

.field public static final pspdf__ic_ink_highlighter:I = 0x7f080203

.field public static final pspdf__ic_input_error:I = 0x7f080204

.field public static final pspdf__ic_instant_comment:I = 0x7f080205

.field public static final pspdf__ic_italic:I = 0x7f080206

.field public static final pspdf__ic_line:I = 0x7f080207

.field public static final pspdf__ic_line_arrow:I = 0x7f080208

.field public static final pspdf__ic_link:I = 0x7f080209

.field public static final pspdf__ic_lock:I = 0x7f08020a

.field public static final pspdf__ic_magic_ink:I = 0x7f08020b

.field public static final pspdf__ic_measurement_area_ellipse:I = 0x7f08020c

.field public static final pspdf__ic_measurement_area_polygon:I = 0x7f08020d

.field public static final pspdf__ic_measurement_area_rectangle:I = 0x7f08020e

.field public static final pspdf__ic_measurement_distance:I = 0x7f08020f

.field public static final pspdf__ic_measurement_perimeter:I = 0x7f080210

.field public static final pspdf__ic_more:I = 0x7f080211

.field public static final pspdf__ic_more_horizontal:I = 0x7f080212

.field public static final pspdf__ic_note:I = 0x7f080213

.field public static final pspdf__ic_open_in:I = 0x7f080214

.field public static final pspdf__ic_outline:I = 0x7f080215

.field public static final pspdf__ic_outline_view_annotations:I = 0x7f080216

.field public static final pspdf__ic_outline_view_bookmarks:I = 0x7f080217

.field public static final pspdf__ic_outline_view_information:I = 0x7f080218

.field public static final pspdf__ic_outline_view_outline:I = 0x7f080219

.field public static final pspdf__ic_pause:I = 0x7f08021a

.field public static final pspdf__ic_play:I = 0x7f08021b

.field public static final pspdf__ic_polygon:I = 0x7f08021c

.field public static final pspdf__ic_polyline:I = 0x7f08021d

.field public static final pspdf__ic_print:I = 0x7f08021e

.field public static final pspdf__ic_print_large:I = 0x7f08021f

.field public static final pspdf__ic_reader_view:I = 0x7f080220

.field public static final pspdf__ic_record:I = 0x7f080221

.field public static final pspdf__ic_redaction:I = 0x7f080222

.field public static final pspdf__ic_redo:I = 0x7f080223

.field public static final pspdf__ic_remove:I = 0x7f080224

.field public static final pspdf__ic_replies:I = 0x7f080225

.field public static final pspdf__ic_richmedia:I = 0x7f080226

.field public static final pspdf__ic_rotate_page:I = 0x7f080227

.field public static final pspdf__ic_search:I = 0x7f080228

.field public static final pspdf__ic_select_font_name:I = 0x7f080229

.field public static final pspdf__ic_select_font_size:I = 0x7f08022a

.field public static final pspdf__ic_settings:I = 0x7f08022b

.field public static final pspdf__ic_settings_automatic_layout:I = 0x7f08022c

.field public static final pspdf__ic_settings_continuous_horizontal:I = 0x7f08022d

.field public static final pspdf__ic_settings_continuous_vertical:I = 0x7f08022e

.field public static final pspdf__ic_settings_default_theme:I = 0x7f08022f

.field public static final pspdf__ic_settings_default_theme2:I = 0x7f080230

.field public static final pspdf__ic_settings_double_layout:I = 0x7f080231

.field public static final pspdf__ic_settings_horizontal:I = 0x7f080232

.field public static final pspdf__ic_settings_jump:I = 0x7f080233

.field public static final pspdf__ic_settings_night_theme:I = 0x7f080234

.field public static final pspdf__ic_settings_night_theme2:I = 0x7f080235

.field public static final pspdf__ic_settings_not_selected_circular:I = 0x7f080236

.field public static final pspdf__ic_settings_selected_circular:I = 0x7f080237

.field public static final pspdf__ic_settings_single_layout:I = 0x7f080238

.field public static final pspdf__ic_settings_vertical:I = 0x7f080239

.field public static final pspdf__ic_share:I = 0x7f08023a

.field public static final pspdf__ic_signature:I = 0x7f08023b

.field public static final pspdf__ic_size:I = 0x7f08023c

.field public static final pspdf__ic_sound:I = 0x7f08023d

.field public static final pspdf__ic_square:I = 0x7f08023e

.field public static final pspdf__ic_squiggly:I = 0x7f08023f

.field public static final pspdf__ic_stamp:I = 0x7f080240

.field public static final pspdf__ic_status_accepted:I = 0x7f080241

.field public static final pspdf__ic_status_cancelled:I = 0x7f080242

.field public static final pspdf__ic_status_clear:I = 0x7f080243

.field public static final pspdf__ic_status_completed:I = 0x7f080244

.field public static final pspdf__ic_status_rejected:I = 0x7f080245

.field public static final pspdf__ic_stop:I = 0x7f080246

.field public static final pspdf__ic_strikeout:I = 0x7f080247

.field public static final pspdf__ic_stylus:I = 0x7f080248

.field public static final pspdf__ic_thumbnails:I = 0x7f080249

.field public static final pspdf__ic_thumbnails_active:I = 0x7f08024a

.field public static final pspdf__ic_underline:I = 0x7f08024b

.field public static final pspdf__ic_undo:I = 0x7f08024c

.field public static final pspdf__ic_warning:I = 0x7f08024d

.field public static final pspdf__ic_widget:I = 0x7f08024e

.field public static final pspdf__ic_zindex_bottom:I = 0x7f08024f

.field public static final pspdf__ic_zindex_down:I = 0x7f080250

.field public static final pspdf__ic_zindex_top:I = 0x7f080251

.field public static final pspdf__ic_zindex_up:I = 0x7f080252

.field public static final pspdf__list_divider:I = 0x7f080253

.field public static final pspdf__list_divider_light:I = 0x7f080254

.field public static final pspdf__navigate_back:I = 0x7f080255

.field public static final pspdf__navigate_forward:I = 0x7f080256

.field public static final pspdf__note_editor_status_count_background:I = 0x7f080257

.field public static final pspdf__note_icon_check:I = 0x7f080258

.field public static final pspdf__note_icon_circle:I = 0x7f080259

.field public static final pspdf__note_icon_comment:I = 0x7f08025a

.field public static final pspdf__note_icon_cross:I = 0x7f08025b

.field public static final pspdf__note_icon_help:I = 0x7f08025c

.field public static final pspdf__note_icon_insert:I = 0x7f08025d

.field public static final pspdf__note_icon_instant_comment:I = 0x7f08025e

.field public static final pspdf__note_icon_key:I = 0x7f08025f

.field public static final pspdf__note_icon_new_paragraph:I = 0x7f080260

.field public static final pspdf__note_icon_note:I = 0x7f080261

.field public static final pspdf__note_icon_paragraph:I = 0x7f080262

.field public static final pspdf__note_icon_right_arrow:I = 0x7f080263

.field public static final pspdf__note_icon_right_pointer:I = 0x7f080264

.field public static final pspdf__note_icon_star:I = 0x7f080265

.field public static final pspdf__page_grid_item_selector:I = 0x7f080266

.field public static final pspdf__point_selection_1:I = 0x7f080267

.field public static final pspdf__point_selection_2:I = 0x7f080268

.field public static final pspdf__point_selection_3:I = 0x7f080269

.field public static final pspdf__rounded_rect:I = 0x7f08026a

.field public static final pspdf__rounded_rect_measurements:I = 0x7f08026b

.field public static final pspdf__rounded_rect_note_editor_selected_status:I = 0x7f08026c

.field public static final pspdf__rounded_rect_note_editor_status:I = 0x7f08026d

.field public static final pspdf__rounded_rect_note_editor_status_selected:I = 0x7f08026e

.field public static final pspdf__rounded_rect_note_editor_style_box_item:I = 0x7f08026f

.field public static final pspdf__rounded_rect_note_editor_style_box_item_selected:I = 0x7f080270

.field public static final pspdf__rounded_rect_translucent:I = 0x7f080271

.field public static final pspdf__rounder_rect_white:I = 0x7f080272

.field public static final pspdf__select_point_fab:I = 0x7f080273

.field public static final pspdf__select_point_indicator:I = 0x7f080274

.field public static final pspdf__settings_horizontal_double_dark:I = 0x7f080275

.field public static final pspdf__settings_horizontal_double_light:I = 0x7f080276

.field public static final pspdf__settings_horizontal_single_dark:I = 0x7f080277

.field public static final pspdf__settings_horizontal_single_light:I = 0x7f080278

.field public static final pspdf__settings_item_background:I = 0x7f080279

.field public static final pspdf__settings_vertical_single_scroll_dark:I = 0x7f08027a

.field public static final pspdf__settings_vertical_single_scroll_light:I = 0x7f08027b

.field public static final pspdf__signature_signer_chip_avatar_icon:I = 0x7f08027c

.field public static final pspdf__signature_signer_chip_background:I = 0x7f08027d

.field public static final pspdf__signature_signer_chip_background_selectable:I = 0x7f08027e

.field public static final pspdf__text_select_handle_left:I = 0x7f08027f

.field public static final pspdf__text_select_handle_right:I = 0x7f080280

.field public static final pspdf__thumbnail_bar_background:I = 0x7f080281

.field public static final pspdf__uvv_itv_player_play:I = 0x7f080282

.field public static final pspdf__uvv_on_error:I = 0x7f080283

.field public static final pspdf__uvv_player_player_btn:I = 0x7f080284

.field public static final pspdf__uvv_player_scale_btn:I = 0x7f080285

.field public static final pspdf__uvv_player_scale_out_btn:I = 0x7f080286

.field public static final pspdf__uvv_star_play_progress_seek:I = 0x7f080287

.field public static final pspdf__uvv_stop_btn:I = 0x7f080288

.field public static final test_level_drawable:I = 0x7f0802b4

.field public static final tooltip_frame_dark:I = 0x7f0802bc

.field public static final tooltip_frame_light:I = 0x7f0802bd


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

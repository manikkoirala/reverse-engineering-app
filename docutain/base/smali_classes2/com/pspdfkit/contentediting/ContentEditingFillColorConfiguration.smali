.class public interface abstract Lcom/pspdfkit/contentediting/ContentEditingFillColorConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract customColorPickerEnabled()Z
.end method

.method public abstract getAvailableFillColors()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDefaultFillColor()I
.end method

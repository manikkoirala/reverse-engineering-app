.class public Lcom/pspdfkit/forms/SignatureFormConfiguration;
.super Lcom/pspdfkit/forms/FormElementConfiguration;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/forms/SignatureFormConfiguration$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/forms/FormElementConfiguration<",
        "Lcom/pspdfkit/forms/SignatureFormElement;",
        "Lcom/pspdfkit/forms/SignatureFormField;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/SignatureFormConfiguration$Builder;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/forms/FormElementConfiguration;-><init>(Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;)V

    return-void
.end method


# virtual methods
.method final a(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/forms/SignatureFormField;

    .line 2
    new-instance v0, Lcom/pspdfkit/forms/SignatureFormElement;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/SignatureFormElement;-><init>(Lcom/pspdfkit/forms/SignatureFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/forms/FormElementConfiguration;->a(Lcom/pspdfkit/forms/FormElement;)V

    return-object v0
.end method

.method final a()Lcom/pspdfkit/forms/FormType;
    .locals 1

    .line 4
    sget-object v0, Lcom/pspdfkit/forms/FormType;->SIGNATURE:Lcom/pspdfkit/forms/FormType;

    return-object v0
.end method

.method final a(I)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

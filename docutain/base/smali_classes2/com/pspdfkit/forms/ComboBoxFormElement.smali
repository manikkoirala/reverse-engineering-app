.class public Lcom/pspdfkit/forms/ComboBoxFormElement;
.super Lcom/pspdfkit/forms/ChoiceFormElement;
.source "SourceFile"


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/ComboBoxFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/forms/ChoiceFormElement;-><init>(Lcom/pspdfkit/forms/ChoiceFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-void
.end method


# virtual methods
.method public getCustomText()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFormControl;->getCustomValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getFormField()Lcom/pspdfkit/forms/ChoiceFormField;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ComboBoxFormElement;->getFormField()Lcom/pspdfkit/forms/ComboBoxFormField;

    move-result-object v0

    return-object v0
.end method

.method public getFormField()Lcom/pspdfkit/forms/ComboBoxFormField;
    .locals 1

    .line 3
    invoke-super {p0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getFormField()Lcom/pspdfkit/forms/ChoiceFormField;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/forms/ComboBoxFormField;

    return-object v0
.end method

.method public bridge synthetic getFormField()Lcom/pspdfkit/forms/FormField;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ComboBoxFormElement;->getFormField()Lcom/pspdfkit/forms/ComboBoxFormField;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/pspdfkit/forms/FormType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/forms/FormType;->COMBOBOX:Lcom/pspdfkit/forms/FormType;

    return-object v0
.end method

.method public isCustomTextSet()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFormControl;->isCustomValueSet()Z

    move-result v0

    return v0
.end method

.method public isEditable()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ComboBoxFormElement;->getFormField()Lcom/pspdfkit/forms/ChoiceFormField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getChoiceFlags()Ljava/util/EnumSet;

    move-result-object v0

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;->EDIT:Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSpellCheckEnabled()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ComboBoxFormElement;->getFormField()Lcom/pspdfkit/forms/ChoiceFormField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getChoiceFlags()Ljava/util/EnumSet;

    move-result-object v0

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;->DO_NOT_SPELL_CHECK:Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public setCustomText(Ljava/lang/String;)Z
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ComboBoxFormElement;->isEditable()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFormControl;->getCustomValue()Ljava/lang/String;

    move-result-object v0

    .line 3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    return v1

    .line 4
    :cond_3
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeFormControl;->setCustomValue(Ljava/lang/String;)V

    return v3

    :cond_4
    return v1
.end method

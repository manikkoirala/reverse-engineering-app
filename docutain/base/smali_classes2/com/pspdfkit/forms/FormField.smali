.class public Lcom/pspdfkit/forms/FormField;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/jni/NativeFormField;

.field private final b:I

.field private final c:Lcom/pspdfkit/forms/FormType;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private h:Lcom/pspdfkit/internal/jni/NativeFormControl;

.field private i:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeFormFlags;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeFormTextFlags;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/forms/FormElement;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcom/pspdfkit/internal/tf;


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/forms/FormField;)Lcom/pspdfkit/internal/jni/NativeFormField;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/forms/FormField;->a:Lcom/pspdfkit/internal/jni/NativeFormField;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/forms/FormField;)Lcom/pspdfkit/internal/jni/NativeFormControl;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/forms/FormField;->h:Lcom/pspdfkit/internal/jni/NativeFormControl;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeti(Lcom/pspdfkit/forms/FormField;)Ljava/util/EnumSet;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/forms/FormField;->i:Ljava/util/EnumSet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetj(Lcom/pspdfkit/forms/FormField;)Ljava/util/EnumSet;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/forms/FormField;->j:Ljava/util/EnumSet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetk(Lcom/pspdfkit/forms/FormField;)Ljava/util/EnumSet;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/forms/FormField;->k:Ljava/util/EnumSet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputh(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/internal/jni/NativeFormControl;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/forms/FormField;->h:Lcom/pspdfkit/internal/jni/NativeFormControl;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputi(Lcom/pspdfkit/forms/FormField;Ljava/util/EnumSet;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/forms/FormField;->i:Ljava/util/EnumSet;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputj(Lcom/pspdfkit/forms/FormField;Ljava/util/EnumSet;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/forms/FormField;->j:Ljava/util/EnumSet;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputk(Lcom/pspdfkit/forms/FormField;Ljava/util/EnumSet;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/forms/FormField;->k:Ljava/util/EnumSet;

    return-void
.end method

.method constructor <init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/forms/FormField$1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/forms/FormField$1;-><init>(Lcom/pspdfkit/forms/FormField;)V

    iput-object v0, p0, Lcom/pspdfkit/forms/FormField;->m:Lcom/pspdfkit/internal/tf;

    .line 95
    iput-object p2, p0, Lcom/pspdfkit/forms/FormField;->a:Lcom/pspdfkit/internal/jni/NativeFormField;

    .line 96
    iput p1, p0, Lcom/pspdfkit/forms/FormField;->b:I

    .line 98
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeFormField;->getType()Lcom/pspdfkit/internal/jni/NativeFormType;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/jni/NativeFormType;)Lcom/pspdfkit/forms/FormType;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/forms/FormField;->c:Lcom/pspdfkit/forms/FormType;

    .line 100
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeFormField;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/forms/FormField;->d:Ljava/lang/String;

    .line 101
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeFormField;->getFQN()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/forms/FormField;->e:Ljava/lang/String;

    .line 102
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeFormField;->getMappingName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/forms/FormField;->f:Ljava/lang/String;

    .line 103
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeFormField;->getAlternateFieldName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/forms/FormField;->g:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .line 2
    iget v0, p0, Lcom/pspdfkit/forms/FormField;->b:I

    return v0
.end method

.method final a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormElement;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/forms/FormField;->l:Ljava/util/List;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/forms/FormField;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 3
    :cond_1
    check-cast p1, Lcom/pspdfkit/forms/FormField;

    .line 4
    iget v1, p0, Lcom/pspdfkit/forms/FormField;->b:I

    iget v3, p1, Lcom/pspdfkit/forms/FormField;->b:I

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/forms/FormField;->e:Ljava/lang/String;

    iget-object p1, p1, Lcom/pspdfkit/forms/FormField;->e:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getAlternateFieldName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormField;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getFormElement()Lcom/pspdfkit/forms/FormElement;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormField;->l:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/forms/FormField;->l:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/forms/FormElement;

    return-object v0

    .line 4
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Form field has no elements!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFormElements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/forms/FormElement;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormField;->l:Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getFullyQualifiedName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormField;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getInternal()Lcom/pspdfkit/internal/tf;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormField;->m:Lcom/pspdfkit/internal/tf;

    return-object v0
.end method

.method public getMappingName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormField;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormField;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/pspdfkit/forms/FormType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormField;->c:Lcom/pspdfkit/forms/FormType;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormField;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 2
    iget v1, p0, Lcom/pspdfkit/forms/FormField;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method public isDirty()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getNativeFormControl()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFormControl;->isDirty()Z

    move-result v0

    return v0
.end method

.method public isExported()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormField;->m:Lcom/pspdfkit/internal/tf;

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getFlags()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormFlags;->NOEXPORT:Lcom/pspdfkit/internal/jni/NativeFormFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isReadOnly()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormField;->m:Lcom/pspdfkit/internal/tf;

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getFlags()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormFlags;->READONLY:Lcom/pspdfkit/internal/jni/NativeFormFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isRequired()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormField;->m:Lcom/pspdfkit/internal/tf;

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getFlags()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormFlags;->REQUIRED:Lcom/pspdfkit/internal/jni/NativeFormFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public reset()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getNativeFormControl()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFormControl;->reset()Z

    move-result v0

    return v0
.end method

.class public abstract Lcom/pspdfkit/forms/FormElementConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/pspdfkit/forms/FormElement;",
        "K:",
        "Lcom/pspdfkit/forms/FormField;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected final a:I

.field protected final b:Landroid/graphics/RectF;

.field protected final c:Lcom/pspdfkit/forms/FormElement;

.field protected final d:Lcom/pspdfkit/forms/FormElement;

.field private final e:Lcom/pspdfkit/internal/o;

.field private final f:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeFormFlags;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iget v0, p1, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->a:I

    iput v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->a:I

    .line 3
    iget-object v0, p1, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->b:Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->b:Landroid/graphics/RectF;

    .line 4
    iget-object v0, p1, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->d:Lcom/pspdfkit/forms/FormElement;

    iput-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->c:Lcom/pspdfkit/forms/FormElement;

    .line 5
    iget-object v0, p1, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->e:Lcom/pspdfkit/forms/FormElement;

    iput-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->d:Lcom/pspdfkit/forms/FormElement;

    .line 6
    iget-object v0, p1, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->f:Lcom/pspdfkit/internal/o;

    iput-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->e:Lcom/pspdfkit/internal/o;

    .line 7
    iget-object p1, p1, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->c:Ljava/util/EnumSet;

    invoke-static {p1}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->f:Ljava/util/EnumSet;

    return-void
.end method


# virtual methods
.method abstract a(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/pspdfkit/annotations/WidgetAnnotation;",
            ")TT;"
        }
    .end annotation
.end method

.method abstract a()Lcom/pspdfkit/forms/FormType;
.end method

.method abstract a(I)Ljava/lang/String;
.end method

.method final a(Lcom/pspdfkit/forms/FormElement;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->d:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/forms/FormElement;->setNextElement(Lcom/pspdfkit/forms/FormElement;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->c:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/forms/FormElement;->setPreviousElement(Lcom/pspdfkit/forms/FormElement;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->e:Lcom/pspdfkit/internal/o;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/o;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/actions/Action;

    invoke-virtual {v2, v3, v1}, Lcom/pspdfkit/annotations/WidgetAnnotation;->setAdditionalAction(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;Lcom/pspdfkit/annotations/actions/Action;)V

    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getFormField()Lcom/pspdfkit/forms/FormField;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->f:Ljava/util/EnumSet;

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/tf;->setFlags(Ljava/util/EnumSet;)V

    return-void
.end method

.method public getAdditionalActions()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;",
            "Lcom/pspdfkit/annotations/actions/Action;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->e:Lcom/pspdfkit/internal/o;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/o;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getNextElement()Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->d:Lcom/pspdfkit/forms/FormElement;

    return-object v0
.end method

.method public getPreviousElement()Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->c:Lcom/pspdfkit/forms/FormElement;

    return-object v0
.end method

.method public isReadOnly()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->f:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormFlags;->READONLY:Lcom/pspdfkit/internal/jni/NativeFormFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isRequired()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration;->f:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormFlags;->REQUIRED:Lcom/pspdfkit/internal/jni/NativeFormFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

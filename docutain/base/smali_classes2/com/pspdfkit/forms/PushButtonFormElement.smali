.class public Lcom/pspdfkit/forms/PushButtonFormElement;
.super Lcom/pspdfkit/forms/ButtonFormElement;
.source "SourceFile"


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/PushButtonFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/forms/ButtonFormElement;-><init>(Lcom/pspdfkit/forms/ButtonFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/forms/PushButtonFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/forms/ButtonFormElement;-><init>(Lcom/pspdfkit/forms/ButtonFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    .line 3
    invoke-virtual {p0, p3}, Lcom/pspdfkit/forms/PushButtonFormElement;->setBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method


# virtual methods
.method public getAction()Lcom/pspdfkit/annotations/actions/Action;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/LinkAnnotation;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v0

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getAnnotationResource()Lcom/pspdfkit/internal/x1;

    move-result-object v0

    .line 2
    instance-of v1, v0, Lcom/pspdfkit/internal/a0;

    if-eqz v1, :cond_0

    .line 3
    check-cast v0, Lcom/pspdfkit/internal/a0;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/a0;->j()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic getFormField()Lcom/pspdfkit/forms/FormField;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/PushButtonFormElement;->getFormField()Lcom/pspdfkit/forms/PushButtonFormField;

    move-result-object v0

    return-object v0
.end method

.method public getFormField()Lcom/pspdfkit/forms/PushButtonFormField;
    .locals 1

    .line 2
    invoke-super {p0}, Lcom/pspdfkit/forms/FormElement;->getFormField()Lcom/pspdfkit/forms/FormField;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/forms/PushButtonFormField;

    return-object v0
.end method

.method public getType()Lcom/pspdfkit/forms/FormType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/forms/FormType;->PUSHBUTTON:Lcom/pspdfkit/forms/FormType;

    return-object v0
.end method

.method public setAction(Lcom/pspdfkit/annotations/actions/Action;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/LinkAnnotation;->setAction(Lcom/pspdfkit/annotations/actions/Action;)V

    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 3

    const-string v0, "bitmap"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/internal/a0;

    .line 55
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, p1, v2}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;Z)V

    .line 57
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    .line 58
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    return-void
.end method

.class public Lcom/pspdfkit/forms/PushButtonFormConfiguration$Builder;
.super Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/forms/PushButtonFormConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder<",
        "Lcom/pspdfkit/forms/PushButtonFormConfiguration;",
        "Lcom/pspdfkit/forms/PushButtonFormConfiguration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field g:Landroid/graphics/Bitmap;

.field h:Lcom/pspdfkit/annotations/actions/Action;


# direct methods
.method public constructor <init>(ILandroid/graphics/RectF;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;-><init>(ILandroid/graphics/RectF;)V

    const-string p1, "bitmap"

    .line 2
    invoke-static {p3, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p3, p0, Lcom/pspdfkit/forms/PushButtonFormConfiguration$Builder;->g:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method protected final a()Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic build()Lcom/pspdfkit/forms/FormElementConfiguration;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/PushButtonFormConfiguration$Builder;->build()Lcom/pspdfkit/forms/PushButtonFormConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/pspdfkit/forms/PushButtonFormConfiguration;
    .locals 1

    .line 2
    new-instance v0, Lcom/pspdfkit/forms/PushButtonFormConfiguration;

    invoke-direct {v0, p0}, Lcom/pspdfkit/forms/PushButtonFormConfiguration;-><init>(Lcom/pspdfkit/forms/PushButtonFormConfiguration$Builder;)V

    return-object v0
.end method

.method public setAction(Lcom/pspdfkit/annotations/actions/Action;)Lcom/pspdfkit/forms/PushButtonFormConfiguration$Builder;
    .locals 2

    const-string v0, "action"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/forms/PushButtonFormConfiguration$Builder;->h:Lcom/pspdfkit/annotations/actions/Action;

    return-object p0
.end method

.class Lcom/pspdfkit/forms/FormProviderImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/uf;


# instance fields
.field private final a:Lcom/pspdfkit/internal/zf;

.field private final b:Lcom/pspdfkit/internal/jni/NativeFormManager;

.field private final c:I

.field private final d:Lcom/pspdfkit/internal/bc;

.field private e:Lcom/pspdfkit/internal/sb;

.field private final f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final g:Ljava/util/ArrayList;


# direct methods
.method public static synthetic $r8$lambda$-ZkXWaJDRcBGzSBnHRfGbBLNJo4(Lcom/pspdfkit/forms/FormProviderImpl;Ljava/lang/String;Lcom/pspdfkit/forms/FormElementConfiguration;)Lcom/pspdfkit/forms/FormField;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/forms/FormProviderImpl;->a(Ljava/lang/String;Lcom/pspdfkit/forms/FormElementConfiguration;)Lcom/pspdfkit/forms/FormField;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$-ye8v41tgZP6ncT8z9JlPka_Rfk(Lcom/pspdfkit/forms/FormProviderImpl;Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/forms/FormProviderImpl;->a(Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$6tcknSaTjaEbbz407hQVJl86ylg(Lcom/pspdfkit/forms/FormProviderImpl;Ljava/lang/String;)Lcom/pspdfkit/forms/FormField;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/forms/FormProviderImpl;->b(Ljava/lang/String;)Lcom/pspdfkit/forms/FormField;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$EI7RaPFN--BscyhcR7sZazsi2yg(Lcom/pspdfkit/forms/FormProviderImpl;Ljava/lang/String;)Lcom/pspdfkit/forms/FormElement;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/forms/FormProviderImpl;->a(Ljava/lang/String;)Lcom/pspdfkit/forms/FormElement;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$cFJTXgAr0UYrfY-EtDQuaZmACDw(Lcom/pspdfkit/forms/FormProviderImpl;Ljava/lang/String;Ljava/util/List;)Lcom/pspdfkit/forms/FormField;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/forms/FormProviderImpl;->a(Ljava/lang/String;Ljava/util/List;)Lcom/pspdfkit/forms/FormField;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$h8ZpXWRbQNBxnBybd0V_QD1a4Rs(Lcom/pspdfkit/forms/FormProviderImpl;Lcom/pspdfkit/forms/FormElement;)Ljava/lang/Boolean;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/forms/FormProviderImpl;->a(Lcom/pspdfkit/forms/FormElement;)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method constructor <init>(Lcom/pspdfkit/internal/zf;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->g:Ljava/util/ArrayList;

    .line 14
    iput-object p1, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->getDocumentSources()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->c:I

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeFormManager;->create(Lcom/pspdfkit/internal/jni/NativeDocument;)Lcom/pspdfkit/internal/jni/NativeFormManager;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->b:Lcom/pspdfkit/internal/jni/NativeFormManager;

    .line 17
    new-instance v1, Lcom/pspdfkit/internal/bc;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/bc;-><init>(Lcom/pspdfkit/internal/uf;Lcom/pspdfkit/internal/zf;)V

    iput-object v1, p0, Lcom/pspdfkit/forms/FormProviderImpl;->d:Lcom/pspdfkit/internal/bc;

    .line 18
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object p1

    sget-object v2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ACRO_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 19
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeFormManager;->registerFormObserver(Lcom/pspdfkit/internal/jni/NativeFormObserver;)V

    :cond_0
    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/forms/FormProviderImpl;->getFormElementForAnnotation(Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Ljava/lang/String;)Lcom/pspdfkit/forms/FormElement;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 5
    invoke-virtual {p0, p1}, Lcom/pspdfkit/forms/FormProviderImpl;->getFormElementWithName(Ljava/lang/String;)Lcom/pspdfkit/forms/FormElement;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Ljava/lang/String;Lcom/pspdfkit/forms/FormElementConfiguration;)Lcom/pspdfkit/forms/FormField;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 2
    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/forms/FormProviderImpl;->addFormElementsToPage(Ljava/lang/String;Ljava/util/List;)Lcom/pspdfkit/forms/FormField;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Ljava/lang/String;Ljava/util/List;)Lcom/pspdfkit/forms/FormField;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/forms/FormProviderImpl;->addFormElementsToPage(Ljava/lang/String;Ljava/util/List;)Lcom/pspdfkit/forms/FormField;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Lcom/pspdfkit/forms/FormElement;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 3
    invoke-virtual {p0, p1}, Lcom/pspdfkit/forms/FormProviderImpl;->removeFormElementFromPage(Lcom/pspdfkit/forms/FormElement;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method private static a()V
    .locals 2

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ACRO_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 7
    :cond_0
    new-instance v0, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v1, "Your license does not allow forms display and editing."

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private synthetic b(Ljava/lang/String;)Lcom/pspdfkit/forms/FormField;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/forms/FormProviderImpl;->getFormFieldWithFullyQualifiedName(Ljava/lang/String;)Lcom/pspdfkit/forms/FormField;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public addFormElementToPage(Ljava/lang/String;Lcom/pspdfkit/forms/FormElementConfiguration;)Lcom/pspdfkit/forms/FormField;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/pspdfkit/forms/FormElementConfiguration;",
            ">(",
            "Ljava/lang/String;",
            "TT;)",
            "Lcom/pspdfkit/forms/FormField;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    const-string v0, "fullyQualifiedName"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "formElementConfiguration"

    .line 54
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 106
    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/forms/FormProviderImpl;->addFormElementsToPage(Ljava/lang/String;Ljava/util/List;)Lcom/pspdfkit/forms/FormField;

    move-result-object p1

    return-object p1
.end method

.method public addFormElementToPageAsync(Ljava/lang/String;Lcom/pspdfkit/forms/FormElementConfiguration;)Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/pspdfkit/forms/FormElementConfiguration;",
            ">(",
            "Ljava/lang/String;",
            "TT;)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/forms/FormField;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    const-string v0, "fullyQualifiedName"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "formElementConfiguration"

    .line 54
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 106
    new-instance v0, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/forms/FormProviderImpl;Ljava/lang/String;Lcom/pspdfkit/forms/FormElementConfiguration;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x5

    .line 108
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public addFormElementsToPage(Ljava/lang/String;Ljava/util/List;)Lcom/pspdfkit/forms/FormField;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/pspdfkit/forms/FormElementConfiguration;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "TT;>;)",
            "Lcom/pspdfkit/forms/FormField;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    const-string v0, "fullyQualifiedName"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 57
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_4

    .line 62
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/forms/FormElementConfiguration;

    invoke-virtual {v3}, Lcom/pspdfkit/forms/FormElementConfiguration;->a()Lcom/pspdfkit/forms/FormType;

    move-result-object v3

    .line 63
    sget-object v4, Lcom/pspdfkit/forms/FormType;->UNDEFINED:Lcom/pspdfkit/forms/FormType;

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-le v0, v4, :cond_2

    if-lez v2, :cond_2

    add-int/lit8 v4, v2, -0x1

    .line 70
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/forms/FormElementConfiguration;

    invoke-virtual {v5}, Lcom/pspdfkit/forms/FormElementConfiguration;->a()Lcom/pspdfkit/forms/FormType;

    move-result-object v5

    if-ne v3, v5, :cond_1

    .line 74
    iget-object v3, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/forms/FormElementConfiguration;

    .line 75
    iget v5, v5, Lcom/pspdfkit/forms/FormElementConfiguration;->a:I

    .line 76
    invoke-virtual {v3, v5}, Lcom/pspdfkit/internal/zf;->g(I)I

    move-result v3

    iget-object v5, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    .line 78
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/forms/FormElementConfiguration;

    .line 79
    iget v4, v4, Lcom/pspdfkit/forms/FormElementConfiguration;->a:I

    .line 80
    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/zf;->g(I)I

    move-result v4

    if-ne v3, v4, :cond_0

    goto :goto_1

    .line 83
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "All form annotations to add must be in the same document provider"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 84
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Form elements children of the same form field need to be the same type."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 85
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Form elements with an undefined type cannot create a form field."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 106
    :cond_4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/forms/FormProviderImpl;->getFormFieldWithFullyQualifiedName(Ljava/lang/String;)Lcom/pspdfkit/forms/FormField;

    move-result-object v2

    if-nez v2, :cond_a

    .line 111
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 112
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 113
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 114
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v0, :cond_6

    .line 117
    new-instance v7, Lcom/pspdfkit/annotations/WidgetAnnotation;

    .line 118
    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/pspdfkit/forms/FormElementConfiguration;

    .line 119
    iget v8, v8, Lcom/pspdfkit/forms/FormElementConfiguration;->a:I

    .line 120
    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/pspdfkit/forms/FormElementConfiguration;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 121
    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10}, Landroid/graphics/RectF;-><init>()V

    .line 122
    iget-object v9, v9, Lcom/pspdfkit/forms/FormElementConfiguration;->b:Landroid/graphics/RectF;

    invoke-virtual {v10, v9}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 123
    invoke-direct {v7, v8, v10}, Lcom/pspdfkit/annotations/WidgetAnnotation;-><init>(ILandroid/graphics/RectF;)V

    .line 124
    iget-object v8, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v8}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v8

    check-cast v8, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v8, v7}, Lcom/pspdfkit/internal/r1;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 125
    invoke-virtual {v7}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v8

    invoke-interface {v8}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/forms/FormElementConfiguration;

    invoke-virtual {v7, v6}, Lcom/pspdfkit/forms/FormElementConfiguration;->a(I)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_5

    const-string v7, ""

    goto :goto_3

    .line 130
    :cond_5
    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/forms/FormElementConfiguration;

    invoke-virtual {v7, v6}, Lcom/pspdfkit/forms/FormElementConfiguration;->a(I)Ljava/lang/String;

    move-result-object v7

    .line 131
    :goto_3
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 137
    :cond_6
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/forms/FormElementConfiguration;

    invoke-virtual {v6}, Lcom/pspdfkit/forms/FormElementConfiguration;->a()Lcom/pspdfkit/forms/FormType;

    move-result-object v6

    .line 138
    iget-object v7, p0, Lcom/pspdfkit/forms/FormProviderImpl;->b:Lcom/pspdfkit/internal/jni/NativeFormManager;

    .line 139
    const-class v8, Lcom/pspdfkit/internal/jni/NativeFormType;

    invoke-static {v8, v6}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v8

    check-cast v8, Lcom/pspdfkit/internal/jni/NativeFormType;

    .line 140
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 141
    invoke-virtual {v7, v8, p1, v2, v9}, Lcom/pspdfkit/internal/jni/NativeFormManager;->createAndInsertFormField(Lcom/pspdfkit/internal/jni/NativeFormType;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/pspdfkit/internal/jni/NativeFormFieldCreationResult;

    move-result-object p1

    .line 148
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeFormFieldCreationResult;->getCreatedFormField()Lcom/pspdfkit/internal/jni/NativeFormField;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 152
    iget-object v2, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    .line 153
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/forms/FormElementConfiguration;

    .line 154
    iget v5, v5, Lcom/pspdfkit/forms/FormElementConfiguration;->a:I

    .line 155
    invoke-virtual {v2, v5}, Lcom/pspdfkit/internal/zf;->g(I)I

    move-result v2

    .line 156
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeFormFieldCreationResult;->getCreatedFormField()Lcom/pspdfkit/internal/jni/NativeFormField;

    move-result-object p1

    .line 157
    sget-object v5, Lcom/pspdfkit/forms/FormProviderImpl$1;->a:[I

    invoke-virtual {v6}, Ljava/lang/Enum;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 174
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Cannot create a form field with an undefined type."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 175
    :pswitch_0
    new-instance v5, Lcom/pspdfkit/forms/TextFormField;

    invoke-direct {v5, v2, p1}, Lcom/pspdfkit/forms/TextFormField;-><init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    goto :goto_4

    .line 176
    :pswitch_1
    new-instance v5, Lcom/pspdfkit/forms/SignatureFormField;

    iget-object v6, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    invoke-direct {v5, v6, v2, p1}, Lcom/pspdfkit/forms/SignatureFormField;-><init>(Lcom/pspdfkit/internal/zf;ILcom/pspdfkit/internal/jni/NativeFormField;)V

    goto :goto_4

    .line 177
    :pswitch_2
    new-instance v5, Lcom/pspdfkit/forms/RadioButtonFormField;

    invoke-direct {v5, v2, p1}, Lcom/pspdfkit/forms/RadioButtonFormField;-><init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    goto :goto_4

    .line 178
    :pswitch_3
    new-instance v5, Lcom/pspdfkit/forms/PushButtonFormField;

    invoke-direct {v5, v2, p1}, Lcom/pspdfkit/forms/PushButtonFormField;-><init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    goto :goto_4

    .line 179
    :pswitch_4
    new-instance v5, Lcom/pspdfkit/forms/ListBoxFormField;

    invoke-direct {v5, v2, p1}, Lcom/pspdfkit/forms/ListBoxFormField;-><init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    goto :goto_4

    .line 180
    :pswitch_5
    new-instance v5, Lcom/pspdfkit/forms/ComboBoxFormField;

    invoke-direct {v5, v2, p1}, Lcom/pspdfkit/forms/ComboBoxFormField;-><init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    goto :goto_4

    .line 181
    :pswitch_6
    new-instance v5, Lcom/pspdfkit/forms/CheckBoxFormField;

    invoke-direct {v5, v2, p1}, Lcom/pspdfkit/forms/CheckBoxFormField;-><init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    :goto_4
    if-ge v1, v0, :cond_7

    .line 182
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/forms/FormElementConfiguration;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-virtual {v2, v5, v6}, Lcom/pspdfkit/forms/FormElementConfiguration;->a(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 185
    :cond_7
    invoke-virtual {v5, v4}, Lcom/pspdfkit/forms/FormField;->a(Ljava/util/List;)V

    .line 190
    invoke-virtual {v5}, Lcom/pspdfkit/forms/FormField;->a()I

    move-result p2

    invoke-virtual {p0, p2, p1}, Lcom/pspdfkit/forms/FormProviderImpl;->onFormFieldAdded(ILcom/pspdfkit/internal/jni/NativeFormField;)Lcom/pspdfkit/forms/FormField;

    move-result-object p1

    if-eqz p1, :cond_8

    move-object v5, p1

    .line 197
    :cond_8
    iget-object p1, p0, Lcom/pspdfkit/forms/FormProviderImpl;->d:Lcom/pspdfkit/internal/bc;

    invoke-virtual {p1, v5}, Lcom/pspdfkit/internal/bc;->a(Lcom/pspdfkit/forms/FormField;)V

    return-object v5

    .line 198
    :cond_9
    new-instance p2, Lcom/pspdfkit/forms/exceptions/FormCreationFailedException;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeFormFieldCreationResult;->getErrorMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/pspdfkit/forms/exceptions/FormCreationFailedException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 199
    :cond_a
    new-instance p2, Ljava/lang/IllegalArgumentException;

    const-string v0, "Form element with this fully qualified name already exists: "

    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 200
    :cond_b
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Form element list must not be empty."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public addFormElementsToPageAsync(Ljava/lang/String;Ljava/util/List;)Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/pspdfkit/forms/FormElementConfiguration;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "TT;>;)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/forms/FormField;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    const-string v0, "fullyQualifiedName"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "formElementConfigurations"

    .line 54
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 106
    new-instance v0, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/forms/FormProviderImpl;Ljava/lang/String;Ljava/util/List;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x5

    .line 107
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public addOnButtonFormFieldUpdatedListener(Lcom/pspdfkit/forms/FormListeners$OnButtonFormFieldUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->d:Lcom/pspdfkit/internal/bc;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/bc;->a(Lcom/pspdfkit/forms/FormListeners$OnButtonFormFieldUpdatedListener;)V

    return-void
.end method

.method public addOnChoiceFormFieldUpdatedListener(Lcom/pspdfkit/forms/FormListeners$OnChoiceFormFieldUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->d:Lcom/pspdfkit/internal/bc;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/bc;->a(Lcom/pspdfkit/forms/FormListeners$OnChoiceFormFieldUpdatedListener;)V

    return-void
.end method

.method public addOnFormFieldUpdatedListener(Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->d:Lcom/pspdfkit/internal/bc;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/bc;->a(Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;)V

    return-void
.end method

.method public addOnFormTabOrderUpdatedListener(Lcom/pspdfkit/forms/FormListeners$OnFormTabOrderUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->d:Lcom/pspdfkit/internal/bc;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/bc;->a(Lcom/pspdfkit/forms/FormListeners$OnFormTabOrderUpdatedListener;)V

    return-void
.end method

.method public addOnTextFormFieldUpdatedListener(Lcom/pspdfkit/forms/FormListeners$OnTextFormFieldUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->d:Lcom/pspdfkit/internal/bc;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/bc;->a(Lcom/pspdfkit/forms/FormListeners$OnTextFormFieldUpdatedListener;)V

    return-void
.end method

.method public attachFormElement(Lcom/pspdfkit/forms/FormField;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/forms/FormField;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormElement;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p1, p2}, Lcom/pspdfkit/forms/FormField;->a(Ljava/util/List;)V

    return-void
.end method

.method public createFormElement(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/forms/FormProviderImpl$1;->a:[I

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 18
    new-instance v0, Lcom/pspdfkit/forms/UnknownFormElement;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/UnknownFormElement;-><init>(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-object v0

    .line 19
    :pswitch_0
    new-instance v0, Lcom/pspdfkit/forms/TextFormElement;

    check-cast p1, Lcom/pspdfkit/forms/TextFormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/TextFormElement;-><init>(Lcom/pspdfkit/forms/TextFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-object v0

    .line 31
    :pswitch_1
    new-instance v0, Lcom/pspdfkit/forms/SignatureFormElement;

    check-cast p1, Lcom/pspdfkit/forms/SignatureFormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/SignatureFormElement;-><init>(Lcom/pspdfkit/forms/SignatureFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-object v0

    .line 32
    :pswitch_2
    new-instance v0, Lcom/pspdfkit/forms/RadioButtonFormElement;

    check-cast p1, Lcom/pspdfkit/forms/RadioButtonFormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/RadioButtonFormElement;-><init>(Lcom/pspdfkit/forms/RadioButtonFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-object v0

    .line 33
    :pswitch_3
    new-instance v0, Lcom/pspdfkit/forms/PushButtonFormElement;

    check-cast p1, Lcom/pspdfkit/forms/PushButtonFormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/PushButtonFormElement;-><init>(Lcom/pspdfkit/forms/PushButtonFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-object v0

    .line 39
    :pswitch_4
    new-instance v0, Lcom/pspdfkit/forms/ListBoxFormElement;

    check-cast p1, Lcom/pspdfkit/forms/ListBoxFormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/ListBoxFormElement;-><init>(Lcom/pspdfkit/forms/ListBoxFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-object v0

    .line 41
    :pswitch_5
    new-instance v0, Lcom/pspdfkit/forms/ComboBoxFormElement;

    check-cast p1, Lcom/pspdfkit/forms/ComboBoxFormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/ComboBoxFormElement;-><init>(Lcom/pspdfkit/forms/ComboBoxFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-object v0

    .line 42
    :pswitch_6
    new-instance v0, Lcom/pspdfkit/forms/CheckBoxFormElement;

    check-cast p1, Lcom/pspdfkit/forms/CheckBoxFormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/CheckBoxFormElement;-><init>(Lcom/pspdfkit/forms/CheckBoxFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public createFormField(ILcom/pspdfkit/internal/jni/NativeFormField;)Lcom/pspdfkit/forms/FormField;
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/forms/FormProviderImpl$1;->b:[I

    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeFormField;->getType()Lcom/pspdfkit/internal/jni/NativeFormType;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 18
    new-instance v0, Lcom/pspdfkit/forms/FormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/FormField;-><init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    return-object v0

    .line 19
    :pswitch_0
    new-instance v0, Lcom/pspdfkit/forms/SignatureFormField;

    iget-object v1, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    invoke-direct {v0, v1, p1, p2}, Lcom/pspdfkit/forms/SignatureFormField;-><init>(Lcom/pspdfkit/internal/zf;ILcom/pspdfkit/internal/jni/NativeFormField;)V

    return-object v0

    .line 20
    :pswitch_1
    new-instance v0, Lcom/pspdfkit/forms/ComboBoxFormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/ComboBoxFormField;-><init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    return-object v0

    .line 21
    :pswitch_2
    new-instance v0, Lcom/pspdfkit/forms/ListBoxFormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/ListBoxFormField;-><init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    return-object v0

    .line 22
    :pswitch_3
    new-instance v0, Lcom/pspdfkit/forms/CheckBoxFormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/CheckBoxFormField;-><init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    return-object v0

    .line 23
    :pswitch_4
    new-instance v0, Lcom/pspdfkit/forms/RadioButtonFormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/RadioButtonFormField;-><init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    return-object v0

    .line 24
    :pswitch_5
    new-instance v0, Lcom/pspdfkit/forms/PushButtonFormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/PushButtonFormField;-><init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    return-object v0

    .line 25
    :pswitch_6
    new-instance v0, Lcom/pspdfkit/forms/TextFormField;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/TextFormField;-><init>(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getFormCache()Lcom/pspdfkit/internal/sb;
    .locals 4

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->e:Lcom/pspdfkit/internal/sb;

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/sb;

    iget-object v1, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    iget-object v2, p0, Lcom/pspdfkit/forms/FormProviderImpl;->b:Lcom/pspdfkit/internal/jni/NativeFormManager;

    invoke-direct {v0, p0, v1, v2}, Lcom/pspdfkit/internal/sb;-><init>(Lcom/pspdfkit/internal/uf;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/jni/NativeFormManager;)V

    iput-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->e:Lcom/pspdfkit/internal/sb;

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ig;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ig;->a()Z

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/forms/FormProviderImpl;->e:Lcom/pspdfkit/internal/sb;

    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/pspdfkit/internal/jni/NativeFormField;

    invoke-virtual {v2, v3, v1}, Lcom/pspdfkit/internal/sb;->a(ILcom/pspdfkit/internal/jni/NativeFormField;)Lcom/pspdfkit/forms/FormField;

    goto :goto_0

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->e:Lcom/pspdfkit/internal/sb;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 14
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getFormElementForAnnotation(Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotation"

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    monitor-enter p0

    .line 55
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormProviderImpl;->getFormCache()Lcom/pspdfkit/internal/sb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/sb;->a(Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;

    move-result-object p1

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 56
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getFormElementForAnnotationAsync(Lcom/pspdfkit/annotations/WidgetAnnotation;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/WidgetAnnotation;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/forms/FormElement;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda7;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/forms/FormProviderImpl;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 55
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public getFormElementWithName(Ljava/lang/String;)Lcom/pspdfkit/forms/FormElement;
    .locals 4

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    const-string v0, "fieldName"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormProviderImpl;->getFormElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/forms/FormElement;

    .line 55
    invoke-virtual {v2}, Lcom/pspdfkit/forms/FormElement;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    return-object v2

    :cond_1
    return-object v1
.end method

.method public getFormElementWithNameAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/forms/FormElement;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    const-string v0, "fieldName"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda8;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/forms/FormProviderImpl;Ljava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 55
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public getFormElements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormElement;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    .line 2
    monitor-enter p0

    .line 3
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormProviderImpl;->getFormCache()Lcom/pspdfkit/internal/sb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sb;->c()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getFormElementsAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormElement;",
            ">;>;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    .line 2
    new-instance v0, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0}, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/forms/FormProviderImpl;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    const/4 v2, 0x5

    .line 3
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public getFormFieldWithFullyQualifiedName(Ljava/lang/String;)Lcom/pspdfkit/forms/FormField;
    .locals 3

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    const-string v0, "fullyQualifiedName"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v0, 0x0

    .line 54
    :goto_0
    iget v2, p0, Lcom/pspdfkit/forms/FormProviderImpl;->c:I

    if-ge v0, v2, :cond_1

    .line 55
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormProviderImpl;->getFormCache()Lcom/pspdfkit/internal/sb;

    move-result-object v2

    invoke-virtual {v2, v0, p1}, Lcom/pspdfkit/internal/sb;->a(ILjava/lang/String;)Lcom/pspdfkit/forms/FormField;

    move-result-object v2

    if-eqz v2, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public getFormFieldWithFullyQualifiedNameAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/forms/FormField;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    const-string v0, "fullyQualifiedName"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/forms/FormProviderImpl;Ljava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 55
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public getFormFields()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormField;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    .line 2
    monitor-enter p0

    .line 3
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormProviderImpl;->getFormCache()Lcom/pspdfkit/internal/sb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sb;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getFormFieldsAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormField;",
            ">;>;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    .line 2
    new-instance v0, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/forms/FormProviderImpl;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    const/4 v2, 0x5

    .line 3
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public getTabOrder()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormElement;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    .line 2
    monitor-enter p0

    .line 3
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormProviderImpl;->getFormCache()Lcom/pspdfkit/internal/sb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sb;->e()Ljava/util/ArrayList;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getTabOrderAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormElement;",
            ">;>;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    .line 2
    new-instance v0, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0}, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/forms/FormProviderImpl;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    const/4 v2, 0x5

    .line 3
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public hasFieldsCache()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->e:Lcom/pspdfkit/internal/sb;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasUnsavedChanges()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public markFormAsSavedToDisk()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/forms/FormProviderImpl;->setDirty(Z)V

    return-void
.end method

.method public declared-synchronized onFormFieldAdded(ILcom/pspdfkit/internal/jni/NativeFormField;)Lcom/pspdfkit/forms/FormField;
    .locals 2

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->e:Lcom/pspdfkit/internal/sb;

    if-nez v0, :cond_0

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->g:Ljava/util/ArrayList;

    new-instance v1, Landroid/util/Pair;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x0

    monitor-exit p0

    return-object p1

    .line 11
    :cond_0
    :try_start_1
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/sb;->a(ILcom/pspdfkit/internal/jni/NativeFormField;)Lcom/pspdfkit/forms/FormField;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public prepareFieldsCache()Lio/reactivex/rxjava3/core/Completable;
    .locals 3

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    .line 2
    new-instance v0, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/forms/FormProviderImpl;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    const/4 v2, 0x1

    .line 3
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    return-object v0
.end method

.method public removeFormElementFromPage(Lcom/pspdfkit/forms/FormElement;)Z
    .locals 4

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    const-string v0, "formElement"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getFormField()Lcom/pspdfkit/forms/FormField;

    move-result-object v0

    .line 56
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/forms/FormType;->SIGNATURE:Lcom/pspdfkit/forms/FormType;

    if-ne v1, v2, :cond_0

    .line 57
    move-object v1, v0

    check-cast v1, Lcom/pspdfkit/forms/SignatureFormField;

    .line 58
    invoke-virtual {v1}, Lcom/pspdfkit/forms/SignatureFormField;->removeSignature()V

    .line 59
    iget-object v2, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getDocumentSignatureInfo()Lcom/pspdfkit/signatures/DocumentSignatureInfo;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/pspdfkit/signatures/DocumentSignatureInfo;->removeSignatureFormField(Lcom/pspdfkit/forms/SignatureFormField;)V

    .line 62
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/tf;->getNativeFormField()Lcom/pspdfkit/internal/jni/NativeFormField;

    move-result-object v1

    .line 64
    iget-object v2, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v2

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object p1

    invoke-interface {v2, p1}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 66
    iget-object p1, p0, Lcom/pspdfkit/forms/FormProviderImpl;->b:Lcom/pspdfkit/internal/jni/NativeFormManager;

    new-instance v2, Ljava/util/ArrayList;

    .line 67
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/jni/NativeFormManager;->removeFormFields(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/jni/NativeFormRemovalResult;

    move-result-object p1

    .line 69
    iget-object v2, p0, Lcom/pspdfkit/forms/FormProviderImpl;->e:Lcom/pspdfkit/internal/sb;

    if-eqz v2, :cond_1

    .line 70
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->a()I

    move-result v3

    invoke-virtual {v2, v3, v1}, Lcom/pspdfkit/internal/sb;->b(ILcom/pspdfkit/internal/jni/NativeFormField;)V

    .line 73
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/forms/FormProviderImpl;->d:Lcom/pspdfkit/internal/bc;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/bc;->a(Lcom/pspdfkit/forms/FormField;)V

    .line 75
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeFormRemovalResult;->getHasError()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public removeFormElementFromPageAsync(Lcom/pspdfkit/forms/FormElement;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/forms/FormElement;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/forms/FormProviderImpl;->a()V

    const-string v0, "formElement"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda9;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/forms/FormProviderImpl$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/forms/FormProviderImpl;Lcom/pspdfkit/forms/FormElement;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 55
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public removeOnButtonFormFieldUpdatedListener(Lcom/pspdfkit/forms/FormListeners$OnButtonFormFieldUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->d:Lcom/pspdfkit/internal/bc;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/bc;->b(Lcom/pspdfkit/forms/FormListeners$OnButtonFormFieldUpdatedListener;)V

    return-void
.end method

.method public removeOnChoiceFormFieldUpdatedListener(Lcom/pspdfkit/forms/FormListeners$OnChoiceFormFieldUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->d:Lcom/pspdfkit/internal/bc;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/bc;->b(Lcom/pspdfkit/forms/FormListeners$OnChoiceFormFieldUpdatedListener;)V

    return-void
.end method

.method public removeOnFormFieldUpdatedListener(Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->d:Lcom/pspdfkit/internal/bc;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/bc;->b(Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;)V

    return-void
.end method

.method public removeOnFormTabOrderUpdatedListener(Lcom/pspdfkit/forms/FormListeners$OnFormTabOrderUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->d:Lcom/pspdfkit/internal/bc;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/bc;->b(Lcom/pspdfkit/forms/FormListeners$OnFormTabOrderUpdatedListener;)V

    return-void
.end method

.method public removeOnTextFormFieldUpdatedListener(Lcom/pspdfkit/forms/FormListeners$OnTextFormFieldUpdatedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->d:Lcom/pspdfkit/internal/bc;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/bc;->b(Lcom/pspdfkit/forms/FormListeners$OnTextFormFieldUpdatedListener;)V

    return-void
.end method

.method public resetFormFields(Ljava/util/List;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormField;",
            ">;Z)V"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/forms/FormField;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/tf;->getNativeFormField()Lcom/pspdfkit/internal/jni/NativeFormField;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 8
    sget-object p1, Lcom/pspdfkit/internal/jni/NativeFormResetFlags;->INCLUDE_EXCLUDE:Lcom/pspdfkit/internal/jni/NativeFormResetFlags;

    invoke-static {p1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object p1

    goto :goto_1

    .line 10
    :cond_1
    const-class p1, Lcom/pspdfkit/internal/jni/NativeFormResetFlags;

    invoke-static {p1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object p1

    .line 13
    :goto_1
    iget-object p2, p0, Lcom/pspdfkit/forms/FormProviderImpl;->b:Lcom/pspdfkit/internal/jni/NativeFormManager;

    invoke-virtual {p2, v0, p1}, Lcom/pspdfkit/internal/jni/NativeFormManager;->resetForm(Ljava/util/ArrayList;Ljava/util/EnumSet;)Lcom/pspdfkit/internal/jni/NativeFormResetResult;

    return-void
.end method

.method public setDirty(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormProviderImpl;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method public syncDirtyWidgetAnnotationsToNative()V
    .locals 3

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormProviderImpl;->hasFieldsCache()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    monitor-exit p0

    return-void

    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormProviderImpl;->getFormCache()Lcom/pspdfkit/internal/sb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sb;->c()Ljava/util/ArrayList;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/forms/FormElement;

    .line 7
    invoke-virtual {v1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    .line 8
    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->needsSyncingWithCore()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    .line 9
    invoke-interface {v1, v2}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached(Z)Z

    goto :goto_0

    .line 12
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

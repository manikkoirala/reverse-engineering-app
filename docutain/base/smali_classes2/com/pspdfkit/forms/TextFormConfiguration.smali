.class public Lcom/pspdfkit/forms/TextFormConfiguration;
.super Lcom/pspdfkit/forms/FormElementConfiguration;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/forms/TextFormConfiguration$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/forms/FormElementConfiguration<",
        "Lcom/pspdfkit/forms/TextFormElement;",
        "Lcom/pspdfkit/forms/TextFormField;",
        ">;"
    }
.end annotation


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeFormTextFlags;",
            ">;"
        }
    .end annotation
.end field

.field private final j:I


# direct methods
.method private constructor <init>(Lcom/pspdfkit/forms/TextFormConfiguration$Builder;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/forms/FormElementConfiguration;-><init>(Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;)V

    .line 2
    iget-object v0, p1, Lcom/pspdfkit/forms/TextFormConfiguration$Builder;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->g:Ljava/lang/String;

    .line 3
    iget-object v0, p1, Lcom/pspdfkit/forms/TextFormConfiguration$Builder;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->h:Ljava/lang/String;

    .line 4
    iget-object v0, p1, Lcom/pspdfkit/forms/TextFormConfiguration$Builder;->j:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->i:Ljava/util/EnumSet;

    .line 5
    iget p1, p1, Lcom/pspdfkit/forms/TextFormConfiguration$Builder;->i:I

    iput p1, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->j:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/forms/TextFormConfiguration$Builder;Lcom/pspdfkit/forms/TextFormConfiguration-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/forms/TextFormConfiguration;-><init>(Lcom/pspdfkit/forms/TextFormConfiguration$Builder;)V

    return-void
.end method


# virtual methods
.method final a(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/forms/TextFormField;

    .line 2
    new-instance v0, Lcom/pspdfkit/forms/TextFormElement;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/TextFormElement;-><init>(Lcom/pspdfkit/forms/TextFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/forms/FormElementConfiguration;->a(Lcom/pspdfkit/forms/FormElement;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->g:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 6
    invoke-virtual {v0, p1}, Lcom/pspdfkit/forms/TextFormElement;->setText(Ljava/lang/String;)Z

    .line 8
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->h:Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 9
    invoke-virtual {v0, p1}, Lcom/pspdfkit/forms/TextFormElement;->setRichText(Ljava/lang/String;)V

    .line 12
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/forms/TextFormElement;->getFormField()Lcom/pspdfkit/forms/TextFormField;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object p1

    .line 13
    iget-object p2, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->i:Ljava/util/EnumSet;

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/tf;->setTextFlags(Ljava/util/EnumSet;)V

    .line 14
    invoke-interface {p1}, Lcom/pspdfkit/internal/tf;->getNativeFormControl()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object p1

    iget p2, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->j:I

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/jni/NativeFormControl;->setMaxLength(I)V

    return-object v0
.end method

.method final a()Lcom/pspdfkit/forms/FormType;
    .locals 1

    .line 15
    sget-object v0, Lcom/pspdfkit/forms/FormType;->TEXT:Lcom/pspdfkit/forms/FormType;

    return-object v0
.end method

.method final a(I)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getMaxLength()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->j:I

    return v0
.end method

.method public getRichText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->g:Ljava/lang/String;

    return-object v0
.end method

.method public isMultiLine()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->i:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->MULTI_LINE:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isPassword()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->i:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->PASSWORD:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSpellCheckEnabled()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/TextFormConfiguration;->i:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->DO_NOT_SPELL_CHECK:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

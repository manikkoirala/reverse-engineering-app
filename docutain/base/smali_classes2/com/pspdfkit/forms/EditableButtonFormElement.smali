.class public abstract Lcom/pspdfkit/forms/EditableButtonFormElement;
.super Lcom/pspdfkit/forms/ButtonFormElement;
.source "SourceFile"


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/EditableButtonFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/forms/ButtonFormElement;-><init>(Lcom/pspdfkit/forms/ButtonFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-void
.end method


# virtual methods
.method public deselect()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeFormControl;->deselectButton(I)Z

    move-result v0

    return v0
.end method

.method public isSelected()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeFormControl;->isButtonSelected(I)Z

    move-result v0

    return v0
.end method

.method public select()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeFormControl;->selectButton(I)Z

    move-result v0

    return v0
.end method

.method public toggleSelection()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/EditableButtonFormElement;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/forms/EditableButtonFormElement;->deselect()Z

    move-result v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/forms/EditableButtonFormElement;->select()Z

    move-result v0

    :goto_0
    return v0
.end method

.class public Lcom/pspdfkit/forms/CheckBoxFormConfiguration;
.super Lcom/pspdfkit/forms/FormElementConfiguration;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/forms/CheckBoxFormConfiguration$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/forms/FormElementConfiguration<",
        "Lcom/pspdfkit/forms/CheckBoxFormElement;",
        "Lcom/pspdfkit/forms/CheckBoxFormField;",
        ">;"
    }
.end annotation


# instance fields
.field private final g:Z


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/CheckBoxFormConfiguration$Builder;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/forms/FormElementConfiguration;-><init>(Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;)V

    .line 2
    iget-boolean p1, p1, Lcom/pspdfkit/forms/CheckBoxFormConfiguration$Builder;->g:Z

    iput-boolean p1, p0, Lcom/pspdfkit/forms/CheckBoxFormConfiguration;->g:Z

    return-void
.end method


# virtual methods
.method final a(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/forms/CheckBoxFormField;

    .line 2
    new-instance v0, Lcom/pspdfkit/forms/CheckBoxFormElement;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/CheckBoxFormElement;-><init>(Lcom/pspdfkit/forms/CheckBoxFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/forms/FormElementConfiguration;->a(Lcom/pspdfkit/forms/FormElement;)V

    .line 5
    iget-boolean p1, p0, Lcom/pspdfkit/forms/CheckBoxFormConfiguration;->g:Z

    if-eqz p1, :cond_0

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/forms/EditableButtonFormElement;->select()Z

    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/forms/EditableButtonFormElement;->deselect()Z

    :goto_0
    return-object v0
.end method

.method final a()Lcom/pspdfkit/forms/FormType;
    .locals 1

    .line 9
    sget-object v0, Lcom/pspdfkit/forms/FormType;->CHECKBOX:Lcom/pspdfkit/forms/FormType;

    return-object v0
.end method

.method final a(I)Ljava/lang/String;
    .locals 2

    .line 10
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CheckBox-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public isSelected()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/forms/CheckBoxFormConfiguration;->g:Z

    return v0
.end method

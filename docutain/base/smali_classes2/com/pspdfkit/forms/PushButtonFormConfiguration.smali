.class public Lcom/pspdfkit/forms/PushButtonFormConfiguration;
.super Lcom/pspdfkit/forms/FormElementConfiguration;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/forms/PushButtonFormConfiguration$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/forms/FormElementConfiguration<",
        "Lcom/pspdfkit/forms/PushButtonFormElement;",
        "Lcom/pspdfkit/forms/PushButtonFormField;",
        ">;"
    }
.end annotation


# instance fields
.field final g:Landroid/graphics/Bitmap;

.field private final h:Lcom/pspdfkit/annotations/actions/Action;


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/PushButtonFormConfiguration$Builder;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/forms/FormElementConfiguration;-><init>(Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;)V

    .line 2
    iget-object v0, p1, Lcom/pspdfkit/forms/PushButtonFormConfiguration$Builder;->g:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/pspdfkit/forms/PushButtonFormConfiguration;->g:Landroid/graphics/Bitmap;

    .line 3
    iget-object p1, p1, Lcom/pspdfkit/forms/PushButtonFormConfiguration$Builder;->h:Lcom/pspdfkit/annotations/actions/Action;

    iput-object p1, p0, Lcom/pspdfkit/forms/PushButtonFormConfiguration;->h:Lcom/pspdfkit/annotations/actions/Action;

    return-void
.end method


# virtual methods
.method final a(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/forms/PushButtonFormField;

    .line 2
    new-instance v0, Lcom/pspdfkit/forms/PushButtonFormElement;

    iget-object v1, p0, Lcom/pspdfkit/forms/PushButtonFormConfiguration;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, p1, p2, v1}, Lcom/pspdfkit/forms/PushButtonFormElement;-><init>(Lcom/pspdfkit/forms/PushButtonFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;Landroid/graphics/Bitmap;)V

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/forms/FormElementConfiguration;->a(Lcom/pspdfkit/forms/FormElement;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/forms/PushButtonFormConfiguration;->h:Lcom/pspdfkit/annotations/actions/Action;

    if-eqz p1, :cond_0

    .line 6
    invoke-virtual {v0, p1}, Lcom/pspdfkit/forms/PushButtonFormElement;->setAction(Lcom/pspdfkit/annotations/actions/Action;)V

    :cond_0
    return-object v0
.end method

.method final a()Lcom/pspdfkit/forms/FormType;
    .locals 1

    .line 7
    sget-object v0, Lcom/pspdfkit/forms/FormType;->PUSHBUTTON:Lcom/pspdfkit/forms/FormType;

    return-object v0
.end method

.method final a(I)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getAction()Lcom/pspdfkit/annotations/actions/Action;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/PushButtonFormConfiguration;->h:Lcom/pspdfkit/annotations/actions/Action;

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/PushButtonFormConfiguration;->g:Landroid/graphics/Bitmap;

    invoke-static {v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

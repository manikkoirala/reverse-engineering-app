.class public Lcom/pspdfkit/forms/ListBoxFormConfiguration;
.super Lcom/pspdfkit/forms/FormElementConfiguration;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/forms/ListBoxFormConfiguration$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/forms/FormElementConfiguration<",
        "Lcom/pspdfkit/forms/ListBoxFormElement;",
        "Lcom/pspdfkit/forms/ListBoxFormField;",
        ">;"
    }
.end annotation


# instance fields
.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormOption;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Z


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/ListBoxFormConfiguration$Builder;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/forms/FormElementConfiguration;-><init>(Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;)V

    .line 2
    iget-object v0, p1, Lcom/pspdfkit/forms/ListBoxFormConfiguration$Builder;->g:Ljava/util/List;

    iput-object v0, p0, Lcom/pspdfkit/forms/ListBoxFormConfiguration;->g:Ljava/util/List;

    .line 3
    iget-object v0, p1, Lcom/pspdfkit/forms/ListBoxFormConfiguration$Builder;->h:Ljava/util/List;

    iput-object v0, p0, Lcom/pspdfkit/forms/ListBoxFormConfiguration;->h:Ljava/util/List;

    .line 4
    invoke-static {p1}, Lcom/pspdfkit/forms/ListBoxFormConfiguration$Builder;->-$$Nest$fgeti(Lcom/pspdfkit/forms/ListBoxFormConfiguration$Builder;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/forms/ListBoxFormConfiguration;->i:Z

    return-void
.end method


# virtual methods
.method final a(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/forms/ListBoxFormField;

    .line 2
    new-instance v0, Lcom/pspdfkit/forms/ListBoxFormElement;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/ListBoxFormElement;-><init>(Lcom/pspdfkit/forms/ListBoxFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/forms/FormElementConfiguration;->a(Lcom/pspdfkit/forms/FormElement;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/forms/ListBoxFormConfiguration;->h:Ljava/util/List;

    if-eqz p1, :cond_0

    .line 6
    invoke-virtual {v0, p1}, Lcom/pspdfkit/forms/ChoiceFormElement;->setOptions(Ljava/util/List;)V

    .line 8
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/forms/ListBoxFormConfiguration;->g:Ljava/util/List;

    if-eqz p1, :cond_1

    .line 9
    invoke-virtual {v0, p1}, Lcom/pspdfkit/forms/ChoiceFormElement;->setSelectedIndexes(Ljava/util/List;)V

    .line 11
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ListBoxFormConfiguration;->isMultiSelectionEnabled()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/forms/ListBoxFormElement;->getFormField()Lcom/pspdfkit/forms/ListBoxFormField;

    move-result-object p1

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object p1

    sget-object p2, Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;->MULTI_SELECT:Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;

    .line 15
    invoke-static {p2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/tf;->setChoiceFlags(Ljava/util/EnumSet;)V

    :cond_2
    return-object v0
.end method

.method final a()Lcom/pspdfkit/forms/FormType;
    .locals 1

    .line 16
    sget-object v0, Lcom/pspdfkit/forms/FormType;->LISTBOX:Lcom/pspdfkit/forms/FormType;

    return-object v0
.end method

.method final a(I)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormOption;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/ListBoxFormConfiguration;->h:Ljava/util/List;

    return-object v0
.end method

.method public getSelectedIndexes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/ListBoxFormConfiguration;->g:Ljava/util/List;

    return-object v0
.end method

.method public isMultiSelectionEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/forms/ListBoxFormConfiguration;->i:Z

    return v0
.end method

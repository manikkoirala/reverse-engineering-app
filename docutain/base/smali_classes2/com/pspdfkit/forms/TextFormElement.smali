.class public Lcom/pspdfkit/forms/TextFormElement;
.super Lcom/pspdfkit/forms/FormElement;
.source "SourceFile"


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/TextFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/forms/FormElement;-><init>(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-void
.end method

.method private c()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeFormTextFlags;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/TextFormElement;->getFormField()Lcom/pspdfkit/forms/TextFormField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getTextFlags()Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getEditingContents()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/TextFormElement;->getFormField()Lcom/pspdfkit/forms/TextFormField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getNativeFormField()Lcom/pspdfkit/internal/jni/NativeFormField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFormField;->getEditingContents()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getFormField()Lcom/pspdfkit/forms/FormField;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/TextFormElement;->getFormField()Lcom/pspdfkit/forms/TextFormField;

    move-result-object v0

    return-object v0
.end method

.method public getFormField()Lcom/pspdfkit/forms/TextFormField;
    .locals 1

    .line 2
    invoke-super {p0}, Lcom/pspdfkit/forms/FormElement;->getFormField()Lcom/pspdfkit/forms/FormField;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/forms/TextFormField;

    return-object v0
.end method

.method public getFormattedContents()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/TextFormElement;->getFormField()Lcom/pspdfkit/forms/TextFormField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getNativeFormField()Lcom/pspdfkit/internal/jni/NativeFormField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFormField;->getFormattedContents()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInputFormat()Lcom/pspdfkit/forms/TextInputFormat;
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/internal/vb;->a(Lcom/pspdfkit/forms/FormElement;)Lcom/pspdfkit/forms/TextInputFormat;

    move-result-object v0

    return-object v0
.end method

.method public getMaxLength()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFormControl;->getMaxLength()I

    move-result v0

    return v0
.end method

.method public getRichText()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFormControl;->getRichText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFormControl;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/pspdfkit/forms/FormType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/forms/FormType;->TEXT:Lcom/pspdfkit/forms/FormType;

    return-object v0
.end method

.method public isComb()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/forms/TextFormElement;->c()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->COMB:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isFileSelect()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/forms/TextFormElement;->c()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->FILE_SELECT:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isMultiLine()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/forms/TextFormElement;->c()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->MULTI_LINE:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isPassword()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/forms/TextFormElement;->c()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->PASSWORD:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isRichText()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/forms/TextFormElement;->c()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->RICH_TEXT:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isScrollEnabled()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/forms/TextFormElement;->c()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->DO_NOT_SCROLL:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isSpellCheckEnabled()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/forms/TextFormElement;->c()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormTextFlags;->DO_NOT_SPELL_CHECK:Lcom/pspdfkit/internal/jni/NativeFormTextFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public setRichText(Ljava/lang/String;)V
    .locals 2

    const-string v0, "richText"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeFormControl;->setRichText(Ljava/lang/String;)Z

    return-void
.end method

.method public setText(Ljava/lang/String;)Z
    .locals 2

    const-string v0, "text"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeFormControl;->setText(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.class public Lcom/pspdfkit/forms/ComboBoxFormConfiguration;
.super Lcom/pspdfkit/forms/FormElementConfiguration;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/forms/ComboBoxFormConfiguration$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/forms/FormElementConfiguration<",
        "Lcom/pspdfkit/forms/ComboBoxFormElement;",
        "Lcom/pspdfkit/forms/ComboBoxFormField;",
        ">;"
    }
.end annotation


# instance fields
.field private final g:Ljava/lang/Integer;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormOption;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/ComboBoxFormConfiguration$Builder;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/forms/FormElementConfiguration;-><init>(Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;)V

    .line 2
    iget-object v0, p1, Lcom/pspdfkit/forms/ComboBoxFormConfiguration$Builder;->g:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->g:Ljava/lang/Integer;

    .line 3
    iget-object v0, p1, Lcom/pspdfkit/forms/ComboBoxFormConfiguration$Builder;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->h:Ljava/lang/String;

    .line 4
    iget-object v0, p1, Lcom/pspdfkit/forms/ComboBoxFormConfiguration$Builder;->i:Ljava/util/List;

    iput-object v0, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->i:Ljava/util/List;

    .line 5
    iget-object p1, p1, Lcom/pspdfkit/forms/ComboBoxFormConfiguration$Builder;->j:Ljava/util/EnumSet;

    invoke-static {p1}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->j:Ljava/util/EnumSet;

    return-void
.end method


# virtual methods
.method final a(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/forms/ComboBoxFormField;

    .line 2
    new-instance v0, Lcom/pspdfkit/forms/ComboBoxFormElement;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/forms/ComboBoxFormElement;-><init>(Lcom/pspdfkit/forms/ComboBoxFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/forms/FormElementConfiguration;->a(Lcom/pspdfkit/forms/FormElement;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->i:Ljava/util/List;

    if-eqz p1, :cond_0

    .line 6
    invoke-virtual {v0, p1}, Lcom/pspdfkit/forms/ChoiceFormElement;->setOptions(Ljava/util/List;)V

    .line 8
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->h:Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->j:Ljava/util/EnumSet;

    sget-object p2, Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;->EDIT:Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;

    invoke-virtual {p1, p2}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 12
    invoke-virtual {v0}, Lcom/pspdfkit/forms/ComboBoxFormElement;->getFormField()Lcom/pspdfkit/forms/ComboBoxFormField;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->j:Ljava/util/EnumSet;

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/tf;->setChoiceFlags(Ljava/util/EnumSet;)V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->h:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/forms/ComboBoxFormElement;->setCustomText(Ljava/lang/String;)Z

    goto :goto_0

    .line 16
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/forms/ComboBoxFormElement;->getFormField()Lcom/pspdfkit/forms/ComboBoxFormField;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->j:Ljava/util/EnumSet;

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/tf;->setChoiceFlags(Ljava/util/EnumSet;)V

    .line 18
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->g:Ljava/lang/Integer;

    if-eqz p1, :cond_2

    .line 19
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/forms/ChoiceFormElement;->setSelectedIndexes(Ljava/util/List;)V

    :cond_2
    return-object v0
.end method

.method final a()Lcom/pspdfkit/forms/FormType;
    .locals 1

    .line 20
    sget-object v0, Lcom/pspdfkit/forms/FormType;->COMBOBOX:Lcom/pspdfkit/forms/FormType;

    return-object v0
.end method

.method final a(I)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getCustomText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormOption;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->i:Ljava/util/List;

    return-object v0
.end method

.method public getSelectedIndex()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->g:Ljava/lang/Integer;

    return-object v0
.end method

.method public isEditable()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->j:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;->EDIT:Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isMultiSelectionEnabled()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/ComboBoxFormConfiguration;->j:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;->MULTI_SELECT:Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

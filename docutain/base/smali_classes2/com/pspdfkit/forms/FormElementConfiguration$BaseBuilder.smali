.class public abstract Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/forms/FormElementConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BaseBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Lcom/pspdfkit/forms/FormElementConfiguration;",
        "B:",
        "Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder<",
        "TV;TB;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:I

.field final b:Landroid/graphics/RectF;

.field final c:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeFormFlags;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/pspdfkit/forms/FormElement;

.field e:Lcom/pspdfkit/forms/FormElement;

.field f:Lcom/pspdfkit/internal/o;


# direct methods
.method protected constructor <init>(ILandroid/graphics/RectF;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-class v0, Lcom/pspdfkit/internal/jni/NativeFormFlags;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->c:Ljava/util/EnumSet;

    .line 10
    new-instance v0, Lcom/pspdfkit/internal/o;

    invoke-direct {v0}, Lcom/pspdfkit/internal/o;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->f:Lcom/pspdfkit/internal/o;

    const-string v0, "boundingBox"

    .line 21
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iput p1, p0, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->a:I

    .line 23
    iput-object p2, p0, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->b:Landroid/graphics/RectF;

    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TB;"
        }
    .end annotation
.end method

.method public abstract build()Lcom/pspdfkit/forms/FormElementConfiguration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation
.end method

.method public setAdditionalAction(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;Lcom/pspdfkit/annotations/actions/Action;)Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;",
            "Lcom/pspdfkit/annotations/actions/Action;",
            ")TB;"
        }
    .end annotation

    const-string v0, "triggerEvent"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->f:Lcom/pspdfkit/internal/o;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/o;->a(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;Lcom/pspdfkit/annotations/actions/Action;)V

    .line 55
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->a()Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;

    move-result-object p1

    return-object p1
.end method

.method public setNextElement(Lcom/pspdfkit/forms/FormElement;)Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/forms/FormElement;",
            ")TB;"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->e:Lcom/pspdfkit/forms/FormElement;

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->a()Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;

    move-result-object p1

    return-object p1
.end method

.method public setPreviousElement(Lcom/pspdfkit/forms/FormElement;)Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/forms/FormElement;",
            ")TB;"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->d:Lcom/pspdfkit/forms/FormElement;

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->a()Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;

    move-result-object p1

    return-object p1
.end method

.method public setReadOnly(Z)Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->c:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormFlags;->READONLY:Lcom/pspdfkit/internal/jni/NativeFormFlags;

    invoke-static {v0, v1, p1}, Lcom/pspdfkit/internal/fv;->a(Ljava/util/EnumSet;Ljava/lang/Enum;Z)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->a()Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;

    move-result-object p1

    return-object p1
.end method

.method public setRequired(Z)Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->c:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormFlags;->REQUIRED:Lcom/pspdfkit/internal/jni/NativeFormFlags;

    invoke-static {v0, v1, p1}, Lcom/pspdfkit/internal/fv;->a(Ljava/util/EnumSet;Ljava/lang/Enum;Z)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;->a()Lcom/pspdfkit/forms/FormElementConfiguration$BaseBuilder;

    move-result-object p1

    return-object p1
.end method

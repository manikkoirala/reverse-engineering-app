.class public abstract Lcom/pspdfkit/forms/ChoiceFormElement;
.super Lcom/pspdfkit/forms/FormElement;
.source "SourceFile"


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/ChoiceFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/forms/FormElement;-><init>(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-void
.end method


# virtual methods
.method public getFormField()Lcom/pspdfkit/forms/ChoiceFormField;
    .locals 1

    .line 2
    invoke-super {p0}, Lcom/pspdfkit/forms/FormElement;->getFormField()Lcom/pspdfkit/forms/FormField;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/forms/ChoiceFormField;

    return-object v0
.end method

.method public bridge synthetic getFormField()Lcom/pspdfkit/forms/FormField;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getFormField()Lcom/pspdfkit/forms/ChoiceFormField;

    move-result-object v0

    return-object v0
.end method

.method public getOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormOption;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getFormField()Lcom/pspdfkit/forms/ChoiceFormField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/forms/ChoiceFormField;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedIndexes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFormControl;->getSelectedIndexes()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public isCommitOnSelectionChangeEnabled()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getFormField()Lcom/pspdfkit/forms/ChoiceFormField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getChoiceFlags()Ljava/util/EnumSet;

    move-result-object v0

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;->COMMIT_ON_SEL_CHANGE:Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isMultiSelectEnabled()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getFormField()Lcom/pspdfkit/forms/ChoiceFormField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getChoiceFlags()Ljava/util/EnumSet;

    move-result-object v0

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;->MULTI_SELECT:Lcom/pspdfkit/internal/jni/NativeFormChoiceFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setOptions(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/forms/FormOption;",
            ">;)V"
        }
    .end annotation

    const-string v0, "options"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getFormField()Lcom/pspdfkit/forms/ChoiceFormField;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/forms/ChoiceFormField;->setOptions(Ljava/util/List;)V

    return-void
.end method

.method public setSelectedIndexes(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-string v0, "selectedIndexes"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getSelectedIndexes()Ljava/util/List;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 55
    :cond_0
    instance-of v0, p1, Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 56
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeFormControl;->setSelectedIndexes(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 58
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeFormControl;->setSelectedIndexes(Ljava/util/ArrayList;)V

    :goto_0
    return-void
.end method

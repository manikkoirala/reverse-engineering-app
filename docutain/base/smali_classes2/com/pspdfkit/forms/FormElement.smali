.class public abstract Lcom/pspdfkit/forms/FormElement;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/annotations/WidgetAnnotation;

.field private final b:Lcom/pspdfkit/forms/FormField;

.field private c:Lcom/pspdfkit/forms/FormElement;

.field private d:Lcom/pspdfkit/forms/FormElement;


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/forms/FormElement;->b:Lcom/pspdfkit/forms/FormField;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/forms/FormElement;->a:Lcom/pspdfkit/annotations/WidgetAnnotation;

    return-void
.end method


# virtual methods
.method final a()Lcom/pspdfkit/internal/jni/NativeFormControl;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElement;->b:Lcom/pspdfkit/forms/FormField;

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getNativeFormControl()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    return-object v0
.end method

.method final b()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElement;->a:Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/forms/FormElement;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2
    :cond_1
    check-cast p1, Lcom/pspdfkit/forms/FormElement;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/forms/FormElement;->a:Lcom/pspdfkit/annotations/WidgetAnnotation;

    iget-object v3, p1, Lcom/pspdfkit/forms/FormElement;->a:Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-virtual {v1, v3}, Lcom/pspdfkit/annotations/Annotation;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/forms/FormElement;->b:Lcom/pspdfkit/forms/FormField;

    iget-object p1, p1, Lcom/pspdfkit/forms/FormElement;->b:Lcom/pspdfkit/forms/FormField;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/forms/FormField;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElement;->a:Lcom/pspdfkit/annotations/WidgetAnnotation;

    return-object v0
.end method

.method public getFormField()Lcom/pspdfkit/forms/FormField;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElement;->b:Lcom/pspdfkit/forms/FormField;

    return-object v0
.end method

.method public getFullyQualifiedName()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElement;->b:Lcom/pspdfkit/forms/FormField;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "formElement"

    const-string v2, "argumentName"

    .line 2
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getFormElements()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getNativeFormField()Lcom/pspdfkit/internal/jni/NativeFormField;

    move-result-object v0

    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeFormField;->getFQNForAnnotationWidgetId(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElement;->b:Lcom/pspdfkit/forms/FormField;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "formElement"

    const-string v2, "argumentName"

    .line 2
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getFormElements()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/tf;->getNativeFormField()Lcom/pspdfkit/internal/jni/NativeFormField;

    move-result-object v0

    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeFormField;->getNameForAnnotationWidgetId(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getNextElement()Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElement;->d:Lcom/pspdfkit/forms/FormElement;

    return-object v0
.end method

.method public getPreviousElement()Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElement;->c:Lcom/pspdfkit/forms/FormElement;

    return-object v0
.end method

.method public abstract getType()Lcom/pspdfkit/forms/FormType;
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElement;->a:Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/forms/FormElement;->b:Lcom/pspdfkit/forms/FormField;

    invoke-virtual {v1}, Lcom/pspdfkit/forms/FormField;->hashCode()I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method public isReadOnly()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElement;->b:Lcom/pspdfkit/forms/FormField;

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->isReadOnly()Z

    move-result v0

    return v0
.end method

.method public isRequired()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/forms/FormElement;->b:Lcom/pspdfkit/forms/FormField;

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->isRequired()Z

    move-result v0

    return v0
.end method

.method public setNextElement(Lcom/pspdfkit/forms/FormElement;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/forms/FormElement;->d:Lcom/pspdfkit/forms/FormElement;

    return-void
.end method

.method public setPreviousElement(Lcom/pspdfkit/forms/FormElement;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/forms/FormElement;->c:Lcom/pspdfkit/forms/FormElement;

    return-void
.end method

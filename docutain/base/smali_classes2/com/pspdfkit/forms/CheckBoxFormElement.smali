.class public Lcom/pspdfkit/forms/CheckBoxFormElement;
.super Lcom/pspdfkit/forms/EditableButtonFormElement;
.source "SourceFile"


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/CheckBoxFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/forms/EditableButtonFormElement;-><init>(Lcom/pspdfkit/forms/EditableButtonFormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    return-void
.end method


# virtual methods
.method public getExportValue()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->a()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object v0

    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeFormControl;->getExportValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormField()Lcom/pspdfkit/forms/CheckBoxFormField;
    .locals 1

    .line 2
    invoke-super {p0}, Lcom/pspdfkit/forms/FormElement;->getFormField()Lcom/pspdfkit/forms/FormField;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/forms/CheckBoxFormField;

    return-object v0
.end method

.method public bridge synthetic getFormField()Lcom/pspdfkit/forms/FormField;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/CheckBoxFormElement;->getFormField()Lcom/pspdfkit/forms/CheckBoxFormField;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/pspdfkit/forms/FormType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/forms/FormType;->CHECKBOX:Lcom/pspdfkit/forms/FormType;

    return-object v0
.end method

.class final Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/rf;
.implements Lcom/pspdfkit/bookmarks/Bookmark$OnBookmarkUpdatedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/zf;

.field private final b:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;

.field private d:Z


# direct methods
.method public static synthetic $r8$lambda$Fh3j3vaZALaGV9_VN7jaZBL_JWE(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->a(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$mTzmiUdDnwQeGHG4IGlxubfh6bQ(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;)Lio/reactivex/rxjava3/core/ObservableSource;
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->b()Lio/reactivex/rxjava3/core/ObservableSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$mdXrmoNv386f2kmNx_g3FT0T8gc(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;Lcom/pspdfkit/bookmarks/Bookmark;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->a(Lcom/pspdfkit/bookmarks/Bookmark;)V

    return-void
.end method

.method public static synthetic $r8$lambda$xtkwBbx32c1AtPv56mCXGlqwZUQ(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;Lcom/pspdfkit/bookmarks/Bookmark;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->b(Lcom/pspdfkit/bookmarks/Bookmark;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->b:Lcom/pspdfkit/internal/nh;

    const/4 v0, 0x0

    .line 8
    iput-boolean v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->d:Z

    .line 11
    iput-object p1, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    return-void
.end method

.method private a()Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->c:Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->getBookmarkManager()Lcom/pspdfkit/internal/jni/NativeBookmarkManager;

    move-result-object v0

    .line 5
    invoke-static {v0, p0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;->a(Lcom/pspdfkit/internal/jni/NativeBookmarkManager;Lcom/pspdfkit/bookmarks/Bookmark$OnBookmarkUpdatedListener;)Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->c:Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->c:Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 10
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private synthetic a(Lcom/pspdfkit/bookmarks/Bookmark;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 11
    invoke-virtual {p0, p1}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->addBookmark(Lcom/pspdfkit/bookmarks/Bookmark;)Z

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;Ljava/util/List;)V
    .locals 0

    .line 12
    invoke-interface {p0, p1}, Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;->onBookmarksChanged(Ljava/util/List;)V

    return-void
.end method

.method private synthetic b()Lio/reactivex/rxjava3/core/ObservableSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->getBookmarks()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    return-object v0
.end method

.method private synthetic b(Lcom/pspdfkit/bookmarks/Bookmark;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->removeBookmark(Lcom/pspdfkit/bookmarks/Bookmark;)Z

    return-void
.end method

.method private c()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->a()Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;

    move-result-object v0

    .line 2
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;->-$$Nest$fgetb(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;

    .line 5
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/u;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/u;->c()Ljava/util/concurrent/Executor;

    move-result-object v3

    new-instance v4, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$$ExternalSyntheticLambda1;

    invoke-direct {v4, v2, v1}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;Ljava/util/List;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addBookmark(Lcom/pspdfkit/bookmarks/Bookmark;)Z
    .locals 7

    const-string v0, "bookmark"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 56
    monitor-enter p0

    .line 57
    :try_start_0
    invoke-direct {p0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->a()Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;

    move-result-object v0

    .line 58
    invoke-virtual {v0, p1}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;->exists(Lcom/pspdfkit/bookmarks/Bookmark;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    const-string v0, "PSPDFKit.Bookmarks"

    const-string v1, "Attempted to add already added bookmark (id %s already exists), skipping..."

    new-array v3, v3, [Ljava/lang/Object;

    .line 62
    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getUuid()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v3, v2

    .line 63
    invoke-static {v0, v1, v3}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    monitor-exit p0

    return v2

    .line 70
    :cond_0
    iput-boolean v3, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->d:Z

    .line 72
    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getSortKey()Ljava/lang/Integer;

    move-result-object v6

    .line 73
    invoke-static {v1, v4, v5, v6}, Lcom/pspdfkit/internal/jni/NativeBookmark;->createBookmark(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)Lcom/pspdfkit/internal/jni/NativeBookmark;

    move-result-object v1

    .line 76
    iget-object v4, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    .line 77
    invoke-virtual {v4}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/jni/NativeDocument;->getBookmarkManager()Lcom/pspdfkit/internal/jni/NativeBookmarkManager;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/pspdfkit/internal/jni/NativeBookmarkManager;->addBookmark(Lcom/pspdfkit/internal/jni/NativeBookmark;)Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object v4

    .line 78
    invoke-virtual {v4}, Lcom/pspdfkit/internal/jni/NativeResult;->getHasError()Z

    move-result v5

    if-nez v5, :cond_1

    .line 82
    invoke-static {v0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;->-$$Nest$fgeta(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getUuid()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    invoke-static {v0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;->-$$Nest$fgetb(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    invoke-virtual {p1, p0}, Lcom/pspdfkit/bookmarks/Bookmark;->a(Lcom/pspdfkit/bookmarks/Bookmark$OnBookmarkUpdatedListener;)V

    .line 86
    invoke-direct {p0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->c()V

    .line 87
    monitor-exit p0

    return v3

    :cond_1
    const-string v0, "PSPDFKit.Bookmarks"

    const-string v1, "Failed to add bookmark %s to document!"

    new-array v3, v3, [Ljava/lang/Object;

    .line 88
    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getUuid()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v3, v2

    invoke-static {v0, v1, v3}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/jni/NativeResult;->getErrorString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 97
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 98
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Page must be set on new bookmarks!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public addBookmarkAsync(Lcom/pspdfkit/bookmarks/Bookmark;)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    const-string v0, "bookmark"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;Lcom/pspdfkit/bookmarks/Bookmark;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 55
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public addBookmarkListener(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public getBookmarks()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/bookmarks/Bookmark;",
            ">;"
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->a()Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;->-$$Nest$fgetb(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBookmarksAsync()Lio/reactivex/rxjava3/core/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/bookmarks/Bookmark;",
            ">;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    const/4 v2, 0x5

    .line 2
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    return-object v0
.end method

.method public hasUnsavedChanges()Z
    .locals 4

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->d:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    monitor-exit p0

    return v1

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->c:Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    monitor-exit p0

    return v2

    .line 5
    :cond_1
    invoke-static {v0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;->-$$Nest$fgetb(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/bookmarks/Bookmark;

    .line 6
    invoke-virtual {v3}, Lcom/pspdfkit/bookmarks/Bookmark;->isDirty()Z

    move-result v3

    if-eqz v3, :cond_2

    monitor-exit p0

    return v1

    .line 9
    :cond_3
    monitor-exit p0

    return v2

    :catchall_0
    move-exception v0

    .line 10
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public markBookmarksAsSavedToDisk()V
    .locals 1

    .line 1
    monitor-enter p0

    const/4 v0, 0x0

    .line 2
    :try_start_0
    iput-boolean v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->d:Z

    .line 3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onBookmarkUpdated(Lcom/pspdfkit/bookmarks/Bookmark;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->c()V

    return-void
.end method

.method public prepareToSave()V
    .locals 4

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->c:Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    .line 3
    :cond_0
    invoke-static {v0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;->-$$Nest$fgetb(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/bookmarks/Bookmark;

    .line 4
    invoke-virtual {v1}, Lcom/pspdfkit/bookmarks/Bookmark;->isDirty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->c:Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;

    invoke-static {v2}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;->-$$Nest$fgeta(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1}, Lcom/pspdfkit/bookmarks/Bookmark;->getUuid()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/jni/NativeBookmark;

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/bookmarks/Bookmark;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/jni/NativeBookmark;->setName(Ljava/lang/String;)V

    .line 7
    invoke-virtual {v1}, Lcom/pspdfkit/bookmarks/Bookmark;->getSortKey()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/jni/NativeBookmark;->setSortKey(Ljava/lang/Integer;)V

    .line 8
    invoke-virtual {v1}, Lcom/pspdfkit/bookmarks/Bookmark;->clearDirty()V

    goto :goto_0

    .line 11
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeBookmark(Lcom/pspdfkit/bookmarks/Bookmark;)Z
    .locals 6

    const-string v0, "bookmark"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    monitor-enter p0

    .line 55
    :try_start_0
    invoke-direct {p0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->a()Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;

    move-result-object v0

    .line 56
    invoke-virtual {v0, p1}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;->exists(Lcom/pspdfkit/bookmarks/Bookmark;)Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v2, :cond_0

    const-string v0, "PSPDFKit.Bookmarks"

    const-string v1, "Attempted to remove non-existing bookmark (id %s), skipping..."

    new-array v2, v4, [Ljava/lang/Object;

    .line 60
    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getUuid()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    .line 61
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    monitor-exit p0

    return v3

    .line 68
    :cond_0
    invoke-static {v0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;->-$$Nest$fgeta(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getUuid()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/jni/NativeBookmark;

    .line 69
    iget-object v5, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    .line 70
    invoke-virtual {v5}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/internal/jni/NativeDocument;->getBookmarkManager()Lcom/pspdfkit/internal/jni/NativeBookmarkManager;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/pspdfkit/internal/jni/NativeBookmarkManager;->removeBookmark(Lcom/pspdfkit/internal/jni/NativeBookmark;)Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object v2

    .line 71
    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeResult;->getHasError()Z

    move-result v5

    if-nez v5, :cond_1

    .line 76
    iput-boolean v4, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->d:Z

    .line 77
    invoke-static {v0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;->-$$Nest$fgetb(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 78
    invoke-static {v0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;->-$$Nest$fgeta(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$BookmarkCache;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    invoke-virtual {p1, v1}, Lcom/pspdfkit/bookmarks/Bookmark;->a(Lcom/pspdfkit/bookmarks/Bookmark$OnBookmarkUpdatedListener;)V

    .line 81
    invoke-direct {p0}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->c()V

    .line 83
    monitor-exit p0

    return v4

    :cond_1
    const-string v0, "PSPDFKit.Bookmarks"

    const-string v1, "Failed to remove bookmark %s from document!"

    new-array v4, v4, [Ljava/lang/Object;

    .line 84
    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getUuid()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, v3

    invoke-static {v0, v1, v4}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeResult;->getErrorString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 96
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public removeBookmarkAsync(Lcom/pspdfkit/bookmarks/Bookmark;)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    const-string v0, "bookmark"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;Lcom/pspdfkit/bookmarks/Bookmark;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 55
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public removeBookmarkListener(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/BookmarkProviderImpl;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

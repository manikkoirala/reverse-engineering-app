.class public Lcom/pspdfkit/bookmarks/Bookmark;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/bookmarks/Bookmark$OnBookmarkUpdatedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/pspdfkit/bookmarks/Bookmark;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;

.field private e:Z

.field private f:Lcom/pspdfkit/bookmarks/Bookmark$OnBookmarkUpdatedListener;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->e:Z

    const/4 v0, 0x0

    .line 13
    iput-object v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->c:Ljava/lang/String;

    .line 14
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->b:Ljava/lang/Integer;

    .line 15
    invoke-static {}, Lcom/pspdfkit/internal/gj;->w()Lcom/pspdfkit/internal/gv;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/jo;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jo;->a()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 17
    iput-boolean v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->e:Z

    .line 41
    iput-object p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->c:Ljava/lang/String;

    .line 42
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->b:Ljava/lang/Integer;

    .line 43
    invoke-static {}, Lcom/pspdfkit/internal/gj;->w()Lcom/pspdfkit/internal/gv;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/jo;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jo;->a()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 45
    iput-boolean v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->e:Z

    const-string v0, "uuid"

    .line 84
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iput-object p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->a:Ljava/lang/String;

    .line 86
    iput-object p2, p0, Lcom/pspdfkit/bookmarks/Bookmark;->c:Ljava/lang/String;

    .line 87
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->b:Ljava/lang/Integer;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 89
    iput-boolean v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->e:Z

    const-string v0, "uuid"

    .line 139
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    iput-object p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->a:Ljava/lang/String;

    .line 141
    iput-object p2, p0, Lcom/pspdfkit/bookmarks/Bookmark;->c:Ljava/lang/String;

    .line 142
    iput-object p3, p0, Lcom/pspdfkit/bookmarks/Bookmark;->b:Ljava/lang/Integer;

    .line 143
    iput-object p4, p0, Lcom/pspdfkit/bookmarks/Bookmark;->d:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method final declared-synchronized a(Lcom/pspdfkit/bookmarks/Bookmark$OnBookmarkUpdatedListener;)V
    .locals 0

    monitor-enter p0

    .line 1
    :try_start_0
    iput-object p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->f:Lcom/pspdfkit/bookmarks/Bookmark$OnBookmarkUpdatedListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized clearDirty()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 1
    :try_start_0
    iput-boolean v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized compareTo(Lcom/pspdfkit/bookmarks/Bookmark;)I
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    if-nez p1, :cond_0

    monitor-exit p0

    return v0

    .line 2
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getSortKey()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->d:Ljava/lang/Integer;

    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getSortKey()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    .line 6
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->b:Ljava/lang/Integer;

    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return p1

    :cond_2
    monitor-exit p0

    return v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/bookmarks/Bookmark;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/bookmarks/Bookmark;->compareTo(Lcom/pspdfkit/bookmarks/Bookmark;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 1
    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/bookmarks/Bookmark;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 3
    :cond_1
    check-cast p1, Lcom/pspdfkit/bookmarks/Bookmark;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->a:Ljava/lang/String;

    iget-object p1, p1, Lcom/pspdfkit/bookmarks/Bookmark;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public declared-synchronized getName()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->c:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPageIndex()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public declared-synchronized getSortKey()Ljava/lang/Integer;
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->d:Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getUuid()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->a:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public declared-synchronized isDirty()Z
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setName(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->c:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->c:Ljava/lang/String;

    const/4 p1, 0x1

    .line 3
    iput-boolean p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->e:Z

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->f:Lcom/pspdfkit/bookmarks/Bookmark$OnBookmarkUpdatedListener;

    if-eqz p1, :cond_0

    .line 5
    invoke-interface {p1, p0}, Lcom/pspdfkit/bookmarks/Bookmark$OnBookmarkUpdatedListener;->onBookmarkUpdated(Lcom/pspdfkit/bookmarks/Bookmark;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setSortKey(I)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/bookmarks/Bookmark;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 2
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->d:Ljava/lang/Integer;

    const/4 p1, 0x1

    .line 3
    iput-boolean p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->e:Z

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->f:Lcom/pspdfkit/bookmarks/Bookmark$OnBookmarkUpdatedListener;

    if-eqz p1, :cond_1

    .line 5
    invoke-interface {p1, p0}, Lcom/pspdfkit/bookmarks/Bookmark$OnBookmarkUpdatedListener;->onBookmarkUpdated(Lcom/pspdfkit/bookmarks/Bookmark;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 2

    monitor-enter p0

    .line 1
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bookmark{uuid=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', page="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->b:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', sortKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/bookmarks/Bookmark;->d:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

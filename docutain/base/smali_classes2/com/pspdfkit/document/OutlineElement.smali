.class public Lcom/pspdfkit/document/OutlineElement;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/document/OutlineElement$Builder;,
        Lcom/pspdfkit/document/OutlineElement$OutlineElementStyle;
    }
.end annotation


# static fields
.field public static final DEFAULT_COLOR:I = -0x1000000


# instance fields
.field private final action:Lcom/pspdfkit/annotations/actions/Action;

.field private final children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/OutlineElement;",
            ">;"
        }
    .end annotation
.end field

.field private final color:I

.field private final expanded:Z

.field private final label:Ljava/lang/String;

.field private final style:I

.field private final title:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;IIZLcom/pspdfkit/annotations/actions/Action;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIZ",
            "Lcom/pspdfkit/annotations/actions/Action;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/OutlineElement;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/document/OutlineElement;->title:Ljava/lang/String;

    .line 3
    iput p2, p0, Lcom/pspdfkit/document/OutlineElement;->color:I

    .line 4
    iput p3, p0, Lcom/pspdfkit/document/OutlineElement;->style:I

    .line 5
    iput-object p7, p0, Lcom/pspdfkit/document/OutlineElement;->children:Ljava/util/List;

    .line 6
    iput-object p5, p0, Lcom/pspdfkit/document/OutlineElement;->action:Lcom/pspdfkit/annotations/actions/Action;

    .line 7
    iput-object p6, p0, Lcom/pspdfkit/document/OutlineElement;->label:Ljava/lang/String;

    .line 8
    iput-boolean p4, p0, Lcom/pspdfkit/document/OutlineElement;->expanded:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IIZLcom/pspdfkit/annotations/actions/Action;Ljava/lang/String;Ljava/util/List;Lcom/pspdfkit/document/OutlineElement-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/pspdfkit/document/OutlineElement;-><init>(Ljava/lang/String;IIZLcom/pspdfkit/annotations/actions/Action;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 1
    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/document/OutlineElement;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    return v1

    .line 3
    :cond_1
    check-cast p1, Lcom/pspdfkit/document/OutlineElement;

    .line 5
    iget v0, p0, Lcom/pspdfkit/document/OutlineElement;->color:I

    iget v2, p1, Lcom/pspdfkit/document/OutlineElement;->color:I

    if-eq v0, v2, :cond_2

    return v1

    .line 6
    :cond_2
    iget v0, p0, Lcom/pspdfkit/document/OutlineElement;->style:I

    iget v2, p1, Lcom/pspdfkit/document/OutlineElement;->style:I

    if-eq v0, v2, :cond_3

    return v1

    .line 7
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/document/OutlineElement;->action:Lcom/pspdfkit/annotations/actions/Action;

    iget-object v2, p1, Lcom/pspdfkit/document/OutlineElement;->action:Lcom/pspdfkit/annotations/actions/Action;

    invoke-static {v0, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    return v1

    .line 8
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/document/OutlineElement;->title:Ljava/lang/String;

    iget-object p1, p1, Lcom/pspdfkit/document/OutlineElement;->title:Ljava/lang/String;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getAction()Lcom/pspdfkit/annotations/actions/Action;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/OutlineElement;->action:Lcom/pspdfkit/annotations/actions/Action;

    return-object v0
.end method

.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/OutlineElement;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/OutlineElement;->children:Ljava/util/List;

    return-object v0
.end method

.method public getColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/document/OutlineElement;->color:I

    return v0
.end method

.method public getPageLabel()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/OutlineElement;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getStyle()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/document/OutlineElement;->style:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/OutlineElement;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/OutlineElement;->title:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 2
    iget v1, p0, Lcom/pspdfkit/document/OutlineElement;->color:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 3
    iget v1, p0, Lcom/pspdfkit/document/OutlineElement;->style:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/document/OutlineElement;->action:Lcom/pspdfkit/annotations/actions/Action;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/document/OutlineElement;->children:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method public isExpanded()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/document/OutlineElement;->expanded:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "OutlineElement{action="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/document/OutlineElement;->action:Lcom/pspdfkit/annotations/actions/Action;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", title=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/document/OutlineElement;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/document/OutlineElement;->color:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", style="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/document/OutlineElement;->style:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

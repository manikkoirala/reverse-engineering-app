.class public final Lcom/pspdfkit/document/html/HtmlToPdfConverter;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/document/html/HtmlToPdfConverter$PageLoadingProgressListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_DENSITY_DPI:I = 0x12c

.field private static final DEFAULT_WEB_VIEW_TITLE:Ljava/lang/String; = "about:blank"

.field private static final INTERNAL_PAGE_SIZE_ID:Ljava/lang/String; = "page_size"

.field private static final INTERNAL_RESOLUTION_ID:Ljava/lang/String; = "resolution"

.field static final LOG_TAG:Ljava/lang/String; = "HtmlToPdfConverter"

.field private static final MIME_TYPE_HTML:Ljava/lang/String; = "text/html"


# instance fields
.field private final baseUrl:Ljava/lang/String;

.field private final context:Landroid/content/Context;

.field private customTitle:Ljava/lang/String;

.field private densityDpi:I

.field private final htmlString:Ljava/lang/String;

.field private isJavascriptEnabled:Z

.field private mediaSize:Landroid/print/PrintAttributes$MediaSize;

.field private pageLoadingProgressListener:Lcom/pspdfkit/document/html/HtmlToPdfConverter$PageLoadingProgressListener;

.field private resourceInterceptor:Lcom/pspdfkit/document/html/ResourceInterceptor;

.field private final sourceUri:Landroid/net/Uri;

.field private timeoutMs:J


# direct methods
.method static bridge synthetic -$$Nest$mtryClose(Lcom/pspdfkit/document/html/HtmlToPdfConverter;Lio/reactivex/rxjava3/core/CompletableEmitter;Landroid/os/ParcelFileDescriptor;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->tryClose(Lio/reactivex/rxjava3/core/CompletableEmitter;Landroid/os/ParcelFileDescriptor;)Z

    move-result p0

    return p0
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lcom/pspdfkit/document/processor/NewPage;->PAGE_SIZE_A4:Lcom/pspdfkit/utils/Size;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->pdfSizeToMediaSize(Lcom/pspdfkit/utils/Size;)Landroid/print/PrintAttributes$MediaSize;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->mediaSize:Landroid/print/PrintAttributes$MediaSize;

    const/16 v0, 0x12c

    .line 5
    iput v0, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->densityDpi:I

    const/4 v0, 0x1

    .line 10
    iput-boolean v0, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->isJavascriptEnabled:Z

    const-wide/16 v0, 0x7530

    .line 12
    iput-wide v0, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->timeoutMs:J

    .line 15
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->WEBKIT_HTML_CONVERSION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "context"

    .line 18
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "url"

    .line 19
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->context:Landroid/content/Context;

    .line 22
    invoke-virtual {p2}, Landroid/net/Uri;->normalizeScheme()Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->sourceUri:Landroid/net/Uri;

    const/4 p1, 0x0

    .line 23
    iput-object p1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->htmlString:Ljava/lang/String;

    .line 24
    iput-object p1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->baseUrl:Ljava/lang/String;

    return-void

    .line 25
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Your current license does not allow HTML-to-PDF conversion."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    sget-object v0, Lcom/pspdfkit/document/processor/NewPage;->PAGE_SIZE_A4:Lcom/pspdfkit/utils/Size;

    .line 28
    invoke-static {v0}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->pdfSizeToMediaSize(Lcom/pspdfkit/utils/Size;)Landroid/print/PrintAttributes$MediaSize;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->mediaSize:Landroid/print/PrintAttributes$MediaSize;

    const/16 v0, 0x12c

    .line 30
    iput v0, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->densityDpi:I

    const/4 v0, 0x1

    .line 35
    iput-boolean v0, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->isJavascriptEnabled:Z

    const-wide/16 v0, 0x7530

    .line 37
    iput-wide v0, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->timeoutMs:J

    .line 54
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->WEBKIT_HTML_CONVERSION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "context"

    .line 57
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "htmlString"

    .line 58
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iput-object p1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->context:Landroid/content/Context;

    .line 61
    sget-object p1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iput-object p1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->sourceUri:Landroid/net/Uri;

    .line 62
    iput-object p2, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->htmlString:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->baseUrl:Ljava/lang/String;

    return-void

    .line 64
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Your current license does not allow HTML-to-PDF conversion."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private createPdfFromPrintAdapter(Landroid/print/PrintDocumentAdapter;Ljava/io/File;Landroid/os/CancellationSignal;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter;Landroid/print/PrintDocumentAdapter;Ljava/io/File;Landroid/os/CancellationSignal;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public static doesDeviceSupportConversion(Landroid/content/Context;)Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "doesDeviceSupportConversion() may only be called from the main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    const-string v0, "context"

    const-string v1, "message"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_0

    .line 4
    invoke-static {p0}, Lcom/pspdfkit/internal/e8;->g(Landroid/content/Context;)Z

    move-result p0

    return p0

    .line 5
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static fromHTMLString(Landroid/content/Context;Ljava/lang/String;)Lcom/pspdfkit/document/html/HtmlToPdfConverter;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-static {p0, p1, v0}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->fromHTMLString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/document/html/HtmlToPdfConverter;

    move-result-object p0

    return-object p0
.end method

.method public static fromHTMLString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/document/html/HtmlToPdfConverter;
    .locals 1

    .line 2
    new-instance v0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static fromUri(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/document/html/HtmlToPdfConverter;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    return-object v0
.end method

.method private getDocumentTitle(Landroid/webkit/WebView;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/WebView;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter;Landroid/webkit/WebView;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 15
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method private getPrintAttributes()Landroid/print/PrintAttributes;
    .locals 4

    .line 1
    new-instance v0, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v0}, Landroid/print/PrintAttributes$Builder;-><init>()V

    const/4 v1, 0x2

    .line 3
    invoke-virtual {v0, v1}, Landroid/print/PrintAttributes$Builder;->setColorMode(I)Landroid/print/PrintAttributes$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->mediaSize:Landroid/print/PrintAttributes$MediaSize;

    .line 5
    invoke-virtual {v0, v1}, Landroid/print/PrintAttributes$Builder;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;

    move-result-object v0

    new-instance v1, Landroid/print/PrintAttributes$Resolution;

    iget v2, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->densityDpi:I

    const-string v3, "resolution"

    invoke-direct {v1, v3, v3, v2, v2}, Landroid/print/PrintAttributes$Resolution;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 6
    invoke-virtual {v0, v1}, Landroid/print/PrintAttributes$Builder;->setResolution(Landroid/print/PrintAttributes$Resolution;)Landroid/print/PrintAttributes$Builder;

    move-result-object v0

    new-instance v1, Landroid/print/PrintAttributes$Margins;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v2, v2, v2}, Landroid/print/PrintAttributes$Margins;-><init>(IIII)V

    .line 8
    invoke-virtual {v0, v1}, Landroid/print/PrintAttributes$Builder;->setMinMargins(Landroid/print/PrintAttributes$Margins;)Landroid/print/PrintAttributes$Builder;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$convertToPdfAsync$2([Landroid/webkit/WebView;Landroid/webkit/WebView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    aput-object p1, p0, v0

    return-void
.end method

.method static synthetic lambda$convertToPdfAsync$6([Landroid/webkit/WebView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    aget-object p0, p0, v0

    if-eqz p0, :cond_0

    .line 2
    invoke-virtual {p0}, Landroid/webkit/WebView;->destroy()V

    :cond_0
    return-void
.end method

.method static synthetic lambda$performDocumentPostprocessing$11(Ljava/lang/String;Lcom/pspdfkit/document/PdfDocument;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPdfMetadata()Lcom/pspdfkit/document/metadata/DocumentPdfMetadata;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/document/metadata/DocumentPdfMetadata;->setTitle(Ljava/lang/String;)V

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->saveIfModified()Z

    return-void
.end method

.method private loadHtmlData(Landroid/webkit/WebView;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter;Landroid/webkit/WebView;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method private static pdfSizeToMediaSize(Lcom/pspdfkit/utils/Size;)Landroid/print/PrintAttributes$MediaSize;
    .locals 3

    .line 1
    new-instance v0, Landroid/print/PrintAttributes$MediaSize;

    iget v1, p0, Lcom/pspdfkit/utils/Size;->width:F

    .line 2
    invoke-static {v1}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->ptToMil(F)I

    move-result v1

    iget p0, p0, Lcom/pspdfkit/utils/Size;->height:F

    invoke-static {p0}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->ptToMil(F)I

    move-result p0

    const-string v2, "page_size"

    invoke-direct {v0, v2, v2, v1, p0}, Landroid/print/PrintAttributes$MediaSize;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    return-object v0
.end method

.method private performDocumentPostprocessing(Ljava/io/File;Landroid/webkit/WebView;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 1
    invoke-direct {p0, p2}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->getDocumentTitle(Landroid/webkit/WebView;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p2

    .line 3
    invoke-virtual {p2}, Lio/reactivex/rxjava3/core/Maybe;->onErrorComplete()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter;Ljava/io/File;)V

    .line 5
    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Maybe;->flatMapCompletable(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method private prepareWebView()Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Landroid/webkit/WebView;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method private printToFile(Landroid/print/PrintDocumentAdapter;Ljava/io/File;Landroid/os/CancellationSignal;Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 16

    move-object/from16 v8, p4

    :try_start_0
    const-string v0, "w"

    .line 2
    invoke-static {v0}, Landroid/os/ParcelFileDescriptor;->parseMode(Ljava/lang/String;)I

    move-result v0

    move-object/from16 v1, p2

    invoke-static {v1, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    .line 4
    new-instance v7, Lcom/pspdfkit/document/html/HtmlToPdfConverter$1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-object/from16 v9, p0

    move-object/from16 v0, p1

    :try_start_1
    invoke-direct {v7, v9, v0, v8, v4}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$1;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter;Landroid/print/PrintDocumentAdapter;Lio/reactivex/rxjava3/core/CompletableEmitter;Landroid/os/ParcelFileDescriptor;)V

    .line 26
    new-instance v14, Lcom/pspdfkit/document/html/HtmlToPdfConverter$2;

    move-object v1, v14

    move-object/from16 v2, p0

    move-object/from16 v3, p4

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$2;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter;Lio/reactivex/rxjava3/core/CompletableEmitter;Landroid/os/ParcelFileDescriptor;Landroid/print/PrintDocumentAdapter;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V

    .line 59
    invoke-virtual/range {p1 .. p1}, Landroid/print/PrintDocumentAdapter;->onStart()V

    const/4 v11, 0x0

    .line 64
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->getPrintAttributes()Landroid/print/PrintAttributes;

    move-result-object v12

    new-instance v15, Landroid/os/Bundle;

    invoke-direct {v15}, Landroid/os/Bundle;-><init>()V

    move-object/from16 v10, p1

    move-object/from16 v13, p3

    .line 65
    invoke-virtual/range {v10 .. v15}, Landroid/print/PrintDocumentAdapter;->onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object/from16 v9, p0

    .line 68
    :goto_0
    new-instance v1, Lcom/pspdfkit/document/html/HtmlConversionException;

    const-string v2, "Unexpected error when performing HTML-to-PDF conversion."

    invoke-direct {v1, v2, v0}, Lcom/pspdfkit/document/html/HtmlConversionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v8, v1}, Lio/reactivex/rxjava3/core/CompletableEmitter;->tryOnError(Ljava/lang/Throwable;)Z

    :goto_1
    return-void
.end method

.method private static ptToMil(F)I
    .locals 1

    const v0, 0x3c638e39

    mul-float p0, p0, v0

    const/high16 v0, 0x447a0000    # 1000.0f

    mul-float p0, p0, v0

    .line 1
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    move-result p0

    return p0
.end method

.method private tryClose(Lio/reactivex/rxjava3/core/CompletableEmitter;Landroid/os/ParcelFileDescriptor;)Z
    .locals 2

    .line 1
    :try_start_0
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p2

    .line 3
    new-instance v0, Lcom/pspdfkit/document/html/HtmlConversionException;

    const-string v1, "Can\'t write PDF file."

    invoke-direct {v0, v1, p2}, Lcom/pspdfkit/document/html/HtmlConversionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {p1, v0}, Lio/reactivex/rxjava3/core/CompletableEmitter;->tryOnError(Ljava/lang/Throwable;)Z

    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public convertToPdfAsync(Ljava/io/File;)Lio/reactivex/rxjava3/core/Completable;
    .locals 8

    const-string v0, "outputFile"

    const-string v1, "argumentName"

    .line 11
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 62
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 63
    new-instance v0, Landroid/os/CancellationSignal;

    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/webkit/WebView;

    .line 68
    invoke-direct {p0}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->prepareWebView()Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    new-instance v3, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda9;

    invoke-direct {v3, v1}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda9;-><init>([Landroid/webkit/WebView;)V

    .line 71
    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Single;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    new-instance v3, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda10;

    invoke-direct {v3, p0}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter;)V

    .line 73
    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Single;->flatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    iget-wide v3, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->timeoutMs:J

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Lcom/pspdfkit/document/html/HtmlConversionException;

    const-string v7, "HTML loading timed out."

    invoke-direct {v6, v7}, Lcom/pspdfkit/document/html/HtmlConversionException;-><init>(Ljava/lang/String;)V

    .line 78
    invoke-static {v6}, Lio/reactivex/rxjava3/core/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v6

    .line 79
    invoke-virtual {v2, v3, v4, v5, v6}, Lio/reactivex/rxjava3/core/Single;->timeout(JLjava/util/concurrent/TimeUnit;Lio/reactivex/rxjava3/core/SingleSource;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    new-instance v3, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda11;

    invoke-direct {v3}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda11;-><init>()V

    .line 84
    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    new-instance v3, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda12;

    invoke-direct {v3, p0, p1, v0}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda12;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter;Ljava/io/File;Landroid/os/CancellationSignal;)V

    .line 86
    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Single;->flatMapCompletable(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v2

    .line 90
    new-instance v3, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda13;

    invoke-direct {v3, v0}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda13;-><init>(Landroid/os/CancellationSignal;)V

    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Completable;->doOnDispose(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 95
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v2, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda14;

    invoke-direct {v2, p0, p1, v1}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda14;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter;Ljava/io/File;[Landroid/webkit/WebView;)V

    .line 97
    invoke-static {v2}, Lio/reactivex/rxjava3/core/Completable;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 100
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda15;

    invoke-direct {v0, v1}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda15;-><init>([Landroid/webkit/WebView;)V

    .line 101
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public convertToPdfAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda7;

    invoke-direct {v0, p0}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 8
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda8;

    invoke-direct {v1, p0}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter;)V

    .line 9
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->flatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public density(I)Lcom/pspdfkit/document/html/HtmlToPdfConverter;
    .locals 1

    if-lez p1, :cond_0

    .line 1
    iput p1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->densityDpi:I

    return-object p0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "density cannot be less than or equal to zero."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method synthetic lambda$convertToPdfAsync$0$com-pspdfkit-document-html-HtmlToPdfConverter()Lio/reactivex/rxjava3/core/SingleSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->context:Landroid/content/Context;

    const-string v1, "pdf"

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to create output file."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0

    .line 5
    :cond_0
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method synthetic lambda$convertToPdfAsync$1$com-pspdfkit-document-html-HtmlToPdfConverter(Ljava/io/File;)Lio/reactivex/rxjava3/core/SingleSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->convertToPdfAsync(Ljava/io/File;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/core/Completable;->toSingleDefault(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method synthetic lambda$convertToPdfAsync$3$com-pspdfkit-document-html-HtmlToPdfConverter(Landroid/webkit/WebView;)Lio/reactivex/rxjava3/core/SingleSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->loadHtmlData(Landroid/webkit/WebView;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/core/Completable;->toSingleDefault(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method synthetic lambda$convertToPdfAsync$4$com-pspdfkit-document-html-HtmlToPdfConverter(Ljava/io/File;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p3, p1, p2}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->createPdfFromPrintAdapter(Landroid/print/PrintDocumentAdapter;Ljava/io/File;Landroid/os/CancellationSignal;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method synthetic lambda$convertToPdfAsync$5$com-pspdfkit-document-html-HtmlToPdfConverter(Ljava/io/File;[Landroid/webkit/WebView;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    aget-object p2, p2, v0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->performDocumentPostprocessing(Ljava/io/File;Landroid/webkit/WebView;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method synthetic lambda$createPdfFromPrintAdapter$10$com-pspdfkit-document-html-HtmlToPdfConverter(Landroid/print/PrintDocumentAdapter;Ljava/io/File;Landroid/os/CancellationSignal;Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    new-instance v6, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda3;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter;Landroid/print/PrintDocumentAdapter;Ljava/io/File;Landroid/os/CancellationSignal;Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    invoke-static {v6}, Lcom/pspdfkit/internal/dt;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method synthetic lambda$createPdfFromPrintAdapter$9$com-pspdfkit-document-html-HtmlToPdfConverter(Landroid/print/PrintDocumentAdapter;Ljava/io/File;Landroid/os/CancellationSignal;Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->printToFile(Landroid/print/PrintDocumentAdapter;Ljava/io/File;Landroid/os/CancellationSignal;Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    return-void
.end method

.method synthetic lambda$getDocumentTitle$13$com-pspdfkit-document-html-HtmlToPdfConverter(Landroid/webkit/WebView;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "getDocumentTitle() must be executed on the main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->customTitle:Ljava/lang/String;

    .line 4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 5
    invoke-virtual {p1}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object v0

    const-string p1, "about:blank"

    .line 8
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :cond_0
    if-eqz v0, :cond_1

    .line 12
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method synthetic lambda$loadHtmlData$8$com-pspdfkit-document-html-HtmlToPdfConverter(Landroid/webkit/WebView;Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/document/html/WebViewClientImpl;

    iget-object v1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->sourceUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->resourceInterceptor:Lcom/pspdfkit/document/html/ResourceInterceptor;

    invoke-direct {v0, v1, v2, v3, p2}, Lcom/pspdfkit/document/html/WebViewClientImpl;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/pspdfkit/document/html/ResourceInterceptor;Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 3
    iget-object v6, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->htmlString:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 4
    iget-object v5, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->baseUrl:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v7, "text/html"

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 7
    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->sourceUri:Landroid/net/Uri;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method synthetic lambda$performDocumentPostprocessing$12$com-pspdfkit-document-html-HtmlToPdfConverter(Ljava/io/File;Ljava/lang/String;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->context:Landroid/content/Context;

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocumentAsync(Landroid/content/Context;Landroid/net/Uri;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 2
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda5;

    invoke-direct {v0, p2}, Lcom/pspdfkit/document/html/HtmlToPdfConverter$$ExternalSyntheticLambda5;-><init>(Ljava/lang/String;)V

    .line 3
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 7
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->ignoreElement()Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method synthetic lambda$prepareWebView$7$com-pspdfkit-document-html-HtmlToPdfConverter()Lio/reactivex/rxjava3/core/SingleSource;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;)Landroid/webkit/WebView;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->pageLoadingProgressListener:Lcom/pspdfkit/document/html/HtmlToPdfConverter$PageLoadingProgressListener;

    if-eqz v2, :cond_0

    .line 8
    new-instance v3, Lcom/pspdfkit/document/html/WebChromeClientImpl;

    invoke-direct {v3, v2}, Lcom/pspdfkit/document/html/WebChromeClientImpl;-><init>(Lcom/pspdfkit/document/html/HtmlToPdfConverter$PageLoadingProgressListener;)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 12
    :cond_0
    sget-object v2, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    const/4 v2, 0x2

    .line 14
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 17
    iget-boolean v2, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->isJavascriptEnabled:Z

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    const/4 v2, 0x0

    .line 20
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 21
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 22
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setGeolocationEnabled(Z)V

    .line 23
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 25
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    .line 27
    new-instance v1, Lcom/pspdfkit/document/html/HtmlConversionException;

    const-string v2, "Could not initialize HTML-to-PDF conversion."

    invoke-direct {v1, v2, v0}, Lcom/pspdfkit/document/html/HtmlConversionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public pageSize(Lcom/pspdfkit/utils/Size;)Lcom/pspdfkit/document/html/HtmlToPdfConverter;
    .locals 2

    const-string v0, "pageSize"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-static {p1}, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->pdfSizeToMediaSize(Lcom/pspdfkit/utils/Size;)Landroid/print/PrintAttributes$MediaSize;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->mediaSize:Landroid/print/PrintAttributes$MediaSize;

    return-object p0
.end method

.method public setJavaScriptEnabled(Z)Lcom/pspdfkit/document/html/HtmlToPdfConverter;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->isJavascriptEnabled:Z

    return-object p0
.end method

.method public setPageLoadingProgressListener(Lcom/pspdfkit/document/html/HtmlToPdfConverter$PageLoadingProgressListener;)Lcom/pspdfkit/document/html/HtmlToPdfConverter;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->pageLoadingProgressListener:Lcom/pspdfkit/document/html/HtmlToPdfConverter$PageLoadingProgressListener;

    return-object p0
.end method

.method public setResourceInterceptor(Lcom/pspdfkit/document/html/ResourceInterceptor;)Lcom/pspdfkit/document/html/HtmlToPdfConverter;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->resourceInterceptor:Lcom/pspdfkit/document/html/ResourceInterceptor;

    return-object p0
.end method

.method public timeout(J)Lcom/pspdfkit/document/html/HtmlToPdfConverter;
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 1
    iput-wide p1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->timeoutMs:J

    return-object p0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "timeout cannot be less than or equal to zero."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public title(Ljava/lang/String;)Lcom/pspdfkit/document/html/HtmlToPdfConverter;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/document/html/HtmlToPdfConverter;->customTitle:Ljava/lang/String;

    return-object p0
.end method

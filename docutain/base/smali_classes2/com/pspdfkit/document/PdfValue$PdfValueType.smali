.class public final enum Lcom/pspdfkit/document/PdfValue$PdfValueType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/document/PdfValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PdfValueType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/document/PdfValue$PdfValueType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/document/PdfValue$PdfValueType;

.field public static final enum ARRAY:Lcom/pspdfkit/document/PdfValue$PdfValueType;

.field public static final enum BOOLEAN:Lcom/pspdfkit/document/PdfValue$PdfValueType;

.field public static final enum DICTIONARY:Lcom/pspdfkit/document/PdfValue$PdfValueType;

.field public static final enum DOUBLE:Lcom/pspdfkit/document/PdfValue$PdfValueType;

.field public static final enum INTEGER:Lcom/pspdfkit/document/PdfValue$PdfValueType;

.field public static final enum NAME:Lcom/pspdfkit/document/PdfValue$PdfValueType;

.field public static final enum NULLOBJ:Lcom/pspdfkit/document/PdfValue$PdfValueType;

.field public static final enum STREAM:Lcom/pspdfkit/document/PdfValue$PdfValueType;

.field public static final enum STRING:Lcom/pspdfkit/document/PdfValue$PdfValueType;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/document/PdfValue$PdfValueType;

    const-string v1, "BOOLEAN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/document/PdfValue$PdfValueType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/document/PdfValue$PdfValueType;->BOOLEAN:Lcom/pspdfkit/document/PdfValue$PdfValueType;

    .line 2
    new-instance v1, Lcom/pspdfkit/document/PdfValue$PdfValueType;

    const-string v3, "INTEGER"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/document/PdfValue$PdfValueType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/document/PdfValue$PdfValueType;->INTEGER:Lcom/pspdfkit/document/PdfValue$PdfValueType;

    .line 3
    new-instance v3, Lcom/pspdfkit/document/PdfValue$PdfValueType;

    const-string v5, "DOUBLE"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/document/PdfValue$PdfValueType;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/document/PdfValue$PdfValueType;->DOUBLE:Lcom/pspdfkit/document/PdfValue$PdfValueType;

    .line 4
    new-instance v5, Lcom/pspdfkit/document/PdfValue$PdfValueType;

    const-string v7, "STRING"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/document/PdfValue$PdfValueType;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/document/PdfValue$PdfValueType;->STRING:Lcom/pspdfkit/document/PdfValue$PdfValueType;

    .line 5
    new-instance v7, Lcom/pspdfkit/document/PdfValue$PdfValueType;

    const-string v9, "NAME"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/document/PdfValue$PdfValueType;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/document/PdfValue$PdfValueType;->NAME:Lcom/pspdfkit/document/PdfValue$PdfValueType;

    .line 6
    new-instance v9, Lcom/pspdfkit/document/PdfValue$PdfValueType;

    const-string v11, "ARRAY"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/document/PdfValue$PdfValueType;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/document/PdfValue$PdfValueType;->ARRAY:Lcom/pspdfkit/document/PdfValue$PdfValueType;

    .line 7
    new-instance v11, Lcom/pspdfkit/document/PdfValue$PdfValueType;

    const-string v13, "DICTIONARY"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/document/PdfValue$PdfValueType;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/document/PdfValue$PdfValueType;->DICTIONARY:Lcom/pspdfkit/document/PdfValue$PdfValueType;

    .line 8
    new-instance v13, Lcom/pspdfkit/document/PdfValue$PdfValueType;

    const-string v15, "STREAM"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/document/PdfValue$PdfValueType;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/document/PdfValue$PdfValueType;->STREAM:Lcom/pspdfkit/document/PdfValue$PdfValueType;

    .line 9
    new-instance v15, Lcom/pspdfkit/document/PdfValue$PdfValueType;

    const-string v14, "NULLOBJ"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/document/PdfValue$PdfValueType;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/document/PdfValue$PdfValueType;->NULLOBJ:Lcom/pspdfkit/document/PdfValue$PdfValueType;

    const/16 v14, 0x9

    new-array v14, v14, [Lcom/pspdfkit/document/PdfValue$PdfValueType;

    aput-object v0, v14, v2

    aput-object v1, v14, v4

    aput-object v3, v14, v6

    aput-object v5, v14, v8

    aput-object v7, v14, v10

    const/4 v0, 0x5

    aput-object v9, v14, v0

    const/4 v0, 0x6

    aput-object v11, v14, v0

    const/4 v0, 0x7

    aput-object v13, v14, v0

    aput-object v15, v14, v12

    .line 10
    sput-object v14, Lcom/pspdfkit/document/PdfValue$PdfValueType;->$VALUES:[Lcom/pspdfkit/document/PdfValue$PdfValueType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/document/PdfValue$PdfValueType;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/document/PdfValue$PdfValueType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/document/PdfValue$PdfValueType;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/document/PdfValue$PdfValueType;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/document/PdfValue$PdfValueType;->$VALUES:[Lcom/pspdfkit/document/PdfValue$PdfValueType;

    invoke-virtual {v0}, [Lcom/pspdfkit/document/PdfValue$PdfValueType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/document/PdfValue$PdfValueType;

    return-object v0
.end method

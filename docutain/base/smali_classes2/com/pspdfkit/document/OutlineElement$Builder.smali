.class public final Lcom/pspdfkit/document/OutlineElement$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/document/OutlineElement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private action:Lcom/pspdfkit/annotations/actions/Action;

.field private children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/OutlineElement;",
            ">;"
        }
    .end annotation
.end field

.field private color:I

.field private isExpanded:Z

.field private pageLabel:Ljava/lang/String;

.field private style:I

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/String;I)V
    .locals 2

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, -0x1000000

    .line 28
    iput v0, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->color:I

    const/4 v0, 0x0

    .line 31
    iput v0, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->style:I

    .line 34
    iput-boolean v0, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->isExpanded:Z

    const/4 v1, 0x0

    .line 36
    iput-object v1, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->action:Lcom/pspdfkit/annotations/actions/Action;

    .line 39
    iput-object v1, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->pageLabel:Ljava/lang/String;

    .line 43
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->children:Ljava/util/List;

    const-string v1, "document"

    .line 65
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "title"

    .line 66
    invoke-static {p2, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    iput-object p2, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->title:Ljava/lang/String;

    .line 68
    new-instance p2, Lcom/pspdfkit/annotations/actions/GoToAction;

    invoke-direct {p2, p3}, Lcom/pspdfkit/annotations/actions/GoToAction;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->action:Lcom/pspdfkit/annotations/actions/Action;

    .line 69
    invoke-interface {p1, p3, v0}, Lcom/pspdfkit/document/PdfDocument;->getPageLabel(IZ)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->pageLabel:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, -0x1000000

    .line 2
    iput v0, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->color:I

    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->style:I

    .line 8
    iput-boolean v0, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->isExpanded:Z

    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->action:Lcom/pspdfkit/annotations/actions/Action;

    .line 13
    iput-object v0, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->pageLabel:Ljava/lang/String;

    .line 17
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->children:Ljava/util/List;

    const-string v0, "title"

    .line 25
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iput-object p1, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->title:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public build()Lcom/pspdfkit/document/OutlineElement;
    .locals 10

    .line 1
    new-instance v9, Lcom/pspdfkit/document/OutlineElement;

    iget-object v1, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->title:Ljava/lang/String;

    iget v2, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->color:I

    iget v3, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->style:I

    iget-boolean v4, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->isExpanded:Z

    iget-object v5, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->action:Lcom/pspdfkit/annotations/actions/Action;

    iget-object v6, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->pageLabel:Ljava/lang/String;

    iget-object v7, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->children:Ljava/util/List;

    const/4 v8, 0x0

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/document/OutlineElement;-><init>(Ljava/lang/String;IIZLcom/pspdfkit/annotations/actions/Action;Ljava/lang/String;Ljava/util/List;Lcom/pspdfkit/document/OutlineElement-IA;)V

    return-object v9
.end method

.method public setAction(Lcom/pspdfkit/annotations/actions/Action;)Lcom/pspdfkit/document/OutlineElement$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->action:Lcom/pspdfkit/annotations/actions/Action;

    return-object p0
.end method

.method public setChildren(Ljava/util/List;)Lcom/pspdfkit/document/OutlineElement$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/OutlineElement;",
            ">;)",
            "Lcom/pspdfkit/document/OutlineElement$Builder;"
        }
    .end annotation

    const-string v0, "children"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->children:Ljava/util/List;

    return-object p0
.end method

.method public setColor(I)Lcom/pspdfkit/document/OutlineElement$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->color:I

    return-object p0
.end method

.method public setExpanded(Z)Lcom/pspdfkit/document/OutlineElement$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->isExpanded:Z

    return-object p0
.end method

.method public setPageLabel(Ljava/lang/String;)Lcom/pspdfkit/document/OutlineElement$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->pageLabel:Ljava/lang/String;

    return-object p0
.end method

.method public setStyle(I)Lcom/pspdfkit/document/OutlineElement$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->style:I

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/pspdfkit/document/OutlineElement$Builder;
    .locals 2

    const-string v0, "title"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/document/OutlineElement$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.class public Lcom/pspdfkit/document/download/DownloadJob;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/document/download/DownloadJob$ProgressListener;,
        Lcom/pspdfkit/document/download/DownloadJob$ProgressListenerAdapter;
    }
.end annotation


# static fields
.field private static final BUFFER_SIZE:I = 0x2000

.field private static final LOG_TAG:Ljava/lang/String; = "PSPDFKit.DownloadJob"


# instance fields
.field private final downloadDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

.field private progressListenerDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

.field private final progressProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/processors/BehaviorProcessor<",
            "Lcom/pspdfkit/document/download/Progress;",
            ">;"
        }
    .end annotation
.end field

.field private final request:Lcom/pspdfkit/document/download/DownloadRequest;


# direct methods
.method static bridge synthetic -$$Nest$fgetprogressProcessor(Lcom/pspdfkit/document/download/DownloadJob;)Lio/reactivex/rxjava3/processors/BehaviorProcessor;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/document/download/DownloadJob;->progressProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetrequest(Lcom/pspdfkit/document/download/DownloadJob;)Lcom/pspdfkit/document/download/DownloadRequest;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    return-object p0
.end method

.method private constructor <init>(Lcom/pspdfkit/document/download/DownloadRequest;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "request"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    .line 4
    invoke-static {}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->create()Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/document/download/DownloadJob;->progressProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/document/download/DownloadJob;->startDownloadTask()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/document/download/DownloadJob;->downloadDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public static startDownload(Lcom/pspdfkit/document/download/DownloadRequest;)Lcom/pspdfkit/document/download/DownloadJob;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/document/download/DownloadJob;

    invoke-direct {v0, p0}, Lcom/pspdfkit/document/download/DownloadJob;-><init>(Lcom/pspdfkit/document/download/DownloadRequest;)V

    return-object v0
.end method

.method private startDownloadTask()Lio/reactivex/rxjava3/disposables/Disposable;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/document/download/DownloadJob$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/document/download/DownloadJob$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/document/download/DownloadJob;)V

    sget-object v1, Lio/reactivex/rxjava3/core/BackpressureStrategy;->MISSING:Lio/reactivex/rxjava3/core/BackpressureStrategy;

    invoke-static {v0, v1}, Lio/reactivex/rxjava3/core/Flowable;->create(Lio/reactivex/rxjava3/core/FlowableOnSubscribe;Lio/reactivex/rxjava3/core/BackpressureStrategy;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    .line 73
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Flowable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/document/download/DownloadJob$2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/document/download/DownloadJob$2;-><init>(Lcom/pspdfkit/document/download/DownloadJob;)V

    .line 74
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Flowable;->subscribeWith(Lorg/reactivestreams/Subscriber;)Lorg/reactivestreams/Subscriber;

    move-result-object v0

    check-cast v0, Lio/reactivex/rxjava3/disposables/Disposable;

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/download/DownloadJob;->downloadDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/document/download/DownloadJob;->progressListenerDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    :cond_0
    return-void
.end method

.method public getOutputFile()Ljava/io/File;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v0, v0, Lcom/pspdfkit/document/download/DownloadRequest;->outputFile:Ljava/io/File;

    return-object v0
.end method

.method public getProgress()Lio/reactivex/rxjava3/core/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Lcom/pspdfkit/document/download/Progress;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/download/DownloadJob;->progressProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->onBackpressureDrop()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    return-object v0
.end method

.method public isComplete()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/download/DownloadJob;->progressProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->hasComplete()Z

    move-result v0

    return v0
.end method

.method synthetic lambda$startDownloadTask$0$com-pspdfkit-document-download-DownloadJob(Lio/reactivex/rxjava3/core/FlowableEmitter;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v0, v0, Lcom/pspdfkit/document/download/DownloadRequest;->outputFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-boolean v0, v0, Lcom/pspdfkit/document/download/DownloadRequest;->overwriteExisting:Z

    if-nez v0, :cond_1

    .line 2
    invoke-interface {p1}, Lio/reactivex/rxjava3/core/FlowableEmitter;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lio/reactivex/rxjava3/core/FlowableEmitter;->onComplete()V

    :cond_0
    return-void

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-boolean v1, v0, Lcom/pspdfkit/document/download/DownloadRequest;->useTemporaryOutputFile:Z

    if-eqz v1, :cond_2

    .line 7
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v1, v1, Lcom/pspdfkit/document/download/DownloadRequest;->outputFile:Ljava/io/File;

    .line 8
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v3, v3, Lcom/pspdfkit/document/download/DownloadRequest;->outputFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".tmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 9
    :cond_2
    iget-object v0, v0, Lcom/pspdfkit/document/download/DownloadRequest;->outputFile:Ljava/io/File;

    .line 11
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v1, v1, Lcom/pspdfkit/document/download/DownloadRequest;->outputFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_4

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v1, v1, Lcom/pspdfkit/document/download/DownloadRequest;->outputFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_1

    .line 13
    :cond_3
    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Output folder did not exists and could not be created. Folder: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v1, v1, Lcom/pspdfkit/document/download/DownloadRequest;->outputFile:Ljava/io/File;

    .line 15
    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 18
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v1, v1, Lcom/pspdfkit/document/download/DownloadRequest;->outputFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 19
    iget-object v1, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v1, v1, Lcom/pspdfkit/document/download/DownloadRequest;->outputFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_5

    goto :goto_2

    .line 20
    :cond_5
    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Output file already existed and could not be deleted before downloading. File: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v1, v1, Lcom/pspdfkit/document/download/DownloadRequest;->outputFile:Ljava/io/File;

    .line 22
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 25
    :cond_6
    :goto_2
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 26
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_7

    goto :goto_3

    .line 27
    :cond_7
    new-instance p1, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Download file already existed and could not be deleted before downloading. File: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 29
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    :goto_3
    const/4 v1, 0x0

    .line 32
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    :try_start_1
    iget-object v3, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v3, v3, Lcom/pspdfkit/document/download/DownloadRequest;->source:Lcom/pspdfkit/document/download/source/DownloadSource;

    invoke-interface {v3}, Lcom/pspdfkit/document/download/source/DownloadSource;->open()Ljava/io/InputStream;

    move-result-object v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 34
    :try_start_2
    iget-object v4, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v4, v4, Lcom/pspdfkit/document/download/DownloadRequest;->source:Lcom/pspdfkit/document/download/source/DownloadSource;

    invoke-interface {v4}, Lcom/pspdfkit/document/download/source/DownloadSource;->getLength()J

    move-result-wide v4

    const/16 v6, 0x2000

    new-array v7, v6, [B

    const-wide/16 v8, 0x0

    .line 38
    :goto_4
    invoke-virtual {v3, v7, v1, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v10

    const/4 v11, -0x1

    if-le v10, v11, :cond_9

    invoke-interface {p1}, Lio/reactivex/rxjava3/core/FlowableEmitter;->isCancelled()Z

    move-result v11

    if-nez v11, :cond_9

    .line 39
    invoke-virtual {v2, v7, v1, v10}, Ljava/io/FileOutputStream;->write([BII)V

    int-to-long v10, v10

    add-long/2addr v8, v10

    .line 41
    new-instance v10, Lcom/pspdfkit/document/download/Progress;

    invoke-direct {v10, v8, v9, v4, v5}, Lcom/pspdfkit/document/download/Progress;-><init>(JJ)V

    .line 42
    invoke-interface {p1, v10}, Lio/reactivex/rxjava3/core/FlowableEmitter;->onNext(Ljava/lang/Object;)V

    goto :goto_4

    .line 45
    :cond_9
    invoke-interface {p1}, Lio/reactivex/rxjava3/core/FlowableEmitter;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 47
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 61
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    return-void

    .line 62
    :cond_a
    :try_start_5
    iget-object v4, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-boolean v5, v4, Lcom/pspdfkit/document/download/DownloadRequest;->useTemporaryOutputFile:Z

    if-eqz v5, :cond_c

    .line 63
    iget-object v4, v4, Lcom/pspdfkit/document/download/DownloadRequest;->outputFile:Ljava/io/File;

    invoke-virtual {v0, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_b

    goto :goto_5

    .line 64
    :cond_b
    new-instance v0, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error moving download from temporary file to output file. Output file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v5, v5, Lcom/pspdfkit/document/download/DownloadRequest;->outputFile:Ljava/io/File;

    .line 66
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_c
    :goto_5
    invoke-interface {p1}, Lio/reactivex/rxjava3/core/FlowableEmitter;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_d

    .line 71
    invoke-interface {p1}, Lio/reactivex/rxjava3/core/FlowableEmitter;->onComplete()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 73
    :cond_d
    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_8

    :catchall_0
    move-exception v0

    if-eqz v3, :cond_e

    .line 74
    :try_start_8
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_6

    :catchall_1
    move-exception v3

    :try_start_9
    invoke-virtual {v0, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :cond_e
    :goto_6
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :catchall_2
    move-exception v0

    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    goto :goto_7

    :catchall_3
    move-exception v2

    :try_start_b
    invoke-virtual {v0, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_7
    throw v0
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0

    :catch_0
    move-exception v0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.DownloadJob"

    const-string v3, "Download failed!"

    .line 104
    invoke-static {v2, v0, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    invoke-interface {p1}, Lio/reactivex/rxjava3/core/FlowableEmitter;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_f

    .line 106
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error while downloading from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/pspdfkit/document/download/DownloadJob;->request:Lcom/pspdfkit/document/download/DownloadRequest;

    iget-object v3, v3, Lcom/pspdfkit/document/download/DownloadRequest;->source:Lcom/pspdfkit/document/download/source/DownloadSource;

    .line 107
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 108
    invoke-interface {p1, v1}, Lio/reactivex/rxjava3/core/FlowableEmitter;->onError(Ljava/lang/Throwable;)V

    :cond_f
    :goto_8
    return-void
.end method

.method public setProgressListener(Lcom/pspdfkit/document/download/DownloadJob$ProgressListener;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/download/DownloadJob;->progressListenerDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/document/download/DownloadJob;->progressListenerDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_0
    if-eqz p1, :cond_1

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/document/download/DownloadJob;->progressProcessor:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 8
    invoke-virtual {v0}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->onBackpressureDrop()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    .line 9
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Flowable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/document/download/DownloadJob$1;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/document/download/DownloadJob$1;-><init>(Lcom/pspdfkit/document/download/DownloadJob;Lcom/pspdfkit/document/download/DownloadJob$ProgressListener;)V

    .line 10
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Flowable;->subscribeWith(Lorg/reactivestreams/Subscriber;)Lorg/reactivestreams/Subscriber;

    move-result-object p1

    check-cast p1, Lio/reactivex/rxjava3/disposables/Disposable;

    iput-object p1, p0, Lcom/pspdfkit/document/download/DownloadJob;->progressListenerDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_1
    return-void
.end method

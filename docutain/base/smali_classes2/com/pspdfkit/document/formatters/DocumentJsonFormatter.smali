.class public final Lcom/pspdfkit/document/formatters/DocumentJsonFormatter;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static exportDocumentJson(Lcom/pspdfkit/document/PdfDocument;Ljava/io/OutputStream;)V
    .locals 2

    .line 1
    invoke-static {p0, p1}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter;->validateSerializationArguments(Lcom/pspdfkit/document/PdfDocument;Ljava/io/OutputStream;)V

    .line 2
    check-cast p0, Lcom/pspdfkit/internal/zf;

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v0

    .line 5
    new-instance v1, Lcom/pspdfkit/internal/sl;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/sl;-><init>(Ljava/io/OutputStream;)V

    .line 6
    invoke-static {p0}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter;->syncDirtyAnnotations(Lcom/pspdfkit/internal/zf;)V

    const/4 p0, 0x0

    .line 7
    invoke-static {v0, p0, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentJSONFormatter;->exportJson(Lcom/pspdfkit/internal/jni/NativeDocument;ILcom/pspdfkit/internal/jni/NativeDataSink;)Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object p0

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativeResult;->getHasError()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 9
    :cond_0
    new-instance p1, Lcom/pspdfkit/document/formatters/DocumentJsonFormatterException;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativeResult;->getErrorString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatterException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static exportDocumentJsonAsync(Lcom/pspdfkit/document/PdfDocument;Ljava/io/OutputStream;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 1
    invoke-static {p0, p1}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter;->validateSerializationArguments(Lcom/pspdfkit/document/PdfDocument;Ljava/io/OutputStream;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/document/PdfDocument;Ljava/io/OutputStream;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    return-object p0
.end method

.method public static importDocumentJson(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/providers/DataProvider;)V
    .locals 3

    .line 1
    invoke-static {p0, p1}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter;->validateDeserializationArguments(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/providers/DataProvider;)V

    .line 2
    check-cast p0, Lcom/pspdfkit/internal/zf;

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v0

    .line 6
    new-instance v1, Lcom/pspdfkit/internal/s7;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/s7;-><init>(Lcom/pspdfkit/document/providers/DataProvider;)V

    .line 9
    invoke-static {p0}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter;->syncDirtyAnnotations(Lcom/pspdfkit/internal/zf;)V

    const/4 p1, 0x0

    .line 12
    invoke-static {p0, p1, v1}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter;->prefetchRemovedAnnotations(Lcom/pspdfkit/internal/zf;ILcom/pspdfkit/internal/jni/NativeDataProvider;)Ljava/util/List;

    move-result-object v2

    .line 14
    invoke-static {v0, p1, v1, p1}, Lcom/pspdfkit/internal/jni/NativeDocumentJSONFormatter;->importJson(Lcom/pspdfkit/internal/jni/NativeDocument;ILcom/pspdfkit/internal/jni/NativeDataProvider;Z)Lcom/pspdfkit/internal/jni/NativeImportDocumentJSONResult;

    move-result-object p1

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeImportDocumentJSONResult;->getResult()Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeResult;->getHasError()Z

    move-result v0

    if-nez v0, :cond_0

    .line 20
    invoke-static {p0, v2, p1}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter;->notifyAnnotationListenersOfImport(Lcom/pspdfkit/internal/zf;Ljava/util/List;Lcom/pspdfkit/internal/jni/NativeImportDocumentJSONResult;)V

    return-void

    .line 21
    :cond_0
    new-instance p0, Lcom/pspdfkit/document/formatters/DocumentJsonFormatterException;

    .line 22
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeImportDocumentJSONResult;->getResult()Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeResult;->getErrorString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatterException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static importDocumentJsonAsync(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/providers/DataProvider;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 1
    invoke-static {p0, p1}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter;->validateDeserializationArguments(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/providers/DataProvider;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/providers/DataProvider;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$exportDocumentJsonAsync$0(Lcom/pspdfkit/document/PdfDocument;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-static {p0, p1}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter;->exportDocumentJson(Lcom/pspdfkit/document/PdfDocument;Ljava/io/OutputStream;)V

    return-void
.end method

.method static synthetic lambda$importDocumentJsonAsync$1(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/providers/DataProvider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-static {p0, p1}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatter;->importDocumentJson(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/providers/DataProvider;)V

    return-void
.end method

.method private static notifyAnnotationListenersOfImport(Lcom/pspdfkit/internal/zf;Ljava/util/List;Lcom/pspdfkit/internal/jni/NativeImportDocumentJSONResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/zf;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Lcom/pspdfkit/internal/jni/NativeImportDocumentJSONResult;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p0

    .line 2
    invoke-interface {p0}, Lcom/pspdfkit/internal/qf;->invalidateCache()V

    .line 5
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeImportDocumentJSONResult;->getUpdatedAnnotations()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/jni/NativeAnnotation;

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAbsolutePageIndex()Ljava/lang/Integer;

    move-result-object v2

    .line 7
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object v1

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 10
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    long-to-int v1, v3

    move-object v3, p0

    check-cast v3, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v3, v2, v1}, Lcom/pspdfkit/internal/r1;->getAnnotation(II)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 11
    invoke-virtual {v3, v1}, Lcom/pspdfkit/internal/r1;->j(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    .line 15
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 16
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->onDetachedFromDocument()V

    .line 17
    move-object v1, p0

    check-cast v1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/r1;->i(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_1

    .line 20
    :cond_2
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeImportDocumentJSONResult;->getAddedAnnotations()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/jni/NativeAnnotation;

    .line 21
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAbsolutePageIndex()Ljava/lang/Integer;

    move-result-object v0

    .line 22
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object p2

    if-eqz v0, :cond_3

    if-eqz p2, :cond_3

    .line 25
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    long-to-int p2, v1

    move-object v1, p0

    check-cast v1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v1, v0, p2}, Lcom/pspdfkit/internal/r1;->getAnnotation(II)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 26
    invoke-virtual {v1, p2}, Lcom/pspdfkit/internal/r1;->h(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_2

    :cond_4
    return-void
.end method

.method private static prefetchRemovedAnnotations(Lcom/pspdfkit/internal/zf;ILcom/pspdfkit/internal/jni/NativeDataProvider;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/zf;",
            "I",
            "Lcom/pspdfkit/internal/jni/NativeDataProvider;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v0

    const/4 v1, 0x0

    .line 3
    invoke-static {v0, p1, p2, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentJSONFormatter;->getSkippedAnnotations(Lcom/pspdfkit/internal/jni/NativeDocument;ILcom/pspdfkit/internal/jni/NativeDataProvider;Z)Lcom/pspdfkit/internal/jni/NativeSkippedAnnotationResult;

    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeSkippedAnnotationResult;->getResult()Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeResult;->getHasError()Z

    move-result p2

    if-nez p2, :cond_2

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeSkippedAnnotationResult;->getSkippedAnnotations()Ljava/util/ArrayList;

    move-result-object p1

    .line 10
    new-instance p2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 11
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p0

    .line 12
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/jni/NativeAnnotation;

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object v1

    .line 14
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAbsolutePageIndex()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 16
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    long-to-int v2, v1

    move-object v1, p0

    check-cast v1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v1, v0, v2}, Lcom/pspdfkit/internal/r1;->getAnnotation(II)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object p2

    .line 17
    :cond_2
    new-instance p0, Lcom/pspdfkit/document/formatters/DocumentJsonFormatterException;

    .line 18
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeSkippedAnnotationResult;->getResult()Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeResult;->getErrorString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/document/formatters/DocumentJsonFormatterException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static syncDirtyAnnotations(Lcom/pspdfkit/internal/zf;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p0

    .line 2
    invoke-interface {p0}, Lcom/pspdfkit/internal/qf;->a()V

    return-void
.end method

.method private static validateDeserializationArguments(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/providers/DataProvider;)V
    .locals 3

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "dataProvider"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    invoke-interface {p0}, Lcom/pspdfkit/document/PdfDocument;->getDocumentSources()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    const/4 p1, 0x1

    if-gt p0, p1, :cond_0

    return-void

    .line 108
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Can\'t apply annotations to documents with more than one document source."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static validateSerializationArguments(Lcom/pspdfkit/document/PdfDocument;Ljava/io/OutputStream;)V
    .locals 3

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "outputStream"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    invoke-interface {p0}, Lcom/pspdfkit/document/PdfDocument;->getDocumentSources()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    const/4 p1, 0x1

    if-gt p0, p1, :cond_0

    return-void

    .line 108
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Can\'t serialize documents with more than one document source."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

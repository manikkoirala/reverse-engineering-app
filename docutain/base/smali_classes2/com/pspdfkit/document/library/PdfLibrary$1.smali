.class Lcom/pspdfkit/document/library/PdfLibrary$1;
.super Lcom/pspdfkit/internal/jni/NativeDocumentLibraryQueryResultHandler;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/document/library/PdfLibrary;->search(Ljava/lang/String;Lcom/pspdfkit/document/library/QueryOptions;Lcom/pspdfkit/document/library/QueryResultListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/document/library/PdfLibrary;

.field final synthetic val$resultListener:Lcom/pspdfkit/document/library/QueryResultListener;


# direct methods
.method constructor <init>(Lcom/pspdfkit/document/library/PdfLibrary;Lcom/pspdfkit/document/library/QueryResultListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/document/library/PdfLibrary$1;->this$0:Lcom/pspdfkit/document/library/PdfLibrary;

    iput-object p2, p0, Lcom/pspdfkit/document/library/PdfLibrary$1;->val$resultListener:Lcom/pspdfkit/document/library/QueryResultListener;

    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryQueryResultHandler;-><init>()V

    return-void
.end method

.method static synthetic lambda$previewHandler$1(Lcom/pspdfkit/document/library/QueryResultListener;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1

    .line 1
    :try_start_0
    invoke-interface {p0, p1, p2}, Lcom/pspdfkit/document/library/QueryResultListener;->onSearchPreviewsGenerated(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "PSPDFKit.PdfLibrary"

    const-string v0, "Exception in onSearchPreviewsGenerated callback!"

    .line 3
    invoke-static {p2, p0, v0, p1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    throw p0
.end method

.method static synthetic lambda$successHandler$0(Lcom/pspdfkit/document/library/QueryResultListener;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1

    .line 1
    :try_start_0
    invoke-interface {p0, p1, p2}, Lcom/pspdfkit/document/library/QueryResultListener;->onSearchCompleted(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "PSPDFKit.PdfLibrary"

    const-string v0, "Exception in onSearchCompleted callback!"

    .line 3
    invoke-static {p2, p0, v0, p1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    throw p0
.end method


# virtual methods
.method public previewHandler(Lcom/pspdfkit/internal/jni/NativeDocumentLibraryQuery;Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/jni/NativeDocumentLibraryQuery;",
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/internal/jni/NativeDocumentLibraryPreviewResult;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryPreviewResult;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryPreviewResult;->getUid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryPreviewResult;->getUid()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    :cond_0
    new-instance v2, Lcom/pspdfkit/document/library/QueryPreviewResult;

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryPreviewResult;->getUid()Ljava/lang/String;

    move-result-object v5

    .line 7
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryPreviewResult;->getPageIndex()J

    move-result-wide v3

    long-to-int v6, v3

    .line 8
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryPreviewResult;->getRange()Lcom/pspdfkit/datastructures/Range;

    move-result-object v7

    .line 9
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryPreviewResult;->getPreviewText()Ljava/lang/String;

    move-result-object v8

    .line 10
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryPreviewResult;->getRangeInPreviewText()Lcom/pspdfkit/datastructures/Range;

    move-result-object v9

    move-object v4, v2

    invoke-direct/range {v4 .. v9}, Lcom/pspdfkit/document/library/QueryPreviewResult;-><init>(Ljava/lang/String;ILcom/pspdfkit/datastructures/Range;Ljava/lang/String;Lcom/pspdfkit/datastructures/Range;)V

    .line 12
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryPreviewResult;->getUid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 15
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryQuery;->getSearchString()Ljava/lang/String;

    move-result-object p1

    .line 23
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/u;

    const/4 v1, 0x5

    .line 24
    invoke-virtual {p2, v1}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    .line 25
    invoke-virtual {p2}, Lio/reactivex/rxjava3/core/Scheduler;->createWorker()Lio/reactivex/rxjava3/core/Scheduler$Worker;

    move-result-object p2

    iget-object v1, p0, Lcom/pspdfkit/document/library/PdfLibrary$1;->val$resultListener:Lcom/pspdfkit/document/library/QueryResultListener;

    new-instance v2, Lcom/pspdfkit/document/library/PdfLibrary$1$$ExternalSyntheticLambda1;

    invoke-direct {v2, v1, p1, v0}, Lcom/pspdfkit/document/library/PdfLibrary$1$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/document/library/QueryResultListener;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {p2, v2}, Lio/reactivex/rxjava3/core/Scheduler$Worker;->schedule(Ljava/lang/Runnable;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public successHandler(Lcom/pspdfkit/internal/jni/NativeDocumentLibraryQuery;Ljava/util/HashMap;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/jni/NativeDocumentLibraryQuery;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 3
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 4
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 5
    invoke-virtual {v4}, Ljava/lang/Long;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 7
    :cond_0
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 10
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryQuery;->getSearchString()Ljava/lang/String;

    move-result-object p1

    .line 18
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/u;

    const/4 v1, 0x5

    .line 19
    invoke-virtual {p2, v1}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    .line 20
    invoke-virtual {p2}, Lio/reactivex/rxjava3/core/Scheduler;->createWorker()Lio/reactivex/rxjava3/core/Scheduler$Worker;

    move-result-object p2

    iget-object v1, p0, Lcom/pspdfkit/document/library/PdfLibrary$1;->val$resultListener:Lcom/pspdfkit/document/library/QueryResultListener;

    new-instance v2, Lcom/pspdfkit/document/library/PdfLibrary$1$$ExternalSyntheticLambda0;

    invoke-direct {v2, v1, p1, v0}, Lcom/pspdfkit/document/library/PdfLibrary$1$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/document/library/QueryResultListener;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {p2, v2}, Lio/reactivex/rxjava3/core/Scheduler$Worker;->schedule(Ljava/lang/Runnable;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

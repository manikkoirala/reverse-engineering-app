.class public final Lcom/pspdfkit/document/library/IndexingOptions$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/document/library/IndexingOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private ignoreAnnotations:Z

.field private ignoreDocumentText:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/document/library/IndexingOptions$Builder;->ignoreAnnotations:Z

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/document/library/IndexingOptions$Builder;->ignoreDocumentText:Z

    return-void
.end method


# virtual methods
.method public build()Lcom/pspdfkit/document/library/IndexingOptions;
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/document/library/IndexingOptions$Builder;->ignoreAnnotations:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/pspdfkit/document/library/IndexingOptions$Builder;->ignoreDocumentText:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "It is not allowed to ignore annotations and document text at the same time."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6
    :cond_1
    :goto_0
    new-instance v0, Lcom/pspdfkit/document/library/IndexingOptions;

    iget-boolean v1, p0, Lcom/pspdfkit/document/library/IndexingOptions$Builder;->ignoreAnnotations:Z

    iget-boolean v2, p0, Lcom/pspdfkit/document/library/IndexingOptions$Builder;->ignoreDocumentText:Z

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/document/library/IndexingOptions;-><init>(ZZ)V

    return-object v0
.end method

.method public setIgnoreAnnotations(Z)Lcom/pspdfkit/document/library/IndexingOptions$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/document/library/IndexingOptions$Builder;->ignoreAnnotations:Z

    return-object p0
.end method

.method public setIgnoreDocumentText(Z)Lcom/pspdfkit/document/library/IndexingOptions$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/document/library/IndexingOptions$Builder;->ignoreDocumentText:Z

    return-object p0
.end method

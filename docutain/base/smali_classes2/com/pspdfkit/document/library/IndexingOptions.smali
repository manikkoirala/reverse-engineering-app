.class public final Lcom/pspdfkit/document/library/IndexingOptions;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/document/library/IndexingOptions$Builder;
    }
.end annotation


# static fields
.field static final DEFAULT_OPTIONS:Lcom/pspdfkit/document/library/IndexingOptions;


# instance fields
.field private final ignoreAnnotations:Z

.field private final ignoreDocumentText:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/document/library/IndexingOptions$Builder;

    invoke-direct {v0}, Lcom/pspdfkit/document/library/IndexingOptions$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/pspdfkit/document/library/IndexingOptions$Builder;->build()Lcom/pspdfkit/document/library/IndexingOptions;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/document/library/IndexingOptions;->DEFAULT_OPTIONS:Lcom/pspdfkit/document/library/IndexingOptions;

    return-void
.end method

.method constructor <init>(ZZ)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/document/library/IndexingOptions;->ignoreAnnotations:Z

    .line 3
    iput-boolean p2, p0, Lcom/pspdfkit/document/library/IndexingOptions;->ignoreDocumentText:Z

    return-void
.end method


# virtual methods
.method createNativeEnqueueOptions()Ljava/util/EnumSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeEnqueueOptions;",
            ">;"
        }
    .end annotation

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeEnqueueOptions;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 3
    iget-boolean v1, p0, Lcom/pspdfkit/document/library/IndexingOptions;->ignoreAnnotations:Z

    if-eqz v1, :cond_0

    .line 4
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeEnqueueOptions;->IGNORE_ANNOTATIONS:Lcom/pspdfkit/internal/jni/NativeEnqueueOptions;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 7
    :cond_0
    iget-boolean v1, p0, Lcom/pspdfkit/document/library/IndexingOptions;->ignoreDocumentText:Z

    if-eqz v1, :cond_1

    .line 8
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeEnqueueOptions;->IGNORE_DOCUMENT_TEXT:Lcom/pspdfkit/internal/jni/NativeEnqueueOptions;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object v0
.end method

.class Lcom/pspdfkit/document/library/LibraryObserverShim;
.super Lcom/pspdfkit/internal/jni/NativeDocumentLibraryIndexingObserver;
.source "SourceFile"


# instance fields
.field private final indexingListener:Lcom/pspdfkit/document/library/LibraryIndexingListener;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/document/library/LibraryIndexingListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryIndexingObserver;-><init>()V

    const-string v0, "libraryIndexingListener"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/document/library/LibraryObserverShim;->indexingListener:Lcom/pspdfkit/document/library/LibraryIndexingListener;

    return-void
.end method


# virtual methods
.method public didFinishIndexingDocument(Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;Ljava/lang/String;Z)V
    .locals 3

    const-string v0, "documentLibrary"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string p1, "uid"

    .line 55
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, p1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    iget-object p1, p0, Lcom/pspdfkit/document/library/LibraryObserverShim;->indexingListener:Lcom/pspdfkit/document/library/LibraryIndexingListener;

    invoke-interface {p1, p2, p3}, Lcom/pspdfkit/document/library/LibraryIndexingListener;->onFinishIndexingDocument(Ljava/lang/String;Z)V

    return-void
.end method

.method public didIndexPage(Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    const-string v0, "documentLibrary"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string p1, "uid"

    .line 55
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, p1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string p1, "text"

    .line 108
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-static {p4, p1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 160
    iget-object p1, p0, Lcom/pspdfkit/document/library/LibraryObserverShim;->indexingListener:Lcom/pspdfkit/document/library/LibraryIndexingListener;

    invoke-interface {p1, p2, p3, p4}, Lcom/pspdfkit/document/library/LibraryIndexingListener;->onPageIndexed(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public getSubscribedEvents()Ljava/util/EnumSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeObservingEvents;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/LibraryObserverShim;->indexingListener:Lcom/pspdfkit/document/library/LibraryIndexingListener;

    invoke-interface {v0}, Lcom/pspdfkit/document/library/LibraryIndexingListener;->enableOnPageIndexedEvents()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    const-class v0, Lcom/pspdfkit/internal/jni/NativeObservingEvents;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0

    .line 4
    :cond_0
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeObservingEvents;->START_INDEXING:Lcom/pspdfkit/internal/jni/NativeObservingEvents;

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeObservingEvents;->FINISH_INDEXING:Lcom/pspdfkit/internal/jni/NativeObservingEvents;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public willStartIndexingDocument(Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;Ljava/lang/String;)V
    .locals 3

    const-string v0, "documentLibrary"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string p1, "uid"

    .line 55
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, p1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    iget-object p1, p0, Lcom/pspdfkit/document/library/LibraryObserverShim;->indexingListener:Lcom/pspdfkit/document/library/LibraryIndexingListener;

    invoke-interface {p1, p2}, Lcom/pspdfkit/document/library/LibraryIndexingListener;->onStartIndexingDocument(Ljava/lang/String;)V

    return-void
.end method

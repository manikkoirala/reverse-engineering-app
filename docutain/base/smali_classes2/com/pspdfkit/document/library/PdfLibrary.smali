.class public final Lcom/pspdfkit/document/library/PdfLibrary;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/document/library/PdfLibrary$Tokenizer;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "PSPDFKit.PdfLibrary"

.field public static final PORTER_TOKENIZER:Ljava/lang/String; = "PorterTokenizer"

.field public static final UNICODE_TOKENIZER:Ljava/lang/String; = "UnicodeTokenizer"


# instance fields
.field final libraryObserverMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/pspdfkit/document/library/LibraryIndexingListener;",
            "Lcom/pspdfkit/document/library/LibraryObserverShim;",
            ">;"
        }
    .end annotation
.end field

.field final nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->libraryObserverMapping:Ljava/util/Map;

    .line 10
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->INDEXED_FTS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13
    sget-object v4, Lcom/pspdfkit/internal/jni/NativeThreadPriority;->VERY_LOW:Lcom/pspdfkit/internal/jni/NativeThreadPriority;

    sget-object v6, Lcom/pspdfkit/internal/jni/NativeFTSVersion;->HIGHEST_AVAILABLE:Lcom/pspdfkit/internal/jni/NativeFTSVersion;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v1, p1

    move-object v5, p2

    invoke-static/range {v1 .. v6}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->create(Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeDatabaseEncryptionProvider;Lcom/pspdfkit/internal/jni/NativeEncryptionKeyProvider;Lcom/pspdfkit/internal/jni/NativeThreadPriority;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeFTSVersion;)Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 18
    iput-object p1, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    return-void

    .line 19
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string p2, "Could not initialize document library."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 20
    :cond_1
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Your current license does not allow usage of full-text search."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static get(Ljava/lang/String;)Lcom/pspdfkit/document/library/PdfLibrary;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/document/library/PdfLibrary;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/document/library/PdfLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static get(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/document/library/PdfLibrary;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    new-instance v0, Lcom/pspdfkit/document/library/PdfLibrary;

    const-string v1, "PorterTokenizer"

    .line 3
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->porterTokenizerName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string v2, "UnicodeTokenizer"

    .line 5
    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->unicodeTokenizerName()Ljava/lang/String;

    move-result-object p1

    .line 7
    :goto_0
    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/document/library/PdfLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 8
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v0, v3

    const/4 p1, 0x1

    aput-object v1, v0, p1

    const/4 p1, 0x2

    aput-object v2, v0, p1

    const-string p1, "Illegal tokenizer passed in: %s, should be one of %s, %s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public addLibraryIndexingListener(Lcom/pspdfkit/document/library/LibraryIndexingListener;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->libraryObserverMapping:Ljava/util/Map;

    monitor-enter v0

    if-eqz p1, :cond_0

    .line 4
    :try_start_0
    new-instance v1, Lcom/pspdfkit/document/library/LibraryObserverShim;

    invoke-direct {v1, p1}, Lcom/pspdfkit/document/library/LibraryObserverShim;-><init>(Lcom/pspdfkit/document/library/LibraryIndexingListener;)V

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/document/library/PdfLibrary;->libraryObserverMapping:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->addIndexingObserver(Lcom/pspdfkit/internal/jni/NativeDocumentLibraryIndexingObserver;)V

    .line 7
    monitor-exit v0

    return-void

    .line 8
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener must not be null!"

    invoke-direct {p1, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 12
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public clearIndex()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->clearAllIndexes()V

    return-void
.end method

.method public enqueueDocumentSources(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/document/DocumentSource;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->toDataDescriptor()Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    move-result-object v2

    invoke-static {v2}, Lcom/pspdfkit/internal/fv;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    .line 5
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeLibraryDocumentDescriptor;

    new-instance v4, Lcom/pspdfkit/internal/jni/NativeDocumentDescriptor;

    const/4 v5, 0x0

    invoke-direct {v4, v2, v5}, Lcom/pspdfkit/internal/jni/NativeDocumentDescriptor;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getUid()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v4, v5, v5, v1}, Lcom/pspdfkit/internal/jni/NativeLibraryDocumentDescriptor;-><init>(Lcom/pspdfkit/internal/jni/NativeDocumentDescriptor;[BLjava/util/ArrayList;Ljava/lang/String;)V

    .line 7
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 11
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    const-class v1, Lcom/pspdfkit/internal/jni/NativeEnqueueOptions;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->enqueueDocumentDescriptors(Ljava/util/ArrayList;Ljava/util/EnumSet;)V

    return-void

    .line 12
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Document sources must not be null!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public enqueueDocumentSourcesWithMetadata(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/document/DocumentSource;",
            "[B>;>;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/core/util/Pair;

    .line 3
    iget-object v2, v1, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/pspdfkit/document/DocumentSource;

    .line 4
    invoke-virtual {v2}, Lcom/pspdfkit/document/DocumentSource;->toDataDescriptor()Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    move-result-object v2

    invoke-static {v2}, Lcom/pspdfkit/internal/fv;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    .line 6
    new-instance v3, Lcom/pspdfkit/internal/jni/NativeLibraryDocumentDescriptor;

    new-instance v4, Lcom/pspdfkit/internal/jni/NativeDocumentDescriptor;

    const/4 v5, 0x0

    invoke-direct {v4, v2, v5}, Lcom/pspdfkit/internal/jni/NativeDocumentDescriptor;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    iget-object v2, v1, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, [B

    iget-object v1, v1, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/pspdfkit/document/DocumentSource;

    .line 10
    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getUid()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v4, v2, v5, v1}, Lcom/pspdfkit/internal/jni/NativeLibraryDocumentDescriptor;-><init>(Lcom/pspdfkit/internal/jni/NativeDocumentDescriptor;[BLjava/util/ArrayList;Ljava/lang/String;)V

    .line 11
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 18
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    const-class v1, Lcom/pspdfkit/internal/jni/NativeEnqueueOptions;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->enqueueDocumentDescriptors(Ljava/util/ArrayList;Ljava/util/EnumSet;)V

    return-void

    .line 19
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Document sources must not be null!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public enqueueDocuments(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/PdfDocument;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/pspdfkit/document/library/IndexingOptions;->DEFAULT_OPTIONS:Lcom/pspdfkit/document/library/IndexingOptions;

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/document/library/PdfLibrary;->enqueueDocuments(Ljava/util/List;Lcom/pspdfkit/document/library/IndexingOptions;)V

    return-void
.end method

.method public enqueueDocuments(Ljava/util/List;Lcom/pspdfkit/document/library/IndexingOptions;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/PdfDocument;",
            ">;",
            "Lcom/pspdfkit/document/library/IndexingOptions;",
            ")V"
        }
    .end annotation

    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 3
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/document/PdfDocument;

    .line 4
    check-cast v1, Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 7
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {p2}, Lcom/pspdfkit/document/library/IndexingOptions;->createNativeEnqueueOptions()Ljava/util/EnumSet;

    move-result-object p2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v1, p2}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->enqueueDocuments(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/EnumSet;)V

    return-void

    .line 8
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Indexing options must not be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 9
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Documents array must not be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public enqueueDocumentsWithMetadata(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/document/PdfDocument;",
            "[B>;>;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/pspdfkit/document/library/IndexingOptions;->DEFAULT_OPTIONS:Lcom/pspdfkit/document/library/IndexingOptions;

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/document/library/PdfLibrary;->enqueueDocumentsWithMetadata(Ljava/util/List;Lcom/pspdfkit/document/library/IndexingOptions;)V

    return-void
.end method

.method public enqueueDocumentsWithMetadata(Ljava/util/List;Lcom/pspdfkit/document/library/IndexingOptions;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/document/PdfDocument;",
            "[B>;>;",
            "Lcom/pspdfkit/document/library/IndexingOptions;",
            ")V"
        }
    .end annotation

    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 3
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 4
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/core/util/Pair;

    .line 5
    iget-object v3, v2, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Lcom/pspdfkit/internal/zf;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6
    iget-object v2, v2, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, [B

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {p2}, Lcom/pspdfkit/document/library/IndexingOptions;->createNativeEnqueueOptions()Ljava/util/EnumSet;

    move-result-object p2

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1, p2}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->enqueueDocuments(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/EnumSet;)V

    return-void

    .line 10
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Indexing options must not be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 11
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Documents array must not be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getIndexStatusForUID(Ljava/lang/String;)Lcom/pspdfkit/document/library/LibraryIndexStatus;
    .locals 3

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->indexStatus(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeDocumentLibraryIndexStatusProgress;

    move-result-object p1

    .line 2
    new-instance v0, Lcom/pspdfkit/document/library/LibraryIndexStatus;

    .line 3
    invoke-static {}, Lcom/pspdfkit/document/library/LibraryIndexStatus$Status;->values()[Lcom/pspdfkit/document/library/LibraryIndexStatus$Status;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryIndexStatusProgress;->getIndexStatus()Lcom/pspdfkit/internal/jni/NativeDocumentLibraryIndexStatus;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryIndexStatusProgress;->getProgress()F

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/document/library/LibraryIndexStatus;-><init>(Lcom/pspdfkit/document/library/LibraryIndexStatus$Status;F)V

    return-object v0

    .line 4
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "UID must not be null!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getIndexedUIDs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->indexedUids()Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    return-object v0
.end method

.method public getMetadataForUID(Ljava/lang/String;)[B
    .locals 1

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->metadataForUid(Ljava/lang/String;)[B

    move-result-object p1

    return-object p1

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "UID must not be null!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getQueuedUIDs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->queuedUids()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getSaveReverseText()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->saveReversedText()Z

    move-result v0

    return v0
.end method

.method public isIndexing()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->isIndexing()Z

    move-result v0

    return v0
.end method

.method public removeDocuments(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    .line 2
    instance-of v1, p1, Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    check-cast p1, Ljava/util/ArrayList;

    goto :goto_0

    .line 3
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object p1, v1

    .line 4
    :goto_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->removeDocuments(Ljava/util/ArrayList;)V

    return-void

    .line 5
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "UID must not be null!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public removeLibraryIndexingListener(Lcom/pspdfkit/document/library/LibraryIndexingListener;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->libraryObserverMapping:Ljava/util/Map;

    monitor-enter v0

    if-eqz p1, :cond_1

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/document/library/PdfLibrary;->libraryObserverMapping:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    iget-object v2, p0, Lcom/pspdfkit/document/library/PdfLibrary;->libraryObserverMapping:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryIndexingObserver;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->removeIndexingObserver(Lcom/pspdfkit/internal/jni/NativeDocumentLibraryIndexingObserver;)V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/document/library/PdfLibrary;->libraryObserverMapping:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    :cond_0
    monitor-exit v0

    return-void

    .line 9
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener must not be null!"

    invoke-direct {p1, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 14
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public search(Ljava/lang/String;Lcom/pspdfkit/document/library/QueryOptions;Lcom/pspdfkit/document/library/QueryResultListener;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    if-nez p2, :cond_0

    .line 1
    new-instance v2, Lcom/pspdfkit/document/library/QueryOptions$Builder;

    invoke-direct {v2}, Lcom/pspdfkit/document/library/QueryOptions$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/pspdfkit/document/library/QueryOptions$Builder;->build()Lcom/pspdfkit/document/library/QueryOptions;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object/from16 v2, p2

    :goto_0
    if-eqz p1, :cond_2

    if-eqz v1, :cond_1

    .line 8
    new-instance v15, Lcom/pspdfkit/document/library/PdfLibrary$1;

    invoke-direct {v15, v0, v1}, Lcom/pspdfkit/document/library/PdfLibrary$1;-><init>(Lcom/pspdfkit/document/library/PdfLibrary;Lcom/pspdfkit/document/library/QueryResultListener;)V

    .line 82
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryQuery;

    .line 84
    invoke-virtual {v2}, Lcom/pspdfkit/document/library/QueryOptions;->shouldIgnoreAnnotations()Z

    move-result v5

    .line 85
    invoke-virtual {v2}, Lcom/pspdfkit/document/library/QueryOptions;->shouldIgnoreDocumentText()Z

    move-result v6

    .line 86
    invoke-virtual {v2}, Lcom/pspdfkit/document/library/QueryOptions;->shouldMatchExactPhrases()Z

    move-result v7

    .line 87
    invoke-virtual {v2}, Lcom/pspdfkit/document/library/QueryOptions;->shouldMatchExactWords()Z

    move-result v8

    .line 88
    invoke-virtual {v2}, Lcom/pspdfkit/document/library/QueryOptions;->getMaximumSearchResultsPerDocument()I

    move-result v9

    .line 89
    invoke-virtual {v2}, Lcom/pspdfkit/document/library/QueryOptions;->getMaximumSearchResultsTotal()I

    move-result v10

    .line 90
    invoke-virtual {v2}, Lcom/pspdfkit/document/library/QueryOptions;->getMaximumPreviewResultsPerDocument()I

    move-result v11

    .line 91
    invoke-virtual {v2}, Lcom/pspdfkit/document/library/QueryOptions;->getMaximumPreviewResultsTotal()I

    move-result v12

    .line 92
    invoke-virtual {v2}, Lcom/pspdfkit/document/library/QueryOptions;->shouldGenerateTextPreviews()Z

    move-result v13

    .line 93
    invoke-virtual {v2}, Lcom/pspdfkit/document/library/QueryOptions;->getPreviewRange()Lcom/pspdfkit/datastructures/Range;

    move-result-object v14

    move-object v3, v1

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v14}, Lcom/pspdfkit/internal/jni/NativeDocumentLibraryQuery;-><init>(Ljava/lang/String;ZZZZIIIIZLcom/pspdfkit/datastructures/Range;)V

    .line 95
    iget-object v2, v0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {v2, v1, v15}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->query(Lcom/pspdfkit/internal/jni/NativeDocumentLibraryQuery;Lcom/pspdfkit/internal/jni/NativeDocumentLibraryQueryResultHandler;)V

    return-void

    .line 96
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Result listener must not be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 97
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Search string must not be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setSaveReverseText(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->setSaveReversedText(Z)V

    return-void
.end method

.method public size()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->indexedUidCount()I

    move-result v0

    return v0
.end method

.method public stopSearch()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/library/PdfLibrary;->nativeLibrary:Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentLibrary;->cancelAllPreviewTextOperations()V

    return-void
.end method

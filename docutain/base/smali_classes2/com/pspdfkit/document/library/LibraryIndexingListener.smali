.class public interface abstract Lcom/pspdfkit/document/library/LibraryIndexingListener;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract enableOnPageIndexedEvents()Z
.end method

.method public abstract onFinishIndexingDocument(Ljava/lang/String;Z)V
.end method

.method public abstract onPageIndexed(Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract onStartIndexingDocument(Ljava/lang/String;)V
.end method

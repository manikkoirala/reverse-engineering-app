.class public Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# static fields
.field private static final EXTENSION:Ljava/lang/String; = "pscpt"


# instance fields
.field private final checkpointDir:Ljava/io/File;

.field private final checkpointFile:Ljava/io/File;

.field private final checkpointFolderPath:Ljava/lang/String;

.field private final dirty:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final document:Lcom/pspdfkit/internal/zf;

.field private final maxAllowedCheckpointAgeMs:J

.field private final saving:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private strategy:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;

.field private final timedCheckpointIntervalMs:J

.field private timedStrategyDisposable:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/zf;Ljava/io/File;Lcom/pspdfkit/internal/h5;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->document:Lcom/pspdfkit/internal/zf;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointFile:Ljava/io/File;

    .line 4
    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointDir:Ljava/io/File;

    .line 6
    sget-object p2, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;->MANUAL:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;

    iput-object p2, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->strategy:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;

    .line 7
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-wide/16 p2, 0x7530

    iput-wide p2, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->timedCheckpointIntervalMs:J

    const-string p2, "PSPDFDocumentCheckpoints"

    .line 8
    iput-object p2, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointFolderPath:Ljava/lang/String;

    const-wide/32 p2, 0x240c8400

    .line 9
    iput-wide p2, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->maxAllowedCheckpointAgeMs:J

    .line 10
    invoke-direct {p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->cleanStaleCheckpoints()I

    move-result p2

    const/4 p3, 0x0

    if-lez p2, :cond_0

    .line 12
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " checkpoints cleaned."

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    new-array v0, p3, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Checkpoint"

    invoke-static {v1, p2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    :cond_0
    new-instance p2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p2, p3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p2, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->dirty:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 15
    new-instance p2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p2, p3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p2, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->saving:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/r1;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    return-void
.end method

.method private cleanStaleCheckpoints()I
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Abstract pathname denoted by checkpoint folder must be a directory."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 4
    :cond_1
    :goto_0
    monitor-enter p0

    .line 5
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_2

    monitor-exit p0

    return v1

    .line 7
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->maxAllowedCheckpointAgeMs:J

    sub-long/2addr v2, v4

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 9
    array-length v4, v0

    const/4 v5, 0x0

    :goto_1
    if-ge v1, v4, :cond_5

    aget-object v6, v0, v1

    .line 10
    iget-object v7, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    goto :goto_2

    .line 14
    :cond_3
    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    cmp-long v9, v7, v2

    if-gez v9, :cond_4

    .line 15
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-result v6

    if-eqz v6, :cond_4

    add-int/lit8 v5, v5, 0x1

    :cond_4
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 19
    :cond_5
    monitor-exit p0

    return v5

    :catchall_0
    move-exception v0

    .line 20
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static generateCheckpointPath(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object p0

    .line 7
    :try_start_0
    invoke-static {p1}, Lcom/pspdfkit/internal/ft;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 12
    :catch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p2, Ljava/io/File;->separator:Ljava/lang/String;

    const-string v1, "%s.pscpt"

    .line 13
    invoke-static {v0, p2, v1}, Lcom/pspdfkit/internal/rg;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 156
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {p2, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-array p0, v1, [Ljava/lang/Object;

    .line 157
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    aput-object p1, p0, v3

    const-string p1, "PSPDFKit.Checkpoint"

    const-string p2, "Generated checkpoint path %s."

    invoke-static {p1, p2, p0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public static isCheckpointSupported(Lcom/pspdfkit/document/DocumentSource;)Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSource;->getPassword()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object p0

    invoke-interface {p0}, Lcom/pspdfkit/document/providers/DataProvider;->getUid()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$deleteCheckpointAsync$8(Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error when deleting checkpoint file."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Checkpoint"

    invoke-static {v1, p0, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$deleteCheckpointAsync$9(Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Checkpoint file "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const-string p0, ""

    goto :goto_0

    :cond_0
    const-string p0, "not "

    :goto_0
    const-string v1, "deleted."

    .line 2
    invoke-static {v0, p0, v1}, Lcom/pspdfkit/internal/rg;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Checkpoint"

    .line 272
    invoke-static {v1, p0, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$saveCheckpointAsync$4(Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 1
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const-string p0, "was saved."

    goto :goto_0

    :cond_0
    const-string p0, "not saved."

    :goto_0
    const/4 v1, 0x0

    aput-object p0, v0, v1

    const-string p0, "PSPDFKit.Checkpoint"

    const-string v1, "Checkpoint %s"

    invoke-static {p0, v1, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$saveCheckpointAsync$5(Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error when saving the checkpoint "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Checkpoint"

    invoke-static {v1, p0, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private onDocumentModified()V
    .locals 3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Checkpoint"

    const-string v2, "Document modified."

    .line 1
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->dirty:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->strategy:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;

    sget-object v1, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;->IMMEDIATE:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->performImmediateSaveChanges()V

    :cond_0
    return-void
.end method

.method private performImmediateSaveChanges()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->saveCheckpointAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public static setCheckpointPath(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/document/DocumentSource;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getUid()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->generateCheckpointPath(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object p0

    .line 2
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result p2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->isFile()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Checkpoint"

    const-string v2, "Found valid pre-existing checkpoint."

    .line 4
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    :cond_1
    new-instance v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v0, p1, p0, p2}, Lcom/pspdfkit/document/DocumentSource;-><init>(Lcom/pspdfkit/document/DocumentSource;Ljava/io/File;Z)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private setTimedStrategy()V
    .locals 3

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->timedCheckpointIntervalMs:J

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2}, Lio/reactivex/rxjava3/core/Observable;->interval(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;)V

    .line 3
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->flatMapSingle(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Observable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->timedStrategyDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public checkpointExists()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public deleteAllCheckpoints()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 8
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v5, v0, v3

    .line 9
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v5

    if-eqz v5, :cond_1

    add-int/lit8 v4, v4, 0x1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    if-lez v4, :cond_3

    .line 14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " checkpoints deleted."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Checkpoint"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    return-void

    .line 15
    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "The file denoted by the checkpoint folder pathname is not a directory."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public deleteCheckpoint()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->deleteCheckpointAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public deleteCheckpointAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda2;

    invoke-direct {v1}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda2;-><init>()V

    .line 5
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doOnError(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda3;

    invoke-direct {v1}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda3;-><init>()V

    .line 7
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public documentSavedSuccessfully()V
    .locals 4

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Checkpoint"

    const-string v3, "Document saved successfully."

    .line 1
    invoke-static {v2, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->dirty:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->deleteCheckpoint()Z

    return-void
.end method

.method public getStrategy()Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->strategy:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;

    return-object v0
.end method

.method public isDirty()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->dirty:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public isSaving()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->saving:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method synthetic lambda$deleteCheckpointAsync$7$com-pspdfkit-document-checkpoint-PdfDocumentCheckpointer()Ljava/lang/Boolean;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "PSPDFKit.Checkpoint"

    const-string v2, "Deleting checkpoint file at %s"

    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method synthetic lambda$saveCheckpointAsync$1$com-pspdfkit-document-checkpoint-PdfDocumentCheckpointer()Ljava/lang/Boolean;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->dirty:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method synthetic lambda$saveCheckpointAsync$2$com-pspdfkit-document-checkpoint-PdfDocumentCheckpointer(Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->saving:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method synthetic lambda$saveCheckpointAsync$3$com-pspdfkit-document-checkpoint-PdfDocumentCheckpointer(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const-string v0, "PSPDFKit.Checkpoint"

    const/4 v1, 0x0

    if-nez p1, :cond_0

    new-array p1, v1, [Ljava/lang/Object;

    const-string v1, "Latest changes already saved."

    .line 2
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object p1

    :cond_0
    const/4 p1, 0x1

    new-array v2, p1, [Ljava/lang/Object;

    .line 6
    iget-object v3, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v3, "Saving checkpoint to file %s."

    invoke-static {v0, v3, v2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    iget-object v2, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    new-array p1, p1, [Ljava/lang/Object;

    .line 8
    iget-object v2, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointFolderPath:Ljava/lang/String;

    aput-object v2, p1, v1

    const-string v1, "Creating %s folder."

    invoke-static {v0, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointDir:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->mkdir()Z

    .line 12
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->document:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->saveCheckpoint(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeDocumentSaveResult;

    .line 13
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object p1
.end method

.method synthetic lambda$saveCheckpointAsync$6$com-pspdfkit-document-checkpoint-PdfDocumentCheckpointer()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->saving:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method synthetic lambda$setTimedStrategy$0$com-pspdfkit-document-checkpoint-PdfDocumentCheckpointer(Ljava/lang/Long;)Lio/reactivex/rxjava3/core/SingleSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->saveCheckpointAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->onDocumentModified()V

    return-void
.end method

.method public onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->onDocumentModified()V

    return-void
.end method

.method public onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->onDocumentModified()V

    return-void
.end method

.method public onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->onDocumentModified()V

    return-void
.end method

.method public saveCheckpoint()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->saveCheckpointAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public saveCheckpointAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;)V

    .line 2
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doOnSubscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda6;

    invoke-direct {v1, p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;)V

    .line 3
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda7;

    invoke-direct {v1}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda7;-><init>()V

    .line 18
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda8;

    invoke-direct {v1}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda8;-><init>()V

    .line 19
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doOnError(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda9;

    invoke-direct {v1, p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;)V

    .line 21
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public setStrategy(Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;)V
    .locals 2

    const-string v0, "strategy"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->strategy:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 54
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->strategy:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;

    .line 55
    sget-object v0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;->TIMED:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->timedStrategyDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 57
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->setTimedStrategy()V

    goto :goto_0

    .line 60
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->timedStrategyDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 61
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 62
    iput-object v1, p0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->timedStrategyDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 65
    :cond_3
    :goto_0
    sget-object v0, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;->IMMEDIATE:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointingStrategy;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 66
    invoke-direct {p0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->performImmediateSaveChanges()V

    :cond_4
    return-void
.end method

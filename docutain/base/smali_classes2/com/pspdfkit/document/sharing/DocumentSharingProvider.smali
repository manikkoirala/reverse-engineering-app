.class public Lcom/pspdfkit/document/sharing/DocumentSharingProvider;
.super Lcom/pspdfkit/internal/utilities/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;
    }
.end annotation


# static fields
.field private static final PROVIDER_PATH_SHARING:Ljava/lang/String; = "sharing"

.field private static final PROVIDER_PATH_TEMP:Ljava/lang/String; = "temp"

.field private static final PROVIDER_STRATEGY:Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;-><init>(Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy-IA;)V

    sput-object v0, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->PROVIDER_STRATEGY:Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->PROVIDER_STRATEGY:Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/utilities/a;-><init>(Lcom/pspdfkit/internal/utilities/a$a;)V

    return-void
.end method

.method public static checkProviderConfiguration(Landroid/content/Context;)V
    .locals 1

    const-string v0, "sharing"

    .line 1
    invoke-static {p0, v0}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->checkProviderConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public static checkProviderConfiguration(Landroid/content/Context;Ljava/lang/String;)V
    .locals 9

    .line 2
    const-class v0, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;

    const-string v1, "context"

    const-string v2, "argumentName"

    .line 3
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 54
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v1, "featureName"

    .line 56
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p1, v1, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 108
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 109
    iget-object v3, v3, Landroid/content/pm/PackageInfo;->providers:[Landroid/content/pm/ProviderInfo;

    if-eqz v3, :cond_4

    .line 110
    array-length v4, v3
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v5, 0x0

    :goto_0
    if-ge v2, v4, :cond_3

    :try_start_1
    aget-object v6, v3, v2

    .line 111
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v6, Landroid/content/pm/ProviderInfo;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v7, :cond_2

    .line 114
    :try_start_2
    sget-object v5, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->PROVIDER_STRATEGY:Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;

    invoke-virtual {v5, p0}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;->getAuthority(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, v6, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 119
    iget-boolean v5, v6, Landroid/content/pm/ProviderInfo;->grantUriPermissions:Z

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    .line 120
    :cond_0
    new-instance p0, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string v2, "DocumentSharingProvider must allow granting Uri permissions via android:grantUriPermissions=\"true\"!"

    invoke-direct {p0, v2}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 121
    :cond_1
    new-instance v2, Lcom/pspdfkit/exceptions/PSPDFKitException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DocumentSharingProvider must have authority: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    invoke-virtual {v5, p0}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;->getAuthority(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "! Was: "

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, v6, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, p0}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    nop

    goto :goto_2

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_1
    nop

    move v1, v5

    goto :goto_2

    :cond_3
    move v2, v5

    goto :goto_3

    :catch_2
    nop

    const/4 v1, 0x0

    :goto_2
    move v2, v1

    :cond_4
    :goto_3
    if-eqz v2, :cond_5

    return-void

    .line 137
    :cond_5
    new-instance p0, Lcom/pspdfkit/exceptions/PSPDFKitException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "You need to declare DocumentSharingProvider ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 138
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ") in AndroidManifest.xml for "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " to work!"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static createTemporaryFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "fileName"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    :try_start_0
    invoke-static {p0}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->getTempFileDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 110
    invoke-static {p1, p2, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    move-object p1, v2

    :goto_0
    if-eqz p1, :cond_0

    .line 114
    invoke-static {p0, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->getUriForFile(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    :cond_0
    return-object v2
.end method

.method public static deleteFile(Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 3

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "sharedFileUri"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    :try_start_0
    sget-object v0, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->PROVIDER_STRATEGY:Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;

    invoke-static {p0, v0, p1}, Lcom/pspdfkit/internal/utilities/a;->getFile(Landroid/content/Context;Lcom/pspdfkit/internal/utilities/a$a;Landroid/net/Uri;)Ljava/io/File;

    move-result-object p0

    .line 108
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    const/4 p0, 0x0

    return p0
.end method

.method public static getDocumentProviderAuthority(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->PROVIDER_STRATEGY:Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;->getAuthority(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getSharedFileDirectory(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    const-string v0, "Couldn\'t create temporary share directory."

    const-string v1, "context"

    const-string v2, "argumentName"

    .line 2
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    :try_start_0
    sget-object v1, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->PROVIDER_STRATEGY:Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;

    invoke-virtual {v1, p0}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;->getDirectories(Landroid/content/Context;)Ljava/util/Map;

    move-result-object p0

    const-string v1, "sharing"

    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/io/File;

    if-eqz p0, :cond_0

    .line 58
    invoke-virtual {p0}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object p0

    .line 60
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    return-object p0

    .line 61
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    .line 68
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static getTempFileDirectory(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    const-string v0, "Couldn\'t create temporary share directory."

    const-string v1, "context"

    const-string v2, "argumentName"

    .line 2
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    :try_start_0
    sget-object v1, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->PROVIDER_STRATEGY:Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;

    invoke-virtual {v1, p0}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;->getDirectories(Landroid/content/Context;)Ljava/util/Map;

    move-result-object p0

    const-string v1, "temp"

    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/io/File;

    if-eqz p0, :cond_0

    .line 58
    invoke-virtual {p0}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object p0

    .line 60
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    return-object p0

    .line 61
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    .line 68
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static getUriForFile(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;
    .locals 3

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "fileName"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    :try_start_0
    sget-object v0, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->PROVIDER_STRATEGY:Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;

    invoke-static {p0, v0, p1}, Lcom/pspdfkit/internal/utilities/a;->getUriForFile(Landroid/content/Context;Lcom/pspdfkit/internal/utilities/a$a;Ljava/io/File;)Landroid/net/Uri;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 109
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to share file \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 111
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ". For security reasons, only files from shared directories (see DocumentSharingProvider#getSharedFileDirectory and #getTempFileDirectory) may be shared."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method static reset()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->PROVIDER_STRATEGY:Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;

    invoke-static {v0}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;->-$$Nest$mreset(Lcom/pspdfkit/document/sharing/DocumentSharingProvider$DocumentSharingProviderStrategy;)V

    return-void
.end method

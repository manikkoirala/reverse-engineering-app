.class public final synthetic Lcom/pspdfkit/document/sharing/DocumentSharingIntentHelper$$ExternalSyntheticLambda1;
.super Ljava/lang/Object;
.source "D8$$SyntheticClass"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Supplier;


# instance fields
.field public final synthetic f$0:Landroid/content/Context;

.field public final synthetic f$1:Landroid/net/Uri;

.field public final synthetic f$2:Lcom/pspdfkit/document/sharing/ShareAction;


# direct methods
.method public synthetic constructor <init>(Landroid/content/Context;Landroid/net/Uri;Lcom/pspdfkit/document/sharing/ShareAction;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/document/sharing/DocumentSharingIntentHelper$$ExternalSyntheticLambda1;->f$0:Landroid/content/Context;

    iput-object p2, p0, Lcom/pspdfkit/document/sharing/DocumentSharingIntentHelper$$ExternalSyntheticLambda1;->f$1:Landroid/net/Uri;

    iput-object p3, p0, Lcom/pspdfkit/document/sharing/DocumentSharingIntentHelper$$ExternalSyntheticLambda1;->f$2:Lcom/pspdfkit/document/sharing/ShareAction;

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DocumentSharingIntentHelper$$ExternalSyntheticLambda1;->f$0:Landroid/content/Context;

    iget-object v1, p0, Lcom/pspdfkit/document/sharing/DocumentSharingIntentHelper$$ExternalSyntheticLambda1;->f$1:Landroid/net/Uri;

    iget-object v2, p0, Lcom/pspdfkit/document/sharing/DocumentSharingIntentHelper$$ExternalSyntheticLambda1;->f$2:Lcom/pspdfkit/document/sharing/ShareAction;

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/document/sharing/DocumentSharingIntentHelper;->lambda$getShareIntentsAsync$0(Landroid/content/Context;Landroid/net/Uri;Lcom/pspdfkit/document/sharing/ShareAction;)Lio/reactivex/rxjava3/core/ObservableSource;

    move-result-object v0

    return-object v0
.end method

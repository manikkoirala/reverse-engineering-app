.class public final synthetic Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$$ExternalSyntheticLambda2;
.super Ljava/lang/Object;
.source "D8$$SyntheticClass"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Supplier;


# instance fields
.field public final synthetic f$0:Landroid/content/Context;

.field public final synthetic f$1:Ljava/lang/String;

.field public final synthetic f$2:Lcom/pspdfkit/document/PdfDocument;

.field public final synthetic f$3:Lcom/pspdfkit/document/processor/PdfProcessorTask;

.field public final synthetic f$4:Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$PdfProcessorProgressListener;


# direct methods
.method public synthetic constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/processor/PdfProcessorTask;Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$PdfProcessorProgressListener;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$$ExternalSyntheticLambda2;->f$0:Landroid/content/Context;

    iput-object p2, p0, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$$ExternalSyntheticLambda2;->f$1:Ljava/lang/String;

    iput-object p3, p0, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$$ExternalSyntheticLambda2;->f$2:Lcom/pspdfkit/document/PdfDocument;

    iput-object p4, p0, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$$ExternalSyntheticLambda2;->f$3:Lcom/pspdfkit/document/processor/PdfProcessorTask;

    iput-object p5, p0, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$$ExternalSyntheticLambda2;->f$4:Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$PdfProcessorProgressListener;

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$$ExternalSyntheticLambda2;->f$0:Landroid/content/Context;

    iget-object v1, p0, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$$ExternalSyntheticLambda2;->f$1:Ljava/lang/String;

    iget-object v2, p0, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$$ExternalSyntheticLambda2;->f$2:Lcom/pspdfkit/document/PdfDocument;

    iget-object v3, p0, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$$ExternalSyntheticLambda2;->f$3:Lcom/pspdfkit/document/processor/PdfProcessorTask;

    iget-object v4, p0, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$$ExternalSyntheticLambda2;->f$4:Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$PdfProcessorProgressListener;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor;->lambda$prepareDocumentForSharing$2(Landroid/content/Context;Ljava/lang/String;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/processor/PdfProcessorTask;Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor$PdfProcessorProgressListener;)Lio/reactivex/rxjava3/core/SingleSource;

    move-result-object v0

    return-object v0
.end method

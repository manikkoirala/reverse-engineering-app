.class public Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;
.super Lcom/pspdfkit/document/sharing/DocumentSharingController;
.source "SourceFile"


# static fields
.field private static final SHOW_PROGRESS_DIALOG_DELAY_MS:J = 0x64L


# instance fields
.field private final handler:Landroid/os/Handler;

.field private progressDialog:Lcom/pspdfkit/internal/eo;

.field private final shareAction:Lcom/pspdfkit/document/sharing/ShareAction;

.field private final shareTarget:Lcom/pspdfkit/document/sharing/ShareTarget;

.field private showProgressDialogRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/document/sharing/ShareAction;->SEND:Lcom/pspdfkit/document/sharing/ShareAction;

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/sharing/ShareAction;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/sharing/ShareAction;)V
    .locals 1

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingController;-><init>(Landroid/content/Context;)V

    .line 3
    new-instance p1, Landroid/os/Handler;

    .line 4
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->handler:Landroid/os/Handler;

    const-string p1, "shareAction"

    .line 26
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iput-object p2, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->shareAction:Lcom/pspdfkit/document/sharing/ShareAction;

    const/4 p1, 0x0

    .line 28
    iput-object p1, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->shareTarget:Lcom/pspdfkit/document/sharing/ShareTarget;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/sharing/ShareTarget;)V
    .locals 1

    .line 29
    invoke-direct {p0, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingController;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance p1, Landroid/os/Handler;

    .line 31
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->handler:Landroid/os/Handler;

    const-string p1, "shareTarget"

    .line 66
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    sget-object p1, Lcom/pspdfkit/document/sharing/ShareAction;->SEND:Lcom/pspdfkit/document/sharing/ShareAction;

    iput-object p1, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->shareAction:Lcom/pspdfkit/document/sharing/ShareAction;

    .line 68
    iput-object p2, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->shareTarget:Lcom/pspdfkit/document/sharing/ShareTarget;

    return-void
.end method

.method private hideProgressDialog()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->showProgressDialogRunnable:Ljava/lang/Runnable;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3
    iput-object v1, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->showProgressDialogRunnable:Ljava/lang/Runnable;

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    if-eqz v0, :cond_1

    .line 6
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 7
    iput-object v1, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    :cond_1
    return-void
.end method


# virtual methods
.method public cancelSharing()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->cancelSharing()V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->hideProgressDialog()V

    return-void
.end method

.method protected getProgressDialogTitle()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__exporting:I

    const/4 v2, 0x0

    .line 2
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShareAction()Lcom/pspdfkit/document/sharing/ShareAction;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->shareAction:Lcom/pspdfkit/document/sharing/ShareAction;

    return-object v0
.end method

.method public getShareTarget()Lcom/pspdfkit/document/sharing/ShareTarget;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->shareTarget:Lcom/pspdfkit/document/sharing/ShareTarget;

    return-object v0
.end method

.method synthetic lambda$onSharingStarted$0$com-pspdfkit-document-sharing-DefaultDocumentSharingController()V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/eo;

    invoke-virtual {p0}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/eo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    const/4 v1, 0x1

    .line 2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/eo;->a(Z)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/eo;->c(I)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    invoke-virtual {p0}, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->getProgressDialogTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/eo;->setMessage(Ljava/lang/CharSequence;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method protected notifyNoApplicationFoundForSharing()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__no_applications_found:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public onDetach()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->hideProgressDialog()V

    .line 2
    invoke-super {p0}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->onDetach()V

    return-void
.end method

.method protected onDocumentPrepared(Landroid/net/Uri;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v1, "shareUri"

    const-string v2, "argumentName"

    .line 4
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 55
    invoke-static {p1, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 56
    iget-object v1, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->shareTarget:Lcom/pspdfkit/document/sharing/ShareTarget;

    if-eqz v1, :cond_1

    .line 58
    invoke-static {v0, p1, v1}, Lcom/pspdfkit/document/sharing/DocumentSharingIntentHelper;->getShareIntent(Landroid/content/Context;Landroid/net/Uri;Lcom/pspdfkit/document/sharing/ShareTarget;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0

    .line 63
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->getShareAction()Lcom/pspdfkit/document/sharing/ShareAction;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/pspdfkit/document/sharing/DocumentSharingIntentHelper;->getShareIntent(Landroid/content/Context;Landroid/net/Uri;Lcom/pspdfkit/document/sharing/ShareAction;)Landroid/content/Intent;

    move-result-object v1

    .line 64
    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_2

    return-void

    .line 70
    :cond_2
    :try_start_0
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 72
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to share document with URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ". Activity cannot be started."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Document"

    invoke-static {v2, v0, p1, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public onSharingError()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->hideProgressDialog()V

    .line 2
    invoke-super {p0}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->onSharingError()V

    return-void
.end method

.method public onSharingFinished(Landroid/net/Uri;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->hideProgressDialog()V

    .line 2
    invoke-super {p0, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->onSharingFinished(Landroid/net/Uri;)V

    return-void
.end method

.method public onSharingProgress(Lcom/pspdfkit/document/processor/PdfProcessor$ProcessorProgress;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->showProgressDialogRunnable:Ljava/lang/Runnable;

    if-eqz v1, :cond_2

    .line 5
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->hideProgressDialog()V

    .line 6
    new-instance v1, Lcom/pspdfkit/internal/eo;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/eo;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    const/4 v0, 0x0

    .line 7
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/eo;->a(Z)V

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/eo;->c(I)V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    invoke-virtual {p0}, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->getProgressDialogTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    invoke-virtual {p1}, Lcom/pspdfkit/document/processor/PdfProcessor$ProcessorProgress;->getTotalPages()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/eo;->a(I)V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 15
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->progressDialog:Lcom/pspdfkit/internal/eo;

    invoke-virtual {p1}, Lcom/pspdfkit/document/processor/PdfProcessor$ProcessorProgress;->getPagesProcessed()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/eo;->b(I)V

    return-void
.end method

.method public onSharingStarted(Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->onSharingStarted(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->hideProgressDialog()V

    .line 4
    new-instance p1, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;)V

    iput-object p1, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->showProgressDialogRunnable:Ljava/lang/Runnable;

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;->handler:Landroid/os/Handler;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, p1, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

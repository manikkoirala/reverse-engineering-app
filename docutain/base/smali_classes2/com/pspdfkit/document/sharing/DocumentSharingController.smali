.class public abstract Lcom/pspdfkit/document/sharing/DocumentSharingController;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private context:Landroid/content/Context;

.field private shareDocumentDisposable:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->checkProviderConfiguration(Landroid/content/Context;)V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/document/sharing/DocumentSharingController;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public cancelSharing()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DocumentSharingController;->shareDocumentDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/document/sharing/DocumentSharingController;->shareDocumentDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_0
    return-void
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DocumentSharingController;->context:Landroid/content/Context;

    return-object v0
.end method

.method isSharingInProgress()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DocumentSharingController;->shareDocumentDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/document/sharing/DocumentSharingController;->context:Landroid/content/Context;

    return-void
.end method

.method public onDetach()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/document/sharing/DocumentSharingController;->context:Landroid/content/Context;

    return-void
.end method

.method protected abstract onDocumentPrepared(Landroid/net/Uri;)V
.end method

.method public onSharingError()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->cancelSharing()V

    return-void
.end method

.method public onSharingFinished(Landroid/net/Uri;)V
    .locals 2

    const-string v0, "shareUri"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object v1, p0, Lcom/pspdfkit/document/sharing/DocumentSharingController;->shareDocumentDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/document/sharing/DocumentSharingController;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 56
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->onDocumentPrepared(Landroid/net/Uri;)V

    return-void
.end method

.method public onSharingProgress(Lcom/pspdfkit/document/processor/PdfProcessor$ProcessorProgress;)V
    .locals 0

    return-void
.end method

.method public onSharingStarted(Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 2

    const-string v0, "shareDocumentDisposable"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/document/sharing/DocumentSharingController;->shareDocumentDisposable:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

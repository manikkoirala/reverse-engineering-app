.class public final enum Lcom/pspdfkit/document/PdfVersion;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/document/PdfVersion;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/document/PdfVersion;

.field public static final enum PDF_1_0:Lcom/pspdfkit/document/PdfVersion;

.field public static final enum PDF_1_1:Lcom/pspdfkit/document/PdfVersion;

.field public static final enum PDF_1_2:Lcom/pspdfkit/document/PdfVersion;

.field public static final enum PDF_1_3:Lcom/pspdfkit/document/PdfVersion;

.field public static final enum PDF_1_4:Lcom/pspdfkit/document/PdfVersion;

.field public static final enum PDF_1_5:Lcom/pspdfkit/document/PdfVersion;

.field public static final enum PDF_1_6:Lcom/pspdfkit/document/PdfVersion;

.field public static final enum PDF_1_7:Lcom/pspdfkit/document/PdfVersion;


# instance fields
.field final majorVersion:I

.field final minorVersion:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/document/PdfVersion;

    const-string v1, "PDF_1_0"

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3, v2}, Lcom/pspdfkit/document/PdfVersion;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/pspdfkit/document/PdfVersion;->PDF_1_0:Lcom/pspdfkit/document/PdfVersion;

    .line 3
    new-instance v1, Lcom/pspdfkit/document/PdfVersion;

    const-string v4, "PDF_1_1"

    invoke-direct {v1, v4, v3, v3, v3}, Lcom/pspdfkit/document/PdfVersion;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/pspdfkit/document/PdfVersion;->PDF_1_1:Lcom/pspdfkit/document/PdfVersion;

    .line 5
    new-instance v4, Lcom/pspdfkit/document/PdfVersion;

    const-string v5, "PDF_1_2"

    const/4 v6, 0x2

    invoke-direct {v4, v5, v6, v3, v6}, Lcom/pspdfkit/document/PdfVersion;-><init>(Ljava/lang/String;III)V

    sput-object v4, Lcom/pspdfkit/document/PdfVersion;->PDF_1_2:Lcom/pspdfkit/document/PdfVersion;

    .line 7
    new-instance v5, Lcom/pspdfkit/document/PdfVersion;

    const-string v7, "PDF_1_3"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8, v3, v8}, Lcom/pspdfkit/document/PdfVersion;-><init>(Ljava/lang/String;III)V

    sput-object v5, Lcom/pspdfkit/document/PdfVersion;->PDF_1_3:Lcom/pspdfkit/document/PdfVersion;

    .line 9
    new-instance v7, Lcom/pspdfkit/document/PdfVersion;

    const-string v9, "PDF_1_4"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10, v3, v10}, Lcom/pspdfkit/document/PdfVersion;-><init>(Ljava/lang/String;III)V

    sput-object v7, Lcom/pspdfkit/document/PdfVersion;->PDF_1_4:Lcom/pspdfkit/document/PdfVersion;

    .line 11
    new-instance v9, Lcom/pspdfkit/document/PdfVersion;

    const-string v11, "PDF_1_5"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12, v3, v12}, Lcom/pspdfkit/document/PdfVersion;-><init>(Ljava/lang/String;III)V

    sput-object v9, Lcom/pspdfkit/document/PdfVersion;->PDF_1_5:Lcom/pspdfkit/document/PdfVersion;

    .line 13
    new-instance v11, Lcom/pspdfkit/document/PdfVersion;

    const-string v13, "PDF_1_6"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14, v3, v14}, Lcom/pspdfkit/document/PdfVersion;-><init>(Ljava/lang/String;III)V

    sput-object v11, Lcom/pspdfkit/document/PdfVersion;->PDF_1_6:Lcom/pspdfkit/document/PdfVersion;

    .line 15
    new-instance v13, Lcom/pspdfkit/document/PdfVersion;

    const-string v15, "PDF_1_7"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14, v3, v14}, Lcom/pspdfkit/document/PdfVersion;-><init>(Ljava/lang/String;III)V

    sput-object v13, Lcom/pspdfkit/document/PdfVersion;->PDF_1_7:Lcom/pspdfkit/document/PdfVersion;

    const/16 v15, 0x8

    new-array v15, v15, [Lcom/pspdfkit/document/PdfVersion;

    aput-object v0, v15, v2

    aput-object v1, v15, v3

    aput-object v4, v15, v6

    aput-object v5, v15, v8

    aput-object v7, v15, v10

    aput-object v9, v15, v12

    const/4 v0, 0x6

    aput-object v11, v15, v0

    aput-object v13, v15, v14

    .line 16
    sput-object v15, Lcom/pspdfkit/document/PdfVersion;->$VALUES:[Lcom/pspdfkit/document/PdfVersion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    iput p3, p0, Lcom/pspdfkit/document/PdfVersion;->majorVersion:I

    .line 3
    iput p4, p0, Lcom/pspdfkit/document/PdfVersion;->minorVersion:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/document/PdfVersion;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/document/PdfVersion;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/document/PdfVersion;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/document/PdfVersion;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/document/PdfVersion;->$VALUES:[Lcom/pspdfkit/document/PdfVersion;

    invoke-virtual {v0}, [Lcom/pspdfkit/document/PdfVersion;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/document/PdfVersion;

    return-object v0
.end method


# virtual methods
.method public getMajorVersion()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/document/PdfVersion;->majorVersion:I

    return v0
.end method

.method public getMaxEncryptionKeyLength()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/document/PdfVersion;->minorVersion:I

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/16 v0, 0x80

    return v0

    :cond_0
    const/16 v0, 0x28

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public getMinorVersion()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/document/PdfVersion;->minorVersion:I

    return v0
.end method

.class Lcom/pspdfkit/document/search/TextSearch$1;
.super Lcom/pspdfkit/internal/jni/NativeDocumentSearcherQueryResultHandler;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/document/search/TextSearch;->lambda$performSearchAsync$0$com-pspdfkit-document-search-TextSearch(Lcom/pspdfkit/document/search/SearchOptions;Ljava/lang/String;Lio/reactivex/rxjava3/core/FlowableEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pspdfkit/document/search/TextSearch;

.field final synthetic val$documentSearcher:Lcom/pspdfkit/internal/jni/NativeDocumentSearcher;

.field final synthetic val$emitter:Lio/reactivex/rxjava3/core/FlowableEmitter;

.field final synthetic val$shouldCreateSnippets:Z


# direct methods
.method constructor <init>(Lcom/pspdfkit/document/search/TextSearch;Lio/reactivex/rxjava3/core/FlowableEmitter;Lcom/pspdfkit/internal/jni/NativeDocumentSearcher;Z)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/document/search/TextSearch$1;->this$0:Lcom/pspdfkit/document/search/TextSearch;

    iput-object p2, p0, Lcom/pspdfkit/document/search/TextSearch$1;->val$emitter:Lio/reactivex/rxjava3/core/FlowableEmitter;

    iput-object p3, p0, Lcom/pspdfkit/document/search/TextSearch$1;->val$documentSearcher:Lcom/pspdfkit/internal/jni/NativeDocumentSearcher;

    iput-boolean p4, p0, Lcom/pspdfkit/document/search/TextSearch$1;->val$shouldCreateSnippets:Z

    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativeDocumentSearcherQueryResultHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public pageResultHandler(Lcom/pspdfkit/internal/jni/NativeDocumentSearcherQuery;Ljava/lang/String;JLjava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/jni/NativeDocumentSearcherQuery;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/internal/jni/NativeDocumentSearcherResult;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/document/search/TextSearch$1;->val$emitter:Lio/reactivex/rxjava3/core/FlowableEmitter;

    invoke-interface {p1}, Lio/reactivex/rxjava3/core/FlowableEmitter;->isCancelled()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/document/search/TextSearch$1;->val$documentSearcher:Lcom/pspdfkit/internal/jni/NativeDocumentSearcher;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeDocumentSearcher;->cancelSearches()V

    return-void

    .line 8
    :cond_1
    invoke-virtual {p5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/jni/NativeDocumentSearcherResult;

    .line 9
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeDocumentSearcherResult;->getPageIndex()J

    move-result-wide p3

    long-to-int v1, p3

    .line 11
    iget-boolean p3, p0, Lcom/pspdfkit/document/search/TextSearch$1;->val$shouldCreateSnippets:Z

    const/4 p4, 0x0

    if-eqz p3, :cond_2

    .line 12
    new-instance p3, Lcom/pspdfkit/document/search/SearchResult$TextSnippet;

    .line 13
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeDocumentSearcherResult;->getPreviewText()Ljava/lang/String;

    move-result-object p5

    .line 14
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeDocumentSearcherResult;->getRangeInPreviewText()Lcom/pspdfkit/datastructures/Range;

    move-result-object v0

    invoke-direct {p3, p5, v0}, Lcom/pspdfkit/document/search/SearchResult$TextSnippet;-><init>(Ljava/lang/String;Lcom/pspdfkit/datastructures/Range;)V

    move-object v3, p3

    goto :goto_1

    :cond_2
    move-object v3, p4

    .line 17
    :goto_1
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeDocumentSearcherResult;->getRangeInText()Lcom/pspdfkit/datastructures/Range;

    move-result-object p3

    .line 21
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeDocumentSearcherResult;->getIsAnnotation()Z

    move-result p5

    if-eqz p5, :cond_6

    .line 28
    iget-object p5, p0, Lcom/pspdfkit/document/search/TextSearch$1;->this$0:Lcom/pspdfkit/document/search/TextSearch;

    invoke-static {p5}, Lcom/pspdfkit/document/search/TextSearch;->-$$Nest$fgetconfiguration(Lcom/pspdfkit/document/search/TextSearch;)Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p5

    .line 29
    invoke-static {p5}, Lcom/pspdfkit/internal/x5;->a(Lcom/pspdfkit/configuration/PdfConfiguration;)Ljava/util/EnumSet;

    move-result-object p5

    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    .line 31
    invoke-virtual {p5, v0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p5

    xor-int/lit8 p5, p5, 0x1

    if-nez p5, :cond_3

    goto :goto_0

    .line 35
    :cond_3
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeDocumentSearcherResult;->getAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object p2

    if-eqz p2, :cond_4

    .line 37
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAbsolutePageIndex()Ljava/lang/Integer;

    move-result-object p5

    if-eqz p5, :cond_4

    .line 38
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object p5

    if-eqz p5, :cond_4

    .line 39
    iget-object p4, p0, Lcom/pspdfkit/document/search/TextSearch$1;->this$0:Lcom/pspdfkit/document/search/TextSearch;

    invoke-static {p4}, Lcom/pspdfkit/document/search/TextSearch;->-$$Nest$fgetdocument(Lcom/pspdfkit/document/search/TextSearch;)Lcom/pspdfkit/internal/zf;

    move-result-object p4

    invoke-virtual {p4}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p4

    .line 41
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAbsolutePageIndex()Ljava/lang/Integer;

    move-result-object p5

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result p5

    .line 43
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int p2, v4

    .line 44
    check-cast p4, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p4, p5, p2}, Lcom/pspdfkit/internal/r1;->getAnnotation(II)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p4

    :cond_4
    if-eqz p4, :cond_5

    .line 51
    iget-object p2, p0, Lcom/pspdfkit/document/search/TextSearch$1;->this$0:Lcom/pspdfkit/document/search/TextSearch;

    invoke-static {p2}, Lcom/pspdfkit/document/search/TextSearch;->-$$Nest$fgetdocument(Lcom/pspdfkit/document/search/TextSearch;)Lcom/pspdfkit/internal/zf;

    move-result-object p2

    invoke-static {p2, p4, p3}, Lcom/pspdfkit/datastructures/TextBlock;->create(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/datastructures/Range;)Lcom/pspdfkit/datastructures/TextBlock;

    move-result-object p2

    goto :goto_2

    .line 55
    :cond_5
    iget-object p2, p0, Lcom/pspdfkit/document/search/TextSearch$1;->this$0:Lcom/pspdfkit/document/search/TextSearch;

    invoke-static {p2}, Lcom/pspdfkit/document/search/TextSearch;->-$$Nest$fgetdocument(Lcom/pspdfkit/document/search/TextSearch;)Lcom/pspdfkit/internal/zf;

    move-result-object p2

    invoke-static {p2, v1, p3}, Lcom/pspdfkit/datastructures/TextBlock;->create(Lcom/pspdfkit/document/PdfDocument;ILcom/pspdfkit/datastructures/Range;)Lcom/pspdfkit/datastructures/TextBlock;

    move-result-object p2

    goto :goto_2

    .line 58
    :cond_6
    iget-object p2, p0, Lcom/pspdfkit/document/search/TextSearch$1;->this$0:Lcom/pspdfkit/document/search/TextSearch;

    invoke-static {p2}, Lcom/pspdfkit/document/search/TextSearch;->-$$Nest$fgetdocument(Lcom/pspdfkit/document/search/TextSearch;)Lcom/pspdfkit/internal/zf;

    move-result-object p2

    invoke-static {p2, v1, p3}, Lcom/pspdfkit/datastructures/TextBlock;->create(Lcom/pspdfkit/document/PdfDocument;ILcom/pspdfkit/datastructures/Range;)Lcom/pspdfkit/datastructures/TextBlock;

    move-result-object p2

    :goto_2
    move-object v2, p2

    move-object v4, p4

    .line 61
    iget-object p2, p0, Lcom/pspdfkit/document/search/TextSearch$1;->val$emitter:Lio/reactivex/rxjava3/core/FlowableEmitter;

    new-instance p3, Lcom/pspdfkit/document/search/SearchResult;

    iget-object p4, p0, Lcom/pspdfkit/document/search/TextSearch$1;->this$0:Lcom/pspdfkit/document/search/TextSearch;

    invoke-static {p4}, Lcom/pspdfkit/document/search/TextSearch;->-$$Nest$fgetdocument(Lcom/pspdfkit/document/search/TextSearch;)Lcom/pspdfkit/internal/zf;

    move-result-object v5

    move-object v0, p3

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/document/search/SearchResult;-><init>(ILcom/pspdfkit/datastructures/TextBlock;Lcom/pspdfkit/document/search/SearchResult$TextSnippet;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/document/PdfDocument;)V

    invoke-interface {p2, p3}, Lio/reactivex/rxjava3/core/FlowableEmitter;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_7
    return-void
.end method

.method public searchCompleteHandler(Lcom/pspdfkit/internal/jni/NativeDocumentSearcherQuery;Ljava/lang/String;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/document/search/TextSearch$1;->val$emitter:Lio/reactivex/rxjava3/core/FlowableEmitter;

    invoke-interface {p1}, Lio/reactivex/rxjava3/core/FlowableEmitter;->onComplete()V

    return-void
.end method

.class public final Lcom/pspdfkit/document/search/TextSearch;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private final defaultSearchOptions:Lcom/pspdfkit/document/search/SearchOptions;

.field private final document:Lcom/pspdfkit/internal/zf;


# direct methods
.method static bridge synthetic -$$Nest$fgetconfiguration(Lcom/pspdfkit/document/search/TextSearch;)Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/document/search/TextSearch;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetdocument(Lcom/pspdfkit/document/search/TextSearch;)Lcom/pspdfkit/internal/zf;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/document/search/TextSearch;->document:Lcom/pspdfkit/internal/zf;

    return-object p0
.end method

.method public constructor <init>(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "document"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    .line 3
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    check-cast p1, Lcom/pspdfkit/internal/zf;

    iput-object p1, p0, Lcom/pspdfkit/document/search/TextSearch;->document:Lcom/pspdfkit/internal/zf;

    .line 5
    new-instance p1, Lcom/pspdfkit/document/search/SearchOptions$Builder;

    invoke-direct {p1}, Lcom/pspdfkit/document/search/SearchOptions$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/pspdfkit/document/search/SearchOptions$Builder;->build()Lcom/pspdfkit/document/search/SearchOptions;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/document/search/TextSearch;->defaultSearchOptions:Lcom/pspdfkit/document/search/SearchOptions;

    .line 6
    iput-object p2, p0, Lcom/pspdfkit/document/search/TextSearch;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-void
.end method


# virtual methods
.method lambda$performSearchAsync$0$com-pspdfkit-document-search-TextSearch(Lcom/pspdfkit/document/search/SearchOptions;Ljava/lang/String;Lio/reactivex/rxjava3/core/FlowableEmitter;)V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    move-object v1, p0

    move-object/from16 v0, p1

    .line 1
    iget-object v2, v0, Lcom/pspdfkit/document/search/SearchOptions;->compareOptionsFlags:Ljava/util/EnumSet;

    .line 2
    invoke-static {v2}, Lcom/pspdfkit/internal/sj;->a(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v5

    .line 4
    iget v2, v0, Lcom/pspdfkit/document/search/SearchOptions;->snippetLength:I

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 5
    :goto_0
    new-instance v14, Lcom/pspdfkit/internal/jni/NativeDocumentSearcherQuery;

    iget-boolean v7, v0, Lcom/pspdfkit/document/search/SearchOptions;->searchAnnotations:Z

    .line 10
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v6

    iget-object v8, v1, Lcom/pspdfkit/document/search/TextSearch;->configuration:Lcom/pspdfkit/configuration/PdfConfiguration;

    monitor-enter v6

    :try_start_0
    const-string v9, "configuration"

    .line 11
    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    sget-object v9, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_REPLIES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v6, v9}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 227
    invoke-virtual {v8}, Lcom/pspdfkit/configuration/PdfConfiguration;->getAnnotationReplyFeatures()Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    move-result-object v8

    sget-object v9, Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;->DISABLED:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v8, v9, :cond_1

    const/4 v8, 0x1

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    :goto_1
    monitor-exit v6

    .line 228
    iget v9, v0, Lcom/pspdfkit/document/search/SearchOptions;->maxSearchResults:I

    iget-boolean v3, v0, Lcom/pspdfkit/document/search/SearchOptions;->searchOnlyInPriorityPages:Z

    xor-int/lit8 v10, v3, 0x1

    iget-object v3, v0, Lcom/pspdfkit/document/search/SearchOptions;->priorityPages:Ljava/util/List;

    if-nez v3, :cond_2

    const/4 v3, 0x0

    :goto_2
    move-object v12, v3

    goto :goto_3

    .line 229
    :cond_2
    instance-of v4, v3, Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    check-cast v3, Ljava/util/ArrayList;

    goto :goto_2

    .line 230
    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v12, v4

    .line 231
    :goto_3
    new-instance v13, Lcom/pspdfkit/datastructures/Range;

    iget v0, v0, Lcom/pspdfkit/document/search/SearchOptions;->snippetLength:I

    const/16 v3, 0x14

    invoke-direct {v13, v3, v0}, Lcom/pspdfkit/datastructures/Range;-><init>(II)V

    const/4 v11, 0x0

    move-object v3, v14

    move-object/from16 v4, p2

    move v6, v2

    invoke-direct/range {v3 .. v13}, Lcom/pspdfkit/internal/jni/NativeDocumentSearcherQuery;-><init>(Ljava/lang/String;Ljava/util/EnumSet;ZZZIZZLjava/util/ArrayList;Lcom/pspdfkit/datastructures/Range;)V

    .line 233
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeDocumentSearcher;->create()Lcom/pspdfkit/internal/jni/NativeDocumentSearcher;

    move-result-object v0

    .line 234
    iget-object v3, v1, Lcom/pspdfkit/document/search/TextSearch;->document:Lcom/pspdfkit/internal/zf;

    .line 235
    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v3

    new-instance v4, Lcom/pspdfkit/document/search/TextSearch$1;

    move-object/from16 v5, p3

    invoke-direct {v4, p0, v5, v0, v2}, Lcom/pspdfkit/document/search/TextSearch$1;-><init>(Lcom/pspdfkit/document/search/TextSearch;Lio/reactivex/rxjava3/core/FlowableEmitter;Lcom/pspdfkit/internal/jni/NativeDocumentSearcher;Z)V

    .line 236
    invoke-virtual {v0, v3, v14, v4}, Lcom/pspdfkit/internal/jni/NativeDocumentSearcher;->searchDocument(Lcom/pspdfkit/internal/jni/NativeDocument;Lcom/pspdfkit/internal/jni/NativeDocumentSearcherQuery;Lcom/pspdfkit/internal/jni/NativeDocumentSearcherQueryResultHandler;)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method lambda$performSearchAsync$1$com-pspdfkit-document-search-TextSearch(Ljava/lang/String;Lcom/pspdfkit/document/search/SearchOptions;)Lorg/reactivestreams/Publisher;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 2
    invoke-static {}, Lio/reactivex/rxjava3/core/Flowable;->empty()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    return-object p1

    :cond_0
    const-string v0, "pspdf:info"

    .line 6
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/pspdfkit/internal/vl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7
    invoke-static {}, Lio/reactivex/rxjava3/core/Flowable;->empty()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    return-object p1

    .line 10
    :cond_1
    new-instance v0, Lcom/pspdfkit/document/search/TextSearch$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p2, p1}, Lcom/pspdfkit/document/search/TextSearch$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/document/search/TextSearch;Lcom/pspdfkit/document/search/SearchOptions;Ljava/lang/String;)V

    sget-object p1, Lio/reactivex/rxjava3/core/BackpressureStrategy;->BUFFER:Lio/reactivex/rxjava3/core/BackpressureStrategy;

    invoke-static {v0, p1}, Lio/reactivex/rxjava3/core/Flowable;->create(Lio/reactivex/rxjava3/core/FlowableOnSubscribe;Lio/reactivex/rxjava3/core/BackpressureStrategy;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    .line 112
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const/4 v1, 0x5

    .line 113
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    .line 114
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Flowable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    .line 116
    iget p2, p2, Lcom/pspdfkit/document/search/SearchOptions;->maxSearchResults:I

    const v0, 0x7fffffff

    if-ne p2, v0, :cond_2

    return-object p1

    :cond_2
    int-to-long v0, p2

    .line 119
    invoke-virtual {p1, v0, v1}, Lio/reactivex/rxjava3/core/Flowable;->take(J)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    return-object p1
.end method

.method public performSearch(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/search/TextSearch;->defaultSearchOptions:Lcom/pspdfkit/document/search/SearchOptions;

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/document/search/TextSearch;->performSearch(Ljava/lang/String;Lcom/pspdfkit/document/search/SearchOptions;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public performSearch(Ljava/lang/String;Lcom/pspdfkit/document/search/SearchOptions;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/document/search/SearchOptions;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;"
        }
    .end annotation

    .line 2
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/document/search/TextSearch;->performSearchAsync(Ljava/lang/String;Lcom/pspdfkit/document/search/SearchOptions;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Flowable;->toList()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    return-object p1
.end method

.method public performSearchAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/search/TextSearch;->defaultSearchOptions:Lcom/pspdfkit/document/search/SearchOptions;

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/document/search/TextSearch;->performSearchAsync(Ljava/lang/String;Lcom/pspdfkit/document/search/SearchOptions;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    return-object p1
.end method

.method public performSearchAsync(Ljava/lang/String;Lcom/pspdfkit/document/search/SearchOptions;)Lio/reactivex/rxjava3/core/Flowable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/document/search/SearchOptions;",
            ")",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Lcom/pspdfkit/document/search/SearchResult;",
            ">;"
        }
    .end annotation

    const-string v0, "searchString"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 54
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "searchOptions"

    .line 56
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 108
    new-instance v0, Lcom/pspdfkit/document/search/TextSearch$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/document/search/TextSearch$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/document/search/TextSearch;Ljava/lang/String;Lcom/pspdfkit/document/search/SearchOptions;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Flowable;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    return-object p1
.end method

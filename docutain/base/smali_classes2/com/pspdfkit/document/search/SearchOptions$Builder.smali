.class public final Lcom/pspdfkit/document/search/SearchOptions$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/document/search/SearchOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# static fields
.field static final DEFAULT_SNIPPET_LENGTH:I = 0x50

.field static final MAX_SEARCH_RESULTS:I = 0x1f4

.field static final MAX_SEARCH_RESULTS_LOW_MEM:I = 0x15e


# instance fields
.field private final compareOptionsFlags:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/document/search/CompareOptions;",
            ">;"
        }
    .end annotation
.end field

.field private maxSearchResults:I

.field private priorityPages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/datastructures/Range;",
            ">;"
        }
    .end annotation
.end field

.field private searchAnnotations:Z

.field private searchOnlyInPriorityPages:Z

.field private snippetLength:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x50

    .line 2
    iput v0, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->snippetLength:I

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->priorityPages:Ljava/util/List;

    const/4 v0, 0x0

    .line 7
    iput-boolean v0, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->searchOnlyInPriorityPages:Z

    const/4 v0, 0x1

    .line 8
    iput-boolean v0, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->searchAnnotations:Z

    .line 10
    sget-object v0, Lcom/pspdfkit/document/search/CompareOptions;->CASE_INSENSITIVE:Lcom/pspdfkit/document/search/CompareOptions;

    sget-object v1, Lcom/pspdfkit/document/search/CompareOptions;->DIACRITIC_INSENSITIVE:Lcom/pspdfkit/document/search/CompareOptions;

    sget-object v2, Lcom/pspdfkit/document/search/CompareOptions;->SMART_SEARCH:Lcom/pspdfkit/document/search/CompareOptions;

    .line 11
    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->compareOptionsFlags:Ljava/util/EnumSet;

    .line 16
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 17
    invoke-static {v0}, Lcom/pspdfkit/internal/e8;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x15e

    goto :goto_0

    :cond_0
    const/16 v0, 0x1f4

    .line 19
    :goto_0
    iput v0, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->maxSearchResults:I

    return-void
.end method


# virtual methods
.method public build()Lcom/pspdfkit/document/search/SearchOptions;
    .locals 9

    .line 1
    new-instance v8, Lcom/pspdfkit/document/search/SearchOptions;

    iget v1, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->maxSearchResults:I

    iget v2, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->snippetLength:I

    iget-boolean v3, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->searchAnnotations:Z

    iget-object v4, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->priorityPages:Ljava/util/List;

    iget-boolean v5, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->searchOnlyInPriorityPages:Z

    iget-object v6, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->compareOptionsFlags:Ljava/util/EnumSet;

    const/4 v7, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/pspdfkit/document/search/SearchOptions;-><init>(IIZLjava/util/List;ZLjava/util/EnumSet;Lcom/pspdfkit/document/search/SearchOptions-IA;)V

    return-object v8
.end method

.method public compareOptions(Ljava/util/EnumSet;)Lcom/pspdfkit/document/search/SearchOptions$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/document/search/CompareOptions;",
            ">;)",
            "Lcom/pspdfkit/document/search/SearchOptions$Builder;"
        }
    .end annotation

    const-string v0, "compareOptions"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->compareOptionsFlags:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->clear()V

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->compareOptionsFlags:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public varargs compareOptions([Lcom/pspdfkit/document/search/CompareOptions;)Lcom/pspdfkit/document/search/SearchOptions$Builder;
    .locals 2

    const-string v0, "compareOptions"

    const-string v1, "argumentName"

    .line 57
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 108
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->compareOptionsFlags:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->clear()V

    .line 110
    iget-object v0, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->compareOptionsFlags:Ljava/util/EnumSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public maxSearchResults(I)Lcom/pspdfkit/document/search/SearchOptions$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->maxSearchResults:I

    return-object p0
.end method

.method public priorityPages(Ljava/util/List;)Lcom/pspdfkit/document/search/SearchOptions$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/datastructures/Range;",
            ">;)",
            "Lcom/pspdfkit/document/search/SearchOptions$Builder;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/document/search/SearchOptions$Builder;->priorityPages(Ljava/util/List;Z)Lcom/pspdfkit/document/search/SearchOptions$Builder;

    return-object p0
.end method

.method public priorityPages(Ljava/util/List;Z)Lcom/pspdfkit/document/search/SearchOptions$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/datastructures/Range;",
            ">;Z)",
            "Lcom/pspdfkit/document/search/SearchOptions$Builder;"
        }
    .end annotation

    const-string v0, "priorityPages"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    iput-object p1, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->priorityPages:Ljava/util/List;

    .line 56
    iput-boolean p2, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->searchOnlyInPriorityPages:Z

    return-object p0
.end method

.method public searchAnnotations(Z)Lcom/pspdfkit/document/search/SearchOptions$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->searchAnnotations:Z

    return-object p0
.end method

.method public snippetLength(I)Lcom/pspdfkit/document/search/SearchOptions$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/document/search/SearchOptions$Builder;->snippetLength:I

    return-object p0
.end method

.class public final enum Lcom/pspdfkit/document/search/CompareOptions;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/document/search/CompareOptions;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/document/search/CompareOptions;

.field public static final enum CASE_INSENSITIVE:Lcom/pspdfkit/document/search/CompareOptions;

.field public static final enum DIACRITIC_INSENSITIVE:Lcom/pspdfkit/document/search/CompareOptions;

.field public static final enum REGULAR_EXPRESSION:Lcom/pspdfkit/document/search/CompareOptions;

.field public static final enum SMART_SEARCH:Lcom/pspdfkit/document/search/CompareOptions;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 1
    new-instance v0, Lcom/pspdfkit/document/search/CompareOptions;

    const-string v1, "CASE_INSENSITIVE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/document/search/CompareOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/document/search/CompareOptions;->CASE_INSENSITIVE:Lcom/pspdfkit/document/search/CompareOptions;

    .line 4
    new-instance v1, Lcom/pspdfkit/document/search/CompareOptions;

    const-string v3, "DIACRITIC_INSENSITIVE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/document/search/CompareOptions;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/document/search/CompareOptions;->DIACRITIC_INSENSITIVE:Lcom/pspdfkit/document/search/CompareOptions;

    .line 18
    new-instance v3, Lcom/pspdfkit/document/search/CompareOptions;

    const-string v5, "SMART_SEARCH"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/document/search/CompareOptions;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/document/search/CompareOptions;->SMART_SEARCH:Lcom/pspdfkit/document/search/CompareOptions;

    .line 27
    new-instance v5, Lcom/pspdfkit/document/search/CompareOptions;

    const-string v7, "REGULAR_EXPRESSION"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/document/search/CompareOptions;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/document/search/CompareOptions;->REGULAR_EXPRESSION:Lcom/pspdfkit/document/search/CompareOptions;

    const/4 v7, 0x4

    new-array v7, v7, [Lcom/pspdfkit/document/search/CompareOptions;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    .line 28
    sput-object v7, Lcom/pspdfkit/document/search/CompareOptions;->$VALUES:[Lcom/pspdfkit/document/search/CompareOptions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/document/search/CompareOptions;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/document/search/CompareOptions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/document/search/CompareOptions;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/document/search/CompareOptions;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/document/search/CompareOptions;->$VALUES:[Lcom/pspdfkit/document/search/CompareOptions;

    invoke-virtual {v0}, [Lcom/pspdfkit/document/search/CompareOptions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/document/search/CompareOptions;

    return-object v0
.end method

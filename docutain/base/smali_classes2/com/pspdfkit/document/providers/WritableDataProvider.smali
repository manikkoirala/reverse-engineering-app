.class public interface abstract Lcom/pspdfkit/document/providers/WritableDataProvider;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/providers/DataProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;
    }
.end annotation


# virtual methods
.method public abstract canWrite()Z
.end method

.method public abstract finishWrite()Z
.end method

.method public abstract startWrite(Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;)Z
.end method

.method public abstract supportsAppending()Z
.end method

.method public abstract write([B)Z
.end method

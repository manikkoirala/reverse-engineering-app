.class public final enum Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/document/providers/WritableDataProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WriteMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

.field public static final enum APPEND_TO_FILE:Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

.field public static final enum REWRITE_FILE:Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1
    new-instance v0, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    const-string v1, "REWRITE_FILE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;->REWRITE_FILE:Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    .line 3
    new-instance v1, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    const-string v3, "APPEND_TO_FILE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;->APPEND_TO_FILE:Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    .line 4
    sput-object v3, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;->$VALUES:[Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;->$VALUES:[Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    invoke-virtual {v0}, [Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    return-object v0
.end method

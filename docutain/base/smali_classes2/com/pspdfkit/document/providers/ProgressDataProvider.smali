.class public interface abstract Lcom/pspdfkit/document/providers/ProgressDataProvider;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final COMPLETE:Lio/reactivex/rxjava3/core/Flowable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 1
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Flowable;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/document/providers/ProgressDataProvider;->COMPLETE:Lio/reactivex/rxjava3/core/Flowable;

    return-void
.end method


# virtual methods
.method public abstract observeProgress()Lio/reactivex/rxjava3/core/Flowable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end method

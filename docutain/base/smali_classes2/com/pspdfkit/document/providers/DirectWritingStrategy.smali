.class public Lcom/pspdfkit/document/providers/DirectWritingStrategy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/providers/WritingStrategy;


# instance fields
.field private adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public finishWriting()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/providers/DirectWritingStrategy;->adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/document/providers/OutputStreamAdapter;->finishWritingToDataProvider()V

    const/4 v0, 0x0

    .line 6
    iput-object v0, p0, Lcom/pspdfkit/document/providers/DirectWritingStrategy;->adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;

    return-void

    .line 7
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "finishWriting() was called before prepare()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public prepare(Lcom/pspdfkit/document/providers/OutputStreamAdapter;)V
    .locals 2

    const-string v0, "adapter"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/document/providers/DirectWritingStrategy;->adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;

    if-nez v0, :cond_0

    .line 58
    iput-object p1, p0, Lcom/pspdfkit/document/providers/DirectWritingStrategy;->adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;

    return-void

    .line 59
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "prepare() was called twice."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public write([B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/providers/DirectWritingStrategy;->adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {v0, p1}, Lcom/pspdfkit/document/providers/OutputStreamAdapter;->writeToDataProvider([B)V

    return-void

    .line 6
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "write() was called before prepare()."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

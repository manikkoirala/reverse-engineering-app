.class Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/document/providers/ContentResolverDataProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WriteProcess"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final currentWriteMode:Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

.field private outputStream:Ljava/io/BufferedOutputStream;

.field private tempFilePath:Ljava/lang/String;

.field final synthetic this$0:Lcom/pspdfkit/document/providers/ContentResolverDataProvider;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/document/providers/ContentResolverDataProvider;Landroid/content/Context;Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->this$0:Lcom/pspdfkit/document/providers/ContentResolverDataProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->context:Landroid/content/Context;

    .line 3
    iput-object p3, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->currentWriteMode:Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    return-void
.end method


# virtual methods
.method public finish()Z
    .locals 7

    const-string v0, "Could not get output stream for URI "

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->outputStream:Ljava/io/BufferedOutputStream;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    .line 3
    :cond_0
    iget-object v3, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->currentWriteMode:Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    sget-object v4, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;->REWRITE_FILE:Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    const/4 v5, 0x0

    const-string v6, "PSPDFKit.ContentResolverDataProvider"

    if-ne v3, v4, :cond_2

    .line 5
    :try_start_0
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 6
    iput-object v5, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->outputStream:Ljava/io/BufferedOutputStream;

    const-string v1, "Tempfile written, transferring to content provider..."

    new-array v3, v2, [Ljava/lang/Object;

    .line 8
    invoke-static {v6, v1, v3}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->this$0:Lcom/pspdfkit/document/providers/ContentResolverDataProvider;

    invoke-virtual {v1}, Lcom/pspdfkit/document/providers/ContextDataProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->this$0:Lcom/pspdfkit/document/providers/ContentResolverDataProvider;

    invoke-static {v3}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->-$$Nest$fgeturi(Lcom/pspdfkit/document/providers/ContentResolverDataProvider;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "w"

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v1

    if-nez v1, :cond_1

    .line 11
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->this$0:Lcom/pspdfkit/document/providers/ContentResolverDataProvider;

    invoke-static {v0}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->-$$Nest$fgeturi(Lcom/pspdfkit/document/providers/ContentResolverDataProvider;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v6, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    .line 15
    :cond_1
    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 16
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->tempFilePath:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 17
    invoke-static {v1, v0}, Lcom/pspdfkit/internal/kb;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 18
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 19
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    const-string v0, "Done."

    new-array v1, v2, [Ljava/lang/Object;

    .line 20
    invoke-static {v6, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "Error finishing write!"

    .line 22
    invoke-static {v6, v0, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    .line 27
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 28
    iput-object v5, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->outputStream:Ljava/io/BufferedOutputStream;

    const-string v0, "Append done."

    new-array v1, v2, [Ljava/lang/Object;

    .line 29
    invoke-static {v6, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    const/4 v0, 0x1

    return v0

    :catch_1
    move-exception v0

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "Error finishing append!"

    .line 31
    invoke-static {v6, v0, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return v2
.end method

.method public start()Z
    .locals 7

    const-string v0, "Could not get output stream for URI "

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->currentWriteMode:Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    sget-object v2, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;->REWRITE_FILE:Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    const/4 v3, 0x1

    const-string v4, "PSPDFKit.ContentResolverDataProvider"

    const/4 v5, 0x0

    if-ne v1, v2, :cond_1

    .line 4
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->context:Landroid/content/Context;

    const-string v1, "pdf"

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/kb;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->tempFilePath:Ljava/lang/String;

    if-nez v0, :cond_0

    return v5

    :cond_0
    const-string v1, "Starting write to temporary file %s..."

    new-array v2, v3, [Ljava/lang/Object;

    aput-object v0, v2, v5

    .line 6
    invoke-static {v4, v1, v2}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    new-instance v0, Ljava/io/BufferedOutputStream;

    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->tempFilePath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->outputStream:Ljava/io/BufferedOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return v3

    :catch_0
    move-exception v0

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "Error creating a temp file!"

    .line 9
    invoke-static {v4, v0, v2, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return v5

    :cond_1
    new-array v1, v3, [Ljava/lang/Object;

    .line 15
    iget-object v2, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->this$0:Lcom/pspdfkit/document/providers/ContentResolverDataProvider;

    invoke-static {v2}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->-$$Nest$fgeturi(Lcom/pspdfkit/document/providers/ContentResolverDataProvider;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v1, v5

    const-string v2, "Starting append to output file %s..."

    invoke-static {v4, v2, v1}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    :try_start_1
    iget-object v1, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->this$0:Lcom/pspdfkit/document/providers/ContentResolverDataProvider;

    invoke-static {v2}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->-$$Nest$fgeturi(Lcom/pspdfkit/document/providers/ContentResolverDataProvider;)Landroid/net/Uri;

    move-result-object v2

    const-string v6, "wa"

    invoke-virtual {v1, v2, v6}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v1

    if-nez v1, :cond_2

    .line 20
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->this$0:Lcom/pspdfkit/document/providers/ContentResolverDataProvider;

    invoke-static {v0}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->-$$Nest$fgeturi(Lcom/pspdfkit/document/providers/ContentResolverDataProvider;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v4, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return v5

    .line 25
    :cond_2
    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->outputStream:Ljava/io/BufferedOutputStream;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return v3

    :catch_1
    move-exception v0

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "Could not start append to output stream!"

    .line 27
    invoke-static {v4, v0, v2, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return v5
.end method

.method public write([B)Z
    .locals 5

    const-string v0, "PSPDFKit.ContentResolverDataProvider"

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->outputStream:Ljava/io/BufferedOutputStream;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    .line 4
    :cond_0
    :try_start_0
    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write([B)V

    const-string v1, "Written %d data..."

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    .line 5
    array-length p1, p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v4, v2

    invoke-static {v0, v1, v4}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return v3

    :catch_0
    move-exception p1

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "Error writing data!"

    .line 7
    invoke-static {v0, p1, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return v2
.end method

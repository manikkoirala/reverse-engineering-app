.class public Lcom/pspdfkit/document/providers/TempFileWritingStrategy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/providers/WritingStrategy;


# static fields
.field private static final BUFFER_SIZE:I = 0x1000


# instance fields
.field private adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;

.field private fileOutputStream:Ljava/io/FileOutputStream;

.field private final tempFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context"

    .line 5
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p1

    const-string v0, "TFWS"

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->tempFile:Ljava/io/File;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "tempFile"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->tempFile:Ljava/io/File;

    return-void
.end method


# virtual methods
.method public finishWriting()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;

    if-eqz v0, :cond_4

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->fileOutputStream:Ljava/io/FileOutputStream;

    if-eqz v0, :cond_3

    .line 8
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->fileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 11
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->tempFile:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v1, 0x1000

    :try_start_0
    new-array v2, v1, [B

    .line 14
    :goto_0
    invoke-virtual {v0, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    if-ne v3, v1, :cond_0

    .line 16
    iget-object v3, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;

    invoke-virtual {v3, v2}, Lcom/pspdfkit/document/providers/OutputStreamAdapter;->writeToDataProvider([B)V

    goto :goto_0

    .line 20
    :cond_0
    iget-object v4, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/pspdfkit/document/providers/OutputStreamAdapter;->writeToDataProvider([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 23
    :cond_1
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;

    invoke-virtual {v0}, Lcom/pspdfkit/document/providers/OutputStreamAdapter;->finishWritingToDataProvider()V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->tempFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 29
    iput-object v0, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;

    return-void

    .line 30
    :cond_2
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Couldn\'t delete temporary file."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v1

    .line 31
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    invoke-virtual {v1, v0}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_1
    throw v1

    .line 32
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "finishWriting() was called before write()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "finishWriting() was called before prepare()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public prepare(Lcom/pspdfkit/document/providers/OutputStreamAdapter;)V
    .locals 2

    const-string v0, "adapter"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;

    if-nez v0, :cond_0

    .line 58
    iput-object p1, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->adapter:Lcom/pspdfkit/document/providers/OutputStreamAdapter;

    return-void

    .line 59
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "prepare() was called twice."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public write([B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->fileOutputStream:Ljava/io/FileOutputStream;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->tempFile:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->fileOutputStream:Ljava/io/FileOutputStream;

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/document/providers/TempFileWritingStrategy;->fileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/FileOutputStream;->write([B)V

    return-void
.end method

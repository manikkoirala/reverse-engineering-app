.class public Lcom/pspdfkit/document/providers/ContentResolverDataProvider;
.super Lcom/pspdfkit/document/providers/InputStreamDataProvider;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/pspdfkit/document/providers/WritableDataProvider;
.implements Lcom/pspdfkit/document/providers/UriDataProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/document/providers/ContentResolverDataProvider;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOG_TAG:Ljava/lang/String; = "PSPDFKit.ContentResolverDataProvider"


# instance fields
.field private fileSize:J

.field private supportsAppending:Ljava/lang/Boolean;

.field private title:Ljava/lang/String;

.field private final uri:Landroid/net/Uri;

.field private writeProcess:Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;


# direct methods
.method static bridge synthetic -$$Nest$fgeturi(Lcom/pspdfkit/document/providers/ContentResolverDataProvider;)Landroid/net/Uri;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$1;

    invoke-direct {v0}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$1;-><init>()V

    sput-object v0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/document/providers/InputStreamDataProvider;-><init>()V

    const-wide/16 v0, -0x1

    .line 2
    iput-wide v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->fileSize:J

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->title:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->supportsAppending:Ljava/lang/Boolean;

    const-string v0, "uri"

    .line 19
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-virtual {p0}, Lcom/pspdfkit/document/providers/ContextDataProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v2, v1}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Z[Landroid/net/Uri;)V

    .line 21
    iput-object p1, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 22
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;-><init>(Landroid/net/Uri;)V

    return-void
.end method


# virtual methods
.method public canWrite()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public finishWrite()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->writeProcess:Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->finish()Z

    move-result v0

    const/4 v2, 0x0

    .line 3
    iput-object v2, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->writeProcess:Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;

    const-wide/16 v2, -0x1

    .line 4
    iput-wide v2, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->fileSize:J

    .line 7
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/document/providers/InputStreamDataProvider;->reopenInputStream()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.ContentResolverDataProvider"

    const-string v4, "Error reopening the input stream."

    .line 10
    invoke-static {v3, v0, v4, v2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return v1
.end method

.method public getSize()J
    .locals 15

    const-string v0, "PSPDFKit.ContentResolverDataProvider"

    .line 1
    iget-wide v1, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->fileSize:J

    const-wide/16 v3, -0x1

    cmp-long v5, v1, v3

    if-nez v5, :cond_3

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/document/providers/ContextDataProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 6
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    iget-object v7, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    const-string v8, "r"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 8
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v7

    .line 9
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V

    const-string v6, "File size from PFD is %d."

    new-array v9, v2, [Ljava/lang/Object;

    .line 10
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v5

    invoke-static {v0, v6, v9}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    cmp-long v6, v7, v3

    if-eqz v6, :cond_0

    .line 11
    iput-wide v7, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->fileSize:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 20
    :cond_0
    :goto_0
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    iget-object v10, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    const-string v1, "_size"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual/range {v9 .. v14}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 22
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-nez v6, :cond_1

    .line 23
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 26
    :cond_1
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 27
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    if-nez v6, :cond_2

    goto :goto_1

    .line 28
    :cond_2
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    :goto_1
    iput-wide v3, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->fileSize:J

    new-array v1, v2, [Ljava/lang/Object;

    .line 29
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    const-string v2, "File size is %d."

    invoke-static {v0, v2, v1}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    :cond_3
    iget-wide v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->fileSize:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->title:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 7
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/document/providers/ContextDataProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 8
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    const-string v0, "_display_name"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 9
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 11
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_1

    .line 12
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 15
    :cond_1
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 16
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    move-object v3, v2

    :goto_0
    const/4 v0, 0x1

    const/16 v4, 0x2e

    if-nez v3, :cond_7

    .line 21
    iget-object v3, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    .line 22
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 23
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_3

    goto :goto_1

    .line 24
    :cond_3
    invoke-static {v3}, Lcom/pspdfkit/internal/r2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_4
    :goto_1
    move-object v3, v2

    :goto_2
    if-nez v3, :cond_5

    goto/16 :goto_7

    .line 25
    :cond_5
    invoke-static {v3}, Lcom/pspdfkit/internal/r2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 26
    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    if-ge v4, v0, :cond_6

    goto :goto_3

    .line 27
    :cond_6
    invoke-virtual {v3, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 28
    :goto_3
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_c

    goto :goto_7

    .line 29
    :cond_7
    iget-object v5, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_c

    .line 31
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    .line 32
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 33
    iget-object v3, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    .line 34
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 35
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_8

    goto :goto_4

    .line 36
    :cond_8
    invoke-static {v3}, Lcom/pspdfkit/internal/r2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_5

    :cond_9
    :goto_4
    move-object v3, v2

    :goto_5
    if-nez v3, :cond_a

    goto :goto_7

    .line 37
    :cond_a
    invoke-static {v3}, Lcom/pspdfkit/internal/r2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 38
    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    if-ge v4, v0, :cond_b

    goto :goto_6

    .line 39
    :cond_b
    invoke-virtual {v3, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 40
    :goto_6
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_c

    goto :goto_7

    :cond_c
    move-object v2, v3

    .line 41
    :goto_7
    iput-object v2, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->title:Ljava/lang/String;

    return-object v2
.end method

.method public getUid()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected openInputStream()Ljava/io/InputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/document/providers/ContextDataProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "PSPDFKit.ContentResolverDataProvider"

    const-string v3, "Reopened input stream %s."

    invoke-static {v2, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public startWrite(Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;)Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->writeProcess:Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.ContentResolverDataProvider"

    const-string v2, "Attempted to write to a ContentResolverDataProvider before finishing previous write!"

    .line 2
    invoke-static {v0, v2, p1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/document/providers/ContextDataProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x1

    new-array v3, v2, [Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->getUri()Landroid/net/Uri;

    move-result-object v4

    aput-object v4, v3, v1

    .line 6
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Ljava/util/List;Z)V

    .line 7
    new-instance v0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;

    invoke-virtual {p0}, Lcom/pspdfkit/document/providers/ContextDataProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;-><init>(Lcom/pspdfkit/document/providers/ContentResolverDataProvider;Landroid/content/Context;Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;)V

    iput-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->writeProcess:Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->start()Z

    move-result p1

    return p1
.end method

.method public supportsAppending()Z
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->supportsAppending:Ljava/lang/Boolean;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->getSize()J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v0, v3, v5

    if-eqz v0, :cond_1

    .line 11
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/document/providers/ContextDataProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    const-string v4, "wa"

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 12
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 16
    :catch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Content provider for "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, " does not support appending."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "PSPDFKit.ContentResolverDataProvider"

    invoke-static {v4, v0, v3}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    const/4 v0, 0x0

    .line 20
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->supportsAppending:Ljava/lang/Boolean;

    .line 24
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->supportsAppending:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public write([B)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->writeProcess:Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider$WriteProcess;->write([B)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;->uri:Landroid/net/Uri;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

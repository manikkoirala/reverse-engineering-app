.class public interface abstract Lcom/pspdfkit/document/providers/WritingStrategy;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract finishWriting()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract prepare(Lcom/pspdfkit/document/providers/OutputStreamAdapter;)V
.end method

.method public abstract write([B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

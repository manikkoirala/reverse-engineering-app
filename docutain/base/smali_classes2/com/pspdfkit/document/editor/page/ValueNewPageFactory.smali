.class public final Lcom/pspdfkit/document/editor/page/ValueNewPageFactory;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/editor/page/NewPageFactory;


# instance fields
.field private final newPage:Lcom/pspdfkit/document/processor/NewPage;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/document/processor/NewPage;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "newPage"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/document/editor/page/ValueNewPageFactory;->newPage:Lcom/pspdfkit/document/processor/NewPage;

    return-void
.end method


# virtual methods
.method public onCreateNewPage(Lcom/pspdfkit/document/editor/page/NewPageFactory$OnNewPageReadyListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/editor/page/ValueNewPageFactory;->newPage:Lcom/pspdfkit/document/processor/NewPage;

    invoke-interface {p1, v0}, Lcom/pspdfkit/document/editor/page/NewPageFactory$OnNewPageReadyListener;->onNewPageReady(Lcom/pspdfkit/document/processor/NewPage;)V

    return-void
.end method

.class public Lcom/pspdfkit/document/editor/PdfDocumentEditorFactory;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createForDocument(Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/document/editor/PdfDocumentEditor;
    .locals 2

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/internal/k8;

    check-cast p0, Lcom/pspdfkit/internal/zf;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/k8;-><init>(Lcom/pspdfkit/internal/zf;)V

    return-object v0
.end method

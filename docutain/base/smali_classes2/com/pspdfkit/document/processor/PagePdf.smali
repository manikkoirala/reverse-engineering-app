.class public Lcom/pspdfkit/document/processor/PagePdf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final context:Landroid/content/Context;

.field private final matrix:Landroid/graphics/Matrix;

.field private final pageIndex:I

.field private password:Ljava/lang/String;

.field private final pdfFile:Landroid/net/Uri;

.field private final position:Lcom/pspdfkit/document/processor/PagePosition;

.field private final provider:Lcom/pspdfkit/document/providers/DataProvider;

.field private zOrder:Lcom/pspdfkit/document/processor/PageZOrder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/pspdfkit/document/processor/PagePdf;-><init>(Landroid/content/Context;Landroid/net/Uri;ILandroid/graphics/Matrix;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;I)V
    .locals 1

    .line 7
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/pspdfkit/document/processor/PagePdf;-><init>(Landroid/content/Context;Landroid/net/Uri;ILandroid/graphics/Matrix;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;ILandroid/graphics/Matrix;)V
    .locals 1

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    sget-object v0, Lcom/pspdfkit/document/processor/PageZOrder;->FOREGROUND:Lcom/pspdfkit/document/processor/PageZOrder;

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    const-string v0, "context"

    .line 262
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pdfFile"

    .line 263
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "matrix"

    .line 264
    invoke-static {p4, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->context:Landroid/content/Context;

    .line 267
    iput-object p2, p0, Lcom/pspdfkit/document/processor/PagePdf;->pdfFile:Landroid/net/Uri;

    .line 268
    iput p3, p0, Lcom/pspdfkit/document/processor/PagePdf;->pageIndex:I

    const/4 p1, 0x0

    .line 269
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->position:Lcom/pspdfkit/document/processor/PagePosition;

    .line 270
    iput-object p4, p0, Lcom/pspdfkit/document/processor/PagePdf;->matrix:Landroid/graphics/Matrix;

    .line 271
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->provider:Lcom/pspdfkit/document/providers/DataProvider;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;ILcom/pspdfkit/document/processor/PagePosition;)V
    .locals 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    sget-object v0, Lcom/pspdfkit/document/processor/PageZOrder;->FOREGROUND:Lcom/pspdfkit/document/processor/PageZOrder;

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    const-string v0, "context"

    .line 117
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pdfFile"

    .line 118
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "position"

    .line 119
    invoke-static {p4, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->context:Landroid/content/Context;

    .line 122
    iput-object p2, p0, Lcom/pspdfkit/document/processor/PagePdf;->pdfFile:Landroid/net/Uri;

    .line 123
    iput p3, p0, Lcom/pspdfkit/document/processor/PagePdf;->pageIndex:I

    .line 124
    iput-object p4, p0, Lcom/pspdfkit/document/processor/PagePdf;->position:Lcom/pspdfkit/document/processor/PagePosition;

    .line 125
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->matrix:Landroid/graphics/Matrix;

    const/4 p1, 0x0

    .line 126
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->provider:Lcom/pspdfkit/document/providers/DataProvider;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Matrix;)V
    .locals 1

    const/4 v0, 0x0

    .line 3
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/pspdfkit/document/processor/PagePdf;-><init>(Landroid/content/Context;Landroid/net/Uri;ILandroid/graphics/Matrix;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Lcom/pspdfkit/document/processor/PagePosition;)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/pspdfkit/document/processor/PagePdf;-><init>(Landroid/content/Context;Landroid/net/Uri;ILcom/pspdfkit/document/processor/PagePosition;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 1

    .line 672
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/pspdfkit/document/processor/PagePdf;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/Matrix;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/Matrix;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    .line 673
    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/document/processor/PagePdf;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/Matrix;Lcom/pspdfkit/document/processor/PagePosition;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/Matrix;Lcom/pspdfkit/document/processor/PagePosition;)V
    .locals 2

    .line 675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 676
    sget-object v0, Lcom/pspdfkit/document/processor/PageZOrder;->FOREGROUND:Lcom/pspdfkit/document/processor/PageZOrder;

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    const-string v0, "context"

    .line 944
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "document"

    .line 945
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "matrix"

    .line 946
    invoke-static {p4, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 949
    check-cast p2, Lcom/pspdfkit/internal/zf;

    .line 951
    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->getDocumentSources()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/zf;->g(I)I

    move-result p2

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/document/DocumentSource;

    .line 952
    invoke-virtual {p2}, Lcom/pspdfkit/document/DocumentSource;->isFileSource()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 953
    invoke-virtual {p2}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->pdfFile:Landroid/net/Uri;

    .line 954
    iput-object v1, p0, Lcom/pspdfkit/document/processor/PagePdf;->provider:Lcom/pspdfkit/document/providers/DataProvider;

    goto :goto_0

    .line 956
    :cond_0
    invoke-virtual {p2}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->provider:Lcom/pspdfkit/document/providers/DataProvider;

    .line 957
    iput-object v1, p0, Lcom/pspdfkit/document/processor/PagePdf;->pdfFile:Landroid/net/Uri;

    .line 959
    :goto_0
    invoke-virtual {p2}, Lcom/pspdfkit/document/DocumentSource;->getPassword()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/document/processor/PagePdf;->password:Ljava/lang/String;

    .line 961
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->context:Landroid/content/Context;

    .line 962
    iput p3, p0, Lcom/pspdfkit/document/processor/PagePdf;->pageIndex:I

    .line 963
    iput-object p5, p0, Lcom/pspdfkit/document/processor/PagePdf;->position:Lcom/pspdfkit/document/processor/PagePosition;

    .line 964
    iput-object p4, p0, Lcom/pspdfkit/document/processor/PagePdf;->matrix:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;ILcom/pspdfkit/document/processor/PagePosition;)V
    .locals 6

    .line 674
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/document/processor/PagePdf;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/Matrix;Lcom/pspdfkit/document/processor/PagePosition;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/providers/DataProvider;)V
    .locals 2

    .line 4
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/pspdfkit/document/processor/PagePdf;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/providers/DataProvider;ILandroid/graphics/Matrix;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/providers/DataProvider;I)V
    .locals 1

    .line 272
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/pspdfkit/document/processor/PagePdf;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/providers/DataProvider;ILandroid/graphics/Matrix;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/providers/DataProvider;ILandroid/graphics/Matrix;)V
    .locals 1

    .line 459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 460
    sget-object v0, Lcom/pspdfkit/document/processor/PageZOrder;->FOREGROUND:Lcom/pspdfkit/document/processor/PageZOrder;

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    const-string v0, "context"

    .line 662
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pdfDataProvider"

    .line 663
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "matrix"

    .line 664
    invoke-static {p4, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 666
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->context:Landroid/content/Context;

    .line 667
    iput-object p2, p0, Lcom/pspdfkit/document/processor/PagePdf;->provider:Lcom/pspdfkit/document/providers/DataProvider;

    .line 668
    iput p3, p0, Lcom/pspdfkit/document/processor/PagePdf;->pageIndex:I

    const/4 p1, 0x0

    .line 669
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->position:Lcom/pspdfkit/document/processor/PagePosition;

    .line 670
    iput-object p4, p0, Lcom/pspdfkit/document/processor/PagePdf;->matrix:Landroid/graphics/Matrix;

    .line 671
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->pdfFile:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/providers/DataProvider;ILcom/pspdfkit/document/processor/PagePosition;)V
    .locals 1

    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274
    sget-object v0, Lcom/pspdfkit/document/processor/PageZOrder;->FOREGROUND:Lcom/pspdfkit/document/processor/PageZOrder;

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    const-string v0, "context"

    .line 449
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pdfDataProvider"

    .line 450
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "position"

    .line 451
    invoke-static {p4, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 453
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->context:Landroid/content/Context;

    .line 454
    iput-object p2, p0, Lcom/pspdfkit/document/processor/PagePdf;->provider:Lcom/pspdfkit/document/providers/DataProvider;

    .line 455
    iput p3, p0, Lcom/pspdfkit/document/processor/PagePdf;->pageIndex:I

    .line 456
    iput-object p4, p0, Lcom/pspdfkit/document/processor/PagePdf;->position:Lcom/pspdfkit/document/processor/PagePosition;

    .line 457
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->matrix:Landroid/graphics/Matrix;

    const/4 p1, 0x0

    .line 458
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->pdfFile:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/providers/DataProvider;Landroid/graphics/Matrix;)V
    .locals 1

    const/4 v0, 0x0

    .line 6
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/pspdfkit/document/processor/PagePdf;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/providers/DataProvider;ILandroid/graphics/Matrix;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/document/providers/DataProvider;Lcom/pspdfkit/document/processor/PagePosition;)V
    .locals 1

    const/4 v0, 0x0

    .line 5
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/pspdfkit/document/processor/PagePdf;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/providers/DataProvider;ILcom/pspdfkit/document/processor/PagePosition;)V

    return-void
.end method


# virtual methods
.method getItemConfiguration()Lcom/pspdfkit/internal/jni/NativeItemConfiguration;
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/document/processor/PagePdf;->getNativeDataDescriptor()Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    move-result-object v2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->position:Lcom/pspdfkit/document/processor/PagePosition;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeItemRelativePosition;->values()[Lcom/pspdfkit/internal/jni/NativeItemRelativePosition;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/document/processor/PagePdf;->position:Lcom/pspdfkit/document/processor/PagePosition;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    :goto_0
    move-object v4, v0

    .line 4
    new-instance v7, Lcom/pspdfkit/internal/jni/NativeItemConfiguration;

    iget v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->pageIndex:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0}, Lcom/pspdfkit/document/processor/PagePdf;->getNativeZPosition()Lcom/pspdfkit/internal/jni/NativeItemZPosition;

    move-result-object v5

    iget-object v6, p0, Lcom/pspdfkit/document/processor/PagePdf;->matrix:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/jni/NativeItemConfiguration;-><init>(Lcom/pspdfkit/internal/jni/NativeImage;Lcom/pspdfkit/internal/jni/NativeDataDescriptor;Ljava/lang/Integer;Lcom/pspdfkit/internal/jni/NativeItemRelativePosition;Lcom/pspdfkit/internal/jni/NativeItemZPosition;Landroid/graphics/Matrix;)V

    return-object v7
.end method

.method public getMatrix()Landroid/graphics/Matrix;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->matrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method getNativeDataDescriptor()Lcom/pspdfkit/internal/jni/NativeDataDescriptor;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->pdfFile:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->provider:Lcom/pspdfkit/document/providers/DataProvider;

    iget-object v1, p0, Lcom/pspdfkit/document/processor/PagePdf;->password:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/s7;->createNativeDataDescriptor(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    move-result-object v0

    goto :goto_0

    .line 5
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/document/processor/PagePdf;->context:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 7
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    iget-object v5, p0, Lcom/pspdfkit/document/processor/PagePdf;->password:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/pspdfkit/internal/jni/NativeDataDescriptor;-><init>(Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeDataProvider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 9
    :cond_1
    new-instance v0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;

    iget-object v1, p0, Lcom/pspdfkit/document/processor/PagePdf;->pdfFile:Landroid/net/Uri;

    invoke-direct {v0, v1}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;-><init>(Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/pspdfkit/document/processor/PagePdf;->password:Ljava/lang/String;

    .line 10
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/s7;->createNativeDataDescriptor(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method getNativeZPosition()Lcom/pspdfkit/internal/jni/NativeItemZPosition;
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeItemZPosition;->values()[Lcom/pspdfkit/internal/jni/NativeItemZPosition;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/document/processor/PagePdf;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getPageIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->pageIndex:I

    return v0
.end method

.method public getPosition()Lcom/pspdfkit/document/processor/PagePosition;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->position:Lcom/pspdfkit/document/processor/PagePosition;

    return-object v0
.end method

.method public getZOrder()Lcom/pspdfkit/document/processor/PageZOrder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PagePdf;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    return-object v0
.end method

.method public setDocumentPassword(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->password:Ljava/lang/String;

    return-void
.end method

.method public setZOrder(Lcom/pspdfkit/document/processor/PageZOrder;)V
    .locals 2

    const-string v0, "zOrder"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PagePdf;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    return-void
.end method

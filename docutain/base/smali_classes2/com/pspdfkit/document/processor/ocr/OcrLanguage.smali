.class public final enum Lcom/pspdfkit/document/processor/ocr/OcrLanguage;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/document/processor/ocr/OcrLanguage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum CROATIAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum CZECH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum DANISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum DUTCH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum ENGLISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum FINNISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum FRENCH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum GERMAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum INDONESIAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum ITALIAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum MALAY:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum NORWEGIAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum POLISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum PORTUGUESE:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum SERBIAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum SLOVAK:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum SLOVENIAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum SPANISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum SWEDISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum TURKISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

.field public static final enum WELSH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;


# instance fields
.field private final trainedDataFilename:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 24

    .line 1
    new-instance v0, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v1, "CROATIAN"

    const/4 v2, 0x0

    const-string v3, "hrv"

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->CROATIAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 17
    new-instance v1, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v3, "CZECH"

    const/4 v4, 0x1

    const-string v5, "ces"

    invoke-direct {v1, v3, v4, v5}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->CZECH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 33
    new-instance v3, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v5, "DANISH"

    const/4 v6, 0x2

    const-string v7, "dan"

    invoke-direct {v3, v5, v6, v7}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->DANISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 49
    new-instance v5, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v7, "DUTCH"

    const/4 v8, 0x3

    const-string v9, "nld"

    invoke-direct {v5, v7, v8, v9}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->DUTCH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 65
    new-instance v7, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v9, "ENGLISH"

    const/4 v10, 0x4

    const-string v11, "eng"

    invoke-direct {v7, v9, v10, v11}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->ENGLISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 81
    new-instance v9, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v11, "FINNISH"

    const/4 v12, 0x5

    const-string v13, "fin"

    invoke-direct {v9, v11, v12, v13}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->FINNISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 97
    new-instance v11, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v13, "FRENCH"

    const/4 v14, 0x6

    const-string v15, "fra"

    invoke-direct {v11, v13, v14, v15}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v11, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->FRENCH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 113
    new-instance v13, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "GERMAN"

    const/4 v14, 0x7

    const-string v12, "deu"

    invoke-direct {v13, v15, v14, v12}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v13, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->GERMAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 129
    new-instance v12, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "INDONESIAN"

    const/16 v14, 0x8

    const-string v10, "ind"

    invoke-direct {v12, v15, v14, v10}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v12, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->INDONESIAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 145
    new-instance v10, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "ITALIAN"

    const/16 v14, 0x9

    const-string v8, "ita"

    invoke-direct {v10, v15, v14, v8}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v10, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->ITALIAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 161
    new-instance v8, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "MALAY"

    const/16 v14, 0xa

    const-string v6, "msa"

    invoke-direct {v8, v15, v14, v6}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v8, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->MALAY:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 177
    new-instance v6, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "NORWEGIAN"

    const/16 v14, 0xb

    const-string v4, "nor"

    invoke-direct {v6, v15, v14, v4}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v6, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->NORWEGIAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 193
    new-instance v4, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "POLISH"

    const/16 v14, 0xc

    const-string v2, "pol"

    invoke-direct {v4, v15, v14, v2}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->POLISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 209
    new-instance v2, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "PORTUGUESE"

    const/16 v14, 0xd

    move-object/from16 v16, v4

    const-string v4, "por"

    invoke-direct {v2, v15, v14, v4}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->PORTUGUESE:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 225
    new-instance v4, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "SERBIAN"

    const/16 v14, 0xe

    move-object/from16 v17, v2

    const-string v2, "srp"

    invoke-direct {v4, v15, v14, v2}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->SERBIAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 241
    new-instance v2, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "SLOVAK"

    const/16 v14, 0xf

    move-object/from16 v18, v4

    const-string v4, "slk"

    invoke-direct {v2, v15, v14, v4}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->SLOVAK:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 257
    new-instance v4, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "SLOVENIAN"

    const/16 v14, 0x10

    move-object/from16 v19, v2

    const-string v2, "slv"

    invoke-direct {v4, v15, v14, v2}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->SLOVENIAN:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 273
    new-instance v2, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "SPANISH"

    const/16 v14, 0x11

    move-object/from16 v20, v4

    const-string v4, "spa"

    invoke-direct {v2, v15, v14, v4}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->SPANISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 289
    new-instance v4, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "SWEDISH"

    const/16 v14, 0x12

    move-object/from16 v21, v2

    const-string v2, "swe"

    invoke-direct {v4, v15, v14, v2}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->SWEDISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 305
    new-instance v2, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "TURKISH"

    const/16 v14, 0x13

    move-object/from16 v22, v4

    const-string v4, "tur"

    invoke-direct {v2, v15, v14, v4}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->TURKISH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    .line 321
    new-instance v4, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const-string v15, "WELSH"

    const/16 v14, 0x14

    move-object/from16 v23, v2

    const-string v2, "cym"

    invoke-direct {v4, v15, v14, v2}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->WELSH:Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const/16 v2, 0x15

    new-array v2, v2, [Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    const/4 v15, 0x0

    aput-object v0, v2, v15

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object v5, v2, v0

    const/4 v0, 0x4

    aput-object v7, v2, v0

    const/4 v0, 0x5

    aput-object v9, v2, v0

    const/4 v0, 0x6

    aput-object v11, v2, v0

    const/4 v0, 0x7

    aput-object v13, v2, v0

    const/16 v0, 0x8

    aput-object v12, v2, v0

    const/16 v0, 0x9

    aput-object v10, v2, v0

    const/16 v0, 0xa

    aput-object v8, v2, v0

    const/16 v0, 0xb

    aput-object v6, v2, v0

    const/16 v0, 0xc

    aput-object v16, v2, v0

    const/16 v0, 0xd

    aput-object v17, v2, v0

    const/16 v0, 0xe

    aput-object v18, v2, v0

    const/16 v0, 0xf

    aput-object v19, v2, v0

    const/16 v0, 0x10

    aput-object v20, v2, v0

    const/16 v0, 0x11

    aput-object v21, v2, v0

    const/16 v0, 0x12

    aput-object v22, v2, v0

    const/16 v0, 0x13

    aput-object v23, v2, v0

    aput-object v4, v2, v14

    .line 322
    sput-object v2, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->$VALUES:[Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    iput-object p3, p0, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->trainedDataFilename:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/document/processor/ocr/OcrLanguage;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/document/processor/ocr/OcrLanguage;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->$VALUES:[Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    invoke-virtual {v0}, [Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/document/processor/ocr/OcrLanguage;

    return-object v0
.end method


# virtual methods
.method public getTrainedDataFilename()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->trainedDataFilename:Ljava/lang/String;

    return-object v0
.end method

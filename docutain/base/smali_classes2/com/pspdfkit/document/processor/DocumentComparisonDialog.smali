.class public Lcom/pspdfkit/document/processor/DocumentComparisonDialog;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static restore(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/document/processor/ComparisonDialogListener;)V
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/internal/g8;->p:I

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/g8$a;->a(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/document/processor/ComparisonDialogListener;)V

    return-void
.end method

.method public static show(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/processor/ComparisonDocument;Lcom/pspdfkit/document/processor/ComparisonDocument;Ljava/io/File;Lcom/pspdfkit/document/processor/ComparisonDialogListener;)V
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/internal/g8;->p:I

    invoke-static/range {p0 .. p5}, Lcom/pspdfkit/internal/g8$a;->a(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/processor/ComparisonDocument;Lcom/pspdfkit/document/processor/ComparisonDocument;Ljava/io/File;Lcom/pspdfkit/document/processor/ComparisonDialogListener;)V

    return-void
.end method

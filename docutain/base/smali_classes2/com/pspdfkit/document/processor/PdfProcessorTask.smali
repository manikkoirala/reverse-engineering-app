.class public final Lcom/pspdfkit/document/processor/PdfProcessorTask;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/document/processor/PdfProcessorTask$NativeProcessorConfigurationMapper;,
        Lcom/pspdfkit/document/processor/PdfProcessorTask$NativeProcessorConfigurationFactory;,
        Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;
    }
.end annotation


# instance fields
.field private final configurationMappers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/processor/PdfProcessorTask$NativeProcessorConfigurationMapper;",
            ">;"
        }
    .end annotation
.end field

.field private final initialConfigurationFactory:Lcom/pspdfkit/document/processor/PdfProcessorTask$NativeProcessorConfigurationFactory;

.field final sourceDocument:Lcom/pspdfkit/internal/zf;


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    .line 77
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 82
    iput-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->sourceDocument:Lcom/pspdfkit/internal/zf;

    .line 83
    new-instance v0, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda20;

    invoke-direct {v0}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda20;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->initialConfigurationFactory:Lcom/pspdfkit/document/processor/PdfProcessorTask$NativeProcessorConfigurationFactory;

    return-void

    .line 84
    :cond_0
    new-instance v0, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;

    const-string v1, "PSPDFKit must be initialized with the initialize() call before use of processor."

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private constructor <init>(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    .line 17
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "sourceDocument"

    .line 22
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    check-cast p1, Lcom/pspdfkit/internal/zf;

    iput-object p1, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->sourceDocument:Lcom/pspdfkit/internal/zf;

    .line 25
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object p1

    .line 27
    new-instance v0, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda12;

    invoke-direct {v0, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda12;-><init>(Lcom/pspdfkit/internal/jni/NativeDocument;)V

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->initialConfigurationFactory:Lcom/pspdfkit/document/processor/PdfProcessorTask$NativeProcessorConfigurationFactory;

    return-void

    .line 28
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;

    const-string v0, "PSPDFKit must be initialized with the initialize() call before use of processor."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private constructor <init>(Lcom/pspdfkit/document/processor/NewPage;)V
    .locals 1

    .line 29
    invoke-direct {p0}, Lcom/pspdfkit/document/processor/PdfProcessorTask;-><init>()V

    const-string v0, "newPage"

    .line 30
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 31
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->addNewPage(Lcom/pspdfkit/document/processor/NewPage;I)Lcom/pspdfkit/document/processor/PdfProcessorTask;

    return-void
.end method

.method private checkCurrentPageIndexOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;I)V
    .locals 2

    if-ltz p2, :cond_0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->getPageCount()I

    move-result p1

    if-ge p2, p1, :cond_0

    return-void

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Page index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " isn\'t within existing page ranges!"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private checkCurrentPageIndexesOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ltz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->getPageCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v1, "Page index "

    invoke-direct {p2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " isn\'t within existing page ranges!"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    return-void
.end method

.method private checkDestinationIndexOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;I)V
    .locals 2

    if-ltz p2, :cond_0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->getPageCount()I

    move-result p1

    if-gt p2, p1, :cond_0

    return-void

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Destination index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " isn\'t within range!"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static empty()Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/document/processor/PdfProcessorTask;

    invoke-direct {v0}, Lcom/pspdfkit/document/processor/PdfProcessorTask;-><init>()V

    return-object v0
.end method

.method public static fromDocument(Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    const-string v0, "sourceDocument"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/document/processor/PdfProcessorTask;

    invoke-direct {v0, p0}, Lcom/pspdfkit/document/processor/PdfProcessorTask;-><init>(Lcom/pspdfkit/document/PdfDocument;)V

    return-object v0
.end method

.method static synthetic lambda$applyRedactions$24(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 2

    const/4 v0, 0x0

    .line 1
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->getPageCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->applyRedactAnnotations(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method static synthetic lambda$changeAllAnnotations$9(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->values()[Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 4
    invoke-static {p0}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)Lcom/pspdfkit/internal/jni/NativeProcessOperation;

    move-result-object p0

    .line 5
    invoke-virtual {p1, v0, p0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->processAnnotationsWithOperation(Ljava/util/ArrayList;Lcom/pspdfkit/internal/jni/NativeProcessOperation;)V

    return-object p1
.end method

.method static synthetic lambda$changeAnnotations$8(Ljava/util/List;Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 7
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 12
    :cond_2
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)Lcom/pspdfkit/internal/jni/NativeProcessOperation;

    move-result-object p0

    .line 13
    invoke-virtual {p2, v0, p0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->processAnnotations(Ljava/util/ArrayList;Lcom/pspdfkit/internal/jni/NativeProcessOperation;)V

    return-object p2
.end method

.method static lambda$changeAnnotationsOfType$6(Lcom/pspdfkit/annotations/AnnotationType;Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 3

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/internal/fv;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_1

    .line 2
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 3
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/AnnotationType;

    .line 4
    const-class v2, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    invoke-static {v2, v1}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 5
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object p0, v0

    .line 6
    :goto_1
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)Lcom/pspdfkit/internal/jni/NativeProcessOperation;

    move-result-object p1

    .line 7
    invoke-virtual {p2, p0, p1}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->processAnnotationsWithOperation(Ljava/util/ArrayList;Lcom/pspdfkit/internal/jni/NativeProcessOperation;)V

    return-object p2
.end method

.method static lambda$changeFormsOfType$7(Lcom/pspdfkit/forms/FormType;Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    const-class v1, Lcom/pspdfkit/internal/jni/NativeFormType;

    invoke-static {v1, p0}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeFormType;

    .line 3
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)Lcom/pspdfkit/internal/jni/NativeProcessOperation;

    move-result-object p0

    .line 6
    invoke-virtual {p2, v0, p0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->processFormsWithOperation(Ljava/util/ArrayList;Lcom/pspdfkit/internal/jni/NativeProcessOperation;)V

    return-object p2
.end method

.method static synthetic lambda$clearPageLabels$20(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->clearPageLabels()V

    return-object p0
.end method

.method static synthetic lambda$new$0(Lcom/pspdfkit/internal/jni/NativeDocument;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->create(Lcom/pspdfkit/internal/jni/NativeDocument;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$new$1()Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->create(Lcom/pspdfkit/internal/jni/NativeDocument;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$performOcrOnPages$25(Lcom/pspdfkit/document/processor/ocr/OcrLanguage;Ljava/util/Set;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 4

    .line 1
    :try_start_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/pspdfkit/document/processor/ocr/OcrLanguage;->getTrainedDataFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".traineddata"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string v2, "ocr/trained-data"

    .line 9
    invoke-static {v2}, Lcom/pspdfkit/internal/kb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "pspdfkit/ocr/trained-data"

    .line 10
    invoke-static {v0, v2, v1, v3}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashSet;Ljava/lang/String;)Ljava/io/File;

    .line 16
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 17
    invoke-static {p0}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/document/processor/ocr/OcrLanguage;)Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    move-result-object p0

    .line 18
    invoke-virtual {p2, v0, p0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->performOcr(Ljava/util/HashSet;Lcom/pspdfkit/internal/jni/NativeOcrLanguage;)V

    return-object p2

    .line 19
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "PSPDFKit must be initialized!"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    .line 33
    new-instance p1, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string p2, "Error while trying to perform OCR on the page.Did you forget to import core OCR library or OCR language pack in your dependencies?"

    invoke-direct {p1, p2, p0}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method

.method static lambda$setFormFieldNameMappings$22(Ljava/util/Map;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 1
    :cond_0
    instance-of v0, p0, Ljava/util/HashMap;

    if-eqz v0, :cond_1

    check-cast p0, Ljava/util/HashMap;

    goto :goto_0

    .line 2
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    move-object p0, v0

    .line 3
    :goto_0
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->changeFormFieldNames(Ljava/util/HashMap;)V

    return-object p1
.end method

.method static lambda$setFormMappingNameMappings$23(Ljava/util/Map;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 1
    :cond_0
    instance-of v0, p0, Ljava/util/HashMap;

    if-eqz v0, :cond_1

    check-cast p0, Ljava/util/HashMap;

    goto :goto_0

    .line 2
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    move-object p0, v0

    .line 3
    :goto_0
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->changeFormMappingNames(Ljava/util/HashMap;)V

    return-object p1
.end method

.method static synthetic lambda$stripEmptyPages$5(ZLcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 0

    .line 1
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->setShouldStripGeneratedBlankPages(Z)V

    return-object p1
.end method

.method static synthetic lambda$withMetadata$14(Ljava/util/HashMap;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->clearMetadata()V

    .line 2
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->updateMetadata(Ljava/util/HashMap;)V

    return-object p1
.end method

.method public static newPage(Lcom/pspdfkit/document/processor/NewPage;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    const-string v0, "newPage"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/document/processor/PdfProcessorTask;

    invoke-direct {v0, p0}, Lcom/pspdfkit/document/processor/PdfProcessorTask;-><init>(Lcom/pspdfkit/document/processor/NewPage;)V

    return-object v0
.end method


# virtual methods
.method public addCanvasDrawingToPage(Lcom/pspdfkit/document/processor/PageCanvas;I)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    const-string v0, "pageCanvas"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda18;

    invoke-direct {v1, p0, p2, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda18;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;ILcom/pspdfkit/document/processor/PageCanvas;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addImageToPage(Lcom/pspdfkit/document/processor/PageImage;I)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda19;

    invoke-direct {v1, p0, p2, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda19;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;ILcom/pspdfkit/document/processor/PageImage;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Image must not be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public addNewPage(Lcom/pspdfkit/document/processor/NewPage;I)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    .line 7
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, p2, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;ILcom/pspdfkit/document/processor/NewPage;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 11
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "PSPDFKit must be initialized!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 12
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "New page configuration must not be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 13
    :cond_2
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Adding new pages requires document editor feature in your license!"

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public applyRedactions()Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->REDACTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda16;

    invoke-direct {v1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda16;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 6
    :cond_0
    new-instance v0, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v1, "Redacting requires Redaction License."

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public changeAllAnnotations(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda6;

    invoke-direct {v1, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Processing mode must not be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public changeAnnotations(Ljava/util/List;Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;",
            ")",
            "Lcom/pspdfkit/document/processor/PdfProcessorTask;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->sourceDocument:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    return-object p0

    :cond_0
    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda5;

    invoke-direct {v1, p1, p2}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda5;-><init>(Ljava/util/List;Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 9
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Processing mode must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 10
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "List of annotations must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public changeAnnotationsOfType(Lcom/pspdfkit/annotations/AnnotationType;Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda1;

    invoke-direct {v1, p1, p2}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/annotations/AnnotationType;Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Processing mode must not be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 3
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Annotation type must not be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public changeFormsOfType(Lcom/pspdfkit/forms/FormType;Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda13;

    invoke-direct {v1, p1, p2}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda13;-><init>(Lcom/pspdfkit/forms/FormType;Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Processing mode must not be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 3
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Form type must not be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public changeStrokeColorOnPage(II)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->COMPARISON:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda9;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 7
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Changing page stroke color requires document comparison feature in your license."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public clearPageLabels()Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda21;

    invoke-direct {v1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda21;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method getProcessorConfiguration()Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->initialConfigurationFactory:Lcom/pspdfkit/document/processor/PdfProcessorTask$NativeProcessorConfigurationFactory;

    invoke-interface {v0}, Lcom/pspdfkit/document/processor/PdfProcessorTask$NativeProcessorConfigurationFactory;->create()Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;

    move-result-object v0

    const-string v1, "Mapped configuration may not be null!"

    const-string v2, "message"

    .line 3
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    .line 4
    iget-object v3, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/document/processor/PdfProcessorTask$NativeProcessorConfigurationMapper;

    .line 5
    invoke-interface {v4, v0}, Lcom/pspdfkit/document/processor/PdfProcessorTask$NativeProcessorConfigurationMapper;->apply(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;

    move-result-object v0

    .line 7
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    goto :goto_0

    .line 33
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0

    .line 34
    :cond_2
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public keepPages(Ljava/util/Set;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/pspdfkit/document/processor/PdfProcessorTask;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda14;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda14;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;Ljava/util/Set;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Set of pages to keep must not be null!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method synthetic lambda$addCanvasDrawingToPage$16$com-pspdfkit-document-processor-PdfProcessorTask(ILcom/pspdfkit/document/processor/PageCanvas;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 0

    .line 1
    invoke-direct {p0, p3, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkDestinationIndexOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;I)V

    .line 2
    invoke-virtual {p2}, Lcom/pspdfkit/document/processor/PageCanvas;->getItemConfiguration()Lcom/pspdfkit/internal/jni/NativeItemConfiguration;

    move-result-object p2

    invoke-virtual {p3, p1, p2}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->mergeContentFromItem(ILcom/pspdfkit/internal/jni/NativeItemConfiguration;)V

    return-object p3
.end method

.method synthetic lambda$addImageToPage$15$com-pspdfkit-document-processor-PdfProcessorTask(ILcom/pspdfkit/document/processor/PageImage;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 0

    .line 1
    invoke-direct {p0, p3, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkDestinationIndexOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;I)V

    .line 3
    :try_start_0
    invoke-virtual {p2}, Lcom/pspdfkit/document/processor/PageImage;->getItemConfiguration()Lcom/pspdfkit/internal/jni/NativeItemConfiguration;

    move-result-object p2

    invoke-virtual {p3, p1, p2}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->mergeContentFromItem(ILcom/pspdfkit/internal/jni/NativeItemConfiguration;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p3

    :catch_0
    move-exception p1

    .line 5
    new-instance p2, Lcom/pspdfkit/document/processor/PdfProcessorException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/pspdfkit/document/processor/PdfProcessorException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method synthetic lambda$addNewPage$10$com-pspdfkit-document-processor-PdfProcessorTask(ILcom/pspdfkit/document/processor/NewPage;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 0

    .line 1
    invoke-direct {p0, p3, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkDestinationIndexOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;I)V

    .line 2
    invoke-virtual {p2}, Lcom/pspdfkit/document/processor/NewPage;->getNativeNewPageConfiguration()Lcom/pspdfkit/internal/jni/NativeNewPageConfiguration;

    move-result-object p2

    .line 3
    invoke-virtual {p3, p1, p2}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->addNewPage(ILcom/pspdfkit/internal/jni/NativeNewPageConfiguration;)V

    return-object p3
.end method

.method synthetic lambda$changeStrokeColorOnPage$19$com-pspdfkit-document-processor-PdfProcessorTask(IILcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 1

    .line 1
    invoke-direct {p0, p3, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkDestinationIndexOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;I)V

    .line 2
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    sget-object v0, Lcom/pspdfkit/internal/jni/NativePageColorOptions;->STROKING:Lcom/pspdfkit/internal/jni/NativePageColorOptions;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p3, p1, p2, v0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->adjustPageColors(ILjava/lang/Integer;Ljava/util/EnumSet;)V

    return-object p3
.end method

.method lambda$keepPages$2$com-pspdfkit-document-processor-PdfProcessorTask(Ljava/util/Set;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 3

    .line 1
    invoke-direct {p0, p2, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkCurrentPageIndexesOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;Ljava/util/Set;)V

    .line 2
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x0

    .line 3
    :goto_0
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->getPageCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 8
    :cond_1
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->removePages(Ljava/util/HashSet;)V

    return-object p2
.end method

.method synthetic lambda$mergePage$17$com-pspdfkit-document-processor-PdfProcessorTask(ILcom/pspdfkit/document/processor/PagePdf;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 7

    .line 1
    invoke-direct {p0, p3, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkDestinationIndexOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;I)V

    .line 4
    invoke-virtual {p2}, Lcom/pspdfkit/document/processor/PagePdf;->getNativeDataDescriptor()Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    move-result-object v2

    .line 5
    invoke-virtual {p2}, Lcom/pspdfkit/document/processor/PagePdf;->getPageIndex()I

    move-result v3

    .line 6
    invoke-virtual {p2}, Lcom/pspdfkit/document/processor/PagePdf;->getNativeZPosition()Lcom/pspdfkit/internal/jni/NativeItemZPosition;

    move-result-object v4

    .line 7
    invoke-virtual {p2}, Lcom/pspdfkit/document/processor/PagePdf;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p3

    move v1, p1

    .line 8
    invoke-virtual/range {v0 .. v6}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->mergeAutoRotatedContentFromDataDescriptor(ILcom/pspdfkit/internal/jni/NativeDataDescriptor;ILcom/pspdfkit/internal/jni/NativeItemZPosition;Landroid/graphics/Matrix;Lcom/pspdfkit/internal/jni/NativeBlendMode;)V

    return-object p3
.end method

.method lambda$mergePage$18$com-pspdfkit-document-processor-PdfProcessorTask(ILcom/pspdfkit/document/processor/PagePdf;Lcom/pspdfkit/annotations/BlendMode;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 7

    .line 1
    invoke-direct {p0, p4, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkDestinationIndexOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;I)V

    .line 4
    invoke-virtual {p2}, Lcom/pspdfkit/document/processor/PagePdf;->getNativeDataDescriptor()Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    move-result-object v2

    .line 5
    invoke-virtual {p2}, Lcom/pspdfkit/document/processor/PagePdf;->getPageIndex()I

    move-result v3

    .line 6
    invoke-virtual {p2}, Lcom/pspdfkit/document/processor/PagePdf;->getNativeZPosition()Lcom/pspdfkit/internal/jni/NativeItemZPosition;

    move-result-object v4

    .line 7
    invoke-virtual {p2}, Lcom/pspdfkit/document/processor/PagePdf;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v5

    .line 8
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeBlendMode;->values()[Lcom/pspdfkit/internal/jni/NativeBlendMode;

    move-result-object p2

    invoke-virtual {p3}, Ljava/lang/Enum;->ordinal()I

    move-result p3

    aget-object v6, p2, p3

    move-object v0, p4

    move v1, p1

    .line 9
    invoke-virtual/range {v0 .. v6}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->mergeAutoRotatedContentFromDataDescriptor(ILcom/pspdfkit/internal/jni/NativeDataDescriptor;ILcom/pspdfkit/internal/jni/NativeItemZPosition;Landroid/graphics/Matrix;Lcom/pspdfkit/internal/jni/NativeBlendMode;)V

    return-object p4
.end method

.method lambda$movePages$4$com-pspdfkit-document-processor-PdfProcessorTask(Ljava/util/Set;ILcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 1

    .line 1
    invoke-direct {p0, p3, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkCurrentPageIndexesOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;Ljava/util/Set;)V

    .line 2
    invoke-direct {p0, p3, p2}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkDestinationIndexOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;I)V

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 3
    :cond_0
    instance-of v0, p1, Ljava/util/HashSet;

    if-eqz v0, :cond_1

    .line 4
    check-cast p1, Ljava/util/HashSet;

    goto :goto_0

    .line 7
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object p1, v0

    .line 8
    :goto_0
    invoke-virtual {p3, p1, p2}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->movePages(Ljava/util/HashSet;I)V

    return-object p3
.end method

.method lambda$removePages$3$com-pspdfkit-document-processor-PdfProcessorTask(Ljava/util/Set;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 1

    .line 1
    invoke-direct {p0, p2, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkCurrentPageIndexesOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;Ljava/util/Set;)V

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 2
    :cond_0
    instance-of v0, p1, Ljava/util/HashSet;

    if-eqz v0, :cond_1

    .line 3
    check-cast p1, Ljava/util/HashSet;

    goto :goto_0

    .line 6
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object p1, v0

    .line 7
    :goto_0
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->removePages(Ljava/util/HashSet;)V

    return-object p2
.end method

.method synthetic lambda$resizePage$13$com-pspdfkit-document-processor-PdfProcessorTask(ILcom/pspdfkit/utils/Size;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 2

    .line 1
    invoke-direct {p0, p3, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkCurrentPageIndexOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;I)V

    .line 2
    iget v0, p2, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v0, v0

    iget p2, p2, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int p2, p2

    sget-object v1, Lcom/pspdfkit/internal/jni/NativePageSizeFormat;->POINTS:Lcom/pspdfkit/internal/jni/NativePageSizeFormat;

    invoke-virtual {p3, p1, v0, p2, v1}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->scalePage(IIILcom/pspdfkit/internal/jni/NativePageSizeFormat;)V

    return-object p3
.end method

.method synthetic lambda$rotatePage$12$com-pspdfkit-document-processor-PdfProcessorTask(IILcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 0

    .line 1
    invoke-direct {p0, p3, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkCurrentPageIndexOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;I)V

    .line 2
    invoke-virtual {p3, p1, p2}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->rotatePage(II)V

    return-object p3
.end method

.method synthetic lambda$setPageBox$11$com-pspdfkit-document-processor-PdfProcessorTask(ILcom/pspdfkit/document/PdfBox;Landroid/graphics/RectF;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 0

    .line 1
    invoke-direct {p0, p4, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkDestinationIndexOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;I)V

    .line 2
    invoke-static {p2}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/document/PdfBox;)Lcom/pspdfkit/internal/jni/NativePDFBoxType;

    move-result-object p2

    invoke-virtual {p4, p1, p2, p3}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->changeBox(ILcom/pspdfkit/internal/jni/NativePDFBoxType;Landroid/graphics/RectF;)V

    return-object p4
.end method

.method synthetic lambda$setPageLabel$21$com-pspdfkit-document-processor-PdfProcessorTask(ILjava/lang/String;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 0

    .line 1
    invoke-direct {p0, p3, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->checkDestinationIndexOrThrow(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;I)V

    .line 2
    invoke-virtual {p3, p1, p2}, Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;->setPageLabel(ILjava/lang/String;)V

    return-object p3
.end method

.method public mergePage(Lcom/pspdfkit/document/processor/PagePdf;I)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    const-string v0, "pagePdf"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0, p2, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;ILcom/pspdfkit/document/processor/PagePdf;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public mergePage(Lcom/pspdfkit/document/processor/PagePdf;ILcom/pspdfkit/annotations/BlendMode;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 3

    const-string v0, "pagePdf"

    const-string v1, "argumentName"

    .line 56
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 107
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "blendMode"

    .line 109
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    invoke-static {p3, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 161
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->COMPARISON:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    invoke-virtual {p1}, Lcom/pspdfkit/document/processor/PagePdf;->getPosition()Lcom/pspdfkit/document/processor/PagePosition;

    move-result-object v0

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;ILcom/pspdfkit/document/processor/PagePdf;Lcom/pspdfkit/annotations/BlendMode;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 171
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Page position parameter of PagePdf is not supported when using blendMode."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 172
    :cond_1
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Adding page for comparison requires document comparison feature in your license."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public movePages(Ljava/util/Set;I)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;I)",
            "Lcom/pspdfkit/document/processor/PdfProcessorTask;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda11;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda11;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;Ljava/util/Set;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Set of pages to move must not be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public performOcrOnPages(Ljava/util/Set;Lcom/pspdfkit/document/processor/ocr/OcrLanguage;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/pspdfkit/document/processor/ocr/OcrLanguage;",
            ")",
            "Lcom/pspdfkit/document/processor/PdfProcessorTask;"
        }
    .end annotation

    const-string v0, "Provided page indexes for OCR processing cannot be empty."

    .line 1
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/String;Ljava/util/Collection;)V

    const-string v0, "Provided page indexes for OCR processing cannot contain null elements."

    .line 2
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Ljava/util/Collection;)V

    const-string v0, "ocrLanguage"

    const-string v1, "argumentName"

    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 56
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 57
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->OCR:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda7;

    invoke-direct {v1, p2, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/document/processor/ocr/OcrLanguage;Ljava/util/Set;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 62
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Performing OCR requires OCR License."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public removePages(Ljava/util/Set;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/pspdfkit/document/processor/PdfProcessorTask;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;Ljava/util/Set;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Set of pages to remove must not be null!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public resizePage(ILcom/pspdfkit/utils/Size;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5
    iget v0, p2, Lcom/pspdfkit/utils/Size;->width:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p2, Lcom/pspdfkit/utils/Size;->height:F

    cmpg-float v0, v0, v1

    if-lez v0, :cond_0

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda23;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda23;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;ILcom/pspdfkit/utils/Size;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 9
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Page size must be positive!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 10
    :cond_1
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Scaling pages requires document editor feature in your license!"

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public rotatePage(II)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-eqz v0, :cond_1

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_1

    const/16 v1, 0xb4

    if-eq v0, v1, :cond_1

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 7
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Rotation value may only be 0, 90, 180 or 270."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 10
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda10;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 11
    :cond_2
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Rotating pages requires document editor feature in your license!"

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setFormFieldNameMappings(Ljava/util/Map;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/pspdfkit/document/processor/PdfProcessorTask;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda25;

    invoke-direct {v1, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda25;-><init>(Ljava/util/Map;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 9
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "formFieldNameMapping must not be null!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 10
    :cond_1
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Renaming fields / mappings in forms requires Document Editor license."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setFormMappingNameMappings(Ljava/util/Map;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/pspdfkit/document/processor/PdfProcessorTask;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda8;

    invoke-direct {v1, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda8;-><init>(Ljava/util/Map;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 10
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "formFieldNameMapping must not be null!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 11
    :cond_1
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Renaming fields / mappings in forms requires Document Editor license."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setPageBox(ILcom/pspdfkit/document/PdfBox;Landroid/graphics/RectF;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p2, :cond_3

    if-eqz p3, :cond_2

    .line 7
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    .line 8
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Rect sizes must not be zero!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 10
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda24;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda24;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;ILcom/pspdfkit/document/PdfBox;Landroid/graphics/RectF;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 11
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Box rect must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 12
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Box parameter must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 13
    :cond_4
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Modifying page box requires document editor feature in your license!"

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setPageLabel(ILjava/lang/String;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda17;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda17;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public stripEmptyPages(Z)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda22;

    invoke-direct {v1, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda22;-><init>(Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public withMetadata(Ljava/util/HashMap;)Lcom/pspdfkit/document/processor/PdfProcessorTask;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/pspdfkit/document/processor/PdfProcessorTask;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask;->configurationMappers:Ljava/util/List;

    new-instance v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda15;

    invoke-direct {v1, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda15;-><init>(Ljava/util/HashMap;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Metadata must not be null!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

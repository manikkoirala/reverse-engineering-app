.class public Lcom/pspdfkit/document/processor/PageCanvas;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final canvasCallback:Lcom/pspdfkit/document/processor/NewPage$OnDrawCanvasCallback;

.field private matrix:Landroid/graphics/Matrix;

.field private final pageSize:Lcom/pspdfkit/utils/Size;

.field private position:Lcom/pspdfkit/document/processor/PagePosition;

.field private zOrder:Lcom/pspdfkit/document/processor/PageZOrder;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/document/processor/NewPage$OnDrawCanvasCallback;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lcom/pspdfkit/document/processor/PageZOrder;->FOREGROUND:Lcom/pspdfkit/document/processor/PageZOrder;

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PageCanvas;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    .line 5
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PageCanvas;->matrix:Landroid/graphics/Matrix;

    const/4 v0, 0x0

    .line 8
    iput-object v0, p0, Lcom/pspdfkit/document/processor/PageCanvas;->position:Lcom/pspdfkit/document/processor/PagePosition;

    const-string v0, "pageSize"

    .line 19
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    .line 20
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PageCanvas;->pageSize:Lcom/pspdfkit/utils/Size;

    .line 23
    iput-object p2, p0, Lcom/pspdfkit/document/processor/PageCanvas;->canvasCallback:Lcom/pspdfkit/document/processor/NewPage$OnDrawCanvasCallback;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/document/processor/NewPage$OnDrawCanvasCallback;Landroid/graphics/Matrix;)V
    .locals 2

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    sget-object v0, Lcom/pspdfkit/document/processor/PageZOrder;->FOREGROUND:Lcom/pspdfkit/document/processor/PageZOrder;

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PageCanvas;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    .line 28
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PageCanvas;->matrix:Landroid/graphics/Matrix;

    const/4 v0, 0x0

    .line 31
    iput-object v0, p0, Lcom/pspdfkit/document/processor/PageCanvas;->position:Lcom/pspdfkit/document/processor/PagePosition;

    const-string v1, "pageSize"

    .line 62
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "callback"

    .line 63
    invoke-static {p2, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "matrix"

    .line 64
    invoke-static {p3, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PageCanvas;->pageSize:Lcom/pspdfkit/utils/Size;

    .line 67
    iput-object p2, p0, Lcom/pspdfkit/document/processor/PageCanvas;->canvasCallback:Lcom/pspdfkit/document/processor/NewPage$OnDrawCanvasCallback;

    .line 68
    iput-object v0, p0, Lcom/pspdfkit/document/processor/PageCanvas;->position:Lcom/pspdfkit/document/processor/PagePosition;

    .line 69
    iput-object p3, p0, Lcom/pspdfkit/document/processor/PageCanvas;->matrix:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/document/processor/NewPage$OnDrawCanvasCallback;Lcom/pspdfkit/document/processor/PagePosition;)V
    .locals 1

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    sget-object v0, Lcom/pspdfkit/document/processor/PageZOrder;->FOREGROUND:Lcom/pspdfkit/document/processor/PageZOrder;

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PageCanvas;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    .line 74
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/document/processor/PageCanvas;->matrix:Landroid/graphics/Matrix;

    const/4 v0, 0x0

    .line 77
    iput-object v0, p0, Lcom/pspdfkit/document/processor/PageCanvas;->position:Lcom/pspdfkit/document/processor/PagePosition;

    const-string v0, "pageSize"

    .line 130
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    .line 131
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "position"

    .line 132
    invoke-static {p3, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PageCanvas;->pageSize:Lcom/pspdfkit/utils/Size;

    .line 135
    iput-object p2, p0, Lcom/pspdfkit/document/processor/PageCanvas;->canvasCallback:Lcom/pspdfkit/document/processor/NewPage$OnDrawCanvasCallback;

    .line 136
    iput-object p3, p0, Lcom/pspdfkit/document/processor/PageCanvas;->position:Lcom/pspdfkit/document/processor/PagePosition;

    .line 137
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/document/processor/PageCanvas;->matrix:Landroid/graphics/Matrix;

    return-void
.end method


# virtual methods
.method getItemConfiguration()Lcom/pspdfkit/internal/jni/NativeItemConfiguration;
    .locals 9

    .line 1
    new-instance v0, Landroid/graphics/pdf/PdfDocument;

    invoke-direct {v0}, Landroid/graphics/pdf/PdfDocument;-><init>()V

    .line 2
    new-instance v1, Landroid/graphics/pdf/PdfDocument$PageInfo$Builder;

    iget-object v2, p0, Lcom/pspdfkit/document/processor/PageCanvas;->pageSize:Lcom/pspdfkit/utils/Size;

    iget v2, v2, Lcom/pspdfkit/utils/Size;->width:F

    float-to-double v2, v2

    .line 4
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget-object v3, p0, Lcom/pspdfkit/document/processor/PageCanvas;->pageSize:Lcom/pspdfkit/utils/Size;

    iget v3, v3, Lcom/pspdfkit/utils/Size;->height:F

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Landroid/graphics/pdf/PdfDocument$PageInfo$Builder;-><init>(III)V

    .line 5
    invoke-virtual {v1}, Landroid/graphics/pdf/PdfDocument$PageInfo$Builder;->create()Landroid/graphics/pdf/PdfDocument$PageInfo;

    move-result-object v1

    .line 6
    invoke-virtual {v0, v1}, Landroid/graphics/pdf/PdfDocument;->startPage(Landroid/graphics/pdf/PdfDocument$PageInfo;)Landroid/graphics/pdf/PdfDocument$Page;

    move-result-object v1

    .line 7
    invoke-virtual {v1}, Landroid/graphics/pdf/PdfDocument$Page;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v2

    .line 9
    iget-object v3, p0, Lcom/pspdfkit/document/processor/PageCanvas;->pageSize:Lcom/pspdfkit/utils/Size;

    iget v3, v3, Lcom/pspdfkit/utils/Size;->height:F

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-float v3, v3

    iget-object v4, p0, Lcom/pspdfkit/document/processor/PageCanvas;->pageSize:Lcom/pspdfkit/utils/Size;

    iget v4, v4, Lcom/pspdfkit/utils/Size;->height:F

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 10
    iget-object v3, p0, Lcom/pspdfkit/document/processor/PageCanvas;->canvasCallback:Lcom/pspdfkit/document/processor/NewPage$OnDrawCanvasCallback;

    invoke-interface {v3, v2}, Lcom/pspdfkit/document/processor/NewPage$OnDrawCanvasCallback;->onDrawCanvas(Landroid/graphics/Canvas;)V

    .line 11
    invoke-virtual {v0, v1}, Landroid/graphics/pdf/PdfDocument;->finishPage(Landroid/graphics/pdf/PdfDocument$Page;)V

    .line 14
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x200

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 16
    :try_start_0
    invoke-virtual {v0, v1}, Landroid/graphics/pdf/PdfDocument;->writeTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    invoke-virtual {v0}, Landroid/graphics/pdf/PdfDocument;->close()V

    .line 24
    new-instance v0, Lcom/pspdfkit/internal/bj;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/bj;-><init>([B)V

    .line 27
    iget-object v1, p0, Lcom/pspdfkit/document/processor/PageCanvas;->position:Lcom/pspdfkit/document/processor/PagePosition;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeItemRelativePosition;->values()[Lcom/pspdfkit/internal/jni/NativeItemRelativePosition;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/document/processor/PageCanvas;->position:Lcom/pspdfkit/document/processor/PagePosition;

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    :goto_0
    move-object v6, v1

    .line 28
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeItemZPosition;->values()[Lcom/pspdfkit/internal/jni/NativeItemZPosition;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/document/processor/PageCanvas;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget-object v7, v1, v2

    .line 29
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeItemConfiguration;

    .line 31
    invoke-static {v0}, Lcom/pspdfkit/internal/s7;->createNativeDataDescriptor(Lcom/pspdfkit/document/providers/DataProvider;)Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    move-result-object v4

    iget-object v8, p0, Lcom/pspdfkit/document/processor/PageCanvas;->matrix:Landroid/graphics/Matrix;

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v8}, Lcom/pspdfkit/internal/jni/NativeItemConfiguration;-><init>(Lcom/pspdfkit/internal/jni/NativeImage;Lcom/pspdfkit/internal/jni/NativeDataDescriptor;Ljava/lang/Integer;Lcom/pspdfkit/internal/jni/NativeItemRelativePosition;Lcom/pspdfkit/internal/jni/NativeItemZPosition;Landroid/graphics/Matrix;)V

    return-object v1

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v1

    .line 32
    :try_start_1
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Couldn\'t write the document canvas to an output stream."

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 34
    :goto_1
    invoke-virtual {v0}, Landroid/graphics/pdf/PdfDocument;->close()V

    .line 35
    throw v1
.end method

.method public getMatrix()Landroid/graphics/Matrix;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PageCanvas;->matrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getPosition()Lcom/pspdfkit/document/processor/PagePosition;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PageCanvas;->position:Lcom/pspdfkit/document/processor/PagePosition;

    return-object v0
.end method

.method public getZOrder()Lcom/pspdfkit/document/processor/PageZOrder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/document/processor/PageCanvas;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    return-object v0
.end method

.method public setZOrder(Lcom/pspdfkit/document/processor/PageZOrder;)V
    .locals 2

    const-string v0, "zOrder"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/document/processor/PageCanvas;->zOrder:Lcom/pspdfkit/document/processor/PageZOrder;

    return-void
.end method

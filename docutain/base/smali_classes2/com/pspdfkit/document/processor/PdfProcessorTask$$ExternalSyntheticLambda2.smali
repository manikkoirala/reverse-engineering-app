.class public final synthetic Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda2;
.super Ljava/lang/Object;
.source "D8$$SyntheticClass"

# interfaces
.implements Lcom/pspdfkit/document/processor/PdfProcessorTask$NativeProcessorConfigurationMapper;


# instance fields
.field public final synthetic f$0:Lcom/pspdfkit/document/processor/PdfProcessorTask;

.field public final synthetic f$1:I

.field public final synthetic f$2:Lcom/pspdfkit/document/processor/PagePdf;

.field public final synthetic f$3:Lcom/pspdfkit/annotations/BlendMode;


# direct methods
.method public synthetic constructor <init>(Lcom/pspdfkit/document/processor/PdfProcessorTask;ILcom/pspdfkit/document/processor/PagePdf;Lcom/pspdfkit/annotations/BlendMode;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda2;->f$0:Lcom/pspdfkit/document/processor/PdfProcessorTask;

    iput p2, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda2;->f$1:I

    iput-object p3, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda2;->f$2:Lcom/pspdfkit/document/processor/PagePdf;

    iput-object p4, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda2;->f$3:Lcom/pspdfkit/annotations/BlendMode;

    return-void
.end method


# virtual methods
.method public final apply(Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;
    .locals 4

    iget-object v0, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda2;->f$0:Lcom/pspdfkit/document/processor/PdfProcessorTask;

    iget v1, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda2;->f$1:I

    iget-object v2, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda2;->f$2:Lcom/pspdfkit/document/processor/PagePdf;

    iget-object v3, p0, Lcom/pspdfkit/document/processor/PdfProcessorTask$$ExternalSyntheticLambda2;->f$3:Lcom/pspdfkit/annotations/BlendMode;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->lambda$mergePage$18$com-pspdfkit-document-processor-PdfProcessorTask(ILcom/pspdfkit/document/processor/PagePdf;Lcom/pspdfkit/annotations/BlendMode;Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;)Lcom/pspdfkit/internal/jni/NativeProcessorConfiguration;

    move-result-object p1

    return-object p1
.end method

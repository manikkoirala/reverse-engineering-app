.class public Lcom/pspdfkit/document/PdfDocumentLoader;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private checkpointerConfiguration:Lcom/pspdfkit/internal/h5;

.field private final context:Landroid/content/Context;

.field private final documentSources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;"
        }
    .end annotation
.end field

.field private isMultithreadedRenderingEnabled:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/document/PdfDocumentLoader;->isMultithreadedRenderingEnabled:Z

    const-string v0, "context"

    .line 8
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "documentSources"

    .line 9
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    iput-object p1, p0, Lcom/pspdfkit/document/PdfDocumentLoader;->context:Landroid/content/Context;

    .line 11
    iput-object p2, p0, Lcom/pspdfkit/document/PdfDocumentLoader;->documentSources:Ljava/util/List;

    return-void
.end method

.method static fromDocumentSource(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/PdfDocumentLoader;
    .locals 3

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 54
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "documentSource"

    .line 56
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 108
    new-instance v0, Lcom/pspdfkit/document/PdfDocumentLoader;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/document/PdfDocumentLoader;-><init>(Landroid/content/Context;Ljava/util/List;)V

    return-object v0
.end method

.method private static fromDocumentSources(Landroid/content/Context;Ljava/util/List;)Lcom/pspdfkit/document/PdfDocumentLoader;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;)",
            "Lcom/pspdfkit/document/PdfDocumentLoader;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 54
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "documentSources"

    .line 56
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "At least one document source is required to open a PDF!"

    .line 108
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/String;Ljava/util/Collection;)V

    .line 110
    new-instance v0, Lcom/pspdfkit/document/PdfDocumentLoader;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/document/PdfDocumentLoader;-><init>(Landroid/content/Context;Ljava/util/List;)V

    return-object v0
.end method

.method static synthetic lambda$openDocumentAsync$0(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/document/PdfDocument;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    return-object p0
.end method

.method public static openDocument(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/document/PdfDocument;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 54
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "documentUri"

    .line 56
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 108
    new-instance v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v0, p1}, Lcom/pspdfkit/document/DocumentSource;-><init>(Landroid/net/Uri;)V

    invoke-static {p0, v0}, Lcom/pspdfkit/document/PdfDocumentLoader;->fromDocumentSource(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p0

    return-object p0
.end method

.method public static openDocument(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lcom/pspdfkit/document/PdfDocument;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;
        }
    .end annotation

    .line 109
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 111
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 162
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "documentUri"

    .line 164
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 216
    new-instance v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/document/DocumentSource;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/pspdfkit/document/PdfDocumentLoader;->fromDocumentSource(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    .line 217
    invoke-virtual {p0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p0

    return-object p0
.end method

.method public static openDocument(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/PdfDocument;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 219
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 270
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "source"

    .line 272
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 323
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 324
    invoke-static {p0, p1}, Lcom/pspdfkit/document/PdfDocumentLoader;->fromDocumentSource(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p0

    return-object p0
.end method

.method public static openDocument(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;Z)Lcom/pspdfkit/document/PdfDocument;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 325
    invoke-static {p0, p1}, Lcom/pspdfkit/document/PdfDocumentLoader;->fromDocumentSource(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    .line 326
    invoke-direct {p0, p2}, Lcom/pspdfkit/document/PdfDocumentLoader;->setMultithreadedRenderingEnabled(Z)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    .line 327
    invoke-virtual {p0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p0

    return-object p0
.end method

.method private openDocumentAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/document/PdfDocument;",
            ">;"
        }
    .end annotation

    .line 222
    iget-object v0, p0, Lcom/pspdfkit/document/PdfDocumentLoader;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/pspdfkit/document/PdfDocumentLoader;->documentSources:Ljava/util/List;

    .line 225
    iget-object v2, p0, Lcom/pspdfkit/document/PdfDocumentLoader;->checkpointerConfiguration:Lcom/pspdfkit/internal/h5;

    if-eqz v2, :cond_0

    goto :goto_0

    .line 227
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/h5$a;->a()Lcom/pspdfkit/internal/h5;

    move-result-object v2

    :goto_0
    iget-boolean v3, p0, Lcom/pspdfkit/document/PdfDocumentLoader;->isMultithreadedRenderingEnabled:Z

    .line 228
    invoke-static {v0, v1, v2, v3}, Lcom/pspdfkit/internal/e9;->a(Landroid/content/Context;Ljava/util/List;Lcom/pspdfkit/internal/h5;Z)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/document/PdfDocumentLoader$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/pspdfkit/document/PdfDocumentLoader$$ExternalSyntheticLambda0;-><init>()V

    .line 235
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public static openDocumentAsync(Landroid/content/Context;Landroid/net/Uri;)Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/document/PdfDocument;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 54
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "documentUri"

    .line 56
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 108
    new-instance v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v0, p1}, Lcom/pspdfkit/document/DocumentSource;-><init>(Landroid/net/Uri;)V

    invoke-static {p0, v0}, Lcom/pspdfkit/document/PdfDocumentLoader;->fromDocumentSource(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    invoke-direct {p0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocumentAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method public static openDocumentAsync(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/document/PdfDocument;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;
        }
    .end annotation

    .line 109
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 111
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 162
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "documentUri"

    .line 164
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 216
    new-instance v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/document/DocumentSource;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/pspdfkit/document/PdfDocumentLoader;->fromDocumentSource(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    .line 217
    invoke-direct {p0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocumentAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method public static openDocumentAsync(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lio/reactivex/rxjava3/core/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/document/DocumentSource;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/document/PdfDocument;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;
        }
    .end annotation

    .line 218
    invoke-static {p0, p1}, Lcom/pspdfkit/document/PdfDocumentLoader;->fromDocumentSource(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    invoke-direct {p0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocumentAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method public static openDocumentAsync(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;Z)Lio/reactivex/rxjava3/core/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/document/DocumentSource;",
            "Z)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/document/PdfDocument;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;
        }
    .end annotation

    .line 219
    invoke-static {p0, p1}, Lcom/pspdfkit/document/PdfDocumentLoader;->fromDocumentSource(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    .line 220
    invoke-direct {p0, p2}, Lcom/pspdfkit/document/PdfDocumentLoader;->setMultithreadedRenderingEnabled(Z)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    .line 221
    invoke-direct {p0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocumentAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method public static openDocuments(Landroid/content/Context;Ljava/util/List;)Lcom/pspdfkit/document/PdfDocument;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;)",
            "Lcom/pspdfkit/document/PdfDocument;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-static {p0, p1}, Lcom/pspdfkit/document/PdfDocumentLoader;->fromDocumentSources(Landroid/content/Context;Ljava/util/List;)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p0

    return-object p0
.end method

.method public static openDocuments(Landroid/content/Context;Ljava/util/List;Z)Lcom/pspdfkit/document/PdfDocument;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;Z)",
            "Lcom/pspdfkit/document/PdfDocument;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    invoke-static {p0, p1}, Lcom/pspdfkit/document/PdfDocumentLoader;->fromDocumentSources(Landroid/content/Context;Ljava/util/List;)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    .line 3
    invoke-direct {p0, p2}, Lcom/pspdfkit/document/PdfDocumentLoader;->setMultithreadedRenderingEnabled(Z)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p0

    return-object p0
.end method

.method public static openDocumentsAsync(Landroid/content/Context;Ljava/util/List;)Lio/reactivex/rxjava3/core/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/document/PdfDocument;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {p0, p1}, Lcom/pspdfkit/document/PdfDocumentLoader;->fromDocumentSources(Landroid/content/Context;Ljava/util/List;)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    invoke-direct {p0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocumentAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method public static openDocumentsAsync(Landroid/content/Context;Ljava/util/List;Z)Lio/reactivex/rxjava3/core/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;Z)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/document/PdfDocument;",
            ">;"
        }
    .end annotation

    .line 2
    invoke-static {p0, p1}, Lcom/pspdfkit/document/PdfDocumentLoader;->fromDocumentSources(Landroid/content/Context;Ljava/util/List;)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    .line 3
    invoke-direct {p0, p2}, Lcom/pspdfkit/document/PdfDocumentLoader;->setMultithreadedRenderingEnabled(Z)Lcom/pspdfkit/document/PdfDocumentLoader;

    move-result-object p0

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocumentAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method private setMultithreadedRenderingEnabled(Z)Lcom/pspdfkit/document/PdfDocumentLoader;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/document/PdfDocumentLoader;->isMultithreadedRenderingEnabled:Z

    return-object p0
.end method


# virtual methods
.method openDocument()Lcom/pspdfkit/document/PdfDocument;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 328
    :try_start_0
    invoke-direct {p0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocumentAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/document/PdfDocument;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 330
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/io/IOException;

    if-eqz v1, :cond_0

    .line 331
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 333
    :cond_0
    throw v0
.end method

.method setCheckpointerConfiguration(Lcom/pspdfkit/internal/h5;)Lcom/pspdfkit/document/PdfDocumentLoader;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/document/PdfDocumentLoader;->checkpointerConfiguration:Lcom/pspdfkit/internal/h5;

    return-object p0
.end method

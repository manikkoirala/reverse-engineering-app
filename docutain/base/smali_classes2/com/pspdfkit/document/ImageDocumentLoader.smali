.class public final Lcom/pspdfkit/document/ImageDocumentLoader;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultImageDocumentActivityConfiguration(Landroid/content/Context;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
    .locals 2

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    invoke-direct {v0, p0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/pspdfkit/document/ImageDocumentLoader;->getDefaultImageDocumentActivityConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p0

    return-object p0
.end method

.method private static getDefaultImageDocumentActivityConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
    .locals 6

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NONE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->HIGHLIGHT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->STRIKEOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v4, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->UNDERLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v5, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SQUIGGLY:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-static {v1, v2, v3, v4, v5}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-static {v1}, Ljava/util/EnumSet;->complementOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 115
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->disableBookmarkList()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_NONE:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    .line 116
    invoke-virtual {v1, v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->setThumbnailBarMode(Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object v1

    .line 117
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->hidePageNumberOverlay()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object v1

    .line 119
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p0

    invoke-static {p0}, Lcom/pspdfkit/document/ImageDocumentLoader;->getDefaultImageDocumentConfiguration(Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p0

    .line 120
    invoke-virtual {v1, p0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->configuration(Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object p0

    .line 122
    invoke-virtual {p0, v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->enabledAnnotationTools(Ljava/util/List;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    move-result-object p0

    .line 123
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p0

    return-object p0
.end method

.method public static getDefaultImageDocumentActivityConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
    .locals 2

    const-string v0, "configuration"

    const-string v1, "argumentName"

    .line 56
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 107
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 108
    new-instance v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;

    invoke-direct {v0, p0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V

    invoke-static {v0}, Lcom/pspdfkit/document/ImageDocumentLoader;->getDefaultImageDocumentActivityConfiguration(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object p0

    return-object p0
.end method

.method public static getDefaultImageDocumentConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-direct {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;-><init>()V

    invoke-static {v0}, Lcom/pspdfkit/document/ImageDocumentLoader;->getDefaultImageDocumentConfiguration(Lcom/pspdfkit/configuration/PdfConfiguration$Builder;)Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    return-object v0
.end method

.method private static getDefaultImageDocumentConfiguration(Lcom/pspdfkit/configuration/PdfConfiguration$Builder;)Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 1

    .line 56
    sget-object v0, Lcom/pspdfkit/configuration/page/PageFitMode;->FIT_TO_SCREEN:Lcom/pspdfkit/configuration/page/PageFitMode;

    invoke-virtual {p0, v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->fitMode(Lcom/pspdfkit/configuration/page/PageFitMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    move-result-object p0

    sget-object v0, Lcom/pspdfkit/configuration/page/PageLayoutMode;->SINGLE:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    .line 57
    invoke-virtual {p0, v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->layoutMode(Lcom/pspdfkit/configuration/page/PageLayoutMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    move-result-object p0

    const/4 v0, 0x0

    .line 58
    invoke-virtual {p0, v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->textSelectionEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    move-result-object p0

    .line 59
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->build()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p0

    return-object p0
.end method

.method public static getDefaultImageDocumentConfiguration(Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 2

    const-string v0, "configuration"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    new-instance v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-direct {v0, p0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/PdfConfiguration;)V

    invoke-static {v0}, Lcom/pspdfkit/document/ImageDocumentLoader;->getDefaultImageDocumentConfiguration(Lcom/pspdfkit/configuration/PdfConfiguration$Builder;)Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$openDocumentAsync$0(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/internal/ae;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-static {p0, p1}, Lcom/pspdfkit/document/ImageDocumentLoader;->openImageDocument(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/internal/ae;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$openDocumentAsync$1(Lcom/pspdfkit/internal/ae;)Lcom/pspdfkit/document/ImageDocument;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    return-object p0
.end method

.method public static openDocument(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/document/ImageDocument;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 54
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "documentUri"

    .line 56
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 108
    new-instance v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v0, p1}, Lcom/pspdfkit/document/DocumentSource;-><init>(Landroid/net/Uri;)V

    invoke-static {p0, v0}, Lcom/pspdfkit/document/ImageDocumentLoader;->openDocument(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/ImageDocument;

    move-result-object p0

    return-object p0
.end method

.method public static openDocument(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/ImageDocument;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 109
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 111
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 162
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "source"

    const-string v1, "Document source is required to open an image document!"

    .line 163
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 166
    :try_start_0
    invoke-static {p0, p1}, Lcom/pspdfkit/document/ImageDocumentLoader;->openImageDocument(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/internal/ae;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 168
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    instance-of p1, p1, Ljava/io/IOException;

    if-eqz p1, :cond_0

    .line 169
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    check-cast p0, Ljava/io/IOException;

    throw p0

    .line 171
    :cond_0
    throw p0
.end method

.method public static openDocumentAsync(Landroid/content/Context;Landroid/net/Uri;)Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/document/ImageDocument;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 54
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "documentUri"

    .line 56
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 108
    new-instance v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v0, p1}, Lcom/pspdfkit/document/DocumentSource;-><init>(Landroid/net/Uri;)V

    invoke-static {p0, v0}, Lcom/pspdfkit/document/ImageDocumentLoader;->openDocumentAsync(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method public static openDocumentAsync(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/document/DocumentSource;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/document/ImageDocument;",
            ">;"
        }
    .end annotation

    .line 109
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 111
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 162
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "source"

    .line 164
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 216
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    .line 217
    new-instance v0, Lcom/pspdfkit/document/ImageDocumentLoader$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/document/ImageDocumentLoader$$ExternalSyntheticLambda0;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    new-instance p1, Lcom/pspdfkit/document/ImageDocumentLoader$$ExternalSyntheticLambda1;

    invoke-direct {p1}, Lcom/pspdfkit/document/ImageDocumentLoader$$ExternalSyntheticLambda1;-><init>()V

    .line 218
    invoke-virtual {p0, p1}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method private static openImageDocument(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/internal/ae;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "documentSource"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/e9;->a(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/DocumentSource;

    move-result-object p0

    .line 108
    invoke-static {p0}, Lcom/pspdfkit/internal/ae;->a(Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/internal/ae;

    move-result-object p0

    return-object p0
.end method

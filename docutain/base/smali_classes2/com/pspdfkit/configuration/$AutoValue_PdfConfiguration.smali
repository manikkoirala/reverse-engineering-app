.class abstract Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;
.super Lcom/pspdfkit/configuration/PdfConfiguration;
.source "SourceFile"


# instance fields
.field private final A:Z

.field private final B:Z

.field private final C:Z

.field private final D:F

.field private final E:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final F:Z

.field private final G:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private final H:Z

.field private final I:I

.field private final J:Z

.field private final K:Z

.field private final L:Z

.field private final M:Z

.field private final N:Z

.field private final O:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;",
            ">;"
        }
    .end annotation
.end field

.field private final P:Z

.field private final Q:Z

.field private final R:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

.field private final S:Ljava/lang/Integer;

.field private final T:Z

.field private final U:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

.field private final V:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

.field private final W:Ljava/lang/String;

.field private final X:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

.field private final Y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;",
            ">;"
        }
    .end annotation
.end field

.field private final Z:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

.field private final a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

.field private final a0:Lcom/pspdfkit/signatures/SignatureAppearance;

.field private final b:Lcom/pspdfkit/configuration/page/PageScrollMode;

.field private final b0:Z

.field private final c:Lcom/pspdfkit/configuration/page/PageFitMode;

.field private final c0:Z

.field private final d:Lcom/pspdfkit/configuration/page/PageLayoutMode;

.field private final d0:Z

.field private final e:Lcom/pspdfkit/configuration/theming/ThemeMode;

.field private final e0:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/sharing/ShareFeatures;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Z

.field private final f0:Z

.field private final g:Z

.field private final g0:Z

.field private final h:Z

.field private final h0:Z

.field private final i:I

.field private final i0:I

.field private final j:Ljava/lang/Integer;

.field private final j0:Z

.field private final k:I

.field private final k0:Z

.field private final l:Z

.field private final l0:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/utils/Size;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Z

.field private final n:F

.field private final o:F

.field private final p:Z

.field private final q:Z

.field private final r:Z

.field private final s:Z

.field private final t:Z

.field private final u:Z

.field private final v:Z

.field private final w:Z

.field private final x:Z

.field private final y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(FFFIIIILcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/page/PageFitMode;Lcom/pspdfkit/configuration/page/PageLayoutMode;Lcom/pspdfkit/configuration/page/PageScrollDirection;Lcom/pspdfkit/configuration/page/PageScrollMode;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/theming/ThemeMode;Lcom/pspdfkit/signatures/SignatureAppearance;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/EnumSet;Ljava/util/EnumSet;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Map;ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p8

    move-object/from16 v2, p9

    move-object/from16 v3, p10

    move-object/from16 v4, p11

    move-object/from16 v5, p12

    move-object/from16 v6, p13

    move-object/from16 v7, p14

    move-object/from16 v8, p15

    move-object/from16 v9, p16

    move-object/from16 v10, p17

    move-object/from16 v11, p22

    move-object/from16 v12, p23

    move-object/from16 v13, p24

    move-object/from16 v14, p25

    move-object/from16 v15, p26

    move-object/from16 v13, p27

    move-object/from16 v7, p28

    .line 1
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/configuration/PdfConfiguration;-><init>()V

    if-eqz v5, :cond_11

    .line 5
    iput-object v5, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    if-eqz v6, :cond_10

    .line 9
    iput-object v6, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->b:Lcom/pspdfkit/configuration/page/PageScrollMode;

    if-eqz v3, :cond_f

    .line 13
    iput-object v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->c:Lcom/pspdfkit/configuration/page/PageFitMode;

    if-eqz v4, :cond_e

    .line 17
    iput-object v4, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->d:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    if-eqz v10, :cond_d

    .line 21
    iput-object v10, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->e:Lcom/pspdfkit/configuration/theming/ThemeMode;

    move/from16 v3, p30

    .line 22
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->f:Z

    move/from16 v3, p31

    .line 23
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->g:Z

    move/from16 v3, p32

    .line 24
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->h:Z

    move/from16 v3, p4

    .line 25
    iput v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->i:I

    move-object/from16 v3, p19

    .line 26
    iput-object v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->j:Ljava/lang/Integer;

    move/from16 v3, p5

    .line 27
    iput v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->k:I

    move/from16 v3, p33

    .line 28
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->l:Z

    move/from16 v3, p34

    .line 29
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->m:Z

    move/from16 v3, p1

    .line 30
    iput v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->n:F

    move/from16 v3, p2

    .line 31
    iput v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->o:F

    move/from16 v3, p35

    .line 32
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->p:Z

    move/from16 v3, p36

    .line 33
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->q:Z

    move/from16 v3, p37

    .line 34
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->r:Z

    move/from16 v3, p38

    .line 35
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->s:Z

    move/from16 v3, p39

    .line 36
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->t:Z

    move/from16 v3, p40

    .line 37
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->u:Z

    move/from16 v3, p41

    .line 38
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->v:Z

    move/from16 v3, p42

    .line 39
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->w:Z

    move/from16 v3, p43

    .line 40
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->x:Z

    if-eqz v14, :cond_c

    .line 44
    iput-object v14, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->y:Ljava/util/List;

    if-eqz v15, :cond_b

    .line 48
    iput-object v15, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->z:Ljava/util/List;

    move/from16 v3, p44

    .line 49
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->A:Z

    move/from16 v3, p45

    .line 50
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->B:Z

    move/from16 v3, p46

    .line 51
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->C:Z

    move/from16 v3, p3

    .line 52
    iput v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->D:F

    if-eqz v13, :cond_a

    .line 56
    iput-object v13, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->E:Ljava/util/List;

    move/from16 v3, p47

    .line 57
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->F:Z

    if-eqz v11, :cond_9

    .line 61
    iput-object v11, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->G:Ljava/util/ArrayList;

    move/from16 v3, p48

    .line 62
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->H:Z

    move/from16 v3, p6

    .line 63
    iput v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->I:I

    move/from16 v3, p49

    .line 64
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->J:Z

    move/from16 v3, p50

    .line 65
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->K:Z

    move/from16 v3, p51

    .line 66
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->L:Z

    move/from16 v3, p52

    .line 67
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->M:Z

    move/from16 v3, p53

    .line 68
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->N:Z

    if-eqz v12, :cond_8

    .line 72
    iput-object v12, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->O:Ljava/util/EnumSet;

    move/from16 v3, p54

    .line 73
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->P:Z

    move/from16 v3, p55

    .line 74
    iput-boolean v3, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Q:Z

    if-eqz v1, :cond_7

    .line 78
    iput-object v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->R:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    move-object/from16 v1, p20

    .line 79
    iput-object v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->S:Ljava/lang/Integer;

    move/from16 v1, p56

    .line 80
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->T:Z

    if-eqz v2, :cond_6

    .line 84
    iput-object v2, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->U:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    if-eqz v9, :cond_5

    .line 88
    iput-object v9, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->V:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-object/from16 v1, p21

    .line 89
    iput-object v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->W:Ljava/lang/String;

    if-eqz v8, :cond_4

    .line 93
    iput-object v8, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->X:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    if-eqz v7, :cond_3

    .line 97
    iput-object v7, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Y:Ljava/util/List;

    move-object/from16 v1, p14

    if-eqz v1, :cond_2

    .line 101
    iput-object v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Z:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    move-object/from16 v1, p18

    .line 102
    iput-object v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->a0:Lcom/pspdfkit/signatures/SignatureAppearance;

    move/from16 v1, p57

    .line 103
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->b0:Z

    move/from16 v1, p58

    .line 104
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->c0:Z

    move/from16 v1, p59

    .line 105
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->d0:Z

    move-object/from16 v1, p24

    if-eqz v1, :cond_1

    .line 109
    iput-object v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->e0:Ljava/util/EnumSet;

    move/from16 v1, p60

    .line 110
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->f0:Z

    move/from16 v1, p61

    .line 111
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->g0:Z

    move/from16 v1, p62

    .line 112
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->h0:Z

    move/from16 v1, p7

    .line 113
    iput v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->i0:I

    move/from16 v1, p63

    .line 114
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->j0:Z

    move/from16 v1, p64

    .line 115
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->k0:Z

    move-object/from16 v1, p29

    if-eqz v1, :cond_0

    .line 119
    iput-object v1, v0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->l0:Ljava/util/Map;

    return-void

    .line 120
    :cond_0
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getMinimumAnnotationSizeMap"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 121
    :cond_1
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getEnabledShareFeatures"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 122
    :cond_2
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getSignatureCertificateSelectionMode"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 123
    :cond_3
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getSignatureCreationModes"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 124
    :cond_4
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getSignatureColorOptions"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 125
    :cond_5
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getSignatureSavingStrategy"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 126
    :cond_6
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getSignaturePickerOrientation"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 127
    :cond_7
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getAnnotationReplyFeatures"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 128
    :cond_8
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getEnabledCopyPasteFeatures"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 129
    :cond_9
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getExcludedAnnotationTypes"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 130
    :cond_a
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getGuideLineIntervals"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 131
    :cond_b
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getEnabledAnnotationTools"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 132
    :cond_c
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getEditableAnnotationTypes"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 133
    :cond_d
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getThemeMode"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 134
    :cond_e
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getLayoutMode"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 135
    :cond_f
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getFitMode"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 136
    :cond_10
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getScrollMode"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 137
    :cond_11
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getScrollDirection"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method protected final a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/utils/Size;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->l0:Ljava/util/Map;

    return-object v0
.end method

.method public allowMultipleBookmarksPerPage()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->f0:Z

    return v0
.end method

.method public animateScrollOnEdgeTaps()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->h0:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/configuration/PdfConfiguration;

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    .line 2
    check-cast p1, Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getScrollDirection()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->b:Lcom/pspdfkit/configuration/page/PageScrollMode;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getScrollMode()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->c:Lcom/pspdfkit/configuration/page/PageFitMode;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getFitMode()Lcom/pspdfkit/configuration/page/PageFitMode;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->d:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getLayoutMode()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->e:Lcom/pspdfkit/configuration/theming/ThemeMode;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getThemeMode()Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->f:Z

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isFirstPageAlwaysSingle()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->g:Z

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->showGapBetweenPages()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->h:Z

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isScrollbarsEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->i:I

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getBackgroundColor()I

    move-result v3

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->j:Ljava/lang/Integer;

    if-nez v1, :cond_1

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getLoadingProgressDrawable()Ljava/lang/Integer;

    move-result-object v1

    if-nez v1, :cond_5

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getLoadingProgressDrawable()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_0
    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->k:I

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMemoryCacheSize()I

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->l:Z

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->m:Z

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->n:F

    .line 16
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getStartZoomScale()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v1, v3, :cond_5

    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->o:F

    .line 17
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMaxZoomScale()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->p:Z

    .line 18
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->shouldZoomOutBounce()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->q:Z

    .line 19
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isTextSelectionEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->r:Z

    .line 20
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isFormEditingEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->s:Z

    .line 21
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAutoSelectNextFormElementEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->t:Z

    .line 22
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationEditingEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->u:Z

    .line 23
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationRotationEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->v:Z

    .line 24
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isContentEditingEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->w:Z

    .line 25
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isMeasurementsEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->x:Z

    .line 26
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationLimitedToPageBounds()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->y:Ljava/util/List;

    .line 27
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEditableAnnotationTypes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->z:Ljava/util/List;

    .line 28
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledAnnotationTools()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->A:Z

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSelectedAnnotationResizeEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->B:Z

    .line 30
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSelectedAnnotationResizeGuidesEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->C:Z

    .line 31
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSelectedAnnotationFontScalingOnResizeEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->D:F

    .line 32
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getResizeGuideSnapAllowance()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->E:Ljava/util/List;

    .line 33
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getGuideLineIntervals()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->F:Z

    .line 34
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationInspectorEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->G:Ljava/util/ArrayList;

    .line 35
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getExcludedAnnotationTypes()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->H:Z

    .line 36
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAutosaveEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->I:I

    .line 37
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getPagePadding()I

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->J:Z

    .line 38
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isVideoPlaybackEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->K:Z

    .line 39
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isPlayingMultipleMediaInstancesEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->L:Z

    .line 40
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isLastViewedPageRestorationEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->M:Z

    .line 41
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAutomaticLinkGenerationEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->N:Z

    .line 42
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isCopyPasteEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->O:Ljava/util/EnumSet;

    .line 43
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledCopyPasteFeatures()Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->P:Z

    .line 44
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isUndoEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Q:Z

    .line 45
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isRedoEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->R:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    .line 46
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getAnnotationReplyFeatures()Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->S:Ljava/lang/Integer;

    if-nez v1, :cond_2

    .line 47
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getFixedLowResRenderPixelCount()Ljava/lang/Integer;

    move-result-object v1

    if-nez v1, :cond_5

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getFixedLowResRenderPixelCount()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_1
    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->T:Z

    .line 48
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isMultithreadedRenderingEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->U:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    .line 49
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignaturePickerOrientation()Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->V:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 50
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->W:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 51
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getDefaultSigner()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getDefaultSigner()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_2
    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->X:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    .line 52
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Y:Ljava/util/List;

    .line 53
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Z:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    .line 54
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureCertificateSelectionMode()Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->a0:Lcom/pspdfkit/signatures/SignatureAppearance;

    if-nez v1, :cond_4

    .line 55
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureAppearance()Lcom/pspdfkit/signatures/SignatureAppearance;

    move-result-object v1

    if-nez v1, :cond_5

    goto :goto_3

    :cond_4
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureAppearance()Lcom/pspdfkit/signatures/SignatureAppearance;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_3
    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->b0:Z

    .line 56
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isNoteAnnotationNoZoomHandlingEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->c0:Z

    .line 57
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isJavaScriptEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->d0:Z

    .line 58
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isTextSelectionPopupToolbarEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->e0:Ljava/util/EnumSet;

    .line 59
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledShareFeatures()Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->f0:Z

    .line 60
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->allowMultipleBookmarksPerPage()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->g0:Z

    .line 61
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->scrollOnEdgeTapEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->h0:Z

    .line 62
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->animateScrollOnEdgeTaps()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->i0:I

    .line 63
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->scrollOnEdgeTapMargin()I

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->j0:Z

    .line 64
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isMagnifierEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->k0:Z

    .line 65
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->showSignHereOverlay()Z

    move-result v3

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->l0:Ljava/util/Map;

    .line 66
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->a()Ljava/util/Map;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    goto :goto_4

    :cond_5
    const/4 v0, 0x0

    :goto_4
    return v0

    :cond_6
    return v2
.end method

.method public getAnnotationReplyFeatures()Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->R:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    return-object v0
.end method

.method public getBackgroundColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->i:I

    return v0
.end method

.method public getDefaultSigner()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->W:Ljava/lang/String;

    return-object v0
.end method

.method public getEditableAnnotationTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->y:Ljava/util/List;

    return-object v0
.end method

.method public getEnabledAnnotationTools()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->z:Ljava/util/List;

    return-object v0
.end method

.method public getEnabledCopyPasteFeatures()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->O:Ljava/util/EnumSet;

    return-object v0
.end method

.method public getEnabledShareFeatures()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/sharing/ShareFeatures;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->e0:Ljava/util/EnumSet;

    return-object v0
.end method

.method public getExcludedAnnotationTypes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->G:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFitMode()Lcom/pspdfkit/configuration/page/PageFitMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->c:Lcom/pspdfkit/configuration/page/PageFitMode;

    return-object v0
.end method

.method public getFixedLowResRenderPixelCount()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->S:Ljava/lang/Integer;

    return-object v0
.end method

.method public getGuideLineIntervals()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->E:Ljava/util/List;

    return-object v0
.end method

.method public getLayoutMode()Lcom/pspdfkit/configuration/page/PageLayoutMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->d:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    return-object v0
.end method

.method public getLoadingProgressDrawable()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->j:Ljava/lang/Integer;

    return-object v0
.end method

.method public getMaxZoomScale()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->o:F

    return v0
.end method

.method public getMemoryCacheSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->k:I

    return v0
.end method

.method public getPagePadding()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->I:I

    return v0
.end method

.method public getResizeGuideSnapAllowance()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->D:F

    return v0
.end method

.method public getScrollDirection()Lcom/pspdfkit/configuration/page/PageScrollDirection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    return-object v0
.end method

.method public getScrollMode()Lcom/pspdfkit/configuration/page/PageScrollMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->b:Lcom/pspdfkit/configuration/page/PageScrollMode;

    return-object v0
.end method

.method public getSelectedAnnotationFontScalingOnResizeEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->C:Z

    return v0
.end method

.method public getSelectedAnnotationResizeEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->A:Z

    return v0
.end method

.method public getSelectedAnnotationResizeGuidesEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->B:Z

    return v0
.end method

.method public getSignatureAppearance()Lcom/pspdfkit/signatures/SignatureAppearance;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->a0:Lcom/pspdfkit/signatures/SignatureAppearance;

    return-object v0
.end method

.method public getSignatureCertificateSelectionMode()Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Z:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    return-object v0
.end method

.method public getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->X:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    return-object v0
.end method

.method public getSignatureCreationModes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Y:Ljava/util/List;

    return-object v0
.end method

.method public getSignaturePickerOrientation()Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->U:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    return-object v0
.end method

.method public getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->V:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    return-object v0
.end method

.method public getStartZoomScale()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->n:F

    return v0
.end method

.method public getThemeMode()Lcom/pspdfkit/configuration/theming/ThemeMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->e:Lcom/pspdfkit/configuration/theming/ThemeMode;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0xf4243

    xor-int/2addr v0, v1

    mul-int v0, v0, v1

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->b:Lcom/pspdfkit/configuration/page/PageScrollMode;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->c:Lcom/pspdfkit/configuration/page/PageFitMode;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 7
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->d:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->e:Lcom/pspdfkit/configuration/theming/ThemeMode;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 11
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->f:Z

    const/16 v3, 0x4cf

    const/16 v4, 0x4d5

    if-eqz v2, :cond_0

    const/16 v2, 0x4cf

    goto :goto_0

    :cond_0
    const/16 v2, 0x4d5

    :goto_0
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 13
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->g:Z

    if-eqz v2, :cond_1

    const/16 v2, 0x4cf

    goto :goto_1

    :cond_1
    const/16 v2, 0x4d5

    :goto_1
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 15
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->h:Z

    if-eqz v2, :cond_2

    const/16 v2, 0x4cf

    goto :goto_2

    :cond_2
    const/16 v2, 0x4d5

    :goto_2
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 17
    iget v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->i:I

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 19
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->j:Ljava/lang/Integer;

    const/4 v5, 0x0

    if-nez v2, :cond_3

    const/4 v2, 0x0

    goto :goto_3

    :cond_3
    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_3
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 21
    iget v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->k:I

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 23
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->l:Z

    if-eqz v2, :cond_4

    const/16 v2, 0x4cf

    goto :goto_4

    :cond_4
    const/16 v2, 0x4d5

    :goto_4
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 25
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->m:Z

    if-eqz v2, :cond_5

    const/16 v2, 0x4cf

    goto :goto_5

    :cond_5
    const/16 v2, 0x4d5

    :goto_5
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 27
    iget v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->n:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 29
    iget v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->o:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 31
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->p:Z

    if-eqz v2, :cond_6

    const/16 v2, 0x4cf

    goto :goto_6

    :cond_6
    const/16 v2, 0x4d5

    :goto_6
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 33
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->q:Z

    if-eqz v2, :cond_7

    const/16 v2, 0x4cf

    goto :goto_7

    :cond_7
    const/16 v2, 0x4d5

    :goto_7
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 35
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->r:Z

    if-eqz v2, :cond_8

    const/16 v2, 0x4cf

    goto :goto_8

    :cond_8
    const/16 v2, 0x4d5

    :goto_8
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 37
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->s:Z

    if-eqz v2, :cond_9

    const/16 v2, 0x4cf

    goto :goto_9

    :cond_9
    const/16 v2, 0x4d5

    :goto_9
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 39
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->t:Z

    if-eqz v2, :cond_a

    const/16 v2, 0x4cf

    goto :goto_a

    :cond_a
    const/16 v2, 0x4d5

    :goto_a
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 41
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->u:Z

    if-eqz v2, :cond_b

    const/16 v2, 0x4cf

    goto :goto_b

    :cond_b
    const/16 v2, 0x4d5

    :goto_b
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 43
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->v:Z

    if-eqz v2, :cond_c

    const/16 v2, 0x4cf

    goto :goto_c

    :cond_c
    const/16 v2, 0x4d5

    :goto_c
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 45
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->w:Z

    if-eqz v2, :cond_d

    const/16 v2, 0x4cf

    goto :goto_d

    :cond_d
    const/16 v2, 0x4d5

    :goto_d
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 47
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->x:Z

    if-eqz v2, :cond_e

    const/16 v2, 0x4cf

    goto :goto_e

    :cond_e
    const/16 v2, 0x4d5

    :goto_e
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 49
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->y:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 51
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->z:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 53
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->A:Z

    if-eqz v2, :cond_f

    const/16 v2, 0x4cf

    goto :goto_f

    :cond_f
    const/16 v2, 0x4d5

    :goto_f
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 55
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->B:Z

    if-eqz v2, :cond_10

    const/16 v2, 0x4cf

    goto :goto_10

    :cond_10
    const/16 v2, 0x4d5

    :goto_10
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 57
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->C:Z

    if-eqz v2, :cond_11

    const/16 v2, 0x4cf

    goto :goto_11

    :cond_11
    const/16 v2, 0x4d5

    :goto_11
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 59
    iget v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->D:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 61
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->E:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 63
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->F:Z

    if-eqz v2, :cond_12

    const/16 v2, 0x4cf

    goto :goto_12

    :cond_12
    const/16 v2, 0x4d5

    :goto_12
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 65
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->G:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 67
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->H:Z

    if-eqz v2, :cond_13

    const/16 v2, 0x4cf

    goto :goto_13

    :cond_13
    const/16 v2, 0x4d5

    :goto_13
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 69
    iget v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->I:I

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 71
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->J:Z

    if-eqz v2, :cond_14

    const/16 v2, 0x4cf

    goto :goto_14

    :cond_14
    const/16 v2, 0x4d5

    :goto_14
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 73
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->K:Z

    if-eqz v2, :cond_15

    const/16 v2, 0x4cf

    goto :goto_15

    :cond_15
    const/16 v2, 0x4d5

    :goto_15
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 75
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->L:Z

    if-eqz v2, :cond_16

    const/16 v2, 0x4cf

    goto :goto_16

    :cond_16
    const/16 v2, 0x4d5

    :goto_16
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 77
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->M:Z

    if-eqz v2, :cond_17

    const/16 v2, 0x4cf

    goto :goto_17

    :cond_17
    const/16 v2, 0x4d5

    :goto_17
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 79
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->N:Z

    if-eqz v2, :cond_18

    const/16 v2, 0x4cf

    goto :goto_18

    :cond_18
    const/16 v2, 0x4d5

    :goto_18
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 81
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->O:Ljava/util/EnumSet;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 83
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->P:Z

    if-eqz v2, :cond_19

    const/16 v2, 0x4cf

    goto :goto_19

    :cond_19
    const/16 v2, 0x4d5

    :goto_19
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 85
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Q:Z

    if-eqz v2, :cond_1a

    const/16 v2, 0x4cf

    goto :goto_1a

    :cond_1a
    const/16 v2, 0x4d5

    :goto_1a
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 87
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->R:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 89
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->S:Ljava/lang/Integer;

    if-nez v2, :cond_1b

    const/4 v2, 0x0

    goto :goto_1b

    :cond_1b
    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_1b
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 91
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->T:Z

    if-eqz v2, :cond_1c

    const/16 v2, 0x4cf

    goto :goto_1c

    :cond_1c
    const/16 v2, 0x4d5

    :goto_1c
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 93
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->U:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 95
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->V:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 97
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->W:Ljava/lang/String;

    if-nez v2, :cond_1d

    const/4 v2, 0x0

    goto :goto_1d

    :cond_1d
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1d
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 99
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->X:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 101
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Y:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 103
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Z:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 105
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->a0:Lcom/pspdfkit/signatures/SignatureAppearance;

    if-nez v2, :cond_1e

    goto :goto_1e

    :cond_1e
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v5

    :goto_1e
    xor-int/2addr v0, v5

    mul-int v0, v0, v1

    .line 107
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->b0:Z

    if-eqz v2, :cond_1f

    const/16 v2, 0x4cf

    goto :goto_1f

    :cond_1f
    const/16 v2, 0x4d5

    :goto_1f
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 109
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->c0:Z

    if-eqz v2, :cond_20

    const/16 v2, 0x4cf

    goto :goto_20

    :cond_20
    const/16 v2, 0x4d5

    :goto_20
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 111
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->d0:Z

    if-eqz v2, :cond_21

    const/16 v2, 0x4cf

    goto :goto_21

    :cond_21
    const/16 v2, 0x4d5

    :goto_21
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 113
    iget-object v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->e0:Ljava/util/EnumSet;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 115
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->f0:Z

    if-eqz v2, :cond_22

    const/16 v2, 0x4cf

    goto :goto_22

    :cond_22
    const/16 v2, 0x4d5

    :goto_22
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 117
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->g0:Z

    if-eqz v2, :cond_23

    const/16 v2, 0x4cf

    goto :goto_23

    :cond_23
    const/16 v2, 0x4d5

    :goto_23
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 119
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->h0:Z

    if-eqz v2, :cond_24

    const/16 v2, 0x4cf

    goto :goto_24

    :cond_24
    const/16 v2, 0x4d5

    :goto_24
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 121
    iget v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->i0:I

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 123
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->j0:Z

    if-eqz v2, :cond_25

    const/16 v2, 0x4cf

    goto :goto_25

    :cond_25
    const/16 v2, 0x4d5

    :goto_25
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 125
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->k0:Z

    if-eqz v2, :cond_26

    goto :goto_26

    :cond_26
    const/16 v3, 0x4d5

    :goto_26
    xor-int/2addr v0, v3

    mul-int v0, v0, v1

    .line 127
    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->l0:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isAnnotationEditingEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->t:Z

    return v0
.end method

.method public isAnnotationInspectorEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->F:Z

    return v0
.end method

.method public isAnnotationLimitedToPageBounds()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->x:Z

    return v0
.end method

.method public isAnnotationRotationEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->u:Z

    return v0
.end method

.method public isAutoSelectNextFormElementEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->s:Z

    return v0
.end method

.method public isAutomaticLinkGenerationEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->M:Z

    return v0
.end method

.method public isAutosaveEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->H:Z

    return v0
.end method

.method public isContentEditingEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->v:Z

    return v0
.end method

.method public isCopyPasteEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->N:Z

    return v0
.end method

.method public isFirstPageAlwaysSingle()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->f:Z

    return v0
.end method

.method public isFormEditingEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->r:Z

    return v0
.end method

.method public isInvertColors()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->l:Z

    return v0
.end method

.method public isJavaScriptEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->c0:Z

    return v0
.end method

.method public isLastViewedPageRestorationEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->L:Z

    return v0
.end method

.method public isMagnifierEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->j0:Z

    return v0
.end method

.method public isMeasurementsEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->w:Z

    return v0
.end method

.method public isMultithreadedRenderingEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->T:Z

    return v0
.end method

.method public isNoteAnnotationNoZoomHandlingEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->b0:Z

    return v0
.end method

.method public isPlayingMultipleMediaInstancesEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->K:Z

    return v0
.end method

.method public isRedoEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Q:Z

    return v0
.end method

.method public isScrollbarsEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->h:Z

    return v0
.end method

.method public isTextSelectionEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->q:Z

    return v0
.end method

.method public isTextSelectionPopupToolbarEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->d0:Z

    return v0
.end method

.method public isToGrayscale()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->m:Z

    return v0
.end method

.method public isUndoEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->P:Z

    return v0
.end method

.method public isVideoPlaybackEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->J:Z

    return v0
.end method

.method public scrollOnEdgeTapEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->g0:Z

    return v0
.end method

.method public scrollOnEdgeTapMargin()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->i0:I

    return v0
.end method

.method public shouldZoomOutBounce()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->p:Z

    return v0
.end method

.method public showGapBetweenPages()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->g:Z

    return v0
.end method

.method public showSignHereOverlay()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->k0:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PdfConfiguration{getScrollDirection="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getScrollMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->b:Lcom/pspdfkit/configuration/page/PageScrollMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getFitMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->c:Lcom/pspdfkit/configuration/page/PageFitMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getLayoutMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->d:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getThemeMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->e:Lcom/pspdfkit/configuration/theming/ThemeMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isFirstPageAlwaysSingle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showGapBetweenPages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isScrollbarsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->h:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getBackgroundColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", getLoadingProgressDrawable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->j:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getMemoryCacheSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isInvertColors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->l:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isToGrayscale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->m:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getStartZoomScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->n:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", getMaxZoomScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->o:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", shouldZoomOutBounce="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->p:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isTextSelectionEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->q:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isFormEditingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->r:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isAutoSelectNextFormElementEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->s:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isAnnotationEditingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->t:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isAnnotationRotationEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->u:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isContentEditingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->v:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isMeasurementsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->w:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isAnnotationLimitedToPageBounds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->x:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getEditableAnnotationTypes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->y:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getEnabledAnnotationTools="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->z:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getSelectedAnnotationResizeEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->A:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getSelectedAnnotationResizeGuidesEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->B:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getSelectedAnnotationFontScalingOnResizeEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->C:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getResizeGuideSnapAllowance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->D:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", getGuideLineIntervals="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->E:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isAnnotationInspectorEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->F:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getExcludedAnnotationTypes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isAutosaveEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->H:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getPagePadding="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->I:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isVideoPlaybackEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->J:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isPlayingMultipleMediaInstancesEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->K:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isLastViewedPageRestorationEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->L:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isAutomaticLinkGenerationEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->M:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isCopyPasteEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->N:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getEnabledCopyPasteFeatures="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->O:Ljava/util/EnumSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isUndoEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->P:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isRedoEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Q:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getAnnotationReplyFeatures="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->R:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getFixedLowResRenderPixelCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->S:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isMultithreadedRenderingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->T:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getSignaturePickerOrientation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->U:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getSignatureSavingStrategy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->V:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getDefaultSigner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->W:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", getSignatureColorOptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->X:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getSignatureCreationModes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Y:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getSignatureCertificateSelectionMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->Z:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getSignatureAppearance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->a0:Lcom/pspdfkit/signatures/SignatureAppearance;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isNoteAnnotationNoZoomHandlingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->b0:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isJavaScriptEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->c0:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isTextSelectionPopupToolbarEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->d0:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getEnabledShareFeatures="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->e0:Ljava/util/EnumSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allowMultipleBookmarksPerPage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->f0:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", scrollOnEdgeTapEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->g0:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", animateScrollOnEdgeTaps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->h0:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", scrollOnEdgeTapMargin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->i0:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isMagnifierEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->j0:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showSignHereOverlay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->k0:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getMinimumAnnotationSizeMap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->l0:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class final Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;
.super Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration$1;

    invoke-direct {v0}, Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration$1;-><init>()V

    sput-object v0, Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(FFFIIIILcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/page/PageFitMode;Lcom/pspdfkit/configuration/page/PageLayoutMode;Lcom/pspdfkit/configuration/page/PageScrollDirection;Lcom/pspdfkit/configuration/page/PageScrollMode;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/theming/ThemeMode;Lcom/pspdfkit/signatures/SignatureAppearance;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/EnumSet;Ljava/util/EnumSet;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Map;ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p64}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;-><init>(FFFIIIILcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/page/PageFitMode;Lcom/pspdfkit/configuration/page/PageLayoutMode;Lcom/pspdfkit/configuration/page/PageScrollDirection;Lcom/pspdfkit/configuration/page/PageScrollMode;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/theming/ThemeMode;Lcom/pspdfkit/signatures/SignatureAppearance;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/EnumSet;Ljava/util/EnumSet;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Map;ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getScrollDirection()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getScrollMode()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getFitMode()Lcom/pspdfkit/configuration/page/PageFitMode;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getLayoutMode()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getThemeMode()Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isFirstPageAlwaysSingle()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->showGapBetweenPages()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isScrollbarsEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getBackgroundColor()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 10
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getLoadingProgressDrawable()Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 11
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 13
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 14
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getLoadingProgressDrawable()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 16
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getMemoryCacheSize()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 17
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isInvertColors()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 18
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isToGrayscale()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 19
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getStartZoomScale()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 20
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getMaxZoomScale()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 21
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->shouldZoomOutBounce()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 22
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isTextSelectionEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 23
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isFormEditingEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 24
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isAutoSelectNextFormElementEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 25
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isAnnotationEditingEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 26
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isAnnotationRotationEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 27
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isContentEditingEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 28
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isMeasurementsEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 29
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isAnnotationLimitedToPageBounds()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 30
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getEditableAnnotationTypes()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 31
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getEnabledAnnotationTools()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 32
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getSelectedAnnotationResizeEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 33
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getSelectedAnnotationResizeGuidesEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 34
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getSelectedAnnotationFontScalingOnResizeEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 35
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getResizeGuideSnapAllowance()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 36
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getGuideLineIntervals()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 37
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isAnnotationInspectorEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 38
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getExcludedAnnotationTypes()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 39
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isAutosaveEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 40
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getPagePadding()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 41
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isVideoPlaybackEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 42
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isPlayingMultipleMediaInstancesEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 43
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isLastViewedPageRestorationEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 44
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isAutomaticLinkGenerationEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 45
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isCopyPasteEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 46
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getEnabledCopyPasteFeatures()Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 47
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isUndoEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 48
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isRedoEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getAnnotationReplyFeatures()Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getFixedLowResRenderPixelCount()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 51
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 53
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getFixedLowResRenderPixelCount()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 56
    :goto_1
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isMultithreadedRenderingEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 57
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getSignaturePickerOrientation()Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getDefaultSigner()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 60
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 62
    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 63
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getDefaultSigner()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 65
    :goto_2
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 66
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 67
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getSignatureCertificateSelectionMode()Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getSignatureAppearance()Lcom/pspdfkit/signatures/SignatureAppearance;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 69
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isNoteAnnotationNoZoomHandlingEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isJavaScriptEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 71
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isTextSelectionPopupToolbarEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 72
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->getEnabledShareFeatures()Ljava/util/EnumSet;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 73
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->allowMultipleBookmarksPerPage()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 74
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->scrollOnEdgeTapEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 75
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->animateScrollOnEdgeTaps()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 76
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->scrollOnEdgeTapMargin()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->isMagnifierEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 78
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->showSignHereOverlay()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 79
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/$AutoValue_PdfConfiguration;->a()Ljava/util/Map;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    return-void
.end method

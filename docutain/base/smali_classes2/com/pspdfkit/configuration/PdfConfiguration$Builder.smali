.class public final Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/configuration/PdfConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# static fields
.field private static final m0:[Ljava/lang/Float;


# instance fields
.field private A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            ">;"
        }
    .end annotation
.end field

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:F

.field private F:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private G:Z

.field private H:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private I:Z

.field private J:I

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Z

.field private P:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Z

.field private R:Z

.field private S:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

.field private T:Ljava/lang/Integer;

.field private U:Z

.field private V:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

.field private W:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

.field private X:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

.field private Y:Ljava/lang/String;

.field private Z:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

.field private a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

.field a0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/pspdfkit/configuration/page/PageFitMode;

.field private b0:Lcom/pspdfkit/signatures/SignatureAppearance;

.field private c:Lcom/pspdfkit/configuration/page/PageScrollMode;

.field private c0:Z

.field private d:Lcom/pspdfkit/configuration/page/PageLayoutMode;

.field private d0:Z

.field private e:Lcom/pspdfkit/configuration/theming/ThemeMode;

.field private e0:Z

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/utils/Size;",
            ">;"
        }
    .end annotation
.end field

.field private f0:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/sharing/ShareFeatures;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private g0:Z

.field private h:Z

.field private h0:Z

.field private i:Z

.field private i0:Z

.field private j:I

.field private j0:I

.field private k:Ljava/lang/Integer;

.field private k0:Z

.field private l:I

.field private l0:Z

.field private m:Z

.field private n:Z

.field private o:F

.field private p:F

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Float;

    const/high16 v1, 0x40a00000    # 5.0f

    .line 1
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->m0:[Ljava/lang/Float;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lcom/pspdfkit/configuration/page/PageScrollDirection;->HORIZONTAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    .line 6
    sget-object v0, Lcom/pspdfkit/configuration/page/PageFitMode;->FIT_TO_SCREEN:Lcom/pspdfkit/configuration/page/PageFitMode;

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->b:Lcom/pspdfkit/configuration/page/PageFitMode;

    .line 10
    sget-object v0, Lcom/pspdfkit/configuration/page/PageScrollMode;->PER_PAGE:Lcom/pspdfkit/configuration/page/PageScrollMode;

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->c:Lcom/pspdfkit/configuration/page/PageScrollMode;

    .line 14
    sget-object v0, Lcom/pspdfkit/configuration/page/PageLayoutMode;->AUTO:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->d:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    .line 18
    sget-object v0, Lcom/pspdfkit/configuration/theming/ThemeMode;->DEFAULT:Lcom/pspdfkit/configuration/theming/ThemeMode;

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->e:Lcom/pspdfkit/configuration/theming/ThemeMode;

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->f:Ljava/util/Map;

    const/4 v0, 0x0

    .line 27
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->g:Z

    .line 33
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->h:Z

    const/4 v1, 0x1

    .line 36
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->i:Z

    const/4 v2, -0x1

    .line 39
    iput v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->j:I

    .line 43
    sget-object v2, Lcom/pspdfkit/configuration/PdfConfiguration;->DEFAULT_LOADING_PROGRESS_DRAWABLE:Ljava/lang/Integer;

    iput-object v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->k:Ljava/lang/Integer;

    .line 51
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->m:Z

    .line 54
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->n:Z

    const/high16 v2, 0x3f800000    # 1.0f

    .line 57
    iput v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->o:F

    const/high16 v2, 0x41700000    # 15.0f

    .line 60
    iput v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->p:F

    .line 63
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->q:Z

    .line 66
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->r:Z

    .line 69
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->s:Z

    .line 72
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->t:Z

    .line 75
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->u:Z

    .line 78
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->v:Z

    .line 81
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->w:Z

    .line 84
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->x:Z

    .line 87
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->y:Z

    .line 90
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->z:Ljava/util/List;

    .line 94
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->A:Ljava/util/List;

    .line 98
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->B:Z

    .line 101
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->C:Z

    .line 104
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->D:Z

    const/high16 v2, 0x41f00000    # 30.0f

    .line 107
    iput v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->E:F

    .line 111
    sget-object v2, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->m0:[Ljava/lang/Float;

    .line 112
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->F:Ljava/util/List;

    .line 115
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->G:Z

    .line 118
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->H:Ljava/util/ArrayList;

    .line 122
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->I:Z

    const/16 v2, 0x10

    .line 125
    iput v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->J:I

    .line 128
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->K:Z

    .line 134
    invoke-static {}, Lcom/pspdfkit/internal/tq;->a()Z

    move-result v2

    iput-boolean v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->L:Z

    .line 141
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->M:Z

    .line 146
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->N:Z

    .line 149
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->O:Z

    .line 152
    invoke-static {}, Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;->allFeatures()Ljava/util/EnumSet;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->P:Ljava/util/EnumSet;

    .line 155
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->Q:Z

    .line 158
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->R:Z

    .line 164
    sget-object v2, Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;->ENABLED:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    iput-object v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->S:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    const/4 v2, 0x0

    .line 168
    iput-object v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->T:Ljava/lang/Integer;

    .line 171
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->U:Z

    .line 174
    sget-object v3, Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;->AUTOMATIC:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    iput-object v3, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->V:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    .line 184
    sget-object v3, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;->SAVE_IF_SELECTED:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    iput-object v3, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->W:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 188
    sget-object v3, Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;->IF_AVAILABLE:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    iput-object v3, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->X:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    .line 196
    iput-object v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->Y:Ljava/lang/String;

    .line 205
    invoke-static {}, Lcom/pspdfkit/configuration/signatures/SignatureColorOptions$-CC;->fromDefaults()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->Z:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    const/4 v2, 0x3

    new-array v2, v2, [Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    .line 214
    sget-object v3, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;->DRAW:Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    aput-object v3, v2, v0

    sget-object v3, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;->IMAGE:Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    aput-object v3, v2, v1

    sget-object v3, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;->TYPE:Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    const/4 v4, 0x2

    aput-object v3, v2, v4

    .line 217
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->a0:Ljava/util/List;

    .line 228
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->c0:Z

    .line 231
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->d0:Z

    .line 234
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->e0:Z

    .line 236
    invoke-static {}, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->all()Ljava/util/EnumSet;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->f0:Ljava/util/EnumSet;

    .line 238
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->g0:Z

    .line 239
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->h0:Z

    .line 240
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->i0:Z

    const/16 v0, 0x18

    .line 241
    iput v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->j0:I

    .line 248
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->k0:Z

    .line 251
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->l0:Z

    .line 255
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    long-to-int v1, v0

    .line 256
    div-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->l:I

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 1

    .line 257
    invoke-direct {p0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;-><init>()V

    .line 258
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getScrollDirection()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    .line 259
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getScrollMode()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->c:Lcom/pspdfkit/configuration/page/PageScrollMode;

    .line 260
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getFitMode()Lcom/pspdfkit/configuration/page/PageFitMode;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->b:Lcom/pspdfkit/configuration/page/PageFitMode;

    .line 261
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getLayoutMode()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->d:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    .line 262
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getThemeMode()Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->e:Lcom/pspdfkit/configuration/theming/ThemeMode;

    .line 263
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isFirstPageAlwaysSingle()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->g:Z

    .line 264
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->showGapBetweenPages()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->h:Z

    .line 265
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isScrollbarsEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->i:Z

    .line 266
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getBackgroundColor()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->j:I

    .line 267
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getLoadingProgressDrawable()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->k:Ljava/lang/Integer;

    .line 268
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->m:Z

    .line 269
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->n:Z

    .line 270
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAutosaveEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->I:Z

    .line 271
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isTextSelectionEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->r:Z

    .line 272
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isFormEditingEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->s:Z

    .line 273
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAutoSelectNextFormElementEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->t:Z

    .line 274
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationEditingEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->u:Z

    .line 275
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationRotationEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->v:Z

    .line 276
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isContentEditingEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->w:Z

    .line 277
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isMeasurementsEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->x:Z

    .line 278
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationLimitedToPageBounds()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->y:Z

    .line 279
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEditableAnnotationTypes()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->z:Ljava/util/List;

    .line 280
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledAnnotationTools()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->A:Ljava/util/List;

    .line 281
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSelectedAnnotationResizeEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->B:Z

    .line 282
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSelectedAnnotationResizeGuidesEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->C:Z

    .line 284
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSelectedAnnotationFontScalingOnResizeEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->D:Z

    .line 285
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getResizeGuideSnapAllowance()F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->E:F

    .line 286
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getGuideLineIntervals()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->F:Ljava/util/List;

    .line 287
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationInspectorEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->G:Z

    .line 288
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getExcludedAnnotationTypes()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->H:Ljava/util/ArrayList;

    .line 289
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getPagePadding()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->J:I

    .line 290
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isLastViewedPageRestorationEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->M:Z

    .line 291
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMemoryCacheSize()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->l:I

    .line 292
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getStartZoomScale()F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->o:F

    .line 293
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMaxZoomScale()F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->p:F

    .line 294
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->shouldZoomOutBounce()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->q:Z

    .line 295
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isVideoPlaybackEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->L:Z

    .line 296
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAutomaticLinkGenerationEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->N:Z

    .line 297
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isCopyPasteEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->O:Z

    .line 298
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isUndoEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->Q:Z

    .line 299
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isRedoEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->R:Z

    .line 300
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignaturePickerOrientation()Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->V:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    .line 301
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->W:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 302
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getDefaultSigner()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->Y:Ljava/lang/String;

    .line 303
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->Z:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    .line 304
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->a0:Ljava/util/List;

    .line 305
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureCertificateSelectionMode()Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->X:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    .line 306
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureAppearance()Lcom/pspdfkit/signatures/SignatureAppearance;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->b0:Lcom/pspdfkit/signatures/SignatureAppearance;

    .line 307
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getFixedLowResRenderPixelCount()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->T:Ljava/lang/Integer;

    .line 308
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isMultithreadedRenderingEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->U:Z

    .line 309
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledCopyPasteFeatures()Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->P:Ljava/util/EnumSet;

    .line 310
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isNoteAnnotationNoZoomHandlingEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->c0:Z

    .line 311
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getAnnotationReplyFeatures()Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->S:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    .line 312
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isJavaScriptEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->d0:Z

    .line 313
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isTextSelectionPopupToolbarEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->e0:Z

    .line 314
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledShareFeatures()Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->f0:Ljava/util/EnumSet;

    .line 315
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->allowMultipleBookmarksPerPage()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->g0:Z

    .line 316
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->scrollOnEdgeTapEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->h0:Z

    .line 317
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->animateScrollOnEdgeTaps()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->i0:Z

    .line 318
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->scrollOnEdgeTapMargin()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->j0:I

    .line 319
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isMagnifierEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->k0:Z

    .line 320
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->a()Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->f:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public allowMultipleBookmarksPerPage(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->g0:Z

    return-object p0
.end method

.method public animateScrollOnEdgeTaps(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->i0:Z

    return-object p0
.end method

.method public annotationReplyFeatures(Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2

    const-string v0, "annotationReplyFeatures"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->S:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    return-object p0
.end method

.method public automaticallyGenerateLinks(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->N:Z

    return-object p0
.end method

.method public autosaveEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->I:Z

    return-object p0
.end method

.method public backgroundColor(I)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->j:I

    return-object p0
.end method

.method public build()Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 67

    move-object/from16 v0, p0

    .line 1
    iget-object v1, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->A:Ljava/util/List;

    .line 2
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3
    const-class v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-static {v2}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    .line 4
    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INSTANT_COMMENT_MARKER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-virtual {v2, v3}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 5
    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INSTANT_HIGHLIGHT_COMMENT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-virtual {v2, v3}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 6
    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 8
    :cond_0
    iget-object v13, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    iget-object v14, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->c:Lcom/pspdfkit/configuration/page/PageScrollMode;

    iget-object v11, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->b:Lcom/pspdfkit/configuration/page/PageFitMode;

    iget-object v12, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->d:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    iget-object v2, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->e:Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-object/from16 v18, v2

    iget-boolean v2, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->g:Z

    move/from16 v31, v2

    iget-boolean v2, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->h:Z

    move/from16 v32, v2

    iget-boolean v2, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->i:Z

    move/from16 v33, v2

    iget v5, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->j:I

    iget-object v2, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->k:Ljava/lang/Integer;

    move-object/from16 v20, v2

    iget v6, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->l:I

    iget-boolean v2, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->m:Z

    move/from16 v34, v2

    iget-boolean v2, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->n:Z

    move/from16 v35, v2

    iget v2, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->o:F

    iget v3, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->p:F

    iget-boolean v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->q:Z

    move/from16 v36, v4

    iget-boolean v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->r:Z

    move/from16 v37, v4

    iget-boolean v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->e0:Z

    move/from16 v60, v4

    iget-boolean v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->s:Z

    move/from16 v38, v4

    iget-boolean v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->t:Z

    move/from16 v39, v4

    iget-boolean v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->u:Z

    move/from16 v40, v4

    iget-boolean v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->v:Z

    move/from16 v41, v4

    iget-boolean v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->w:Z

    move/from16 v42, v4

    iget-boolean v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->x:Z

    move/from16 v43, v4

    iget-boolean v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->y:Z

    move/from16 v44, v4

    iget-object v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->z:Ljava/util/List;

    move-object/from16 v26, v4

    iget-boolean v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->B:Z

    move/from16 v45, v4

    iget-boolean v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->C:Z

    move/from16 v46, v4

    iget-boolean v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->D:Z

    move/from16 v47, v4

    iget v4, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->E:F

    iget-object v7, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->F:Ljava/util/List;

    move-object/from16 v28, v7

    iget-boolean v7, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->G:Z

    move/from16 v48, v7

    iget-object v7, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->H:Ljava/util/ArrayList;

    move-object/from16 v23, v7

    iget-boolean v7, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->I:Z

    move/from16 v49, v7

    iget v7, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->J:I

    iget-boolean v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->L:Z

    move/from16 v50, v8

    iget-boolean v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->K:Z

    move/from16 v51, v8

    iget-boolean v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->M:Z

    move/from16 v52, v8

    iget-boolean v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->N:Z

    move/from16 v53, v8

    iget-boolean v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->O:Z

    move/from16 v54, v8

    iget-object v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->P:Ljava/util/EnumSet;

    move-object/from16 v24, v8

    iget-boolean v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->Q:Z

    move/from16 v55, v8

    iget-boolean v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->R:Z

    move/from16 v56, v8

    iget-object v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->T:Ljava/lang/Integer;

    move-object/from16 v21, v8

    iget-boolean v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->U:Z

    move/from16 v57, v8

    iget-object v10, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->V:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    iget-object v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->W:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-object/from16 v17, v8

    iget-object v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->Y:Ljava/lang/String;

    move-object/from16 v22, v8

    iget-object v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->Z:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-object/from16 v16, v8

    iget-object v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->a0:Ljava/util/List;

    move-object/from16 v29, v8

    iget-object v15, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->X:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    iget-object v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->b0:Lcom/pspdfkit/signatures/SignatureAppearance;

    move-object/from16 v19, v8

    iget-boolean v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->c0:Z

    move/from16 v58, v8

    iget-object v9, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->S:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    iget-boolean v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->d0:Z

    move/from16 v59, v8

    iget-object v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->f0:Ljava/util/EnumSet;

    move-object/from16 v25, v8

    iget-boolean v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->g0:Z

    move/from16 v61, v8

    iget-boolean v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->h0:Z

    move/from16 v62, v8

    iget-boolean v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->i0:Z

    move/from16 v63, v8

    iget v8, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->j0:I

    move-object/from16 v27, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->k0:Z

    move/from16 v64, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->l0:Z

    move/from16 v65, v1

    iget-object v1, v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->f:Ljava/util/Map;

    move-object/from16 v30, v1

    sget-object v1, Lcom/pspdfkit/configuration/PdfConfiguration;->DEFAULT_LOADING_PROGRESS_DRAWABLE:Ljava/lang/Integer;

    .line 9
    new-instance v66, Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;

    move-object/from16 v1, v66

    invoke-direct/range {v1 .. v65}, Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;-><init>(FFFIIIILcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/page/PageFitMode;Lcom/pspdfkit/configuration/page/PageLayoutMode;Lcom/pspdfkit/configuration/page/PageScrollDirection;Lcom/pspdfkit/configuration/page/PageScrollMode;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/theming/ThemeMode;Lcom/pspdfkit/signatures/SignatureAppearance;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/EnumSet;Ljava/util/EnumSet;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Map;ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ)V

    return-object v66
.end method

.method public defaultSigner(Ljava/lang/String;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->Y:Ljava/lang/String;

    return-object p0
.end method

.method public disableAnnotationEditing()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->u:Z

    return-object p0
.end method

.method public disableAnnotationLimitedToPageBounds()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->y:Z

    return-object p0
.end method

.method public disableAnnotationRotation()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->v:Z

    return-object p0
.end method

.method public disableAutoSelectNextFormElement()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->t:Z

    return-object p0
.end method

.method public disableContentEditing()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->w:Z

    return-object p0
.end method

.method public disableCopyPaste()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->O:Z

    return-object p0
.end method

.method public disableFormEditing()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->s:Z

    return-object p0
.end method

.method public editableAnnotationTypes(Ljava/util/List;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)",
            "Lcom/pspdfkit/configuration/PdfConfiguration$Builder;"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->z:Ljava/util/List;

    goto :goto_0

    .line 3
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->z:Ljava/util/List;

    :goto_0
    return-object p0
.end method

.method public enableAnnotationEditing()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->u:Z

    return-object p0
.end method

.method public enableAnnotationRotation()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->v:Z

    return-object p0
.end method

.method public enableAutoSelectNextFormElement()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->t:Z

    return-object p0
.end method

.method public enableContentEditing()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->w:Z

    return-object p0
.end method

.method public enableCopyPaste()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->O:Z

    return-object p0
.end method

.method public enableFormEditing()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->s:Z

    return-object p0
.end method

.method public enableMagnifier(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->k0:Z

    return-object p0
.end method

.method public enabledAnnotationTools(Ljava/util/List;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            ">;)",
            "Lcom/pspdfkit/configuration/PdfConfiguration$Builder;"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->A:Ljava/util/List;

    goto :goto_0

    .line 3
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->A:Ljava/util/List;

    :goto_0
    return-object p0
.end method

.method public excludedAnnotationTypes(Ljava/util/List;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)",
            "Lcom/pspdfkit/configuration/PdfConfiguration$Builder;"
        }
    .end annotation

    const-string v0, "excludedAnnotationTypes"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->H:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->H:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public firstPageAlwaysSingle(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->g:Z

    return-object p0
.end method

.method public fitMode(Lcom/pspdfkit/configuration/page/PageFitMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2

    const-string v0, "mode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->b:Lcom/pspdfkit/configuration/page/PageFitMode;

    return-object p0
.end method

.method public invertColors(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->m:Z

    return-object p0
.end method

.method public layoutMode(Lcom/pspdfkit/configuration/page/PageLayoutMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2

    const-string v0, "mode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->d:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    return-object p0
.end method

.method public loadingProgressDrawable(Ljava/lang/Integer;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->k:Ljava/lang/Integer;

    return-object p0
.end method

.method public maxZoomScale(F)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 1

    const/high16 v0, 0x41a00000    # 20.0f

    .line 1
    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result p1

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    .line 2
    iput p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->p:F

    return-object p0
.end method

.method public memoryCacheSize(I)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->l:I

    return-object p0
.end method

.method public pagePadding(I)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->J:I

    return-object p0
.end method

.method public playingMultipleMediaInstancesEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->K:Z

    return-object p0
.end method

.method public redoEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    if-eqz p1, :cond_0

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->Q:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->R:Z

    return-object p0
.end method

.method public restoreLastViewedPage(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->M:Z

    return-object p0
.end method

.method public scrollDirection(Lcom/pspdfkit/configuration/page/PageScrollDirection;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2

    const-string v0, "orientation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    return-object p0
.end method

.method public scrollMode(Lcom/pspdfkit/configuration/page/PageScrollMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2

    const-string v0, "mode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->c:Lcom/pspdfkit/configuration/page/PageScrollMode;

    return-object p0
.end method

.method public scrollOnEdgeTapEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->h0:Z

    return-object p0
.end method

.method public scrollOnEdgeTapMargin(I)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2

    if-lez p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "marginDp needs to be at least 1."

    .line 1
    invoke-static {v1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    .line 2
    iput p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->j0:I

    return-object p0
.end method

.method public scrollbarsEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->i:Z

    return-object p0
.end method

.method public setAnnotationInspectorEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->G:Z

    return-object p0
.end method

.method public setEnableNoteAnnotationNoZoomHandling(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->c0:Z

    return-object p0
.end method

.method public setEnabledCopyPasteFeatures(Ljava/util/EnumSet;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;",
            ">;)",
            "Lcom/pspdfkit/configuration/PdfConfiguration$Builder;"
        }
    .end annotation

    const-string v0, "enabledFeatures"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->P:Ljava/util/EnumSet;

    return-object p0
.end method

.method public setEnabledShareFeatures(Ljava/util/EnumSet;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/sharing/ShareFeatures;",
            ">;)",
            "Lcom/pspdfkit/configuration/PdfConfiguration$Builder;"
        }
    .end annotation

    const-string v0, "enabledShareFeatures"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-static {p1}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->f0:Ljava/util/EnumSet;

    return-object p0
.end method

.method public setFixedLowResRenderPixelCount(Ljava/lang/Integer;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->T:Ljava/lang/Integer;

    return-object p0
.end method

.method public setJavaScriptEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->d0:Z

    return-object p0
.end method

.method public setMeasurementToolsEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->x:Z

    return-object p0
.end method

.method public setMinimumAnnotationSize(Ljava/lang/Class;Lcom/pspdfkit/utils/Size;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/pspdfkit/annotations/ResizableAnnotation;",
            ">;",
            "Lcom/pspdfkit/utils/Size;",
            ")",
            "Lcom/pspdfkit/configuration/PdfConfiguration$Builder;"
        }
    .end annotation

    const-string v0, "size"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->f:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setMultithreadedRenderingEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->U:Z

    return-object p0
.end method

.method public setResizeGuideLineIntervals(Ljava/util/List;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;)",
            "Lcom/pspdfkit/configuration/PdfConfiguration$Builder;"
        }
    .end annotation

    const-string v0, "intervals"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    rem-int/2addr v0, v1

    if-nez v0, :cond_0

    .line 58
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->F:Ljava/util/List;

    return-object p0

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "intervals must contain at least 2 elements and an even number. Found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 60
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setResizeGuideSnapAllowance(F)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->E:F

    return-object p0
.end method

.method public setSelectedAnnotationFontScalingOnResizeEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->D:Z

    return-object p0
.end method

.method public setSelectedAnnotationResizeEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->B:Z

    return-object p0
.end method

.method public setSelectedAnnotationResizeGuidesEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->C:Z

    return-object p0
.end method

.method public setSignaturePickerOrientation(Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2

    const-string v0, "orientation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->V:Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    return-object p0
.end method

.method public showGapBetweenPages(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->h:Z

    return-object p0
.end method

.method public showSignHereOverlay(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->l0:Z

    return-object p0
.end method

.method public signatureAppearance(Lcom/pspdfkit/signatures/SignatureAppearance;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->b0:Lcom/pspdfkit/signatures/SignatureAppearance;

    return-object p0
.end method

.method public signatureCertificateSelectionMode(Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2

    const-string v0, "signatureCertificateSelectionMode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->X:Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    return-object p0
.end method

.method public signatureColorOptions(Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2

    const-string v0, "signatureColorOptions"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->Z:Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    return-object p0
.end method

.method public signatureCreationModes(Ljava/util/List;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;",
            ">;)",
            "Lcom/pspdfkit/configuration/PdfConfiguration$Builder;"
        }
    .end annotation

    const-string v0, "signatureCreationModes"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "signatureCreationModes must contain no null items."

    .line 54
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Ljava/util/Collection;)V

    .line 56
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_1

    .line 60
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 61
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 65
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->a0:Ljava/util/List;

    return-object p0

    .line 66
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "`signatureCreationModes` must not have duplicates."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 67
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "`signatureCreationModes` must have 1 to 3 elements. Found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 68
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public signatureSavingStrategy(Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2

    const-string v0, "signatureSavingStrategy"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->W:Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    return-object p0
.end method

.method public startZoomScale(F)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->o:F

    return-object p0
.end method

.method public textSelectionEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->r:Z

    return-object p0
.end method

.method public textSelectionPopupToolbarEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->e0:Z

    return-object p0
.end method

.method public themeMode(Lcom/pspdfkit/configuration/theming/ThemeMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 2

    const-string v0, "mode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->e:Lcom/pspdfkit/configuration/theming/ThemeMode;

    return-object p0
.end method

.method public toGrayscale(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->n:Z

    return-object p0
.end method

.method public undoEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->Q:Z

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 4
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->R:Z

    :cond_0
    return-object p0
.end method

.method public videoPlaybackEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->L:Z

    return-object p0
.end method

.method public zoomOutBounce(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->q:Z

    return-object p0
.end method

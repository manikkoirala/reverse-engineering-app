.class public final Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0010\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008M\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u00a2\u0006\u0004\u0008h\u0010iB\u0011\u0008\u0016\u0012\u0006\u0010j\u001a\u00020\u001f\u00a2\u0006\u0004\u0008h\u0010kJ\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002J\u000e\u0010\u0006\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0002J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0002J\u000e\u0010\u0008\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0002J\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0002J\u000e\u0010\u000b\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002J\u000e\u0010\u000c\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0002J\u000e\u0010\u000e\u001a\u00020\u00002\u0006\u0010\r\u001a\u00020\u0002J\u000e\u0010\u000f\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0002J\u000e\u0010\u0010\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0002J\u000e\u0010\u0011\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0002J\u000e\u0010\u0012\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0002J\u0010\u0010\u0015\u001a\u00020\u00002\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0013J\u0010\u0010\u0016\u001a\u00020\u00002\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0013J\u0010\u0010\u0017\u001a\u00020\u00002\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0013J\u0010\u0010\u0018\u001a\u00020\u00002\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0013J\u0010\u0010\u0019\u001a\u00020\u00002\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0013J\u0010\u0010\u001a\u001a\u00020\u00002\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0013J\u0010\u0010\u001b\u001a\u00020\u00002\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0013J\u0010\u0010\u001c\u001a\u00020\u00002\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0013J\u0010\u0010\u001d\u001a\u00020\u00002\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0013J\u0010\u0010\u001e\u001a\u00020\u00002\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0013J\u0006\u0010 \u001a\u00020\u001fR(\u0010&\u001a\u0004\u0018\u00010\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@BX\u0086\u000e\u00a2\u0006\u000c\n\u0004\u0008\"\u0010#\u001a\u0004\u0008$\u0010%R(\u0010)\u001a\u0004\u0018\u00010\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@BX\u0087\u000e\u00a2\u0006\u000c\n\u0004\u0008\'\u0010#\u001a\u0004\u0008(\u0010%R(\u0010,\u001a\u0004\u0018\u00010\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@BX\u0087\u000e\u00a2\u0006\u000c\n\u0004\u0008*\u0010#\u001a\u0004\u0008+\u0010%R(\u0010/\u001a\u0004\u0018\u00010\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@BX\u0087\u000e\u00a2\u0006\u000c\n\u0004\u0008-\u0010#\u001a\u0004\u0008.\u0010%R(\u00102\u001a\u0004\u0018\u00010\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@BX\u0086\u000e\u00a2\u0006\u000c\n\u0004\u00080\u0010#\u001a\u0004\u00081\u0010%R(\u00105\u001a\u0004\u0018\u00010\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@BX\u0087\u000e\u00a2\u0006\u000c\n\u0004\u00083\u0010#\u001a\u0004\u00084\u0010%R(\u00108\u001a\u0004\u0018\u00010\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@BX\u0087\u000e\u00a2\u0006\u000c\n\u0004\u00086\u0010#\u001a\u0004\u00087\u0010%R(\u0010;\u001a\u0004\u0018\u00010\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@BX\u0087\u000e\u00a2\u0006\u000c\n\u0004\u00089\u0010#\u001a\u0004\u0008:\u0010%R(\u0010>\u001a\u0004\u0018\u00010\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@BX\u0087\u000e\u00a2\u0006\u000c\n\u0004\u0008<\u0010#\u001a\u0004\u0008=\u0010%R(\u0010A\u001a\u0004\u0018\u00010\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@BX\u0087\u000e\u00a2\u0006\u000c\n\u0004\u0008?\u0010#\u001a\u0004\u0008@\u0010%R(\u0010D\u001a\u0004\u0018\u00010\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@BX\u0087\u000e\u00a2\u0006\u000c\n\u0004\u0008B\u0010#\u001a\u0004\u0008C\u0010%R(\u0010G\u001a\u0004\u0018\u00010\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@BX\u0087\u000e\u00a2\u0006\u000c\n\u0004\u0008E\u0010#\u001a\u0004\u0008F\u0010%R(\u0010L\u001a\u0004\u0018\u00010\u00132\u0008\u0010!\u001a\u0004\u0018\u00010\u00138\u0006@BX\u0086\u000e\u00a2\u0006\u000c\n\u0004\u0008H\u0010I\u001a\u0004\u0008J\u0010KR(\u0010O\u001a\u0004\u0018\u00010\u00132\u0008\u0010!\u001a\u0004\u0018\u00010\u00138\u0006@BX\u0086\u000e\u00a2\u0006\u000c\n\u0004\u0008M\u0010I\u001a\u0004\u0008N\u0010KR(\u0010R\u001a\u0004\u0018\u00010\u00132\u0008\u0010!\u001a\u0004\u0018\u00010\u00138\u0006@BX\u0086\u000e\u00a2\u0006\u000c\n\u0004\u0008P\u0010I\u001a\u0004\u0008Q\u0010KR(\u0010U\u001a\u0004\u0018\u00010\u00132\u0008\u0010!\u001a\u0004\u0018\u00010\u00138\u0006@BX\u0086\u000e\u00a2\u0006\u000c\n\u0004\u0008S\u0010I\u001a\u0004\u0008T\u0010KR(\u0010X\u001a\u0004\u0018\u00010\u00132\u0008\u0010!\u001a\u0004\u0018\u00010\u00138\u0006@BX\u0086\u000e\u00a2\u0006\u000c\n\u0004\u0008V\u0010I\u001a\u0004\u0008W\u0010KR(\u0010[\u001a\u0004\u0018\u00010\u00132\u0008\u0010!\u001a\u0004\u0018\u00010\u00138\u0006@BX\u0086\u000e\u00a2\u0006\u000c\n\u0004\u0008Y\u0010I\u001a\u0004\u0008Z\u0010KR(\u0010^\u001a\u0004\u0018\u00010\u00132\u0008\u0010!\u001a\u0004\u0018\u00010\u00138\u0006@BX\u0086\u000e\u00a2\u0006\u000c\n\u0004\u0008\\\u0010I\u001a\u0004\u0008]\u0010KR(\u0010a\u001a\u0004\u0018\u00010\u00132\u0008\u0010!\u001a\u0004\u0018\u00010\u00138\u0006@BX\u0086\u000e\u00a2\u0006\u000c\n\u0004\u0008_\u0010I\u001a\u0004\u0008`\u0010KR(\u0010d\u001a\u0004\u0018\u00010\u00132\u0008\u0010!\u001a\u0004\u0018\u00010\u00138\u0006@BX\u0086\u000e\u00a2\u0006\u000c\n\u0004\u0008b\u0010I\u001a\u0004\u0008c\u0010KR(\u0010g\u001a\u0004\u0018\u00010\u00132\u0008\u0010!\u001a\u0004\u0018\u00010\u00138\u0006@BX\u0086\u000e\u00a2\u0006\u000c\n\u0004\u0008e\u0010I\u001a\u0004\u0008f\u0010K\u00a8\u0006l"
    }
    d2 = {
        "Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;",
        "",
        "",
        "width",
        "setSelectionBorderWidth",
        "color",
        "setSelectionBorderColor",
        "setSelectionScaleHandleColor",
        "setSelectionEditHandleColor",
        "padding",
        "setSelectionPadding",
        "setGuideLineWidth",
        "setGuideLineColor",
        "increase",
        "setGuideLineIncrease",
        "setLinkAnnotationBackgroundColor",
        "setLinkAnnotationBorderColor",
        "setLinkAnnotationHighlightBackgroundColor",
        "setLinkAnnotationHighlightBorderColor",
        "Landroid/graphics/drawable/Drawable;",
        "drawable",
        "setTopLeftScaleHandleDrawable",
        "setTopCenterScaleHandleDrawable",
        "setTopRightScaleHandleDrawable",
        "setCenterLeftScaleHandleDrawable",
        "setCenterRightScaleHandleDrawable",
        "setBottomLeftScaleHandleDrawable",
        "setBottomCenterScaleHandleDrawable",
        "setBottomRightScaleHandleDrawable",
        "setRotationHandleDrawable",
        "setBackgroundDrawable",
        "Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;",
        "build",
        "<set-?>",
        "a",
        "Ljava/lang/Integer;",
        "getSelectionBorderWidth",
        "()Ljava/lang/Integer;",
        "selectionBorderWidth",
        "b",
        "getSelectionBorderColor",
        "selectionBorderColor",
        "c",
        "getSelectionScaleHandleColor",
        "selectionScaleHandleColor",
        "d",
        "getSelectionEditHandleColor",
        "selectionEditHandleColor",
        "e",
        "getSelectionPadding",
        "selectionPadding",
        "f",
        "getGuideLineWidth",
        "guideLineWidth",
        "g",
        "getGuideLineColor",
        "guideLineColor",
        "h",
        "getGuideLineIncrease",
        "guideLineIncrease",
        "i",
        "getLinkAnnotationBackgroundColor",
        "linkAnnotationBackgroundColor",
        "j",
        "getLinkAnnotationBorderColor",
        "linkAnnotationBorderColor",
        "k",
        "getLinkAnnotationHighlightBackgroundColor",
        "linkAnnotationHighlightBackgroundColor",
        "l",
        "getLinkAnnotationHighlightBorderColor",
        "linkAnnotationHighlightBorderColor",
        "m",
        "Landroid/graphics/drawable/Drawable;",
        "getTopLeftScaleHandleDrawable",
        "()Landroid/graphics/drawable/Drawable;",
        "topLeftScaleHandleDrawable",
        "n",
        "getTopCenterScaleHandleDrawable",
        "topCenterScaleHandleDrawable",
        "o",
        "getTopRightScaleHandleDrawable",
        "topRightScaleHandleDrawable",
        "p",
        "getCenterLeftScaleHandleDrawable",
        "centerLeftScaleHandleDrawable",
        "q",
        "getCenterRightScaleHandleDrawable",
        "centerRightScaleHandleDrawable",
        "r",
        "getBottomLeftScaleHandleDrawable",
        "bottomLeftScaleHandleDrawable",
        "s",
        "getBottomCenterScaleHandleDrawable",
        "bottomCenterScaleHandleDrawable",
        "t",
        "getBottomRightScaleHandleDrawable",
        "bottomRightScaleHandleDrawable",
        "u",
        "getRotationHandleDrawable",
        "rotationHandleDrawable",
        "v",
        "getBackgroundDrawable",
        "backgroundDrawable",
        "<init>",
        "()V",
        "configuration",
        "(Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;)V",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# static fields
.field public static final $stable:I = 0x8


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/Integer;

.field private l:Ljava/lang/Integer;

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:Landroid/graphics/drawable/Drawable;

.field private p:Landroid/graphics/drawable/Drawable;

.field private q:Landroid/graphics/drawable/Drawable;

.field private r:Landroid/graphics/drawable/Drawable;

.field private s:Landroid/graphics/drawable/Drawable;

.field private t:Landroid/graphics/drawable/Drawable;

.field private u:Landroid/graphics/drawable/Drawable;

.field private v:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;)V
    .locals 1

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;-><init>()V

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionBorderWidth()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->a:Ljava/lang/Integer;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionBorderColor()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->b:Ljava/lang/Integer;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionScaleHandleColor()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->c:Ljava/lang/Integer;

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionEditHandleColor()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->d:Ljava/lang/Integer;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionPadding()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->e:Ljava/lang/Integer;

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getGuideLineWidth()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->f:Ljava/lang/Integer;

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getGuideLineColor()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->g:Ljava/lang/Integer;

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getGuideLineIncrease()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->h:Ljava/lang/Integer;

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getLinkAnnotationBackgroundColor()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->i:Ljava/lang/Integer;

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getLinkAnnotationBorderColor()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->j:Ljava/lang/Integer;

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getLinkAnnotationHighlightBackgroundColor()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->k:Ljava/lang/Integer;

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getLinkAnnotationHighlightBorderColor()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->l:Ljava/lang/Integer;

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getTopLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->m:Landroid/graphics/drawable/Drawable;

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getTopCenterScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->n:Landroid/graphics/drawable/Drawable;

    .line 17
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getTopRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->o:Landroid/graphics/drawable/Drawable;

    .line 18
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getCenterLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->p:Landroid/graphics/drawable/Drawable;

    .line 19
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getCenterRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->q:Landroid/graphics/drawable/Drawable;

    .line 20
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getBottomLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->r:Landroid/graphics/drawable/Drawable;

    .line 21
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getBottomCenterScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->s:Landroid/graphics/drawable/Drawable;

    .line 22
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getBottomRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->t:Landroid/graphics/drawable/Drawable;

    .line 23
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getRotationHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->u:Landroid/graphics/drawable/Drawable;

    .line 24
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getBackgroundDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->v:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public final build()Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;-><init>(Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final getBackgroundDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->v:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getBottomCenterScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->s:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getBottomLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->r:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getBottomRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->t:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getCenterLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->p:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getCenterRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->q:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getGuideLineColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->g:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getGuideLineIncrease()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->h:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getGuideLineWidth()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->f:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getLinkAnnotationBackgroundColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->i:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getLinkAnnotationBorderColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->j:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getLinkAnnotationHighlightBackgroundColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->k:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getLinkAnnotationHighlightBorderColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->l:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getRotationHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->u:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getSelectionBorderColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getSelectionBorderWidth()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->a:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getSelectionEditHandleColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->d:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getSelectionPadding()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->e:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getSelectionScaleHandleColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->c:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getTopCenterScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->n:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getTopLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->m:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getTopRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->o:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->v:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public final setBottomCenterScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->s:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public final setBottomLeftScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->r:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public final setBottomRightScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->t:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public final setCenterLeftScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->p:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public final setCenterRightScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->q:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public final setGuideLineColor(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->g:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setGuideLineIncrease(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->h:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setGuideLineWidth(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->f:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setLinkAnnotationBackgroundColor(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->i:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setLinkAnnotationBorderColor(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->j:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setLinkAnnotationHighlightBackgroundColor(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->k:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setLinkAnnotationHighlightBorderColor(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->l:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setRotationHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->u:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public final setSelectionBorderColor(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->b:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setSelectionBorderWidth(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->a:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setSelectionEditHandleColor(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->d:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setSelectionPadding(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->e:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setSelectionScaleHandleColor(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->c:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setTopCenterScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->n:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public final setTopLeftScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->m:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public final setTopRightScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->o:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.class public final Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0008\n\u0002\u0008&\n\u0002\u0018\u0002\n\u0002\u0008$\u0008\u0007\u0018\u00002\u00020\u0001:\u0001LB\u00f7\u0001\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0002\u0012\n\u0008\u0001\u0010\n\u001a\u0004\u0018\u00010\u0002\u0012\n\u0008\u0001\u0010\r\u001a\u0004\u0018\u00010\u0002\u0012\n\u0008\u0001\u0010\u0010\u001a\u0004\u0018\u00010\u0002\u0012\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0002\u0012\n\u0008\u0001\u0010\u0016\u001a\u0004\u0018\u00010\u0002\u0012\n\u0008\u0001\u0010\u0019\u001a\u0004\u0018\u00010\u0002\u0012\n\u0008\u0001\u0010\u001c\u001a\u0004\u0018\u00010\u0002\u0012\n\u0008\u0001\u0010\u001f\u001a\u0004\u0018\u00010\u0002\u0012\n\u0008\u0001\u0010\"\u001a\u0004\u0018\u00010\u0002\u0012\n\u0008\u0001\u0010%\u001a\u0004\u0018\u00010\u0002\u0012\n\u0008\u0001\u0010(\u001a\u0004\u0018\u00010\u0002\u0012\u0008\u0010.\u001a\u0004\u0018\u00010)\u0012\u0008\u00101\u001a\u0004\u0018\u00010)\u0012\u0008\u00104\u001a\u0004\u0018\u00010)\u0012\u0008\u00107\u001a\u0004\u0018\u00010)\u0012\u0008\u0010:\u001a\u0004\u0018\u00010)\u0012\u0008\u0010=\u001a\u0004\u0018\u00010)\u0012\u0008\u0010@\u001a\u0004\u0018\u00010)\u0012\u0008\u0010C\u001a\u0004\u0018\u00010)\u0012\u0008\u0010F\u001a\u0004\u0018\u00010)\u0012\u0008\u0010I\u001a\u0004\u0018\u00010)\u00a2\u0006\u0004\u0008J\u0010KR\u0019\u0010\u0007\u001a\u0004\u0018\u00010\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008\u0003\u0010\u0004\u001a\u0004\u0008\u0005\u0010\u0006R\u0019\u0010\n\u001a\u0004\u0018\u00010\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008\u0008\u0010\u0004\u001a\u0004\u0008\t\u0010\u0006R\u0019\u0010\r\u001a\u0004\u0018\u00010\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008\u000b\u0010\u0004\u001a\u0004\u0008\u000c\u0010\u0006R\u0019\u0010\u0010\u001a\u0004\u0018\u00010\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008\u000e\u0010\u0004\u001a\u0004\u0008\u000f\u0010\u0006R\u0019\u0010\u0013\u001a\u0004\u0018\u00010\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008\u0011\u0010\u0004\u001a\u0004\u0008\u0012\u0010\u0006R\u0019\u0010\u0016\u001a\u0004\u0018\u00010\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008\u0014\u0010\u0004\u001a\u0004\u0008\u0015\u0010\u0006R\u0019\u0010\u0019\u001a\u0004\u0018\u00010\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008\u0017\u0010\u0004\u001a\u0004\u0008\u0018\u0010\u0006R\u0019\u0010\u001c\u001a\u0004\u0018\u00010\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008\u001a\u0010\u0004\u001a\u0004\u0008\u001b\u0010\u0006R\u0019\u0010\u001f\u001a\u0004\u0018\u00010\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008\u001d\u0010\u0004\u001a\u0004\u0008\u001e\u0010\u0006R\u0019\u0010\"\u001a\u0004\u0018\u00010\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008 \u0010\u0004\u001a\u0004\u0008!\u0010\u0006R\u0019\u0010%\u001a\u0004\u0018\u00010\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008#\u0010\u0004\u001a\u0004\u0008$\u0010\u0006R\u0019\u0010(\u001a\u0004\u0018\u00010\u00028\u0006\u00a2\u0006\u000c\n\u0004\u0008&\u0010\u0004\u001a\u0004\u0008\'\u0010\u0006R\u0019\u0010.\u001a\u0004\u0018\u00010)8\u0006\u00a2\u0006\u000c\n\u0004\u0008*\u0010+\u001a\u0004\u0008,\u0010-R\u0019\u00101\u001a\u0004\u0018\u00010)8\u0006\u00a2\u0006\u000c\n\u0004\u0008/\u0010+\u001a\u0004\u00080\u0010-R\u0019\u00104\u001a\u0004\u0018\u00010)8\u0006\u00a2\u0006\u000c\n\u0004\u00082\u0010+\u001a\u0004\u00083\u0010-R\u0019\u00107\u001a\u0004\u0018\u00010)8\u0006\u00a2\u0006\u000c\n\u0004\u00085\u0010+\u001a\u0004\u00086\u0010-R\u0019\u0010:\u001a\u0004\u0018\u00010)8\u0006\u00a2\u0006\u000c\n\u0004\u00088\u0010+\u001a\u0004\u00089\u0010-R\u0019\u0010=\u001a\u0004\u0018\u00010)8\u0006\u00a2\u0006\u000c\n\u0004\u0008;\u0010+\u001a\u0004\u0008<\u0010-R\u0019\u0010@\u001a\u0004\u0018\u00010)8\u0006\u00a2\u0006\u000c\n\u0004\u0008>\u0010+\u001a\u0004\u0008?\u0010-R\u0019\u0010C\u001a\u0004\u0018\u00010)8\u0006\u00a2\u0006\u000c\n\u0004\u0008A\u0010+\u001a\u0004\u0008B\u0010-R\u0019\u0010F\u001a\u0004\u0018\u00010)8\u0006\u00a2\u0006\u000c\n\u0004\u0008D\u0010+\u001a\u0004\u0008E\u0010-R\u0019\u0010I\u001a\u0004\u0018\u00010)8\u0006\u00a2\u0006\u000c\n\u0004\u0008G\u0010+\u001a\u0004\u0008H\u0010-\u00a8\u0006M"
    }
    d2 = {
        "Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;",
        "",
        "",
        "a",
        "Ljava/lang/Integer;",
        "getSelectionBorderWidth",
        "()Ljava/lang/Integer;",
        "selectionBorderWidth",
        "b",
        "getSelectionBorderColor",
        "selectionBorderColor",
        "c",
        "getSelectionScaleHandleColor",
        "selectionScaleHandleColor",
        "d",
        "getSelectionEditHandleColor",
        "selectionEditHandleColor",
        "e",
        "getSelectionPadding",
        "selectionPadding",
        "f",
        "getGuideLineWidth",
        "guideLineWidth",
        "g",
        "getGuideLineColor",
        "guideLineColor",
        "h",
        "getGuideLineIncrease",
        "guideLineIncrease",
        "i",
        "getLinkAnnotationBackgroundColor",
        "linkAnnotationBackgroundColor",
        "j",
        "getLinkAnnotationBorderColor",
        "linkAnnotationBorderColor",
        "k",
        "getLinkAnnotationHighlightBackgroundColor",
        "linkAnnotationHighlightBackgroundColor",
        "l",
        "getLinkAnnotationHighlightBorderColor",
        "linkAnnotationHighlightBorderColor",
        "Landroid/graphics/drawable/Drawable;",
        "m",
        "Landroid/graphics/drawable/Drawable;",
        "getTopLeftScaleHandleDrawable",
        "()Landroid/graphics/drawable/Drawable;",
        "topLeftScaleHandleDrawable",
        "n",
        "getTopCenterScaleHandleDrawable",
        "topCenterScaleHandleDrawable",
        "o",
        "getTopRightScaleHandleDrawable",
        "topRightScaleHandleDrawable",
        "p",
        "getCenterLeftScaleHandleDrawable",
        "centerLeftScaleHandleDrawable",
        "q",
        "getCenterRightScaleHandleDrawable",
        "centerRightScaleHandleDrawable",
        "r",
        "getBottomLeftScaleHandleDrawable",
        "bottomLeftScaleHandleDrawable",
        "s",
        "getBottomCenterScaleHandleDrawable",
        "bottomCenterScaleHandleDrawable",
        "t",
        "getBottomRightScaleHandleDrawable",
        "bottomRightScaleHandleDrawable",
        "u",
        "getRotationHandleDrawable",
        "rotationHandleDrawable",
        "v",
        "getBackgroundDrawable",
        "backgroundDrawable",
        "<init>",
        "(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V",
        "Builder",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# static fields
.field public static final $stable:I = 0x8


# instance fields
.field private final a:Ljava/lang/Integer;

.field private final b:Ljava/lang/Integer;

.field private final c:Ljava/lang/Integer;

.field private final d:Ljava/lang/Integer;

.field private final e:Ljava/lang/Integer;

.field private final f:Ljava/lang/Integer;

.field private final g:Ljava/lang/Integer;

.field private final h:Ljava/lang/Integer;

.field private final i:Ljava/lang/Integer;

.field private final j:Ljava/lang/Integer;

.field private final k:Ljava/lang/Integer;

.field private final l:Ljava/lang/Integer;

.field private final m:Landroid/graphics/drawable/Drawable;

.field private final n:Landroid/graphics/drawable/Drawable;

.field private final o:Landroid/graphics/drawable/Drawable;

.field private final p:Landroid/graphics/drawable/Drawable;

.field private final q:Landroid/graphics/drawable/Drawable;

.field private final r:Landroid/graphics/drawable/Drawable;

.field private final s:Landroid/graphics/drawable/Drawable;

.field private final t:Landroid/graphics/drawable/Drawable;

.field private final u:Landroid/graphics/drawable/Drawable;

.field private final v:Landroid/graphics/drawable/Drawable;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;)V
    .locals 23

    move-object/from16 v0, p0

    .line 77
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getSelectionBorderWidth()Ljava/lang/Integer;

    move-result-object v1

    .line 78
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getSelectionBorderColor()Ljava/lang/Integer;

    move-result-object v2

    .line 79
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getSelectionScaleHandleColor()Ljava/lang/Integer;

    move-result-object v3

    .line 80
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getSelectionEditHandleColor()Ljava/lang/Integer;

    move-result-object v4

    .line 81
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getSelectionPadding()Ljava/lang/Integer;

    move-result-object v5

    .line 82
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getGuideLineWidth()Ljava/lang/Integer;

    move-result-object v6

    .line 83
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getGuideLineColor()Ljava/lang/Integer;

    move-result-object v7

    .line 84
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getGuideLineIncrease()Ljava/lang/Integer;

    move-result-object v8

    .line 85
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getLinkAnnotationBackgroundColor()Ljava/lang/Integer;

    move-result-object v9

    .line 86
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getLinkAnnotationBorderColor()Ljava/lang/Integer;

    move-result-object v10

    .line 87
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getLinkAnnotationHighlightBackgroundColor()Ljava/lang/Integer;

    move-result-object v11

    .line 88
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getLinkAnnotationHighlightBorderColor()Ljava/lang/Integer;

    move-result-object v12

    .line 89
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getTopLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 90
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getTopCenterScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v14

    .line 91
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getTopRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v15

    .line 92
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getCenterLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v16

    .line 93
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getCenterRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v17

    .line 94
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getBottomLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v18

    .line 95
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getBottomCenterScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v19

    .line 96
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getBottomRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v20

    .line 97
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getRotationHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v21

    .line 98
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->getBackgroundDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v22

    .line 99
    invoke-direct/range {v0 .. v22}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;-><init>(Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    move-object v0, p0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 3
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->a:Ljava/lang/Integer;

    move-object v1, p2

    .line 6
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->b:Ljava/lang/Integer;

    move-object v1, p3

    .line 10
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->c:Ljava/lang/Integer;

    move-object v1, p4

    .line 14
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->d:Ljava/lang/Integer;

    move-object v1, p5

    .line 18
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->e:Ljava/lang/Integer;

    move-object v1, p6

    .line 21
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->f:Ljava/lang/Integer;

    move-object v1, p7

    .line 25
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->g:Ljava/lang/Integer;

    move-object v1, p8

    .line 29
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->h:Ljava/lang/Integer;

    move-object v1, p9

    .line 33
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->i:Ljava/lang/Integer;

    move-object v1, p10

    .line 37
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->j:Ljava/lang/Integer;

    move-object v1, p11

    .line 41
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->k:Ljava/lang/Integer;

    move-object v1, p12

    .line 45
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->l:Ljava/lang/Integer;

    move-object v1, p13

    .line 49
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->m:Landroid/graphics/drawable/Drawable;

    move-object/from16 v1, p14

    .line 52
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->n:Landroid/graphics/drawable/Drawable;

    move-object/from16 v1, p15

    .line 55
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->o:Landroid/graphics/drawable/Drawable;

    move-object/from16 v1, p16

    .line 58
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->p:Landroid/graphics/drawable/Drawable;

    move-object/from16 v1, p17

    .line 61
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->q:Landroid/graphics/drawable/Drawable;

    move-object/from16 v1, p18

    .line 64
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->r:Landroid/graphics/drawable/Drawable;

    move-object/from16 v1, p19

    .line 67
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->s:Landroid/graphics/drawable/Drawable;

    move-object/from16 v1, p20

    .line 70
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->t:Landroid/graphics/drawable/Drawable;

    move-object/from16 v1, p21

    .line 73
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->u:Landroid/graphics/drawable/Drawable;

    move-object/from16 v1, p22

    .line 76
    iput-object v1, v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->v:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public final getBackgroundDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->v:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getBottomCenterScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->s:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getBottomLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->r:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getBottomRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->t:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getCenterLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->p:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getCenterRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->q:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getGuideLineColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->g:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getGuideLineIncrease()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->h:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getGuideLineWidth()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->f:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getLinkAnnotationBackgroundColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->i:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getLinkAnnotationBorderColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->j:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getLinkAnnotationHighlightBackgroundColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->k:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getLinkAnnotationHighlightBorderColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->l:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getRotationHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->u:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getSelectionBorderColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getSelectionBorderWidth()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->a:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getSelectionEditHandleColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->d:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getSelectionPadding()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->e:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getSelectionScaleHandleColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->c:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getTopCenterScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->n:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getTopLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->m:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getTopRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->o:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

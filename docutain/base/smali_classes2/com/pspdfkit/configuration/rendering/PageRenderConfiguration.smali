.class public Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    }
.end annotation


# instance fields
.field public final formHighlightColor:Ljava/lang/Integer;

.field public final formItemHighlightColor:Ljava/lang/Integer;

.field public final formRequiredFieldBorderColor:Ljava/lang/Integer;

.field public final invertColors:Z

.field public final paperColor:I

.field public final redactionAnnotationPreviewEnabled:Z

.field public final regionFullPageHeight:I

.field public final regionFullPageWidth:I

.field public final regionX:I

.field public final regionY:I

.field public final renderRegion:Z

.field public final renderedDrawables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawable;",
            ">;"
        }
    .end annotation
.end field

.field public final reuseBitmap:Landroid/graphics/Bitmap;

.field public final showSignHereOverlay:Z

.field public final signHereOverlayBackgroundColor:Ljava/lang/Integer;

.field public final toGrayscale:Z

.field public final useCache:Z


# direct methods
.method private constructor <init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ZLandroid/graphics/Bitmap;ZIIIIZZZLjava/util/ArrayList;Z)V
    .locals 2

    move-object v0, p0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move v1, p1

    .line 2
    iput v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->paperColor:I

    move-object v1, p2

    .line 3
    iput-object v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->formHighlightColor:Ljava/lang/Integer;

    move-object v1, p3

    .line 4
    iput-object v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->formItemHighlightColor:Ljava/lang/Integer;

    move-object v1, p4

    .line 5
    iput-object v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->formRequiredFieldBorderColor:Ljava/lang/Integer;

    move-object v1, p5

    .line 6
    iput-object v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->signHereOverlayBackgroundColor:Ljava/lang/Integer;

    move v1, p13

    .line 7
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->invertColors:Z

    move/from16 v1, p14

    .line 8
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->toGrayscale:Z

    move/from16 v1, p15

    .line 9
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->redactionAnnotationPreviewEnabled:Z

    move v1, p6

    .line 10
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->useCache:Z

    move-object v1, p7

    .line 11
    iput-object v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->reuseBitmap:Landroid/graphics/Bitmap;

    move v1, p8

    .line 12
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->renderRegion:Z

    move v1, p9

    .line 13
    iput v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->regionX:I

    move v1, p10

    .line 14
    iput v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->regionY:I

    move v1, p11

    .line 15
    iput v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->regionFullPageWidth:I

    move v1, p12

    .line 16
    iput v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->regionFullPageHeight:I

    move-object/from16 v1, p16

    .line 17
    iput-object v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->renderedDrawables:Ljava/util/List;

    move/from16 v1, p17

    .line 18
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->showSignHereOverlay:Z

    return-void
.end method

.method synthetic constructor <init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ZLandroid/graphics/Bitmap;ZIIIIZZZLjava/util/ArrayList;ZLcom/pspdfkit/configuration/rendering/PageRenderConfiguration-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p17}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ZLandroid/graphics/Bitmap;ZIIIIZZZLjava/util/ArrayList;Z)V

    return-void
.end method

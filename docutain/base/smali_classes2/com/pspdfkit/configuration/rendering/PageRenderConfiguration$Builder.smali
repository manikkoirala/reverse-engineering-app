.class public Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private b:I

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Landroid/graphics/Bitmap;

.field private l:Z

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->a:Ljava/util/ArrayList;

    const/4 v0, -0x1

    .line 6
    iput v0, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->b:I

    const/4 v0, 0x0

    .line 8
    iput-object v0, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->c:Ljava/lang/Integer;

    .line 11
    iput-object v0, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->d:Ljava/lang/Integer;

    .line 14
    iput-object v0, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->e:Ljava/lang/Integer;

    .line 17
    iput-object v0, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->f:Ljava/lang/Integer;

    const/4 v1, 0x0

    .line 21
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->g:Z

    .line 22
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->h:Z

    .line 23
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->i:Z

    const/4 v2, 0x1

    .line 24
    iput-boolean v2, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->j:Z

    .line 26
    iput-object v0, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->k:Landroid/graphics/Bitmap;

    .line 29
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->l:Z

    .line 30
    iput v1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->m:I

    .line 31
    iput v1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->n:I

    .line 32
    iput v1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->o:I

    .line 33
    iput v1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->p:I

    .line 34
    iput-boolean v2, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->q:Z

    return-void
.end method


# virtual methods
.method public build()Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;
    .locals 22

    move-object/from16 v0, p0

    .line 1
    new-instance v20, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    move-object/from16 v1, v20

    iget v2, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->b:I

    iget-object v3, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->c:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->d:Ljava/lang/Integer;

    iget-object v5, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->e:Ljava/lang/Integer;

    iget-object v6, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->f:Ljava/lang/Integer;

    iget-boolean v7, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->j:Z

    iget-object v8, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->k:Landroid/graphics/Bitmap;

    iget-boolean v9, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->l:Z

    iget v10, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->m:I

    iget v11, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->n:I

    iget v12, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->o:I

    iget v13, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->p:I

    iget-boolean v14, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->g:Z

    iget-boolean v15, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->h:Z

    move-object/from16 v21, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->i:Z

    move/from16 v16, v1

    iget-object v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->a:Ljava/util/ArrayList;

    move-object/from16 v17, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->q:Z

    move/from16 v18, v1

    const/16 v19, 0x0

    move-object/from16 v1, v21

    invoke-direct/range {v1 .. v19}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ZLandroid/graphics/Bitmap;ZIIIIZZZLjava/util/ArrayList;ZLcom/pspdfkit/configuration/rendering/PageRenderConfiguration-IA;)V

    return-object v20
.end method

.method public cache(Z)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->j:Z

    return-object p0
.end method

.method public formHighlightColor(I)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->c:Ljava/lang/Integer;

    return-object p0
.end method

.method public formItemHighlightColor(I)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->d:Ljava/lang/Integer;

    return-object p0
.end method

.method public formRequiredFieldBorderColor(I)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->e:Ljava/lang/Integer;

    return-object p0
.end method

.method public invertColors(Z)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->g:Z

    return-object p0
.end method

.method public paperColor(I)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->b:I

    return-object p0
.end method

.method public redactionAnnotationPreviewEnabled(Z)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->i:Z

    return-object p0
.end method

.method public region(IIII)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->l:Z

    .line 2
    iput p1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->m:I

    .line 3
    iput p2, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->n:I

    .line 4
    iput p3, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->o:I

    .line 5
    iput p4, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->p:I

    return-object p0
.end method

.method public renderedDrawables(Ljava/util/List;)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawable;",
            ">;)",
            "Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;"
        }
    .end annotation

    const-string v0, "pdfDrawables"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public reuseBitmap(Landroid/graphics/Bitmap;)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->k:Landroid/graphics/Bitmap;

    return-object p0
.end method

.method public showSignHereOverlay(Z)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->q:Z

    return-object p0
.end method

.method public signHereOverlayBackgroundColor(Ljava/lang/Integer;)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->f:Ljava/lang/Integer;

    return-object p0
.end method

.method public toGrayscale(Z)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->h:Z

    return-object p0
.end method

.class Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;
    .locals 70

    move-object/from16 v0, p1

    .line 2
    new-instance v65, Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;

    .line 3
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/pspdfkit/configuration/page/PageScrollDirection;

    invoke-static {v2, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/pspdfkit/configuration/page/PageScrollDirection;

    .line 4
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/pspdfkit/configuration/page/PageScrollMode;

    invoke-static {v2, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/pspdfkit/configuration/page/PageScrollMode;

    .line 5
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/pspdfkit/configuration/page/PageFitMode;

    invoke-static {v2, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/pspdfkit/configuration/page/PageFitMode;

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/pspdfkit/configuration/page/PageLayoutMode;

    invoke-static {v2, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/pspdfkit/configuration/page/PageLayoutMode;

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/pspdfkit/configuration/theming/ThemeMode;

    invoke-static {v2, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/pspdfkit/configuration/theming/ThemeMode;

    .line 8
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    const/16 v30, 0x1

    goto :goto_0

    :cond_0
    const/16 v30, 0x0

    .line 9
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v3, :cond_1

    const/16 v31, 0x1

    goto :goto_1

    :cond_1
    const/16 v31, 0x0

    .line 10
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v3, :cond_2

    const/16 v32, 0x1

    goto :goto_2

    :cond_2
    const/16 v32, 0x0

    .line 11
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 12
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v19, v1

    goto :goto_3

    :cond_3
    const/16 v19, 0x0

    .line 13
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 14
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v3, :cond_4

    const/16 v33, 0x1

    goto :goto_4

    :cond_4
    const/16 v33, 0x0

    .line 15
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v3, :cond_5

    const/16 v34, 0x1

    goto :goto_5

    :cond_5
    const/16 v34, 0x0

    .line 16
    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    .line 17
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readFloat()F

    move-result v7

    .line 18
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-ne v8, v3, :cond_6

    const/16 v35, 0x1

    goto :goto_6

    :cond_6
    const/16 v35, 0x0

    .line 19
    :goto_6
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-ne v8, v3, :cond_7

    const/16 v36, 0x1

    goto :goto_7

    :cond_7
    const/16 v36, 0x0

    .line 20
    :goto_7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-ne v8, v3, :cond_8

    const/16 v37, 0x1

    goto :goto_8

    :cond_8
    const/16 v37, 0x0

    .line 21
    :goto_8
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-ne v8, v3, :cond_9

    const/16 v38, 0x1

    goto :goto_9

    :cond_9
    const/16 v38, 0x0

    .line 22
    :goto_9
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-ne v8, v3, :cond_a

    const/16 v39, 0x1

    goto :goto_a

    :cond_a
    const/16 v39, 0x0

    .line 23
    :goto_a
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-ne v8, v3, :cond_b

    const/16 v40, 0x1

    goto :goto_b

    :cond_b
    const/16 v40, 0x0

    .line 24
    :goto_b
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-ne v8, v3, :cond_c

    const/16 v41, 0x1

    goto :goto_c

    :cond_c
    const/16 v41, 0x0

    .line 25
    :goto_c
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-ne v8, v3, :cond_d

    const/16 v42, 0x1

    goto :goto_d

    :cond_d
    const/16 v42, 0x0

    .line 26
    :goto_d
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-ne v8, v3, :cond_e

    const/16 v43, 0x1

    goto :goto_e

    :cond_e
    const/16 v43, 0x0

    .line 27
    :goto_e
    const-class v8, Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v25

    .line 28
    const-class v8, Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v26

    .line 29
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-ne v8, v3, :cond_f

    const/16 v44, 0x1

    goto :goto_f

    :cond_f
    const/16 v44, 0x0

    .line 30
    :goto_f
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-ne v8, v3, :cond_10

    const/16 v45, 0x1

    goto :goto_10

    :cond_10
    const/16 v45, 0x0

    .line 31
    :goto_10
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-ne v8, v3, :cond_11

    const/16 v46, 0x1

    goto :goto_11

    :cond_11
    const/16 v46, 0x0

    .line 32
    :goto_11
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readFloat()F

    move-result v8

    .line 33
    const-class v9, Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v9}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v27

    .line 34
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-ne v9, v3, :cond_12

    const/16 v47, 0x1

    goto :goto_12

    :cond_12
    const/16 v47, 0x0

    .line 35
    :goto_12
    const-class v9, Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v9}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v22

    .line 36
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-ne v9, v3, :cond_13

    const/16 v48, 0x1

    goto :goto_13

    :cond_13
    const/16 v48, 0x0

    .line 37
    :goto_13
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    .line 38
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v14

    if-ne v14, v3, :cond_14

    const/16 v49, 0x1

    goto :goto_14

    :cond_14
    const/16 v49, 0x0

    .line 39
    :goto_14
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v14

    if-ne v14, v3, :cond_15

    const/16 v50, 0x1

    goto :goto_15

    :cond_15
    const/16 v50, 0x0

    .line 40
    :goto_15
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v14

    if-ne v14, v3, :cond_16

    const/16 v51, 0x1

    goto :goto_16

    :cond_16
    const/16 v51, 0x0

    .line 41
    :goto_16
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v14

    if-ne v14, v3, :cond_17

    const/16 v52, 0x1

    goto :goto_17

    :cond_17
    const/16 v52, 0x0

    .line 42
    :goto_17
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v14

    if-ne v14, v3, :cond_18

    const/16 v53, 0x1

    goto :goto_18

    :cond_18
    const/16 v53, 0x0

    .line 43
    :goto_18
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v14

    move-object/from16 v23, v14

    check-cast v23, Ljava/util/EnumSet;

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v14

    if-ne v14, v3, :cond_19

    const/16 v54, 0x1

    goto :goto_19

    :cond_19
    const/16 v54, 0x0

    .line 45
    :goto_19
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v14

    if-ne v14, v3, :cond_1a

    const/16 v55, 0x1

    goto :goto_1a

    :cond_1a
    const/16 v55, 0x0

    .line 46
    :goto_1a
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    const-class v15, Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    invoke-static {v15, v14}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v14

    check-cast v14, Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    .line 47
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v15

    if-nez v15, :cond_1b

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v20, v15

    goto :goto_1b

    :cond_1b
    const/16 v20, 0x0

    .line 48
    :goto_1b
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v15

    if-ne v15, v3, :cond_1c

    const/16 v56, 0x1

    goto :goto_1c

    :cond_1c
    const/16 v56, 0x0

    .line 49
    :goto_1c
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v15

    const-class v2, Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    invoke-static {v2, v15}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v2

    move-object v15, v2

    check-cast v15, Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    .line 50
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    const-class v5, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    invoke-static {v5, v2}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v2

    move-object/from16 v21, v2

    check-cast v21, Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    .line 51
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-nez v2, :cond_1d

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v24, v2

    goto :goto_1d

    :cond_1d
    const/16 v24, 0x0

    .line 52
    :goto_1d
    const-class v2, Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    move-object/from16 v18, v2

    check-cast v18, Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    .line 53
    const-class v2, Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v28

    .line 54
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    const-class v5, Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    invoke-static {v5, v2}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v2

    move-object/from16 v57, v2

    check-cast v57, Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    .line 55
    const-class v2, Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    move-object/from16 v58, v2

    check-cast v58, Lcom/pspdfkit/signatures/SignatureAppearance;

    .line 56
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_1e

    const/16 v59, 0x1

    goto :goto_1e

    :cond_1e
    const/16 v59, 0x0

    .line 57
    :goto_1e
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_1f

    const/16 v60, 0x1

    goto :goto_1f

    :cond_1f
    const/16 v60, 0x0

    .line 58
    :goto_1f
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_20

    const/16 v61, 0x1

    goto :goto_20

    :cond_20
    const/16 v61, 0x0

    .line 59
    :goto_20
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v2

    move-object/from16 v62, v2

    check-cast v62, Ljava/util/EnumSet;

    .line 60
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_21

    const/16 v63, 0x1

    goto :goto_21

    :cond_21
    const/16 v63, 0x0

    .line 61
    :goto_21
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_22

    const/16 v64, 0x1

    goto :goto_22

    :cond_22
    const/16 v64, 0x0

    .line 62
    :goto_22
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_23

    const/16 v66, 0x1

    goto :goto_23

    :cond_23
    const/16 v66, 0x0

    .line 63
    :goto_23
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v67

    .line 64
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_24

    const/16 v68, 0x1

    goto :goto_24

    :cond_24
    const/16 v68, 0x0

    .line 65
    :goto_24
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_25

    const/16 v69, 0x1

    goto :goto_25

    :cond_25
    const/16 v69, 0x0

    .line 66
    :goto_25
    const-class v2, Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v29

    move-object/from16 v0, v65

    move v2, v7

    move v3, v8

    move v5, v6

    move v6, v9

    move/from16 v7, v67

    move-object v8, v14

    move-object v9, v15

    move-object/from16 v14, v57

    move-object/from16 v15, v18

    move-object/from16 v16, v21

    move-object/from16 v18, v58

    move-object/from16 v21, v24

    move-object/from16 v24, v62

    move/from16 v57, v59

    move/from16 v58, v60

    move/from16 v59, v61

    move/from16 v60, v63

    move/from16 v61, v64

    move/from16 v62, v66

    move/from16 v63, v68

    move/from16 v64, v69

    invoke-direct/range {v0 .. v64}, Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;-><init>(FFFIIIILcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;Lcom/pspdfkit/configuration/page/PageFitMode;Lcom/pspdfkit/configuration/page/PageLayoutMode;Lcom/pspdfkit/configuration/page/PageScrollDirection;Lcom/pspdfkit/configuration/page/PageScrollMode;Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;Lcom/pspdfkit/configuration/theming/ThemeMode;Lcom/pspdfkit/signatures/SignatureAppearance;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/EnumSet;Ljava/util/EnumSet;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Map;ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ)V

    return-object v65
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration$1;->createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;
    .locals 0

    .line 2
    new-array p1, p1, [Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration$1;->newArray(I)[Lcom/pspdfkit/configuration/AutoValue_PdfConfiguration;

    move-result-object p1

    return-object p1
.end method

.class public abstract Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$SearchType;,
        Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_LISTED_ANNOTATION_TYPES:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field public static final NO_THEME:I = -0x1

.field public static final SEARCH_INLINE:I = 0x1

.field public static final SEARCH_MODULAR:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->DEFAULT_LISTED_ANNOTATION_TYPES:Ljava/util/EnumSet;

    .line 4
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->LINK:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 5
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->CARET:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 6
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->RICHMEDIA:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 7
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->SCREEN:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 8
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->POPUP:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 9
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->WATERMARK:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 10
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->TRAPNET:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 11
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->TYPE3D:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getActivityTitle()Ljava/lang/String;
.end method

.method public abstract getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;
.end method

.method public abstract getDarkTheme()I
.end method

.method public abstract getLayout()I
.end method

.method public abstract getListedAnnotationTypes()Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSearchConfiguration()Lcom/pspdfkit/configuration/search/SearchConfiguration;
.end method

.method public abstract getSearchType()I
.end method

.method public abstract getSettingsMenuItemShown()Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTabBarHidingMode()Lcom/pspdfkit/configuration/activity/TabBarHidingMode;
.end method

.method public abstract getTheme()I
.end method

.method public abstract getThumbnailBarMode()Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;
.end method

.method public abstract getUserInterfaceViewMode()Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;
.end method

.method public abstract hideUserInterfaceWhenCreatingAnnotations()Z
.end method

.method public abstract isAnnotationListEnabled()Z
.end method

.method public abstract isAnnotationListReorderingEnabled()Z
.end method

.method public abstract isAnnotationNoteHintingEnabled()Z
.end method

.method public abstract isBookmarkEditingEnabled()Z
.end method

.method public abstract isBookmarkListEnabled()Z
.end method

.method public abstract isDocumentEditorEnabled()Z
.end method

.method public abstract isDocumentInfoViewEnabled()Z
.end method

.method public abstract isDocumentInfoViewSeparated()Z
.end method

.method public abstract isImmersiveMode()Z
.end method

.method public abstract isOutlineEnabled()Z
.end method

.method public abstract isPrintingEnabled()Z
.end method

.method public abstract isReaderViewEnabled()Z
.end method

.method public abstract isRedactionUiEnabled()Z
.end method

.method public abstract isSearchEnabled()Z
.end method

.method public abstract isSettingsItemEnabled()Z
.end method

.method public abstract isShowDocumentTitleOverlayEnabled()Z
.end method

.method public abstract isShowNavigationButtonsEnabled()Z
.end method

.method public abstract isShowPageLabels()Z
.end method

.method public abstract isShowPageNumberOverlay()Z
.end method

.method public abstract isSignatureButtonPositionForcedInMainToolbar()Z
.end method

.method public abstract isThumbnailGridEnabled()Z
.end method

.method public abstract isVolumeButtonsNavigationEnabled()Z
.end method

.method public abstract page()I
.end method

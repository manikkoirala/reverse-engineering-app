.class public final enum Lcom/pspdfkit/configuration/activity/TabBarHidingMode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/configuration/activity/TabBarHidingMode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AUTOMATIC:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

.field public static final enum AUTOMATIC_HIDE_SINGLE:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

.field public static final enum HIDE:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

.field public static final enum SHOW:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

.field private static final synthetic a:[Lcom/pspdfkit/configuration/activity/TabBarHidingMode;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    const-string v1, "AUTOMATIC"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;->AUTOMATIC:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    .line 3
    new-instance v1, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    const-string v3, "AUTOMATIC_HIDE_SINGLE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;->AUTOMATIC_HIDE_SINGLE:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    .line 5
    new-instance v3, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    const-string v5, "SHOW"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;->SHOW:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    .line 7
    new-instance v5, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    const-string v7, "HIDE"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;->HIDE:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    const/4 v7, 0x4

    new-array v7, v7, [Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    .line 8
    sput-object v7, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;->a:[Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/configuration/activity/TabBarHidingMode;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/configuration/activity/TabBarHidingMode;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;->a:[Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    invoke-virtual {v0}, [Lcom/pspdfkit/configuration/activity/TabBarHidingMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    return-object v0
.end method

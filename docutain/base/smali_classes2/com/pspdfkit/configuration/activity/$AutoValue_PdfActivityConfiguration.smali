.class abstract Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;
.super Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
.source "SourceFile"


# instance fields
.field private final A:Z

.field private final B:Z

.field private final C:Z

.field private final D:I

.field private final E:Lcom/pspdfkit/configuration/search/SearchConfiguration;

.field private final F:Z

.field private final G:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

.field private final H:Z

.field private final I:Z

.field private final J:Z

.field private final a:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:Z

.field private final g:Z

.field private final h:Z

.field private final i:Z

.field private final j:Z

.field private final k:Z

.field private final l:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

.field private final m:Z

.field private final n:Z

.field private final o:Z

.field private final p:Z

.field private final q:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;",
            ">;"
        }
    .end annotation
.end field

.field private final r:I

.field private final s:Z

.field private final t:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

.field private final u:Z

.field private final v:Z

.field private final w:Z

.field private final x:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Z

.field private final z:Z


# direct methods
.method constructor <init>(IIIIILcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/configuration/activity/TabBarHidingMode;Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;Lcom/pspdfkit/configuration/search/SearchConfiguration;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/EnumSet;ZZZZZZZZZZZZZZZZZZZZZZZ)V
    .locals 7

    move-object v0, p0

    move-object v1, p6

    move-object v2, p7

    move-object v3, p8

    move-object/from16 v4, p9

    move-object/from16 v5, p12

    move-object/from16 v6, p13

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;-><init>()V

    if-eqz v1, :cond_5

    .line 5
    iput-object v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->a:Lcom/pspdfkit/configuration/PdfConfiguration;

    move-object/from16 v1, p11

    .line 6
    iput-object v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->b:Ljava/lang/String;

    move v1, p1

    .line 7
    iput v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->c:I

    move v1, p2

    .line 8
    iput v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->d:I

    move v1, p3

    .line 9
    iput v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->e:I

    move/from16 v1, p14

    .line 10
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->f:Z

    move/from16 v1, p15

    .line 11
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->g:Z

    move/from16 v1, p16

    .line 12
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->h:Z

    move/from16 v1, p17

    .line 13
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->i:Z

    move/from16 v1, p18

    .line 14
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->j:Z

    move/from16 v1, p19

    .line 15
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->k:Z

    if-eqz v3, :cond_4

    .line 19
    iput-object v3, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->l:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    move/from16 v1, p20

    .line 20
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->m:Z

    move/from16 v1, p21

    .line 21
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->n:Z

    move/from16 v1, p22

    .line 22
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->o:Z

    move/from16 v1, p23

    .line 23
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->p:Z

    if-eqz v5, :cond_3

    .line 27
    iput-object v5, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->q:Ljava/util/EnumSet;

    move v1, p4

    .line 28
    iput v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->r:I

    move/from16 v1, p24

    .line 29
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->s:Z

    if-eqz v4, :cond_2

    .line 33
    iput-object v4, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->t:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    move/from16 v1, p25

    .line 34
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->u:Z

    move/from16 v1, p26

    .line 35
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->v:Z

    move/from16 v1, p27

    .line 36
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->w:Z

    if-eqz v6, :cond_1

    .line 40
    iput-object v6, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->x:Ljava/util/EnumSet;

    move/from16 v1, p28

    .line 41
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->y:Z

    move/from16 v1, p29

    .line 42
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->z:Z

    move/from16 v1, p30

    .line 43
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->A:Z

    move/from16 v1, p31

    .line 44
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->B:Z

    move/from16 v1, p32

    .line 45
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->C:Z

    move v1, p5

    .line 46
    iput v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->D:I

    move-object/from16 v1, p10

    .line 47
    iput-object v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->E:Lcom/pspdfkit/configuration/search/SearchConfiguration;

    move/from16 v1, p33

    .line 48
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->F:Z

    if-eqz v2, :cond_0

    .line 52
    iput-object v2, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->G:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    move/from16 v1, p34

    .line 53
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->H:Z

    move/from16 v1, p35

    .line 54
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->I:Z

    move/from16 v1, p36

    .line 55
    iput-boolean v1, v0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->J:Z

    return-void

    .line 56
    :cond_0
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getTabBarHidingMode"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 57
    :cond_1
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getListedAnnotationTypes"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 58
    :cond_2
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getUserInterfaceViewMode"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 59
    :cond_3
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getSettingsMenuItemShown"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 60
    :cond_4
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getThumbnailBarMode"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 61
    :cond_5
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null getConfiguration"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    .line 2
    check-cast p1, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->a:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getActivityTitle()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getActivityTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_0
    iget v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->c:I

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getLayout()I

    move-result v3

    if-ne v1, v3, :cond_3

    iget v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->d:I

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getTheme()I

    move-result v3

    if-ne v1, v3, :cond_3

    iget v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->e:I

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getDarkTheme()I

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->f:Z

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isImmersiveMode()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->g:Z

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageNumberOverlay()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->h:Z

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isSignatureButtonPositionForcedInMainToolbar()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->i:Z

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageLabels()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->j:Z

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowDocumentTitleOverlayEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->k:Z

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowNavigationButtonsEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->l:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getThumbnailBarMode()Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->m:Z

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isThumbnailGridEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->n:Z

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentEditorEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->o:Z

    .line 17
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isSearchEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->p:Z

    .line 18
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isSettingsItemEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->q:Ljava/util/EnumSet;

    .line 19
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSettingsMenuItemShown()Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->r:I

    .line 20
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSearchType()I

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->s:Z

    .line 21
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isPrintingEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->t:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    .line 22
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getUserInterfaceViewMode()Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->u:Z

    .line 23
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->hideUserInterfaceWhenCreatingAnnotations()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->v:Z

    .line 24
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isAnnotationListEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->w:Z

    .line 25
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isAnnotationListReorderingEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->x:Ljava/util/EnumSet;

    .line 26
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getListedAnnotationTypes()Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->y:Z

    .line 27
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isOutlineEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->z:Z

    .line 28
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isBookmarkListEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->A:Z

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isBookmarkEditingEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->B:Z

    .line 30
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentInfoViewEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->C:Z

    .line 31
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentInfoViewSeparated()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->D:I

    .line 32
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->page()I

    move-result v3

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->E:Lcom/pspdfkit/configuration/search/SearchConfiguration;

    if-nez v1, :cond_2

    .line 33
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSearchConfiguration()Lcom/pspdfkit/configuration/search/SearchConfiguration;

    move-result-object v1

    if-nez v1, :cond_3

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSearchConfiguration()Lcom/pspdfkit/configuration/search/SearchConfiguration;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_1
    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->F:Z

    .line 34
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isAnnotationNoteHintingEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->G:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    .line 35
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getTabBarHidingMode()Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->H:Z

    .line 36
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isVolumeButtonsNavigationEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->I:Z

    .line 37
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isRedactionUiEnabled()Z

    move-result v3

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->J:Z

    .line 38
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isReaderViewEnabled()Z

    move-result p1

    if-ne v1, p1, :cond_3

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    return v0

    :cond_4
    return v2
.end method

.method public getActivityTitle()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->a:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-object v0
.end method

.method public getDarkTheme()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->e:I

    return v0
.end method

.method public getLayout()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->c:I

    return v0
.end method

.method public getListedAnnotationTypes()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->x:Ljava/util/EnumSet;

    return-object v0
.end method

.method public getSearchConfiguration()Lcom/pspdfkit/configuration/search/SearchConfiguration;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->E:Lcom/pspdfkit/configuration/search/SearchConfiguration;

    return-object v0
.end method

.method public getSearchType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->r:I

    return v0
.end method

.method public getSettingsMenuItemShown()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->q:Ljava/util/EnumSet;

    return-object v0
.end method

.method public getTabBarHidingMode()Lcom/pspdfkit/configuration/activity/TabBarHidingMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->G:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    return-object v0
.end method

.method public getTheme()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->d:I

    return v0
.end method

.method public getThumbnailBarMode()Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->l:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    return-object v0
.end method

.method public getUserInterfaceViewMode()Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->t:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->a:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0xf4243

    xor-int/2addr v0, v1

    mul-int v0, v0, v1

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->b:Ljava/lang/String;

    const/4 v3, 0x0

    if-nez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_0
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 5
    iget v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->c:I

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 7
    iget v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->d:I

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 9
    iget v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->e:I

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 11
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->f:Z

    const/16 v4, 0x4cf

    const/16 v5, 0x4d5

    if-eqz v2, :cond_1

    const/16 v2, 0x4cf

    goto :goto_1

    :cond_1
    const/16 v2, 0x4d5

    :goto_1
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 13
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->g:Z

    if-eqz v2, :cond_2

    const/16 v2, 0x4cf

    goto :goto_2

    :cond_2
    const/16 v2, 0x4d5

    :goto_2
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 15
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->h:Z

    if-eqz v2, :cond_3

    const/16 v2, 0x4cf

    goto :goto_3

    :cond_3
    const/16 v2, 0x4d5

    :goto_3
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 17
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->i:Z

    if-eqz v2, :cond_4

    const/16 v2, 0x4cf

    goto :goto_4

    :cond_4
    const/16 v2, 0x4d5

    :goto_4
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 19
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->j:Z

    if-eqz v2, :cond_5

    const/16 v2, 0x4cf

    goto :goto_5

    :cond_5
    const/16 v2, 0x4d5

    :goto_5
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 21
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->k:Z

    if-eqz v2, :cond_6

    const/16 v2, 0x4cf

    goto :goto_6

    :cond_6
    const/16 v2, 0x4d5

    :goto_6
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 23
    iget-object v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->l:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 25
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->m:Z

    if-eqz v2, :cond_7

    const/16 v2, 0x4cf

    goto :goto_7

    :cond_7
    const/16 v2, 0x4d5

    :goto_7
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 27
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->n:Z

    if-eqz v2, :cond_8

    const/16 v2, 0x4cf

    goto :goto_8

    :cond_8
    const/16 v2, 0x4d5

    :goto_8
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 29
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->o:Z

    if-eqz v2, :cond_9

    const/16 v2, 0x4cf

    goto :goto_9

    :cond_9
    const/16 v2, 0x4d5

    :goto_9
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 31
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->p:Z

    if-eqz v2, :cond_a

    const/16 v2, 0x4cf

    goto :goto_a

    :cond_a
    const/16 v2, 0x4d5

    :goto_a
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 33
    iget-object v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->q:Ljava/util/EnumSet;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 35
    iget v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->r:I

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 37
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->s:Z

    if-eqz v2, :cond_b

    const/16 v2, 0x4cf

    goto :goto_b

    :cond_b
    const/16 v2, 0x4d5

    :goto_b
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 39
    iget-object v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->t:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 41
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->u:Z

    if-eqz v2, :cond_c

    const/16 v2, 0x4cf

    goto :goto_c

    :cond_c
    const/16 v2, 0x4d5

    :goto_c
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 43
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->v:Z

    if-eqz v2, :cond_d

    const/16 v2, 0x4cf

    goto :goto_d

    :cond_d
    const/16 v2, 0x4d5

    :goto_d
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 45
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->w:Z

    if-eqz v2, :cond_e

    const/16 v2, 0x4cf

    goto :goto_e

    :cond_e
    const/16 v2, 0x4d5

    :goto_e
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 47
    iget-object v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->x:Ljava/util/EnumSet;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 49
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->y:Z

    if-eqz v2, :cond_f

    const/16 v2, 0x4cf

    goto :goto_f

    :cond_f
    const/16 v2, 0x4d5

    :goto_f
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 51
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->z:Z

    if-eqz v2, :cond_10

    const/16 v2, 0x4cf

    goto :goto_10

    :cond_10
    const/16 v2, 0x4d5

    :goto_10
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 53
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->A:Z

    if-eqz v2, :cond_11

    const/16 v2, 0x4cf

    goto :goto_11

    :cond_11
    const/16 v2, 0x4d5

    :goto_11
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 55
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->B:Z

    if-eqz v2, :cond_12

    const/16 v2, 0x4cf

    goto :goto_12

    :cond_12
    const/16 v2, 0x4d5

    :goto_12
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 57
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->C:Z

    if-eqz v2, :cond_13

    const/16 v2, 0x4cf

    goto :goto_13

    :cond_13
    const/16 v2, 0x4d5

    :goto_13
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 59
    iget v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->D:I

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 61
    iget-object v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->E:Lcom/pspdfkit/configuration/search/SearchConfiguration;

    if-nez v2, :cond_14

    goto :goto_14

    :cond_14
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_14
    xor-int/2addr v0, v3

    mul-int v0, v0, v1

    .line 63
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->F:Z

    if-eqz v2, :cond_15

    const/16 v2, 0x4cf

    goto :goto_15

    :cond_15
    const/16 v2, 0x4d5

    :goto_15
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 65
    iget-object v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->G:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 67
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->H:Z

    if-eqz v2, :cond_16

    const/16 v2, 0x4cf

    goto :goto_16

    :cond_16
    const/16 v2, 0x4d5

    :goto_16
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 69
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->I:Z

    if-eqz v2, :cond_17

    const/16 v2, 0x4cf

    goto :goto_17

    :cond_17
    const/16 v2, 0x4d5

    :goto_17
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 71
    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->J:Z

    if-eqz v1, :cond_18

    goto :goto_18

    :cond_18
    const/16 v4, 0x4d5

    :goto_18
    xor-int/2addr v0, v4

    return v0
.end method

.method public hideUserInterfaceWhenCreatingAnnotations()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->u:Z

    return v0
.end method

.method public isAnnotationListEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->v:Z

    return v0
.end method

.method public isAnnotationListReorderingEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->w:Z

    return v0
.end method

.method public isAnnotationNoteHintingEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->F:Z

    return v0
.end method

.method public isBookmarkEditingEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->A:Z

    return v0
.end method

.method public isBookmarkListEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->z:Z

    return v0
.end method

.method public isDocumentEditorEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->n:Z

    return v0
.end method

.method public isDocumentInfoViewEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->B:Z

    return v0
.end method

.method public isDocumentInfoViewSeparated()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->C:Z

    return v0
.end method

.method public isImmersiveMode()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->f:Z

    return v0
.end method

.method public isOutlineEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->y:Z

    return v0
.end method

.method public isPrintingEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->s:Z

    return v0
.end method

.method public isReaderViewEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->J:Z

    return v0
.end method

.method public isRedactionUiEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->I:Z

    return v0
.end method

.method public isSearchEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->o:Z

    return v0
.end method

.method public isSettingsItemEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->p:Z

    return v0
.end method

.method public isShowDocumentTitleOverlayEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->j:Z

    return v0
.end method

.method public isShowNavigationButtonsEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->k:Z

    return v0
.end method

.method public isShowPageLabels()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->i:Z

    return v0
.end method

.method public isShowPageNumberOverlay()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->g:Z

    return v0
.end method

.method public isSignatureButtonPositionForcedInMainToolbar()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->h:Z

    return v0
.end method

.method public isThumbnailGridEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->m:Z

    return v0
.end method

.method public isVolumeButtonsNavigationEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->H:Z

    return v0
.end method

.method public page()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->D:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PdfActivityConfiguration{getConfiguration="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->a:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getActivityTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", getLayout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", getTheme="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", getDarkTheme="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isImmersiveMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isShowPageNumberOverlay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isSignatureButtonPositionForcedInMainToolbar="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->h:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isShowPageLabels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isShowDocumentTitleOverlayEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->j:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isShowNavigationButtonsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->k:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getThumbnailBarMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->l:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isThumbnailGridEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->m:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isDocumentEditorEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->n:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isSearchEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->o:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isSettingsItemEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->p:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getSettingsMenuItemShown="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->q:Ljava/util/EnumSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getSearchType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->r:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isPrintingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->s:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getUserInterfaceViewMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->t:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", hideUserInterfaceWhenCreatingAnnotations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->u:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isAnnotationListEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->v:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isAnnotationListReorderingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->w:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getListedAnnotationTypes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->x:Ljava/util/EnumSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isOutlineEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->y:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isBookmarkListEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->z:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isBookmarkEditingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->A:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isDocumentInfoViewEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->B:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isDocumentInfoViewSeparated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->C:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", page="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->D:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", getSearchConfiguration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->E:Lcom/pspdfkit/configuration/search/SearchConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isAnnotationNoteHintingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->F:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", getTabBarHidingMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->G:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isVolumeButtonsNavigationEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->H:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isRedactionUiEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->I:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isReaderViewEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->J:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

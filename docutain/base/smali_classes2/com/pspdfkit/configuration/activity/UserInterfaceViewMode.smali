.class public final enum Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum USER_INTERFACE_VIEW_MODE_AUTOMATIC:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

.field public static final enum USER_INTERFACE_VIEW_MODE_AUTOMATIC_BORDER_PAGES:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

.field public static final enum USER_INTERFACE_VIEW_MODE_HIDDEN:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

.field public static final enum USER_INTERFACE_VIEW_MODE_MANUAL:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

.field public static final enum USER_INTERFACE_VIEW_MODE_VISIBLE:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

.field private static final synthetic a:[Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    const-string v1, "USER_INTERFACE_VIEW_MODE_AUTOMATIC"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_AUTOMATIC:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    .line 6
    new-instance v1, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    const-string v3, "USER_INTERFACE_VIEW_MODE_AUTOMATIC_BORDER_PAGES"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_AUTOMATIC_BORDER_PAGES:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    .line 9
    new-instance v3, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    const-string v5, "USER_INTERFACE_VIEW_MODE_VISIBLE"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_VISIBLE:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    .line 12
    new-instance v5, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    const-string v7, "USER_INTERFACE_VIEW_MODE_HIDDEN"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_HIDDEN:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    .line 19
    new-instance v7, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    const-string v9, "USER_INTERFACE_VIEW_MODE_MANUAL"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_MANUAL:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    const/4 v9, 0x5

    new-array v9, v9, [Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    aput-object v0, v9, v2

    aput-object v1, v9, v4

    aput-object v3, v9, v6

    aput-object v5, v9, v8

    aput-object v7, v9, v10

    .line 20
    sput-object v9, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->a:[Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->a:[Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    invoke-virtual {v0}, [Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    return-object v0
.end method

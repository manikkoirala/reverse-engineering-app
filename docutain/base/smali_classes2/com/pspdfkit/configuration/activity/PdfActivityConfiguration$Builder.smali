.class public Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

.field private E:I

.field private F:I

.field private G:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

.field private H:Z

.field private I:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

.field private J:Z

.field private a:Ljava/lang/String;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:I

.field private n:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field private p:I

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Lcom/pspdfkit/configuration/search/SearchConfiguration;

.field private u:Z

.field private v:I

.field private w:Z

.field private x:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;",
            ">;"
        }
    .end annotation
.end field

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->b:Z

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->c:Z

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->d:Z

    .line 5
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->e:Z

    .line 6
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->f:Z

    .line 11
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->h:Z

    .line 12
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->i:Z

    .line 13
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->j:Z

    .line 14
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->k:Z

    const/4 v1, 0x0

    .line 15
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->l:Z

    .line 17
    sget v2, Lcom/pspdfkit/R$layout;->pspdf__pdf_activity:I

    iput v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->m:I

    .line 20
    sget-object v2, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->DEFAULT_LISTED_ANNOTATION_TYPES:Ljava/util/EnumSet;

    iput-object v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->n:Ljava/util/EnumSet;

    .line 21
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->o:Z

    .line 22
    iput v1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->p:I

    .line 23
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->q:Z

    .line 24
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->r:Z

    .line 25
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->s:Z

    .line 30
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->u:Z

    .line 35
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->w:Z

    .line 36
    const-class v2, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    invoke-static {v2}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->x:Ljava/util/EnumSet;

    .line 37
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->y:Z

    .line 38
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->z:Z

    .line 39
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->A:Z

    .line 40
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->B:Z

    .line 41
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->C:Z

    .line 43
    sget-object v2, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;->AUTOMATIC_HIDE_SINGLE:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    iput-object v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->D:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    const/4 v2, -0x1

    .line 46
    iput v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->E:I

    .line 49
    iput v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->F:I

    .line 52
    sget-object v2, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_FLOATING:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    iput-object v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->G:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    .line 55
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->H:Z

    .line 57
    sget-object v2, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_AUTOMATIC:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    iput-object v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->I:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    .line 60
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->J:Z

    const-string v1, "context"

    .line 68
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    new-instance v1, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-direct {v1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    .line 70
    invoke-static {p1}, Lcom/pspdfkit/internal/e8;->f(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :cond_0
    iput v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->v:I

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 2

    .line 71
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getTheme()I

    move-result v0

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getDarkTheme()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;II)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;I)V
    .locals 1

    const/4 v0, -0x1

    .line 72
    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;II)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;II)V
    .locals 3

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 74
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->b:Z

    .line 75
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->c:Z

    .line 76
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->d:Z

    .line 77
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->e:Z

    .line 78
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->f:Z

    .line 83
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->h:Z

    .line 84
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->i:Z

    .line 85
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->j:Z

    .line 86
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->k:Z

    const/4 v1, 0x0

    .line 87
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->l:Z

    .line 89
    sget v2, Lcom/pspdfkit/R$layout;->pspdf__pdf_activity:I

    iput v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->m:I

    .line 92
    sget-object v2, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->DEFAULT_LISTED_ANNOTATION_TYPES:Ljava/util/EnumSet;

    iput-object v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->n:Ljava/util/EnumSet;

    .line 93
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->o:Z

    .line 94
    iput v1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->p:I

    .line 95
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->q:Z

    .line 96
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->r:Z

    .line 97
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->s:Z

    .line 102
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->u:Z

    .line 107
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->w:Z

    .line 108
    const-class v2, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    invoke-static {v2}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->x:Ljava/util/EnumSet;

    .line 109
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->y:Z

    .line 110
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->z:Z

    .line 111
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->A:Z

    .line 112
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->B:Z

    .line 113
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->C:Z

    .line 115
    sget-object v2, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;->AUTOMATIC_HIDE_SINGLE:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    iput-object v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->D:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    const/4 v2, -0x1

    .line 118
    iput v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->E:I

    .line 121
    iput v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->F:I

    .line 124
    sget-object v2, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_FLOATING:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    iput-object v2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->G:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    .line 127
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->H:Z

    .line 129
    sget-object v0, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;->USER_INTERFACE_VIEW_MODE_AUTOMATIC:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    iput-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->I:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    .line 132
    iput-boolean v1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->J:Z

    .line 175
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getActivityTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->a:Ljava/lang/String;

    .line 176
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isAnnotationListEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->b:Z

    .line 177
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isAnnotationListReorderingEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->c:Z

    .line 178
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isAnnotationNoteHintingEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->d:Z

    .line 179
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isBookmarkEditingEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->e:Z

    .line 180
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isBookmarkListEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->f:Z

    .line 181
    new-instance v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/PdfConfiguration;)V

    iput-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    .line 182
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentEditorEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->h:Z

    .line 183
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentInfoViewEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->i:Z

    .line 184
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentInfoViewSeparated()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->j:Z

    .line 185
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->hideUserInterfaceWhenCreatingAnnotations()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->k:Z

    .line 186
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isImmersiveMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->l:Z

    .line 187
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getLayout()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->m:I

    .line 188
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getListedAnnotationTypes()Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->n:Ljava/util/EnumSet;

    .line 189
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isOutlineEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->o:Z

    .line 190
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->page()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->p:I

    .line 191
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isPrintingEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->q:Z

    .line 192
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isReaderViewEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->r:Z

    .line 193
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isRedactionUiEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->s:Z

    .line 194
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSearchConfiguration()Lcom/pspdfkit/configuration/search/SearchConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->t:Lcom/pspdfkit/configuration/search/SearchConfiguration;

    .line 195
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isSearchEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->u:Z

    .line 196
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSearchType()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->v:I

    .line 197
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isSettingsItemEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->w:Z

    .line 198
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSettingsMenuItemShown()Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->x:Ljava/util/EnumSet;

    .line 199
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowDocumentTitleOverlayEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->y:Z

    .line 200
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowNavigationButtonsEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->z:Z

    .line 201
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageLabels()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->A:Z

    .line 202
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageNumberOverlay()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->B:Z

    .line 204
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isSignatureButtonPositionForcedInMainToolbar()Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->C:Z

    .line 205
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getTabBarHidingMode()Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->D:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    .line 206
    iput p2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->E:I

    .line 207
    iput p3, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->F:I

    .line 208
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getThumbnailBarMode()Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->G:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    .line 209
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isThumbnailGridEnabled()Z

    move-result p2

    iput-boolean p2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->H:Z

    .line 210
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getUserInterfaceViewMode()Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->I:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    .line 211
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isVolumeButtonsNavigationEnabled()Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->J:Z

    return-void
.end method


# virtual methods
.method public allowMultipleBookmarksPerPage(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->allowMultipleBookmarksPerPage(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public animateScrollOnEdgeTaps(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->animateScrollOnEdgeTaps(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public annotationReplyFeatures(Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "annotationReplyFeatures"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->annotationReplyFeatures(Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public automaticallyGenerateLinks(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->automaticallyGenerateLinks(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public autosaveEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->autosaveEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public backgroundColor(I)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->backgroundColor(I)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public build()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
    .locals 39

    move-object/from16 v0, p0

    .line 1
    iget-object v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->build()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v8

    .line 2
    iget-object v13, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->a:Ljava/lang/String;

    iget v3, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->m:I

    iget v4, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->E:I

    iget v5, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->F:I

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->l:Z

    move/from16 v16, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->B:Z

    move/from16 v17, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->C:Z

    move/from16 v18, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->A:Z

    move/from16 v19, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->y:Z

    move/from16 v20, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->z:Z

    move/from16 v21, v1

    iget-object v10, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->G:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->H:Z

    move/from16 v22, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->h:Z

    move/from16 v23, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->u:Z

    move/from16 v24, v1

    iget v6, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->v:I

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->q:Z

    move/from16 v26, v1

    iget-object v11, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->I:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->k:Z

    move/from16 v27, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->b:Z

    move/from16 v28, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->c:Z

    move/from16 v29, v1

    iget-object v15, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->n:Ljava/util/EnumSet;

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->f:Z

    move/from16 v31, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->e:Z

    move/from16 v32, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->o:Z

    move/from16 v30, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->i:Z

    move/from16 v33, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->j:Z

    move/from16 v34, v1

    iget v7, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->p:I

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->w:Z

    move/from16 v25, v1

    iget-object v14, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->x:Ljava/util/EnumSet;

    iget-object v12, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->t:Lcom/pspdfkit/configuration/search/SearchConfiguration;

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->d:Z

    move/from16 v35, v1

    iget-object v9, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->D:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->J:Z

    move/from16 v36, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->s:Z

    move/from16 v37, v1

    iget-boolean v1, v0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->r:Z

    move/from16 v38, v1

    sget v1, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->SEARCH_INLINE:I

    .line 3
    new-instance v1, Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;

    move-object v2, v1

    invoke-direct/range {v2 .. v38}, Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;-><init>(IIIIILcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/configuration/activity/TabBarHidingMode;Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;Lcom/pspdfkit/configuration/search/SearchConfiguration;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/EnumSet;ZZZZZZZZZZZZZZZZZZZZZZZ)V

    return-object v1
.end method

.method public configuration(Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "configuration"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-direct {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;-><init>(Lcom/pspdfkit/configuration/PdfConfiguration;)V

    iput-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public defaultSigner(Ljava/lang/String;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->defaultSigner(Ljava/lang/String;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public disableAnnotationEditing()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->disableAnnotationEditing()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public disableAnnotationLimitedToPageBounds()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->disableAnnotationLimitedToPageBounds()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public disableAnnotationList()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->b:Z

    return-object p0
.end method

.method public disableAnnotationRotation()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->disableAnnotationRotation()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public disableBookmarkEditing()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->e:Z

    return-object p0
.end method

.method public disableBookmarkList()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->f:Z

    return-object p0
.end method

.method public disableContentEditing()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->disableContentEditing()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public disableCopyPaste()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->disableCopyPaste()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public disableDocumentEditor()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->h:Z

    return-object p0
.end method

.method public disableDocumentInfoView()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->i:Z

    return-object p0
.end method

.method public disableFormEditing()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->disableFormEditing()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public disableOutline()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->o:Z

    return-object p0
.end method

.method public disablePrinting()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->q:Z

    return-object p0
.end method

.method public disableSearch()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->u:Z

    return-object p0
.end method

.method public editableAnnotationTypes(Ljava/util/List;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)",
            "Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->editableAnnotationTypes(Ljava/util/List;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public enableAnnotationEditing()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->enableAnnotationEditing()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public enableAnnotationList()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->b:Z

    return-object p0
.end method

.method public enableAnnotationRotation()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->enableAnnotationRotation()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public enableBookmarkEditing()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->e:Z

    return-object p0
.end method

.method public enableBookmarkList()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->f:Z

    return-object p0
.end method

.method public enableContentEditing()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->enableContentEditing()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public enableCopyPaste()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->enableCopyPaste()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public enableDocumentEditor()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->h:Z

    return-object p0
.end method

.method public enableDocumentInfoView()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->i:Z

    return-object p0
.end method

.method public enableFormEditing()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->enableFormEditing()Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public enableMagnifier(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->enableMagnifier(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public enableOutline()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->o:Z

    return-object p0
.end method

.method public enablePrinting()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->q:Z

    return-object p0
.end method

.method public enableReaderView(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    if-eqz p1, :cond_1

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->READER_VIEW:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Your current license doesn\'t allow using the reader view."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 4
    :cond_1
    :goto_0
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->r:Z

    return-object p0
.end method

.method public enableSearch()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->u:Z

    return-object p0
.end method

.method public enabledAnnotationTools(Ljava/util/List;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            ">;)",
            "Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->enabledAnnotationTools(Ljava/util/List;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public excludedAnnotationTypes(Ljava/util/List;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)",
            "Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;"
        }
    .end annotation

    const-string v0, "excludedAnnotationTypes"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->excludedAnnotationTypes(Ljava/util/List;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public firstPageAlwaysSingle(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->firstPageAlwaysSingle(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public fitMode(Lcom/pspdfkit/configuration/page/PageFitMode;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "mode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->fitMode(Lcom/pspdfkit/configuration/page/PageFitMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public forceSignatureButtonPositionInMainToolbar(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    if-eqz p1, :cond_1

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Creating signature annotations requires Electronic Signatures."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 5
    :cond_1
    :goto_0
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->C:Z

    return-object p0
.end method

.method public hideDocumentTitleOverlay()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->y:Z

    return-object p0
.end method

.method public hideNavigationButtons()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->z:Z

    return-object p0
.end method

.method public hidePageLabels()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->A:Z

    return-object p0
.end method

.method public hidePageNumberOverlay()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->B:Z

    return-object p0
.end method

.method public hideSettingsMenu()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->w:Z

    return-object p0
.end method

.method public hideThumbnailGrid()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->H:Z

    return-object p0
.end method

.method public hideUserInterfaceWhenCreatingAnnotations(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->k:Z

    return-object p0
.end method

.method public invertColors(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->invertColors(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public layout(I)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->m:I

    return-object p0
.end method

.method public layoutMode(Lcom/pspdfkit/configuration/page/PageLayoutMode;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "mode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->layoutMode(Lcom/pspdfkit/configuration/page/PageLayoutMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public listedAnnotationTypes(Ljava/util/EnumSet;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)",
            "Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;"
        }
    .end annotation

    const-string v0, "listedAnnotationTypes"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->n:Ljava/util/EnumSet;

    return-object p0
.end method

.method public loadingProgressDrawable(Ljava/lang/Integer;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->loadingProgressDrawable(Ljava/lang/Integer;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public maxZoomScale(F)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->maxZoomScale(F)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public memoryCacheSize(I)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->memoryCacheSize(I)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public page(I)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->p:I

    return-object p0
.end method

.method public pagePadding(I)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->pagePadding(I)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public redoEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->redoEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public restoreLastViewedPage(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->restoreLastViewedPage(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public scrollDirection(Lcom/pspdfkit/configuration/page/PageScrollDirection;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "orientation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->scrollDirection(Lcom/pspdfkit/configuration/page/PageScrollDirection;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public scrollMode(Lcom/pspdfkit/configuration/page/PageScrollMode;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "mode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->scrollMode(Lcom/pspdfkit/configuration/page/PageScrollMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public scrollOnEdgeTapEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->scrollOnEdgeTapEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public scrollOnEdgeTapMargin(I)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->scrollOnEdgeTapMargin(I)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public scrollbarsEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->scrollbarsEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public searchConfiguration(Lcom/pspdfkit/configuration/search/SearchConfiguration;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "searchConfiguration"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->t:Lcom/pspdfkit/configuration/search/SearchConfiguration;

    return-object p0
.end method

.method public setAnnotationInspectorEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->setAnnotationInspectorEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public setAnnotationListReorderingEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->c:Z

    return-object p0
.end method

.method public setAnnotationNoteHintingEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->d:Z

    return-object p0
.end method

.method public setDocumentInfoViewSeparated(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->j:Z

    return-object p0
.end method

.method public setEnableNoteAnnotationNoZoomHandling(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->setEnableNoteAnnotationNoZoomHandling(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public setEnabledCopyPasteFeatures(Ljava/util/EnumSet;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;",
            ">;)",
            "Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;"
        }
    .end annotation

    const-string v0, "enabledFeatures"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->setEnabledCopyPasteFeatures(Ljava/util/EnumSet;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public setEnabledShareFeatures(Ljava/util/EnumSet;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/sharing/ShareFeatures;",
            ">;)",
            "Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->setEnabledShareFeatures(Ljava/util/EnumSet;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public setJavaScriptEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->setJavaScriptEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public setMeasurementToolsEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->setMeasurementToolsEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public setMultithreadedRenderingEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->setMultithreadedRenderingEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public setRedactionUiEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->s:Z

    return-object p0
.end method

.method public setResizeGuideLineIntervals(Ljava/util/List;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;)",
            "Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;"
        }
    .end annotation

    const-string v0, "intervals"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    rem-int/2addr v0, v1

    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->setResizeGuideLineIntervals(Ljava/util/List;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "intervals must contain at least 2 elements and an even number. Found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 60
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setResizeGuideSnapAllowance(F)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->setResizeGuideSnapAllowance(F)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public setSearchType(I)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->v:I

    return-object p0
.end method

.method public setSelectedAnnotationResizeEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->setSelectedAnnotationResizeEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public setSelectedAnnotationResizeGuidesEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->setSelectedAnnotationResizeGuidesEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public setSelectedFreetextAnnotationFontScalingOnResizeEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->setSelectedAnnotationFontScalingOnResizeEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public setSettingsMenuItems(Ljava/util/EnumSet;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;",
            ">;)",
            "Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;"
        }
    .end annotation

    const-string v0, "settingsMenuItemShown"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->x:Ljava/util/EnumSet;

    .line 55
    invoke-virtual {p1}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->w:Z

    return-object p0
.end method

.method public setSignaturePickerOrientation(Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "orientation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->setSignaturePickerOrientation(Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public setTabBarHidingMode(Lcom/pspdfkit/configuration/activity/TabBarHidingMode;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "tabBarHidingMode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->D:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    return-object p0
.end method

.method public setThumbnailBarMode(Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "thumbnailBarMode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->G:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    return-object p0
.end method

.method public setUserInterfaceViewMode(Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "userInterfaceMode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->I:Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    return-object p0
.end method

.method public setVolumeButtonsNavigationEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->J:Z

    return-object p0
.end method

.method public showDocumentTitleOverlay()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->y:Z

    return-object p0
.end method

.method public showGapBetweenPages(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->showGapBetweenPages(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public showNavigationButtons()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->z:Z

    return-object p0
.end method

.method public showPageLabels()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->A:Z

    return-object p0
.end method

.method public showPageNumberOverlay()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->B:Z

    return-object p0
.end method

.method public showSettingsMenu()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->x:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->w:Z

    :cond_0
    return-object p0
.end method

.method public showSignHereOverlay(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->showSignHereOverlay(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public showThumbnailGrid()Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->H:Z

    return-object p0
.end method

.method public signatureAppearance(Lcom/pspdfkit/signatures/SignatureAppearance;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->signatureAppearance(Lcom/pspdfkit/signatures/SignatureAppearance;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public signatureCertificateSelectionMode(Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "signatureCertificateSelectionMode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->signatureCertificateSelectionMode(Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public signatureColorOptions(Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "signatureColorOptions"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->signatureColorOptions(Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public signatureCreationModes(Ljava/util/List;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;",
            ">;)",
            "Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->signatureCreationModes(Ljava/util/List;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public signatureSavingStrategy(Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "signatureSavingStrategy"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->signatureSavingStrategy(Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public startZoomScale(F)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->startZoomScale(F)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public textSelectionEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->textSelectionEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public textSelectionPopupToolbarEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->textSelectionPopupToolbarEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public theme(I)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->E:I

    return-object p0
.end method

.method public themeDark(I)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->F:I

    return-object p0
.end method

.method public themeMode(Lcom/pspdfkit/configuration/theming/ThemeMode;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 2

    const-string v0, "mode"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->themeMode(Lcom/pspdfkit/configuration/theming/ThemeMode;)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    .line 55
    sget-object v0, Lcom/pspdfkit/configuration/theming/ThemeMode;->DEFAULT:Lcom/pspdfkit/configuration/theming/ThemeMode;

    if-ne p1, v0, :cond_0

    .line 56
    iget-object p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->invertColors(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    goto :goto_0

    .line 57
    :cond_0
    sget-object v0, Lcom/pspdfkit/configuration/theming/ThemeMode;->NIGHT:Lcom/pspdfkit/configuration/theming/ThemeMode;

    if-ne p1, v0, :cond_1

    .line 58
    iget-object p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->invertColors(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    :cond_1
    :goto_0
    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->a:Ljava/lang/String;

    return-object p0
.end method

.method public toGrayscale(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->toGrayscale(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public undoEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->undoEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public useImmersiveMode(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->l:Z

    return-object p0
.end method

.method public videoPlaybackEnabled(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->videoPlaybackEnabled(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

.method public zoomOutBounce(Z)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration$Builder;->g:Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/configuration/PdfConfiguration$Builder;->zoomOutBounce(Z)Lcom/pspdfkit/configuration/PdfConfiguration$Builder;

    return-object p0
.end method

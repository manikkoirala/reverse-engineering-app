.class final Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;
.super Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration$1;

    invoke-direct {v0}, Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration$1;-><init>()V

    sput-object v0, Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IIIIILcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/configuration/activity/TabBarHidingMode;Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;Lcom/pspdfkit/configuration/search/SearchConfiguration;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/EnumSet;ZZZZZZZZZZZZZZZZZZZZZZZ)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p36}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;-><init>(IIIIILcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/configuration/activity/TabBarHidingMode;Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;Lcom/pspdfkit/configuration/search/SearchConfiguration;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/EnumSet;ZZZZZZZZZZZZZZZZZZZZZZZ)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->getActivityTitle()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->getActivityTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 8
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->getLayout()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->getTheme()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 10
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->getDarkTheme()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 11
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isImmersiveMode()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isShowPageNumberOverlay()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 13
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isSignatureButtonPositionForcedInMainToolbar()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 14
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isShowPageLabels()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 15
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isShowDocumentTitleOverlayEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 16
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isShowNavigationButtonsEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 17
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->getThumbnailBarMode()Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isThumbnailGridEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 19
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isDocumentEditorEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 20
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isSearchEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 21
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isSettingsItemEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 22
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->getSettingsMenuItemShown()Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 23
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->getSearchType()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 24
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isPrintingEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 25
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->getUserInterfaceViewMode()Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 26
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->hideUserInterfaceWhenCreatingAnnotations()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 27
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isAnnotationListEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 28
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isAnnotationListReorderingEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 29
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->getListedAnnotationTypes()Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 30
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isOutlineEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 31
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isBookmarkListEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 32
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isBookmarkEditingEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 33
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isDocumentInfoViewEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 34
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isDocumentInfoViewSeparated()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 35
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->page()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 36
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->getSearchConfiguration()Lcom/pspdfkit/configuration/search/SearchConfiguration;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 37
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isAnnotationNoteHintingEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 38
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->getTabBarHidingMode()Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isVolumeButtonsNavigationEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 40
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isRedactionUiEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 41
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/activity/$AutoValue_PdfActivityConfiguration;->isReaderViewEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

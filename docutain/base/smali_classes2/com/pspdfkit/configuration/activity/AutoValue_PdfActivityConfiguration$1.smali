.class Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;
    .locals 40

    move-object/from16 v0, p1

    .line 2
    new-instance v37, Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;

    .line 3
    const-class v1, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 4
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move-object v11, v1

    .line 5
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 8
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    const/4 v7, 0x1

    if-ne v4, v7, :cond_1

    const/4 v14, 0x1

    goto :goto_1

    :cond_1
    const/4 v14, 0x0

    .line 9
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v7, :cond_2

    const/4 v15, 0x1

    goto :goto_2

    :cond_2
    const/4 v15, 0x0

    .line 10
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v7, :cond_3

    const/16 v16, 0x1

    goto :goto_3

    :cond_3
    const/16 v16, 0x0

    .line 11
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v7, :cond_4

    const/16 v17, 0x1

    goto :goto_4

    :cond_4
    const/16 v17, 0x0

    .line 12
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v7, :cond_5

    const/16 v18, 0x1

    goto :goto_5

    :cond_5
    const/16 v18, 0x0

    .line 13
    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v7, :cond_6

    const/16 v19, 0x1

    goto :goto_6

    :cond_6
    const/16 v19, 0x0

    .line 14
    :goto_6
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    const-class v8, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    invoke-static {v8, v4}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v4

    move-object v8, v4

    check-cast v8, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    .line 15
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v7, :cond_7

    const/16 v20, 0x1

    goto :goto_7

    :cond_7
    const/16 v20, 0x0

    .line 16
    :goto_7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v7, :cond_8

    const/16 v21, 0x1

    goto :goto_8

    :cond_8
    const/16 v21, 0x0

    .line 17
    :goto_8
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v7, :cond_9

    const/16 v22, 0x1

    goto :goto_9

    :cond_9
    const/16 v22, 0x0

    .line 18
    :goto_9
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v7, :cond_a

    const/16 v23, 0x1

    goto :goto_a

    :cond_a
    const/16 v23, 0x0

    .line 19
    :goto_a
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v4

    move-object v12, v4

    check-cast v12, Ljava/util/EnumSet;

    .line 20
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 21
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-ne v9, v7, :cond_b

    const/16 v24, 0x1

    goto :goto_b

    :cond_b
    const/16 v24, 0x0

    .line 22
    :goto_b
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    const-class v10, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    invoke-static {v10, v9}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v9

    check-cast v9, Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;

    .line 23
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-ne v10, v7, :cond_c

    const/16 v25, 0x1

    goto :goto_c

    :cond_c
    const/16 v25, 0x0

    .line 24
    :goto_c
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-ne v10, v7, :cond_d

    const/16 v26, 0x1

    goto :goto_d

    :cond_d
    const/16 v26, 0x0

    .line 25
    :goto_d
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-ne v10, v7, :cond_e

    const/16 v27, 0x1

    goto :goto_e

    :cond_e
    const/16 v27, 0x0

    .line 26
    :goto_e
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v10

    move-object v13, v10

    check-cast v13, Ljava/util/EnumSet;

    .line 27
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-ne v10, v7, :cond_f

    const/16 v28, 0x1

    goto :goto_f

    :cond_f
    const/16 v28, 0x0

    .line 28
    :goto_f
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-ne v10, v7, :cond_10

    const/16 v29, 0x1

    goto :goto_10

    :cond_10
    const/16 v29, 0x0

    .line 29
    :goto_10
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-ne v10, v7, :cond_11

    const/16 v30, 0x1

    goto :goto_11

    :cond_11
    const/16 v30, 0x0

    .line 30
    :goto_11
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-ne v10, v7, :cond_12

    const/16 v31, 0x1

    goto :goto_12

    :cond_12
    const/16 v31, 0x0

    .line 31
    :goto_12
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-ne v10, v7, :cond_13

    const/16 v32, 0x1

    goto :goto_13

    :cond_13
    const/16 v32, 0x0

    .line 32
    :goto_13
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 33
    const-class v33, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual/range {v33 .. v33}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v5

    move-object/from16 v33, v5

    check-cast v33, Lcom/pspdfkit/configuration/search/SearchConfiguration;

    .line 34
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-ne v5, v7, :cond_14

    const/16 v35, 0x1

    goto :goto_14

    :cond_14
    const/16 v35, 0x0

    .line 35
    :goto_14
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    const-class v7, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    invoke-static {v7, v5}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v5

    move-object v7, v5

    check-cast v7, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    .line 36
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    const/4 v0, 0x1

    if-ne v5, v0, :cond_15

    const/16 v36, 0x1

    goto :goto_15

    :cond_15
    const/16 v36, 0x0

    .line 37
    :goto_15
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-ne v5, v0, :cond_16

    const/16 v38, 0x1

    goto :goto_16

    :cond_16
    const/16 v38, 0x0

    .line 38
    :goto_16
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-ne v5, v0, :cond_17

    const/16 v39, 0x1

    goto :goto_17

    :cond_17
    const/16 v39, 0x0

    :goto_17
    move-object/from16 v0, v37

    move v5, v10

    move-object/from16 v10, v33

    move/from16 v33, v35

    move/from16 v34, v36

    move/from16 v35, v38

    move/from16 v36, v39

    invoke-direct/range {v0 .. v36}, Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;-><init>(IIIIILcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/configuration/activity/TabBarHidingMode;Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;Lcom/pspdfkit/configuration/activity/UserInterfaceViewMode;Lcom/pspdfkit/configuration/search/SearchConfiguration;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/EnumSet;ZZZZZZZZZZZZZZZZZZZZZZZ)V

    return-object v37
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration$1;->createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;
    .locals 0

    .line 2
    new-array p1, p1, [Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration$1;->newArray(I)[Lcom/pspdfkit/configuration/activity/AutoValue_PdfActivityConfiguration;

    move-result-object p1

    return-object p1
.end method

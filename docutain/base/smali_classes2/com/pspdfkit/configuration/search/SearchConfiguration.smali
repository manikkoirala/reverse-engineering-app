.class public abstract Lcom/pspdfkit/configuration/search/SearchConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_SNIPPET_LENGTH:I = 0x50

.field public static final DEFAULT_START_SEARCH_CHARS:I = 0x2

.field public static final DEFAULT_START_SEARCH_ON_CURRENT_PAGE:Z = false

.field public static final UNLIMITED_SEARCH_RESULTS:I = 0x7fffffff


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getMaxSearchResults()Ljava/lang/Integer;
.end method

.method public abstract getSnippetLength()I
.end method

.method public abstract getStartSearchChars()I
.end method

.method public abstract isStartSearchOnCurrentPage()Z
.end method

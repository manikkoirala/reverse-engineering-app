.class abstract Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;
.super Lcom/pspdfkit/configuration/search/SearchConfiguration;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Z

.field private final d:Ljava/lang/Integer;


# direct methods
.method constructor <init>(IIZLjava/lang/Integer;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/configuration/search/SearchConfiguration;-><init>()V

    .line 2
    iput p1, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->a:I

    .line 3
    iput p2, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->b:I

    .line 4
    iput-boolean p3, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->c:Z

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->d:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/configuration/search/SearchConfiguration;

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    .line 2
    check-cast p1, Lcom/pspdfkit/configuration/search/SearchConfiguration;

    .line 3
    iget v1, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->a:I

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/search/SearchConfiguration;->getStartSearchChars()I

    move-result v3

    if-ne v1, v3, :cond_2

    iget v1, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->b:I

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/search/SearchConfiguration;->getSnippetLength()I

    move-result v3

    if-ne v1, v3, :cond_2

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->c:Z

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/search/SearchConfiguration;->isStartSearchOnCurrentPage()Z

    move-result v3

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->d:Ljava/lang/Integer;

    if-nez v1, :cond_1

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/search/SearchConfiguration;->getMaxSearchResults()Ljava/lang/Integer;

    move-result-object p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/search/SearchConfiguration;->getMaxSearchResults()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    return v2
.end method

.method public getMaxSearchResults()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->d:Ljava/lang/Integer;

    return-object v0
.end method

.method public getSnippetLength()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->b:I

    return v0
.end method

.method public getStartSearchChars()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->a:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->a:I

    const v1, 0xf4243

    xor-int/2addr v0, v1

    mul-int v0, v0, v1

    .line 3
    iget v2, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->b:I

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 5
    iget-boolean v2, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->c:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x4cf

    goto :goto_0

    :cond_0
    const/16 v2, 0x4d5

    :goto_0
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->d:Ljava/lang/Integer;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    :goto_1
    xor-int/2addr v0, v1

    return v0
.end method

.method public isStartSearchOnCurrentPage()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->c:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SearchConfiguration{startSearchChars="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", snippetLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", startSearchOnCurrentPage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", maxSearchResults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->d:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

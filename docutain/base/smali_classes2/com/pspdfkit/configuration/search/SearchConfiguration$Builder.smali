.class public Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/configuration/search/SearchConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 4
    iput v0, p0, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;->a:I

    const/16 v0, 0x50

    .line 7
    iput v0, p0, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;->b:I

    const/4 v0, 0x0

    .line 10
    iput-boolean v0, p0, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;->c:Z

    return-void
.end method


# virtual methods
.method public build()Lcom/pspdfkit/configuration/search/SearchConfiguration;
    .locals 5

    .line 1
    iget v0, p0, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;->a:I

    iget v1, p0, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;->b:I

    iget-boolean v2, p0, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;->c:Z

    iget-object v3, p0, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;->d:Ljava/lang/Integer;

    .line 2
    new-instance v4, Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;-><init>(IIZLjava/lang/Integer;)V

    return-object v4
.end method

.method public maxSearchResults(I)Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;->d:Ljava/lang/Integer;

    return-object p0
.end method

.method public setSnippetLength(I)Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;->b:I

    return-object p0
.end method

.method public setStartSearchChars(I)Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;->a:I

    return-object p0
.end method

.method public setStartSearchOnCurrentPage(Z)Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;->c:Z

    return-object p0
.end method

.class final Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;
.super Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration$1;

    invoke-direct {v0}, Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration$1;-><init>()V

    sput-object v0, Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IIZLjava/lang/Integer;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;-><init>(IIZLjava/lang/Integer;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->getStartSearchChars()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->getSnippetLength()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->isStartSearchOnCurrentPage()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->getMaxSearchResults()Ljava/lang/Integer;

    move-result-object p2

    if-nez p2, :cond_0

    const/4 p2, 0x1

    .line 5
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 7
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/search/$AutoValue_SearchConfiguration;->getMaxSearchResults()Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    return-void
.end method

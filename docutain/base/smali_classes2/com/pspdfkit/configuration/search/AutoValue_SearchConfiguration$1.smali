.class Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;
    .locals 5

    .line 2
    new-instance v0, Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;

    .line 3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 5
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    .line 6
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    invoke-direct {v0, v1, v2, v4, p1}, Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;-><init>(IIZLjava/lang/Integer;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration$1;->createFromParcel(Landroid/os/Parcel;)Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;
    .locals 0

    .line 2
    new-array p1, p1, [Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration$1;->newArray(I)[Lcom/pspdfkit/configuration/search/AutoValue_SearchConfiguration;

    move-result-object p1

    return-object p1
.end method

.class public abstract Lcom/pspdfkit/configuration/PdfConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/configuration/PdfConfiguration$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_LOADING_PROGRESS_DRAWABLE:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/high16 v0, -0x80000000

    .line 1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/configuration/PdfConfiguration;->DEFAULT_LOADING_PROGRESS_DRAWABLE:Ljava/lang/Integer;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/utils/Size;",
            ">;"
        }
    .end annotation
.end method

.method public abstract allowMultipleBookmarksPerPage()Z
.end method

.method public abstract animateScrollOnEdgeTaps()Z
.end method

.method public abstract getAnnotationReplyFeatures()Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;
.end method

.method public abstract getBackgroundColor()I
.end method

.method public abstract getDefaultSigner()Ljava/lang/String;
.end method

.method public abstract getEditableAnnotationTypes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEnabledAnnotationTools()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEnabledCopyPasteFeatures()Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEnabledShareFeatures()Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/sharing/ShareFeatures;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getExcludedAnnotationTypes()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFitMode()Lcom/pspdfkit/configuration/page/PageFitMode;
.end method

.method public abstract getFixedLowResRenderPixelCount()Ljava/lang/Integer;
.end method

.method public abstract getGuideLineIntervals()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLayoutMode()Lcom/pspdfkit/configuration/page/PageLayoutMode;
.end method

.method public abstract getLoadingProgressDrawable()Ljava/lang/Integer;
.end method

.method public abstract getMaxZoomScale()F
.end method

.method public abstract getMemoryCacheSize()I
.end method

.method public getMinimumAnnotationSize(Ljava/lang/Class;)Lcom/pspdfkit/utils/Size;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/pspdfkit/annotations/ResizableAnnotation;",
            ">;)",
            "Lcom/pspdfkit/utils/Size;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/PdfConfiguration;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    return-object p1
.end method

.method public abstract getPagePadding()I
.end method

.method public abstract getResizeGuideSnapAllowance()F
.end method

.method public abstract getScrollDirection()Lcom/pspdfkit/configuration/page/PageScrollDirection;
.end method

.method public abstract getScrollMode()Lcom/pspdfkit/configuration/page/PageScrollMode;
.end method

.method public abstract getSelectedAnnotationFontScalingOnResizeEnabled()Z
.end method

.method public abstract getSelectedAnnotationResizeEnabled()Z
.end method

.method public abstract getSelectedAnnotationResizeGuidesEnabled()Z
.end method

.method public abstract getSignatureAppearance()Lcom/pspdfkit/signatures/SignatureAppearance;
.end method

.method public abstract getSignatureCertificateSelectionMode()Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;
.end method

.method public abstract getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;
.end method

.method public abstract getSignatureCreationModes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSignaturePickerOrientation()Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;
.end method

.method public abstract getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;
.end method

.method public abstract getStartZoomScale()F
.end method

.method public abstract getThemeMode()Lcom/pspdfkit/configuration/theming/ThemeMode;
.end method

.method public abstract isAnnotationEditingEnabled()Z
.end method

.method public abstract isAnnotationInspectorEnabled()Z
.end method

.method public abstract isAnnotationLimitedToPageBounds()Z
.end method

.method public abstract isAnnotationRotationEnabled()Z
.end method

.method public abstract isAutoSelectNextFormElementEnabled()Z
.end method

.method public abstract isAutomaticLinkGenerationEnabled()Z
.end method

.method public abstract isAutosaveEnabled()Z
.end method

.method public abstract isContentEditingEnabled()Z
.end method

.method public abstract isCopyPasteEnabled()Z
.end method

.method public abstract isFirstPageAlwaysSingle()Z
.end method

.method public abstract isFormEditingEnabled()Z
.end method

.method public abstract isInvertColors()Z
.end method

.method public abstract isJavaScriptEnabled()Z
.end method

.method public abstract isLastViewedPageRestorationEnabled()Z
.end method

.method public abstract isMagnifierEnabled()Z
.end method

.method public abstract isMeasurementsEnabled()Z
.end method

.method public abstract isMultithreadedRenderingEnabled()Z
.end method

.method public abstract isNoteAnnotationNoZoomHandlingEnabled()Z
.end method

.method public abstract isPlayingMultipleMediaInstancesEnabled()Z
.end method

.method public abstract isRedoEnabled()Z
.end method

.method public abstract isScrollbarsEnabled()Z
.end method

.method public abstract isTextSelectionEnabled()Z
.end method

.method public abstract isTextSelectionPopupToolbarEnabled()Z
.end method

.method public abstract isToGrayscale()Z
.end method

.method public abstract isUndoEnabled()Z
.end method

.method public abstract isVideoPlaybackEnabled()Z
.end method

.method public abstract scrollOnEdgeTapEnabled()Z
.end method

.method public abstract scrollOnEdgeTapMargin()I
.end method

.method public abstract shouldZoomOutBounce()Z
.end method

.method public abstract showGapBetweenPages()Z
.end method

.method public abstract showSignHereOverlay()Z
.end method

.class public final Lcom/pspdfkit/datastructures/TextBlock;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/pspdfkit/datastructures/TextBlock;",
        ">;"
    }
.end annotation


# instance fields
.field public final annotation:Lcom/pspdfkit/annotations/Annotation;

.field public final pageIndex:I

.field public final pageRects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field public final range:Lcom/pspdfkit/datastructures/Range;

.field public final text:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;ILcom/pspdfkit/datastructures/Range;Ljava/util/List;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/pspdfkit/datastructures/Range;",
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 7
    iput-object p1, p0, Lcom/pspdfkit/datastructures/TextBlock;->text:Ljava/lang/String;

    .line 8
    iput p2, p0, Lcom/pspdfkit/datastructures/TextBlock;->pageIndex:I

    .line 9
    iput-object p3, p0, Lcom/pspdfkit/datastructures/TextBlock;->range:Lcom/pspdfkit/datastructures/Range;

    .line 10
    invoke-static {p4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/datastructures/TextBlock;->pageRects:Ljava/util/List;

    .line 11
    iput-object p5, p0, Lcom/pspdfkit/datastructures/TextBlock;->annotation:Lcom/pspdfkit/annotations/Annotation;

    return-void

    .line 12
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Provided pageRects list is empty. Cant\'t create a TextBlock without at least one rect."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static create(ILcom/pspdfkit/datastructures/Range;Ljava/util/List;Ljava/lang/String;)Lcom/pspdfkit/datastructures/TextBlock;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/pspdfkit/datastructures/Range;",
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/pspdfkit/datastructures/TextBlock;"
        }
    .end annotation

    const-string v0, "range"

    const-string v1, "argumentName"

    .line 126
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 177
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "pageRects"

    .line 179
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 230
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "text"

    .line 232
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 283
    invoke-static {p3, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "pageRects may not be empty"

    .line 284
    invoke-static {v0, p2}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/String;Ljava/util/Collection;)V

    .line 285
    new-instance v0, Lcom/pspdfkit/datastructures/TextBlock;

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p3

    move v3, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/datastructures/TextBlock;-><init>(Ljava/lang/String;ILcom/pspdfkit/datastructures/Range;Ljava/util/List;Lcom/pspdfkit/annotations/Annotation;)V

    return-object v0
.end method

.method public static create(Lcom/pspdfkit/document/PdfDocument;ILcom/pspdfkit/datastructures/Range;)Lcom/pspdfkit/datastructures/TextBlock;
    .locals 9

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "range"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    invoke-interface {p0}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v0

    const/4 v1, 0x1

    if-ge p1, v0, :cond_0

    .line 116
    invoke-virtual {p2}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v0

    invoke-virtual {p2}, Lcom/pspdfkit/datastructures/Range;->getLength()I

    move-result v2

    invoke-interface {p0, p1, v0, v2, v1}, Lcom/pspdfkit/document/PdfDocument;->getPageTextRects(IIIZ)Ljava/util/List;

    move-result-object v7

    .line 117
    invoke-virtual {p2}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v0

    invoke-virtual {p2}, Lcom/pspdfkit/datastructures/Range;->getLength()I

    move-result v1

    invoke-interface {p0, p1, v0, v1}, Lcom/pspdfkit/document/PdfDocument;->getPageText(III)Ljava/lang/String;

    move-result-object v4

    .line 118
    new-instance p0, Lcom/pspdfkit/datastructures/TextBlock;

    const/4 v8, 0x0

    move-object v3, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v3 .. v8}, Lcom/pspdfkit/datastructures/TextBlock;-><init>(Ljava/lang/String;ILcom/pspdfkit/datastructures/Range;Ljava/util/List;Lcom/pspdfkit/annotations/Annotation;)V

    return-object p0

    .line 119
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    .line 120
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 122
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 123
    invoke-interface {p0}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v2, v1

    const-string p0, "pageIndex %d exceeds document page count of %d."

    .line 124
    invoke-static {v0, p0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p2, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public static create(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/datastructures/Range;)Lcom/pspdfkit/datastructures/TextBlock;
    .locals 7

    const-string v0, "range"

    const-string v1, "argumentName"

    .line 287
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 338
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v3, "annotation"

    .line 340
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 391
    invoke-static {p1, v3, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 393
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 444
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 445
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    if-ltz v0, :cond_7

    .line 449
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-interface {p0}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v3

    if-ge v0, v3, :cond_6

    .line 456
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_4

    .line 458
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->WIDGET:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v3, :cond_4

    .line 459
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/annotations/WidgetAnnotation;

    .line 460
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 461
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/forms/FormType;->TEXT:Lcom/pspdfkit/forms/FormType;

    if-ne v3, v4, :cond_0

    .line 462
    check-cast v0, Lcom/pspdfkit/forms/TextFormElement;

    .line 463
    invoke-virtual {v0}, Lcom/pspdfkit/forms/TextFormElement;->getText()Ljava/lang/String;

    move-result-object p0

    goto :goto_2

    :cond_0
    const/16 v3, 0xa

    if-eqz v0, :cond_2

    .line 464
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/forms/FormType;->COMBOBOX:Lcom/pspdfkit/forms/FormType;

    if-ne v4, v5, :cond_2

    .line 465
    check-cast v0, Lcom/pspdfkit/forms/ComboBoxFormElement;

    .line 466
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 467
    invoke-virtual {v0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getOptions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/forms/FormOption;

    .line 468
    invoke-virtual {v4}, Lcom/pspdfkit/forms/FormOption;->getLabel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 469
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 471
    :cond_1
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_2

    :cond_2
    if-eqz v0, :cond_4

    .line 472
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/forms/FormType;->LISTBOX:Lcom/pspdfkit/forms/FormType;

    if-ne v4, v5, :cond_4

    .line 473
    check-cast v0, Lcom/pspdfkit/forms/ListBoxFormElement;

    .line 474
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 475
    invoke-virtual {v0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getOptions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/forms/FormOption;

    .line 476
    invoke-virtual {v4}, Lcom/pspdfkit/forms/FormOption;->getLabel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 479
    :cond_3
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_4
    :goto_2
    if-eqz p0, :cond_5

    .line 488
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 489
    invoke-virtual {p2}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v0

    invoke-virtual {p2}, Lcom/pspdfkit/datastructures/Range;->getEndPosition()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 490
    new-instance p0, Lcom/pspdfkit/datastructures/TextBlock;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v3

    move-object v1, p0

    move-object v4, p2

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/datastructures/TextBlock;-><init>(Ljava/lang/String;ILcom/pspdfkit/datastructures/Range;Ljava/util/List;Lcom/pspdfkit/annotations/Annotation;)V

    return-object p0

    .line 491
    :cond_5
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 492
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p2

    new-array v0, v2, [Ljava/lang/Object;

    aput-object p1, v0, v1

    const-string p1, "annotation has empty contents: %s"

    invoke-static {p2, p1, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 493
    :cond_6
    new-instance p2, Ljava/lang/IllegalArgumentException;

    .line 494
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 496
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v3, v1

    .line 497
    invoke-interface {p0}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v3, v2

    const-string p0, "annotation pageIndex %d exceeds document page count of %d."

    .line 498
    invoke-static {v0, p0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p2, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 499
    :cond_7
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 500
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p2

    new-array v0, v2, [Ljava/lang/Object;

    aput-object p1, v0, v1

    const-string p1, "annotation is not attached to the document: %s"

    invoke-static {p2, p1, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static getTextForBlocks(Lcom/pspdfkit/document/PdfDocument;Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/document/PdfDocument;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/datastructures/TextBlock;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "textBlocks"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    check-cast p0, Lcom/pspdfkit/internal/zf;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/zf;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public compareTo(Lcom/pspdfkit/datastructures/TextBlock;)I
    .locals 4

    .line 2
    iget v0, p1, Lcom/pspdfkit/datastructures/TextBlock;->pageIndex:I

    iget v1, p0, Lcom/pspdfkit/datastructures/TextBlock;->pageIndex:I

    if-ne v0, v1, :cond_3

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/datastructures/TextBlock;->annotation:Lcom/pspdfkit/annotations/Annotation;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/pspdfkit/datastructures/TextBlock;->annotation:Lcom/pspdfkit/annotations/Annotation;

    if-nez v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/datastructures/TextBlock;->range:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {v0}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v0

    iget-object p1, p1, Lcom/pspdfkit/datastructures/TextBlock;->range:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {p1}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result p1

    sub-int/2addr v0, p1

    return v0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/datastructures/TextBlock;->pageRects:Ljava/util/List;

    invoke-static {v0}, Lcom/pspdfkit/internal/di;->a(Ljava/util/List;)Landroid/graphics/RectF;

    move-result-object v0

    .line 7
    iget-object p1, p1, Lcom/pspdfkit/datastructures/TextBlock;->pageRects:Ljava/util/List;

    invoke-static {p1}, Lcom/pspdfkit/internal/di;->a(Ljava/util/List;)Landroid/graphics/RectF;

    move-result-object p1

    .line 8
    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iget v2, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_2

    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v3, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    goto :goto_0

    .line 11
    :cond_1
    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget p1, p1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, p1

    float-to-int p1, v0

    return p1

    .line 12
    :cond_2
    :goto_0
    iget p1, p1, Landroid/graphics/RectF;->top:F

    sub-float/2addr p1, v2

    float-to-int p1, p1

    return p1

    :cond_3
    sub-int/2addr v1, v0

    return v1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/datastructures/TextBlock;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/datastructures/TextBlock;->compareTo(Lcom/pspdfkit/datastructures/TextBlock;)I

    move-result p1

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TextBlock{text=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/datastructures/TextBlock;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', pageIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/pspdfkit/datastructures/TextBlock;->pageIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", range="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/datastructures/TextBlock;->range:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pageRects="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/datastructures/TextBlock;->pageRects:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

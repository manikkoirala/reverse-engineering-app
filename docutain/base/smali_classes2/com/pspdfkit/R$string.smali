.class public final Lcom/pspdfkit/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f120000

.field public static final abc_action_bar_up_description:I = 0x7f120001

.field public static final abc_action_menu_overflow_description:I = 0x7f120002

.field public static final abc_action_mode_done:I = 0x7f120003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f120004

.field public static final abc_activitychooserview_choose_application:I = 0x7f120005

.field public static final abc_capital_off:I = 0x7f120006

.field public static final abc_capital_on:I = 0x7f120007

.field public static final abc_menu_alt_shortcut_label:I = 0x7f120008

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f120009

.field public static final abc_menu_delete_shortcut_label:I = 0x7f12000a

.field public static final abc_menu_enter_shortcut_label:I = 0x7f12000b

.field public static final abc_menu_function_shortcut_label:I = 0x7f12000c

.field public static final abc_menu_meta_shortcut_label:I = 0x7f12000d

.field public static final abc_menu_shift_shortcut_label:I = 0x7f12000e

.field public static final abc_menu_space_shortcut_label:I = 0x7f12000f

.field public static final abc_menu_sym_shortcut_label:I = 0x7f120010

.field public static final abc_prepend_shortcut_label:I = 0x7f120011

.field public static final abc_search_hint:I = 0x7f120012

.field public static final abc_searchview_description_clear:I = 0x7f120013

.field public static final abc_searchview_description_query:I = 0x7f120014

.field public static final abc_searchview_description_search:I = 0x7f120015

.field public static final abc_searchview_description_submit:I = 0x7f120016

.field public static final abc_searchview_description_voice:I = 0x7f120017

.field public static final abc_shareactionprovider_share_with:I = 0x7f120018

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f120019

.field public static final abc_toolbar_collapse_description:I = 0x7f12001a

.field public static final appbar_scrolling_view_behavior:I = 0x7f12001d

.field public static final bottom_sheet_behavior:I = 0x7f12001e

.field public static final bottomsheet_action_collapse:I = 0x7f12001f

.field public static final bottomsheet_action_expand:I = 0x7f120020

.field public static final bottomsheet_action_expand_halfway:I = 0x7f120021

.field public static final bottomsheet_drag_handle_clicked:I = 0x7f120022

.field public static final bottomsheet_drag_handle_content_description:I = 0x7f120023

.field public static final character_counter_content_description:I = 0x7f12002b

.field public static final character_counter_overflowed_content_description:I = 0x7f12002c

.field public static final character_counter_pattern:I = 0x7f12002d

.field public static final clear_text_end_icon_content_description:I = 0x7f12002e

.field public static final copy:I = 0x7f120042

.field public static final error_a11y_label:I = 0x7f120046

.field public static final error_icon_content_description:I = 0x7f120047

.field public static final expand_button_title:I = 0x7f120048

.field public static final exposed_dropdown_menu_content_description:I = 0x7f120049

.field public static final fab_transformation_scrim_behavior:I = 0x7f12004a

.field public static final fab_transformation_sheet_behavior:I = 0x7f12004b

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f120062

.field public static final icon_content_description:I = 0x7f120063

.field public static final item_view_role_description:I = 0x7f120065

.field public static final m3_sys_motion_easing_emphasized:I = 0x7f12006a

.field public static final m3_sys_motion_easing_emphasized_accelerate:I = 0x7f12006b

.field public static final m3_sys_motion_easing_emphasized_decelerate:I = 0x7f12006c

.field public static final m3_sys_motion_easing_emphasized_path_data:I = 0x7f12006d

.field public static final m3_sys_motion_easing_legacy:I = 0x7f12006e

.field public static final m3_sys_motion_easing_legacy_accelerate:I = 0x7f12006f

.field public static final m3_sys_motion_easing_legacy_decelerate:I = 0x7f120070

.field public static final m3_sys_motion_easing_linear:I = 0x7f120071

.field public static final m3_sys_motion_easing_standard:I = 0x7f120072

.field public static final m3_sys_motion_easing_standard_accelerate:I = 0x7f120073

.field public static final m3_sys_motion_easing_standard_decelerate:I = 0x7f120074

.field public static final material_clock_display_divider:I = 0x7f120075

.field public static final material_clock_toggle_content_description:I = 0x7f120076

.field public static final material_hour_24h_suffix:I = 0x7f120077

.field public static final material_hour_selection:I = 0x7f120078

.field public static final material_hour_suffix:I = 0x7f120079

.field public static final material_minute_selection:I = 0x7f12007a

.field public static final material_minute_suffix:I = 0x7f12007b

.field public static final material_motion_easing_accelerated:I = 0x7f12007c

.field public static final material_motion_easing_decelerated:I = 0x7f12007d

.field public static final material_motion_easing_emphasized:I = 0x7f12007e

.field public static final material_motion_easing_linear:I = 0x7f12007f

.field public static final material_motion_easing_standard:I = 0x7f120080

.field public static final material_slider_range_end:I = 0x7f120081

.field public static final material_slider_range_start:I = 0x7f120082

.field public static final material_slider_value:I = 0x7f120083

.field public static final material_timepicker_am:I = 0x7f120084

.field public static final material_timepicker_clock_mode_description:I = 0x7f120085

.field public static final material_timepicker_hour:I = 0x7f120086

.field public static final material_timepicker_minute:I = 0x7f120087

.field public static final material_timepicker_pm:I = 0x7f120088

.field public static final material_timepicker_select_time:I = 0x7f120089

.field public static final material_timepicker_text_input_mode_description:I = 0x7f12008a

.field public static final mtrl_badge_numberless_content_description:I = 0x7f12008b

.field public static final mtrl_checkbox_button_icon_path_checked:I = 0x7f12008c

.field public static final mtrl_checkbox_button_icon_path_group_name:I = 0x7f12008d

.field public static final mtrl_checkbox_button_icon_path_indeterminate:I = 0x7f12008e

.field public static final mtrl_checkbox_button_icon_path_name:I = 0x7f12008f

.field public static final mtrl_checkbox_button_path_checked:I = 0x7f120090

.field public static final mtrl_checkbox_button_path_group_name:I = 0x7f120091

.field public static final mtrl_checkbox_button_path_name:I = 0x7f120092

.field public static final mtrl_checkbox_button_path_unchecked:I = 0x7f120093

.field public static final mtrl_checkbox_state_description_checked:I = 0x7f120094

.field public static final mtrl_checkbox_state_description_indeterminate:I = 0x7f120095

.field public static final mtrl_checkbox_state_description_unchecked:I = 0x7f120096

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f120097

.field public static final mtrl_exceed_max_badge_number_content_description:I = 0x7f120098

.field public static final mtrl_exceed_max_badge_number_suffix:I = 0x7f120099

.field public static final mtrl_picker_a11y_next_month:I = 0x7f12009a

.field public static final mtrl_picker_a11y_prev_month:I = 0x7f12009b

.field public static final mtrl_picker_announce_current_range_selection:I = 0x7f12009c

.field public static final mtrl_picker_announce_current_selection:I = 0x7f12009d

.field public static final mtrl_picker_announce_current_selection_none:I = 0x7f12009e

.field public static final mtrl_picker_cancel:I = 0x7f12009f

.field public static final mtrl_picker_confirm:I = 0x7f1200a0

.field public static final mtrl_picker_date_header_selected:I = 0x7f1200a1

.field public static final mtrl_picker_date_header_title:I = 0x7f1200a2

.field public static final mtrl_picker_date_header_unselected:I = 0x7f1200a3

.field public static final mtrl_picker_day_of_week_column_header:I = 0x7f1200a4

.field public static final mtrl_picker_end_date_description:I = 0x7f1200a5

.field public static final mtrl_picker_invalid_format:I = 0x7f1200a6

.field public static final mtrl_picker_invalid_format_example:I = 0x7f1200a7

.field public static final mtrl_picker_invalid_format_use:I = 0x7f1200a8

.field public static final mtrl_picker_invalid_range:I = 0x7f1200a9

.field public static final mtrl_picker_navigate_to_current_year_description:I = 0x7f1200aa

.field public static final mtrl_picker_navigate_to_year_description:I = 0x7f1200ab

.field public static final mtrl_picker_out_of_range:I = 0x7f1200ac

.field public static final mtrl_picker_range_header_only_end_selected:I = 0x7f1200ad

.field public static final mtrl_picker_range_header_only_start_selected:I = 0x7f1200ae

.field public static final mtrl_picker_range_header_selected:I = 0x7f1200af

.field public static final mtrl_picker_range_header_title:I = 0x7f1200b0

.field public static final mtrl_picker_range_header_unselected:I = 0x7f1200b1

.field public static final mtrl_picker_save:I = 0x7f1200b2

.field public static final mtrl_picker_start_date_description:I = 0x7f1200b3

.field public static final mtrl_picker_text_input_date_hint:I = 0x7f1200b5

.field public static final mtrl_picker_text_input_date_range_end_hint:I = 0x7f1200b6

.field public static final mtrl_picker_text_input_date_range_start_hint:I = 0x7f1200b7

.field public static final mtrl_picker_text_input_day_abbr:I = 0x7f1200b8

.field public static final mtrl_picker_text_input_month_abbr:I = 0x7f1200b9

.field public static final mtrl_picker_text_input_year_abbr:I = 0x7f1200ba

.field public static final mtrl_picker_today_description:I = 0x7f1200bb

.field public static final mtrl_picker_toggle_to_calendar_input_mode:I = 0x7f1200bc

.field public static final mtrl_picker_toggle_to_day_selection:I = 0x7f1200bd

.field public static final mtrl_picker_toggle_to_text_input_mode:I = 0x7f1200be

.field public static final mtrl_picker_toggle_to_year_selection:I = 0x7f1200bf

.field public static final mtrl_switch_thumb_group_name:I = 0x7f1200c0

.field public static final mtrl_switch_thumb_path_checked:I = 0x7f1200c1

.field public static final mtrl_switch_thumb_path_morphing:I = 0x7f1200c2

.field public static final mtrl_switch_thumb_path_name:I = 0x7f1200c3

.field public static final mtrl_switch_thumb_path_pressed:I = 0x7f1200c4

.field public static final mtrl_switch_thumb_path_unchecked:I = 0x7f1200c5

.field public static final mtrl_switch_track_decoration_path:I = 0x7f1200c6

.field public static final mtrl_switch_track_path:I = 0x7f1200c7

.field public static final mtrl_timepicker_cancel:I = 0x7f1200c8

.field public static final mtrl_timepicker_confirm:I = 0x7f1200c9

.field public static final not_set:I = 0x7f1200cc

.field public static final password_toggle_content_description:I = 0x7f1200ce

.field public static final path_password_eye:I = 0x7f1200cf

.field public static final path_password_eye_mask_strike_through:I = 0x7f1200d0

.field public static final path_password_eye_mask_visible:I = 0x7f1200d1

.field public static final path_password_strike_through:I = 0x7f1200d2

.field public static final preference_copied:I = 0x7f1200d3

.field public static final pspdf__action_menu_copy:I = 0x7f1200d5

.field public static final pspdf__action_menu_speak:I = 0x7f1200d6

.field public static final pspdf__activity_empty_message:I = 0x7f1200d7

.field public static final pspdf__activity_menu_no_outline:I = 0x7f1200d8

.field public static final pspdf__activity_menu_outline:I = 0x7f1200d9

.field public static final pspdf__activity_menu_pagegrid:I = 0x7f1200da

.field public static final pspdf__activity_menu_reader_view:I = 0x7f1200db

.field public static final pspdf__activity_menu_search:I = 0x7f1200dc

.field public static final pspdf__activity_menu_settings:I = 0x7f1200dd

.field public static final pspdf__activity_title_unnamed_document:I = 0x7f1200de

.field public static final pspdf__add:I = 0x7f1200df

.field public static final pspdf__add_bookmark:I = 0x7f1200e0

.field public static final pspdf__add_link:I = 0x7f1200e1

.field public static final pspdf__add_page:I = 0x7f1200e2

.field public static final pspdf__add_signature:I = 0x7f1200e3

.field public static final pspdf__align:I = 0x7f1200e4

.field public static final pspdf__align_documents:I = 0x7f1200e5

.field public static final pspdf__all:I = 0x7f1200e6

.field public static final pspdf__annotation_copied:I = 0x7f1200e7

.field public static final pspdf__annotation_creator_author_name:I = 0x7f1200e8

.field public static final pspdf__annotation_creator_info_text:I = 0x7f1200e9

.field public static final pspdf__annotation_cut:I = 0x7f1200ea

.field public static final pspdf__annotation_editing_embed:I = 0x7f1200eb

.field public static final pspdf__annotation_editing_embed_description:I = 0x7f1200ec

.field public static final pspdf__annotation_editing_flatten:I = 0x7f1200ed

.field public static final pspdf__annotation_editing_flatten_description:I = 0x7f1200ee

.field public static final pspdf__annotation_editing_ignore:I = 0x7f1200ef

.field public static final pspdf__annotation_editing_ignore_description:I = 0x7f1200f0

.field public static final pspdf__annotation_list_page:I = 0x7f1200f1

.field public static final pspdf__annotation_pasted:I = 0x7f1200f2

.field public static final pspdf__annotation_type_camera:I = 0x7f1200f3

.field public static final pspdf__annotation_type_circle:I = 0x7f1200f4

.field public static final pspdf__annotation_type_cloudy:I = 0x7f1200f5

.field public static final pspdf__annotation_type_cloudy_ellipse:I = 0x7f1200f6

.field public static final pspdf__annotation_type_cloudy_polygon:I = 0x7f1200f7

.field public static final pspdf__annotation_type_cloudy_rectangle:I = 0x7f1200f8

.field public static final pspdf__annotation_type_dashed_ellipse:I = 0x7f1200f9

.field public static final pspdf__annotation_type_dashed_polygon:I = 0x7f1200fa

.field public static final pspdf__annotation_type_dashed_rectangle:I = 0x7f1200fb

.field public static final pspdf__annotation_type_eraser:I = 0x7f1200fc

.field public static final pspdf__annotation_type_file:I = 0x7f1200fd

.field public static final pspdf__annotation_type_freetext:I = 0x7f1200fe

.field public static final pspdf__annotation_type_highlight:I = 0x7f1200ff

.field public static final pspdf__annotation_type_ink:I = 0x7f120100

.field public static final pspdf__annotation_type_instantComments:I = 0x7f120101

.field public static final pspdf__annotation_type_line:I = 0x7f120102

.field public static final pspdf__annotation_type_link:I = 0x7f120103

.field public static final pspdf__annotation_type_measure_distance:I = 0x7f120104

.field public static final pspdf__annotation_type_measure_elliptical_area:I = 0x7f120105

.field public static final pspdf__annotation_type_measure_perimeter:I = 0x7f120106

.field public static final pspdf__annotation_type_measure_polygonal_area:I = 0x7f120107

.field public static final pspdf__annotation_type_measure_rectangular_area:I = 0x7f120108

.field public static final pspdf__annotation_type_note:I = 0x7f120109

.field public static final pspdf__annotation_type_polygon:I = 0x7f12010a

.field public static final pspdf__annotation_type_polyline:I = 0x7f12010b

.field public static final pspdf__annotation_type_redaction:I = 0x7f12010c

.field public static final pspdf__annotation_type_rich_media:I = 0x7f12010d

.field public static final pspdf__annotation_type_screen:I = 0x7f12010e

.field public static final pspdf__annotation_type_sound:I = 0x7f12010f

.field public static final pspdf__annotation_type_square:I = 0x7f120110

.field public static final pspdf__annotation_type_squiggly:I = 0x7f120111

.field public static final pspdf__annotation_type_stamp:I = 0x7f120112

.field public static final pspdf__annotation_type_strikeout:I = 0x7f120113

.field public static final pspdf__annotation_type_underline:I = 0x7f120114

.field public static final pspdf__annotations:I = 0x7f120115

.field public static final pspdf__audio_error_start_playback:I = 0x7f120116

.field public static final pspdf__audio_error_start_recording:I = 0x7f120117

.field public static final pspdf__audio_pause:I = 0x7f120118

.field public static final pspdf__audio_play:I = 0x7f120119

.field public static final pspdf__audio_record:I = 0x7f12011a

.field public static final pspdf__audio_resume:I = 0x7f12011b

.field public static final pspdf__bookmark:I = 0x7f12011c

.field public static final pspdf__bookmarks:I = 0x7f12011d

.field public static final pspdf__calibration_value_hint:I = 0x7f12011e

.field public static final pspdf__camera_not_available:I = 0x7f12011f

.field public static final pspdf__cancel:I = 0x7f120120

.field public static final pspdf__certificate:I = 0x7f120121

.field public static final pspdf__clear:I = 0x7f120122

.field public static final pspdf__clear_annotations:I = 0x7f120123

.field public static final pspdf__clear_annotations_confirm:I = 0x7f120124

.field public static final pspdf__close:I = 0x7f120125

.field public static final pspdf__cloudy:I = 0x7f120126

.field public static final pspdf__color_blue:I = 0x7f120127

.field public static final pspdf__color_green:I = 0x7f120128

.field public static final pspdf__color_orange:I = 0x7f120129

.field public static final pspdf__color_picker_color_variations:I = 0x7f12012a

.field public static final pspdf__color_picker_default_color_palette:I = 0x7f12012b

.field public static final pspdf__color_picker_hex:I = 0x7f12012c

.field public static final pspdf__color_picker_hex_color_value:I = 0x7f12012d

.field public static final pspdf__color_picker_hsl:I = 0x7f12012e

.field public static final pspdf__color_picker_hue:I = 0x7f12012f

.field public static final pspdf__color_picker_invalid_color_value:I = 0x7f120130

.field public static final pspdf__color_picker_lightness:I = 0x7f120131

.field public static final pspdf__color_picker_palette:I = 0x7f120132

.field public static final pspdf__color_picker_paste_from_clipboard:I = 0x7f120133

.field public static final pspdf__color_picker_recently_used:I = 0x7f120134

.field public static final pspdf__color_picker_rgb:I = 0x7f120135

.field public static final pspdf__color_picker_saturation:I = 0x7f120136

.field public static final pspdf__color_pink:I = 0x7f120137

.field public static final pspdf__color_red:I = 0x7f120138

.field public static final pspdf__color_yellow:I = 0x7f120139

.field public static final pspdf__comparison_hint_text:I = 0x7f12013a

.field public static final pspdf__contentediting_confirm_discard_changes:I = 0x7f12013b

.field public static final pspdf__contentediting_label_decrease_fontsize:I = 0x7f12013c

.field public static final pspdf__contentediting_label_increase_fontsize:I = 0x7f12013d

.field public static final pspdf__contentediting_label_mixed_colors:I = 0x7f12013e

.field public static final pspdf__contentediting_mixed_font_sizes:I = 0x7f12013f

.field public static final pspdf__contentediting_mixed_fonts:I = 0x7f120140

.field public static final pspdf__contentediting_title:I = 0x7f120141

.field public static final pspdf__contentediting_unknown_font:I = 0x7f120142

.field public static final pspdf__copy:I = 0x7f120143

.field public static final pspdf__create_link:I = 0x7f120144

.field public static final pspdf__create_stamp:I = 0x7f120145

.field public static final pspdf__current_page:I = 0x7f120146

.field public static final pspdf__custom_stamp:I = 0x7f120147

.field public static final pspdf__custom_value:I = 0x7f120148

.field public static final pspdf__custom_value_hint:I = 0x7f120149

.field public static final pspdf__cut:I = 0x7f12014a

.field public static final pspdf__date_switch:I = 0x7f12014b

.field public static final pspdf__default_values:I = 0x7f12014c

.field public static final pspdf__delete:I = 0x7f12014d

.field public static final pspdf__delete_comment:I = 0x7f12014e

.field public static final pspdf__delete_pages:I = 0x7f12014f

.field public static final pspdf__digital_signature_certificate_failed_retrieve_signature_contents:I = 0x7f120150

.field public static final pspdf__digital_signature_certificate_general_validation_problem:I = 0x7f120151

.field public static final pspdf__digital_signature_certificate_invalid:I = 0x7f120152

.field public static final pspdf__digital_signature_certificate_not_yet_valid:I = 0x7f120153

.field public static final pspdf__digital_signature_certificate_revoked:I = 0x7f120154

.field public static final pspdf__digital_signature_certificate_status_expired:I = 0x7f120155

.field public static final pspdf__digital_signature_could_not_save:I = 0x7f120156

.field public static final pspdf__digital_signature_error_certificate_chain_invalid:I = 0x7f120157

.field public static final pspdf__digital_signature_error_certificate_chain_not_provided:I = 0x7f120158

.field public static final pspdf__digital_signature_error_integrity_check:I = 0x7f120159

.field public static final pspdf__digital_signature_error_validation_failed:I = 0x7f12015a

.field public static final pspdf__digital_signature_explanation_invalid:I = 0x7f12015b

.field public static final pspdf__digital_signature_explanation_valid_modified:I = 0x7f12015c

.field public static final pspdf__digital_signature_explanation_valid_not_modified:I = 0x7f12015d

.field public static final pspdf__digital_signature_failed_compute_digest:I = 0x7f12015e

.field public static final pspdf__digital_signature_failed_encryption_padding:I = 0x7f12015f

.field public static final pspdf__digital_signature_failed_retrieve_byte_range:I = 0x7f120160

.field public static final pspdf__digital_signature_failed_retrieve_public_key:I = 0x7f120161

.field public static final pspdf__digital_signature_failed_retrieve_signature_contents:I = 0x7f120162

.field public static final pspdf__digital_signature_failed_retrieve_signing_certificate:I = 0x7f120163

.field public static final pspdf__digital_signature_general_failure:I = 0x7f120164

.field public static final pspdf__digital_signature_integrity_self_signed:I = 0x7f120165

.field public static final pspdf__digital_signature_invalid:I = 0x7f120166

.field public static final pspdf__digital_signature_sign:I = 0x7f120167

.field public static final pspdf__digital_signature_signed:I = 0x7f120168

.field public static final pspdf__digital_signature_signed_by:I = 0x7f120169

.field public static final pspdf__digital_signature_signed_date:I = 0x7f12016a

.field public static final pspdf__digital_signature_signed_location:I = 0x7f12016b

.field public static final pspdf__digital_signature_signed_reason:I = 0x7f12016c

.field public static final pspdf__digital_signature_signed_with_name:I = 0x7f12016d

.field public static final pspdf__digital_signature_signed_with_name_invalid:I = 0x7f12016e

.field public static final pspdf__digital_signature_signed_without_name:I = 0x7f12016f

.field public static final pspdf__digital_signature_signed_without_name_invalid:I = 0x7f120170

.field public static final pspdf__digital_signature_signing:I = 0x7f120171

.field public static final pspdf__digital_signature_valid:I = 0x7f120172

.field public static final pspdf__digital_signature_valid_warnings:I = 0x7f120173

.field public static final pspdf__discard_changes:I = 0x7f120174

.field public static final pspdf__dismiss_button:I = 0x7f120175

.field public static final pspdf__document_comparison:I = 0x7f120176

.field public static final pspdf__document_could_not_be_saved:I = 0x7f120177

.field public static final pspdf__document_info:I = 0x7f120178

.field public static final pspdf__document_info_author:I = 0x7f120179

.field public static final pspdf__document_info_changes:I = 0x7f12017a

.field public static final pspdf__document_info_content:I = 0x7f12017b

.field public static final pspdf__document_info_content_creator:I = 0x7f12017c

.field public static final pspdf__document_info_creation_date:I = 0x7f12017d

.field public static final pspdf__document_info_file_size:I = 0x7f12017e

.field public static final pspdf__document_info_keywords:I = 0x7f12017f

.field public static final pspdf__document_info_mod_date:I = 0x7f120180

.field public static final pspdf__document_info_not_set:I = 0x7f120181

.field public static final pspdf__document_info_number_pf_pages:I = 0x7f120182

.field public static final pspdf__document_info_producer:I = 0x7f120183

.field public static final pspdf__document_info_subject:I = 0x7f120184

.field public static final pspdf__document_info_title:I = 0x7f120185

.field public static final pspdf__document_name:I = 0x7f120186

.field public static final pspdf__done:I = 0x7f120187

.field public static final pspdf__duplicate_pages:I = 0x7f120188

.field public static final pspdf__edit:I = 0x7f120189

.field public static final pspdf__edit_menu_callout:I = 0x7f12018a

.field public static final pspdf__edit_menu_color:I = 0x7f12018b

.field public static final pspdf__edit_menu_fill_color:I = 0x7f12018c

.field public static final pspdf__edit_menu_freetext:I = 0x7f12018d

.field public static final pspdf__edit_menu_highlight:I = 0x7f12018e

.field public static final pspdf__edit_menu_ink:I = 0x7f12018f

.field public static final pspdf__edit_menu_ink_highlighter:I = 0x7f120190

.field public static final pspdf__edit_menu_ink_pen:I = 0x7f120191

.field public static final pspdf__edit_menu_line_arrow:I = 0x7f120192

.field public static final pspdf__edit_menu_magic_ink:I = 0x7f120193

.field public static final pspdf__edit_menu_note:I = 0x7f120194

.field public static final pspdf__edit_menu_outline_color:I = 0x7f120195

.field public static final pspdf__edit_menu_overlay_text:I = 0x7f120196

.field public static final pspdf__edit_menu_repeat_overlay_text:I = 0x7f120197

.field public static final pspdf__edit_menu_squiggly:I = 0x7f120198

.field public static final pspdf__edit_menu_strikeout:I = 0x7f120199

.field public static final pspdf__edit_menu_text_color:I = 0x7f12019a

.field public static final pspdf__edit_menu_underline:I = 0x7f12019b

.field public static final pspdf__electronic_signature_clear_signature:I = 0x7f12019c

.field public static final pspdf__electronic_signature_draw_tab:I = 0x7f12019d

.field public static final pspdf__electronic_signature_image_tab:I = 0x7f12019e

.field public static final pspdf__electronic_signature_replace_image:I = 0x7f12019f

.field public static final pspdf__electronic_signature_save_signature:I = 0x7f1201a0

.field public static final pspdf__electronic_signature_select_image:I = 0x7f1201a1

.field public static final pspdf__electronic_signature_sign_here:I = 0x7f1201a2

.field public static final pspdf__electronic_signature_type_tab:I = 0x7f1201a3

.field public static final pspdf__electronic_signature_type_your_signature_above:I = 0x7f1201a4

.field public static final pspdf__equals_sign:I = 0x7f1201a5

.field public static final pspdf__export_pages:I = 0x7f1201a6

.field public static final pspdf__exporting:I = 0x7f1201a7

.field public static final pspdf__file_not_available:I = 0x7f1201a8

.field public static final pspdf__file_not_found_message:I = 0x7f1201a9

.field public static final pspdf__file_not_found_title:I = 0x7f1201aa

.field public static final pspdf__filename_redacted:I = 0x7f1201ab

.field public static final pspdf__font_missing:I = 0x7f1201ac

.field public static final pspdf__form_type_button:I = 0x7f1201ad

.field public static final pspdf__form_type_choice_field:I = 0x7f1201ae

.field public static final pspdf__form_type_signature_field:I = 0x7f1201af

.field public static final pspdf__form_type_text_field:I = 0x7f1201b0

.field public static final pspdf__forms_clear_field:I = 0x7f1201b1

.field public static final pspdf__fragment_password_hint:I = 0x7f1201b2

.field public static final pspdf__gallery_item_img_desc:I = 0x7f1201b3

.field public static final pspdf__hint_add_your_comment:I = 0x7f1201b4

.field public static final pspdf__import_document:I = 0x7f1201b5

.field public static final pspdf__invalid_date_time:I = 0x7f1201b6

.field public static final pspdf__invalid_value:I = 0x7f1201b7

.field public static final pspdf__invalid_value_format:I = 0x7f1201b8

.field public static final pspdf__invalid_value_greater_than_and_less_than:I = 0x7f1201b9

.field public static final pspdf__invalid_value_greater_than_or_equal:I = 0x7f1201ba

.field public static final pspdf__invalid_value_less_than_or_equal:I = 0x7f1201bb

.field public static final pspdf__landscape:I = 0x7f1201bc

.field public static final pspdf__link_annotation_creation_failed:I = 0x7f1201bd

.field public static final pspdf__link_annotation_creation_parsed_text_error:I = 0x7f1201be

.field public static final pspdf__link_annotation_successfully_created:I = 0x7f1201bf

.field public static final pspdf__link_destination:I = 0x7f1201c0

.field public static final pspdf__link_enter_page_index_or_url:I = 0x7f1201c1

.field public static final pspdf__link_page_not_found:I = 0x7f1201c2

.field public static final pspdf__loading:I = 0x7f1201c3

.field public static final pspdf__more_options:I = 0x7f1201c4

.field public static final pspdf__name:I = 0x7f1201c5

.field public static final pspdf__new_document:I = 0x7f1201c6

.field public static final pspdf__no_annotations:I = 0x7f1201c7

.field public static final pspdf__no_applications_found:I = 0x7f1201c8

.field public static final pspdf__no_bookmarks:I = 0x7f1201c9

.field public static final pspdf__no_signatures:I = 0x7f1201ca

.field public static final pspdf__note_editor_style:I = 0x7f1201cb

.field public static final pspdf__note_icon_checkmark:I = 0x7f1201cc

.field public static final pspdf__note_icon_circle:I = 0x7f1201cd

.field public static final pspdf__note_icon_comment:I = 0x7f1201ce

.field public static final pspdf__note_icon_cross:I = 0x7f1201cf

.field public static final pspdf__note_icon_help:I = 0x7f1201d0

.field public static final pspdf__note_icon_insert_text:I = 0x7f1201d1

.field public static final pspdf__note_icon_key:I = 0x7f1201d2

.field public static final pspdf__note_icon_new_paragraph:I = 0x7f1201d3

.field public static final pspdf__note_icon_paragraph:I = 0x7f1201d4

.field public static final pspdf__note_icon_right_arrow:I = 0x7f1201d5

.field public static final pspdf__note_icon_right_pointer:I = 0x7f1201d6

.field public static final pspdf__note_icon_star:I = 0x7f1201d7

.field public static final pspdf__note_icon_text_note:I = 0x7f1201d8

.field public static final pspdf__ok:I = 0x7f1201d9

.field public static final pspdf__old_document:I = 0x7f1201da

.field public static final pspdf__open:I = 0x7f1201db

.field public static final pspdf__open_settings:I = 0x7f1201dc

.field public static final pspdf__orientation:I = 0x7f1201dd

.field public static final pspdf__page_binding:I = 0x7f1201de

.field public static final pspdf__page_binding_left:I = 0x7f1201df

.field public static final pspdf__page_binding_left_edge:I = 0x7f1201e0

.field public static final pspdf__page_binding_right:I = 0x7f1201e1

.field public static final pspdf__page_binding_right_edge:I = 0x7f1201e2

.field public static final pspdf__page_binding_unknown:I = 0x7f1201e3

.field public static final pspdf__page_overlay:I = 0x7f1201e4

.field public static final pspdf__page_overlay_double_page:I = 0x7f1201e5

.field public static final pspdf__page_overlay_with_label:I = 0x7f1201e6

.field public static final pspdf__page_pattern_dot_5mm:I = 0x7f1201e7

.field public static final pspdf__page_pattern_grid_5mm:I = 0x7f1201e8

.field public static final pspdf__page_pattern_line_5mm:I = 0x7f1201e9

.field public static final pspdf__page_pattern_line_7mm:I = 0x7f1201ea

.field public static final pspdf__page_pattern_none:I = 0x7f1201eb

.field public static final pspdf__page_range:I = 0x7f1201ec

.field public static final pspdf__page_size_a4:I = 0x7f1201ed

.field public static final pspdf__page_size_a5:I = 0x7f1201ee

.field public static final pspdf__page_size_us_legal:I = 0x7f1201ef

.field public static final pspdf__page_size_us_letter:I = 0x7f1201f0

.field public static final pspdf__page_with_number:I = 0x7f1201f1

.field public static final pspdf__pages:I = 0x7f1201f2

.field public static final pspdf__password:I = 0x7f1201f3

.field public static final pspdf__paste:I = 0x7f1201f4

.field public static final pspdf__permission_rationale_local_access_denied_permanently:I = 0x7f1201f5

.field public static final pspdf__permission_rationale_record_audio_denied_permanently:I = 0x7f1201f6

.field public static final pspdf__picker_calibrate:I = 0x7f1201f7

.field public static final pspdf__picker_font:I = 0x7f1201f8

.field public static final pspdf__picker_line_end:I = 0x7f1201f9

.field public static final pspdf__picker_line_ends_fill_color:I = 0x7f1201fa

.field public static final pspdf__picker_line_start:I = 0x7f1201fb

.field public static final pspdf__picker_line_style:I = 0x7f1201fc

.field public static final pspdf__picker_opacity:I = 0x7f1201fd

.field public static final pspdf__picker_precision:I = 0x7f1201fe

.field public static final pspdf__picker_scale:I = 0x7f1201ff

.field public static final pspdf__picker_snapping:I = 0x7f120200

.field public static final pspdf__picker_thickness:I = 0x7f120201

.field public static final pspdf__point_selection_step:I = 0x7f120202

.field public static final pspdf__point_selection_target:I = 0x7f120203

.field public static final pspdf__portrait:I = 0x7f120204

.field public static final pspdf__print:I = 0x7f120205

.field public static final pspdf__print_with_annotations:I = 0x7f120206

.field public static final pspdf__print_without_annotations:I = 0x7f120207

.field public static final pspdf__prompt_delete_annotation:I = 0x7f120208

.field public static final pspdf__redaction_apply_dialog_failed:I = 0x7f120209

.field public static final pspdf__redaction_apply_dialog_message:I = 0x7f12020a

.field public static final pspdf__redaction_apply_dialog_new_file:I = 0x7f12020b

.field public static final pspdf__redaction_apply_dialog_overwrite_file:I = 0x7f12020c

.field public static final pspdf__redaction_apply_redactions:I = 0x7f12020d

.field public static final pspdf__redaction_clear_redactions:I = 0x7f12020e

.field public static final pspdf__redaction_disable_preview:I = 0x7f12020f

.field public static final pspdf__redaction_editor_warning:I = 0x7f120210

.field public static final pspdf__redaction_enable_preview:I = 0x7f120211

.field public static final pspdf__redaction_redact:I = 0x7f120212

.field public static final pspdf__redaction_redacting:I = 0x7f120213

.field public static final pspdf__redo:I = 0x7f120214

.field public static final pspdf__remove:I = 0x7f120215

.field public static final pspdf__remove_signature:I = 0x7f120216

.field public static final pspdf__reply_accepted_by:I = 0x7f120217

.field public static final pspdf__reply_cancelled_by:I = 0x7f120218

.field public static final pspdf__reply_completed_by:I = 0x7f120219

.field public static final pspdf__reply_rejected_by:I = 0x7f12021a

.field public static final pspdf__reply_status_accepted:I = 0x7f12021b

.field public static final pspdf__reply_status_cancelled:I = 0x7f12021c

.field public static final pspdf__reply_status_completed:I = 0x7f12021d

.field public static final pspdf__reply_status_none:I = 0x7f12021e

.field public static final pspdf__reply_status_rejected:I = 0x7f12021f

.field public static final pspdf__rotate_pages:I = 0x7f120220

.field public static final pspdf__sample:I = 0x7f120221

.field public static final pspdf__save:I = 0x7f120222

.field public static final pspdf__save_as:I = 0x7f120223

.field public static final pspdf__saving:I = 0x7f120224

.field public static final pspdf__scale_value_hint:I = 0x7f120225

.field public static final pspdf__search_btn_next:I = 0x7f120226

.field public static final pspdf__search_btn_previous:I = 0x7f120227

.field public static final pspdf__search_complete:I = 0x7f120228

.field public static final pspdf__search_hint:I = 0x7f120229

.field public static final pspdf__search_no_matches:I = 0x7f12022a

.field public static final pspdf__search_outline_hint:I = 0x7f12022b

.field public static final pspdf__search_result_of:I = 0x7f12022c

.field public static final pspdf__select_point:I = 0x7f12022d

.field public static final pspdf__set_reply_status:I = 0x7f12022e

.field public static final pspdf__settings_menu_auto:I = 0x7f12022f

.field public static final pspdf__settings_menu_continuous:I = 0x7f120230

.field public static final pspdf__settings_menu_default:I = 0x7f120231

.field public static final pspdf__settings_menu_double:I = 0x7f120232

.field public static final pspdf__settings_menu_horizontal:I = 0x7f120233

.field public static final pspdf__settings_menu_jump:I = 0x7f120234

.field public static final pspdf__settings_menu_keep_screen_on:I = 0x7f120235

.field public static final pspdf__settings_menu_night:I = 0x7f120236

.field public static final pspdf__settings_menu_page_layout:I = 0x7f120237

.field public static final pspdf__settings_menu_page_transition:I = 0x7f120238

.field public static final pspdf__settings_menu_scroll_direction:I = 0x7f120239

.field public static final pspdf__settings_menu_single:I = 0x7f12023a

.field public static final pspdf__settings_menu_theme:I = 0x7f12023b

.field public static final pspdf__settings_menu_vertical:I = 0x7f12023c

.field public static final pspdf__share:I = 0x7f12023d

.field public static final pspdf__sign:I = 0x7f12023e

.field public static final pspdf__signature:I = 0x7f12023f

.field public static final pspdf__signature_sign_here:I = 0x7f120240

.field public static final pspdf__signatures:I = 0x7f120241

.field public static final pspdf__size:I = 0x7f120242

.field public static final pspdf__stamp_accepted:I = 0x7f120243

.field public static final pspdf__stamp_approved:I = 0x7f120244

.field public static final pspdf__stamp_as_is:I = 0x7f120245

.field public static final pspdf__stamp_completed:I = 0x7f120246

.field public static final pspdf__stamp_confidential:I = 0x7f120247

.field public static final pspdf__stamp_custom_description:I = 0x7f120248

.field public static final pspdf__stamp_custom_section:I = 0x7f120249

.field public static final pspdf__stamp_departmental:I = 0x7f12024a

.field public static final pspdf__stamp_draft:I = 0x7f12024b

.field public static final pspdf__stamp_experimental:I = 0x7f12024c

.field public static final pspdf__stamp_expired:I = 0x7f12024d

.field public static final pspdf__stamp_final:I = 0x7f12024e

.field public static final pspdf__stamp_for_comment:I = 0x7f12024f

.field public static final pspdf__stamp_for_public_release:I = 0x7f120250

.field public static final pspdf__stamp_information_only:I = 0x7f120251

.field public static final pspdf__stamp_initial_here:I = 0x7f120252

.field public static final pspdf__stamp_not_approved:I = 0x7f120253

.field public static final pspdf__stamp_not_for_public_release:I = 0x7f120254

.field public static final pspdf__stamp_preliminary_results:I = 0x7f120255

.field public static final pspdf__stamp_rejected:I = 0x7f120256

.field public static final pspdf__stamp_revised:I = 0x7f120257

.field public static final pspdf__stamp_sign_here:I = 0x7f120258

.field public static final pspdf__stamp_sold:I = 0x7f120259

.field public static final pspdf__stamp_standard_section:I = 0x7f12025a

.field public static final pspdf__stamp_text:I = 0x7f12025b

.field public static final pspdf__stamp_top_secret:I = 0x7f12025c

.field public static final pspdf__stamp_void:I = 0x7f12025d

.field public static final pspdf__stamp_witness:I = 0x7f12025e

.field public static final pspdf__store_signature:I = 0x7f12025f

.field public static final pspdf__text_copied_to_clipboard:I = 0x7f120260

.field public static final pspdf__time_switch:I = 0x7f120261

.field public static final pspdf__tts_not_available:I = 0x7f120262

.field public static final pspdf__undo:I = 0x7f120263

.field public static final pspdf__unit_pt:I = 0x7f120264

.field public static final pspdf__unknown_date:I = 0x7f120265

.field public static final pspdf__unknown_time:I = 0x7f120266

.field public static final pspdf__unnamed_image_document:I = 0x7f120267

.field public static final pspdf__update_required:I = 0x7f120268

.field public static final pspdf__update_required_description:I = 0x7f120269

.field public static final pspdf__use_document_size:I = 0x7f12026a

.field public static final pspdf__z_index_back:I = 0x7f12026b

.field public static final pspdf__z_index_backward:I = 0x7f12026c

.field public static final pspdf__z_index_forward:I = 0x7f12026d

.field public static final pspdf__z_index_front:I = 0x7f12026e

.field public static final pspdf__z_index_order:I = 0x7f12026f

.field public static final search_menu_title:I = 0x7f120271

.field public static final searchbar_scrolling_view_behavior:I = 0x7f120272

.field public static final searchview_clear_text_content_description:I = 0x7f120273

.field public static final searchview_navigation_content_description:I = 0x7f120274

.field public static final side_sheet_accessibility_pane_title:I = 0x7f120275

.field public static final side_sheet_behavior:I = 0x7f120276

.field public static final status_bar_notification_info_overflow:I = 0x7f120277

.field public static final summary_collapsed_preference_list:I = 0x7f120278

.field public static final v7_preference_off:I = 0x7f120279

.field public static final v7_preference_on:I = 0x7f12027a


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

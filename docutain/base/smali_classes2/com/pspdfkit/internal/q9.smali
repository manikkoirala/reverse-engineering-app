.class final Lcom/pspdfkit/internal/q9;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/r9;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/r9;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/q9;->a:Lcom/pspdfkit/internal/r9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAccept(Lcom/pspdfkit/document/sharing/SharingOptions;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/q9;->a:Lcom/pspdfkit/internal/r9;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/r9;->-$$Nest$fputh(Lcom/pspdfkit/internal/r9;Z)V

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/r9;->-$$Nest$fgete(Lcom/pspdfkit/internal/r9;)Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    invoke-static {v0}, Lcom/pspdfkit/internal/r9;->-$$Nest$fgeta(Lcom/pspdfkit/internal/r9;)Lcom/pspdfkit/document/PdfDocument;

    move-result-object v2

    invoke-static {v0}, Lcom/pspdfkit/internal/r9;->-$$Nest$fgetd(Lcom/pspdfkit/internal/r9;)Lcom/pspdfkit/document/sharing/ShareAction;

    move-result-object v3

    invoke-static {v1, v2, v3, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingManager;->shareDocument(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/sharing/ShareAction;Lcom/pspdfkit/document/sharing/SharingOptions;)Lcom/pspdfkit/document/sharing/DocumentSharingController;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/r9;->-$$Nest$fputf(Lcom/pspdfkit/internal/r9;Lcom/pspdfkit/document/sharing/DocumentSharingController;)V

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    const-string v1, "share"

    .line 5
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    invoke-static {v0}, Lcom/pspdfkit/internal/r9;->-$$Nest$fgetd(Lcom/pspdfkit/internal/r9;)Lcom/pspdfkit/document/sharing/ShareAction;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    const-string v1, "action"

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    :goto_0
    return-void
.end method

.method public final onDismiss()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/q9;->a:Lcom/pspdfkit/internal/r9;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/r9;->-$$Nest$fputh(Lcom/pspdfkit/internal/r9;Z)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/m3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/audio/AudioRecordingController;
.implements Lcom/pspdfkit/internal/k3$b;
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# instance fields
.field private final b:Lcom/pspdfkit/internal/a3;

.field private final c:Lcom/pspdfkit/internal/fl;

.field private final d:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/audio/AudioRecordingController$AudioRecordingListener;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/pspdfkit/internal/t;

.field private f:Lcom/pspdfkit/annotations/SoundAnnotation;

.field private g:Lcom/pspdfkit/internal/k3;


# direct methods
.method public static synthetic $r8$lambda$fG37yzGbhRNODwgdQ0ygDRPJHA8(Lcom/pspdfkit/internal/m3;Lcom/pspdfkit/annotations/SoundAnnotation;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/m3;Lcom/pspdfkit/annotations/SoundAnnotation;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/a3;Lcom/pspdfkit/internal/vu;)V
    .locals 1

    const-string v0, "audioManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEditRecordedListener"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/m3;->b:Lcom/pspdfkit/internal/a3;

    iput-object p2, p0, Lcom/pspdfkit/internal/m3;->c:Lcom/pspdfkit/internal/fl;

    .line 3
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/m3;->d:Lcom/pspdfkit/internal/nh;

    .line 5
    invoke-static {}, Lcom/pspdfkit/internal/t$a;->b()Lcom/pspdfkit/internal/t;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/m3;->e:Lcom/pspdfkit/internal/t;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/m3;)Lcom/pspdfkit/annotations/SoundAnnotation;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/m3;->f:Lcom/pspdfkit/annotations/SoundAnnotation;

    return-object p0
.end method

.method private final a(Lcom/pspdfkit/internal/ls;)V
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->f:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-nez v0, :cond_0

    return-void

    .line 64
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getSoundAnnotationState()Lcom/pspdfkit/internal/ls;

    move-result-object v1

    if-eq v1, p1, :cond_1

    .line 65
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/pf;->setSoundAnnotationState(Lcom/pspdfkit/internal/ls;)V

    .line 66
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/r1;->j(Lcom/pspdfkit/annotations/Annotation;)V

    :cond_1
    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/m3;Lcom/pspdfkit/annotations/SoundAnnotation;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/m3;->f:Lcom/pspdfkit/annotations/SoundAnnotation;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/m3;Lcom/pspdfkit/annotations/SoundAnnotation;Z)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->c:Lcom/pspdfkit/internal/fl;

    new-instance v1, Lcom/pspdfkit/internal/t3;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/t3;-><init>(Lcom/pspdfkit/annotations/SoundAnnotation;)V

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    if-eqz p2, :cond_0

    .line 62
    iget-object p0, p0, Lcom/pspdfkit/internal/m3;->b:Lcom/pspdfkit/internal/a3;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/a3;->enterAudioPlaybackMode(Lcom/pspdfkit/annotations/SoundAnnotation;)V

    :cond_0
    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/m3;Z)V
    .locals 3

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->b:Lcom/pspdfkit/internal/a3;

    .line 23
    invoke-virtual {v0}, Lcom/pspdfkit/internal/a3;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SOUND:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const-class v2, Lcom/pspdfkit/annotations/configuration/SoundAnnotationConfiguration;

    invoke-interface {v0, v1, v2}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/SoundAnnotationConfiguration;

    if-eqz v0, :cond_0

    .line 25
    new-instance v1, Lcom/pspdfkit/internal/k3;

    .line 26
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/SoundAnnotationConfiguration;->getRecordingSampleRate()I

    move-result v2

    .line 27
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/SoundAnnotationConfiguration;->getAudioRecordingTimeLimit()I

    move-result v0

    .line 28
    invoke-direct {v1, v2, v0}, Lcom/pspdfkit/internal/k3;-><init>(II)V

    goto :goto_0

    .line 33
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/k3;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/k3;-><init>(I)V

    .line 35
    :goto_0
    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/k3;->a(Lcom/pspdfkit/internal/k3$b;)V

    .line 36
    iput-object v1, p0, Lcom/pspdfkit/internal/m3;->g:Lcom/pspdfkit/internal/k3;

    .line 37
    new-instance v0, Lcom/pspdfkit/internal/p3;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/p3;-><init>(Lcom/pspdfkit/internal/m3;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    if-eqz p1, :cond_1

    .line 38
    invoke-virtual {p0}, Lcom/pspdfkit/internal/m3;->resume()V

    :cond_1
    return-void
.end method

.method private final a(Z)V
    .locals 3

    .line 45
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->g:Lcom/pspdfkit/internal/k3;

    if-nez v0, :cond_0

    return-void

    .line 46
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/m3;->f:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v1, :cond_1

    .line 47
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 48
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/k3;->a(Lcom/pspdfkit/annotations/SoundAnnotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 49
    new-instance v2, Lcom/pspdfkit/internal/m3$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v1, p1}, Lcom/pspdfkit/internal/m3$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/m3;Lcom/pspdfkit/annotations/SoundAnnotation;Z)V

    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/core/Completable;->doOnComplete(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_0

    .line 57
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/internal/k3;->b()V

    :goto_0
    const/4 p1, 0x0

    .line 59
    iput-object p1, p0, Lcom/pspdfkit/internal/m3;->g:Lcom/pspdfkit/internal/k3;

    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/m3;)Lcom/pspdfkit/internal/a3;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/m3;->b:Lcom/pspdfkit/internal/a3;

    return-object p0
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/m3;)Lcom/pspdfkit/internal/nh;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/m3;->d:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method

.method public static final synthetic d(Lcom/pspdfkit/internal/m3;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/ls;->c:Lcom/pspdfkit/internal/ls;

    .line 43
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/ls;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/v3;
    .locals 5

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->f:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v0, :cond_0

    .line 41
    new-instance v1, Lcom/pspdfkit/internal/v3;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/m3;->isResumed()Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 42
    invoke-direct {v1, v0, v3, v2, v4}, Lcom/pspdfkit/internal/v3;-><init>(Lcom/pspdfkit/annotations/SoundAnnotation;ZZI)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public final a(Landroid/content/Context;Lcom/pspdfkit/annotations/SoundAnnotation;Z)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->f:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 4
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/m3;->a(Z)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->f:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-nez v0, :cond_1

    goto :goto_0

    .line 7
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 8
    sget-object v0, Lcom/pspdfkit/internal/ls;->a:Lcom/pspdfkit/internal/ls;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/ls;)V

    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/pspdfkit/internal/m3;->f:Lcom/pspdfkit/annotations/SoundAnnotation;

    .line 10
    :goto_0
    new-instance v0, Lcom/pspdfkit/internal/m3$a;

    invoke-direct {v0, p0, p2, p3}, Lcom/pspdfkit/internal/m3$a;-><init>(Lcom/pspdfkit/internal/m3;Lcom/pspdfkit/annotations/SoundAnnotation;Z)V

    .line 11
    invoke-static {p1}, Lcom/pspdfkit/internal/in$a;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/in;

    move-result-object p2

    const-string p3, "android.permission.RECORD_AUDIO"

    .line 12
    invoke-interface {p2, p3}, Lcom/pspdfkit/internal/in;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 13
    invoke-static {}, Lcom/pspdfkit/internal/v;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 14
    invoke-static {p1}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;)Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 16
    iget-object p3, p0, Lcom/pspdfkit/internal/m3;->e:Lcom/pspdfkit/internal/t;

    invoke-interface {p3, p1, v1, p2, v0}, Lcom/pspdfkit/internal/t;->a(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/internal/in;Lkotlin/jvm/functions/Function1;)V

    goto :goto_1

    .line 18
    :cond_2
    invoke-interface {p2, p3}, Lcom/pspdfkit/internal/in;->a(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/m3$a;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 21
    :cond_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/m3$a;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/v3;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "document"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p3, p2}, Lcom/pspdfkit/internal/v3;->a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p2

    .line 44
    new-instance v0, Lcom/pspdfkit/internal/m3$b;

    invoke-direct {v0, p0, p1, p3}, Lcom/pspdfkit/internal/m3$b;-><init>(Lcom/pspdfkit/internal/m3;Landroid/content/Context;Lcom/pspdfkit/internal/v3;)V

    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/k3$a;Ljava/lang/Throwable;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_5

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 p2, 0x4

    if-eq p1, p2, :cond_0

    goto :goto_0

    .line 85
    :cond_0
    sget-object p1, Lcom/pspdfkit/internal/ls;->a:Lcom/pspdfkit/internal/ls;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/ls;)V

    .line 86
    new-instance p1, Lcom/pspdfkit/internal/r3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/r3;-><init>(Lcom/pspdfkit/internal/m3;)V

    invoke-static {p1}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 87
    :cond_1
    sget-object p1, Lcom/pspdfkit/internal/ls;->a:Lcom/pspdfkit/internal/ls;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/ls;)V

    if-nez p2, :cond_2

    .line 88
    new-instance p2, Ljava/lang/IllegalStateException;

    const-string p1, "Can\'t record audio"

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 89
    :cond_2
    new-instance p1, Lcom/pspdfkit/internal/n3;

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/internal/n3;-><init>(Lcom/pspdfkit/internal/m3;Ljava/lang/Throwable;)V

    invoke-static {p1}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 90
    :cond_3
    sget-object p1, Lcom/pspdfkit/internal/ls;->a:Lcom/pspdfkit/internal/ls;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/ls;)V

    .line 91
    new-instance p1, Lcom/pspdfkit/internal/s3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/s3;-><init>(Lcom/pspdfkit/internal/m3;)V

    invoke-static {p1}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 92
    :cond_4
    sget-object p1, Lcom/pspdfkit/internal/ls;->c:Lcom/pspdfkit/internal/ls;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/ls;)V

    .line 93
    new-instance p1, Lcom/pspdfkit/internal/o3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/o3;-><init>(Lcom/pspdfkit/internal/m3;)V

    invoke-static {p1}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 94
    :cond_5
    sget-object p1, Lcom/pspdfkit/internal/ls;->b:Lcom/pspdfkit/internal/ls;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/ls;)V

    .line 95
    new-instance p1, Lcom/pspdfkit/internal/q3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/q3;-><init>(Lcom/pspdfkit/internal/m3;)V

    invoke-static {p1}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    :goto_0
    return-void
.end method

.method public final addAudioRecordingListener(Lcom/pspdfkit/ui/audio/AudioRecordingController$AudioRecordingListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b()Z
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->f:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final discardRecording()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->g:Lcom/pspdfkit/internal/k3;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k3;->b()V

    :cond_0
    return-void
.end method

.method public synthetic exitAudioRecordingMode()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/audio/AudioRecordingController$-CC;->$default$exitAudioRecordingMode(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V

    return-void
.end method

.method public final exitAudioRecordingMode(Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/m3;->a(Z)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/m3;->f:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-nez p1, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 5
    sget-object p1, Lcom/pspdfkit/internal/ls;->a:Lcom/pspdfkit/internal/ls;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/ls;)V

    const/4 p1, 0x0

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/m3;->f:Lcom/pspdfkit/annotations/SoundAnnotation;

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/m3;->b:Lcom/pspdfkit/internal/a3;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/a3;->c(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V

    :goto_0
    return-void
.end method

.method public final getAudioModeManager()Lcom/pspdfkit/ui/audio/AudioModeManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->b:Lcom/pspdfkit/internal/a3;

    return-object v0
.end method

.method public final getCurrentPosition()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->g:Lcom/pspdfkit/internal/k3;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k3;->c()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getRecordingTimeLimit()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->g:Lcom/pspdfkit/internal/k3;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k3;->d()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getVisualizerFlowable()Lio/reactivex/rxjava3/core/Flowable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->g:Lcom/pspdfkit/internal/k3;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k3;->e()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lio/reactivex/rxjava3/core/Flowable;->empty()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    const-string v1, "empty()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    return-object v0
.end method

.method public final isReady()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->g:Lcom/pspdfkit/internal/k3;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isResumed()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->g:Lcom/pspdfkit/internal/k3;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k3;->f()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-interface {p0}, Lcom/pspdfkit/ui/audio/AudioRecordingController;->exitAudioRecordingMode()V

    return-void
.end method

.method public final onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    const-string p1, "oldOrder"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "newOrder"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final pause()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->g:Lcom/pspdfkit/internal/k3;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k3;->g()V

    :cond_0
    return-void
.end method

.method public final removeAudioRecordingListener(Lcom/pspdfkit/ui/audio/AudioRecordingController$AudioRecordingListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final resume()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m3;->g:Lcom/pspdfkit/internal/k3;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k3;->h()V

    :cond_0
    return-void
.end method

.method public synthetic toggle()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/audio/AudioRecordingController$-CC;->$default$toggle(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V

    return-void
.end method

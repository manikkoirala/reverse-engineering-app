.class final Lcom/pspdfkit/internal/nr$a;
.super Lcom/pspdfkit/internal/as;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/nr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/graphics/Point;

.field final synthetic b:Lcom/pspdfkit/internal/nr;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/nr;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/nr$a;->b:Lcom/pspdfkit/internal/nr;

    invoke-direct {p0}, Lcom/pspdfkit/internal/as;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/nr;Lcom/pspdfkit/internal/nr$a-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/nr$a;-><init>(Lcom/pspdfkit/internal/nr;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/view/MotionEvent;)V
    .locals 0

    const/4 p1, 0x0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/nr$a;->a:Landroid/graphics/Point;

    return-void
.end method

.method public final d(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/nr$a;->a:Landroid/graphics/Point;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/pspdfkit/internal/nr$a;->b:Lcom/pspdfkit/internal/nr;

    invoke-static {v2}, Lcom/pspdfkit/internal/nr;->-$$Nest$fgetg(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/zf;

    move-result-object v3

    if-nez v3, :cond_0

    goto/16 :goto_0

    .line 3
    :cond_0
    invoke-static {v2}, Lcom/pspdfkit/internal/nr;->-$$Nest$fgetc(Lcom/pspdfkit/internal/nr;)Landroid/content/Context;

    move-result-object v2

    iget v3, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    .line 4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    float-to-int v5, v5

    .line 5
    invoke-static {v2, v3, v0, v4, v5}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;IIII)Z

    move-result v0

    .line 7
    iget-object v2, p0, Lcom/pspdfkit/internal/nr$a;->b:Lcom/pspdfkit/internal/nr;

    invoke-static {v2}, Lcom/pspdfkit/internal/nr;->-$$Nest$fgeth(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/dm;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/pspdfkit/internal/am;->b(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 9
    iget-object v3, p0, Lcom/pspdfkit/internal/nr$a;->b:Lcom/pspdfkit/internal/nr;

    invoke-static {v3}, Lcom/pspdfkit/internal/nr;->-$$Nest$fgeth(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/dm;

    move-result-object v4

    invoke-static {v3}, Lcom/pspdfkit/internal/nr;->-$$Nest$fgetj(Lcom/pspdfkit/internal/nr;)Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 10
    iget-object v3, p0, Lcom/pspdfkit/internal/nr$a;->b:Lcom/pspdfkit/internal/nr;

    invoke-static {v3}, Lcom/pspdfkit/internal/nr;->-$$Nest$fgetk(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/na;

    move-result-object v4

    invoke-static {v3}, Lcom/pspdfkit/internal/nr;->-$$Nest$fgetj(Lcom/pspdfkit/internal/nr;)Landroid/graphics/Matrix;

    move-result-object v3

    .line 11
    invoke-virtual {v4, p1, v3, v1}, Lcom/pspdfkit/internal/na;->a(Landroid/view/MotionEvent;Landroid/graphics/Matrix;Z)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/WidgetAnnotation;

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    .line 14
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ACRO_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/pspdfkit/internal/nr$a;->b:Lcom/pspdfkit/internal/nr;

    invoke-static {v4}, Lcom/pspdfkit/internal/nr;->-$$Nest$fgetg(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/zf;

    move-result-object v4

    .line 15
    invoke-virtual {v4}, Lcom/pspdfkit/internal/zf;->e()Lcom/pspdfkit/internal/uf;

    move-result-object v4

    invoke-interface {v4}, Lcom/pspdfkit/internal/uf;->hasFieldsCache()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 16
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v1

    .line 17
    instance-of v4, v1, Lcom/pspdfkit/forms/SignatureFormElement;

    if-eqz v4, :cond_1

    .line 18
    check-cast v1, Lcom/pspdfkit/forms/SignatureFormElement;

    .line 19
    invoke-virtual {v1}, Lcom/pspdfkit/forms/SignatureFormElement;->getOverlappingSignature()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/internal/nr$a;->b:Lcom/pspdfkit/internal/nr;

    invoke-static {p1}, Lcom/pspdfkit/internal/nr;->-$$Nest$fgetb(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/specialMode/handler/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    invoke-virtual {v1}, Lcom/pspdfkit/forms/SignatureFormElement;->getOverlappingSignature()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->setSelectedAnnotation(Lcom/pspdfkit/annotations/Annotation;)V

    return v3

    :cond_1
    if-nez v0, :cond_3

    if-nez v2, :cond_3

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/nr$a;->b:Lcom/pspdfkit/internal/nr;

    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-direct {v1, v2, p1}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/nr;->-$$Nest$fputf(Lcom/pspdfkit/internal/nr;Landroid/graphics/PointF;)V

    .line 28
    iget-object p1, p0, Lcom/pspdfkit/internal/nr$a;->b:Lcom/pspdfkit/internal/nr;

    invoke-static {p1}, Lcom/pspdfkit/internal/nr;->-$$Nest$fgetf(Lcom/pspdfkit/internal/nr;)Landroid/graphics/PointF;

    move-result-object v0

    invoke-static {p1}, Lcom/pspdfkit/internal/nr;->-$$Nest$fgeth(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 29
    iget-object p1, p0, Lcom/pspdfkit/internal/nr$a;->b:Lcom/pspdfkit/internal/nr;

    invoke-static {p1}, Lcom/pspdfkit/internal/nr;->-$$Nest$fgetb(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/specialMode/handler/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/nr$a;->b:Lcom/pspdfkit/internal/nr;

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/qr;->a(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/internal/nr$a;->b:Lcom/pspdfkit/internal/nr;

    invoke-static {p1}, Lcom/pspdfkit/internal/nr;->-$$Nest$fgetd(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/lq;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 31
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lq;->c()Lcom/pspdfkit/internal/me;

    move-result-object p1

    .line 32
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/me;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 33
    :cond_2
    iput-object v1, p0, Lcom/pspdfkit/internal/nr$a;->a:Landroid/graphics/Point;

    :cond_3
    return v3

    :cond_4
    :goto_0
    return v1
.end method

.method public final h(Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public final onDown(Landroid/view/MotionEvent;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    float-to-int p1, p1

    invoke-direct {v0, v1, p1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/pspdfkit/internal/nr$a;->a:Landroid/graphics/Point;

    return-void
.end method

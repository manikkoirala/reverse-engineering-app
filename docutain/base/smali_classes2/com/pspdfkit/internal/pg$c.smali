.class public final Lcom/pspdfkit/internal/pg$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/pg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private final c:Lcom/pspdfkit/internal/pg$d;

.field private final d:Landroid/view/View;

.field private final e:Landroid/graphics/Rect;

.field private f:I

.field private g:I


# direct methods
.method public static synthetic $r8$lambda$K6sokP6SEvM5_5LPO4fegwub3hk(Lcom/pspdfkit/internal/pg$c;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/pg$c;->c()V

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Lcom/pspdfkit/internal/pg$d;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/pg$c;->e:Landroid/graphics/Rect;

    const/4 v0, 0x0

    .line 6
    iput v0, p0, Lcom/pspdfkit/internal/pg$c;->g:I

    .line 13
    iput-object p1, p0, Lcom/pspdfkit/internal/pg$c;->a:Landroid/app/Activity;

    .line 14
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/pg$c;->d:Landroid/view/View;

    .line 15
    iput-object p2, p0, Lcom/pspdfkit/internal/pg$c;->c:Lcom/pspdfkit/internal/pg$d;

    .line 16
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/pg$c;->a(Z)V

    .line 17
    new-instance p2, Lcom/pspdfkit/internal/pg$c$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/pg$c$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/pg$c;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/pg$c;->b:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 18
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/app/Activity;Lcom/pspdfkit/internal/pg$d;Lcom/pspdfkit/internal/pg$c-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/pg$c;-><init>(Landroid/app/Activity;Lcom/pspdfkit/internal/pg$d;)V

    return-void
.end method

.method private a(Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pg$c;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/pspdfkit/internal/pg$c;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/pg$c;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 3
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/16 v4, 0x18

    if-lt v1, v4, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/pg$c;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/pg$c;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getRootWindowInsets()Landroid/view/WindowInsets;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/pg$c;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v4, p0, Lcom/pspdfkit/internal/pg$c;->e:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v4

    .line 11
    invoke-virtual {v1}, Landroid/view/WindowInsets;->getStableInsetTop()I

    move-result v4

    sub-int/2addr v0, v4

    .line 15
    invoke-virtual {v1}, Landroid/view/WindowInsets;->getStableInsetBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 18
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/pg$c;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 20
    iget v1, p0, Lcom/pspdfkit/internal/pg$c;->f:I

    if-eq v0, v1, :cond_3

    .line 21
    iget-object v1, p0, Lcom/pspdfkit/internal/pg$c;->a:Landroid/app/Activity;

    invoke-static {v1}, Lcom/pspdfkit/internal/zj;->a(Landroid/app/Activity;)I

    move-result v1

    if-le v0, v1, :cond_2

    .line 22
    iget v1, p0, Lcom/pspdfkit/internal/pg$c;->g:I

    if-nez v1, :cond_3

    .line 23
    iput v0, p0, Lcom/pspdfkit/internal/pg$c;->g:I

    if-eqz p1, :cond_3

    .line 25
    iget-object p1, p0, Lcom/pspdfkit/internal/pg$c;->c:Lcom/pspdfkit/internal/pg$d;

    invoke-interface {p1, v2}, Lcom/pspdfkit/internal/pg$d;->a(Z)V

    goto :goto_1

    .line 29
    :cond_2
    iget v1, p0, Lcom/pspdfkit/internal/pg$c;->g:I

    if-lez v1, :cond_3

    .line 30
    iput v3, p0, Lcom/pspdfkit/internal/pg$c;->g:I

    if-eqz p1, :cond_3

    .line 32
    iget-object p1, p0, Lcom/pspdfkit/internal/pg$c;->c:Lcom/pspdfkit/internal/pg$d;

    invoke-interface {p1, v3}, Lcom/pspdfkit/internal/pg$d;->a(Z)V

    .line 37
    :cond_3
    :goto_1
    iput v0, p0, Lcom/pspdfkit/internal/pg$c;->f:I

    return-void
.end method

.method private synthetic c()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/pg$c;->a(Z)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 38
    iget v0, p0, Lcom/pspdfkit/internal/pg$c;->g:I

    return v0
.end method

.method public final b()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/pg$c;->g:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final d()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pg$c;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/pg$c;->b:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.class final Lcom/pspdfkit/internal/bc$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/bc;->formDidReset(Lcom/pspdfkit/internal/jni/NativeDocument;ILjava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/pspdfkit/internal/bc;


# direct methods
.method constructor <init>(ILcom/pspdfkit/internal/bc;)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/bc$d;->a:I

    iput-object p2, p0, Lcom/pspdfkit/internal/bc$d;->b:Lcom/pspdfkit/internal/bc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 3

    .line 1
    check-cast p1, Lcom/pspdfkit/forms/FormField;

    if-nez p1, :cond_0

    goto :goto_1

    .line 2
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/bc$d;->a:I

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/hc;->a(Lcom/pspdfkit/forms/FormField;I)Lcom/pspdfkit/forms/FormElement;

    move-result-object v0

    if-nez v0, :cond_1

    goto :goto_1

    .line 3
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/bc$d;->b:Lcom/pspdfkit/internal/bc;

    invoke-static {v1}, Lcom/pspdfkit/internal/bc;->c(Lcom/pspdfkit/internal/bc;)Lcom/pspdfkit/internal/nh;

    move-result-object v1

    .line 113
    invoke-virtual {v1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;

    .line 114
    invoke-interface {v2, p1, v0}, Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;->onFormFieldReset(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/forms/FormElement;)V

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.class public final Lcom/pspdfkit/internal/xq;
.super Landroidx/appcompat/app/AppCompatDialogFragment;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/zq;


# annotations
.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\u00a2\u0006\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/pspdfkit/internal/xq;",
        "Landroidx/appcompat/app/AppCompatDialogFragment;",
        "Lcom/pspdfkit/internal/zq;",
        "<init>",
        "()V",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# static fields
.field public static final synthetic f:I


# instance fields
.field private b:Lcom/pspdfkit/internal/zq;

.field public c:Lcom/pspdfkit/internal/ar;

.field private d:Lcom/pspdfkit/internal/ar;

.field private e:Lcom/pspdfkit/internal/yq;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zq;Lcom/pspdfkit/internal/ar;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "options"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/xq;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/xq;->b:Lcom/pspdfkit/internal/zq;

    .line 4
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/xq;->a(Lcom/pspdfkit/internal/ar;)V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/xq;Lcom/pspdfkit/internal/zq;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/xq;->b:Lcom/pspdfkit/internal/zq;

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/ar;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/xq;->c:Lcom/pspdfkit/internal/ar;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zq;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/xq;->b:Lcom/pspdfkit/internal/zq;

    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    if-eqz p1, :cond_3

    const-string v0, "SETTINGS_ORIGINAL_OPTIONS"

    .line 1
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    instance-of v1, v0, Lcom/pspdfkit/internal/ar;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    check-cast v0, Lcom/pspdfkit/internal/ar;

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-eqz v0, :cond_1

    const-string v1, "<set-?>"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iput-object v0, p0, Lcom/pspdfkit/internal/xq;->c:Lcom/pspdfkit/internal/ar;

    :cond_1
    const-string v0, "SETTINGS_OPTIONS"

    .line 67
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    instance-of v1, v0, Lcom/pspdfkit/internal/ar;

    if-eqz v1, :cond_2

    move-object v2, v0

    check-cast v2, Lcom/pspdfkit/internal/ar;

    :cond_2
    if-eqz v2, :cond_3

    iput-object v2, p0, Lcom/pspdfkit/internal/xq;->d:Lcom/pspdfkit/internal/ar;

    .line 68
    :cond_3
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_Dialog_Light_Panel_Dim:I

    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Landroidx/appcompat/app/AppCompatDialogFragment;->setStyle(II)V

    .line 69
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object p1

    const-string v0, "super.onCreateDialog(savedInstanceState)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 70
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    return-object p1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/xq;->c:Lcom/pspdfkit/internal/ar;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "originalOptions"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :goto_0
    const-string v2, "SETTINGS_ORIGINAL_OPTIONS"

    .line 3
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/xq;->e:Lcom/pspdfkit/internal/yq;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/yq;->getOptions()Lcom/pspdfkit/internal/ar;

    move-result-object v1

    :cond_1
    const-string v0, "SETTINGS_OPTIONS"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    return-void
.end method

.method public final onSettingsClose()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->dismiss()V

    return-void
.end method

.method public final onSettingsSave(Lcom/pspdfkit/internal/ar;)V
    .locals 1

    const-string v0, "changedOptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xq;->b:Lcom/pspdfkit/internal/zq;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/zq;->onSettingsSave(Lcom/pspdfkit/internal/ar;)V

    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 6

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 2
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 3
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-string v2, "dlg.window ?: return"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "resources"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_width:I

    .line 196
    sget v4, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_height:I

    .line 197
    invoke-static {v2, v3, v4}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/res/Resources;II)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    const/4 v3, -0x1

    if-eqz v2, :cond_1

    const/4 v4, -0x1

    goto :goto_0

    .line 198
    :cond_1
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_width:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    :goto_0
    if-eqz v2, :cond_2

    goto :goto_1

    .line 205
    :cond_2
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_height:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v3, v2

    .line 209
    :goto_1
    invoke-virtual {v1, v4, v3}, Landroid/view/Window;->setLayout(II)V

    const/16 v2, 0x11

    .line 210
    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    const/4 v2, 0x0

    .line 211
    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    const/high16 v0, 0x4000000

    .line 217
    invoke-virtual {v1, v0}, Landroid/view/Window;->addFlags(I)V

    :cond_3
    return-void
.end method

.method public final setupDialog(Landroid/app/Dialog;I)V
    .locals 3

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/appcompat/app/AppCompatDialogFragment;->setupDialog(Landroid/app/Dialog;I)V

    .line 3
    new-instance p2, Lcom/pspdfkit/internal/yq;

    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "requireContext()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/xq;->c:Lcom/pspdfkit/internal/ar;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "originalOptions"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 5
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/xq;->d:Lcom/pspdfkit/internal/ar;

    invoke-direct {p2, v0, v1, v2, p0}, Lcom/pspdfkit/internal/yq;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ar;Lcom/pspdfkit/internal/ar;Lcom/pspdfkit/internal/zq;)V

    .line 6
    iput-object p2, p0, Lcom/pspdfkit/internal/xq;->e:Lcom/pspdfkit/internal/yq;

    .line 7
    invoke-virtual {p1, p2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/xr;
.super Lcom/pspdfkit/internal/x4;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/x4<",
        "Lcom/pspdfkit/internal/ds;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/internal/ds$a;)V
    .locals 8

    .line 3
    new-instance v7, Lcom/pspdfkit/internal/ds;

    move-object v0, v7

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/ds;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/internal/ds$a;)V

    invoke-direct {p0, v7}, Lcom/pspdfkit/internal/x4;-><init>(Lcom/pspdfkit/internal/y4;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/ds$a;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ds;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/ds;-><init>(Lcom/pspdfkit/internal/ds$a;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/x4;-><init>(Lcom/pspdfkit/internal/y4;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/ds;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/x4;-><init>(Lcom/pspdfkit/internal/y4;)V

    return-void
.end method

.method private b(FLandroid/graphics/Matrix;)Landroid/graphics/RectF;
    .locals 3

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ds;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ds;->v()Landroid/graphics/RectF;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-lez v1, :cond_1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    goto :goto_0

    .line 22
    :cond_0
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 23
    iget v0, v1, Landroid/graphics/RectF;->left:F

    mul-float v0, v0, p1

    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 24
    iget v0, v1, Landroid/graphics/RectF;->right:F

    mul-float v0, v0, p1

    iput v0, v1, Landroid/graphics/RectF;->right:F

    .line 25
    iget v0, v1, Landroid/graphics/RectF;->top:F

    mul-float v0, v0, p1

    iput v0, v1, Landroid/graphics/RectF;->top:F

    .line 26
    iget v0, v1, Landroid/graphics/RectF;->bottom:F

    mul-float v0, v0, p1

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 27
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 28
    invoke-virtual {p1, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 29
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;)V

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public final a(ILandroid/graphics/Matrix;F)Lcom/pspdfkit/annotations/Annotation;
    .locals 2

    .line 1
    invoke-direct {p0, p3, p2}, Lcom/pspdfkit/internal/xr;->b(FLandroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object p2

    const/4 p3, 0x0

    if-nez p2, :cond_0

    return-object p3

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ds;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ds;->w()Lcom/pspdfkit/internal/ds$a;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/ds$a;->a:Lcom/pspdfkit/internal/ds$a;

    if-ne v0, v1, :cond_1

    .line 5
    new-instance p3, Lcom/pspdfkit/annotations/SquareAnnotation;

    invoke-direct {p3, p1, p2}, Lcom/pspdfkit/annotations/SquareAnnotation;-><init>(ILandroid/graphics/RectF;)V

    goto :goto_0

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ds;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ds;->w()Lcom/pspdfkit/internal/ds$a;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/ds$a;->b:Lcom/pspdfkit/internal/ds$a;

    if-ne v0, v1, :cond_2

    .line 7
    new-instance p3, Lcom/pspdfkit/annotations/CircleAnnotation;

    invoke-direct {p3, p1, p2}, Lcom/pspdfkit/annotations/CircleAnnotation;-><init>(ILandroid/graphics/RectF;)V

    .line 12
    :goto_0
    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/x4;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    :cond_2
    return-object p3
.end method

.method public final a(Landroid/graphics/PointF;Landroid/graphics/Matrix;F)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-interface {v0, p1, p2, p3}, Lcom/pspdfkit/internal/br;->a(Landroid/graphics/PointF;Landroid/graphics/Matrix;F)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/br$a;)V
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/h4;->a(Lcom/pspdfkit/internal/br$a;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ri;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/h4;->a(Lcom/pspdfkit/internal/ri;)V

    return-void
.end method

.method public final a()Z
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-interface {v0}, Lcom/pspdfkit/internal/br;->a()Z

    move-result v0

    return v0
.end method

.method public final a(FLandroid/graphics/Matrix;)Z
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/h4;->a(FLandroid/graphics/Matrix;)Z

    move-result p1

    return p1
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z
    .locals 5

    .line 13
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0, p2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    const/high16 v1, 0x3f800000    # 1.0f

    div-float/2addr v1, p3

    .line 14
    invoke-virtual {v0, v1, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    .line 17
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 18
    invoke-virtual {v2, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 19
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ds;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ds;->v()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ds;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 22
    iget v1, v2, Landroid/graphics/RectF;->left:F

    iget v3, v2, Landroid/graphics/RectF;->top:F

    iget v4, v2, Landroid/graphics/RectF;->right:F

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/pspdfkit/internal/ds;->a(FFFF)V

    if-eqz p4, :cond_0

    .line 23
    sget-object v0, Lcom/pspdfkit/internal/br$a;->b:Lcom/pspdfkit/internal/br$a;

    .line 24
    iget-object v1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/h4;->a(Lcom/pspdfkit/internal/br$a;)V

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 25
    :goto_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/x4;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z

    move-result p1

    or-int/2addr p1, v0

    return p1
.end method

.method public final a(Z)Z
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/h4;->a(Z)Z

    move-result p1

    return p1
.end method

.method public final b()Z
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->k()Lcom/pspdfkit/internal/ri;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final b(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;F)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ds;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ds;->w()Lcom/pspdfkit/internal/ds$a;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/ds$a;->a:Lcom/pspdfkit/internal/ds$a;

    if-ne v0, v1, :cond_1

    instance-of v0, p1, Lcom/pspdfkit/annotations/SquareAnnotation;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "You need to pass a SquareAnnotation to this shape."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 3
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ds;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ds;->w()Lcom/pspdfkit/internal/ds$a;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/ds$a;->b:Lcom/pspdfkit/internal/ds$a;

    if-ne v0, v1, :cond_3

    instance-of v0, p1, Lcom/pspdfkit/annotations/CircleAnnotation;

    if-eqz v0, :cond_2

    goto :goto_1

    .line 4
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "You need to pass a CircleAnnotation to this shape."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0, p3, p2}, Lcom/pspdfkit/internal/xr;->b(FLandroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object p2

    if-eqz p2, :cond_4

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_4

    const/4 v0, 0x1

    .line 13
    invoke-virtual {p1, p2}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    .line 17
    :cond_4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/x4;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    or-int/2addr p1, v0

    return p1
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final hide()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->hide()V

    return-void
.end method

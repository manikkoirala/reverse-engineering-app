.class public final Lcom/pspdfkit/internal/n2;
.super Lcom/pspdfkit/internal/im$h;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/n2$a;,
        Lcom/pspdfkit/internal/n2$c;,
        Lcom/pspdfkit/internal/n2$b;
    }
.end annotation


# instance fields
.field private final d:Lcom/pspdfkit/annotations/actions/ActionResolver;

.field private final e:Lcom/pspdfkit/internal/n2$b;

.field private final f:Lcom/pspdfkit/internal/n2$c;

.field private final g:Z

.field private final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field final i:Ljava/util/ArrayList;

.field private final j:Lcom/pspdfkit/internal/xc;

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lio/reactivex/rxjava3/disposables/Disposable;

.field private final m:Lcom/pspdfkit/internal/a1;

.field private final n:Landroid/graphics/Matrix;


# direct methods
.method public static synthetic $r8$lambda$I-gKMgWT5gEyx503TSEItcVoeh8(Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/n2;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$_7gVf9V_eeMO0oja7AtSa5Ria_g(Lcom/pspdfkit/internal/n2;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/n2;->a(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$_Ezvvso1UjoOqmXBKSTUO7iBvUs(Lcom/pspdfkit/internal/n2;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/n2;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$nSRNrJJx3RMw5K-zQLthvVTTHr0(Lcom/pspdfkit/internal/n2;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/n2;->b(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$vU4sHvipBIkIEaxclmqYafa2eNo(Lcom/pspdfkit/internal/n2;Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/o0;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/n2;->b(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/o0;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/n2$b;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/n2;->e:Lcom/pspdfkit/internal/n2$b;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/n2$c;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/n2;->f:Lcom/pspdfkit/internal/n2$c;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/internal/n2;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/n2;->h:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetk(Lcom/pspdfkit/internal/n2;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/n2;->k:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetm(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/a1;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/n2;->m:Lcom/pspdfkit/internal/a1;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetn(Lcom/pspdfkit/internal/n2;)Landroid/graphics/Matrix;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/n2;->n:Landroid/graphics/Matrix;

    return-object p0
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/i;Lcom/pspdfkit/internal/n2$b;Lcom/pspdfkit/internal/n2$c;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/a1;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/im$h;-><init>(Lcom/pspdfkit/internal/im;)V

    .line 2
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/n2;->i:Ljava/util/ArrayList;

    .line 7
    new-instance p1, Lcom/pspdfkit/internal/n2$a;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/pspdfkit/internal/n2$a;-><init>(Lcom/pspdfkit/internal/n2;Lcom/pspdfkit/internal/n2$a-IA;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/n2;->j:Lcom/pspdfkit/internal/xc;

    .line 20
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/n2;->n:Landroid/graphics/Matrix;

    .line 30
    iput-object p2, p0, Lcom/pspdfkit/internal/n2;->d:Lcom/pspdfkit/annotations/actions/ActionResolver;

    .line 31
    iput-object p3, p0, Lcom/pspdfkit/internal/n2;->e:Lcom/pspdfkit/internal/n2$b;

    .line 32
    iput-object p4, p0, Lcom/pspdfkit/internal/n2;->f:Lcom/pspdfkit/internal/n2$c;

    .line 33
    invoke-virtual {p5}, Lcom/pspdfkit/configuration/PdfConfiguration;->isVideoPlaybackEnabled()Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/internal/n2;->g:Z

    .line 34
    new-instance p1, Ljava/util/ArrayList;

    invoke-virtual {p5}, Lcom/pspdfkit/configuration/PdfConfiguration;->getExcludedAnnotationTypes()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/n2;->h:Ljava/util/ArrayList;

    .line 35
    iput-object p6, p0, Lcom/pspdfkit/internal/n2;->m:Lcom/pspdfkit/internal/a1;

    return-void
.end method

.method static synthetic a(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    return-object p0
.end method

.method private static synthetic a(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfView"

    const-string v2, "Exception while retrieving link annotations."

    .line 15
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private synthetic a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 14
    iput-object p1, p0, Lcom/pspdfkit/internal/n2;->k:Ljava/util/List;

    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/n2;->h:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 18
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    return v2

    .line 19
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->LINK:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v3, :cond_2

    return v2

    .line 25
    :cond_2
    check-cast p1, Lcom/pspdfkit/annotations/LinkAnnotation;

    .line 26
    iget-boolean v0, p0, Lcom/pspdfkit/internal/n2;->g:Z

    if-nez v0, :cond_3

    .line 27
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/LinkAnnotation;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v0

    instance-of v0, v0, Lcom/pspdfkit/annotations/actions/UriAction;

    if-eqz v0, :cond_3

    .line 28
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/LinkAnnotation;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/actions/UriAction;

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/UriAction;->getUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 30
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/UriAction;->getUri()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/media/MediaUri;->parse(Ljava/lang/String;)Lcom/pspdfkit/media/MediaUri;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/media/MediaUri;->isVideoUri()Z

    move-result p1

    xor-int/2addr p1, v1

    return p1

    :cond_3
    return v1
.end method

.method static synthetic b(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    return-object p0
.end method

.method private synthetic b(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/o0;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 48
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->LINK:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_0

    .line 49
    new-instance v0, Lcom/pspdfkit/internal/kh;

    check-cast p1, Lcom/pspdfkit/annotations/LinkAnnotation;

    iget-object v1, p0, Lcom/pspdfkit/internal/n2;->d:Lcom/pspdfkit/annotations/actions/ActionResolver;

    invoke-direct {v0, p1, v1}, Lcom/pspdfkit/internal/kh;-><init>(Lcom/pspdfkit/annotations/LinkAnnotation;Lcom/pspdfkit/annotations/actions/ActionResolver;)V

    return-object v0

    .line 51
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/o0;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/o0;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    return-object v0
.end method

.method private b()V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 7
    iput-object v1, p0, Lcom/pspdfkit/internal/n2;->k:Ljava/util/List;

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    .line 10
    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v1

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/r1;->getAnnotationsAsync(I)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/n2$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/n2$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/n2;)V

    .line 11
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->doOnNext(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/n2$$ExternalSyntheticLambda1;

    invoke-direct {v1}, Lcom/pspdfkit/internal/n2$$ExternalSyntheticLambda1;-><init>()V

    .line 12
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->flatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/n2$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/n2$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/n2;)V

    .line 13
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/n2$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/n2$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/n2;)V

    .line 37
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Observable;->toList()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 45
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/n2$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/n2$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/n2;)V

    new-instance v2, Lcom/pspdfkit/internal/n2$$ExternalSyntheticLambda5;

    invoke-direct {v2}, Lcom/pspdfkit/internal/n2$$ExternalSyntheticLambda5;-><init>()V

    .line 46
    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/n2;->l:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void

    .line 47
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to load the annotations in AnnotationsSubview while the State is not initialized, meaning that the view was never bound to the page, or already recycled."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private synthetic b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfView"

    const-string v2, "Link annotations retrieved."

    .line 52
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/n2;->i:Ljava/util/ArrayList;

    monitor-enter v0

    .line 55
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/n2;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 56
    iget-object v1, p0, Lcom/pspdfkit/internal/n2;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 57
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    invoke-virtual {p0}, Lcom/pspdfkit/internal/n2;->c()V

    .line 60
    iget-object p1, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void

    :catchall_0
    move-exception p1

    .line 61
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method static synthetic c(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    return-object p0
.end method

.method static synthetic d(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    return-object p0
.end method

.method static synthetic e(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    return-object p0
.end method

.method static synthetic f(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    return-object p0
.end method

.method static synthetic g(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    return-object p0
.end method

.method static synthetic h(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    return-object p0
.end method

.method static synthetic i(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    return-object p0
.end method

.method static synthetic j(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    return-object p0
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/xc;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/n2;->j:Lcom/pspdfkit/internal/xc;

    return-object v0
.end method

.method public final a(Lcom/pspdfkit/internal/dm$e;)V
    .locals 0

    .line 2
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/im$h;->a(Lcom/pspdfkit/internal/dm$e;)V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/n2;->b()V

    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)Z
    .locals 4

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/n2;->i:Ljava/util/ArrayList;

    monitor-enter v0

    .line 5
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/n2;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/n2;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/o0;

    .line 7
    iget-object v3, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Lcom/pspdfkit/internal/o0;->a(Landroid/content/Context;Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    .line 9
    monitor-exit v0

    return p1

    :cond_1
    const/4 p1, 0x0

    .line 12
    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    .line 13
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final c()V
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/n2;->i:Ljava/util/ArrayList;

    monitor-enter v0

    .line 3
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    iget-object v2, p0, Lcom/pspdfkit/internal/n2;->n:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/im;->a(Landroid/graphics/Matrix;)V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/n2;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/o0;

    .line 5
    iget-object v3, p0, Lcom/pspdfkit/internal/n2;->n:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/o0;->a(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 7
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public final onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public final onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public final onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result p1

    if-ne v0, p1, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/n2;->b()V

    :cond_0
    return-void
.end method

.method public final onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final recycle()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/im$h;->recycle()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/n2;->l:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/n2;->l:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/n2;->i:Ljava/util/ArrayList;

    monitor-enter v0

    .line 6
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/n2;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 7
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

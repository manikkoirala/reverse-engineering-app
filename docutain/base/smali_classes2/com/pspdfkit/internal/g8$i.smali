.class public final Lcom/pspdfkit/internal/g8$i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/listeners/DocumentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/g8;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/pspdfkit/internal/g8;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/g8;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/g8$i;->b:Lcom/pspdfkit/internal/g8;

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic onDocumentClick()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentClick(Lcom/pspdfkit/listeners/DocumentListener;)Z

    move-result v0

    return v0
.end method

.method public synthetic onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentLoadFailed(Lcom/pspdfkit/listeners/DocumentListener;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 5

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/g8$i;->b:Lcom/pspdfkit/internal/g8;

    invoke-static {v0}, Lcom/pspdfkit/internal/g8;->d(Lcom/pspdfkit/internal/g8;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/g8$i;->b:Lcom/pspdfkit/internal/g8;

    invoke-static {v1}, Lcom/pspdfkit/internal/g8;->a(Lcom/pspdfkit/internal/g8;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "selectedPoints[comparisonDocumentIndex]"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    iget-object v1, p0, Lcom/pspdfkit/internal/g8$i;->b:Lcom/pspdfkit/internal/g8;

    .line 310
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v3, Landroid/graphics/PointF;

    .line 311
    invoke-static {v1, v3, v2}, Lcom/pspdfkit/internal/g8;->a(Lcom/pspdfkit/internal/g8;Landroid/graphics/PointF;I)V

    .line 312
    invoke-static {v1}, Lcom/pspdfkit/internal/g8;->e(Lcom/pspdfkit/internal/g8;)Lcom/pspdfkit/internal/ui/stepper/StepperView;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, "stepperView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v2, 0x0

    :cond_1
    invoke-virtual {v2, v4}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->a(I)V

    move v2, v4

    goto :goto_0

    .line 314
    :cond_2
    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentLoaded(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public synthetic onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSave(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result p1

    return p1
.end method

.method public synthetic onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSaveCancelled(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public synthetic onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSaveFailed(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V

    return-void
.end method

.method public synthetic onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSaved(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public synthetic onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentZoomed(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;IF)V

    return-void
.end method

.method public synthetic onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onPageChanged(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;I)V

    return-void
.end method

.method public synthetic onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onPageClick(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    return p1
.end method

.method public synthetic onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onPageUpdated(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;I)V

    return-void
.end method

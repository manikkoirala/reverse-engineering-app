.class public final Lcom/pspdfkit/internal/sc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/sc$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/sc;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/sc$a;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/sc$a;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/sc;->a:Lcom/pspdfkit/internal/sc$a;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;)V
    .locals 0

    const/16 p1, 0xa

    .line 4
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/sc;-><init>(I)V

    return-void
.end method

.method private static a(Lcom/pspdfkit/document/PdfDocument;I)Ljava/lang/String;
    .locals 4

    .line 7
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    invoke-interface {p0}, Lcom/pspdfkit/document/PdfDocument;->getUid()Ljava/lang/String;

    move-result-object p0

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const/4 p1, 0x1

    aput-object p0, v2, p1

    invoke-static {v2, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p0

    const-string p1, "d[%s]p[%d]"

    invoke-static {v0, p1, p0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "format(locale, format, *args)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/rc;)Landroid/graphics/Bitmap;
    .locals 3

    const-string v0, "renderOptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/sc;->a:Lcom/pspdfkit/internal/sc$a;

    iget-object v1, p1, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    const-string v2, "renderOptions.document"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget v2, p1, Lcom/pspdfkit/internal/g4;->d:I

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/sc;->a(Lcom/pspdfkit/document/PdfDocument;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/collection/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/d5;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/d5;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    monitor-enter v2

    .line 4
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/d5;->a(Lcom/pspdfkit/internal/rc;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_1

    monitor-exit v2

    return-object v1

    .line 5
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lcom/pspdfkit/internal/d5;->a()Landroid/graphics/Bitmap;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v2

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v2

    throw p1
.end method

.method public final a()V
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/sc;->a:Lcom/pspdfkit/internal/sc$a;

    invoke-virtual {v0}, Landroidx/collection/LruCache;->evictAll()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yh;)V
    .locals 3

    const-string v0, "renderOptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bitmap"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sc;->a:Lcom/pspdfkit/internal/sc$a;

    iget-object v1, p1, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    const-string v2, "renderOptions.document"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget v2, p1, Lcom/pspdfkit/internal/g4;->d:I

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/sc;->a(Lcom/pspdfkit/document/PdfDocument;I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/d5;

    invoke-direct {v2, p1, p2}, Lcom/pspdfkit/internal/d5;-><init>(Lcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yh;)V

    invoke-virtual {v0, v1, v2}, Landroidx/collection/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final b(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sc;->a:Lcom/pspdfkit/internal/sc$a;

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/sc;->a(Lcom/pspdfkit/document/PdfDocument;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroidx/collection/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

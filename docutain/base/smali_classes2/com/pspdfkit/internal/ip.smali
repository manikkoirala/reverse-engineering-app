.class public final Lcom/pspdfkit/internal/ip;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ip$a;,
        Lcom/pspdfkit/internal/ip$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/ip$b;


# instance fields
.field private final a:Lcom/pspdfkit/internal/d7;

.field public b:Landroid/graphics/Bitmap;

.field private c:Ljava/util/UUID;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/ip$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/ip$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/ip;->Companion:Lcom/pspdfkit/internal/ip$b;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/pspdfkit/internal/d7;)V
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    and-int/lit8 v0, p1, 0x1

    const/4 v1, 0x1

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/ip$a;->a:Lcom/pspdfkit/internal/ip$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ip$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/ip;->a:Lcom/pspdfkit/internal/d7;

    .line 7
    new-instance p1, Ljava/util/UUID;

    const-wide/16 v0, 0x0

    invoke-direct {p1, v0, v1, v0, v1}, Ljava/util/UUID;-><init>(JJ)V

    .line 8
    iput-object p1, p0, Lcom/pspdfkit/internal/ip;->c:Ljava/util/UUID;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/ip;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/d7$a;->a:Lcom/pspdfkit/internal/d7$a;

    iget-object p0, p0, Lcom/pspdfkit/internal/ip;->a:Lcom/pspdfkit/internal/d7;

    const/4 v1, 0x0

    invoke-interface {p1, p2, v1, v0, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/d7;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ip;->a:Lcom/pspdfkit/internal/d7;

    return-object v0
.end method

.method public final a(Ljava/util/UUID;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/ip;->c:Ljava/util/UUID;

    return-void
.end method

.method public final b()Ljava/util/UUID;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ip;->c:Ljava/util/UUID;

    return-object v0
.end method

.method public final c()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ip;->a:Lcom/pspdfkit/internal/d7;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d7;->a()Lcom/pspdfkit/internal/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jv;->a()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public final d()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ip;->a:Lcom/pspdfkit/internal/d7;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d7;->a()Lcom/pspdfkit/internal/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jv;->b()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.class public final Lcom/pspdfkit/internal/g9;
.super Lcom/pspdfkit/internal/f9;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/metadata/DocumentPdfMetadata;


# static fields
.field private static final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const-string v0, "Title"

    const-string v1, "Author"

    const-string v2, "Subject"

    const-string v3, "Keywords"

    const-string v4, "Creator"

    const-string v5, "Producer"

    const-string v6, "CreationDate"

    const-string v7, "ModDate"

    .line 8
    filled-new-array/range {v0 .. v7}, [Ljava/lang/String;

    move-result-object v0

    .line 9
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/g9;->e:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;Z)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/f9;-><init>(Lcom/pspdfkit/internal/zf;Z)V

    return-void
.end method


# virtual methods
.method public final get(Ljava/lang/String;)Lcom/pspdfkit/document/PdfValue;
    .locals 2

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->b()Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;->getFromPDF(Ljava/lang/String;Ljava/lang/Integer;)Lcom/pspdfkit/internal/jni/NativePDFObject;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/ej;->a(Lcom/pspdfkit/internal/jni/NativePDFObject;)Lcom/pspdfkit/document/PdfValue;

    move-result-object p1

    return-object p1
.end method

.method public final getAuthor()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->c()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "Author"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getCreationDate()Ljava/util/Date;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->c()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "CreationDate"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2
    :cond_0
    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeDateUtilities;->stringToPdfDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public final getCreator()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->c()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "Creator"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getKeywords()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->c()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "Keywords"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 2
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3
    new-instance v3, Lkotlin/text/Regex;

    const-string v4, "\\s*,\\s*"

    invoke-direct {v3, v4}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Lkotlin/text/Regex;->split(Ljava/lang/CharSequence;I)Ljava/util/List;

    move-result-object v3

    .line 194
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v5

    const/4 v6, 0x1

    if-nez v5, :cond_3

    .line 195
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v3, v5}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v5

    .line 196
    :cond_1
    invoke-interface {v5}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 197
    invoke-interface {v5}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 198
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_2

    const/4 v7, 0x1

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    :goto_0
    if-nez v7, :cond_1

    .line 393
    invoke-interface {v5}, Ljava/util/ListIterator;->nextIndex()I

    move-result v5

    add-int/2addr v5, v6

    invoke-static {v3, v5}, Lkotlin/collections/CollectionsKt;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v3

    goto :goto_1

    .line 397
    :cond_3
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v3

    :goto_1
    new-array v5, v4, [Ljava/lang/String;

    .line 399
    invoke-interface {v3, v5}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    .line 400
    check-cast v3, [Ljava/lang/String;

    array-length v5, v3

    const/4 v7, 0x0

    :goto_2
    if-ge v7, v5, :cond_5

    aget-object v8, v3, v7

    .line 401
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 402
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 404
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_6

    const/4 v4, 0x1

    :cond_6
    if-eqz v4, :cond_7

    goto :goto_3

    :cond_7
    move-object v1, v2

    :goto_3
    return-object v1
.end method

.method public final getMetadata()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/document/PdfValue;",
            ">;"
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->b()Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;->getTopLevelKeysFromPDF(Ljava/lang/Integer;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->b()Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;->getFromPDF(Ljava/lang/String;Ljava/lang/Integer;)Lcom/pspdfkit/internal/jni/NativePDFObject;

    move-result-object v4

    .line 6
    invoke-static {v4}, Lcom/pspdfkit/internal/ej;->a(Lcom/pspdfkit/internal/jni/NativePDFObject;)Lcom/pspdfkit/document/PdfValue;

    move-result-object v4

    if-nez v4, :cond_0

    goto :goto_0

    :cond_0
    const-string v5, "MetadataConverters.nativ\u2026            ) ?: continue"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "key"

    .line 9
    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 12
    :cond_1
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getModificationDate()Ljava/util/Date;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->c()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "ModDate"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2
    :cond_0
    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeDateUtilities;->stringToPdfDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public final getProducer()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->c()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "Producer"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getSubject()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->c()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "Subject"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->c()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "Title"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final set(Ljava/lang/String;Lcom/pspdfkit/document/PdfValue;)V
    .locals 3

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4
    monitor-enter p0

    .line 5
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->b()Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;

    move-result-object v0

    invoke-static {p2}, Lcom/pspdfkit/internal/ej;->a(Lcom/pspdfkit/document/PdfValue;)Lcom/pspdfkit/internal/jni/NativePDFObject;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;->setInPDF(Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativePDFObject;Ljava/lang/Integer;)V

    .line 7
    sget-object v0, Lcom/pspdfkit/internal/g9;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->c()Ljava/util/HashMap;

    move-result-object p2

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->c()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p2}, Lcom/pspdfkit/document/PdfValue;->getString()Ljava/lang/String;

    move-result-object p2

    const-string v1, "value.string"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f9;->e()V

    .line 16
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    .line 18
    :cond_2
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Document metadata are read-only!"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final setAuthor(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1
    new-instance v0, Lcom/pspdfkit/document/PdfValue;

    invoke-direct {v0, p1}, Lcom/pspdfkit/document/PdfValue;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string p1, "Author"

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/g9;->set(Ljava/lang/String;Lcom/pspdfkit/document/PdfValue;)V

    return-void
.end method

.method public final setCreationDate(Ljava/util/Date;)V
    .locals 2

    const-string v0, "CreationDate"

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 1
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/g9;->set(Ljava/lang/String;Lcom/pspdfkit/document/PdfValue;)V

    goto :goto_0

    .line 3
    :cond_0
    new-instance v1, Lcom/pspdfkit/document/PdfValue;

    invoke-static {p1}, Lcom/pspdfkit/internal/jni/NativeDateUtilities;->pdfDateToString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/pspdfkit/document/PdfValue;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/g9;->set(Ljava/lang/String;Lcom/pspdfkit/document/PdfValue;)V

    :goto_0
    return-void
.end method

.method public final setCreator(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1
    new-instance v0, Lcom/pspdfkit/document/PdfValue;

    invoke-direct {v0, p1}, Lcom/pspdfkit/document/PdfValue;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string p1, "Creator"

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/g9;->set(Ljava/lang/String;Lcom/pspdfkit/document/PdfValue;)V

    return-void
.end method

.method public final setKeywords(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "Keywords"

    if-eqz p1, :cond_3

    .line 1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 4
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    .line 5
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    :goto_0
    if-ge v2, v3, :cond_2

    .line 6
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 7
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v2, v4, :cond_1

    const-string v4, ","

    .line 9
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 12
    :cond_2
    new-instance p1, Lcom/pspdfkit/document/PdfValue;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/pspdfkit/document/PdfValue;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/g9;->set(Ljava/lang/String;Lcom/pspdfkit/document/PdfValue;)V

    goto :goto_2

    :cond_3
    :goto_1
    const/4 p1, 0x0

    .line 13
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/g9;->set(Ljava/lang/String;Lcom/pspdfkit/document/PdfValue;)V

    :goto_2
    return-void
.end method

.method public final setModificationDate(Ljava/util/Date;)V
    .locals 2

    const-string v0, "ModDate"

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 1
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/g9;->set(Ljava/lang/String;Lcom/pspdfkit/document/PdfValue;)V

    goto :goto_0

    .line 3
    :cond_0
    new-instance v1, Lcom/pspdfkit/document/PdfValue;

    invoke-static {p1}, Lcom/pspdfkit/internal/jni/NativeDateUtilities;->pdfDateToString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/pspdfkit/document/PdfValue;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/g9;->set(Ljava/lang/String;Lcom/pspdfkit/document/PdfValue;)V

    :goto_0
    return-void
.end method

.method public final setProducer(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1
    new-instance v0, Lcom/pspdfkit/document/PdfValue;

    invoke-direct {v0, p1}, Lcom/pspdfkit/document/PdfValue;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string p1, "Producer"

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/g9;->set(Ljava/lang/String;Lcom/pspdfkit/document/PdfValue;)V

    return-void
.end method

.method public final setSubject(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1
    new-instance v0, Lcom/pspdfkit/document/PdfValue;

    invoke-direct {v0, p1}, Lcom/pspdfkit/document/PdfValue;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string p1, "Subject"

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/g9;->set(Ljava/lang/String;Lcom/pspdfkit/document/PdfValue;)V

    return-void
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1
    new-instance v0, Lcom/pspdfkit/document/PdfValue;

    invoke-direct {v0, p1}, Lcom/pspdfkit/document/PdfValue;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string p1, "Title"

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/g9;->set(Ljava/lang/String;Lcom/pspdfkit/document/PdfValue;)V

    return-void
.end method

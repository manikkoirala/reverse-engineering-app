.class public final Lcom/pspdfkit/internal/se;
.super Lcom/pspdfkit/internal/j2;
.source "SourceFile"


# instance fields
.field private final p:Lcom/pspdfkit/internal/no;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/no<",
            "Lcom/pspdfkit/internal/we;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$vKqt2rxlwZlk7l1QYiI_UmlFWmA(Lcom/pspdfkit/internal/se;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/we;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/se;->a(Lcom/pspdfkit/internal/se;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/we;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/instant/ui/InstantPdfFragment;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fragment"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p3, p2}, Lcom/pspdfkit/internal/j2;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/PdfFragment;)V

    .line 3
    new-instance p1, Lcom/pspdfkit/internal/no;

    const/4 p2, 0x3

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/no;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/se;->p:Lcom/pspdfkit/internal/no;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/se;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/we;
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    new-instance v0, Lcom/pspdfkit/internal/we;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/j2;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/j2;->a()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p0

    invoke-direct {v0, v1, p0, p1}, Lcom/pspdfkit/internal/we;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;)Lcom/pspdfkit/internal/views/annotations/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            "Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;",
            ")",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;"
        }
    .end annotation

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationRenderStrategy"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/j2;->c()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v1, v2, :cond_0

    .line 5
    move-object v1, p1

    check-cast v1, Lcom/pspdfkit/annotations/StampAnnotation;

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/StampAnnotation;->hasBitmap()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7
    iget-object p2, p0, Lcom/pspdfkit/internal/se;->p:Lcom/pspdfkit/internal/no;

    new-instance v1, Lcom/pspdfkit/internal/se$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, v0}, Lcom/pspdfkit/internal/se$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/se;Lcom/pspdfkit/document/PdfDocument;)V

    invoke-virtual {p2, v1}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/no$a;)Lcom/pspdfkit/internal/mo;

    move-result-object p2

    const-string v0, "imageStampAnnotationView\u2026onfiguration, document) }"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/pspdfkit/internal/we;

    .line 8
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/we;->setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V

    const-string p1, "annotationView"

    .line 9
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-virtual {p0}, Lcom/pspdfkit/internal/j2;->d()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p2

    .line 15
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/internal/j2;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object p1

    return-object p1

    .line 16
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Annotation view can be created only while document is loaded!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Lcom/pspdfkit/internal/views/annotations/a;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "annotationView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 19
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_1

    .line 20
    instance-of p1, p1, Lcom/pspdfkit/internal/we;

    return p1

    .line 22
    :cond_1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/j2;->a(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result p1

    return p1
.end method

.method public final b(Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "annotationView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/internal/we;

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/se;->p:Lcom/pspdfkit/internal/no;

    move-object v1, p1

    check-cast v1, Lcom/pspdfkit/internal/mo;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/mo;)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/j2;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5
    :cond_0
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/j2;->b(Lcom/pspdfkit/internal/views/annotations/a;)V

    :goto_0
    return-void
.end method

.class public final Lcom/pspdfkit/internal/wf;
.super Lcom/pspdfkit/internal/zf;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/instant/document/InstantPdfDocument;


# instance fields
.field final K:Lcom/pspdfkit/internal/d2;

.field private final L:Lcom/pspdfkit/instant/client/InstantClient;

.field private final M:Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

.field private final N:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/pspdfkit/instant/client/InstantClient;Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;Ljava/util/EnumSet;Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/internal/jni/NativeDocument;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/instant/client/InstantClient;",
            "Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;",
            ">;",
            "Lcom/pspdfkit/internal/xe;",
            "Lcom/pspdfkit/internal/jni/NativeDocument;",
            ")V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/wf$a;

    invoke-direct {v0, p2, p4}, Lcom/pspdfkit/internal/wf$a;-><init>(Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;Lcom/pspdfkit/internal/xe;)V

    const/4 p4, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p5, p4, v0, v1}, Lcom/pspdfkit/internal/zf;-><init>(Lcom/pspdfkit/internal/jni/NativeDocument;ZLcom/pspdfkit/internal/z7;Lcom/pspdfkit/document/DocumentSource;)V

    .line 29
    iput-object p1, p0, Lcom/pspdfkit/internal/wf;->L:Lcom/pspdfkit/instant/client/InstantClient;

    .line 30
    iput-object p2, p0, Lcom/pspdfkit/internal/wf;->M:Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    .line 32
    invoke-direct {p0, p3}, Lcom/pspdfkit/internal/wf;->a(Ljava/util/EnumSet;)V

    .line 33
    invoke-virtual {p3}, Ljava/util/EnumSet;->clone()Ljava/util/EnumSet;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/wf;->N:Ljava/util/EnumSet;

    .line 36
    invoke-super {p0, p4}, Lcom/pspdfkit/internal/zf;->setAutomaticLinkGenerationEnabled(Z)V

    .line 39
    new-instance p1, Lcom/pspdfkit/internal/d2;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/d2;-><init>(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/wf;->K:Lcom/pspdfkit/internal/d2;

    return-void
.end method

.method public static a(Lcom/pspdfkit/instant/client/InstantClient;Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;Ljava/util/EnumSet;Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/internal/jni/NativeDocument;)Lcom/pspdfkit/internal/wf;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/instant/client/InstantClient;",
            "Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;",
            ">;",
            "Lcom/pspdfkit/internal/xe;",
            "Lcom/pspdfkit/internal/jni/NativeDocument;",
            ")",
            "Lcom/pspdfkit/internal/wf;"
        }
    .end annotation

    .line 1
    new-instance v6, Lcom/pspdfkit/internal/wf;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/wf;-><init>(Lcom/pspdfkit/instant/client/InstantClient;Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;Ljava/util/EnumSet;Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/internal/jni/NativeDocument;)V

    return-object v6
.end method

.method private a(Ljava/util/EnumSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;",
            ">;)V"
        }
    .end annotation

    const-string v0, "capabilities"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;->WRITE:Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;

    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->getPermissions()Ljava/util/EnumSet;

    move-result-object p1

    .line 57
    sget-object v0, Lcom/pspdfkit/document/DocumentPermissions;->ANNOTATIONS_AND_FORMS:Lcom/pspdfkit/document/DocumentPermissions;

    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 58
    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->B:Ljava/util/EnumSet;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->N:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;->WRITE:Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->isValidForEditing()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final addInstantDocumentListener(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->M:Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    .line 55
    invoke-virtual {v0}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getInternal()Lcom/pspdfkit/internal/vf;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->d()Lcom/pspdfkit/internal/ef;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/hf;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/hf;-><init>(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V

    .line 57
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ef;->a(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V

    return-void
.end method

.method public final declared-synchronized b(Ljava/util/EnumSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "capabilities"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->N:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    .line 58
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->N:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->clear()V

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->N:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    .line 60
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->getPermissions()Ljava/util/EnumSet;

    move-result-object v0

    .line 61
    sget-object v1, Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;->WRITE:Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;

    invoke-virtual {p1, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 62
    sget-object p1, Lcom/pspdfkit/document/DocumentPermissions;->ANNOTATIONS_AND_FORMS:Lcom/pspdfkit/document/DocumentPermissions;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 64
    :cond_1
    sget-object p1, Lcom/pspdfkit/document/DocumentPermissions;->ANNOTATIONS_AND_FORMS:Lcom/pspdfkit/document/DocumentPermissions;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 66
    :goto_0
    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->B:Ljava/util/EnumSet;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final bridge synthetic c()Lcom/pspdfkit/internal/qf;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/wf;->s()Lcom/pspdfkit/internal/qe;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/wf;->s()Lcom/pspdfkit/internal/qe;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getAnnotationProvider()Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/wf;->s()Lcom/pspdfkit/internal/qe;

    move-result-object v0

    return-object v0
.end method

.method public final getBookmarkProvider()Lcom/pspdfkit/bookmarks/BookmarkProvider;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Bookmarks are not supported in instant documents!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getDelayForSyncingLocalChanges()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->K:Lcom/pspdfkit/internal/d2;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d2;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getDocumentState()Lcom/pspdfkit/instant/document/InstantDocumentState;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->M:Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getInternal()Lcom/pspdfkit/internal/vf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->g()Lcom/pspdfkit/instant/document/InstantDocumentState;

    move-result-object v0

    return-object v0
.end method

.method public final getInstantClient()Lcom/pspdfkit/instant/client/InstantClient;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->L:Lcom/pspdfkit/instant/client/InstantClient;

    return-object v0
.end method

.method public final getInstantDocumentDescriptor()Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->M:Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    return-object v0
.end method

.method public final isListeningToServerChanges()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->K:Lcom/pspdfkit/internal/d2;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d2;->d()Z

    move-result v0

    return v0
.end method

.method public final notifyConnectivityChanged(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->K:Lcom/pspdfkit/internal/d2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/d2;->b(Z)V

    if-eqz p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/wf;->M:Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    invoke-virtual {p1}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getInternal()Lcom/pspdfkit/internal/vf;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/vf;->b()Lcom/pspdfkit/internal/xe;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/xe;->a()V

    :cond_0
    return-void
.end method

.method public final reauthenticateWithJwt(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/wf;->reauthenticateWithJwtAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V

    return-void
.end method

.method public final reauthenticateWithJwtAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->M:Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getInternal()Lcom/pspdfkit/internal/vf;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vf;->e(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final removeInstantDocumentListener(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->M:Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    .line 55
    invoke-virtual {v0}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getInternal()Lcom/pspdfkit/internal/vf;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->d()Lcom/pspdfkit/internal/ef;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/hf;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/hf;-><init>(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V

    .line 57
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ef;->b(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V

    return-void
.end method

.method public final removeLocalStorage()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->M:Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->removeLocalStorage()V

    return-void
.end method

.method public final s()Lcom/pspdfkit/internal/qe;
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    instance-of v0, v0, Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider;

    if-eqz v0, :cond_0

    .line 5
    invoke-super {p0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/qe;

    return-object v0

    .line 6
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Wrong type of annotation provider type. InstantAnnotationProvider was expected!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setAutomaticLinkGenerationEnabled(Z)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 1
    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Automatic link generation is not supported for instant documents!"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final setDelayForSyncingLocalChanges(J)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->K:Lcom/pspdfkit/internal/d2;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/d2;->a(J)V

    return-void
.end method

.method public final setListenToServerChanges(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->K:Lcom/pspdfkit/internal/d2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/d2;->c(Z)V

    return-void
.end method

.method public final syncAnnotations()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/wf;->syncAnnotationsAsync()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Flowable;->blockingSubscribe()V

    return-void
.end method

.method public final syncAnnotationsAsync()Lio/reactivex/rxjava3/core/Flowable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Lcom/pspdfkit/instant/client/InstantProgress;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wf;->K:Lcom/pspdfkit/internal/d2;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/d2;->a(ZZ)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    return-object v0
.end method

.method public final wasModified()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

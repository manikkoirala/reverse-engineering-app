.class public final Lcom/pspdfkit/internal/vu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/undo/UndoManager;
.implements Lcom/pspdfkit/internal/fl;


# instance fields
.field private final a:Ljava/util/ArrayDeque;

.field private final b:Ljava/util/ArrayDeque;

.field private final c:I

.field private final d:Lcom/pspdfkit/internal/uu;

.field private final e:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field private g:Z


# direct methods
.method public static synthetic $r8$lambda$tyKjd1CWYt8Qn-aBr1AfE-OoqAs(Lcom/pspdfkit/internal/vu;Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/vu;->a(Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/vu;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/vu;->e:Lcom/pspdfkit/internal/nh;

    const/4 p1, 0x1

    .line 6
    iput-boolean p1, p0, Lcom/pspdfkit/internal/vu;->f:Z

    .line 9
    iput-boolean p1, p0, Lcom/pspdfkit/internal/vu;->g:Z

    const/16 p1, 0x64

    .line 33
    iput p1, p0, Lcom/pspdfkit/internal/vu;->c:I

    .line 34
    new-instance p1, Ljava/util/ArrayDeque;

    const/16 v0, 0x65

    invoke-direct {p1, v0}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/vu;->a:Ljava/util/ArrayDeque;

    .line 35
    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1, v0}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/vu;->b:Ljava/util/ArrayDeque;

    .line 37
    new-instance p1, Lcom/pspdfkit/internal/uu;

    invoke-direct {p1}, Lcom/pspdfkit/internal/uu;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/vu;->d:Lcom/pspdfkit/internal/uu;

    .line 40
    new-instance v0, Lcom/pspdfkit/internal/w5;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/w5;-><init>(Lcom/pspdfkit/internal/uu;)V

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/uu;->a(Lcom/pspdfkit/internal/k4;)V

    return-void
.end method

.method private a()V
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->e:Lcom/pspdfkit/internal/nh;

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 109
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/vu$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/vu$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/vu;)V

    .line 110
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 111
    invoke-interface {p1, p0}, Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;->onUndoHistoryChanged(Lcom/pspdfkit/undo/UndoManager;)V

    return-void
.end method

.method private declared-synchronized b(Lcom/pspdfkit/internal/ja;)Z
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->d:Lcom/pspdfkit/internal/uu;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/uu;->a(Lcom/pspdfkit/internal/ja;)Lcom/pspdfkit/internal/tu;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/tu;->d(Lcom/pspdfkit/internal/ja;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized c(Lcom/pspdfkit/internal/ja;)Z
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->d:Lcom/pspdfkit/internal/uu;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/uu;->a(Lcom/pspdfkit/internal/ja;)Lcom/pspdfkit/internal/tu;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/tu;->c(Lcom/pspdfkit/internal/ja;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    const-string v0, "allowedActions"

    const-string v1, "argumentName"

    .line 54
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 105
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/vn;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 106
    :goto_0
    iput-boolean v2, p0, Lcom/pspdfkit/internal/vu;->f:Z

    const/4 v2, 0x3

    if-ne p1, v2, :cond_1

    const/4 v0, 0x1

    .line 107
    :cond_1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/vu;->g:Z

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ja;)V
    .locals 3

    .line 112
    monitor-enter p0

    :try_start_0
    const-string v0, "edit"

    const-string v1, "Trying to add a null object to the edit history."

    .line 113
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 115
    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Inserted Edit into the history stack. Edit = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.UndoRedo"

    invoke-static {v2, p1, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    iget-object p1, p0, Lcom/pspdfkit/internal/vu;->b:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->clear()V

    new-array p1, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.UndoRedo"

    const-string v2, "Redo history has been discarded since new Edit was added."

    .line 120
    invoke-static {v1, v2, p1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    iget-object p1, p0, Lcom/pspdfkit/internal/vu;->a:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->size()I

    move-result p1

    iget v1, p0, Lcom/pspdfkit/internal/vu;->c:I

    if-le p1, v1, :cond_0

    .line 125
    iget-object p1, p0, Lcom/pspdfkit/internal/vu;->a:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.UndoRedo"

    const-string v1, "New Edit was added to the history stack, increasing the size of the stack over the max allowed value. The oldest Edit was discarded to make space."

    .line 126
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/vu;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized a(Lcom/pspdfkit/internal/k4;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "executor"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "executor"

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->d:Lcom/pspdfkit/internal/uu;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/uu;->a(Lcom/pspdfkit/internal/k4;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final addOnUndoHistoryChangeListener(Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final declared-synchronized canRedo()Z
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/vu;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->peekLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ja;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/vu;->b(Lcom/pspdfkit/internal/ja;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized canUndo()Z
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/vu;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->peekLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ja;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/vu;->c(Lcom/pspdfkit/internal/ja;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearHistory()V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/vu;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized redo()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "Invoking redo action for edit = "

    const-string v1, "Trying to invoke redo action on Edit that\'s not redoable. Edit = "

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->p()Lcom/pspdfkit/internal/fn;

    move-result-object v2

    const-string v3, "redo"

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/fn;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5
    :try_start_1
    iget-object v2, p0, Lcom/pspdfkit/internal/vu;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/internal/vu;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->peekLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/ja;

    .line 10
    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/vu;->b(Lcom/pspdfkit/internal/ja;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/vu;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v1, v2}, Ljava/util/ArrayDeque;->remove(Ljava/lang/Object;)Z

    const-string v1, "PSPDFKit.UndoRedo"

    .line 12
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v3}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->d:Lcom/pspdfkit/internal/uu;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/uu;->a(Lcom/pspdfkit/internal/ja;)Lcom/pspdfkit/internal/tu;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/pspdfkit/internal/tu;->a(Lcom/pspdfkit/internal/ja;)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0, v2}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 15
    invoke-direct {p0}, Lcom/pspdfkit/internal/vu;->a()V
    :try_end_1
    .catch Lcom/pspdfkit/undo/exceptions/RedoEditFailedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    .line 17
    :cond_0
    :try_start_2
    new-instance v0, Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 18
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_1
    new-instance v0, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;

    const-string v1, "There are no Edits scheduled for redo action."

    invoke-direct {v0, v1}, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/pspdfkit/undo/exceptions/RedoEditFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 35
    :try_start_3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/vu;->clearHistory()V

    .line 36
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 39
    :goto_0
    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final removeOnUndoHistoryChangeListener(Lcom/pspdfkit/undo/OnUndoHistoryChangeListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final declared-synchronized undo()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "Invoking undo action for edit = "

    const-string v1, "Trying to invoke undo action on Edit that\'s not undoable. Edit = "

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->p()Lcom/pspdfkit/internal/fn;

    move-result-object v2

    const-string v3, "undo"

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/fn;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5
    :try_start_1
    iget-object v2, p0, Lcom/pspdfkit/internal/vu;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/internal/vu;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->peekLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/ja;

    .line 11
    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/vu;->c(Lcom/pspdfkit/internal/ja;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/vu;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v1, v2}, Ljava/util/ArrayDeque;->remove(Ljava/lang/Object;)Z

    const-string v1, "PSPDFKit.UndoRedo"

    .line 13
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v3}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->d:Lcom/pspdfkit/internal/uu;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/uu;->a(Lcom/pspdfkit/internal/ja;)Lcom/pspdfkit/internal/tu;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/pspdfkit/internal/tu;->b(Lcom/pspdfkit/internal/ja;)V

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/vu;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0, v2}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 17
    invoke-direct {p0}, Lcom/pspdfkit/internal/vu;->a()V
    :try_end_1
    .catch Lcom/pspdfkit/undo/exceptions/UndoEditFailedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    .line 20
    :cond_0
    :try_start_2
    new-instance v0, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 21
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_1
    new-instance v0, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;

    const-string v1, "There are no Edits scheduled for undo action."

    invoke-direct {v0, v1}, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/pspdfkit/undo/exceptions/UndoEditFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 40
    :try_start_3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/vu;->clearHistory()V

    .line 41
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 44
    :goto_0
    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

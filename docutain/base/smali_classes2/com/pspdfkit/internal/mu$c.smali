.class final Lcom/pspdfkit/internal/mu$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/mu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/dm$e;

.field final b:Landroid/graphics/Rect;

.field private final c:Lio/reactivex/rxjava3/functions/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/functions/Consumer<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lio/reactivex/rxjava3/functions/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/functions/Consumer<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field e:Landroid/graphics/Bitmap;

.field f:Z

.field private g:Lio/reactivex/rxjava3/disposables/Disposable;

.field private final h:Lcom/pspdfkit/internal/gm$a;

.field final synthetic i:Lcom/pspdfkit/internal/mu;


# direct methods
.method static bridge synthetic -$$Nest$fgetg(Lcom/pspdfkit/internal/mu$c;)Lio/reactivex/rxjava3/disposables/Disposable;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/mu$c;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputg(Lcom/pspdfkit/internal/mu$c;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/mu$c;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/mu;Lcom/pspdfkit/internal/dm$e;Landroid/graphics/Rect;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/mu$c;->i:Lcom/pspdfkit/internal/mu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/mu$c;->f:Z

    const-string v0, "state was null"

    .line 18
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iput-object p2, p0, Lcom/pspdfkit/internal/mu$c;->a:Lcom/pspdfkit/internal/dm$e;

    .line 23
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/mu$c;->b:Landroid/graphics/Rect;

    .line 24
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/n4;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    .line 25
    new-instance v0, Lcom/pspdfkit/internal/mu$a;

    invoke-direct {v0, p3}, Lcom/pspdfkit/internal/mu$a;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/mu$c;->d:Lio/reactivex/rxjava3/functions/Consumer;

    .line 26
    new-instance v0, Lcom/pspdfkit/internal/mu$b;

    invoke-direct {v0, p1, p0}, Lcom/pspdfkit/internal/mu$b;-><init>(Lcom/pspdfkit/internal/mu;Lcom/pspdfkit/internal/mu$c;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/mu$c;->c:Lio/reactivex/rxjava3/functions/Consumer;

    .line 29
    new-instance p1, Lcom/pspdfkit/internal/gm$a;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    invoke-virtual {p2}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v1

    invoke-direct {p1, v0, v1}, Lcom/pspdfkit/internal/gm$a;-><init>(Lcom/pspdfkit/internal/zf;I)V

    .line 30
    invoke-virtual {p2}, Lcom/pspdfkit/internal/dm$e;->d()Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/gm$a;->b(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/gm$a;

    move-result-object p1

    .line 31
    invoke-virtual {p2}, Lcom/pspdfkit/internal/dm$e;->b()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/g4$a;->a(Z)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/gm$a;

    iget-object p2, p0, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    .line 32
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/g4$a;->a(Landroid/graphics/Bitmap;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/gm$a;

    iget-object p2, p0, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    .line 33
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/g4$a;->b(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/gm$a;

    iget-object p2, p0, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    .line 34
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/g4$a;->a(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/gm$a;

    iget p2, p3, Landroid/graphics/Rect;->left:I

    neg-int p2, p2

    .line 35
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/gm$a;->f(I)Lcom/pspdfkit/internal/gm$a;

    move-result-object p1

    iget p2, p3, Landroid/graphics/Rect;->top:I

    neg-int p2, p2

    .line 36
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/gm$a;->g(I)Lcom/pspdfkit/internal/gm$a;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/mu$c;->h:Lcom/pspdfkit/internal/gm$a;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mu$c;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/mu$c;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    const/4 v0, 0x0

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/internal/mu$c;->f:Z

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/mu$c;->a:Lcom/pspdfkit/internal/dm$e;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->g()Lcom/pspdfkit/utils/Size;

    move-result-object v1

    iget v1, v1, Lcom/pspdfkit/utils/Size;->width:F

    iget-object v2, p0, Lcom/pspdfkit/internal/mu$c;->i:Lcom/pspdfkit/internal/mu;

    invoke-static {v2}, Lcom/pspdfkit/internal/mu;->-$$Nest$fgeti(Lcom/pspdfkit/internal/mu;)F

    move-result v2

    mul-float v1, v1, v2

    float-to-int v1, v1

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/mu$c;->a:Lcom/pspdfkit/internal/dm$e;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm$e;->g()Lcom/pspdfkit/utils/Size;

    move-result-object v2

    iget v2, v2, Lcom/pspdfkit/utils/Size;->height:F

    iget-object v3, p0, Lcom/pspdfkit/internal/mu$c;->i:Lcom/pspdfkit/internal/mu;

    invoke-static {v3}, Lcom/pspdfkit/internal/mu;->-$$Nest$fgeti(Lcom/pspdfkit/internal/mu;)F

    move-result v4

    mul-float v2, v2, v4

    float-to-int v2, v2

    .line 8
    invoke-static {v3}, Lcom/pspdfkit/internal/mu;->-$$Nest$fgeth(Lcom/pspdfkit/internal/mu;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f99999a    # 1.2f

    mul-float v3, v3, v4

    int-to-float v5, v1

    cmpl-float v3, v3, v5

    if-gtz v3, :cond_0

    iget-object v3, p0, Lcom/pspdfkit/internal/mu$c;->i:Lcom/pspdfkit/internal/mu;

    invoke-static {v3}, Lcom/pspdfkit/internal/mu;->-$$Nest$fgeth(Lcom/pspdfkit/internal/mu;)Landroid/graphics/Rect;

    move-result-object v3

    .line 9
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    mul-float v3, v3, v4

    int-to-float v4, v2

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 11
    :cond_1
    iget-object v3, p0, Lcom/pspdfkit/internal/mu$c;->h:Lcom/pspdfkit/internal/gm$a;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/16 v0, 0xa

    .line 12
    :goto_0
    invoke-virtual {v3, v0}, Lcom/pspdfkit/internal/g4$a;->c(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/gm$a;

    .line 13
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/gm$a;->e(I)Lcom/pspdfkit/internal/gm$a;

    move-result-object v0

    .line 14
    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/gm$a;->d(I)Lcom/pspdfkit/internal/gm$a;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/mu$c;->a:Lcom/pspdfkit/internal/dm$e;

    .line 15
    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->f()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/g4$a;->b(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/gm$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/mu$c;->a:Lcom/pspdfkit/internal/dm$e;

    .line 16
    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->e()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/gm$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/mu$c;->a:Lcom/pspdfkit/internal/dm$e;

    .line 17
    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->i()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/g4$a;->b(Z)Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/gm$a;

    .line 18
    invoke-virtual {v0}, Lcom/pspdfkit/internal/gm$a;->b()Lcom/pspdfkit/internal/gm;

    move-result-object v0

    .line 19
    invoke-static {v0}, Lcom/pspdfkit/internal/hm;->a(Lcom/pspdfkit/internal/gm;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x64

    .line 20
    invoke-virtual {v0, v2, v3, v1}, Lio/reactivex/rxjava3/core/Single;->delaySubscription(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 21
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/mu$c;->c:Lio/reactivex/rxjava3/functions/Consumer;

    iget-object v2, p0, Lcom/pspdfkit/internal/mu$c;->d:Lio/reactivex/rxjava3/functions/Consumer;

    .line 22
    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/mu$c;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

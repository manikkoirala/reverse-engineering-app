.class public final Lcom/pspdfkit/internal/na;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/na$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/a1;

.field private b:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/pspdfkit/internal/na$a;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/a1;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-class v0, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/na;->b:Ljava/util/EnumSet;

    .line 8
    iput-object p1, p0, Lcom/pspdfkit/internal/na;->a:Lcom/pspdfkit/internal/a1;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;Landroid/graphics/Matrix;Z)Lcom/pspdfkit/annotations/Annotation;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/na;->a:Lcom/pspdfkit/internal/a1;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/pspdfkit/internal/a1;->a(FFLandroid/graphics/Matrix;Z)Ljava/util/List;

    move-result-object p1

    .line 3
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/annotations/Annotation;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/na;->c:Lcom/pspdfkit/internal/na$a;

    if-eqz v0, :cond_1

    invoke-interface {v0, p2}, Lcom/pspdfkit/internal/na$a;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/na;->a:Lcom/pspdfkit/internal/a1;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/a1;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p3, :cond_2

    .line 6
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/na;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    return-object p2

    :cond_4
    const/4 p1, 0x0

    return-object p1
.end method

.method public final a(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)Ljava/util/ArrayList;
    .locals 3

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/na;->a:Lcom/pspdfkit/internal/a1;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/pspdfkit/internal/a1;->a(FFLandroid/graphics/Matrix;Z)Ljava/util/List;

    move-result-object p1

    .line 9
    new-instance p2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 10
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/na;->c:Lcom/pspdfkit/internal/na$a;

    if-eqz v1, :cond_1

    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/na$a;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 12
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/na;->a:Lcom/pspdfkit/internal/a1;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/a1;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_0

    .line 13
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object p2
.end method

.method public final a(Lcom/pspdfkit/internal/na$a;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/pspdfkit/internal/na;->c:Lcom/pspdfkit/internal/na$a;

    return-void
.end method

.method public final a(Ljava/util/EnumSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)V"
        }
    .end annotation

    .line 19
    iput-object p1, p0, Lcom/pspdfkit/internal/na;->b:Ljava/util/EnumSet;

    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 3

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/pspdfkit/internal/na;->b:Ljava/util/EnumSet;

    invoke-virtual {v1, v0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 16
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationFlags;->READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 18
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public final b(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/na;->a:Lcom/pspdfkit/internal/a1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/a1;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    return p1
.end method

.class public final Lcom/pspdfkit/internal/tj;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static final a(Lcom/pspdfkit/internal/jni/NativeMeasurementPrecision;)Lcom/pspdfkit/annotations/measurements/FloatPrecision;
    .locals 3

    const-string v0, "nativePrecision"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/annotations/measurements/FloatPrecision;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    .line 42
    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received unknown native measurement precision: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static final a(Lcom/pspdfkit/internal/jni/NativeUnitFrom;)Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;
    .locals 3

    const-string v0, "nativeUnitFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    .line 48
    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received unknown native unit from: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static final a(Lcom/pspdfkit/internal/jni/NativeUnitTo;)Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;
    .locals 3

    const-string v0, "nativeUnitTo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    .line 54
    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received unknown native unit to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static final a(Lcom/pspdfkit/document/DocumentSaveOptions;Lcom/pspdfkit/internal/zf;Z)Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;
    .locals 8

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "forDocument"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeDocumentSaveFlags;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSaveOptions;->isIncremental()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeDocumentSaveFlags;->INCREMENTAL:Lcom/pspdfkit/internal/jni/NativeDocumentSaveFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 3
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSaveOptions;->shouldApplyRedactions()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeDocumentSaveFlags;->APPLYREDACTANNOTATIONS:Lcom/pspdfkit/internal/jni/NativeDocumentSaveFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 4
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSaveOptions;->shouldRewriteAndOptimizeFileSize()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeDocumentSaveFlags;->OPTIMIZEFILESIZE:Lcom/pspdfkit/internal/jni/NativeDocumentSaveFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz p2, :cond_3

    .line 5
    sget-object p2, Lcom/pspdfkit/internal/jni/NativeDocumentSaveFlags;->KEEPDIRTY:Lcom/pspdfkit/internal/jni/NativeDocumentSaveFlags;

    invoke-virtual {v0, p2}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 7
    :cond_3
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPassword()Ljava/lang/String;

    move-result-object v3

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->k()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/document/PdfVersion;->getMajorVersion()I

    move-result p2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/document/PdfVersion;->getMajorVersion()I

    move-result v1

    if-ne p2, v1, :cond_4

    .line 10
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/document/PdfVersion;->getMinorVersion()I

    move-result p2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/document/PdfVersion;->getMinorVersion()I

    move-result v1

    if-ne p2, v1, :cond_4

    .line 11
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPermissions()Ljava/util/EnumSet;

    move-result-object p2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->getPermissions()Ljava/util/EnumSet;

    move-result-object p1

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 15
    new-instance p0, Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;

    const/4 p1, 0x0

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;-><init>(Lcom/pspdfkit/internal/jni/NativeDocumentSecurityOptions;Ljava/util/EnumSet;)V

    return-object p0

    .line 17
    :cond_4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object p1

    sget-object p2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 23
    sget-object p1, Lcom/pspdfkit/internal/jni/NativeDocumentSaveFlags;->INCREMENTAL:Lcom/pspdfkit/internal/jni/NativeDocumentSaveFlags;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 24
    new-instance v6, Lcom/pspdfkit/internal/jni/NativePDFVersion;

    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/document/PdfVersion;->getMajorVersion()I

    move-result p1

    int-to-byte p1, p1

    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/document/PdfVersion;->getMinorVersion()I

    move-result p2

    int-to-byte p2, p2

    invoke-direct {v6, p1, p2}, Lcom/pspdfkit/internal/jni/NativePDFVersion;-><init>(BB)V

    .line 25
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPermissions()Ljava/util/EnumSet;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->d(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v5

    const-string p1, "permissionsToNativePermissions(permissions)"

    invoke-static {v5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    new-instance p1, Lcom/pspdfkit/internal/jni/NativeDocumentSecurityOptions;

    .line 29
    invoke-virtual {p0}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/document/PdfVersion;->getMaxEncryptionKeyLength()I

    move-result v4

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, v3

    .line 30
    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/internal/jni/NativeDocumentSecurityOptions;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/EnumSet;Lcom/pspdfkit/internal/jni/NativePDFVersion;Lcom/pspdfkit/internal/jni/NativeDocumentSecurityEncryptionAlgorithm;)V

    .line 38
    new-instance p0, Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;-><init>(Lcom/pspdfkit/internal/jni/NativeDocumentSecurityOptions;Ljava/util/EnumSet;)V

    return-object p0

    .line 39
    :cond_5
    new-instance p0, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p1, "You need document editing feature enabled in your license to change document password, version or permissions."

    invoke-direct {p0, p1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final a(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Lcom/pspdfkit/internal/jni/NativeMeasurementPrecision;
    .locals 3

    const-string v0, "floatPrecision"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeMeasurementPrecision;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeMeasurementPrecision;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    .line 45
    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received unknown float precision: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static final a(Lcom/pspdfkit/annotations/measurements/Scale;)Lcom/pspdfkit/internal/jni/NativeMeasurementScale;
    .locals 8

    const-string v0, "scale"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;

    .line 59
    iget-object v1, p0, Lcom/pspdfkit/annotations/measurements/Scale;->unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    const-string v2, "scale.unitFrom"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;)Lcom/pspdfkit/internal/jni/NativeUnitFrom;

    move-result-object v2

    .line 60
    iget-object v1, p0, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const-string v3, "scale.unitTo"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;)Lcom/pspdfkit/internal/jni/NativeUnitTo;

    move-result-object v3

    .line 61
    iget v1, p0, Lcom/pspdfkit/annotations/measurements/Scale;->valueFrom:F

    float-to-double v4, v1

    .line 62
    iget p0, p0, Lcom/pspdfkit/annotations/measurements/Scale;->valueTo:F

    float-to-double v6, p0

    move-object v1, v0

    .line 63
    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;-><init>(Lcom/pspdfkit/internal/jni/NativeUnitFrom;Lcom/pspdfkit/internal/jni/NativeUnitTo;DD)V

    return-object v0
.end method

.method public static final a(Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;)Lcom/pspdfkit/internal/jni/NativeUnitFrom;
    .locals 3

    const-string v0, "unitFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeUnitFrom;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeUnitFrom;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    .line 51
    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received unknown scale unit from: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static final a(Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;)Lcom/pspdfkit/internal/jni/NativeUnitTo;
    .locals 3

    const-string v0, "unitTo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeUnitTo;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeUnitTo;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    .line 57
    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received unknown scale unit to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

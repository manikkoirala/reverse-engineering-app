.class public final Lcom/pspdfkit/internal/h9;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/zt$d;


# instance fields
.field private final a:Lcom/pspdfkit/ui/PdfFragment;

.field private final b:Lkotlin/Lazy;

.field private final c:Lkotlin/Lazy;

.field private d:Lcom/pspdfkit/internal/zt$b;

.field private e:Lio/reactivex/rxjava3/disposables/Disposable;

.field private f:Lcom/pspdfkit/ui/PopupToolbar;

.field private g:Lcom/pspdfkit/listeners/OnPreparePopupToolbarListener;


# direct methods
.method public static synthetic $r8$lambda$jXixhIOOJ_Rz4Wp5Q5z2JffKAw8(Lcom/pspdfkit/internal/h9;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/h9;->b(Lcom/pspdfkit/internal/h9;)V

    return-void
.end method

.method public static synthetic $r8$lambda$t3io2ckQJP7mum5u9TMZWB_Xuok(Lcom/pspdfkit/internal/h7;FFILcom/pspdfkit/internal/h9;Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)Z
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/pspdfkit/internal/h9;->a(Lcom/pspdfkit/internal/h7;FFILcom/pspdfkit/internal/h9;Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)Z

    move-result p0

    return p0
.end method

.method public constructor <init>(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 1

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/h9;->a:Lcom/pspdfkit/ui/PdfFragment;

    .line 7
    new-instance p1, Lcom/pspdfkit/internal/h9$a;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/h9$a;-><init>(Lcom/pspdfkit/internal/h9;)V

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/h9;->b:Lkotlin/Lazy;

    .line 15
    new-instance p1, Lcom/pspdfkit/internal/h9$b;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/h9$b;-><init>(Lcom/pspdfkit/internal/h9;)V

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/h9;->c:Lkotlin/Lazy;

    .line 23
    sget-object p1, Lcom/pspdfkit/internal/zt$b;->a:Lcom/pspdfkit/internal/zt$b;

    iput-object p1, p0, Lcom/pspdfkit/internal/h9;->d:Lcom/pspdfkit/internal/zt$b;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/h9;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/h9;->a:Lcom/pspdfkit/ui/PdfFragment;

    return-object p0
.end method

.method private static final a(Lcom/pspdfkit/internal/h7;FFILcom/pspdfkit/internal/h9;Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;)Z
    .locals 1

    const-string v0, "$this_apply"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "this$0"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "popupToolbarMenuItem"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p5}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;->getId()I

    move-result p5

    sget v0, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_paste_annotation:I

    if-ne p5, v0, :cond_1

    .line 51
    invoke-interface {p0}, Lcom/pspdfkit/internal/h7;->a()Z

    move-result p5

    if-eqz p5, :cond_0

    .line 52
    new-instance p5, Landroid/graphics/PointF;

    invoke-direct {p5, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 53
    invoke-interface {p0, p3, p5}, Lcom/pspdfkit/internal/h7;->a(ILandroid/graphics/PointF;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    invoke-virtual {p0}, Lio/reactivex/rxjava3/core/Maybe;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    .line 54
    :cond_0
    iget-object p0, p4, Lcom/pspdfkit/internal/h9;->b:Lkotlin/Lazy;

    .line 55
    invoke-interface {p0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/ui/PopupToolbar;

    .line 56
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PopupToolbar;->dismiss()V

    :cond_1
    const/4 p0, 0x1

    return p0
.end method

.method private static final b(Lcom/pspdfkit/internal/h9;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    iget-object p0, p0, Lcom/pspdfkit/internal/h9;->f:Lcom/pspdfkit/ui/PopupToolbar;

    if-nez p0, :cond_0

    return-void

    .line 8
    :cond_0
    instance-of v0, p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;

    invoke-virtual {p0}, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->showForSelectedText()V

    goto :goto_0

    .line 9
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PopupToolbar;->showAgain()V

    :goto_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/pspdfkit/internal/h9;->f:Lcom/pspdfkit/ui/PopupToolbar;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PopupToolbar;->dismiss()V

    :cond_0
    const/4 v0, 0x0

    .line 58
    iput-object v0, p0, Lcom/pspdfkit/internal/h9;->f:Lcom/pspdfkit/ui/PopupToolbar;

    return-void
.end method

.method public final a(IFF)V
    .locals 8

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/internal/h9;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/ag;->getPasteManager()Lcom/pspdfkit/internal/h7;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 31
    invoke-interface {v2}, Lcom/pspdfkit/internal/h7;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/h9;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isCopyPasteEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/h9;->b:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/PopupToolbar;

    .line 33
    new-instance v7, Lcom/pspdfkit/internal/h9$$ExternalSyntheticLambda0;

    move-object v1, v7

    move v3, p2

    move v4, p3

    move v5, p1

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/h9$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/h7;FFILcom/pspdfkit/internal/h9;)V

    invoke-virtual {v0, v7}, Lcom/pspdfkit/ui/PopupToolbar;->setOnPopupToolbarItemClickedListener(Lcom/pspdfkit/ui/PopupToolbar$OnPopupToolbarItemClickedListener;)V

    .line 45
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h9;->a()V

    .line 46
    iget-object v0, p0, Lcom/pspdfkit/internal/h9;->b:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/PopupToolbar;

    .line 47
    invoke-virtual {v0, p1, p2, p3}, Lcom/pspdfkit/ui/PopupToolbar;->show(IFF)V

    .line 48
    iget-object p1, p0, Lcom/pspdfkit/internal/h9;->b:Lkotlin/Lazy;

    invoke-interface {p1}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/PopupToolbar;

    .line 49
    iput-object p1, p0, Lcom/pspdfkit/internal/h9;->f:Lcom/pspdfkit/ui/PopupToolbar;

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/specialMode/handler/e;)V
    .locals 2

    const-string v0, "textSelectionSpecialModeHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/h9;->c:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;

    if-nez v0, :cond_0

    return-void

    .line 21
    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->bindController(Lcom/pspdfkit/ui/special_mode/controller/TextSelectionController;)V

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/internal/h9;->g:Lcom/pspdfkit/listeners/OnPreparePopupToolbarListener;

    if-eqz p1, :cond_1

    invoke-interface {p1, v0}, Lcom/pspdfkit/listeners/OnPreparePopupToolbarListener;->onPrepareTextSelectionPopupToolbar(Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;)V

    .line 25
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/h9;->f:Lcom/pspdfkit/ui/PopupToolbar;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PopupToolbar;->dismiss()V

    .line 26
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/h9;->d:Lcom/pspdfkit/internal/zt$b;

    sget-object v1, Lcom/pspdfkit/internal/zt$b;->a:Lcom/pspdfkit/internal/zt$b;

    if-ne p1, v1, :cond_3

    .line 27
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->showForSelectedText()V

    .line 28
    iput-object v0, p0, Lcom/pspdfkit/internal/h9;->f:Lcom/pspdfkit/ui/PopupToolbar;

    :cond_3
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zt$b;)V
    .locals 2

    const-string v0, "handleDragStatus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/h9;->c:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->showForSelectedText()V

    .line 14
    sget-object p1, Lcom/pspdfkit/internal/zt$b;->a:Lcom/pspdfkit/internal/zt$b;

    goto :goto_0

    .line 15
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->dismiss()V

    .line 16
    sget-object p1, Lcom/pspdfkit/internal/zt$b;->c:Lcom/pspdfkit/internal/zt$b;

    goto :goto_0

    .line 17
    :cond_2
    invoke-virtual {v0}, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;->dismiss()V

    .line 18
    sget-object p1, Lcom/pspdfkit/internal/zt$b;->b:Lcom/pspdfkit/internal/zt$b;

    .line 19
    :goto_0
    iput-object p1, p0, Lcom/pspdfkit/internal/h9;->d:Lcom/pspdfkit/internal/zt$b;

    return-void
.end method

.method public final a(Lcom/pspdfkit/listeners/OnPreparePopupToolbarListener;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/pspdfkit/internal/h9;->g:Lcom/pspdfkit/listeners/OnPreparePopupToolbarListener;

    return-void
.end method

.method public final b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/h9;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/h9;->f:Lcom/pspdfkit/ui/PopupToolbar;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PopupToolbar;->dismiss()V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/h9;->b:Lkotlin/Lazy;

    invoke-interface {v1}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/PopupToolbar;

    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/pspdfkit/internal/h9;->f:Lcom/pspdfkit/ui/PopupToolbar;

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/h9;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/h9$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/h9$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/h9;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 8
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3, v1}, Lio/reactivex/rxjava3/core/Completable;->delay(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/pspdfkit/internal/h9;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

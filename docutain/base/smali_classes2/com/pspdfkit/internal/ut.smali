.class public final Lcom/pspdfkit/internal/ut;
.super Lcom/pspdfkit/internal/tt;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ut$a;,
        Lcom/pspdfkit/internal/ut$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/ut$b;


# instance fields
.field private final a:Lcom/pspdfkit/internal/iv;

.field private final b:Lcom/pspdfkit/internal/bd;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/ut$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/ut$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/ut;->Companion:Lcom/pspdfkit/internal/ut$b;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/bd;)V
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    and-int/lit8 v0, p1, 0x3

    const/4 v1, 0x3

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/ut$a;->a:Lcom/pspdfkit/internal/ut$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ut$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/tt;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/ut;->a:Lcom/pspdfkit/internal/iv;

    iput-object p3, p0, Lcom/pspdfkit/internal/ut;->b:Lcom/pspdfkit/internal/bd;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/ut;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/iv$a;->a:Lcom/pspdfkit/internal/iv$a;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ut;->a:Lcom/pspdfkit/internal/iv;

    const/4 v2, 0x0

    .line 3
    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/bd$a;->a:Lcom/pspdfkit/internal/bd$a;

    .line 4
    iget-object p0, p0, Lcom/pspdfkit/internal/ut;->b:Lcom/pspdfkit/internal/bd;

    const/4 v1, 0x1

    .line 5
    invoke-interface {p1, p2, v1, v0, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/iv;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ut;->a:Lcom/pspdfkit/internal/iv;

    return-object v0
.end method

.method public final b()Lcom/pspdfkit/internal/bd;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ut;->b:Lcom/pspdfkit/internal/bd;

    return-object v0
.end method

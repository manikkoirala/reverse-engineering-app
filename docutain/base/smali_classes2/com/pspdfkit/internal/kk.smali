.class public final enum Lcom/pspdfkit/internal/kk;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/kk;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum b:Lcom/pspdfkit/internal/kk;

.field public static final enum c:Lcom/pspdfkit/internal/kk;

.field public static final enum d:Lcom/pspdfkit/internal/kk;

.field private static final synthetic e:[Lcom/pspdfkit/internal/kk;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/kk;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__note_editor_option_share:I

    const-string v2, "SHARE"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/pspdfkit/internal/kk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/pspdfkit/internal/kk;->b:Lcom/pspdfkit/internal/kk;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/kk;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__note_editor_option_set_reply_status:I

    const-string v4, "SET_STATUS"

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5, v2}, Lcom/pspdfkit/internal/kk;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/pspdfkit/internal/kk;->c:Lcom/pspdfkit/internal/kk;

    .line 3
    new-instance v2, Lcom/pspdfkit/internal/kk;

    sget v4, Lcom/pspdfkit/R$id;->pspdf__note_editor_option_delete_reply:I

    const-string v6, "DELETE"

    const/4 v7, 0x2

    invoke-direct {v2, v6, v7, v4}, Lcom/pspdfkit/internal/kk;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/pspdfkit/internal/kk;->d:Lcom/pspdfkit/internal/kk;

    const/4 v4, 0x3

    new-array v4, v4, [Lcom/pspdfkit/internal/kk;

    aput-object v0, v4, v3

    aput-object v1, v4, v5

    aput-object v2, v4, v7

    .line 4
    sput-object v4, Lcom/pspdfkit/internal/kk;->e:[Lcom/pspdfkit/internal/kk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    iput p3, p0, Lcom/pspdfkit/internal/kk;->a:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/kk;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/kk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/kk;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/kk;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/kk;->e:[Lcom/pspdfkit/internal/kk;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/kk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/kk;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/kk;->a:I

    return v0
.end method

.class public final Lcom/pspdfkit/internal/ad$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public static a(Lcom/pspdfkit/utils/Size;)Lcom/pspdfkit/internal/q6;
    .locals 3

    const-string v0, "pageSize"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v1, Lcom/pspdfkit/internal/q6;

    sget-object v2, Lcom/pspdfkit/internal/pt;->Companion:Lcom/pspdfkit/internal/pt$b;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/pt$b;->serializer()Lkotlinx/serialization/KSerializer;

    move-result-object v2

    invoke-static {v2}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->ListSerializer(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    move-result-object v2

    .line 2
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v0, Lcom/pspdfkit/internal/zc;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/zc;-><init>(Lcom/pspdfkit/utils/Size;)V

    .line 50
    invoke-direct {v1, v2, v0}, Lcom/pspdfkit/internal/q6;-><init>(Lkotlinx/serialization/DeserializationStrategy;Lkotlin/jvm/functions/Function2;)V

    return-object v1
.end method

.method public static a(Ljava/util/List;Lcom/pspdfkit/utils/Size;)V
    .locals 3

    const-string v0, "blocks"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pageSize"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/qt;

    .line 52
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 53
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-virtual {v1}, Lcom/pspdfkit/internal/qt;->e()Lcom/pspdfkit/internal/bv;

    move-result-object v2

    invoke-virtual {v1}, Lcom/pspdfkit/internal/qt;->c()Lcom/pspdfkit/internal/tt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/tt;->a()Lcom/pspdfkit/internal/iv;

    move-result-object v1

    invoke-virtual {v2, p1, v1}, Lcom/pspdfkit/internal/bv;->a(Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/internal/iv;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.class final Lcom/pspdfkit/internal/wm$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/wm;->a(II)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/wm;

.field final synthetic b:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/wm;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/wm$a;->a:Lcom/pspdfkit/internal/wm;

    iput-object p2, p0, Lcom/pspdfkit/internal/wm$a;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 5

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/Annotation;

    const-string v0, "widgetAnnotation"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    instance-of v0, p1, Lcom/pspdfkit/annotations/WidgetAnnotation;

    if-nez v0, :cond_0

    goto :goto_0

    .line 140
    :cond_0
    check-cast p1, Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v0

    .line 141
    instance-of v1, v0, Lcom/pspdfkit/forms/PushButtonFormElement;

    if-nez v1, :cond_1

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.JavaScript"

    const-string v1, "Can\'t import button icon: importButtonIcon action works only on push buttons."

    .line 142
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 146
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/wm$a;->a:Lcom/pspdfkit/internal/wm;

    invoke-static {v1, p1}, Lcom/pspdfkit/internal/wm;->a(Lcom/pspdfkit/internal/wm;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    .line 147
    iget-object v1, p0, Lcom/pspdfkit/internal/wm$a;->a:Lcom/pspdfkit/internal/wm;

    check-cast v0, Lcom/pspdfkit/forms/PushButtonFormElement;

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/wm;->a(Lcom/pspdfkit/internal/wm;Lcom/pspdfkit/forms/PushButtonFormElement;)V

    .line 149
    new-instance v1, Lcom/pspdfkit/document/image/ImagePicker;

    iget-object v2, p0, Lcom/pspdfkit/internal/wm$a;->a:Lcom/pspdfkit/internal/wm;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/wm;->b()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string v3, "JavaScript.IMAGE_PICKER_FRAGMENT_TAG"

    invoke-direct {v1, v2, v3}, Lcom/pspdfkit/document/image/ImagePicker;-><init>(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 150
    iget-object v2, p0, Lcom/pspdfkit/internal/wm$a;->a:Lcom/pspdfkit/internal/wm;

    iget-object v3, p0, Lcom/pspdfkit/internal/wm$a;->b:Landroid/content/Context;

    .line 151
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 152
    new-instance v4, Lcom/pspdfkit/internal/vm;

    invoke-direct {v4, v2, v3, v0, p1}, Lcom/pspdfkit/internal/vm;-><init>(Lcom/pspdfkit/internal/wm;Landroid/content/Context;Lcom/pspdfkit/forms/PushButtonFormElement;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    .line 153
    invoke-virtual {v1, v4}, Lcom/pspdfkit/document/image/ImagePicker;->setOnImagePickedListener(Lcom/pspdfkit/document/image/ImagePicker$OnImagePickedListener;)V

    .line 154
    invoke-virtual {v1}, Lcom/pspdfkit/document/image/ImagePicker;->startImageGallery()V

    :goto_0
    return-void
.end method

.class final Lcom/pspdfkit/internal/m8$c;
.super Lcom/pspdfkit/internal/cs;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/m8;->importDocument(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/cs<",
        "Ljava/util/List<",
        "Lcom/pspdfkit/undo/EditingChange;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/pspdfkit/internal/m8;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/m8;I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/m8$c;->b:Lcom/pspdfkit/internal/m8;

    iput p2, p0, Lcom/pspdfkit/internal/m8$c;->a:I

    invoke-direct {p0}, Lcom/pspdfkit/internal/cs;-><init>()V

    return-void
.end method


# virtual methods
.method public final onComplete()V
    .locals 3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.DocumentEditor"

    const-string v2, "Document importing was canceled."

    .line 1
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final onError(Ljava/lang/Throwable;)V
    .locals 3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.DocumentEditor"

    const-string v2, "Document couldn\'t be imported."

    .line 1
    invoke-static {v1, p1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .line 1
    check-cast p1, Ljava/util/List;

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/m8$c;->b:Lcom/pspdfkit/internal/m8;

    invoke-static {p1}, Lcom/pspdfkit/internal/m8;->-$$Nest$fgetf(Lcom/pspdfkit/internal/m8;)Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    move-result-object p1

    iget v0, p0, Lcom/pspdfkit/internal/m8$c;->a:I

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a(I)V

    return-void
.end method

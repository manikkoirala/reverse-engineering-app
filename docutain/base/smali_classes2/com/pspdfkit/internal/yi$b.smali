.class final Lcom/pspdfkit/internal/yi$b;
.super Lcom/pspdfkit/internal/as;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/yi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field private final a:Landroid/graphics/Matrix;

.field private b:Z

.field final synthetic c:Lcom/pspdfkit/internal/yi;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/yi;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/yi$b;->c:Lcom/pspdfkit/internal/yi;

    invoke-direct {p0}, Lcom/pspdfkit/internal/as;-><init>()V

    .line 4
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/yi$b;->a:Landroid/graphics/Matrix;

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/yi;Lcom/pspdfkit/internal/yi$b-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/yi$b;-><init>(Lcom/pspdfkit/internal/yi;)V

    return-void
.end method


# virtual methods
.method public final d(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yi$b;->c:Lcom/pspdfkit/internal/yi;

    invoke-static {v0}, Lcom/pspdfkit/internal/yi;->-$$Nest$fgetd(Lcom/pspdfkit/internal/yi;)Lcom/pspdfkit/internal/na;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/yi$b;->a:Landroid/graphics/Matrix;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/pspdfkit/internal/na;->a(Landroid/view/MotionEvent;Landroid/graphics/Matrix;Z)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    .line 5
    instance-of v0, p1, Lcom/pspdfkit/annotations/WidgetAnnotation;

    if-eqz v0, :cond_0

    .line 6
    check-cast p1, Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/LinkAnnotation;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/yi$b;->c:Lcom/pspdfkit/internal/yi;

    invoke-static {v0}, Lcom/pspdfkit/internal/yi;->-$$Nest$fgetg(Lcom/pspdfkit/internal/yi;)Lcom/pspdfkit/annotations/actions/ActionResolver;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/annotations/actions/ActionResolver;->executeAction(Lcom/pspdfkit/annotations/actions/Action;)V

    return v2

    :cond_0
    if-eqz p1, :cond_3

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/yi$b;->c:Lcom/pspdfkit/internal/yi;

    .line 13
    invoke-static {v0}, Lcom/pspdfkit/internal/yi;->-$$Nest$fgeth(Lcom/pspdfkit/internal/yi;)Ljava/util/HashMap;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/wi;

    if-eqz v1, :cond_1

    .line 15
    invoke-virtual {v1}, Lcom/pspdfkit/internal/wi;->e()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v2

    if-ne v2, p1, :cond_1

    goto :goto_0

    .line 20
    :cond_2
    invoke-static {p1}, Lcom/pspdfkit/internal/wi;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/wi;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_3

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/yi$b;->c:Lcom/pspdfkit/internal/yi;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/yi;->c(Lcom/pspdfkit/internal/wi;)V

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method public final h(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/yi$b;->b:Z

    return p1
.end method

.method public final onDown(Landroid/view/MotionEvent;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yi$b;->c:Lcom/pspdfkit/internal/yi;

    invoke-static {v0}, Lcom/pspdfkit/internal/yi;->-$$Nest$fgetd(Lcom/pspdfkit/internal/yi;)Lcom/pspdfkit/internal/na;

    move-result-object v1

    invoke-static {v0}, Lcom/pspdfkit/internal/yi;->-$$Nest$fgetb(Lcom/pspdfkit/internal/yi;)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/internal/yi$b;->a:Landroid/graphics/Matrix;

    .line 2
    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v0

    const/4 v2, 0x1

    .line 3
    invoke-virtual {v1, p1, v0, v2}, Lcom/pspdfkit/internal/na;->a(Landroid/view/MotionEvent;Landroid/graphics/Matrix;Z)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 8
    :goto_0
    iput-boolean v2, p0, Lcom/pspdfkit/internal/yi$b;->b:Z

    return-void
.end method

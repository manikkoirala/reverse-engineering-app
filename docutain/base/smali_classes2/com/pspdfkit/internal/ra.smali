.class public final Lcom/pspdfkit/internal/ra;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ra$a;,
        Lcom/pspdfkit/internal/ra$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/ra$b;


# instance fields
.field private final a:I

.field private final b:Lcom/pspdfkit/internal/iv;

.field private final c:Lcom/pspdfkit/internal/iv;

.field private final d:Ljava/lang/String;

.field private final e:Z

.field private final f:Z

.field private final g:Z

.field private final h:Lkotlin/UInt;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/ra$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/ra$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/ra;->Companion:Lcom/pspdfkit/internal/ra$b;

    return-void
.end method

.method private constructor <init>(IILcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/iv;Ljava/lang/String;ZZZLkotlin/UInt;)V
    .locals 2

    and-int/lit8 v0, p1, 0x7f

    const/16 v1, 0x7f

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/ra$a;->a:Lcom/pspdfkit/internal/ra$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ra$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/pspdfkit/internal/ra;->a:I

    iput-object p3, p0, Lcom/pspdfkit/internal/ra;->b:Lcom/pspdfkit/internal/iv;

    iput-object p4, p0, Lcom/pspdfkit/internal/ra;->c:Lcom/pspdfkit/internal/iv;

    iput-object p5, p0, Lcom/pspdfkit/internal/ra;->d:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/pspdfkit/internal/ra;->e:Z

    iput-boolean p7, p0, Lcom/pspdfkit/internal/ra;->f:Z

    iput-boolean p8, p0, Lcom/pspdfkit/internal/ra;->g:Z

    and-int/lit16 p1, p1, 0x80

    if-nez p1, :cond_1

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/pspdfkit/internal/ra;->h:Lkotlin/UInt;

    goto :goto_0

    :cond_1
    iput-object p9, p0, Lcom/pspdfkit/internal/ra;->h:Lkotlin/UInt;

    :goto_0
    return-void
.end method

.method public synthetic constructor <init>(IILcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/iv;Ljava/lang/String;ZZZLkotlin/UInt;I)V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    invoke-direct/range {p0 .. p9}, Lcom/pspdfkit/internal/ra;-><init>(IILcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/iv;Ljava/lang/String;ZZZLkotlin/UInt;)V

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/ra;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 5
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ra;->a:I

    const/4 v1, 0x0

    invoke-interface {p1, p2, v1, v0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeIntElement(Lkotlinx/serialization/descriptors/SerialDescriptor;II)V

    sget-object v0, Lcom/pspdfkit/internal/iv$a;->a:Lcom/pspdfkit/internal/iv$a;

    iget-object v2, p0, Lcom/pspdfkit/internal/ra;->b:Lcom/pspdfkit/internal/iv;

    const/4 v3, 0x1

    invoke-interface {p1, p2, v3, v0, v2}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/pspdfkit/internal/ra;->c:Lcom/pspdfkit/internal/iv;

    const/4 v4, 0x2

    invoke-interface {p1, p2, v4, v0, v2}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/pspdfkit/internal/ra;->d:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-interface {p1, p2, v2, v0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeStringElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILjava/lang/String;)V

    iget-boolean v0, p0, Lcom/pspdfkit/internal/ra;->e:Z

    const/4 v2, 0x4

    invoke-interface {p1, p2, v2, v0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeBooleanElement(Lkotlinx/serialization/descriptors/SerialDescriptor;IZ)V

    iget-boolean v0, p0, Lcom/pspdfkit/internal/ra;->f:Z

    const/4 v2, 0x5

    invoke-interface {p1, p2, v2, v0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeBooleanElement(Lkotlinx/serialization/descriptors/SerialDescriptor;IZ)V

    iget-boolean v0, p0, Lcom/pspdfkit/internal/ra;->g:Z

    const/4 v2, 0x6

    invoke-interface {p1, p2, v2, v0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeBooleanElement(Lkotlinx/serialization/descriptors/SerialDescriptor;IZ)V

    const/4 v0, 0x7

    invoke-interface {p1, p2, v0}, Lkotlinx/serialization/encoding/CompositeEncoder;->shouldEncodeElementDefault(Lkotlinx/serialization/descriptors/SerialDescriptor;I)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/ra;->h:Lkotlin/UInt;

    if-eqz v2, :cond_1

    :goto_0
    const/4 v1, 0x1

    :cond_1
    if-eqz v1, :cond_2

    sget-object v1, Lkotlinx/serialization/internal/UIntSerializer;->INSTANCE:Lkotlinx/serialization/internal/UIntSerializer;

    iget-object p0, p0, Lcom/pspdfkit/internal/ra;->h:Lkotlin/UInt;

    invoke-interface {p1, p2, v0, v1, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeNullableSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/iv;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ra;->c:Lcom/pspdfkit/internal/iv;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ra;->a:I

    return v0
.end method

.method public final c()Lcom/pspdfkit/internal/iv;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ra;->b:Lcom/pspdfkit/internal/iv;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ra;->d:Ljava/lang/String;

    return-object v0
.end method

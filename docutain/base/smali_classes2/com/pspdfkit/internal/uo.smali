.class public final Lcom/pspdfkit/internal/uo;
.super Landroidx/fragment/app/Fragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/uo$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\u0004B\u0007\u00a2\u0006\u0004\u0008\u0002\u0010\u0003\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/pspdfkit/internal/uo;",
        "Landroidx/fragment/app/Fragment;",
        "<init>",
        "()V",
        "a",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# static fields
.field public static final synthetic g:I


# instance fields
.field private final b:Lio/reactivex/rxjava3/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/pspdfkit/ui/PdfUi;

.field private final d:Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

.field private e:Lcom/pspdfkit/internal/zf;

.field private f:Landroid/net/Uri;


# direct methods
.method public static synthetic $r8$lambda$53lA6XHDp5FxaBQTi1NU2So_JOU(Lcom/pspdfkit/internal/uo;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/uo;->g(Lcom/pspdfkit/internal/uo;)V

    return-void
.end method

.method public static synthetic $r8$lambda$5UofWDxnt24pX3ZrDi3z3q9qPZ0(Lcom/pspdfkit/internal/uo;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/uo;->e(Lcom/pspdfkit/internal/uo;)V

    return-void
.end method

.method public static synthetic $r8$lambda$8sUh8w4eT0Xm2MSq07Uyb44Q5Uk(Lcom/pspdfkit/internal/uo;Landroid/net/Uri;Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/uo;->a(Lcom/pspdfkit/internal/uo;Landroid/net/Uri;Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/CompletableSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$R5Y6NAicTuzwIwJvASPWZmQ7_YA(Lcom/pspdfkit/internal/uo;Lcom/pspdfkit/document/PdfDocument;Landroid/net/Uri;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/uo;->a(Lcom/pspdfkit/internal/uo;Lcom/pspdfkit/document/PdfDocument;Landroid/net/Uri;)V

    return-void
.end method

.method public static synthetic $r8$lambda$iUamhx2MF3SHTnSmWozhfgXfQ7M(Lcom/pspdfkit/internal/uo;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/uo;->f(Lcom/pspdfkit/internal/uo;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 4
    invoke-static {}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->create()Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    move-result-object v0

    const-string v1, "create<Boolean>()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/uo;->b:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    .line 9
    new-instance v0, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

    invoke-direct {v0}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/uo;->d:Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/uo;)Lcom/pspdfkit/ui/PdfUi;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/uo;->c:Lcom/pspdfkit/ui/PdfUi;

    return-object p0
.end method

.method private static final a(Lcom/pspdfkit/internal/uo;Landroid/net/Uri;Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$targetUri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$document"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "w"

    invoke-virtual {p0, p1, v0}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    invoke-static {p2}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->fromDocument(Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/document/processor/PdfProcessorTask;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/document/processor/PdfProcessorTask;->applyRedactions()Lcom/pspdfkit/document/processor/PdfProcessorTask;

    move-result-object p1

    if-eqz p0, :cond_0

    .line 20
    invoke-static {p1, p0}, Lcom/pspdfkit/document/processor/PdfProcessor;->processDocumentAsync(Lcom/pspdfkit/document/processor/PdfProcessorTask;Ljava/io/OutputStream;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p0

    invoke-virtual {p0}, Lio/reactivex/rxjava3/core/Flowable;->lastOrError()Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    invoke-virtual {p0}, Lio/reactivex/rxjava3/core/Single;->ignoreElement()Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0

    :catch_0
    move-exception p0

    .line 21
    invoke-static {p0}, Lio/reactivex/rxjava3/core/Completable;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    return-object p0
.end method

.method private final a()V
    .locals 4

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/uo;->b:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    .line 43
    sget-object v1, Lcom/pspdfkit/internal/uo$h;->a:Lcom/pspdfkit/internal/uo$h;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Observable;->firstElement()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 45
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->computation()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 46
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 47
    new-instance v1, Lcom/pspdfkit/internal/uo$i;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/uo$i;-><init>(Lcom/pspdfkit/internal/uo;)V

    new-instance v2, Lcom/pspdfkit/internal/uo$j;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/uo$j;-><init>(Lcom/pspdfkit/internal/uo;)V

    new-instance v3, Lcom/pspdfkit/internal/uo$$ExternalSyntheticLambda1;

    invoke-direct {v3, p0}, Lcom/pspdfkit/internal/uo$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/uo;)V

    invoke-virtual {v0, v1, v2, v3}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private final a(Lcom/pspdfkit/document/PdfDocument;Landroid/net/Uri;)V
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/uo;->b:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    .line 24
    sget-object v1, Lcom/pspdfkit/internal/uo$e;->a:Lcom/pspdfkit/internal/uo$e;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Observable;->firstElement()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 26
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->computation()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 27
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 28
    new-instance v1, Lcom/pspdfkit/internal/uo$f;

    invoke-direct {v1, p0, p2, p1}, Lcom/pspdfkit/internal/uo$f;-><init>(Lcom/pspdfkit/internal/uo;Landroid/net/Uri;Lcom/pspdfkit/document/PdfDocument;)V

    new-instance p1, Lcom/pspdfkit/internal/uo$g;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/uo$g;-><init>(Lcom/pspdfkit/internal/uo;)V

    new-instance p2, Lcom/pspdfkit/internal/uo$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/uo$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/uo;)V

    invoke-virtual {v0, v1, p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/uo;Landroid/net/Uri;)V
    .locals 0

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/uo;->f:Landroid/net/Uri;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/uo;Lcom/pspdfkit/document/PdfDocument;Landroid/net/Uri;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$targetUri"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/uo;->a(Lcom/pspdfkit/document/PdfDocument;Landroid/net/Uri;)V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/uo;Lcom/pspdfkit/internal/zf;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/uo;->a(Lcom/pspdfkit/internal/zf;)V

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/zf;)V
    .locals 3

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/internal/uo;->b:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    .line 30
    sget-object v1, Lcom/pspdfkit/internal/uo$b;->a:Lcom/pspdfkit/internal/uo$b;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Observable;->firstElement()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 32
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->computation()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 33
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 34
    new-instance v1, Lcom/pspdfkit/internal/uo$c;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/uo$c;-><init>(Lcom/pspdfkit/internal/uo;Lcom/pspdfkit/internal/zf;)V

    new-instance p1, Lcom/pspdfkit/internal/uo$d;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/uo$d;-><init>(Lcom/pspdfkit/internal/uo;)V

    new-instance v2, Lcom/pspdfkit/internal/uo$$ExternalSyntheticLambda4;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/uo$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/uo;)V

    invoke-virtual {v0, v1, p1, v2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/zf;Landroid/net/Uri;)V
    .locals 2

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/uo$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p2, p1}, Lcom/pspdfkit/internal/uo$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/uo;Landroid/net/Uri;Lcom/pspdfkit/document/PdfDocument;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 13
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 14
    new-instance v1, Lcom/pspdfkit/internal/uo$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/internal/uo$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/uo;Lcom/pspdfkit/document/PdfDocument;Landroid/net/Uri;)V

    new-instance p1, Lcom/pspdfkit/internal/zo;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/zo;-><init>(Lcom/pspdfkit/internal/uo;)V

    invoke-virtual {v0, v1, p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private final a(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 35
    iput-object v0, p0, Lcom/pspdfkit/internal/uo;->e:Lcom/pspdfkit/internal/zf;

    .line 36
    iput-object v0, p0, Lcom/pspdfkit/internal/uo;->f:Landroid/net/Uri;

    .line 37
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroidx/fragment/app/FragmentTransaction;->remove(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    :cond_0
    if-eqz p1, :cond_1

    .line 41
    iget-object p1, p0, Lcom/pspdfkit/internal/uo;->d:Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;->dismiss()V

    :cond_1
    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/uo;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/uo;->a(Z)V

    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/uo;Lcom/pspdfkit/internal/zf;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/uo;->e:Lcom/pspdfkit/internal/zf;

    return-void
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/uo;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/uo;->a()V

    return-void
.end method

.method static synthetic d(Lcom/pspdfkit/internal/uo;)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/uo;->a(Z)V

    return-void
.end method

.method private static final e(Lcom/pspdfkit/internal/uo;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/uo;->a(Z)V

    return-void
.end method

.method private static final f(Lcom/pspdfkit/internal/uo;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/uo;->a(Z)V

    return-void
.end method

.method private static final g(Lcom/pspdfkit/internal/uo;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/uo;->a(Z)V

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 5
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->setRetainInstance(Z)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/uo;->e:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    .line 8
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/uo;->a(Z)V

    return-void

    .line 9
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/uo;->f:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 12
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/uo;->a(Lcom/pspdfkit/internal/zf;Landroid/net/Uri;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 13
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object v1

    .line 14
    invoke-virtual {v1, p1}, Lcom/pspdfkit/document/DocumentSaveOptions;->setApplyRedactions(Z)V

    const-string p1, "document.getDefaultDocum\u2026edactions(true)\n        }"

    .line 15
    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->d(Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 19
    new-instance v1, Lcom/pspdfkit/internal/vo;

    invoke-direct {v1, p0, v0}, Lcom/pspdfkit/internal/vo;-><init>(Lcom/pspdfkit/internal/uo;Lcom/pspdfkit/internal/zf;)V

    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Single;->flatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 23
    sget-object v0, Lcom/pspdfkit/internal/wo;->a:Lcom/pspdfkit/internal/wo;

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->flatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 31
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 32
    new-instance v0, Lcom/pspdfkit/internal/xo;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/xo;-><init>(Lcom/pspdfkit/internal/uo;)V

    new-instance v1, Lcom/pspdfkit/internal/yo;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/yo;-><init>(Lcom/pspdfkit/internal/uo;)V

    invoke-virtual {p1, v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    :goto_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/uo;->d:Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__redaction_redacting:I

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;->showIndeterminateProgressDialog(Landroid/content/Context;I)V

    .line 2
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public final onStart()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    .line 2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type androidx.fragment.app.FragmentActivity"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/fragment/app/FragmentActivity;

    .line 3
    instance-of v1, v0, Lcom/pspdfkit/ui/PdfActivity;

    if-eqz v1, :cond_0

    .line 4
    move-object v1, v0

    check-cast v1, Lcom/pspdfkit/ui/PdfUi;

    iput-object v1, p0, Lcom/pspdfkit/internal/uo;->c:Lcom/pspdfkit/ui/PdfUi;

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/uo;->b:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 8
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/uo;->c:Lcom/pspdfkit/ui/PdfUi;

    if-nez v1, :cond_3

    .line 10
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v0

    const-string v1, "context.supportFragmentManager.fragments"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/fragment/app/Fragment;

    .line 61
    instance-of v2, v1, Lcom/pspdfkit/ui/PdfUiFragment;

    if-eqz v2, :cond_1

    const-string v0, "null cannot be cast to non-null type com.pspdfkit.ui.PdfUiFragment"

    .line 62
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/pspdfkit/ui/PdfUiFragment;

    iput-object v1, p0, Lcom/pspdfkit/internal/uo;->c:Lcom/pspdfkit/ui/PdfUi;

    .line 63
    iget-object v0, p0, Lcom/pspdfkit/internal/uo;->b:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 112
    :cond_2
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Collection contains no element matching the predicate."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/uo;->e:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 114
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/uo;->a(Z)V

    :cond_4
    return-void
.end method

.method public final onStop()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStop()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/uo;->b:Lio/reactivex/rxjava3/subjects/BehaviorSubject;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/uo;->c:Lcom/pspdfkit/ui/PdfUi;

    return-void
.end method

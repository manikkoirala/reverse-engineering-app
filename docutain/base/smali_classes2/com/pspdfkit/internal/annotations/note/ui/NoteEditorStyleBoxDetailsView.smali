.class public Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field private b:Ljava/lang/String;

.field private c:I

.field private d:Lcom/pspdfkit/internal/jk;

.field private e:I


# direct methods
.method public static synthetic $r8$lambda$8TeP20m5MjClW_dHTL67aHIyI-Q(Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;Ljava/lang/Integer;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->a(Ljava/lang/Integer;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$k4N6dbvoN51ePFt6hGhyqP7p6kI(Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->a(Ljava/lang/String;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->e:I

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 7
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 8
    iput p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->e:I

    .line 17
    invoke-direct {p0}, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    .line 19
    iput p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->e:I

    .line 34
    invoke-direct {p0}, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p1, 0x0

    .line 36
    iput p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->e:I

    .line 60
    invoke-direct {p0}, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->a()V

    return-void
.end method

.method private a()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__note_editor_item_style_box_details_item_spacing_dp:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->c:I

    return-void
.end method

.method private synthetic a(Ljava/lang/Integer;Landroid/view/View;)V
    .locals 0

    .line 65
    iget-object p2, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->d:Lcom/pspdfkit/internal/jk;

    if-eqz p2, :cond_0

    .line 66
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    check-cast p2, Lcom/pspdfkit/internal/uk;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/uk;->a(I)V

    :cond_0
    return-void
.end method

.method private synthetic a(Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .line 63
    iget-object p2, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->d:Lcom/pspdfkit/internal/jk;

    if-eqz p2, :cond_0

    .line 64
    check-cast p2, Lcom/pspdfkit/internal/uk;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/uk;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 9

    .line 2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    const/4 v0, 0x0

    .line 3
    iput v0, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->e:I

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$dimen;->pspdf__note_editor_item_style_box_item_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 9
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 10
    new-instance v3, Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 13
    invoke-virtual {v3, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 15
    new-instance v4, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v4}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    const/4 v5, 0x1

    new-array v6, v5, [I

    const v7, 0x10100a1

    aput v7, v6, v0

    .line 19
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    sget v8, Lcom/pspdfkit/R$drawable;->pspdf__rounded_rect_note_editor_style_box_item_selected:I

    .line 20
    invoke-static {v7, v8}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 21
    invoke-virtual {v4, v6, v7}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-array v6, v0, [I

    .line 27
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    sget v8, Lcom/pspdfkit/R$drawable;->pspdf__rounded_rect_note_editor_style_box_item:I

    invoke-static {v7, v8}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 28
    invoke-virtual {v4, v6, v7}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 31
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 32
    invoke-virtual {v3, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 33
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setCropToPadding(Z)V

    .line 34
    sget-object v4, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 35
    invoke-static {v2}, Lcom/pspdfkit/internal/ao;->b(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 36
    new-instance v4, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView$$ExternalSyntheticLambda0;

    invoke-direct {v4, p0, v2}, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 42
    iget v2, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->e:I

    add-int/2addr v2, v5

    iput v2, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->e:I

    goto :goto_0

    .line 46
    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    .line 47
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 50
    invoke-virtual {v0, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 53
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__rounded_rect_note_editor_style_box_item:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 54
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/GradientDrawable;

    .line 55
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 56
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 57
    new-instance v1, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p2}, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;Ljava/lang/Integer;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 5

    .line 1
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result p1

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result p2

    const/4 p3, 0x0

    .line 5
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p4

    if-ge p3, p4, :cond_2

    .line 6
    invoke-virtual {p0, p3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p4

    .line 10
    iget p5, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->e:I

    if-lt p3, p5, :cond_1

    rem-int/lit8 p5, p5, 0x6

    if-nez p5, :cond_0

    goto :goto_1

    :cond_0
    rsub-int/lit8 p5, p5, 0x6

    add-int/2addr p5, p3

    goto :goto_2

    :cond_1
    :goto_1
    move p5, p3

    .line 14
    :goto_2
    div-int/lit8 v0, p5, 0x6

    .line 15
    rem-int/lit8 p5, p5, 0x6

    .line 17
    invoke-virtual {p4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 18
    invoke-virtual {p4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 20
    iget v3, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->c:I

    add-int v4, v1, v3

    mul-int v4, v4, p5

    add-int/2addr v4, p1

    add-int/2addr v3, v2

    mul-int v3, v3, v0

    add-int/2addr v3, p2

    add-int/2addr v1, v4

    add-int/2addr v2, v3

    .line 25
    invoke-virtual {p4, v4, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 4

    .line 1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result p2

    sub-int/2addr p1, p2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result p2

    sub-int/2addr p1, p2

    iget p2, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->c:I

    mul-int/lit8 p2, p2, 0x5

    sub-int/2addr p1, p2

    div-int/lit8 p1, p1, 0x6

    const/4 p2, 0x0

    const/4 v0, 0x0

    .line 8
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 9
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/high16 v2, 0x40000000    # 2.0f

    .line 11
    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 12
    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 13
    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 18
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->e:I

    rem-int/lit8 v0, v0, 0x6

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    rsub-int/lit8 v0, v0, 0x6

    .line 21
    :goto_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/2addr v1, v0

    .line 22
    div-int/lit8 v0, v1, 0x6

    .line 23
    rem-int/lit8 v1, v1, 0x6

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    const/4 p2, 0x1

    :cond_2
    add-int/2addr v0, p2

    mul-int p1, p1, v0

    sub-int/2addr v0, v2

    .line 24
    iget p2, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->c:I

    mul-int v0, v0, p2

    add-int/2addr v0, p1

    .line 25
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result p1

    add-int/2addr p1, v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result p2

    add-int/2addr p2, p1

    .line 26
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public setAdapterCallbacks(Lcom/pspdfkit/internal/jk;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->d:Lcom/pspdfkit/internal/jk;

    return-void
.end method

.method public setSelectedIconItem(Ljava/lang/String;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->b:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    .line 2
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 3
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 4
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    if-ne v5, v0, :cond_1

    instance-of v5, v4, Landroid/widget/ImageView;

    if-eqz v5, :cond_1

    .line 5
    check-cast v4, Landroid/widget/ImageView;

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    move-object v4, v1

    :goto_2
    if-eqz v4, :cond_3

    .line 6
    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 9
    :cond_3
    iput-object p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->b:Ljava/lang/String;

    if-nez p1, :cond_4

    goto :goto_4

    .line 10
    :cond_4
    :goto_3
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 11
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-ne v3, p1, :cond_5

    instance-of v3, v0, Landroid/widget/ImageView;

    if-eqz v3, :cond_5

    .line 13
    move-object v1, v0

    check-cast v1, Landroid/widget/ImageView;

    goto :goto_4

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    :goto_4
    if-eqz v1, :cond_7

    const/4 p1, 0x1

    .line 14
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    :cond_7
    return-void
.end method

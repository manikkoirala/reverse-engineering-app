.class public Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;
.super Landroid/widget/ListView;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$b;,
        Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$a;
    }
.end annotation


# instance fields
.field b:Ljava/util/ArrayList;

.field c:Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$b;

.field d:Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->b:Ljava/util/ArrayList;

    const/4 p1, 0x0

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->c:Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$b;

    .line 12
    invoke-direct {p0}, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->b:Ljava/util/ArrayList;

    const/4 p1, 0x0

    .line 17
    iput-object p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->c:Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$b;

    .line 29
    invoke-direct {p0}, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->b:Ljava/util/ArrayList;

    const/4 p1, 0x0

    .line 34
    iput-object p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->c:Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$b;

    .line 51
    invoke-direct {p0}, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .line 52
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 53
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->b:Ljava/util/ArrayList;

    const/4 p1, 0x0

    .line 56
    iput-object p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->c:Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$b;

    .line 79
    invoke-direct {p0}, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->a()V

    return-void
.end method

.method private a()V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$a;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$a;-><init>(Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->d:Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$a;

    .line 2
    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method


# virtual methods
.method public setItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/lk;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->d:Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$a;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setOnReviewStateSelectedListener(Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$b;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->c:Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$b;

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->d:Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$a;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

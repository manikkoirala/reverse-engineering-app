.class public final Lcom/pspdfkit/internal/wq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/z1;

.field private final b:Landroid/graphics/Rect;

.field private final c:Landroid/graphics/Rect;

.field private final d:Landroid/graphics/RectF;

.field private final e:Landroid/graphics/RectF;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Z

.field private i:D

.field private j:D

.field private k:D

.field private l:Landroid/graphics/RectF;


# direct methods
.method public static synthetic $r8$lambda$yhTqdcvbKvWf81xhFi96o6uNnPI(Lcom/pspdfkit/internal/views/annotations/a;Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/wq;->a(Lcom/pspdfkit/internal/views/annotations/a;Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/z1;)V
    .locals 2

    const-string v0, "annotationSelectionLayout"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    .line 3
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/wq;->b:Landroid/graphics/Rect;

    .line 4
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/wq;->c:Landroid/graphics/Rect;

    .line 5
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/wq;->d:Landroid/graphics/RectF;

    .line 6
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/wq;->e:Landroid/graphics/RectF;

    .line 8
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/wq;->f:Ljava/util/List;

    const/4 p1, 0x1

    .line 17
    iput-boolean p1, p0, Lcom/pspdfkit/internal/wq;->h:Z

    const-wide/high16 v0, 0x7ff8000000000000L    # Double.NaN

    .line 20
    iput-wide v0, p0, Lcom/pspdfkit/internal/wq;->i:D

    .line 23
    iput-wide v0, p0, Lcom/pspdfkit/internal/wq;->j:D

    .line 26
    iput-wide v0, p0, Lcom/pspdfkit/internal/wq;->k:D

    .line 29
    new-instance p1, Landroid/graphics/RectF;

    const/4 v0, 0x0

    invoke-direct {p1, v0, v0, v0, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object p1, p0, Lcom/pspdfkit/internal/wq;->l:Landroid/graphics/RectF;

    return-void
.end method

.method private final a(Lcom/pspdfkit/annotations/Annotation;)Landroid/graphics/RectF;
    .locals 3

    .line 229
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/wq;->e:Landroid/graphics/RectF;

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->getContentSize(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    if-nez v0, :cond_1

    sget v0, Lcom/pspdfkit/internal/cq;->c:I

    const-string v0, "annotation"

    .line 230
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 291
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    const-string v1, "annotation.boundingBox"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    invoke-virtual {v0}, Landroid/graphics/RectF;->sort()V

    .line 297
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result v1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result p1

    add-int/2addr p1, v1

    rem-int/lit16 p1, p1, 0x168

    const/16 v1, 0x5a

    if-eq p1, v1, :cond_0

    const/16 v1, 0x10e

    if-eq p1, v1, :cond_0

    .line 303
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result p1

    .line 304
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    goto :goto_0

    .line 305
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result p1

    .line 306
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    .line 312
    :goto_0
    new-instance v1, Landroid/graphics/RectF;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v2, p1, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object v0, v1

    :cond_1
    return-object v0
.end method

.method private static final a(Lcom/pspdfkit/internal/views/annotations/a;Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 1

    const-string v0, "$child"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$this_apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    check-cast p0, Lcom/pspdfkit/internal/views/annotations/j;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->setRotation(F)V

    .line 116
    check-cast p1, Lcom/pspdfkit/internal/views/annotations/j;

    const/4 p0, 0x0

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/annotations/j;->setRefreshBoundingBoxAfterRendering(Z)V

    const/4 p0, 0x0

    .line 118
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/annotations/j;->setOnRenderedListener(Lcom/pspdfkit/internal/views/annotations/j$b;)V

    return-void
.end method

.method private final b(Lcom/pspdfkit/internal/views/annotations/a;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;)",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .line 8
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    const/4 v1, 0x2

    div-int/2addr v0, v1

    .line 12
    iget-object v2, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/2addr v2, v1

    .line 13
    iget-wide v3, p0, Lcom/pspdfkit/internal/wq;->i:D

    invoke-static {v3, v4}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result v3

    int-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v3

    goto :goto_0

    .line 18
    :cond_0
    iget-wide v3, p0, Lcom/pspdfkit/internal/wq;->i:D

    .line 19
    :goto_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result p1

    int-to-double v5, p1

    .line 20
    invoke-static {v5, v6}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v5

    add-double/2addr v5, v3

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/internal/wq;->l:Landroid/graphics/RectF;

    .line 26
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-double v3, v3

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    div-double/2addr v3, v7

    iget v9, p0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-double v9, v9

    add-double/2addr v3, v9

    invoke-static {v3, v4, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    .line 27
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v9

    float-to-double v9, v9

    div-double/2addr v9, v7

    iget v11, p0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-double v11, v11

    add-double/2addr v9, v11

    invoke-static {v9, v10, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    add-double/2addr v9, v3

    .line 28
    invoke-static {v9, v10}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    .line 32
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v9

    float-to-double v9, v9

    div-double/2addr v9, v7

    iget v11, p0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-double v11, v11

    add-double/2addr v9, v11

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result p1

    float-to-double v11, p1

    div-double/2addr v11, v7

    iget p1, p0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-double v7, p1

    add-double/2addr v11, v7

    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v7

    const-wide v9, 0x400921fb54442d18L    # Math.PI

    sub-double v9, v5, v9

    add-double/2addr v9, v7

    .line 35
    invoke-static {v9, v10}, Ljava/lang/Math;->cos(D)D

    move-result-wide v11

    mul-double v11, v11, v3

    double-to-float p1, v11

    .line 36
    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    move-result-wide v9

    mul-double v9, v9, v3

    double-to-float v9, v9

    sub-double/2addr v5, v7

    .line 38
    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v7

    mul-double v7, v7, v3

    double-to-float v7, v7

    .line 39
    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double v5, v5, v3

    double-to-float v3, v5

    .line 41
    new-instance v4, Landroid/graphics/PointF;

    int-to-float v0, v0

    add-float v5, v0, p1

    int-to-float v2, v2

    add-float v6, v2, v9

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 46
    new-instance v5, Landroid/graphics/PointF;

    add-float v6, v0, v7

    add-float v8, v2, v3

    invoke-direct {v5, v6, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 51
    new-instance v6, Landroid/graphics/PointF;

    sub-float p1, v0, p1

    sub-float v8, v2, v9

    invoke-direct {v6, p1, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 56
    new-instance p1, Landroid/graphics/PointF;

    sub-float/2addr v0, v7

    sub-float/2addr v2, v3

    invoke-direct {p1, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/PointF;

    const/4 v2, 0x0

    aput-object v4, v0, v2

    const/4 v2, 0x1

    aput-object v5, v0, v2

    aput-object v6, v0, v1

    const/4 v1, 0x3

    aput-object p1, v0, v1

    .line 61
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 62
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "annotationView isn\'t bound to an annotation."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final c(Lcom/pspdfkit/internal/views/annotations/a;)Landroid/graphics/RectF;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;)",
            "Landroid/graphics/RectF;"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/wq;->a(Lcom/pspdfkit/annotations/Annotation;)Landroid/graphics/RectF;

    move-result-object v1

    .line 5
    invoke-virtual {v1}, Landroid/graphics/RectF;->sort()V

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object p1

    iget-object v3, p0, Lcom/pspdfkit/internal/wq;->b:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, v3}, Lcom/pspdfkit/internal/en;->a(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object p1

    const-string v2, "annotationSelectionLayou\u2026View.asView(), reuseRect)"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-virtual {p1}, Landroid/graphics/Rect;->sort()V

    .line 10
    new-instance v2, Lcom/pspdfkit/utils/Size;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result v3

    int-to-float v3, v3

    .line 12
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v3, v0

    .line 13
    invoke-static {v2, v3}, Lcom/pspdfkit/internal/di;->b(Lcom/pspdfkit/utils/Size;F)Lcom/pspdfkit/utils/Size;

    move-result-object v0

    .line 20
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    iget v3, v0, Lcom/pspdfkit/utils/Size;->width:F

    div-float/2addr v2, v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    int-to-float p1, p1

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    div-float/2addr p1, v0

    invoke-static {v2, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    .line 22
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v0

    mul-float v0, v0, p1

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float v2, v2, p1

    const/4 p1, 0x0

    invoke-virtual {v1, p1, p1, v0, v2}, Landroid/graphics/RectF;->set(FFFF)V

    return-object v1

    .line 23
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "annotationView isn\'t bound to an annotation."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 13
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/wq;->f:Ljava/util/List;

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/wq;->l:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->setEmpty()V

    return-void
.end method

.method public final a(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/wq;->g:I

    const/4 p1, 0x1

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/wq;->h:Z

    return-void
.end method

.method public final a(IILandroid/graphics/Rect;)V
    .locals 9

    const-string v0, "out"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    div-int/lit8 p1, p1, 0x2

    .line 209
    div-int/lit8 p2, p2, 0x2

    .line 211
    iget-object v0, p0, Lcom/pspdfkit/internal/wq;->l:Landroid/graphics/RectF;

    .line 215
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-double v1, v1

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    div-double/2addr v1, v3

    iget v5, p0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-double v5, v5

    add-double/2addr v1, v5

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    .line 216
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-double v5, v5

    div-double/2addr v5, v3

    iget v7, p0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-double v7, v7

    add-double/2addr v5, v7

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    add-double/2addr v5, v1

    .line 217
    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    .line 221
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-double v5, v5

    div-double/2addr v5, v3

    iget v7, p0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-double v7, v7

    add-double/2addr v5, v7

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-double v7, v0

    div-double/2addr v7, v3

    iget v0, p0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-double v3, v0

    add-double/2addr v7, v3

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v3

    const-wide v5, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    add-double/2addr v3, v5

    .line 224
    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double v5, v5, v1

    double-to-int v0, v5

    .line 225
    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    move-result-wide v3

    mul-double v3, v3, v1

    double-to-int v1, v3

    add-int v2, p1, v0

    add-int v3, p2, v1

    sub-int/2addr p1, v0

    sub-int/2addr p2, v1

    .line 228
    invoke-virtual {p3, v2, v3, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public final a(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 11

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "boundingBoxPaint"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    invoke-virtual {p0}, Lcom/pspdfkit/internal/wq;->b()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/wq;->f:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/views/annotations/a;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/wq;->b(Lcom/pspdfkit/internal/views/annotations/a;)Ljava/util/List;

    move-result-object v0

    .line 204
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_1

    .line 205
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    rem-int v3, v1, v3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    add-int/lit8 v1, v1, 0x1

    .line 206
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    rem-int v4, v1, v4

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 207
    iget v6, v3, Landroid/graphics/PointF;->x:F

    iget v7, v3, Landroid/graphics/PointF;->y:F

    iget v8, v4, Landroid/graphics/PointF;->x:F

    iget v9, v4, Landroid/graphics/PointF;->y:F

    move-object v5, p1

    move-object v10, p2

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 6

    if-eqz p1, :cond_5

    .line 15
    invoke-virtual {p0}, Lcom/pspdfkit/internal/wq;->b()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 19
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/wq;->c:Landroid/graphics/Rect;

    .line 21
    iget-object v1, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 22
    iget-object v2, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    .line 23
    iget-object v3, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    .line 24
    iget-object v4, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    .line 25
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 33
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    .line 35
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v2, v1, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 36
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    sub-float/2addr v2, v1

    float-to-double v1, v2

    float-to-double v3, v0

    .line 37
    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    .line 39
    iget-object v2, p0, Lcom/pspdfkit/internal/wq;->f:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/views/annotations/a;

    .line 40
    invoke-interface {v2}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    if-nez v3, :cond_1

    return-void

    .line 42
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    if-nez p1, :cond_2

    .line 43
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result p1

    int-to-double v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/pspdfkit/internal/wq;->j:D

    .line 44
    iput-wide v0, p0, Lcom/pspdfkit/internal/wq;->k:D

    .line 47
    :cond_2
    iget-wide v4, p0, Lcom/pspdfkit/internal/wq;->k:D

    sub-double/2addr v0, v4

    iget-wide v4, p0, Lcom/pspdfkit/internal/wq;->j:D

    add-double/2addr v0, v4

    .line 48
    iput-wide v0, p0, Lcom/pspdfkit/internal/wq;->i:D

    .line 50
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    iget-object v4, p0, Lcom/pspdfkit/internal/wq;->d:Landroid/graphics/RectF;

    invoke-interface {p1, v4}, Lcom/pspdfkit/internal/pf;->getContentSize(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p1

    if-nez p1, :cond_3

    .line 52
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-direct {p0, v3}, Lcom/pspdfkit/internal/wq;->a(Lcom/pspdfkit/annotations/Annotation;)Landroid/graphics/RectF;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {p1, v3, v4}, Lcom/pspdfkit/internal/pf;->setContentSize(Landroid/graphics/RectF;Z)V

    .line 56
    :cond_3
    iget-wide v3, p0, Lcom/pspdfkit/internal/wq;->j:D

    sub-double/2addr v0, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float p1, v0

    .line 57
    invoke-interface {v2}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setRotation(F)V

    .line 59
    iget-boolean v0, p0, Lcom/pspdfkit/internal/wq;->h:Z

    if-eqz v0, :cond_4

    .line 60
    iget-object v0, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z1;->setScaleHandleDrawableRotation(F)V

    .line 63
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/z1;->d()V

    .line 64
    iget-object p1, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 66
    iget-object p1, p0, Lcom/pspdfkit/internal/wq;->l:Landroid/graphics/RectF;

    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/wq;->c(Lcom/pspdfkit/internal/views/annotations/a;)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public final a(Ljava/util/EnumMap;)V
    .locals 23

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "scaleHandleCenters"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/wq;->b()Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    .line 123
    :cond_0
    iget-object v2, v0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    const/4 v3, 0x2

    div-int/2addr v2, v3

    .line 124
    iget-object v4, v0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    div-int/2addr v4, v3

    .line 126
    iget-object v5, v0, Lcom/pspdfkit/internal/wq;->f:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/views/annotations/a;

    .line 127
    invoke-interface {v5}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v7

    if-nez v7, :cond_1

    return-void

    .line 128
    :cond_1
    iget-wide v8, v0, Lcom/pspdfkit/internal/wq;->i:D

    invoke-static {v8, v9}, Ljava/lang/Double;->isNaN(D)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 130
    invoke-virtual {v7}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v8

    invoke-interface {v8}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result v8

    int-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    goto :goto_0

    .line 133
    :cond_2
    iget-wide v8, v0, Lcom/pspdfkit/internal/wq;->i:D

    .line 134
    :goto_0
    invoke-virtual {v7}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v10

    invoke-interface {v10}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result v10

    .line 135
    iget-object v11, v0, Lcom/pspdfkit/internal/wq;->l:Landroid/graphics/RectF;

    .line 141
    invoke-virtual {v7}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v12

    invoke-interface {v12}, Lcom/pspdfkit/internal/pf;->needsFlippedContentSize()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 142
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v12

    .line 143
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v11

    goto :goto_1

    .line 145
    :cond_3
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v12

    .line 146
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v11

    :goto_1
    int-to-double v13, v2

    const-wide v15, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v15, v8, v15

    .line 149
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->cos(D)D

    move-result-wide v17

    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/wq;->b()Z

    move-result v2

    const/high16 v19, 0x40c00000    # 6.0f

    if-eqz v2, :cond_4

    .line 151
    iget v2, v0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-float v2, v2

    mul-float v2, v2, v19

    float-to-int v2, v2

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    int-to-float v2, v2

    int-to-float v3, v3

    div-float/2addr v12, v3

    add-float/2addr v2, v12

    move-object/from16 v20, v7

    float-to-double v6, v2

    mul-double v17, v17, v6

    add-double v6, v17, v13

    move/from16 v17, v3

    int-to-double v2, v4

    .line 152
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->sin(D)D

    move-result-wide v21

    .line 153
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/wq;->b()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 154
    iget v4, v0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-float v4, v4

    mul-float v4, v4, v19

    float-to-int v4, v4

    goto :goto_3

    :cond_5
    const/4 v4, 0x0

    :goto_3
    int-to-float v4, v4

    add-float/2addr v4, v12

    move-wide/from16 v18, v8

    float-to-double v8, v4

    mul-double v21, v21, v8

    add-double v8, v21, v2

    .line 155
    sget-object v4, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    if-eqz v4, :cond_6

    double-to-int v6, v6

    double-to-int v7, v8

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Point;->set(II)V

    .line 158
    :cond_6
    invoke-direct {v0, v5}, Lcom/pspdfkit/internal/wq;->b(Lcom/pspdfkit/internal/views/annotations/a;)Ljava/util/List;

    move-result-object v4

    move-object/from16 v5, v20

    .line 159
    instance-of v5, v5, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v5, :cond_a

    .line 163
    sget v5, Lcom/pspdfkit/internal/cq;->c:I

    invoke-static {v10}, Lcom/pspdfkit/internal/cq$a;->a(I)I

    move-result v5

    .line 164
    sget-object v6, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v6}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Point;

    if-eqz v6, :cond_7

    add-int/lit8 v7, v5, 0x0

    rem-int/lit8 v7, v7, 0x4

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    float-to-int v8, v8

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    float-to-int v7, v7

    invoke-virtual {v6, v8, v7}, Landroid/graphics/Point;->set(II)V

    .line 165
    :cond_7
    sget-object v6, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v6}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Point;

    if-eqz v6, :cond_8

    add-int/lit8 v7, v5, 0x1

    rem-int/lit8 v7, v7, 0x4

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    float-to-int v8, v8

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    float-to-int v7, v7

    invoke-virtual {v6, v8, v7}, Landroid/graphics/Point;->set(II)V

    .line 166
    :cond_8
    sget-object v6, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v6}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Point;

    if-eqz v6, :cond_9

    add-int/lit8 v7, v5, 0x2

    rem-int/lit8 v7, v7, 0x4

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    float-to-int v8, v8

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    float-to-int v7, v7

    invoke-virtual {v6, v8, v7}, Landroid/graphics/Point;->set(II)V

    .line 167
    :cond_9
    sget-object v6, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v6}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Point;

    if-eqz v6, :cond_f

    add-int/lit8 v5, v5, 0x3

    rem-int/lit8 v5, v5, 0x4

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    float-to-int v7, v7

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    float-to-int v4, v4

    invoke-virtual {v6, v7, v4}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_5

    :cond_a
    const/4 v5, 0x0

    .line 171
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    .line 172
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    .line 173
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    .line 174
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    .line 175
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    .line 176
    iget v10, v9, Landroid/graphics/PointF;->x:F

    invoke-static {v10, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 177
    iget v10, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v10, v7}, Ljava/lang/Math;->min(FF)F

    move-result v7

    .line 178
    iget v10, v9, Landroid/graphics/PointF;->x:F

    invoke-static {v10, v8}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .line 179
    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v9, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    goto :goto_4

    .line 182
    :cond_b
    sget-object v4, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    if-eqz v4, :cond_c

    float-to-int v9, v6

    float-to-int v10, v7

    invoke-virtual {v4, v9, v10}, Landroid/graphics/Point;->set(II)V

    .line 183
    :cond_c
    sget-object v4, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    if-eqz v4, :cond_d

    float-to-int v9, v8

    float-to-int v7, v7

    invoke-virtual {v4, v9, v7}, Landroid/graphics/Point;->set(II)V

    .line 184
    :cond_d
    sget-object v4, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    if-eqz v4, :cond_e

    float-to-int v6, v6

    float-to-int v7, v5

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Point;->set(II)V

    .line 185
    :cond_e
    sget-object v4, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    if-eqz v4, :cond_f

    float-to-int v6, v8

    float-to-int v5, v5

    invoke-virtual {v4, v6, v5}, Landroid/graphics/Point;->set(II)V

    .line 188
    :cond_f
    :goto_5
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    iget v6, v0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-float v6, v6

    add-float/2addr v6, v12

    float-to-double v6, v6

    mul-double v4, v4, v6

    .line 189
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    iget v8, v0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-float v8, v8

    add-float/2addr v8, v12

    float-to-double v8, v8

    mul-double v6, v6, v8

    .line 191
    sget-object v8, Lcom/pspdfkit/internal/z1$b;->b:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v8}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Point;

    if-eqz v8, :cond_10

    add-double v9, v13, v4

    double-to-int v9, v9

    move v12, v11

    add-double v10, v2, v6

    double-to-int v10, v10

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Point;->set(II)V

    goto :goto_6

    :cond_10
    move v12, v11

    .line 192
    :goto_6
    sget-object v8, Lcom/pspdfkit/internal/z1$b;->g:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v8}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Point;

    if-eqz v8, :cond_11

    sub-double v4, v13, v4

    double-to-int v4, v4

    sub-double v5, v2, v6

    double-to-int v5, v5

    invoke-virtual {v8, v4, v5}, Landroid/graphics/Point;->set(II)V

    .line 194
    :cond_11
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    iget v6, v0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-float v6, v6

    div-float v11, v12, v17

    add-float/2addr v6, v11

    float-to-double v6, v6

    mul-double v4, v4, v6

    .line 195
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    iget v8, v0, Lcom/pspdfkit/internal/wq;->g:I

    int-to-float v8, v8

    add-float/2addr v8, v11

    float-to-double v8, v8

    mul-double v6, v6, v8

    .line 197
    sget-object v8, Lcom/pspdfkit/internal/z1$b;->d:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v8}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Point;

    if-eqz v8, :cond_12

    sub-double v9, v13, v4

    double-to-int v9, v9

    sub-double v10, v2, v6

    double-to-int v10, v10

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Point;->set(II)V

    .line 198
    :cond_12
    sget-object v8, Lcom/pspdfkit/internal/z1$b;->e:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v8}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Point;

    if-eqz v1, :cond_13

    add-double/2addr v13, v4

    double-to-int v4, v13

    add-double/2addr v2, v6

    double-to-int v2, v2

    invoke-virtual {v1, v4, v2}, Landroid/graphics/Point;->set(II)V

    :cond_13
    return-void
.end method

.method public final a([Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    const-string v0, "selectedViews"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-static {p1}, Lkotlin/collections/ArraysKt;->toList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/wq;->f:Ljava/util/List;

    .line 4
    array-length v0, p1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/wq;->l:Landroid/graphics/RectF;

    const/4 v1, 0x0

    aget-object v2, p1, v1

    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/wq;->c(Lcom/pspdfkit/internal/views/annotations/a;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 6
    aget-object p1, p1, v1

    .line 7
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result p1

    int-to-float p1, p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z1;->setScaleHandleDrawableInitialRotation(F)V

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/z1;->setScaleHandleDrawableRotation(F)V

    goto :goto_0

    .line 12
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "annotationView isn\'t bound to an annotation."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/views/annotations/a;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">(",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "TT;>;)Z"
        }
    .end annotation

    const-string v0, "child"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    iget-wide v0, p0, Lcom/pspdfkit/internal/wq;->i:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    .line 69
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-nez v0, :cond_0

    return v1

    .line 71
    :cond_0
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/wq;->a(Lcom/pspdfkit/annotations/Annotation;)Landroid/graphics/RectF;

    move-result-object v1

    .line 72
    invoke-virtual {v1}, Landroid/graphics/RectF;->sort()V

    .line 73
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v2

    const-string v3, "annotation.boundingBox"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {v2}, Landroid/graphics/RectF;->sort()V

    .line 75
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result v3

    .line 76
    new-instance v4, Lcom/pspdfkit/utils/Size;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-direct {v4, v5, v1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 77
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result v1

    int-to-float v1, v1

    int-to-float v3, v3

    add-float/2addr v1, v3

    .line 78
    invoke-static {v4, v1}, Lcom/pspdfkit/internal/di;->b(Lcom/pspdfkit/utils/Size;F)Lcom/pspdfkit/utils/Size;

    move-result-object v1

    .line 85
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget v4, v1, Lcom/pspdfkit/utils/Size;->width:F

    div-float/2addr v3, v4

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget v1, v1, Lcom/pspdfkit/utils/Size;->height:F

    div-float/2addr v2, v1

    invoke-static {v3, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 87
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    iget-wide v3, p0, Lcom/pspdfkit/internal/wq;->i:D

    invoke-static {v3, v4}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v3

    double-to-int v3, v3

    invoke-interface {v2, v3}, Lcom/pspdfkit/internal/pf;->setRotation(I)V

    .line 88
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->adjustBoundsForRotation(F)V

    .line 89
    iget-boolean v0, p0, Lcom/pspdfkit/internal/wq;->h:Z

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/pspdfkit/internal/wq;->a:Lcom/pspdfkit/internal/z1;

    iget-wide v1, p0, Lcom/pspdfkit/internal/wq;->i:D

    invoke-static {v1, v2}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/z1;->setScaleHandleDrawableInitialRotation(F)V

    .line 92
    :cond_1
    instance-of v0, p1, Lcom/pspdfkit/internal/views/annotations/j;

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    .line 95
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/internal/views/annotations/j;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/annotations/j;->setRefreshBoundingBoxAfterRendering(Z)V

    .line 96
    new-instance v2, Lcom/pspdfkit/internal/wq$$ExternalSyntheticLambda0;

    invoke-direct {v2, p1, p1}, Lcom/pspdfkit/internal/wq$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/views/annotations/a;Lcom/pspdfkit/internal/views/annotations/a;)V

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/views/annotations/j;->setOnRenderedListener(Lcom/pspdfkit/internal/views/annotations/j$b;)V

    goto :goto_0

    .line 107
    :cond_2
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setRotation(F)V

    .line 109
    :goto_0
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->b()V

    const-wide/high16 v2, 0x7ff8000000000000L    # Double.NaN

    .line 110
    iput-wide v2, p0, Lcom/pspdfkit/internal/wq;->i:D

    .line 111
    iput-wide v2, p0, Lcom/pspdfkit/internal/wq;->j:D

    .line 112
    iput-wide v2, p0, Lcom/pspdfkit/internal/wq;->k:D

    :cond_3
    return v1
.end method

.method public final b()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/wq;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/views/annotations/a;

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    .line 6
    instance-of v3, v0, Lcom/pspdfkit/annotations/StampAnnotation;

    if-nez v3, :cond_1

    .line 7
    instance-of v3, v0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v3, :cond_0

    check-cast v0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getIntent()Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    move-result-object v0

    sget-object v3, Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;->FREE_TEXT:Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    if-ne v0, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1

    :cond_2
    return v2
.end method

.method public final c()V
    .locals 3

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/wq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/wq;->l:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/pspdfkit/internal/wq;->f:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/views/annotations/a;

    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/wq;->c(Lcom/pspdfkit/internal/views/annotations/a;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    :cond_0
    return-void
.end method

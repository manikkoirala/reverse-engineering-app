.class public final Lcom/pspdfkit/internal/wh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    invoke-static {p1}, Lcom/pspdfkit/internal/xh;->a(Landroid/content/Context;)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 14
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__MainToolbar_pspdf__backgroundColor:I

    .line 15
    sget v2, Landroidx/appcompat/R$attr;->colorPrimary:I

    .line 16
    sget v3, Lcom/pspdfkit/R$color;->pspdf__color:I

    .line 17
    invoke-static {v0, p1, v1, v2, v3}, Lcom/pspdfkit/internal/m;->a(Landroid/content/res/TypedArray;Landroid/content/Context;III)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/wh;->a:I

    .line 24
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__MainToolbar_pspdf__textColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    invoke-static {v0, p1, v1, v2}, Lcom/pspdfkit/internal/m;->a(Landroid/content/res/TypedArray;Landroid/content/Context;II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/wh;->b:I

    .line 26
    sget p1, Lcom/pspdfkit/R$styleable;->pspdf__MainToolbar_pspdf__toolbarPopupTheme:I

    sget v1, Landroidx/appcompat/R$style;->ThemeOverlay_AppCompat_Light:I

    invoke-virtual {v0, p1, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/wh;->c:I

    .line 28
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/wh;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/wh;->c:I

    return v0
.end method

.method public final c()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/wh;->b:I

    return v0
.end method

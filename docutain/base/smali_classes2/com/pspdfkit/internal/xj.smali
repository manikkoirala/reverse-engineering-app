.class public final Lcom/pspdfkit/internal/xj;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/document/DocumentSaveOptions;)Lcom/pspdfkit/internal/jni/NativeDocumentSecurityOptions;
    .locals 7

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 2
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPassword()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/pspdfkit/internal/ft;->d:I

    if-nez v1, :cond_1

    if-nez v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    if-eqz v1, :cond_3

    if-nez v2, :cond_2

    goto :goto_0

    .line 3
    :cond_2
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_4

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/document/PdfVersion;->getMajorVersion()I

    move-result v1

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/document/PdfVersion;->getMajorVersion()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/document/PdfVersion;->getMinorVersion()I

    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/document/PdfVersion;->getMinorVersion()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPermissions()Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->getPermissions()Ljava/util/EnumSet;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    return-object v0

    .line 12
    :cond_4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object p0

    sget-object v0, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 16
    new-instance p0, Lcom/pspdfkit/internal/jni/NativeDocumentSecurityOptions;

    .line 17
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPassword()Ljava/lang/String;

    move-result-object v1

    .line 18
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPassword()Ljava/lang/String;

    move-result-object v2

    .line 19
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/document/PdfVersion;->getMaxEncryptionKeyLength()I

    move-result v3

    .line 20
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPermissions()Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/sj;->d(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v4

    new-instance v5, Lcom/pspdfkit/internal/jni/NativePDFVersion;

    .line 21
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/document/PdfVersion;->getMajorVersion()I

    move-result v0

    int-to-byte v0, v0

    .line 22
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSaveOptions;->getPdfVersion()Lcom/pspdfkit/document/PdfVersion;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/document/PdfVersion;->getMinorVersion()I

    move-result p1

    int-to-byte p1, p1

    invoke-direct {v5, v0, p1}, Lcom/pspdfkit/internal/jni/NativePDFVersion;-><init>(BB)V

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/jni/NativeDocumentSecurityOptions;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/EnumSet;Lcom/pspdfkit/internal/jni/NativePDFVersion;Lcom/pspdfkit/internal/jni/NativeDocumentSecurityEncryptionAlgorithm;)V

    return-object p0

    .line 23
    :cond_5
    new-instance p0, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p1, "Changing document password, permissions or PDF version requires document editor feature in your license!"

    invoke-direct {p0, p1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Lio/reactivex/rxjava3/core/FlowableEmitter;)Lcom/pspdfkit/internal/jni/NativeProcessorDelegate;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/rxjava3/core/FlowableEmitter<",
            "-",
            "Lcom/pspdfkit/document/processor/PdfProcessor$ProcessorProgress;",
            ">;)",
            "Lcom/pspdfkit/internal/jni/NativeProcessorDelegate;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/xj$a;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/xj$a;-><init>(Lio/reactivex/rxjava3/core/FlowableEmitter;)V

    return-object v0
.end method

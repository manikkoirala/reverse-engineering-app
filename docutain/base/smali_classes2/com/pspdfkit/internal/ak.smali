.class public final Lcom/pspdfkit/internal/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/k1;


# instance fields
.field private final b:Lcom/pspdfkit/internal/specialMode/handler/a;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ak;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/16 v0, 0x14

    return v0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/os;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ak;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ak;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->b(Lcom/pspdfkit/internal/k1;)V

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NONE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ak;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

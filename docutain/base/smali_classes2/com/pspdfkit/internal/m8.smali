.class public final Lcom/pspdfkit/internal/m8;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/controller/DocumentEditingController;
.implements Lcom/pspdfkit/document/editor/page/NewPageFactory$OnNewPageReadyListener;


# static fields
.field static final synthetic i:Z = true


# instance fields
.field private final b:Lcom/pspdfkit/internal/gl;

.field private final c:Lcom/pspdfkit/internal/j8;

.field private d:Lcom/pspdfkit/internal/k8;

.field private final e:Lcom/pspdfkit/ui/PdfThumbnailGrid;

.field private final f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

.field private g:Z

.field private h:Z


# direct methods
.method public static synthetic $r8$lambda$1XTKbBFHAATpU2FtSwWW5zMsqa8(Lcom/pspdfkit/internal/m8;Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Lcom/pspdfkit/document/editor/FilePicker;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/m8;->a(Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Lcom/pspdfkit/document/editor/FilePicker;)V

    return-void
.end method

.method public static synthetic $r8$lambda$4AxRyXyQNnPiiEaZ9tR9rpDmsKw(Landroid/content/Context;ZLcom/pspdfkit/document/editor/PdfDocumentEditor;Ljava/util/HashSet;Landroid/net/Uri;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m8;->a(Landroid/content/Context;ZLcom/pspdfkit/document/editor/PdfDocumentEditor;Ljava/util/HashSet;Landroid/net/Uri;)Lio/reactivex/rxjava3/core/MaybeSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$LozeviPELeiB_ALRvZ_mGg53hs8(Lcom/pspdfkit/internal/m8;Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Lcom/pspdfkit/document/editor/FilePicker;Landroid/view/MenuItem;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m8;->a(Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Lcom/pspdfkit/document/editor/FilePicker;Landroid/view/MenuItem;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$bV33eRVwocvYJ1bp5h6aQxXqlBE(Lcom/pspdfkit/internal/m8;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/m8;->a(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$gxvJSvxPp67Kc1iHuaNxSA4SsfA(Lcom/pspdfkit/internal/m8;Landroid/content/Context;ILandroid/net/Uri;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/m8;->a(Landroid/content/Context;ILandroid/net/Uri;)Lio/reactivex/rxjava3/core/MaybeSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$rU_2s5PFi79eb4Ta5v2EZ4iXn_8(Lcom/pspdfkit/internal/m8;Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Ljava/util/HashSet;Lcom/pspdfkit/document/editor/FilePicker;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m8;->a(Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Ljava/util/HashSet;Lcom/pspdfkit/document/editor/FilePicker;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/m8;)Lcom/pspdfkit/internal/gl;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/m8;->b:Lcom/pspdfkit/internal/gl;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/m8;)Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/gl;Lcom/pspdfkit/internal/k8;Lcom/pspdfkit/ui/PdfThumbnailGrid;Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/m8;->g:Z

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/m8;->h:Z

    .line 10
    iput-object p1, p0, Lcom/pspdfkit/internal/m8;->b:Lcom/pspdfkit/internal/gl;

    .line 11
    iput-object p2, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    .line 12
    iput-object p3, p0, Lcom/pspdfkit/internal/m8;->e:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    .line 13
    iput-object p4, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    .line 14
    new-instance p1, Lcom/pspdfkit/internal/j8;

    invoke-direct {p1}, Lcom/pspdfkit/internal/j8;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/m8;->c:Lcom/pspdfkit/internal/j8;

    return-void
.end method

.method private synthetic a(Landroid/content/Context;ILandroid/net/Uri;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 215
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    new-instance v1, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v1, p3}, Lcom/pspdfkit/document/DocumentSource;-><init>(Landroid/net/Uri;)V

    .line 216
    invoke-virtual {v0, p1, v1, p2}, Lcom/pspdfkit/internal/k8;->importDocument(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;I)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 217
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->toMaybe()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method private static a(Landroid/content/Context;ZLcom/pspdfkit/document/editor/PdfDocumentEditor;Ljava/util/HashSet;Landroid/net/Uri;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_0
    new-array v2, v1, [Landroid/net/Uri;

    aput-object p4, v2, v0

    .line 118
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {p0, v2, v1}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Ljava/util/List;Z)V

    .line 119
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "w"

    invoke-virtual {v2, p4, v3}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    sget-boolean v1, Lcom/pspdfkit/internal/m8;->i:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p0, Ljava/lang/AssertionError;

    invoke-direct {p0}, Ljava/lang/AssertionError;-><init>()V

    throw p0

    :cond_1
    :goto_0
    const/4 v1, 0x0

    if-eqz p1, :cond_2

    .line 128
    invoke-interface {p2, p0, v0, v1}, Lcom/pspdfkit/document/editor/PdfDocumentEditor;->saveDocument(Landroid/content/Context;Ljava/io/OutputStream;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    .line 129
    invoke-virtual {p0}, Lio/reactivex/rxjava3/core/Completable;->toMaybe()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    .line 130
    const-class p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lio/reactivex/rxjava3/core/Maybe;->cast(Ljava/lang/Class;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    .line 131
    invoke-virtual {p0, p4}, Lio/reactivex/rxjava3/core/Maybe;->defaultIfEmpty(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    .line 132
    invoke-virtual {p0}, Lio/reactivex/rxjava3/core/Single;->toMaybe()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    return-object p0

    .line 136
    :cond_2
    invoke-interface {p2, p0, v0, p3, v1}, Lcom/pspdfkit/document/editor/PdfDocumentEditor;->exportPages(Landroid/content/Context;Ljava/io/OutputStream;Ljava/util/Set;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    .line 137
    invoke-virtual {p0}, Lio/reactivex/rxjava3/core/Completable;->toMaybe()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    .line 138
    const-class p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lio/reactivex/rxjava3/core/Maybe;->cast(Ljava/lang/Class;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    .line 139
    invoke-virtual {p0, p4}, Lio/reactivex/rxjava3/core/Maybe;->defaultIfEmpty(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    .line 140
    invoke-virtual {p0}, Lio/reactivex/rxjava3/core/Single;->toMaybe()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    return-object p0

    :catch_0
    move-exception p0

    new-array p1, v1, [Ljava/lang/Object;

    aput-object p0, p1, v0

    const-string p2, "PSPDFKit.DocumentEditor"

    const-string p3, "File not found"

    .line 141
    invoke-static {p2, p3, p1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    invoke-static {p0}, Lio/reactivex/rxjava3/core/Maybe;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    return-object p0
.end method

.method private a(Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Lcom/pspdfkit/document/editor/FilePicker;)V
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 19
    :try_start_0
    invoke-interface {p2}, Lcom/pspdfkit/document/editor/PdfDocumentEditor;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/document/PdfDocument;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 20
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 21
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_0

    goto :goto_1

    .line 22
    :cond_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 23
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x2e

    .line 24
    invoke-virtual {v3, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    if-ge v5, v2, :cond_1

    goto :goto_0

    .line 25
    :cond_1
    invoke-virtual {v3, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 26
    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x5f

    .line 27
    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    add-int/2addr v6, v2

    if-lez v6, :cond_3

    .line 31
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v5, v6

    const/16 v7, 0x28

    if-ne v5, v7, :cond_3

    .line 32
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v6

    invoke-virtual {v4, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v3, v2, v1

    const-string v1, "PSPDFKit.DocumentEditor"

    const-string v3, "Could not extract filename from Uri"

    .line 33
    invoke-static {v1, v3, v2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    :goto_1
    move-object v4, v0

    :cond_3
    :goto_2
    const-string v1, "android.intent.action.CREATE_DOCUMENT"

    .line 34
    invoke-interface {p3, v1, v4}, Lcom/pspdfkit/document/editor/FilePicker;->getDestinationUri(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p3

    .line 35
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/pspdfkit/internal/m8;->a(Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Ljava/util/HashSet;Lio/reactivex/rxjava3/core/Maybe;)V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Ljava/util/HashSet;Lcom/pspdfkit/document/editor/FilePicker;)V
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2
    :try_start_0
    invoke-interface {p2}, Lcom/pspdfkit/document/editor/PdfDocumentEditor;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/document/PdfDocument;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 3
    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 4
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    .line 5
    :cond_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 6
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x2e

    .line 7
    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    if-ge v4, v1, :cond_1

    goto :goto_0

    .line 8
    :cond_1
    invoke-virtual {v2, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 9
    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x5f

    .line 10
    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    add-int/2addr v5, v1

    if-lez v5, :cond_3

    .line 14
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v4, v5

    const/16 v6, 0x28

    if-ne v4, v6, :cond_3

    .line 15
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v5

    invoke-virtual {v3, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v0

    const-string v0, "PSPDFKit.DocumentEditor"

    const-string v2, "Could not extract filename from Uri"

    .line 16
    invoke-static {v0, v2, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    :goto_1
    const/4 v3, 0x0

    :cond_3
    :goto_2
    const-string v0, "android.intent.action.CREATE_DOCUMENT"

    .line 17
    invoke-interface {p4, v0, v3}, Lcom/pspdfkit/document/editor/FilePicker;->getDestinationUri(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p4

    .line 18
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m8;->a(Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Ljava/util/HashSet;Lio/reactivex/rxjava3/core/Maybe;)V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Ljava/util/HashSet;Lio/reactivex/rxjava3/core/Maybe;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/document/editor/PdfDocumentEditor;",
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .line 74
    new-instance v0, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

    invoke-direct {v0}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;-><init>()V

    if-eqz p3, :cond_1

    .line 75
    invoke-virtual {p3}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    .line 77
    sget v2, Lcom/pspdfkit/R$string;->pspdf__saving:I

    goto :goto_2

    :cond_2
    sget v2, Lcom/pspdfkit/R$string;->pspdf__exporting:I

    .line 78
    :goto_2
    invoke-virtual {v0, p1, v2}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;->showIndeterminateProgressDialog(Landroid/content/Context;I)V

    .line 81
    new-instance v2, Lcom/pspdfkit/internal/m8$$ExternalSyntheticLambda3;

    invoke-direct {v2, p1, v1, p2, p3}, Lcom/pspdfkit/internal/m8$$ExternalSyntheticLambda3;-><init>(Landroid/content/Context;ZLcom/pspdfkit/document/editor/PdfDocumentEditor;Ljava/util/HashSet;)V

    .line 82
    invoke-virtual {p4, v2}, Lio/reactivex/rxjava3/core/Maybe;->flatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p3

    .line 111
    invoke-interface {p2}, Lcom/pspdfkit/document/editor/PdfDocumentEditor;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/zf;

    const/4 p4, 0x5

    .line 112
    invoke-virtual {p2, p4}, Lcom/pspdfkit/internal/zf;->h(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    .line 113
    invoke-virtual {p3, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p2

    .line 115
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p3

    invoke-virtual {p2, p3}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p2

    new-instance p3, Lcom/pspdfkit/internal/m8$a;

    invoke-direct {p3, p0, v0, p1}, Lcom/pspdfkit/internal/m8$a;-><init>(Lcom/pspdfkit/internal/m8;Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;Landroid/content/Context;)V

    .line 116
    invoke-virtual {p2, p3}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/core/MaybeObserver;)V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/pspdfkit/internal/k8;Landroid/view/View;Lcom/pspdfkit/document/editor/FilePicker;)V
    .locals 2

    .line 37
    new-instance v0, Landroidx/appcompat/widget/PopupMenu;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Landroidx/appcompat/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 38
    new-instance p3, Lcom/pspdfkit/internal/m8$$ExternalSyntheticLambda0;

    invoke-direct {p3, p0, p1, p2, p4}, Lcom/pspdfkit/internal/m8$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/m8;Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Lcom/pspdfkit/document/editor/FilePicker;)V

    invoke-virtual {v0, p3}, Landroidx/appcompat/widget/PopupMenu;->setOnMenuItemClickListener(Landroidx/appcompat/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 53
    sget p1, Lcom/pspdfkit/R$menu;->pspdf__menu_document_editor_save:I

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/PopupMenu;->inflate(I)V

    .line 55
    invoke-virtual {p2}, Lcom/pspdfkit/internal/k8;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->isValidForEditing()Z

    move-result p1

    if-nez p1, :cond_0

    .line 56
    invoke-virtual {v0}, Landroidx/appcompat/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object p1

    sget p2, Lcom/pspdfkit/R$id;->pspdf__menu_document_editor_save:I

    invoke-interface {p1, p2}, Landroid/view/Menu;->removeItem(I)V

    .line 58
    :cond_0
    invoke-virtual {v0}, Landroidx/appcompat/widget/PopupMenu;->show()V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/pspdfkit/internal/k8;Ljava/util/HashSet;Lcom/pspdfkit/document/editor/FilePicker;)V
    .locals 7

    .line 36
    new-instance v6, Lcom/pspdfkit/internal/m8$$ExternalSyntheticLambda1;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/m8$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/m8;Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Ljava/util/HashSet;Lcom/pspdfkit/document/editor/FilePicker;)V

    invoke-direct {p0, p1, p3, v6}, Lcom/pspdfkit/internal/m8;->a(Landroid/content/Context;Ljava/util/HashSet;Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Landroid/content/Context;Ljava/util/HashSet;Ljava/lang/Runnable;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .line 143
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->REDACTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    invoke-interface {p3}, Ljava/lang/Runnable;->run()V

    return-void

    .line 149
    :cond_0
    new-instance v0, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

    invoke-direct {v0}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;-><init>()V

    const/4 v1, 0x1

    if-eqz p2, :cond_2

    .line 150
    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_3

    .line 154
    sget v2, Lcom/pspdfkit/R$string;->pspdf__saving:I

    goto :goto_2

    :cond_3
    sget v2, Lcom/pspdfkit/R$string;->pspdf__exporting:I

    .line 155
    :goto_2
    invoke-virtual {v0, p1, v2}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;->showIndeterminateProgressDialog(Landroid/content/Context;I)V

    .line 157
    iget-object v2, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/k8;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v2

    if-nez p2, :cond_4

    .line 162
    invoke-interface {v2}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p2

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->REDACT:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAllAnnotationsOfTypeAsync(Ljava/util/EnumSet;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p2

    goto :goto_4

    .line 165
    :cond_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 166
    invoke-virtual {p2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 167
    invoke-interface {v2}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v5

    sget-object v6, Lcom/pspdfkit/annotations/AnnotationType;->REDACT:Lcom/pspdfkit/annotations/AnnotationType;

    .line 168
    invoke-static {v6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v6

    invoke-interface {v5, v6, v4, v1}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAllAnnotationsOfTypeAsync(Ljava/util/EnumSet;II)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v4

    .line 169
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 172
    :cond_5
    invoke-static {v3}, Lio/reactivex/rxjava3/core/Observable;->concat(Ljava/lang/Iterable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p2

    .line 175
    :goto_4
    invoke-virtual {p2}, Lio/reactivex/rxjava3/core/Observable;->firstElement()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p2

    .line 176
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {p2, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p2

    new-instance v1, Lcom/pspdfkit/internal/m8$b;

    invoke-direct {v1, p3, v0, p1}, Lcom/pspdfkit/internal/m8$b;-><init>(Ljava/lang/Runnable;Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;Landroid/content/Context;)V

    .line 177
    invoke-virtual {p2, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/core/MaybeObserver;)V

    return-void
.end method

.method private synthetic a(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 218
    iget-object p1, p0, Lcom/pspdfkit/internal/m8;->e:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->exitDocumentEditingMode()V

    return-void
.end method

.method private a(Ljava/util/List;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;Z)V"
        }
    .end annotation

    .line 178
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/undo/EditingChange;

    .line 179
    invoke-virtual {v0}, Lcom/pspdfkit/undo/EditingChange;->getEditingOperation()Lcom/pspdfkit/undo/EditingOperation;

    move-result-object v1

    .line 180
    invoke-virtual {v0}, Lcom/pspdfkit/undo/EditingChange;->getAffectedPageIndex()I

    move-result v2

    .line 181
    invoke-virtual {v0}, Lcom/pspdfkit/undo/EditingChange;->getPageIndexDestination()I

    move-result v0

    .line 183
    sget-object v3, Lcom/pspdfkit/internal/m8$d;->a:[I

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v3, v1

    const/4 v3, 0x1

    if-eq v1, v3, :cond_4

    const/4 v3, 0x2

    if-eq v1, v3, :cond_3

    const/4 v3, 0x3

    if-eq v1, v3, :cond_3

    const/4 v3, 0x4

    if-eq v1, v3, :cond_1

    const/4 v3, 0x5

    if-eq v1, v3, :cond_0

    goto :goto_0

    .line 200
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v1, v2, v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a(II)V

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    .line 201
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->d(I)V

    goto :goto_0

    .line 203
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->c(I)V

    goto :goto_0

    .line 204
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    xor-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a(IZ)V

    goto :goto_0

    .line 205
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->b(I)V

    goto :goto_0

    :cond_5
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Lcom/pspdfkit/document/editor/FilePicker;Landroid/view/MenuItem;)Z
    .locals 3

    .line 59
    invoke-interface {p4}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/pspdfkit/R$id;->pspdf__menu_document_editor_save:I

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 60
    new-instance p3, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

    invoke-direct {p3}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;-><init>()V

    .line 61
    sget p4, Lcom/pspdfkit/R$string;->pspdf__saving:I

    invoke-virtual {p3, p1, p4}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;->showIndeterminateProgressDialog(Landroid/content/Context;I)V

    .line 63
    invoke-interface {p2, p1, v2}, Lcom/pspdfkit/document/editor/PdfDocumentEditor;->saveDocument(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p4

    .line 66
    invoke-interface {p2}, Lcom/pspdfkit/document/editor/PdfDocumentEditor;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x5

    .line 67
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->h(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    .line 68
    invoke-virtual {p4, p2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p2

    .line 70
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p4

    invoke-virtual {p2, p4}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p2

    new-instance p4, Lcom/pspdfkit/internal/n8;

    invoke-direct {p4, p0, p3, p1}, Lcom/pspdfkit/internal/n8;-><init>(Lcom/pspdfkit/internal/m8;Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;Landroid/content/Context;)V

    .line 71
    invoke-virtual {p2, p4}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/core/CompletableObserver;)V

    goto :goto_0

    .line 72
    :cond_0
    invoke-interface {p4}, Landroid/view/MenuItem;->getItemId()I

    move-result p4

    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_document_editor_save_as:I

    if-ne p4, v0, :cond_1

    .line 73
    new-instance p4, Lcom/pspdfkit/internal/m8$$ExternalSyntheticLambda5;

    invoke-direct {p4, p0, p1, p2, p3}, Lcom/pspdfkit/internal/m8$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/m8;Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Lcom/pspdfkit/document/editor/FilePicker;)V

    invoke-direct {p0, p1, v2, p4}, Lcom/pspdfkit/internal/m8;->a(Landroid/content/Context;Ljava/util/HashSet;Ljava/lang/Runnable;)V

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->c:Lcom/pspdfkit/internal/j8;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/j8;->onExitDocumentEditingMode(Lcom/pspdfkit/ui/special_mode/controller/DocumentEditingController;)V

    .line 214
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k8;->a()Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/jni/NativeDocumentEditor;)V
    .locals 2

    .line 207
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/k8;->a(Lcom/pspdfkit/internal/jni/NativeDocumentEditor;)V

    .line 208
    iget-object p1, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    const/4 v1, 0x1

    .line 209
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 210
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a(Lcom/pspdfkit/internal/jni/NativeDocumentEditor;)V

    .line 211
    iget-object p1, p0, Lcom/pspdfkit/internal/m8;->c:Lcom/pspdfkit/internal/j8;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/j8;->onEnterDocumentEditingMode(Lcom/pspdfkit/ui/special_mode/controller/DocumentEditingController;)V

    .line 212
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    const-string v0, "open_document_editor"

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/k8;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 206
    iput-boolean p1, p0, Lcom/pspdfkit/internal/m8;->h:Z

    return-void
.end method

.method public final b()V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->c:Lcom/pspdfkit/internal/j8;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/j8;->onDocumentEditingPageSelectionChanged(Lcom/pspdfkit/ui/special_mode/controller/DocumentEditingController;)V

    return-void
.end method

.method public final b(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/m8;->g:Z

    return-void
.end method

.method public final c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final duplicateSelectedPages()V
    .locals 4

    .line 1
    new-instance v0, Ljava/util/HashSet;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->getSelectedPages()Ljava/util/HashSet;

    move-result-object v1

    .line 3
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->a(Ljava/util/HashSet;)V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/k8;->duplicatePages(Ljava/util/Set;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    const-string v2, "perform_document_editor_action"

    .line 7
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "duplicate_selected_pages"

    .line 8
    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    .line 9
    invoke-static {v0}, Lcom/pspdfkit/internal/ft;->a(Ljava/util/HashSet;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v2, ","

    invoke-static {v2, v0}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "value"

    invoke-virtual {v1, v2, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method public final exitActiveMode()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k8;->canUndo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    new-instance v0, Landroidx/appcompat/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->e:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/pspdfkit/R$string;->pspdf__discard_changes:I

    .line 3
    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setMessage(I)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__ok:I

    new-instance v2, Lcom/pspdfkit/internal/m8$$ExternalSyntheticLambda4;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/m8$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/m8;)V

    .line 4
    invoke-virtual {v0, v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__cancel:I

    const/4 v2, 0x0

    .line 6
    invoke-virtual {v0, v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 7
    invoke-virtual {v0}, Landroidx/appcompat/app/AlertDialog$Builder;->show()Landroidx/appcompat/app/AlertDialog;

    goto :goto_0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->e:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->exitDocumentEditingMode()V

    :goto_0
    return-void
.end method

.method public final exportSelectedPages(Landroid/content/Context;)V
    .locals 3

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    new-instance v0, Ljava/util/HashSet;

    .line 54
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->getSelectedPages()Ljava/util/HashSet;

    move-result-object v1

    .line 55
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 56
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    iget-object v2, p0, Lcom/pspdfkit/internal/m8;->e:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getFilePicker()Lcom/pspdfkit/document/editor/FilePicker;

    move-result-object v2

    invoke-direct {p0, p1, v1, v0, v2}, Lcom/pspdfkit/internal/m8;->a(Landroid/content/Context;Lcom/pspdfkit/internal/k8;Ljava/util/HashSet;Lcom/pspdfkit/document/editor/FilePicker;)V

    .line 57
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    const-string v1, "perform_document_editor_action"

    .line 58
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    const-string v1, "action"

    const-string v2, "export_selected_pages"

    .line 59
    invoke-virtual {p1, v1, v2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 60
    invoke-static {v0}, Lcom/pspdfkit/internal/ft;->a(Ljava/util/HashSet;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v1, ","

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "value"

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method public final getDocumentEditingManager()Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->c:Lcom/pspdfkit/internal/j8;

    return-object v0
.end method

.method public final getSelectedPages()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->getSelectedPages()Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method public final getThumbnailGrid()Lcom/pspdfkit/ui/PdfThumbnailGrid;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->e:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    return-object v0
.end method

.method public final importDocument(Landroid/content/Context;)V
    .locals 3

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Ljava/util/HashSet;

    .line 55
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->getSelectedPages()Ljava/util/HashSet;

    move-result-object v1

    .line 56
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 57
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 58
    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 59
    :cond_0
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 60
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 62
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 63
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 64
    :cond_1
    :goto_1
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 65
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->e:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    .line 66
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getFilePicker()Lcom/pspdfkit/document/editor/FilePicker;

    move-result-object v1

    const-string v2, "android.intent.action.OPEN_DOCUMENT"

    invoke-interface {v1, v2}, Lcom/pspdfkit/document/editor/FilePicker;->getDestinationUri(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v1

    .line 67
    new-instance v2, Lcom/pspdfkit/internal/m8$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0, p1, v0}, Lcom/pspdfkit/internal/m8$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/m8;Landroid/content/Context;I)V

    .line 68
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Maybe;->flatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    .line 73
    invoke-virtual {v1}, Lcom/pspdfkit/internal/k8;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/zf;

    const/4 v2, 0x5

    .line 74
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->h(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    .line 75
    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 77
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance v1, Lcom/pspdfkit/internal/m8$c;

    invoke-direct {v1, p0, v0}, Lcom/pspdfkit/internal/m8$c;-><init>(Lcom/pspdfkit/internal/m8;I)V

    .line 78
    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/core/MaybeObserver;)V

    return-void
.end method

.method public final isDocumentEmpty()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isExportEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/m8;->h:Z

    return v0
.end method

.method public final isRedoEnabled()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k8;->canRedo()Z

    move-result v0

    return v0
.end method

.method public final isSaveAsEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/m8;->g:Z

    return v0
.end method

.method public final isUndoEnabled()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k8;->canUndo()Z

    move-result v0

    return v0
.end method

.method public final onCancelled()V
    .locals 0

    return-void
.end method

.method public final onNewPageReady(Lcom/pspdfkit/document/processor/NewPage;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    const/4 v1, 0x0

    .line 2
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/k8;->addPage(ILcom/pspdfkit/document/processor/NewPage;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 3
    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/m8;->a(Ljava/util/List;Z)V

    .line 5
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    const-string v0, "perform_document_editor_action"

    .line 6
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    const-string v0, "action"

    const-string v1, "insert_new_page"

    .line 7
    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method public final performSaving(Landroid/content/Context;Landroid/view/View;)V
    .locals 3

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 52
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "popupAnchorView"

    .line 53
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 105
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    .line 106
    iget-boolean v1, p0, Lcom/pspdfkit/internal/m8;->g:Z

    if-eqz v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->e:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->getFilePicker()Lcom/pspdfkit/document/editor/FilePicker;

    move-result-object v1

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/pspdfkit/internal/m8;->a(Landroid/content/Context;Lcom/pspdfkit/internal/k8;Landroid/view/View;Lcom/pspdfkit/document/editor/FilePicker;)V

    goto :goto_0

    .line 108
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/k8;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/zf;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 109
    new-instance p2, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

    invoke-direct {p2}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;-><init>()V

    .line 110
    sget v1, Lcom/pspdfkit/R$string;->pspdf__saving:I

    invoke-virtual {p2, p1, v1}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;->showIndeterminateProgressDialog(Landroid/content/Context;I)V

    .line 112
    invoke-virtual {v0, p1, v2}, Lcom/pspdfkit/internal/k8;->saveDocument(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 115
    invoke-virtual {v0}, Lcom/pspdfkit/internal/k8;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/zf;

    const/4 v2, 0x5

    .line 116
    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/zf;->h(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    .line 117
    invoke-virtual {v1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 119
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/n8;

    invoke-direct {v1, p0, p2, p1}, Lcom/pspdfkit/internal/n8;-><init>(Lcom/pspdfkit/internal/m8;Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;Landroid/content/Context;)V

    .line 120
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/core/CompletableObserver;)V

    .line 121
    :cond_1
    :goto_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    const-string p2, "perform_document_editor_action"

    .line 122
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    const-string p2, "action"

    const-string v0, "save_document"

    .line 123
    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 124
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method public final redo()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k8;->redo()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    .line 2
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/m8;->a(Ljava/util/List;Z)V

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    const-string v2, "perform_document_editor_action"

    .line 4
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "redo"

    .line 5
    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-object v0
.end method

.method public final removeSelectedPages()V
    .locals 4

    .line 1
    new-instance v0, Ljava/util/HashSet;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->getSelectedPages()Ljava/util/HashSet;

    move-result-object v1

    .line 3
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->b(Ljava/util/HashSet;)V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/k8;->removePages(Ljava/util/Set;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    const-string v2, "perform_document_editor_action"

    .line 7
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "remove_selected_pages"

    .line 8
    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    .line 9
    invoke-static {v0}, Lcom/pspdfkit/internal/ft;->a(Ljava/util/HashSet;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v2, ","

    invoke-static {v2, v0}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "value"

    invoke-virtual {v1, v2, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method public final rotateSelectedPages()V
    .locals 4

    .line 1
    new-instance v0, Ljava/util/HashSet;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->getSelectedPages()Ljava/util/HashSet;

    move-result-object v1

    .line 3
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 5
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 6
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->c(I)V

    goto :goto_0

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    const/16 v2, 0x5a

    invoke-virtual {v1, v0, v2}, Lcom/pspdfkit/internal/k8;->rotatePages(Ljava/util/Set;I)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    .line 8
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    const-string v2, "perform_document_editor_action"

    .line 9
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "rotate_selected_pages"

    .line 10
    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    .line 11
    invoke-static {v0}, Lcom/pspdfkit/internal/ft;->a(Ljava/util/HashSet;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v2, ","

    invoke-static {v2, v0}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "value"

    invoke-virtual {v1, v2, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method public final setSelectedPages(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->f:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView;->setSelectedPages(Ljava/util/Set;)V

    return-void
.end method

.method public final undo()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8;->d:Lcom/pspdfkit/internal/k8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k8;->undo()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    .line 2
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/m8;->a(Ljava/util/List;Z)V

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    const-string v2, "perform_document_editor_action"

    .line 4
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "undo"

    .line 5
    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-object v0
.end method

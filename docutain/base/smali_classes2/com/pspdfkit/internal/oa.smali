.class public final Lcom/pspdfkit/internal/oa;
.super Landroidx/appcompat/app/AppCompatDialogFragment;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/or;


# annotations
.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\u00a2\u0006\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/pspdfkit/internal/oa;",
        "Landroidx/appcompat/app/AppCompatDialogFragment;",
        "Lcom/pspdfkit/internal/or;",
        "<init>",
        "()V",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# static fields
.field public static final synthetic f:I


# instance fields
.field private b:Lcom/pspdfkit/internal/or;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

.field private e:Lcom/pspdfkit/internal/ui/dialog/signatures/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/oa;Lcom/pspdfkit/internal/or;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/oa;->b:Lcom/pspdfkit/internal/or;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/oa;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/oa;->d:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/oa;->c:Ljava/util/List;

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/oa;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    if-eqz v0, :cond_1

    .line 7
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->setItems(Ljava/util/List;)V

    goto :goto_0

    .line 9
    :cond_1
    iput-object p1, p0, Lcom/pspdfkit/internal/oa;->c:Ljava/util/List;

    :goto_0
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    if-eqz p1, :cond_0

    .line 1
    const-class v0, Lcom/pspdfkit/signatures/Signature;

    const-string v1, "STATE_SIGNATURES"

    invoke-static {p1, v1, v0}, Lcom/pspdfkit/utils/BundleExtensions;->getSupportParcelableArrayList(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/oa;->a(Ljava/util/List;)V

    .line 2
    const-class v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    const-string v1, "STATE_SIGNATURE_OPTIONS"

    invoke-static {p1, v1, v0}, Lcom/pspdfkit/utils/BundleExtensions;->getSupportParcelable(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    iput-object v0, p0, Lcom/pspdfkit/internal/oa;->d:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    .line 3
    :cond_0
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_Dialog_Light_Panel_Dim:I

    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Landroidx/appcompat/app/AppCompatDialogFragment;->setStyle(II)V

    .line 4
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object p1

    const-string v0, "super.onCreateDialog(savedInstanceState)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 5
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    return-object p1
.end method

.method public final onDismiss()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/oa;->b:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onDismiss()V

    .line 2
    :cond_0
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->dismiss()V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/oa;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    const-string v1, "null cannot be cast to non-null type java.util.ArrayList<com.pspdfkit.signatures.Signature>"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/ArrayList;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "STATE_SIGNATURES"

    .line 5
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/oa;->d:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    const-string v1, "STATE_SIGNATURE_OPTIONS"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public final onSignatureCreated(Lcom/pspdfkit/signatures/Signature;Z)V
    .locals 1

    const-string v0, "signature"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/oa;->b:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onSignatureCreated(Lcom/pspdfkit/signatures/Signature;Z)V

    .line 2
    :cond_0
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->dismiss()V

    return-void
.end method

.method public final onSignaturePicked(Lcom/pspdfkit/signatures/Signature;)V
    .locals 1

    const-string v0, "signature"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/oa;->b:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onSignaturePicked(Lcom/pspdfkit/signatures/Signature;)V

    .line 2
    :cond_0
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->dismiss()V

    return-void
.end method

.method public final onSignatureUiDataCollected(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V
    .locals 1

    const-string v0, "signature"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureUiData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/oa;->b:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;->onSignatureUiDataCollected(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V

    :cond_0
    return-void
.end method

.method public final onSignaturesDeleted(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/signatures/Signature;",
            ">;)V"
        }
    .end annotation

    const-string v0, "signatures"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/oa;->b:Lcom/pspdfkit/internal/or;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/or;->onSignaturesDeleted(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 7

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 3
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 4
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-string v2, "it.window ?: return"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_width:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 6
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_height:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    .line 11
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 12
    sget v5, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_width:I

    .line 13
    sget v6, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_height:I

    .line 14
    invoke-static {v4, v5, v6}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/res/Resources;II)Z

    move-result v4

    const/4 v5, -0x1

    if-nez v4, :cond_1

    const/4 v2, -0x1

    :cond_1
    if-nez v4, :cond_2

    const/4 v3, -0x1

    .line 21
    :cond_2
    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setLayout(II)V

    const/16 v2, 0x11

    .line 25
    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    const/4 v2, 0x0

    .line 26
    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    const/high16 v0, 0x4000000

    .line 32
    invoke-virtual {v1, v0}, Landroid/view/Window;->addFlags(I)V

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/oa;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    if-eqz v0, :cond_3

    xor-int/lit8 v1, v4, 0x1

    .line 35
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->setFullscreen(Z)V

    .line 36
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->setListener(Lcom/pspdfkit/internal/or;)V

    .line 37
    iget-object v1, p0, Lcom/pspdfkit/internal/oa;->c:Ljava/util/List;

    if-eqz v1, :cond_3

    .line 38
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->setItems(Ljava/util/List;)V

    const/4 v0, 0x0

    .line 39
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/oa;->a(Ljava/util/List;)V

    :cond_3
    return-void
.end method

.method public final onStop()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStop()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/oa;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->d()V

    :cond_0
    return-void
.end method

.method public final setupDialog(Landroid/app/Dialog;I)V
    .locals 3

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/appcompat/app/AppCompatDialogFragment;->setupDialog(Landroid/app/Dialog;I)V

    .line 4
    iget-object p2, p0, Lcom/pspdfkit/internal/oa;->d:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    if-eqz p2, :cond_0

    .line 6
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    .line 7
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {v0, v1, p2}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V

    .line 12
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/ui/dialog/signatures/d;->setListener(Lcom/pspdfkit/internal/or;)V

    .line 13
    sget p2, Lcom/pspdfkit/R$id;->pspdf__signature_layout:I

    invoke-virtual {v0, p2}, Landroid/view/View;->setId(I)V

    .line 14
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 15
    iput-object v0, p0, Lcom/pspdfkit/internal/oa;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/d;

    return-void

    .line 16
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Signature options are missing!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

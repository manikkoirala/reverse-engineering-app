.class public final Lcom/pspdfkit/internal/mb;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/mb$b;,
        Lcom/pspdfkit/internal/mb$a;
    }
.end annotation


# static fields
.field static final p:Ljava/nio/charset/Charset;

.field static final synthetic q:Z = true


# instance fields
.field a:Ljava/nio/ByteBuffer;

.field b:I

.field c:I

.field d:[I

.field e:I

.field f:Z

.field g:Z

.field h:I

.field i:[I

.field j:I

.field k:I

.field l:Z

.field m:Ljava/nio/charset/CharsetEncoder;

.field n:Ljava/nio/ByteBuffer;

.field o:Lcom/pspdfkit/internal/mb$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "UTF-8"

    .line 5
    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/mb;->p:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/mb;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .line 28
    new-instance p1, Lcom/pspdfkit/internal/mb$b;

    invoke-direct {p1}, Lcom/pspdfkit/internal/mb$b;-><init>()V

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/mb;-><init>(Lcom/pspdfkit/internal/mb$b;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/mb$b;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput v0, p0, Lcom/pspdfkit/internal/mb;->c:I

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/mb;->d:[I

    const/4 v0, 0x0

    .line 4
    iput v0, p0, Lcom/pspdfkit/internal/mb;->e:I

    .line 5
    iput-boolean v0, p0, Lcom/pspdfkit/internal/mb;->f:Z

    .line 6
    iput-boolean v0, p0, Lcom/pspdfkit/internal/mb;->g:Z

    const/16 v1, 0x10

    new-array v1, v1, [I

    .line 8
    iput-object v1, p0, Lcom/pspdfkit/internal/mb;->i:[I

    .line 9
    iput v0, p0, Lcom/pspdfkit/internal/mb;->j:I

    .line 10
    iput v0, p0, Lcom/pspdfkit/internal/mb;->k:I

    .line 11
    iput-boolean v0, p0, Lcom/pspdfkit/internal/mb;->l:Z

    .line 12
    sget-object v0, Lcom/pspdfkit/internal/mb;->p:Ljava/nio/charset/Charset;

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->newEncoder()Ljava/nio/charset/CharsetEncoder;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/mb;->m:Ljava/nio/charset/CharsetEncoder;

    const/16 v0, 0x400

    .line 25
    iput v0, p0, Lcom/pspdfkit/internal/mb;->b:I

    .line 26
    iput-object p1, p0, Lcom/pspdfkit/internal/mb;->o:Lcom/pspdfkit/internal/mb$a;

    .line 27
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/mb$b;->a(I)Ljava/nio/ByteBuffer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 11

    .line 62
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->d:[I

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/pspdfkit/internal/mb;->f:Z

    if-eqz v0, :cond_9

    const/4 v0, 0x4

    const/4 v1, 0x0

    .line 63
    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/mb;->d(II)V

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/mb;->c(I)V

    .line 64
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v0

    .line 66
    iget v2, p0, Lcom/pspdfkit/internal/mb;->e:I

    add-int/lit8 v2, v2, -0x1

    :goto_0
    if-ltz v2, :cond_0

    .line 68
    iget-object v3, p0, Lcom/pspdfkit/internal/mb;->d:[I

    aget v3, v3, v2

    if-nez v3, :cond_0

    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v3, v2, 0x1

    :goto_1
    const/4 v4, 0x2

    if-ltz v2, :cond_2

    .line 72
    iget-object v5, p0, Lcom/pspdfkit/internal/mb;->d:[I

    aget v5, v5, v2

    if-eqz v5, :cond_1

    sub-int v5, v0, v5

    goto :goto_2

    :cond_1
    const/4 v5, 0x0

    :goto_2
    int-to-short v5, v5

    .line 73
    invoke-virtual {p0, v4, v1}, Lcom/pspdfkit/internal/mb;->d(II)V

    invoke-virtual {p0, v5}, Lcom/pspdfkit/internal/mb;->a(S)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 74
    :cond_2
    iget v2, p0, Lcom/pspdfkit/internal/mb;->h:I

    sub-int v2, v0, v2

    int-to-short v2, v2

    .line 75
    invoke-virtual {p0, v4, v1}, Lcom/pspdfkit/internal/mb;->d(II)V

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/mb;->a(S)V

    add-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x2

    int-to-short v2, v3

    .line 76
    invoke-virtual {p0, v4, v1}, Lcom/pspdfkit/internal/mb;->d(II)V

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/mb;->a(S)V

    const/4 v2, 0x0

    .line 77
    :goto_3
    iget v3, p0, Lcom/pspdfkit/internal/mb;->j:I

    if-ge v2, v3, :cond_6

    .line 78
    iget-object v3, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/Buffer;->capacity()I

    move-result v3

    iget-object v5, p0, Lcom/pspdfkit/internal/mb;->i:[I

    aget v5, v5, v2

    sub-int/2addr v3, v5

    .line 79
    iget v5, p0, Lcom/pspdfkit/internal/mb;->b:I

    .line 80
    iget-object v6, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v6, v3}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v6

    .line 81
    iget-object v7, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v7, v5}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v7

    if-ne v6, v7, :cond_5

    const/4 v7, 0x2

    :goto_4
    if-ge v7, v6, :cond_4

    .line 83
    iget-object v8, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    add-int v9, v3, v7

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v8

    iget-object v9, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    add-int v10, v5, v7

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v9

    if-eq v8, v9, :cond_3

    goto :goto_5

    :cond_3
    add-int/lit8 v7, v7, 0x2

    goto :goto_4

    .line 87
    :cond_4
    iget-object v3, p0, Lcom/pspdfkit/internal/mb;->i:[I

    aget v2, v3, v2

    goto :goto_6

    :cond_5
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    :goto_6
    if-eqz v2, :cond_7

    .line 95
    iget-object v3, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/Buffer;->capacity()I

    move-result v3

    sub-int/2addr v3, v0

    iput v3, p0, Lcom/pspdfkit/internal/mb;->b:I

    .line 97
    iget-object v4, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    sub-int/2addr v2, v0

    invoke-virtual {v4, v3, v2}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    goto :goto_7

    .line 101
    :cond_7
    iget v2, p0, Lcom/pspdfkit/internal/mb;->j:I

    iget-object v3, p0, Lcom/pspdfkit/internal/mb;->i:[I

    array-length v5, v3

    if-ne v2, v5, :cond_8

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/internal/mb;->i:[I

    .line 102
    :cond_8
    iget-object v2, p0, Lcom/pspdfkit/internal/mb;->i:[I

    iget v3, p0, Lcom/pspdfkit/internal/mb;->j:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/pspdfkit/internal/mb;->j:I

    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v4

    aput v4, v2, v3

    .line 104
    iget-object v2, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/Buffer;->capacity()I

    move-result v3

    sub-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v4

    sub-int/2addr v4, v0

    invoke-virtual {v2, v3, v4}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 107
    :goto_7
    iput-boolean v1, p0, Lcom/pspdfkit/internal/mb;->f:Z

    return v0

    .line 108
    :cond_9
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "FlatBuffers: endObject called without startObject"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;)I
    .locals 5

    .line 8
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    int-to-float v0, v0

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/mb;->m:Ljava/nio/charset/CharsetEncoder;

    invoke-virtual {v1}, Ljava/nio/charset/CharsetEncoder;->maxBytesPerChar()F

    move-result v1

    mul-float v1, v1, v0

    float-to-int v0, v1

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/mb;->n:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/nio/Buffer;->capacity()I

    move-result v1

    if-ge v1, v0, :cond_1

    :cond_0
    const/16 v1, 0x80

    .line 11
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/mb;->n:Ljava/nio/ByteBuffer;

    .line 14
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->n:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 16
    instance-of v0, p1, Ljava/nio/CharBuffer;

    if-eqz v0, :cond_2

    check-cast p1, Ljava/nio/CharBuffer;

    goto :goto_0

    .line 17
    :cond_2
    invoke-static {p1}, Ljava/nio/CharBuffer;->wrap(Ljava/lang/CharSequence;)Ljava/nio/CharBuffer;

    move-result-object p1

    .line 18
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->m:Ljava/nio/charset/CharsetEncoder;

    iget-object v1, p0, Lcom/pspdfkit/internal/mb;->n:Ljava/nio/ByteBuffer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Ljava/nio/charset/CharsetEncoder;->encode(Ljava/nio/CharBuffer;Ljava/nio/ByteBuffer;Z)Ljava/nio/charset/CoderResult;

    move-result-object p1

    .line 19
    invoke-virtual {p1}, Ljava/nio/charset/CoderResult;->isError()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 21
    :try_start_0
    invoke-virtual {p1}, Ljava/nio/charset/CoderResult;->throwException()V
    :try_end_0
    .catch Ljava/nio/charset/CharacterCodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 23
    new-instance v0, Ljava/lang/Error;

    invoke-direct {v0, p1}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 27
    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/mb;->n:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 28
    iget-object p1, p0, Lcom/pspdfkit/internal/mb;->n:Ljava/nio/ByteBuffer;

    .line 29
    invoke-virtual {p1}, Ljava/nio/Buffer;->remaining()I

    move-result v0

    const/4 v1, 0x0

    .line 30
    invoke-virtual {p0, v2, v1}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 31
    iget-object v3, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    iget v4, p0, Lcom/pspdfkit/internal/mb;->b:I

    sub-int/2addr v4, v2

    iput v4, p0, Lcom/pspdfkit/internal/mb;->b:I

    invoke-virtual {v3, v4, v1}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 32
    invoke-virtual {p0, v2, v0, v2}, Lcom/pspdfkit/internal/mb;->a(III)V

    .line 33
    iget-object v1, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/mb;->b:I

    sub-int/2addr v2, v0

    iput v2, p0, Lcom/pspdfkit/internal/mb;->b:I

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 35
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->b()I

    move-result p1

    return p1
.end method

.method public final a(F)V
    .locals 2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/pspdfkit/internal/mb;->b:I

    add-int/lit8 v1, v1, -0x4

    iput v1, p0, Lcom/pspdfkit/internal/mb;->b:I

    invoke-virtual {v0, v1, p1}, Ljava/nio/ByteBuffer;->putFloat(IF)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public final a(I)V
    .locals 2

    const/4 v0, 0x4

    const/4 v1, 0x0

    .line 4
    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 5
    sget-boolean v1, Lcom/pspdfkit/internal/mb;->q:Z

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v1

    if-gt p1, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 6
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v1

    sub-int/2addr v1, p1

    add-int/2addr v1, v0

    .line 7
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/mb;->c(I)V

    return-void
.end method

.method public final a(IB)V
    .locals 3

    .line 46
    iget-boolean v0, p0, Lcom/pspdfkit/internal/mb;->l:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 47
    invoke-virtual {p0, v1, v0}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 48
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/mb;->b:I

    sub-int/2addr v2, v1

    iput v2, p0, Lcom/pspdfkit/internal/mb;->b:I

    invoke-virtual {v0, v2, p2}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 49
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/mb;->d(I)V

    :cond_1
    return-void
.end method

.method public final a(IF)V
    .locals 5

    .line 59
    iget-boolean v0, p0, Lcom/pspdfkit/internal/mb;->l:Z

    if-nez v0, :cond_0

    float-to-double v0, p2

    const-wide/16 v2, 0x0

    cmpl-double v4, v0, v2

    if-eqz v4, :cond_1

    :cond_0
    const/4 v0, 0x4

    const/4 v1, 0x0

    .line 60
    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/mb;->d(II)V

    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 61
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/mb;->d(I)V

    :cond_1
    return-void
.end method

.method public final a(II)V
    .locals 2

    .line 53
    iget-boolean v0, p0, Lcom/pspdfkit/internal/mb;->l:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x4

    const/4 v1, 0x0

    .line 54
    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/mb;->d(II)V

    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/mb;->c(I)V

    .line 55
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/mb;->d(I)V

    :cond_1
    return-void
.end method

.method public final a(III)V
    .locals 1

    .line 36
    iget-boolean v0, p0, Lcom/pspdfkit/internal/mb;->f:Z

    if-nez v0, :cond_0

    .line 37
    iput p2, p0, Lcom/pspdfkit/internal/mb;->k:I

    mul-int p1, p1, p2

    const/4 p2, 0x4

    .line 38
    invoke-virtual {p0, p2, p1}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 39
    invoke-virtual {p0, p3, p1}, Lcom/pspdfkit/internal/mb;->d(II)V

    const/4 p1, 0x1

    .line 40
    iput-boolean p1, p0, Lcom/pspdfkit/internal/mb;->f:Z

    return-void

    .line 41
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "FlatBuffers: object serialization must not be nested."

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method public final a(IJ)V
    .locals 3

    .line 56
    iget-boolean v0, p0, Lcom/pspdfkit/internal/mb;->l:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-eqz v2, :cond_1

    :cond_0
    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 57
    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/mb;->d(II)V

    invoke-virtual {p0, p2, p3}, Lcom/pspdfkit/internal/mb;->a(J)V

    .line 58
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/mb;->d(I)V

    :cond_1
    return-void
.end method

.method public final a(IS)V
    .locals 2

    .line 50
    iget-boolean v0, p0, Lcom/pspdfkit/internal/mb;->l:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 51
    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/mb;->d(II)V

    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/mb;->a(S)V

    .line 52
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/mb;->d(I)V

    :cond_1
    return-void
.end method

.method public final a(IZZ)V
    .locals 2

    .line 42
    iget-boolean v0, p0, Lcom/pspdfkit/internal/mb;->l:Z

    if-nez v0, :cond_0

    if-eq p2, p3, :cond_1

    :cond_0
    const/4 p3, 0x0

    const/4 v0, 0x1

    .line 43
    invoke-virtual {p0, v0, p3}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 44
    iget-object p3, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/pspdfkit/internal/mb;->b:I

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/pspdfkit/internal/mb;->b:I

    int-to-byte p2, p2

    invoke-virtual {p3, v1, p2}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 45
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/mb;->d(I)V

    :cond_1
    return-void
.end method

.method public final a(J)V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/pspdfkit/internal/mb;->b:I

    add-int/lit8 v1, v1, -0x8

    iput v1, p0, Lcom/pspdfkit/internal/mb;->b:I

    invoke-virtual {v0, v1, p1, p2}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public final a(S)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/pspdfkit/internal/mb;->b:I

    add-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/pspdfkit/internal/mb;->b:I

    invoke-virtual {v0, v1, p1}, Ljava/nio/ByteBuffer;->putShort(IS)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public final b()I
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/mb;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/mb;->f:Z

    .line 4
    iget v0, p0, Lcom/pspdfkit/internal/mb;->k:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/mb;->c(I)V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v0

    return v0

    .line 6
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "FlatBuffers: endVector called without startVector"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final b(I)V
    .locals 2

    .line 8
    iget v0, p0, Lcom/pspdfkit/internal/mb;->c:I

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 9
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/mb;->a(I)V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    iget v0, p0, Lcom/pspdfkit/internal/mb;->b:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    const/4 p1, 0x1

    .line 14
    iput-boolean p1, p0, Lcom/pspdfkit/internal/mb;->g:Z

    return-void
.end method

.method public final b(II)V
    .locals 1

    .line 7
    iget-boolean v0, p0, Lcom/pspdfkit/internal/mb;->l:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/mb;->a(I)V

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/mb;->d(I)V

    :cond_1
    return-void
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/pspdfkit/internal/mb;->l:Z

    return-void
.end method

.method public final c(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/pspdfkit/internal/mb;->b:I

    add-int/lit8 v1, v1, -0x4

    iput v1, p0, Lcom/pspdfkit/internal/mb;->b:I

    invoke-virtual {v0, v1, p1}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public final c(II)V
    .locals 1

    if-eqz p2, :cond_1

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 3
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/mb;->d(I)V

    goto :goto_0

    .line 4
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "FlatBuffers: struct must be serialized inline."

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public final d()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->capacity()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/mb;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final d(I)V
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->d:[I

    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v1

    aput v1, v0, p1

    return-void
.end method

.method public final d(II)V
    .locals 8

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/mb;->c:I

    if-le p1, v0, :cond_0

    iput p1, p0, Lcom/pspdfkit/internal/mb;->c:I

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->capacity()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/mb;->b:I

    sub-int/2addr v0, v1

    add-int/2addr v0, p2

    not-int v0, v0

    const/4 v1, 0x1

    add-int/2addr v0, v1

    add-int/lit8 v2, p1, -0x1

    and-int/2addr v0, v2

    .line 7
    :goto_0
    iget v2, p0, Lcom/pspdfkit/internal/mb;->b:I

    add-int v3, v0, p1

    add-int/2addr v3, p2

    const/4 v4, 0x0

    if-ge v2, v3, :cond_3

    .line 8
    iget-object v2, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/Buffer;->capacity()I

    move-result v2

    .line 9
    iget-object v3, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    iget-object v5, p0, Lcom/pspdfkit/internal/mb;->o:Lcom/pspdfkit/internal/mb$a;

    .line 10
    invoke-virtual {v3}, Ljava/nio/Buffer;->capacity()I

    move-result v6

    const/high16 v7, -0x40000000    # -2.0f

    and-int/2addr v7, v6

    if-nez v7, :cond_2

    if-nez v6, :cond_1

    const/4 v7, 0x1

    goto :goto_1

    :cond_1
    shl-int/lit8 v7, v6, 0x1

    .line 14
    :goto_1
    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 15
    check-cast v5, Lcom/pspdfkit/internal/mb$b;

    invoke-virtual {v5, v7}, Lcom/pspdfkit/internal/mb$b;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    sub-int/2addr v7, v6

    .line 16
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 17
    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 18
    iput-object v4, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    .line 19
    iget v3, p0, Lcom/pspdfkit/internal/mb;->b:I

    invoke-virtual {v4}, Ljava/nio/Buffer;->capacity()I

    move-result v4

    sub-int/2addr v4, v2

    add-int/2addr v4, v3

    iput v4, p0, Lcom/pspdfkit/internal/mb;->b:I

    goto :goto_0

    .line 20
    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "FlatBuffers: cannot grow buffer beyond 2 gigabytes."

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_3
    const/4 p1, 0x0

    :goto_2
    if-ge p1, v0, :cond_4

    .line 21
    iget-object p2, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/mb;->b:I

    sub-int/2addr v2, v1

    iput v2, p0, Lcom/pspdfkit/internal/mb;->b:I

    invoke-virtual {p2, v2, v4}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    :cond_4
    return-void
.end method

.method public final e(I)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/mb;->f:Z

    if-nez v0, :cond_2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->d:[I

    if-eqz v0, :cond_0

    array-length v0, v0

    if-ge v0, p1, :cond_1

    :cond_0
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/pspdfkit/internal/mb;->d:[I

    .line 3
    :cond_1
    iput p1, p0, Lcom/pspdfkit/internal/mb;->e:I

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->d:[I

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, v1}, Ljava/util/Arrays;->fill([IIII)V

    const/4 p1, 0x1

    .line 5
    iput-boolean p1, p0, Lcom/pspdfkit/internal/mb;->f:Z

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->d()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/mb;->h:I

    return-void

    .line 7
    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "FlatBuffers: object serialization must not be nested."

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method public final e()[B
    .locals 3

    .line 8
    iget v0, p0, Lcom/pspdfkit/internal/mb;->b:I

    iget-object v1, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->capacity()I

    move-result v1

    iget v2, p0, Lcom/pspdfkit/internal/mb;->b:I

    sub-int/2addr v1, v2

    .line 9
    iget-boolean v2, p0, Lcom/pspdfkit/internal/mb;->g:Z

    if-eqz v2, :cond_0

    .line 10
    new-array v1, v1, [B

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/mb;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    return-object v1

    .line 13
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "FlatBuffers: you can only access the serialized buffer after it has been finished by FlatBufferBuilder.finish()."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.class public final Lcom/pspdfkit/internal/n6;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/contentediting/defaults/ContentEditingPreferencesManager;


# instance fields
.field private final a:Lcom/pspdfkit/internal/xn;

.field private final b:Lcom/pspdfkit/internal/b6;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/xn;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/xn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/n6;->a:Lcom/pspdfkit/internal/xn;

    .line 3
    new-instance p1, Lcom/pspdfkit/internal/b6;

    invoke-direct {p1}, Lcom/pspdfkit/internal/b6;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/n6;->b:Lcom/pspdfkit/internal/b6;

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/b6;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/n6;->b:Lcom/pspdfkit/internal/b6;

    return-object v0
.end method

.method public final getFillColor()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/n6;->a:Lcom/pspdfkit/internal/xn;

    const/high16 v1, -0x1000000

    const-string v2, "content_editing_preferences_fill_color_"

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/xn;->a(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final setFillColor(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/n6;->a:Lcom/pspdfkit/internal/xn;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "content_editing_preferences_fill_color_"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

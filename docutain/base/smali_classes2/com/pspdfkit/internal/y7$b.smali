.class final Lcom/pspdfkit/internal/y7$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroidx/activity/result/ActivityResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/y7;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroidx/activity/result/ActivityResultCallback<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/y7;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/y7;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/y7$b;->a:Lcom/pspdfkit/internal/y7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActivityResult(Ljava/lang/Object;)V
    .locals 3

    .line 1
    check-cast p1, Landroid/net/Uri;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/y7$b;->a:Lcom/pspdfkit/internal/y7;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 3
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Ljava/util/List;Z)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/y7$b;->a:Lcom/pspdfkit/internal/y7;

    invoke-static {v0}, Lcom/pspdfkit/internal/y7;->b(Lcom/pspdfkit/internal/y7;)Lkotlin/jvm/functions/Function1;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/y7$b;->a:Lcom/pspdfkit/internal/y7;

    invoke-static {p1}, Lcom/pspdfkit/internal/y7;->a(Lcom/pspdfkit/internal/y7;)V

    return-void
.end method

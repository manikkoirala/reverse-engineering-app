.class public final Lcom/pspdfkit/internal/xs;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/xs$a;,
        Lcom/pspdfkit/internal/xs$b;
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/xs$a;

.field private c:Landroidx/recyclerview/widget/RecyclerView;

.field private d:Lcom/pspdfkit/internal/xs$b;

.field private final e:Ljava/util/ArrayList;

.field private f:Lcom/pspdfkit/annotations/stamps/StampPickerItem;


# direct methods
.method public static synthetic $r8$lambda$jNIMPVxR-GX4Ssm73qTUHcSAjUk(Lcom/pspdfkit/internal/xs;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/xs;->a(Lcom/pspdfkit/internal/xs;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/xs$a;)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/xs;->b:Lcom/pspdfkit/internal/xs$a;

    .line 86
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/xs;->e:Ljava/util/ArrayList;

    .line 91
    new-instance p2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p2, v0, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 p2, 0x1

    .line 95
    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 97
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__stamps_picker_custom_section:I

    invoke-virtual {p2, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 98
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    instance-of v2, v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    goto :goto_0

    :cond_0
    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_1

    const/4 v2, 0x0

    .line 99
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 102
    :cond_1
    new-instance v1, Lcom/pspdfkit/internal/xs$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/xs$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/xs;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__stamps_selection_grid:I

    invoke-virtual {p2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 108
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/zs;->a(Landroid/content/Context;)Landroid/content/res/TypedArray;

    move-result-object v1

    const-string v2, "getStampPickerStyle(getContext())"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__StampPicker_pspdf__stamp_grid_backgroundColor:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 110
    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 111
    sget v1, Lcom/pspdfkit/R$id;->pspdf__stamp_selection_recyclerview:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "selectionGrid.findViewBy\u2026p_selection_recyclerview)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v1, p0, Lcom/pspdfkit/internal/xs;->c:Landroidx/recyclerview/widget/RecyclerView;

    const/16 v1, 0x8

    .line 113
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v1

    .line 114
    iget-object v2, p0, Lcom/pspdfkit/internal/xs;->c:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v1, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 115
    iget-object v1, p0, Lcom/pspdfkit/internal/xs;->c:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v2, Landroidx/recyclerview/widget/GridLayoutManager;

    const/4 v3, 0x2

    invoke-direct {v2, p1, v3}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 116
    new-instance v1, Lcom/pspdfkit/internal/xs$b;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/xs$b;-><init>(Lcom/pspdfkit/internal/xs;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/xs;->d:Lcom/pspdfkit/internal/xs$b;

    .line 117
    iget-object p1, p0, Lcom/pspdfkit/internal/xs;->c:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 120
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p1, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v0, 0x3f800000    # 1.0f

    .line 124
    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 125
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 126
    invoke-virtual {p0, p2, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/xs;)Lcom/pspdfkit/internal/xs$a;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/xs;->b:Lcom/pspdfkit/internal/xs$a;

    return-object p0
.end method

.method private static final a(Lcom/pspdfkit/internal/xs;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/xs;->f:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    if-nez p1, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    iget-object p0, p0, Lcom/pspdfkit/internal/xs;->b:Lcom/pspdfkit/internal/xs$a;

    if-eqz p0, :cond_1

    invoke-interface {p0, p1}, Lcom/pspdfkit/internal/xs$a;->a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/xs;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/xs;->e:Ljava/util/ArrayList;

    return-object p0
.end method


# virtual methods
.method public final getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/stamps/StampPickerItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xs;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final setItems(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/stamps/StampPickerItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "items"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xs;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 36
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 37
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 38
    move-object v3, v2

    check-cast v3, Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    .line 39
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->isCustomStamp()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 74
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 76
    :cond_0
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 79
    :cond_1
    new-instance p1, Lkotlin/Pair;

    invoke-direct {p1, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 80
    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 82
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    .line 83
    sget v1, Lcom/pspdfkit/R$id;->pspdf__stamps_custom_section:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v0, :cond_4

    .line 86
    sget v2, Lcom/pspdfkit/R$id;->pspdf__custom_stamp_container:I

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    if-eqz v2, :cond_2

    .line 87
    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 88
    iget-object v3, p0, Lcom/pspdfkit/internal/xs;->d:Lcom/pspdfkit/internal/xs$b;

    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/xs$b;->a(Landroid/view/ViewGroup;)Lcom/pspdfkit/internal/xs$b$a;

    move-result-object v3

    .line 89
    invoke-virtual {v3, v0}, Lcom/pspdfkit/internal/xs$b$a;->a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V

    .line 90
    iget-object v3, v3, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v4, "stampsAdapter.onCreateVi\u2026               }.itemView"

    .line 92
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v4, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    if-nez v1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    .line 95
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_4
    if-nez v1, :cond_5

    goto :goto_1

    :cond_5
    const/16 v2, 0x8

    .line 97
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 100
    :goto_1
    iput-object v0, p0, Lcom/pspdfkit/internal/xs;->f:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    .line 102
    iget-object v0, p0, Lcom/pspdfkit/internal/xs;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 103
    iget-object p1, p0, Lcom/pspdfkit/internal/xs;->d:Lcom/pspdfkit/internal/xs$b;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.class final Lcom/pspdfkit/internal/k6$g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/k6;->t()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/k6;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/k6;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/k6$g;->a:Lcom/pspdfkit/internal/k6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/o6;

    const-string v0, "result"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 320
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$g;->a:Lcom/pspdfkit/internal/k6;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/o6;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/o6;->b()Lcom/pspdfkit/internal/jni/NativeContentEditingResult;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Ljava/util/List;Lcom/pspdfkit/internal/jni/NativeContentEditingResult;)V

    return-void
.end method

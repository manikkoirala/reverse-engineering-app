.class public abstract Lcom/pspdfkit/internal/gs;
.super Lcom/pspdfkit/internal/ug;
.source "SourceFile"


# instance fields
.field protected u:Ljava/util/ArrayList;

.field protected v:Z


# direct methods
.method protected constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V
    .locals 8

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object/from16 v7, p8

    .line 1
    invoke-direct/range {v0 .. v7}, Lcom/pspdfkit/internal/ug;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFILcom/pspdfkit/internal/cm;)V

    move v1, p7

    .line 10
    iput-boolean v1, v0, Lcom/pspdfkit/internal/gs;->v:Z

    .line 11
    invoke-direct {p0}, Lcom/pspdfkit/internal/gs;->B()V

    return-void
.end method

.method private B()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    .line 2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v2

    .line 5
    iget v3, v2, Lcom/pspdfkit/utils/Size;->width:F

    .line 6
    iget v2, v2, Lcom/pspdfkit/utils/Size;->height:F

    .line 11
    iget-boolean v4, p0, Lcom/pspdfkit/internal/gs;->v:Z

    if-eqz v4, :cond_0

    .line 12
    iget v4, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v4, v4

    div-float/2addr v4, v3

    iget v5, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v5, v5

    div-float/2addr v5, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    goto :goto_1

    .line 14
    :cond_0
    iget v4, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v4, v4

    div-float/2addr v4, v3

    :goto_1
    mul-float v3, v3, v4

    mul-float v2, v2, v4

    .line 19
    iget-object v4, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    new-instance v5, Lcom/pspdfkit/utils/Size;

    invoke-direct {v5, v3, v2}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final d(I)I
    .locals 0

    const/4 p1, -0x1

    return p1
.end method

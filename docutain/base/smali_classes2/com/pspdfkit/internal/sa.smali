.class public final Lcom/pspdfkit/internal/sa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/files/EmbeddedFile;


# instance fields
.field private a:Lcom/pspdfkit/internal/zf;

.field private b:Lcom/pspdfkit/annotations/FileAnnotation;

.field private final c:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:J

.field private g:Ljava/lang/String;

.field private h:Ljava/util/Date;


# direct methods
.method public static synthetic $r8$lambda$HGiLPvJoop6Ws2pD0Dcecq2IAD8(Lcom/pspdfkit/internal/sa;Ljava/io/OutputStream;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/sa;->a(Ljava/io/OutputStream;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/annotations/FileAnnotation;Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/sa;->d:Z

    const-string v0, ""

    .line 6
    iput-object v0, p0, Lcom/pspdfkit/internal/sa;->e:Ljava/lang/String;

    const-wide/16 v0, -0x1

    .line 9
    iput-wide v0, p0, Lcom/pspdfkit/internal/sa;->f:J

    const-string v0, "annotation"

    .line 19
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resourceId"

    .line 20
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/pspdfkit/internal/sa;->b:Lcom/pspdfkit/annotations/FileAnnotation;

    .line 22
    iput-object p2, p0, Lcom/pspdfkit/internal/sa;->c:Ljava/lang/String;

    .line 23
    invoke-virtual {p0}, Lcom/pspdfkit/internal/sa;->a()V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;Ljava/lang/String;)V
    .locals 2

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 25
    iput-boolean v0, p0, Lcom/pspdfkit/internal/sa;->d:Z

    const-string v0, ""

    .line 29
    iput-object v0, p0, Lcom/pspdfkit/internal/sa;->e:Ljava/lang/String;

    const-wide/16 v0, -0x1

    .line 32
    iput-wide v0, p0, Lcom/pspdfkit/internal/sa;->f:J

    const-string v0, "document"

    .line 51
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resourceId"

    .line 52
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iput-object p1, p0, Lcom/pspdfkit/internal/sa;->a:Lcom/pspdfkit/internal/zf;

    .line 54
    iput-object p2, p0, Lcom/pspdfkit/internal/sa;->c:Ljava/lang/String;

    .line 55
    invoke-virtual {p0}, Lcom/pspdfkit/internal/sa;->a()V

    return-void
.end method

.method private synthetic a(Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/sa;->writeToStream(Ljava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .line 2
    monitor-enter p0

    .line 3
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/sa;->d:Z

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/sa;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/sa;->b:Lcom/pspdfkit/annotations/FileAnnotation;

    if-eqz v0, :cond_2

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_3

    .line 8
    monitor-exit p0

    return-void

    .line 11
    :cond_3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/r1;->d()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v0

    .line 12
    iget-object v2, p0, Lcom/pspdfkit/internal/sa;->a:Lcom/pspdfkit/internal/zf;

    if-eqz v2, :cond_4

    goto :goto_1

    .line 14
    :cond_4
    iget-object v2, p0, Lcom/pspdfkit/internal/sa;->b:Lcom/pspdfkit/annotations/FileAnnotation;

    if-eqz v2, :cond_5

    .line 15
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v2

    goto :goto_1

    :cond_5
    move-object v2, v1

    :goto_1
    if-eqz v2, :cond_6

    .line 16
    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v2

    goto :goto_2

    :cond_6
    move-object v2, v1

    .line 17
    :goto_2
    iget-object v3, p0, Lcom/pspdfkit/internal/sa;->b:Lcom/pspdfkit/annotations/FileAnnotation;

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v1

    .line 18
    :cond_7
    iget-object v3, p0, Lcom/pspdfkit/internal/sa;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->getFileInformation(Lcom/pspdfkit/internal/jni/NativeDocument;Lcom/pspdfkit/internal/jni/NativeAnnotation;Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeFileResourceInformation;

    move-result-object v0

    if-nez v0, :cond_8

    .line 19
    monitor-exit p0

    return-void

    .line 22
    :cond_8
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFileResourceInformation;->getRawSize()Ljava/lang/Long;

    move-result-object v1

    if-nez v1, :cond_9

    const-wide/16 v1, -0x1

    goto :goto_3

    :cond_9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFileResourceInformation;->getRawSize()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 24
    :goto_3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFileResourceInformation;->getFileSize()Ljava/lang/Long;

    move-result-object v3

    if-nez v3, :cond_a

    goto :goto_4

    :cond_a
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFileResourceInformation;->getFileSize()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    :goto_4
    iput-wide v1, p0, Lcom/pspdfkit/internal/sa;->f:J

    .line 25
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFileResourceInformation;->getFileName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/sa;->e:Ljava/lang/String;

    .line 26
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFileResourceInformation;->getFileDescription()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/sa;->g:Ljava/lang/String;

    .line 27
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFileResourceInformation;->getModificationDate()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/sa;->h:Ljava/util/Date;

    const/4 v0, 0x1

    .line 28
    iput-boolean v0, p0, Lcom/pspdfkit/internal/sa;->d:Z

    .line 29
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final getAnnotation()Lcom/pspdfkit/annotations/FileAnnotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sa;->b:Lcom/pspdfkit/annotations/FileAnnotation;

    return-object v0
.end method

.method public final getFileData()[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    .line 2
    iget-wide v1, p0, Lcom/pspdfkit/internal/sa;->f:J

    const-wide/16 v3, -0x1

    cmp-long v5, v1, v3

    if-eqz v5, :cond_0

    long-to-int v2, v1

    goto :goto_0

    :cond_0
    const/16 v2, 0x400

    :goto_0
    invoke-direct {v0, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/sa;->writeToStream(Ljava/io/OutputStream;)V

    .line 4
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public final getFileDescription()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sa;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final getFileName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sa;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method public final getFileSize()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/sa;->f:J

    return-wide v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sa;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final getModificationDate()Ljava/util/Date;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sa;->h:Ljava/util/Date;

    return-object v0
.end method

.method public final writeToStream(Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "outputStream"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/sa;->a:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/sa;->b:Lcom/pspdfkit/annotations/FileAnnotation;

    if-eqz v0, :cond_1

    .line 56
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_7

    .line 57
    new-instance v2, Lcom/pspdfkit/internal/sl;

    invoke-direct {v2, p1}, Lcom/pspdfkit/internal/sl;-><init>(Ljava/io/OutputStream;)V

    .line 59
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/r1;->d()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object p1

    .line 60
    iget-object v0, p0, Lcom/pspdfkit/internal/sa;->a:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_2

    goto :goto_1

    .line 62
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/sa;->b:Lcom/pspdfkit/annotations/FileAnnotation;

    if-eqz v0, :cond_3

    .line 63
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 64
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v0

    goto :goto_2

    :cond_4
    move-object v0, v1

    .line 65
    :goto_2
    iget-object v3, p0, Lcom/pspdfkit/internal/sa;->b:Lcom/pspdfkit/annotations/FileAnnotation;

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v1

    .line 66
    :cond_5
    iget-object v3, p0, Lcom/pspdfkit/internal/sa;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1, v3, v2}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->getResource(Lcom/pspdfkit/internal/jni/NativeDocument;Lcom/pspdfkit/internal/jni/NativeAnnotation;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeDataSink;)Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object p1

    .line 67
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeResult;->getHasError()Z

    move-result v0

    if-nez v0, :cond_6

    return-void

    .line 68
    :cond_6
    new-instance v0, Ljava/io/IOException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeResult;->getErrorString()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "Couldn\'t retrieve embedded file: %s"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Document must not be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final writeToStreamAsync(Ljava/io/OutputStream;)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    const-string v0, "outputStream"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/sa;->a:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_0

    move-object v1, v0

    goto :goto_0

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/sa;->b:Lcom/pspdfkit/annotations/FileAnnotation;

    if-eqz v0, :cond_1

    .line 56
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v1

    :cond_1
    :goto_0
    if-nez v1, :cond_2

    .line 57
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Document must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Completable;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1

    .line 60
    :cond_2
    new-instance v0, Lcom/pspdfkit/internal/sa$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/sa$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/sa;Ljava/io/OutputStream;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    const/16 v0, 0xa

    .line 61
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

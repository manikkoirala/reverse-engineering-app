.class public abstract Lcom/pspdfkit/internal/v0;
.super Landroidx/fragment/app/DialogFragment;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/v0$a;
    }
.end annotation


# instance fields
.field private b:Lcom/pspdfkit/internal/zf;

.field private c:Lcom/pspdfkit/internal/nm;

.field private d:Lcom/pspdfkit/internal/v0$a;

.field protected e:Lcom/pspdfkit/ui/PdfFragment;

.field protected f:Lcom/pspdfkit/internal/fl;

.field private g:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private h:Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

.field private i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;


# direct methods
.method public static synthetic $r8$lambda$jANFXwUwU0efthiwGYrAiCnnH4M(Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/v0;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$nUoiapXYcu_Pqw6NnmXLiDOY-TQ(Lcom/pspdfkit/internal/v0;Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/v0;->a(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z

    move-result p0

    return p0
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/DialogFragment;-><init>()V

    return-void
.end method

.method private static synthetic a(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.AnnotationEditor"

    const-string v2, "Could not restore annotation from instance state."

    .line 12
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private synthetic a(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x4

    if-ne p2, p1, :cond_0

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismiss()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private f()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/v0;->c:Lcom/pspdfkit/internal/nm;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/internal/v0;->b:Lcom/pspdfkit/internal/zf;

    if-nez v1, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/nm;->a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 5
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/v0$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/v0$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/v0;)V

    new-instance v2, Lcom/pspdfkit/internal/v0$$ExternalSyntheticLambda2;

    invoke-direct {v2}, Lcom/pspdfkit/internal/v0$$ExternalSyntheticLambda2;-><init>()V

    .line 6
    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method protected final a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/v0;->h:Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    return-object v0
.end method

.method public final a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/zf;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/v0;->c:Lcom/pspdfkit/internal/nm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nm;->a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/v0;->c:Lcom/pspdfkit/internal/nm;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nm;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 7
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/nm;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/nm;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/v0;->c:Lcom/pspdfkit/internal/nm;

    .line 8
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/v0;->b(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/v0$a;)V
    .locals 0

    .line 11
    iput-object p1, p0, Lcom/pspdfkit/internal/v0;->d:Lcom/pspdfkit/internal/v0$a;

    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;)V
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/v0;->b(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/v0;->b:Lcom/pspdfkit/internal/zf;

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/v0;->b:Lcom/pspdfkit/internal/zf;

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/v0;->f()V

    return-void
.end method

.method protected final b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/v0;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    return-object v0
.end method

.method protected b(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public final b(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/v0;->e:Lcom/pspdfkit/ui/PdfFragment;

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/v0;->f:Lcom/pspdfkit/internal/fl;

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationConfiguration()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/v0;->h:Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/v0;->i:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/v0;->g:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/zf;

    iput-object p2, p0, Lcom/pspdfkit/internal/v0;->b:Lcom/pspdfkit/internal/zf;

    .line 9
    :cond_0
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    return-void
.end method

.method protected final c()Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/v0;->g:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-object v0
.end method

.method protected final d()Lcom/pspdfkit/internal/zf;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/v0;->b:Lcom/pspdfkit/internal/zf;

    return-object v0
.end method

.method protected final e()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 3
    invoke-virtual {v1}, Landroid/app/Dialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "input_method"

    .line 4
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 5
    invoke-virtual {v1}, Landroid/app/Dialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method public final onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public final onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public final onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public final onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const/16 v1, 0x21c

    .line 2
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_Dialog_Light:I

    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/DialogFragment;->setStyle(II)V

    goto :goto_0

    .line 6
    :cond_0
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_Dialog_Light_Panel_FullScreen:I

    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/DialogFragment;->setStyle(II)V

    .line 9
    :goto_0
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object p1

    .line 12
    new-instance v0, Lcom/pspdfkit/internal/v0$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/v0$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/v0;)V

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    return-object p1
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/v0;->d:Lcom/pspdfkit/internal/v0$a;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, p0, v0}, Lcom/pspdfkit/internal/v0$a;->a(Lcom/pspdfkit/internal/v0;Z)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/v0;->c:Lcom/pspdfkit/internal/nm;

    const-string v1, "annotation"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public onStart()V
    .locals 9

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 4
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 7
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    const/16 v2, 0x21c

    .line 8
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 9
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 10
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 13
    iget v2, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 14
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    const-wide v3, 0x3feb333333333333L    # 0.85

    int-to-double v5, v1

    if-le v2, v1, :cond_0

    mul-double v5, v5, v3

    double-to-int v1, v5

    int-to-double v2, v2

    const-wide v4, 0x3fe6666666666666L    # 0.7

    mul-double v2, v2, v4

    double-to-int v2, v2

    goto :goto_0

    :cond_0
    const-wide/high16 v7, 0x3fe0000000000000L    # 0.5

    mul-double v5, v5, v7

    double-to-int v1, v5

    int-to-double v5, v2

    mul-double v5, v5, v3

    double-to-int v2, v5

    .line 22
    :goto_0
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 26
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/v0;->e:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_2

    .line 27
    invoke-virtual {v0, p0}, Lcom/pspdfkit/ui/PdfFragment;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    :cond_2
    return-void
.end method

.method public onStop()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStop()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/v0;->e:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    :cond_0
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    if-eqz p2, :cond_0

    const-string p1, "annotation"

    .line 4
    invoke-virtual {p2, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/nm;

    iput-object p1, p0, Lcom/pspdfkit/internal/v0;->c:Lcom/pspdfkit/internal/nm;

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/v0;->f()V

    :cond_0
    return-void
.end method

.class public final Lcom/pspdfkit/internal/vt;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x1d

    new-array v0, v0, [F

    .line 1
    fill-array-data v0, :array_0

    .line 4
    sput-object v0, Lcom/pspdfkit/internal/vt;->a:[F

    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
        0x40400000    # 3.0f
        0x40800000    # 4.0f
        0x40c00000    # 6.0f
        0x41000000    # 8.0f
        0x41100000    # 9.0f
        0x41200000    # 10.0f
        0x41400000    # 12.0f
        0x41600000    # 14.0f
        0x41800000    # 16.0f
        0x41900000    # 18.0f
        0x41a00000    # 20.0f
        0x41c80000    # 25.0f
        0x41f00000    # 30.0f
        0x420c0000    # 35.0f
        0x42200000    # 40.0f
        0x42340000    # 45.0f
        0x42480000    # 50.0f
        0x425c0000    # 55.0f
        0x42700000    # 60.0f
        0x428c0000    # 70.0f
        0x42a00000    # 80.0f
        0x42b40000    # 90.0f
        0x42c80000    # 100.0f
        0x42dc0000    # 110.0f
        0x42f00000    # 120.0f
        0x43020000    # 130.0f
        0x43100000    # 144.0f
    .end array-data
.end method

.method public static final a(Ljava/lang/String;Landroid/graphics/Paint;FFZZ)F
    .locals 9

    const-string v0, "text"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v8, Lcom/pspdfkit/internal/vt;->a:[F

    const/4 v7, 0x1

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    .line 2
    invoke-static/range {v1 .. v8}, Lcom/pspdfkit/internal/vt;->a(Ljava/lang/String;Landroid/graphics/Paint;FFZZZ[F)F

    move-result p0

    return p0
.end method

.method public static final a(Ljava/lang/String;Landroid/graphics/Paint;FFZZZ[F)F
    .locals 21

    move-object/from16 v10, p0

    move-object/from16 v0, p1

    move/from16 v11, p2

    move-object/from16 v12, p7

    const-string v1, "text"

    invoke-static {v10, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "paint"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "fontSizes"

    invoke-static {v12, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    cmpg-float v2, v11, v1

    if-lez v2, :cond_b

    cmpg-float v2, p3, v1

    if-gtz v2, :cond_0

    goto/16 :goto_9

    :cond_0
    if-eqz p4, :cond_1

    if-eqz p6, :cond_1

    .line 3
    array-length v1, v12

    div-int/lit8 v1, v1, 0x4

    goto :goto_0

    .line 5
    :cond_1
    array-length v1, v12

    :goto_0
    add-int/lit8 v2, v1, -0x1

    .line 14
    div-int/lit8 v1, v1, 0x2

    .line 18
    new-instance v13, Landroid/text/TextPaint;

    invoke-direct {v13, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    const/4 v14, 0x0

    move v15, v1

    move v9, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_1
    if-gt v8, v9, :cond_9

    .line 25
    aget v0, v12, v15

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 26
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_3

    .line 27
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v0

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1, v13}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    float-to-int v2, v11

    invoke-static {v10, v14, v0, v1, v2}, Landroid/text/StaticLayout$Builder;->obtain(Ljava/lang/CharSequence;IILandroid/text/TextPaint;I)Landroid/text/StaticLayout$Builder;

    move-result-object v0

    const-string v1, "obtain(text, 0, text.len\u2026Paint), maxWidth.toInt())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {v0}, Landroid/text/StaticLayout$Builder;->build()Landroid/text/StaticLayout;

    move-result-object v0

    move/from16 v20, v7

    move/from16 v17, v8

    move/from16 v18, v9

    goto :goto_3

    .line 32
    :cond_3
    new-instance v16, Landroid/text/StaticLayout;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v3

    new-instance v4, Landroid/text/TextPaint;

    invoke-direct {v4, v13}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    float-to-int v5, v11

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/4 v2, 0x0

    const/high16 v17, 0x3f800000    # 1.0f

    const/high16 v18, 0x3f800000    # 1.0f

    const/16 v19, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move/from16 v20, v7

    move/from16 v7, v17

    move/from16 v17, v8

    move/from16 v8, v18

    move/from16 v18, v9

    move/from16 v9, v19

    invoke-direct/range {v0 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    :goto_3
    const-string v1, "if (AndroidVersion.isAtL\u2026_NORMAL, 1f, 1f, false)\n}"

    .line 33
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p4, :cond_5

    .line 34
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v1

    invoke-static {v14, v1}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v1

    .line 85
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 86
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    move-object v3, v1

    check-cast v3, Lkotlin/collections/IntIterator;

    invoke-virtual {v3}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v3

    .line 87
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 140
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 141
    :cond_4
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->maxOrNull(Ljava/lang/Iterable;)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {v11, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    goto :goto_5

    .line 145
    :cond_5
    invoke-static {v10, v13}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v1

    :goto_5
    move v9, v1

    .line 148
    invoke-static {v10, v13}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;)Landroid/text/BoringLayout$Metrics;

    move-result-object v7

    if-nez p4, :cond_6

    if-eqz v7, :cond_6

    .line 153
    new-instance v16, Landroid/text/BoringLayout;

    float-to-int v3, v11

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object v2, v13

    invoke-direct/range {v0 .. v8}, Landroid/text/BoringLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)V

    invoke-virtual/range {v16 .. v16}, Landroid/text/BoringLayout;->getHeight()I

    move-result v0

    goto :goto_6

    .line 157
    :cond_6
    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    move-result v0

    :goto_6
    cmpl-float v1, v9, v11

    if-gtz v1, :cond_8

    int-to-float v0, v0

    cmpl-float v0, v0, p3

    if-lez v0, :cond_7

    goto :goto_7

    :cond_7
    add-int/lit8 v8, v15, 0x1

    add-int v9, v8, v18

    .line 167
    div-int/lit8 v1, v9, 0x2

    move v7, v15

    move/from16 v9, v18

    move v15, v1

    goto/16 :goto_1

    :cond_8
    :goto_7
    add-int/lit8 v9, v15, -0x1

    add-int v8, v17, v9

    .line 168
    div-int/lit8 v15, v8, 0x2

    move/from16 v8, v17

    move/from16 v7, v20

    goto/16 :goto_1

    :cond_9
    move/from16 v20, v7

    if-eqz p5, :cond_a

    move/from16 v14, v20

    .line 179
    aget v0, v12, v14

    goto :goto_8

    :cond_a
    move/from16 v14, v20

    const/4 v0, 0x3

    .line 181
    invoke-static {v14, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    aget v0, v12, v0

    :goto_8
    return v0

    :cond_b
    :goto_9
    return v1
.end method

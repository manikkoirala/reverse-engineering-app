.class final Lcom/pspdfkit/internal/zu$g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/zu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/zu;


# direct methods
.method public static synthetic $r8$lambda$RoqEWE-gnLoSrDwHyGb_xv6AaZM(Lcom/pspdfkit/internal/zu$g;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/zu$g;->a()V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/zu;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zu$g;->a:Lcom/pspdfkit/internal/zu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synthetic a()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zu$g;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetx(Lcom/pspdfkit/internal/zu;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zu;->b(I)V

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zu$g;->a:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->h()V

    return-void
.end method


# virtual methods
.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$g;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {p1, p3}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputn(Lcom/pspdfkit/internal/zu;I)V

    .line 2
    invoke-static {p1, p4}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputo(Lcom/pspdfkit/internal/zu;I)V

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetd(Lcom/pspdfkit/internal/zu;)I

    move-result p2

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x3

    if-ne p2, v2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 4
    :goto_0
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetk(Lcom/pspdfkit/internal/zu;)I

    move-result v2

    if-ne v2, p3, :cond_1

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetl(Lcom/pspdfkit/internal/zu;)I

    move-result p3

    if-ne p3, p4, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 5
    :goto_1
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer;

    move-result-object p3

    if-eqz p3, :cond_2

    if-eqz p2, :cond_2

    if-eqz v0, :cond_2

    .line 8
    new-instance p2, Lcom/pspdfkit/internal/zu$g$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/zu$g$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/zu$g;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zu$g;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fpute(Lcom/pspdfkit/internal/zu;Landroid/view/SurfaceHolder;)V

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$me(Lcom/pspdfkit/internal/zu;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$g;->a:Lcom/pspdfkit/internal/zu;

    .line 4
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetC(Lcom/pspdfkit/internal/zu;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetj(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/ll;

    move-result-object v0

    if-nez v0, :cond_0

    .line 6
    new-instance v0, Lcom/pspdfkit/internal/ll;

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgeth(Lcom/pspdfkit/internal/zu;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/ll;-><init>(Landroid/content/Context;)V

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputj(Lcom/pspdfkit/internal/zu;Lcom/pspdfkit/internal/ll;)V

    .line 7
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ll;->a(Lcom/pspdfkit/internal/zu;)V

    .line 8
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetj(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/ll;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ll;->b()V

    :cond_0
    return-void
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$g;->a:Lcom/pspdfkit/internal/zu;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fpute(Lcom/pspdfkit/internal/zu;Landroid/view/SurfaceHolder;)V

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgeti(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/yu;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/internal/yu;->a()V

    .line 3
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$g;->a:Lcom/pspdfkit/internal/zu;

    .line 4
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 5
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    .line 6
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 7
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputf(Lcom/pspdfkit/internal/zu;Landroid/media/MediaPlayer;)V

    const/4 v0, 0x0

    .line 8
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputc(Lcom/pspdfkit/internal/zu;I)V

    .line 10
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputd(Lcom/pspdfkit/internal/zu;I)V

    .line 11
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$g;->a:Lcom/pspdfkit/internal/zu;

    .line 12
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetj(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/ll;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ll;->a()V

    :cond_2
    return-void
.end method

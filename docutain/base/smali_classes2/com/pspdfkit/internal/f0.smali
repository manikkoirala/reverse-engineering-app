.class public abstract Lcom/pspdfkit/internal/f0;
.super Lcom/pspdfkit/internal/e0;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration$Builder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/f0$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$Builder<",
        "TT;>;>",
        "Lcom/pspdfkit/internal/e0<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration$Builder<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;


# direct methods
.method public varargs constructor <init>(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;[Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationTool"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "supportedProperties"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    array-length v0, p3

    invoke-static {p3, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p3

    check-cast p3, [Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-direct {p0, p3}, Lcom/pspdfkit/internal/e0;-><init>([Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/f0;->c:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/f0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-void
.end method


# virtual methods
.method protected final c()V
    .locals 9

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->b()Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const-string v2, "defaultVariant()"

    const-string v3, "context"

    const-string v4, "annotationTool"

    if-eqz v1, :cond_16

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    if-nez v1, :cond_1

    const/4 v1, -0x1

    goto :goto_1

    .line 2
    :cond_1
    sget-object v5, Lcom/pspdfkit/internal/f0$a;->a:[I

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v5, v1

    :goto_1
    const/4 v5, 0x1

    if-eq v1, v5, :cond_11

    const/4 v6, 0x2

    const/4 v7, 0x0

    if-eq v1, v6, :cond_c

    const/4 v6, 0x3

    if-eq v1, v6, :cond_a

    const/4 v2, 0x4

    if-eq v1, v2, :cond_3

    const/4 v2, 0x5

    if-eq v1, v2, :cond_2

    goto :goto_0

    .line 3
    :cond_2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/g0;->k:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    const/high16 v3, 0x40a00000    # 5.0f

    .line 5
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 6
    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    goto :goto_0

    .line 7
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/internal/f0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->toAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v1, v2, :cond_4

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    :goto_2
    if-eqz v5, :cond_5

    .line 9
    sget-object v1, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->NONE:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    goto :goto_3

    .line 11
    :cond_5
    sget-object v1, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->SOLID:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    :goto_3
    const-string v2, "if (isFreeTextAnnotation\u2026ylePreset.SOLID\n        }"

    .line 12
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/internal/g0;->v:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    if-nez v2, :cond_6

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v2

    invoke-virtual {v2, v3, v1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    .line 23
    :cond_6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/g0;->w:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_0

    .line 28
    new-instance v1, Ljava/util/ArrayList;

    const/4 v3, 0x6

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    if-eqz v5, :cond_7

    .line 31
    sget-object v3, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->NONE:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    sget-object v3, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->SOLID:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 36
    :cond_7
    sget-object v3, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->SOLID:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    :goto_4
    sget-object v3, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->DASHED_1_1:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    sget-object v3, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->DASHED_1_3:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    sget-object v3, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->DASHED_3_3:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    sget-object v3, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->DASHED_6_6:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v3, p0, Lcom/pspdfkit/internal/f0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-virtual {v3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->toAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    const-string v4, "annotationTool.toAnnotationType()"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->SQUARE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v3, v4, :cond_8

    .line 45
    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->CIRCLE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v3, v4, :cond_8

    .line 46
    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->POLYGON:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v3, v4, :cond_9

    .line 48
    :cond_8
    sget-object v3, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->CLOUDY:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_9
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 53
    :cond_a
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    sget-object v5, Lcom/pspdfkit/internal/g0;->h:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v1, v5}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_b

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    .line 55
    iget-object v6, p0, Lcom/pspdfkit/internal/f0;->c:Landroid/content/Context;

    iget-object v7, p0, Lcom/pspdfkit/internal/f0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v8, Lcom/pspdfkit/internal/ao;->a:I

    .line 56
    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v7, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 508
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 509
    invoke-static {v6, v7, v3}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result v2

    .line 510
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 511
    invoke-virtual {v1, v5, v2}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    .line 516
    :cond_b
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/g0;->i:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    .line 518
    sget-object v3, Lcom/pspdfkit/internal/ao;->c:Ljava/util/List;

    .line 519
    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 520
    :cond_c
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/g0;->f:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_e

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    .line 522
    iget-object v3, p0, Lcom/pspdfkit/internal/f0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v5, Lcom/pspdfkit/internal/ao;->a:I

    .line 523
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1024
    sget-object v4, Lcom/pspdfkit/internal/ao$a;->b:[I

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget v3, v4, v3

    const/16 v4, 0x15

    if-ne v3, v4, :cond_d

    const/high16 v3, -0x1000000

    goto :goto_5

    :cond_d
    const/4 v3, 0x0

    .line 1025
    :goto_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 1026
    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    .line 1031
    :cond_e
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/g0;->g:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_f

    invoke-static {}, Lcom/pspdfkit/internal/ao;->a()Ljava/util/List;

    move-result-object v1

    .line 1034
    :cond_f
    iget-object v3, p0, Lcom/pspdfkit/internal/f0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v4, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->REDACTION:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-ne v3, v4, :cond_10

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 1035
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    .line 1036
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1037
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1039
    :cond_10
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1040
    :cond_11
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    sget-object v5, Lcom/pspdfkit/internal/g0;->d:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v1, v5}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_12

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    .line 1042
    iget-object v6, p0, Lcom/pspdfkit/internal/f0;->c:Landroid/content/Context;

    iget-object v7, p0, Lcom/pspdfkit/internal/f0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v8, Lcom/pspdfkit/internal/ao;->a:I

    .line 1043
    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v7, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1495
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1496
    invoke-static {v6, v7, v3}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result v2

    .line 1497
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1498
    invoke-virtual {v1, v5, v2}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    .line 1503
    :cond_12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/g0;->e:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_0

    .line 1504
    iget-object v1, p0, Lcom/pspdfkit/internal/f0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->HIGHLIGHT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq v1, v3, :cond_15

    .line 1505
    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SQUIGGLY:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq v1, v3, :cond_15

    .line 1506
    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->STRIKEOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq v1, v3, :cond_15

    .line 1507
    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->UNDERLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-ne v1, v3, :cond_13

    goto :goto_6

    .line 1510
    :cond_13
    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-ne v1, v3, :cond_14

    .line 1511
    sget-object v1, Lcom/pspdfkit/internal/ao;->f:Ljava/util/List;

    goto :goto_7

    .line 1513
    :cond_14
    sget-object v1, Lcom/pspdfkit/internal/ao;->c:Ljava/util/List;

    goto :goto_7

    .line 1514
    :cond_15
    :goto_6
    invoke-static {}, Lcom/pspdfkit/internal/ao;->b()Ljava/util/List;

    move-result-object v1

    .line 1520
    :goto_7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1521
    :cond_16
    iget-object v0, p0, Lcom/pspdfkit/internal/f0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->toAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_17

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->b()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 1522
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->d:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_17

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    .line 1524
    iget-object v5, p0, Lcom/pspdfkit/internal/f0;->c:Landroid/content/Context;

    iget-object v6, p0, Lcom/pspdfkit/internal/f0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget v7, Lcom/pspdfkit/internal/ao;->a:I

    .line 1525
    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1977
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1978
    invoke-static {v5, v6, v3}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result v2

    .line 1979
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1980
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    :cond_17
    return-void
.end method

.method public final setAvailableColors(Ljava/util/List;)Ljava/lang/Object;
    .locals 2

    const-string v0, "availableColors"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->e:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 50
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setAvailableFillColors(Ljava/util/List;)Ljava/lang/Object;
    .locals 2

    const-string v0, "availableColors"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->g:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 60
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setAvailableOutlineColors(Ljava/util/List;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)TT;"
        }
    .end annotation

    const-string v0, "availableColors"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->i:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setBorderStylePresets(Ljava/util/List;)Ljava/lang/Object;
    .locals 2

    const-string v0, "borderStylePresets"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->w:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 85
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setCustomColorPickerEnabled(Z)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TT;"
        }
    .end annotation

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->j:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final bridge synthetic setCustomColorPickerEnabled(Z)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/f0;->setCustomColorPickerEnabled(Z)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final setDefaultBorderStylePreset(Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)Ljava/lang/Object;
    .locals 2

    const-string v0, "borderStylePreset"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->v:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 80
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setDefaultColor(I)Ljava/lang/Object;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->d:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setDefaultFillColor(I)Ljava/lang/Object;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->f:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setDefaultOutlineColor(I)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->h:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.class final Lcom/pspdfkit/internal/l9;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/pspdfkit/internal/jni/NativeJSResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/n9;

.field final synthetic b:Lcom/pspdfkit/annotations/LinkAnnotation;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/n9;Lcom/pspdfkit/annotations/LinkAnnotation;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/l9;->a:Lcom/pspdfkit/internal/n9;

    iput-object p2, p0, Lcom/pspdfkit/internal/l9;->b:Lcom/pspdfkit/annotations/LinkAnnotation;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/l9;->a:Lcom/pspdfkit/internal/n9;

    invoke-static {v0}, Lcom/pspdfkit/internal/n9;->b(Lcom/pspdfkit/internal/n9;)Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/l9;->b:Lcom/pspdfkit/annotations/LinkAnnotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/l9;->b:Lcom/pspdfkit/annotations/LinkAnnotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v2

    int-to-long v2, v2

    new-instance v4, Lcom/pspdfkit/internal/jni/NativeJSEventSourceTargetInfo;

    iget-object v5, p0, Lcom/pspdfkit/internal/l9;->a:Lcom/pspdfkit/internal/n9;

    invoke-static {v5}, Lcom/pspdfkit/internal/n9;->a(Lcom/pspdfkit/internal/n9;)Lcom/pspdfkit/internal/jni/NativeDocumentProvider;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/pspdfkit/internal/jni/NativeJSEventSourceTargetInfo;-><init>(Lcom/pspdfkit/internal/jni/NativeDocumentProvider;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;->onLinkMouseUp(IJLcom/pspdfkit/internal/jni/NativeJSEventSourceTargetInfo;)Lcom/pspdfkit/internal/jni/NativeJSResult;

    move-result-object v0

    const-string v1, "nativeScriptExecutor.onL\u2026(documentProvider, null))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

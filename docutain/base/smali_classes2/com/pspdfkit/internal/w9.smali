.class public final Lcom/pspdfkit/internal/w9;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/w9$c;,
        Lcom/pspdfkit/internal/w9$b;,
        Lcom/pspdfkit/internal/w9$a;
    }
.end annotation


# static fields
.field public static final r:[I

.field public static final s:I

.field public static final t:I


# instance fields
.field private final b:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/w9$a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I

.field private final e:I

.field private f:Lcom/pspdfkit/internal/w9$b;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/EditText;

.field private i:Landroid/widget/Spinner;

.field private j:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter<",
            "Lcom/pspdfkit/internal/w9$c;",
            ">;"
        }
    .end annotation
.end field

.field private k:Landroid/widget/EditText;

.field private l:Lcom/pspdfkit/internal/w9$c;

.field private m:Landroid/widget/Spinner;

.field private n:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter<",
            "Lcom/pspdfkit/internal/w9$a;",
            ">;"
        }
    .end annotation
.end field

.field private o:Landroid/widget/TextView;

.field private p:I

.field private q:I


# direct methods
.method public static synthetic $r8$lambda$I8l-JmPKwp1aI3H4Tc1SAYgYjj4(Lcom/pspdfkit/internal/w9;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/w9;->a(Landroid/view/View;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/w9;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/w9;->e:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/w9;->h:Landroid/widget/EditText;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeti(Lcom/pspdfkit/internal/w9;)Landroid/widget/Spinner;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/w9;->i:Landroid/widget/Spinner;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetj(Lcom/pspdfkit/internal/w9;)Landroid/widget/ArrayAdapter;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/w9;->j:Landroid/widget/ArrayAdapter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetk(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/w9;->k:Landroid/widget/EditText;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetl(Lcom/pspdfkit/internal/w9;)Lcom/pspdfkit/internal/w9$c;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/w9;->l:Lcom/pspdfkit/internal/w9$c;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetn(Lcom/pspdfkit/internal/w9;)Landroid/widget/ArrayAdapter;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/w9;->n:Landroid/widget/ArrayAdapter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeto(Lcom/pspdfkit/internal/w9;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/w9;->o:Landroid/widget/TextView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetp(Lcom/pspdfkit/internal/w9;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/w9;->p:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetq(Lcom/pspdfkit/internal/w9;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/w9;->q:I

    return p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__SharingDialog:[I

    sput-object v0, Lcom/pspdfkit/internal/w9;->r:[I

    .line 2
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__sharingDialogStyle:I

    sput v0, Lcom/pspdfkit/internal/w9;->s:I

    .line 3
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_SharingDialog:I

    sput v0, Lcom/pspdfkit/internal/w9;->t:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;Ljava/util/ArrayList;)V
    .locals 2

    .line 1
    new-instance v0, Landroidx/appcompat/view/ContextThemeWrapper;

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->a(Landroid/content/Context;)I

    move-result v1

    invoke-direct {v0, p1, v1}, Landroidx/appcompat/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {p0, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/w9;->b:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;

    .line 3
    invoke-virtual {p2}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;->getCurrentPage()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/w9;->d:I

    .line 4
    invoke-virtual {p2}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;->getDocumentPages()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/w9;->e:I

    .line 5
    iput-object p3, p0, Lcom/pspdfkit/internal/w9;->c:Ljava/util/List;

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/internal/w9;->a()V

    return-void
.end method

.method private static a(Landroid/content/Context;)I
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/internal/w9;->s:I

    sget v1, Lcom/pspdfkit/internal/w9;->t:I

    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/cu;->b(Landroid/content/Context;II)I

    move-result p0

    return p0
.end method

.method private a()V
    .locals 13

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__share_dialog:I

    const/4 v6, 0x1

    invoke-virtual {v0, v1, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/w9;->g:Landroid/view/View;

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/utils/b;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/b;-><init>(Landroid/content/Context;)V

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/w9;->r:[I

    sget v3, Lcom/pspdfkit/internal/w9;->s:I

    sget v4, Lcom/pspdfkit/internal/w9;->t:I

    const/4 v5, 0x0

    invoke-virtual {v1, v5, v2, v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 8
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SharingDialog_pspdf__backgroundColor:I

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;)I

    move-result v3

    .line 11
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 14
    sget v3, Lcom/pspdfkit/R$styleable;->pspdf__SharingDialog_pspdf__errorColor:I

    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/pspdfkit/R$color;->pspdf__color_error:I

    invoke-static {v4, v5}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    .line 17
    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iput v3, p0, Lcom/pspdfkit/internal/w9;->q:I

    .line 20
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 23
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v3, Landroidx/appcompat/R$attr;->colorAccent:I

    sget v4, Lcom/pspdfkit/R$color;->pspdf__color_dark:I

    .line 24
    invoke-static {v1, v3, v4}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/w9;->p:I

    .line 28
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3, v0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ui/dialog/utils/b;)V

    .line 29
    iget-object v3, p0, Lcom/pspdfkit/internal/w9;->b:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;

    invoke-virtual {v3}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;->getDialogTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(Ljava/lang/String;)V

    .line 30
    iget-object v3, p0, Lcom/pspdfkit/internal/w9;->g:Landroid/view/View;

    sget v4, Lcom/pspdfkit/R$id;->pspdf__dialog_root:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    const/4 v7, 0x0

    invoke-virtual {v3, v1, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 33
    iget-object v3, p0, Lcom/pspdfkit/internal/w9;->g:Landroid/view/View;

    .line 34
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/utils/b;->getCornerRadius()I

    move-result v0

    .line 35
    invoke-static {v3, v1, v2, v0, v7}, Lcom/pspdfkit/internal/ui/dialog/utils/b;->setRoundedBackground(Landroid/view/View;Lcom/pspdfkit/internal/ui/dialog/utils/a;IIZ)V

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/internal/w9;->g:Landroid/view/View;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__share_dialog_document_name:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/pspdfkit/internal/w9;->h:Landroid/widget/EditText;

    .line 38
    iget-object v1, p0, Lcom/pspdfkit/internal/w9;->b:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;->getInitialDocumentName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/kb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/internal/w9;->h:Landroid/widget/EditText;

    iget v1, p0, Lcom/pspdfkit/internal/w9;->p:I

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/widget/EditText;I)V

    .line 40
    iget-object v0, p0, Lcom/pspdfkit/internal/w9;->h:Landroid/widget/EditText;

    new-instance v1, Lcom/pspdfkit/internal/s9;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/s9;-><init>(Lcom/pspdfkit/internal/w9;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 47
    iget-object v0, p0, Lcom/pspdfkit/internal/w9;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 48
    iget-object v0, p0, Lcom/pspdfkit/internal/w9;->g:Landroid/view/View;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__share_dialog_pages_spinner:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/pspdfkit/internal/w9;->i:Landroid/widget/Spinner;

    .line 49
    new-instance v8, Lcom/pspdfkit/internal/w9$c;

    iget v4, p0, Lcom/pspdfkit/internal/w9;->e:I

    new-instance v5, Lcom/pspdfkit/datastructures/Range;

    iget v0, p0, Lcom/pspdfkit/internal/w9;->e:I

    invoke-direct {v5, v7, v0}, Lcom/pspdfkit/datastructures/Range;-><init>(II)V

    const/4 v2, 0x2

    const/4 v3, 0x0

    move-object v0, v8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/w9$c;-><init>(Lcom/pspdfkit/internal/w9;IIILcom/pspdfkit/datastructures/Range;)V

    iput-object v8, p0, Lcom/pspdfkit/internal/w9;->l:Lcom/pspdfkit/internal/w9$c;

    .line 51
    new-instance v8, Lcom/pspdfkit/internal/w9$c;

    iget v3, p0, Lcom/pspdfkit/internal/w9;->d:I

    iget v4, p0, Lcom/pspdfkit/internal/w9;->e:I

    new-instance v5, Lcom/pspdfkit/datastructures/Range;

    iget v0, p0, Lcom/pspdfkit/internal/w9;->e:I

    invoke-direct {v5, v7, v0}, Lcom/pspdfkit/datastructures/Range;-><init>(II)V

    const/4 v2, 0x3

    move-object v0, v8

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/w9$c;-><init>(Lcom/pspdfkit/internal/w9;IIILcom/pspdfkit/datastructures/Range;)V

    .line 54
    new-instance v9, Landroid/widget/ArrayAdapter;

    .line 55
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v10

    const/4 v0, 0x3

    new-array v11, v0, [Lcom/pspdfkit/internal/w9$c;

    new-instance v12, Lcom/pspdfkit/internal/w9$c;

    iget v3, p0, Lcom/pspdfkit/internal/w9;->d:I

    iget v4, p0, Lcom/pspdfkit/internal/w9;->e:I

    new-instance v5, Lcom/pspdfkit/datastructures/Range;

    iget v0, p0, Lcom/pspdfkit/internal/w9;->d:I

    invoke-direct {v5, v0, v6}, Lcom/pspdfkit/datastructures/Range;-><init>(II)V

    const/4 v2, 0x1

    move-object v0, v12

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/w9$c;-><init>(Lcom/pspdfkit/internal/w9;IIILcom/pspdfkit/datastructures/Range;)V

    aput-object v12, v11, v7

    iget-object v0, p0, Lcom/pspdfkit/internal/w9;->l:Lcom/pspdfkit/internal/w9$c;

    aput-object v0, v11, v6

    const/4 v0, 0x2

    aput-object v8, v11, v0

    const v1, 0x1090009

    invoke-direct {v9, v10, v1, v11}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v9, p0, Lcom/pspdfkit/internal/w9;->j:Landroid/widget/ArrayAdapter;

    .line 61
    iget-object v2, p0, Lcom/pspdfkit/internal/w9;->i:Landroid/widget/Spinner;

    invoke-virtual {v2, v9}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 64
    iget-object v2, p0, Lcom/pspdfkit/internal/w9;->g:Landroid/view/View;

    sget v3, Lcom/pspdfkit/R$id;->pspdf__share_dialog_pages_range:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/pspdfkit/internal/w9;->k:Landroid/widget/EditText;

    .line 65
    iget v3, p0, Lcom/pspdfkit/internal/w9;->p:I

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/ov;->a(Landroid/widget/EditText;I)V

    .line 66
    iget-object v2, p0, Lcom/pspdfkit/internal/w9;->k:Landroid/widget/EditText;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    iget v5, p0, Lcom/pspdfkit/internal/w9;->e:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const-string v5, "%d-%d"

    invoke-static {v3, v5, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v2, p0, Lcom/pspdfkit/internal/w9;->k:Landroid/widget/EditText;

    new-instance v3, Lcom/pspdfkit/internal/t9;

    invoke-direct {v3, p0}, Lcom/pspdfkit/internal/t9;-><init>(Lcom/pspdfkit/internal/w9;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 76
    iget-object v2, p0, Lcom/pspdfkit/internal/w9;->b:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;->isInitialPagesSpinnerAllPages()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77
    iget-object v2, p0, Lcom/pspdfkit/internal/w9;->j:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v8}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v2

    .line 78
    iget-object v3, p0, Lcom/pspdfkit/internal/w9;->i:Landroid/widget/Spinner;

    invoke-virtual {v3, v2}, Landroid/widget/AdapterView;->setSelection(I)V

    .line 81
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/w9;->i:Landroid/widget/Spinner;

    new-instance v3, Lcom/pspdfkit/internal/u9;

    invoke-direct {v3, p0}, Lcom/pspdfkit/internal/u9;-><init>(Lcom/pspdfkit/internal/w9;)V

    invoke-virtual {v2, v3}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 82
    iget-object v2, p0, Lcom/pspdfkit/internal/w9;->g:Landroid/view/View;

    sget v3, Lcom/pspdfkit/R$id;->pspdf__share_dialog_annotations_spinner:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/pspdfkit/internal/w9;->m:Landroid/widget/Spinner;

    .line 83
    new-instance v2, Landroid/widget/ArrayAdapter;

    .line 84
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p0}, Lcom/pspdfkit/internal/w9;->getAnnotationSpinnerItems()Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v1, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/w9;->n:Landroid/widget/ArrayAdapter;

    .line 85
    iget-object v1, p0, Lcom/pspdfkit/internal/w9;->m:Landroid/widget/Spinner;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 88
    iget-object v1, p0, Lcom/pspdfkit/internal/w9;->g:Landroid/view/View;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__share_dialog_annotations_description:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x0

    .line 91
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/internal/w9;->n:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 93
    iget-object v3, p0, Lcom/pspdfkit/internal/w9;->n:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/w9$a;

    iget v3, v3, Lcom/pspdfkit/internal/w9$a;->c:I

    if-gtz v3, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_3

    .line 99
    iget-object v2, p0, Lcom/pspdfkit/internal/w9;->m:Landroid/widget/Spinner;

    new-instance v3, Lcom/pspdfkit/internal/v9;

    invoke-direct {v3, p0, v1}, Lcom/pspdfkit/internal/v9;-><init>(Lcom/pspdfkit/internal/w9;Landroid/widget/TextView;)V

    invoke-virtual {v2, v3}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_2

    :cond_3
    const/16 v2, 0x8

    .line 118
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 119
    :goto_2
    iget-object v1, p0, Lcom/pspdfkit/internal/w9;->g:Landroid/view/View;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__positive_button:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/pspdfkit/internal/w9;->o:Landroid/widget/TextView;

    .line 120
    iget-object v2, p0, Lcom/pspdfkit/internal/w9;->b:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;->getPositiveButtonText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v1, p0, Lcom/pspdfkit/internal/w9;->o:Landroid/widget/TextView;

    new-instance v2, Lcom/pspdfkit/internal/w9$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/w9$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/w9;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v1, p0, Lcom/pspdfkit/internal/w9;->o:Landroid/widget/TextView;

    iget v2, p0, Lcom/pspdfkit/internal/w9;->p:I

    .line 127
    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 128
    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v3

    sget-object v4, Landroid/widget/FrameLayout;->EMPTY_STATE_SET:[I

    invoke-virtual {v3, v4, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v3

    goto :goto_3

    :cond_4
    move v3, v2

    .line 130
    :goto_3
    new-instance v4, Landroid/content/res/ColorStateList;

    new-array v5, v0, [[I

    new-array v8, v6, [I

    const v9, 0x101009e

    aput v9, v8, v7

    aput-object v8, v5, v7

    sget-object v8, Landroid/widget/FrameLayout;->EMPTY_STATE_SET:[I

    aput-object v8, v5, v6

    new-array v0, v0, [I

    aput v2, v0, v7

    aput v3, v0, v6

    invoke-direct {v4, v5, v0}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 133
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method private synthetic a(Landroid/view/View;)V
    .locals 0

    .line 134
    iget-object p1, p0, Lcom/pspdfkit/internal/w9;->f:Lcom/pspdfkit/internal/w9$b;

    if-eqz p1, :cond_0

    .line 135
    invoke-interface {p1, p0}, Lcom/pspdfkit/internal/w9$b;->a(Lcom/pspdfkit/internal/w9;)V

    :cond_0
    return-void
.end method

.method private getAnnotationProcessingMode()Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/w9;->n:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/pspdfkit/internal/w9;->m:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/w9$a;

    iget-object v0, v0, Lcom/pspdfkit/internal/w9$a;->a:Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;

    return-object v0
.end method

.method private getAnnotationSpinnerItems()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/w9$a;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/w9;->c:Ljava/util/List;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 16
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/w9;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/w9$a;

    .line 17
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 18
    :cond_1
    :goto_1
    new-instance v1, Lcom/pspdfkit/internal/w9$a;

    sget-object v2, Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;->KEEP:Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;

    sget v3, Lcom/pspdfkit/R$string;->pspdf__annotation_editing_embed:I

    sget v4, Lcom/pspdfkit/R$string;->pspdf__annotation_editing_embed_description:I

    invoke-direct {v1, v2, v3, v4}, Lcom/pspdfkit/internal/w9$a;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 22
    new-instance v1, Lcom/pspdfkit/internal/w9$a;

    sget-object v2, Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;->FLATTEN:Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;

    sget v3, Lcom/pspdfkit/R$string;->pspdf__annotation_editing_flatten:I

    sget v4, Lcom/pspdfkit/R$string;->pspdf__annotation_editing_flatten_description:I

    invoke-direct {v1, v2, v3, v4}, Lcom/pspdfkit/internal/w9$a;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    new-instance v1, Lcom/pspdfkit/internal/w9$a;

    sget-object v2, Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;->DELETE:Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;

    sget v3, Lcom/pspdfkit/R$string;->pspdf__annotation_editing_ignore:I

    sget v4, Lcom/pspdfkit/R$string;->pspdf__annotation_editing_ignore_description:I

    invoke-direct {v1, v2, v3, v4}, Lcom/pspdfkit/internal/w9$a;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/w9$a;

    .line 36
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 37
    invoke-static {v2, v3}, Lcom/pspdfkit/internal/w9$a;->-$$Nest$fputd(Lcom/pspdfkit/internal/w9$a;Landroid/content/Context;)V

    goto :goto_2

    :cond_3
    return-object v0
.end method


# virtual methods
.method public getSharingOptions()Lcom/pspdfkit/document/sharing/SharingOptions;
    .locals 4

    .line 1
    new-instance v0, Lcom/pspdfkit/document/sharing/SharingOptions;

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/w9;->getAnnotationProcessingMode()Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/w9;->j:Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lcom/pspdfkit/internal/w9;->i:Landroid/widget/Spinner;

    .line 3
    invoke-virtual {v3}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/w9$c;

    iget-object v2, v2, Lcom/pspdfkit/internal/w9$c;->d:Ljava/util/List;

    iget-object v3, p0, Lcom/pspdfkit/internal/w9;->h:Landroid/widget/EditText;

    .line 4
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/document/sharing/SharingOptions;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method public setOnConfirmDocumentSharingListener(Lcom/pspdfkit/internal/w9$b;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/w9;->f:Lcom/pspdfkit/internal/w9$b;

    return-void
.end method

.class public Lcom/pspdfkit/internal/r1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/qf;


# instance fields
.field protected final a:Lcom/pspdfkit/internal/zf;

.field protected final b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

.field protected final c:Lcom/pspdfkit/internal/mj;

.field protected final d:Landroidx/collection/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/SparseArrayCompat<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/pspdfkit/internal/o2;

.field protected g:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:Z


# direct methods
.method public static synthetic $r8$lambda$0r6g5fffBxwbYBAFw8VdnbX1zvg(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r1;->c(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$2BCdHIGxwhcFpM2qFN8U_rtUtk4(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)V

    return-void
.end method

.method public static synthetic $r8$lambda$JL3dinLY_LfSVVS-D0G012QkxLo(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r1;->b(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public static synthetic $r8$lambda$K64aLaeN8-fkee3IJUNZwZivmoU(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V

    return-void
.end method

.method public static synthetic $r8$lambda$K7qJmityBAkIRVIQFgr8q8HOzUI(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r1;->g(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public static synthetic $r8$lambda$M2X5mGyHn0zDc49H1z-cRsRdLXU(Lcom/pspdfkit/internal/r1;Ljava/util/Collection;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r1;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$M9Z6s6ladu1qR8PZOn0tU_FRYQ4(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->b(Lcom/pspdfkit/annotations/Annotation;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$W6oShXkqeQHmssOcuKcoKtUX8h8(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/annotations/Annotation;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$_GXon6lrg5Pm79KW8KZG8syMAXw(Lcom/pspdfkit/internal/r1;Ljava/lang/Integer;)Lio/reactivex/rxjava3/core/ObservableSource;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r1;->a(Ljava/lang/Integer;)Lio/reactivex/rxjava3/core/ObservableSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$chLS_3q_fqmpxzZKo2n1EEYQjBI(Ljava/lang/String;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/r1;->a(Ljava/lang/String;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$eVtydWw1vS1pGsKEuuysagOnkdc(Lcom/pspdfkit/internal/r1;ILjava/lang/String;)Lcom/pspdfkit/annotations/Annotation;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->b(ILjava/lang/String;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$gR3ZYnVhCusz_oqmxvn1zkkBvHU(Lcom/pspdfkit/internal/r1;I)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r1;->b(I)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$hLsl9Yjj-PAhzwTU_ZHEvx96YqE(Lcom/pspdfkit/internal/r1;Ljava/lang/String;)Lio/reactivex/rxjava3/core/SingleSource;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r1;->a(Ljava/lang/String;)Lio/reactivex/rxjava3/core/SingleSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$nWLqFAqZugc4d4Lr57xq5HQ72_0(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$qQXNODJTCNwwaPoIgl7LK_uepdY(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r1;->d(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$sPRRUJzEqAYYY1im-4wL_io5t3A(Lcom/pspdfkit/internal/r1;II)Lcom/pspdfkit/annotations/Annotation;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->a(II)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$sVi6KkuC3BmBDYI6XN5rgoHHwVI(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;)Ljava/lang/Integer;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r1;->f(Lcom/pspdfkit/annotations/Annotation;)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$s_Dma3DC2i851IAhUwqWD4_H6YI(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r1;->e(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$x3envh6JwF9ArkZScGapGcv7oiU(Lcom/pspdfkit/internal/r1;III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/r1;->a(III)V

    return-void
.end method

.method public static synthetic $r8$lambda$x_hb4yYilZGOMuUIkE709ImCmgU(Ljava/util/EnumSet;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/r1;->a(Ljava/util/EnumSet;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p0

    return p0
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;)V
    .locals 3

    .line 1
    new-instance v0, Landroidx/collection/SparseArrayCompat;

    invoke-direct {v0}, Landroidx/collection/SparseArrayCompat;-><init>()V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->b()Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    move-result-object v2

    .line 6
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/pspdfkit/internal/r1;-><init>(Lcom/pspdfkit/internal/zf;Landroidx/collection/SparseArrayCompat;Ljava/util/HashSet;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;Landroidx/collection/SparseArrayCompat;Ljava/util/HashSet;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)V
    .locals 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/r1;->g:Lcom/pspdfkit/internal/nh;

    const/4 v0, 0x0

    .line 13
    iput-boolean v0, p0, Lcom/pspdfkit/internal/r1;->h:Z

    .line 44
    iput-object p1, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    .line 45
    iput-object p4, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 46
    iput-object p2, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    .line 47
    iput-object p3, p0, Lcom/pspdfkit/internal/r1;->e:Ljava/util/Set;

    .line 48
    new-instance p2, Lcom/pspdfkit/internal/o2;

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/o2;-><init>(Lcom/pspdfkit/internal/zf;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/r1;->f:Lcom/pspdfkit/internal/o2;

    .line 49
    invoke-static {}, Lcom/pspdfkit/internal/gj;->n()Lcom/pspdfkit/internal/mj;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/r1;->c:Lcom/pspdfkit/internal/mj;

    return-void
.end method

.method private synthetic a(II)Lcom/pspdfkit/annotations/Annotation;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 292
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->getAnnotation(II)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 289
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->getReviewSummary(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Ljava/lang/Integer;)Lio/reactivex/rxjava3/core/ObservableSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 294
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->getAnnotationsAsync(I)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/ci$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ci$$ExternalSyntheticLambda0;-><init>()V

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->flatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Ljava/lang/String;)Lio/reactivex/rxjava3/core/SingleSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 297
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->createAnnotationFromInstantJson(Ljava/lang/String;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Ljava/util/Collection;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 293
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->getAnnotations(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(III)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 298
    invoke-virtual {p0, p1, p2, p3}, Lcom/pspdfkit/internal/r1;->moveAnnotation(III)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/annotations/Annotation;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 296
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;I)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 299
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->moveAnnotation(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 288
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->appendAnnotationState(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)V

    return-void
.end method

.method private static synthetic a(Ljava/lang/String;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 286
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getInReplyToUuid()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 287
    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static synthetic a(Ljava/util/EnumSet;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 295
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method private b(ILjava/lang/String;)Lcom/pspdfkit/annotations/Annotation;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "uuid"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uuid"

    const/4 v1, 0x0

    .line 52
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    monitor-enter p0

    .line 54
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 55
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    monitor-exit p0

    move-object v1, v0

    goto :goto_0

    .line 59
    :cond_1
    monitor-exit p0

    :goto_0
    return-object v1

    :catchall_0
    move-exception p1

    .line 60
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private synthetic b(I)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 61
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private synthetic b(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 62
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method private synthetic b(Lcom/pspdfkit/annotations/Annotation;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 63
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->moveAnnotation(Lcom/pspdfkit/annotations/Annotation;I)V

    return-void
.end method

.method private synthetic c(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->getAnnotationReplies(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private d(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/annotations/Annotation;Z)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method private synthetic e(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->getReviewHistory(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private synthetic f(Lcom/pspdfkit/annotations/Annotation;)Ljava/lang/Integer;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->getZIndex(Lcom/pspdfkit/annotations/Annotation;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method private synthetic g(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/jni/NativeAnnotation;[B[B)Landroid/graphics/RectF;
    .locals 3

    .line 300
    monitor-enter p0

    .line 301
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->updateProperties(Lcom/pspdfkit/internal/jni/NativeAnnotation;[B[B)Lcom/pspdfkit/internal/jni/NativeUpdatePropertiesResult;

    move-result-object p2

    .line 303
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeUpdatePropertiesResult;->getHasError()Z

    move-result p3

    if-eqz p3, :cond_0

    const-string p3, "PSPDFKit.Annotations"

    const-string v0, "Can\'t update annotation properties %s: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 p1, 0x1

    .line 310
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeUpdatePropertiesResult;->getErrorString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, p1

    .line 311
    invoke-static {p3, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 321
    :cond_0
    iget-boolean p1, p0, Lcom/pspdfkit/internal/r1;->i:Z

    if-eqz p1, :cond_1

    .line 322
    iget-object p1, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->synchronizeToBackend()V

    .line 324
    :cond_1
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeUpdatePropertiesResult;->getUpdatedBoundingBox()Landroid/graphics/RectF;

    move-result-object p1

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 325
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Z)Lcom/pspdfkit/annotations/Annotation;
    .locals 7

    .line 350
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getProperties(Lcom/pspdfkit/internal/jni/NativeAnnotation;)[B

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    .line 351
    array-length v0, v0

    if-nez v0, :cond_0

    goto/16 :goto_3

    .line 357
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/p1;

    invoke-direct {v0}, Lcom/pspdfkit/internal/p1;-><init>()V

    .line 358
    iget-object v2, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {v0, p1, v2}, Lcom/pspdfkit/internal/p1;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)V

    .line 360
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    xor-int/lit8 v4, v2, 0x1

    .line 367
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationType()Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    move-result-object v5

    invoke-static {v5}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/jni/NativeAnnotationType;)Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v5

    .line 369
    sget-object v6, Lcom/pspdfkit/internal/r1$a;->b:[I

    invoke-virtual {v5}, Ljava/lang/Enum;->ordinal()I

    move-result v5

    aget v5, v6, v5

    packed-switch v5, :pswitch_data_0

    .line 455
    new-instance v1, Lcom/pspdfkit/annotations/UnknownAnnotation;

    .line 456
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationType()Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    move-result-object v5

    invoke-direct {v1, v5, v0, v4}, Lcom/pspdfkit/annotations/UnknownAnnotation;-><init>(Lcom/pspdfkit/internal/jni/NativeAnnotationType;Lcom/pspdfkit/internal/p1;Z)V

    goto/16 :goto_1

    .line 457
    :pswitch_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v5

    sget-object v6, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->REDACTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v5, v6}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 458
    new-instance v1, Lcom/pspdfkit/annotations/RedactionAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/RedactionAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto/16 :goto_1

    .line 459
    :pswitch_1
    new-instance v1, Lcom/pspdfkit/annotations/ScreenAnnotation;

    .line 460
    iget-object v5, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/zf;->j()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v5

    .line 461
    invoke-virtual {v5, p1}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->findResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v0, v4, v5}, Lcom/pspdfkit/annotations/ScreenAnnotation;-><init>(Lcom/pspdfkit/internal/p1;ZLjava/lang/String;)V

    goto/16 :goto_1

    .line 462
    :pswitch_2
    new-instance v1, Lcom/pspdfkit/annotations/RichMediaAnnotation;

    .line 463
    iget-object v5, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/zf;->j()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v5

    .line 464
    invoke-virtual {v5, p1}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->findResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v0, v4, v5}, Lcom/pspdfkit/annotations/RichMediaAnnotation;-><init>(Lcom/pspdfkit/internal/p1;ZLjava/lang/String;)V

    goto/16 :goto_1

    .line 465
    :pswitch_3
    new-instance v1, Lcom/pspdfkit/annotations/CircleAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/CircleAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto/16 :goto_1

    .line 466
    :pswitch_4
    new-instance v1, Lcom/pspdfkit/annotations/SquareAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/SquareAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto/16 :goto_1

    .line 467
    :pswitch_5
    new-instance v1, Lcom/pspdfkit/annotations/PolylineAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/PolylineAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto/16 :goto_1

    .line 468
    :pswitch_6
    new-instance v1, Lcom/pspdfkit/annotations/PolygonAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/PolygonAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto/16 :goto_1

    .line 469
    :pswitch_7
    new-instance v1, Lcom/pspdfkit/annotations/LineAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/LineAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto/16 :goto_1

    .line 470
    :pswitch_8
    new-instance v1, Lcom/pspdfkit/annotations/SoundAnnotation;

    .line 471
    iget-object v5, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/zf;->j()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v5

    .line 472
    invoke-virtual {v5, p1}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->findResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v0, v4, v5}, Lcom/pspdfkit/annotations/SoundAnnotation;-><init>(Lcom/pspdfkit/internal/p1;ZLjava/lang/String;)V

    goto :goto_1

    .line 473
    :pswitch_9
    new-instance v1, Lcom/pspdfkit/annotations/FileAnnotation;

    .line 474
    iget-object v5, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/zf;->j()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v5

    .line 475
    invoke-virtual {v5, p1}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->findResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v0, v4, v5}, Lcom/pspdfkit/annotations/FileAnnotation;-><init>(Lcom/pspdfkit/internal/p1;ZLjava/lang/String;)V

    goto :goto_1

    .line 476
    :pswitch_a
    new-instance v1, Lcom/pspdfkit/annotations/StampAnnotation;

    .line 477
    iget-object v5, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/zf;->j()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v5

    .line 478
    invoke-virtual {v5, p1}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->findImageResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v0, v4, v5}, Lcom/pspdfkit/annotations/StampAnnotation;-><init>(Lcom/pspdfkit/internal/p1;ZLjava/lang/String;)V

    goto :goto_1

    .line 479
    :pswitch_b
    new-instance v1, Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/InkAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_1

    .line 480
    :pswitch_c
    new-instance v1, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/FreeTextAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_1

    .line 481
    :pswitch_d
    new-instance v1, Lcom/pspdfkit/annotations/StrikeOutAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/StrikeOutAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_1

    .line 482
    :pswitch_e
    new-instance v1, Lcom/pspdfkit/annotations/HighlightAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/HighlightAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_1

    .line 483
    :pswitch_f
    new-instance v1, Lcom/pspdfkit/annotations/UnderlineAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/UnderlineAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_1

    .line 484
    :pswitch_10
    new-instance v1, Lcom/pspdfkit/annotations/SquigglyAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/SquigglyAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_1

    .line 485
    :pswitch_11
    new-instance v1, Lcom/pspdfkit/annotations/NoteAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/NoteAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_1

    .line 486
    :pswitch_12
    new-instance v1, Lcom/pspdfkit/annotations/WidgetAnnotation;

    .line 487
    iget-object v5, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/zf;->j()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v5

    .line 488
    invoke-virtual {v5, p1}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->findImageResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v0, v4, v5}, Lcom/pspdfkit/annotations/WidgetAnnotation;-><init>(Lcom/pspdfkit/internal/p1;ZLjava/lang/String;)V

    goto :goto_1

    .line 489
    :pswitch_13
    new-instance v1, Lcom/pspdfkit/annotations/LinkAnnotation;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/annotations/LinkAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    :cond_2
    :goto_1
    if-eqz v1, :cond_5

    if-eqz v2, :cond_3

    .line 585
    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->c:Lcom/pspdfkit/internal/mj;

    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 586
    invoke-interface {p2, p1, v0}, Lcom/pspdfkit/internal/mj;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)Lcom/pspdfkit/internal/oj;

    move-result-object p2

    .line 587
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-interface {v0, v2, p2, v3}, Lcom/pspdfkit/internal/pf;->onAttachToDocument(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/oj;Z)V

    .line 588
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p2

    invoke-interface {p2}, Lcom/pspdfkit/internal/pf;->synchronizeFromNativeObjectIfAttached()V

    goto :goto_2

    :cond_3
    if-eqz p2, :cond_4

    .line 591
    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    monitor-enter p2

    .line 596
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->holdAnnotation(Lcom/pspdfkit/internal/jni/NativeAnnotation;)I

    move-result v0

    .line 597
    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 599
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p2

    .line 600
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-interface {p2, v0, v2}, Lcom/pspdfkit/internal/pf;->setDetachedAnnotationLookupKey(Ljava/lang/Integer;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)V

    goto :goto_2

    :catchall_0
    move-exception p1

    .line 601
    :try_start_1
    monitor-exit p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    .line 609
    :cond_4
    :goto_2
    invoke-static {v1, p1}, Lcom/pspdfkit/internal/kn;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/jni/NativeAnnotation;)V

    :cond_5
    :goto_3
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(ILjava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 290
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/r1;ILjava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x5

    .line 291
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;Z)Ljava/util/ArrayList;
    .locals 9

    if-nez p2, :cond_1

    .line 740
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_REPLIES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 741
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Your current license doesn\'t allow creating annotation replies."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    const-string v0, "annotation"

    const-string v1, "Annotation for which we\'re retrieving replies cannot be null."

    .line 742
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 747
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    .line 748
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v1

    .line 749
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v2

    .line 750
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result p1

    if-eqz p1, :cond_7

    const/high16 p1, -0x80000000

    if-eq v0, p1, :cond_7

    if-eq v1, p1, :cond_7

    if-eqz v2, :cond_7

    .line 757
    monitor-enter p0

    .line 763
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object p1

    if-eqz p2, :cond_2

    .line 766
    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    sget-object v0, Lcom/pspdfkit/internal/jni/NativeReplyType;->TEXT_AND_STATE:Lcom/pspdfkit/internal/jni/NativeReplyType;

    invoke-virtual {p2, v2, v0}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getAnnotationsForDeletion(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeReplyType;)Ljava/util/ArrayList;

    move-result-object p2

    goto :goto_1

    .line 769
    :cond_2
    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    sget-object v0, Lcom/pspdfkit/internal/jni/NativeReplyType;->TEXT_AND_STATE:Lcom/pspdfkit/internal/jni/NativeReplyType;

    invoke-virtual {p2, v2, v0}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getFlattenedAnnotationReplies(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeReplyType;)Ljava/util/ArrayList;

    move-result-object p2

    .line 772
    :goto_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 774
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_3
    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/jni/NativeAnnotation;

    .line 775
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object v1

    if-nez v1, :cond_4

    const-string v1, "PSPDFKit.Annotations"

    const-string v2, "Fetched native reply without valid annotation ID. Skipping."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    .line 777
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 780
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/Annotation;

    .line 781
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v6

    int-to-long v6, v6

    cmp-long v8, v4, v6

    if-nez v8, :cond_5

    .line 782
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 787
    :cond_6
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    .line 788
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 789
    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Retrieval of replies for detached annotations is not supported."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Lcom/pspdfkit/forms/FormField;)Ljava/util/ArrayList;
    .locals 7

    const-string v0, "formField"

    const-string v1, "argumentName"

    .line 199
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formField"

    const/4 v1, 0x0

    .line 250
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 251
    monitor-enter p0

    .line 254
    :try_start_0
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/tf;->getNativeFormField()Lcom/pspdfkit/internal/jni/NativeFormField;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeFormField;->getWidgetAnnotations()Lcom/pspdfkit/internal/jni/NativeAnnotationPager;

    move-result-object p1

    .line 255
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotationPager;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 256
    :goto_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotationPager;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    const/16 v4, 0x64

    .line 257
    invoke-virtual {p1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeAnnotationPager;->get(II)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v3, v3, 0x64

    goto :goto_0

    .line 258
    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {p1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 259
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/jni/NativeAnnotation;

    .line 260
    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationType()Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->WIDGET:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    if-eq v4, v5, :cond_2

    goto :goto_1

    .line 265
    :cond_2
    iget-object v4, p0, Lcom/pspdfkit/internal/r1;->c:Lcom/pspdfkit/internal/mj;

    iget-object v5, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 266
    invoke-interface {v4, v3, v5}, Lcom/pspdfkit/internal/mj;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)Lcom/pspdfkit/internal/oj;

    move-result-object v4

    .line 267
    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getPlatformAnnotation()Lcom/pspdfkit/internal/jni/NativePlatformAnnotation;

    move-result-object v5

    .line 268
    instance-of v6, v5, Lcom/pspdfkit/internal/kn;

    if-eqz v6, :cond_3

    .line 269
    check-cast v5, Lcom/pspdfkit/internal/kn;

    const-class v6, Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-virtual {v5, v6}, Lcom/pspdfkit/internal/kn;->a(Ljava/lang/Class;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/annotations/WidgetAnnotation;

    goto :goto_2

    :cond_3
    move-object v5, v1

    :goto_2
    if-nez v5, :cond_4

    .line 272
    invoke-virtual {p0, v3, v2}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Z)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/pspdfkit/annotations/WidgetAnnotation;

    :cond_4
    if-eqz v5, :cond_1

    .line 275
    invoke-virtual {v5}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    iget-object v6, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-interface {v3, v6, v4, v2}, Lcom/pspdfkit/internal/pf;->onAttachToDocument(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/oj;Z)V

    .line 279
    invoke-virtual {v5}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 280
    invoke-virtual {v5}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->synchronizeFromNativeObjectIfAttached()V

    .line 281
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 284
    :cond_5
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 285
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public a(Ljava/util/Set;)Ljava/util/ArrayList;
    .locals 3

    .line 326
    monitor-enter p0

    .line 327
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 328
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_0

    goto :goto_0

    .line 330
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/r1;->e:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 331
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 333
    :cond_1
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    .line 334
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method protected final a(I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    const-string v0, "Retrieved cached annotations for page "

    .line 344
    monitor-enter p0

    .line 345
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v1, p1}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_0

    const/4 p1, 0x0

    .line 346
    monitor-exit p0

    return-object p1

    :cond_0
    const-string v2, "PSPDFKit.Annotations"

    .line 347
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, p1, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 348
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    .line 349
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public a()V
    .locals 4

    .line 335
    monitor-enter p0

    const/4 v0, 0x0

    .line 337
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v1}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 338
    iget-object v1, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v1, v0}, Landroidx/collection/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_0

    goto :goto_2

    .line 339
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    .line 340
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->needsSyncingWithCore()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 341
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    goto :goto_1

    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 342
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->synchronizeToBackend()V

    .line 343
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)V
    .locals 3

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 611
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 662
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "dataProvider"

    .line 664
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 715
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 716
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 722
    new-instance v1, Lcom/pspdfkit/internal/s7;

    invoke-direct {v1, p2}, Lcom/pspdfkit/internal/s7;-><init>(Lcom/pspdfkit/document/providers/DataProvider;)V

    invoke-static {v0, v1, p3}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->attachBinaryInstantJson(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeDataProvider;Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object p2

    .line 724
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeResult;->getHasError()Z

    move-result v1

    if-nez v1, :cond_1

    const-string p2, "image/jpeg"

    .line 727
    invoke-virtual {p2, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 728
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p2

    invoke-interface {p2, v2}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    .line 729
    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    .line 730
    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->j()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object p2

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->findResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 733
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p3

    new-instance v0, Lcom/pspdfkit/internal/a0;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)V

    .line 734
    invoke-interface {p3, v0}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    :cond_0
    return-void

    .line 738
    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeResult;->getErrorString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 739
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "The annotation needs to be attached to the document to fetch the attached binary instant JSON."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/jni/NativeAnnotation;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 6

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotation"

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 54
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-interface {v0, v2}, Lcom/pspdfkit/internal/pf;->ensureAnnotationCanBeAttachedToDocument(Lcom/pspdfkit/internal/zf;)V

    .line 59
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object v0

    .line 60
    monitor-enter p0

    .line 64
    :try_start_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v2

    if-nez v2, :cond_3

    .line 73
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getDetachedAnnotationLookupKey()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 75
    iget-object v3, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 76
    :try_start_1
    iget-object v4, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getHeldAnnotation(I)Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v4

    .line 77
    iget-object v5, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v5, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->dropAnnotation(I)V

    .line 78
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79
    :try_start_2
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2, v1, v1}, Lcom/pspdfkit/internal/pf;->setDetachedAnnotationLookupKey(Ljava/lang/Integer;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v1, v4

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 80
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw p1

    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 86
    iget-object v1, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 87
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v2

    .line 88
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    .line 89
    const-class v4, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    invoke-static {v4, v3}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 90
    invoke-virtual {v1, v2, v3, p2}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->createAnnotation(ILcom/pspdfkit/internal/jni/NativeAnnotationType;Ljava/lang/Integer;)Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v1

    .line 98
    :cond_1
    invoke-virtual {p0, p1, v1}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/jni/NativeAnnotation;)V

    .line 101
    iget-object v2, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {v2, v1, p2, p3}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->attachToDocumentIfNotAttached(Lcom/pspdfkit/internal/jni/NativeAnnotation;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/pspdfkit/internal/jni/NativeResult;

    .line 104
    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->c:Lcom/pspdfkit/internal/mj;

    iget-object v2, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 105
    invoke-interface {p2, v1, v2}, Lcom/pspdfkit/internal/mj;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)Lcom/pspdfkit/internal/oj;

    move-result-object p2

    .line 106
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v4, 0x1

    invoke-interface {v2, v3, p2, v4}, Lcom/pspdfkit/internal/pf;->onAttachToDocument(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/oj;Z)V

    .line 108
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/kn;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/jni/NativeAnnotation;)V

    .line 112
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p2

    const/4 v1, 0x0

    invoke-interface {p2, v1}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached(Z)Z

    .line 113
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p2

    invoke-interface {p2}, Lcom/pspdfkit/internal/pf;->synchronizeFromNativeObjectIfAttached()V

    if-eqz p3, :cond_2

    .line 116
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 118
    :cond_2
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    :goto_1
    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result p3

    invoke-virtual {p2, p3, v0}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 122
    iput-boolean v4, p0, Lcom/pspdfkit/internal/r1;->h:Z

    const-string p2, "PSPDFKit.Annotations"

    const-string p3, "Attached annotation %s with objNum %d to page %d."

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 126
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    aput-object v2, v0, v1

    .line 127
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x2

    .line 128
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 129
    invoke-static {p2, p3, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 137
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->h(Lcom/pspdfkit/annotations/Annotation;)V

    return-void

    .line 138
    :cond_3
    :try_start_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "This annotation can\'t be added, since it is already attached to a document."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_1
    move-exception p1

    .line 196
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw p1

    .line 197
    :cond_4
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    .line 198
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    const-string p3, "Your license does not allow editing this annotation: "

    invoke-virtual {p3, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public a(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public final addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0, v0}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public final addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;I)V
    .locals 1

    .line 2
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public final addAnnotationToPageAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda18;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda18;-><init>(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 55
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final addAnnotationToPageAsync(Lcom/pspdfkit/annotations/Annotation;I)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 56
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;I)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x5

    .line 57
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final addAppearanceStreamGenerator(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/r1;->addAppearanceStreamGenerator(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;Z)V

    return-void
.end method

.method public final addAppearanceStreamGenerator(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;Z)V
    .locals 2

    const-string v0, "appearanceStreamGenerator"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->f:Lcom/pspdfkit/internal/o2;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/o2;->a(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;Z)V

    return-void
.end method

.method public final addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
    .locals 2

    const-string v0, "updatedListener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final appendAnnotationState(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)V
    .locals 4

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_REPLIES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "annotation"

    const-string v1, "Annotation for which we\'re appending the state cannot be null."

    .line 2
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 5
    monitor-enter p0

    .line 6
    :try_start_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 7
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeAnnotationStateChange;

    .line 8
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/note/AnnotationStateChange;->getAuthor()Ljava/lang/String;

    move-result-object v1

    .line 9
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/note/AnnotationStateChange;->getAuthorState()Lcom/pspdfkit/annotations/note/AuthorState;

    move-result-object v2

    .line 10
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeAuthorState;->values()[Lcom/pspdfkit/internal/jni/NativeAuthorState;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget-object v2, v3, v2

    .line 11
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/note/AnnotationStateChange;->getCreationDate()Ljava/util/Date;

    move-result-object p2

    invoke-direct {v0, v1, v2, p2}, Lcom/pspdfkit/internal/jni/NativeAnnotationStateChange;-><init>(Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeAuthorState;Ljava/util/Date;)V

    .line 12
    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {p2, p1, v0}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->appendAnnotationState(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeAnnotationStateChange;)Lcom/pspdfkit/internal/jni/NativeAnnotation;

    .line 14
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 15
    :cond_1
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Your current license doesn\'t allow creating annotation replies."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final appendAnnotationStateAsync(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)Lio/reactivex/rxjava3/core/Completable;
    .locals 3

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "annotationStateChange"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x5

    .line 108
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final b()Lcom/pspdfkit/internal/o2;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->f:Lcom/pspdfkit/internal/o2;

    return-object v0
.end method

.method public final c()Lcom/pspdfkit/internal/jni/NativeAnnotationManager;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    return-object v0
.end method

.method public final c(I)V
    .locals 2

    .line 3
    monitor-enter p0

    .line 4
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0, p1}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-nez p1, :cond_0

    goto :goto_1

    .line 5
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->needsSyncingWithCore()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    goto :goto_0

    .line 8
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->synchronizeToBackend()V

    .line 9
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final createAnnotationFromInstantJson(Ljava/lang/String;)Lcom/pspdfkit/annotations/Annotation;
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hb;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "annotationJson"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationJson"

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    monitor-enter p0

    .line 55
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 56
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->createAnnotationFromInstantJson(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 61
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getPageIndex()Ljava/lang/Integer;

    move-result-object v0

    .line 62
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_1

    if-eqz v0, :cond_1

    .line 70
    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/r1;->a(Ljava/util/Set;)Ljava/util/ArrayList;

    .line 71
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Long;->intValue()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/r1;->getAnnotation(II)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 76
    iput-boolean v0, p0, Lcom/pspdfkit/internal/r1;->h:Z

    .line 77
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->h(Lcom/pspdfkit/annotations/Annotation;)V

    return-object p1

    .line 79
    :cond_0
    :try_start_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Annotation is not valid JSON."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 80
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Annotation is not valid JSON."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 81
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Annotation is not valid JSON."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 100
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    .line 101
    :cond_3
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Your license does not allow annotation editing."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final createAnnotationFromInstantJsonAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    const-string v0, "annotationJson"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda7;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/r1;Ljava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 55
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final d()Lcom/pspdfkit/internal/jni/NativeResourceManager;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->j()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .locals 3

    .line 2
    monitor-enter p0

    const/4 v0, 0x0

    .line 3
    :try_start_0
    iput-boolean v0, p0, Lcom/pspdfkit/internal/r1;->h:Z

    .line 4
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v1}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v1, v0}, Landroidx/collection/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 6
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    .line 7
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->clearModified()V

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 10
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected final f()V
    .locals 1

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/r1;->i:Z

    return-void
.end method

.method protected final finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->c:Lcom/pspdfkit/internal/mj;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mj;->clear()V

    .line 2
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method

.method public final getAllAnnotationsOfType(Ljava/util/EnumSet;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->getAllAnnotationsOfTypeAsync(Ljava/util/EnumSet;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Observable;->toList()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    return-object p1
.end method

.method public final getAllAnnotationsOfType(Ljava/util/EnumSet;II)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;II)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 2
    invoke-virtual {p0, p1, p2, p3}, Lcom/pspdfkit/internal/r1;->getAllAnnotationsOfTypeAsync(Ljava/util/EnumSet;II)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 3
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Observable;->toList()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 4
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    return-object p1
.end method

.method public final getAllAnnotationsOfTypeAsync(Ljava/util/EnumSet;)Lio/reactivex/rxjava3/core/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/pspdfkit/internal/r1;->getAllAnnotationsOfTypeAsync(Ljava/util/EnumSet;II)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final getAllAnnotationsOfTypeAsync(Ljava/util/EnumSet;II)Lio/reactivex/rxjava3/core/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;II)",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 2
    invoke-static {p2, p3}, Lio/reactivex/rxjava3/core/Observable;->range(II)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p2

    new-instance p3, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda12;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda12;-><init>(Lcom/pspdfkit/internal/r1;)V

    .line 3
    invoke-virtual {p2, p3}, Lio/reactivex/rxjava3/core/Observable;->concatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p2

    new-instance p3, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda13;

    invoke-direct {p3, p1}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda13;-><init>(Ljava/util/EnumSet;)V

    .line 4
    invoke-virtual {p2, p3}, Lio/reactivex/rxjava3/core/Observable;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 5
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    return-object p1

    .line 6
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Desired types must be passed into this method!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getAnnotation(II)Lcom/pspdfkit/annotations/Annotation;
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object p1

    .line 3
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v1

    if-ne v1, p2, :cond_0

    .line 5
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 p1, 0x0

    .line 8
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 9
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final getAnnotationAsync(II)Lio/reactivex/rxjava3/core/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda8;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/r1;II)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x5

    .line 2
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public final getAnnotationReplies(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_REPLIES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "annotation"

    const-string v1, "Annotation for which we\'re retrieving replies cannot be null."

    .line 2
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getUuid()Ljava/lang/String;

    move-result-object v1

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result p1

    if-eqz p1, :cond_0

    const/high16 p1, -0x80000000

    if-eq v0, p1, :cond_0

    .line 13
    monitor-enter p0

    .line 18
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda3;

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda3;-><init>(Ljava/lang/String;)V

    .line 19
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 23
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Observable;->toList()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 24
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 25
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 26
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Retrieval of replies for detached annotations is not supported."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 27
    :cond_1
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Your current license doesn\'t allow creating annotation replies."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getAnnotationRepliesAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 55
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public getAnnotations(I)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    move/from16 v0, p1

    if-ltz v0, :cond_b

    .line 1
    iget-object v2, v1, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v2

    if-ge v0, v2, :cond_b

    .line 8
    monitor-enter p0

    .line 14
    :try_start_0
    invoke-virtual/range {p0 .. p1}, Lcom/pspdfkit/internal/r1;->a(I)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 15
    iget-object v3, v1, Lcom/pspdfkit/internal/r1;->e:Ljava/util/Set;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit p0

    return-object v0

    .line 21
    :cond_0
    iget-object v3, v1, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {v3, v0}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getAnnotations(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 25
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 26
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x0

    if-eqz v5, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/jni/NativeAnnotation;

    .line 33
    invoke-virtual {v5}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object v7

    if-eqz v7, :cond_2

    const/4 v7, 0x1

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    :goto_1
    const/4 v8, 0x0

    if-eqz v7, :cond_3

    .line 40
    iget-object v9, v1, Lcom/pspdfkit/internal/r1;->c:Lcom/pspdfkit/internal/mj;

    iget-object v10, v1, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 41
    invoke-interface {v9, v5, v10}, Lcom/pspdfkit/internal/mj;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)Lcom/pspdfkit/internal/oj;

    move-result-object v9

    goto :goto_2

    :cond_3
    move-object v9, v8

    .line 50
    :goto_2
    invoke-virtual {v5}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getPlatformAnnotation()Lcom/pspdfkit/internal/jni/NativePlatformAnnotation;

    move-result-object v10

    .line 51
    instance-of v11, v10, Lcom/pspdfkit/internal/kn;

    if-eqz v11, :cond_4

    .line 52
    check-cast v10, Lcom/pspdfkit/internal/kn;

    .line 53
    const-class v8, Lcom/pspdfkit/annotations/Annotation;

    .line 54
    invoke-virtual {v10, v8}, Lcom/pspdfkit/internal/kn;->a(Ljava/lang/Class;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v8

    :cond_4
    if-nez v8, :cond_7

    if-eqz v2, :cond_7

    .line 55
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_5
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/pspdfkit/annotations/Annotation;

    .line 57
    invoke-virtual {v11}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v12

    invoke-interface {v12}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v12

    if-nez v12, :cond_6

    goto :goto_3

    .line 61
    :cond_6
    invoke-virtual {v12}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getIdentifier()J

    move-result-wide v12

    invoke-virtual {v5}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getIdentifier()J

    move-result-wide v14

    cmp-long v16, v12, v14

    if-nez v16, :cond_5

    move-object v8, v11

    :cond_7
    if-nez v8, :cond_8

    .line 75
    invoke-virtual {v1, v5, v6}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Z)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v8

    :cond_8
    if-eqz v8, :cond_1

    if-eqz v7, :cond_9

    .line 86
    invoke-virtual {v8}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v5

    iget-object v7, v1, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-interface {v5, v7, v9, v6}, Lcom/pspdfkit/internal/pf;->onAttachToDocument(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/oj;Z)V

    .line 90
    invoke-virtual {v8}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v5

    invoke-interface {v5}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 91
    invoke-virtual {v8}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v5

    invoke-interface {v5}, Lcom/pspdfkit/internal/pf;->synchronizeFromNativeObjectIfAttached()V

    goto :goto_4

    .line 98
    :cond_9
    new-instance v6, Lcom/pspdfkit/internal/p1;

    invoke-direct {v6}, Lcom/pspdfkit/internal/p1;-><init>()V

    .line 99
    iget-object v7, v1, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {v6, v5, v7}, Lcom/pspdfkit/internal/p1;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)V

    .line 100
    invoke-virtual {v8}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v5

    invoke-interface {v5, v6}, Lcom/pspdfkit/internal/pf;->setProperties(Lcom/pspdfkit/internal/p1;)V

    .line 102
    :goto_4
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_a
    const-string v2, "PSPDFKit.Annotations"

    .line 107
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Caching annotations for page "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v2, v3, v5}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    iget-object v2, v1, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v2, v0, v4}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 109
    iget-object v2, v1, Lcom/pspdfkit/internal/r1;->e:Ljava/util/Set;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 114
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 115
    :cond_b
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid page number passed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public final getAnnotations(Ljava/util/Collection;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 116
    monitor-enter p0

    .line 117
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    monitor-exit p0

    return-object p1

    .line 119
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 120
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 121
    invoke-interface {v1, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 122
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    const/4 v2, 0x0

    .line 124
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v3

    if-ge v2, v3, :cond_6

    .line 125
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object v3

    .line 126
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_2

    .line 127
    :cond_1
    invoke-virtual {p1}, Ljava/util/HashSet;->clear()V

    .line 128
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 129
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/annotations/Annotation;

    .line 130
    invoke-virtual {v7}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v8

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-ne v8, v9, :cond_3

    .line 131
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    invoke-virtual {p1, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 137
    :cond_4
    invoke-interface {v1, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 138
    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    goto :goto_3

    :cond_5
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 140
    :cond_6
    :goto_3
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    .line 141
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final getAnnotationsAsync(I)Lio/reactivex/rxjava3/core/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/r1;I)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final getAnnotationsAsync(Ljava/util/Collection;)Lio/reactivex/rxjava3/core/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation

    const-string v0, "objectNumbers"

    const-string v1, "argumentName"

    .line 4
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 55
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 56
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda16;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda16;-><init>(Lcom/pspdfkit/internal/r1;Ljava/util/Collection;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 57
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final getFlattenedAnnotationReplies(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/annotations/Annotation;Z)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method public final getFlattenedAnnotationRepliesAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda10;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 55
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final getReviewHistory(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/note/AnnotationStateChange;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_REPLIES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "annotation"

    const-string v1, "Annotation for which we\'re fetching the annotation review history cannot be null."

    .line 2
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 7
    monitor-enter p0

    .line 8
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 12
    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getReviewHistory(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/util/ArrayList;

    move-result-object p1

    .line 13
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/jni/NativeAnnotationStateChange;

    .line 14
    new-instance v2, Lcom/pspdfkit/annotations/note/AnnotationStateChange;

    .line 15
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeAnnotationStateChange;->getAuthor()Ljava/lang/String;

    move-result-object v3

    .line 16
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeAnnotationStateChange;->getState()Lcom/pspdfkit/internal/jni/NativeAuthorState;

    move-result-object v4

    .line 17
    invoke-static {}, Lcom/pspdfkit/annotations/note/AuthorState;->values()[Lcom/pspdfkit/annotations/note/AuthorState;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget-object v4, v5, v4

    .line 18
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeAnnotationStateChange;->getCreationDate()Ljava/util/Date;

    move-result-object v1

    invoke-direct {v2, v3, v4, v1}, Lcom/pspdfkit/annotations/note/AnnotationStateChange;-><init>(Ljava/lang/String;Lcom/pspdfkit/annotations/note/AuthorState;Ljava/util/Date;)V

    .line 19
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 23
    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    .line 24
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 25
    :cond_1
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Your current license doesn\'t allow creating annotation replies."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getReviewHistoryAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/note/AnnotationStateChange;",
            ">;>;"
        }
    .end annotation

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda11;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda11;-><init>(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 55
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final getReviewSummary(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_REPLIES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "annotation"

    const-string v1, "Annotation for which we\'re fetching the annotation review summary cannot be null."

    .line 2
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 7
    monitor-enter p0

    .line 8
    :try_start_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 11
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getReviewSummary(Lcom/pspdfkit/internal/jni/NativeAnnotation;Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeAnnotationReviewSummary;

    move-result-object p1

    .line 12
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/jni/NativeAnnotationReviewSummary;)Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

    move-result-object p1

    monitor-exit p0

    return-object p1

    :cond_0
    const/4 p1, 0x0

    .line 15
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 16
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 17
    :cond_1
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Your current license doesn\'t allow creating annotation replies."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getReviewSummaryAsync(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;",
            ">;"
        }
    .end annotation

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda14;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda14;-><init>(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x5

    .line 55
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public final getZIndex(Lcom/pspdfkit/annotations/Annotation;)I
    .locals 2

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 59
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    return p1

    .line 60
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Annotation needs to be attached in order for its z-index to be retrieved."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getZIndexAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda15;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda15;-><init>(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final h(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->notifyAnnotationCreated()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 3
    invoke-interface {v1, p1}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public hasUnsavedChanges()Z
    .locals 5

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/r1;->h:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    monitor-exit p0

    return v1

    :cond_0
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 3
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v3}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 4
    iget-object v3, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v3, v2}, Landroidx/collection/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 5
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/Annotation;

    .line 6
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->isModified()Z

    move-result v4

    if-eqz v4, :cond_1

    monitor-exit p0

    return v1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 9
    :cond_3
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final i(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->notifyAnnotationRemoved()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 3
    invoke-interface {v1, p1}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public invalidateCache()V
    .locals 4

    .line 1
    monitor-enter p0

    .line 4
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/internal/r1;->e:Ljava/util/Set;

    iget-object v3, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v3, v1}, Landroidx/collection/SparseArrayCompat;->keyAt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 7
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final j(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->notifyAnnotationUpdated()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 3
    invoke-interface {v1, p1}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final moveAnnotation(III)V
    .locals 5

    .line 208
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hb;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 209
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object v0

    if-ltz p2, :cond_3

    .line 210
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    if-gt p2, v1, :cond_3

    if-ltz p3, :cond_2

    .line 216
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v2

    if-gt p3, v0, :cond_2

    .line 224
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object v0

    .line 225
    monitor-enter p0

    .line 226
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    int-to-long v3, p1

    .line 227
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {v1, v3, v4, p2, p3}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->reorderAnnotation(JILjava/lang/Integer;)Lcom/pspdfkit/internal/jni/NativeResult;

    .line 231
    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {p2, p1}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    if-eqz p2, :cond_0

    .line 233
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 237
    :cond_0
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 238
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 239
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/r1;->a(Ljava/util/Set;)Ljava/util/ArrayList;

    .line 241
    iput-boolean v2, p0, Lcom/pspdfkit/internal/r1;->h:Z

    .line 242
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object p2

    .line 243
    iget-object p3, p0, Lcom/pspdfkit/internal/r1;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 244
    invoke-interface {v1, p1, v0, p2}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V

    goto :goto_0

    .line 245
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 246
    :cond_2
    new-instance p3, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Targeted z-index for moving annotation is illegal - either it\'s lower than 0 or greater than the last possible index: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " on page number "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p3

    .line 247
    :cond_3
    new-instance p3, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "There is no annotation with the specified z-index of: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " on page number "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p3

    .line 248
    :cond_4
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Your license does not allow annotation editing."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final moveAnnotation(Lcom/pspdfkit/annotations/Annotation;I)V
    .locals 2

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 59
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/r1;->getZIndex(Lcom/pspdfkit/annotations/Annotation;)I

    move-result p1

    invoke-virtual {p0, v0, p1, p2}, Lcom/pspdfkit/internal/r1;->moveAnnotation(III)V

    return-void

    .line 60
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Annotation needs to be attached in order for its z-index to be changed."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 61
    :cond_1
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    .line 62
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Your license does not allow editing this annotation: "

    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final moveAnnotation(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V
    .locals 5

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 63
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 114
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "zIndexMove"

    .line 115
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 167
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 168
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 173
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object v0

    .line 174
    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 177
    sget-object v2, Lcom/pspdfkit/internal/r1$a;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v2, p2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq p2, v3, :cond_3

    const/4 v4, 0x2

    if-eq p2, v4, :cond_2

    const/4 v0, 0x3

    if-eq p2, v0, :cond_1

    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    goto :goto_0

    :cond_0
    add-int/lit8 v1, v1, -0x1

    .line 188
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 189
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    add-int/lit8 v1, p2, -0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 190
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v3

    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 204
    :goto_0
    invoke-virtual {p0, p1, v1}, Lcom/pspdfkit/internal/r1;->moveAnnotation(Lcom/pspdfkit/annotations/Annotation;I)V

    return-void

    .line 205
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Annotation needs to be attached in order for its z-index to be changed."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 206
    :cond_5
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    .line 207
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Your license does not allow editing this annotation: "

    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final moveAnnotationAsync(III)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda17;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda17;-><init>(Lcom/pspdfkit/internal/r1;III)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 p3, 0x5

    .line 2
    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final moveAnnotationAsync(Lcom/pspdfkit/annotations/Annotation;I)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;I)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x5

    .line 4
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final moveAnnotationAsync(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda9;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x5

    .line 6
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 17

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    const-string v2, "annotation"

    const-string v3, "argumentName"

    .line 1
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "annotation"

    const/4 v3, 0x0

    .line 52
    invoke-static {v0, v2, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 54
    iget-object v2, v1, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v4

    invoke-interface {v4}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    .line 58
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v2

    if-nez v2, :cond_1

    return-void

    .line 67
    :cond_1
    iget-object v4, v1, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {v4, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->removeAnnotation(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Lcom/pspdfkit/internal/jni/NativeAnnotationListResult;

    move-result-object v2

    .line 68
    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationListResult;->hasError()Z

    move-result v4

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-nez v4, :cond_a

    .line 76
    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationListResult;->value()Ljava/util/ArrayList;

    move-result-object v0

    .line 78
    monitor-enter p0

    .line 79
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 81
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move-object v4, v3

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/pspdfkit/internal/jni/NativeAnnotation;

    if-nez v4, :cond_3

    .line 86
    invoke-virtual {v8}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAbsolutePageIndex()Ljava/lang/Integer;

    move-result-object v4

    if-nez v4, :cond_3

    goto :goto_0

    .line 90
    :cond_3
    iget-object v9, v1, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v9, v10}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    new-array v10, v6, [Ljava/lang/Object;

    aput-object v4, v10, v7

    const-string v11, "PSPDFKit.Annotations"

    const-string v12, "Grooming cache for page %d."

    .line 91
    invoke-static {v11, v12, v10}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    if-nez v9, :cond_4

    new-array v8, v7, [Ljava/lang/Object;

    const-string v9, "PSPDFKit.Annotations"

    const-string v10, "Can\'t remove annotations from cache: removed annotation is not cached yet."

    .line 93
    invoke-static {v9, v10, v8}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 101
    :cond_4
    invoke-virtual {v8}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getPlatformAnnotation()Lcom/pspdfkit/internal/jni/NativePlatformAnnotation;

    move-result-object v10

    instance-of v10, v10, Lcom/pspdfkit/internal/kn;

    if-eqz v10, :cond_5

    .line 103
    invoke-virtual {v8}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getPlatformAnnotation()Lcom/pspdfkit/internal/jni/NativePlatformAnnotation;

    move-result-object v10

    check-cast v10, Lcom/pspdfkit/internal/kn;

    .line 104
    const-class v11, Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v10, v11}, Lcom/pspdfkit/internal/kn;->a(Ljava/lang/Class;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v10

    goto :goto_1

    :cond_5
    move-object v10, v3

    :goto_1
    if-nez v10, :cond_7

    .line 105
    invoke-virtual {v8}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getIdentifier()J

    move-result-wide v11

    .line 106
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/pspdfkit/annotations/Annotation;

    .line 108
    invoke-virtual {v13}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v14

    invoke-interface {v14}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v14

    if-eqz v14, :cond_6

    .line 109
    invoke-virtual {v14}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getIdentifier()J

    move-result-wide v14

    cmp-long v16, v14, v11

    if-nez v16, :cond_6

    move-object v10, v13

    :cond_7
    if-eqz v10, :cond_2

    .line 118
    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-array v8, v5, [Ljava/lang/Object;

    .line 122
    invoke-virtual {v10}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v11

    aput-object v11, v8, v7

    .line 123
    invoke-virtual {v10}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v8, v6

    const-string v11, "PSPDFKit.Annotations"

    const-string v12, "Removed annotation %s with objNum %d."

    .line 124
    invoke-static {v11, v12, v8}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    invoke-interface {v9, v10}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 132
    iget-object v8, v1, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v8, v10, v9}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 136
    iput-boolean v6, v1, Lcom/pspdfkit/internal/r1;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :cond_8
    monitor-exit p0

    .line 137
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    .line 138
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->onDetachedFromDocument()V

    .line 139
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/r1;->i(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_2

    :cond_9
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 140
    :cond_a
    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationListResult;->error()Lcom/pspdfkit/internal/jni/NativeDjinniError;

    move-result-object v2

    .line 141
    new-instance v3, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v7

    .line 142
    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeDjinniError;->getCode()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeDjinniError;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const-string v0, "Could not remove annotation %s: %d %s"

    .line 143
    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 144
    :cond_b
    new-instance v2, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    .line 145
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Your license does not allow editing this annotation: "

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public final removeAnnotationFromPageAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda19;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/r1$$ExternalSyntheticLambda19;-><init>(Lcom/pspdfkit/internal/r1;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 55
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final removeAppearanceStreamGenerator(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;)V
    .locals 2

    const-string v0, "appearanceStreamGenerator"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->f:Lcom/pspdfkit/internal/o2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/o2;->a(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;)V

    return-void
.end method

.method public final removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
    .locals 2

    const-string v0, "updatedListener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

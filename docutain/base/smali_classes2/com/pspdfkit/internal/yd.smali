.class final Lcom/pspdfkit/internal/yd;
.super Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;
.source "SourceFile"

# interfaces
.implements Lokhttp3/Callback;


# instance fields
.field private final a:Lcom/pspdfkit/internal/ud;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:[B

.field private final f:Ljava/lang/String;

.field private g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

.field private h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

.field private i:Lokhttp3/Call;

.field private j:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;


# direct methods
.method static bridge synthetic -$$Nest$ma(Lcom/pspdfkit/internal/yd;Lokio/Source;Lokio/BufferedSink;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/yd;->a(Lokio/Source;Lokio/BufferedSink;)V

    return-void
.end method

.method private constructor <init>(Lcom/pspdfkit/internal/ud;ILjava/lang/String;[BLjava/lang/String;Ljava/util/HashMap;Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/ud;",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "[B",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;",
            "Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;-><init>()V

    .line 2
    sget-object v0, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->IDLE:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    iput-object v0, p0, Lcom/pspdfkit/internal/yd;->j:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    .line 14
    iput-object p1, p0, Lcom/pspdfkit/internal/yd;->a:Lcom/pspdfkit/internal/ud;

    .line 15
    iput p2, p0, Lcom/pspdfkit/internal/yd;->b:I

    .line 16
    iput-object p3, p0, Lcom/pspdfkit/internal/yd;->c:Ljava/lang/String;

    .line 17
    iput-object p4, p0, Lcom/pspdfkit/internal/yd;->e:[B

    .line 18
    iput-object p5, p0, Lcom/pspdfkit/internal/yd;->f:Ljava/lang/String;

    .line 19
    iput-object p6, p0, Lcom/pspdfkit/internal/yd;->d:Ljava/util/HashMap;

    .line 20
    iput-object p7, p0, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    .line 21
    iput-object p8, p0, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    return-void
.end method

.method private a(Lokhttp3/Call;Lokhttp3/Response;Z)Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->i:Lokhttp3/Call;

    if-ne p1, v0, :cond_3

    .line 9
    :try_start_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/yd;->b()Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    .line 19
    invoke-static {p2}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    const/4 p1, 0x0

    return-object p1

    .line 20
    :cond_0
    :try_start_1
    invoke-virtual {p2}, Lokhttp3/Response;->isSuccessful()Z

    move-result p1

    if-nez p1, :cond_2

    invoke-virtual {p2}, Lokhttp3/Response;->code()I

    move-result p1

    const/16 v0, 0xc8

    if-lt p1, v0, :cond_2

    invoke-virtual {p2}, Lokhttp3/Response;->code()I

    move-result p1

    const/16 v0, 0x12c

    if-lt p1, v0, :cond_1

    invoke-virtual {p2}, Lokhttp3/Response;->code()I

    move-result p1

    const/16 v0, 0x190

    if-ge p1, v0, :cond_1

    goto :goto_0

    .line 25
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/pspdfkit/internal/yd;->b(Lokhttp3/Response;Z)Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 29
    invoke-static {p2}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    return-object p1

    .line 30
    :cond_2
    :goto_0
    :try_start_2
    invoke-direct {p0, p2, p3}, Lcom/pspdfkit/internal/yd;->a(Lokhttp3/Response;Z)Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 37
    invoke-static {p2}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    return-object p1

    :catchall_0
    move-exception p1

    invoke-static {p2}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    .line 38
    throw p1

    .line 39
    :cond_3
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    const-string v0, "Cannot handle events for unrelated http call "

    invoke-direct {p3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private a(Lokhttp3/Response;Z)Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;
    .locals 19

    move-object/from16 v1, p0

    .line 40
    iget-object v0, v1, Lcom/pspdfkit/internal/yd;->a:Lcom/pspdfkit/internal/ud;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ud;->a()Ljava/io/File;

    move-result-object v2

    const/4 v3, 0x0

    .line 46
    :try_start_0
    iget-object v0, v1, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    const/16 v4, 0x4000

    const-wide/16 v5, 0x0

    if-eqz v0, :cond_2

    .line 48
    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 50
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 51
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 53
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/internal/yd;->b()Z

    move-result v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    .line 107
    invoke-static {v7}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    return-object v3

    .line 108
    :cond_0
    :try_start_2
    new-instance v0, Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;

    .line 109
    invoke-virtual/range {p1 .. p1}, Lokhttp3/Response;->code()I

    move-result v8

    invoke-virtual/range {p1 .. p1}, Lokhttp3/Response;->headers()Lokhttp3/Headers;

    move-result-object v9

    invoke-static {v9}, Lcom/pspdfkit/internal/rj;->a(Lokhttp3/Headers;)Ljava/util/HashMap;

    move-result-object v9

    invoke-direct {v0, v8, v9, v3}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;-><init>(ILjava/util/HashMap;[B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 110
    :try_start_3
    iget-object v8, v1, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    invoke-virtual {v8, v1, v0}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;->onResponse(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;)V

    .line 111
    iget-object v8, v1, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    if-eqz v8, :cond_1

    .line 112
    invoke-virtual {v8, v1, v0}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;->onResponse(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move-object v8, v7

    move-object v7, v0

    move-object v0, v3

    goto :goto_2

    :catch_0
    move-object v9, v3

    goto/16 :goto_7

    :catchall_0
    move-exception v0

    goto/16 :goto_b

    :catch_1
    move-object v0, v3

    move-object v9, v0

    goto/16 :goto_7

    .line 114
    :cond_2
    :try_start_4
    iget-object v0, v1, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    if-nez v0, :cond_4

    if-eqz p2, :cond_3

    goto :goto_0

    :cond_3
    return-object v3

    .line 116
    :cond_4
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->contentLength()J

    move-result-wide v7

    .line 117
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    cmp-long v9, v7, v5

    if-ltz v9, :cond_5

    long-to-int v8, v7

    goto :goto_1

    :cond_5
    const/16 v8, 0x4000

    .line 118
    :goto_1
    invoke-direct {v0, v8}, Ljava/io/ByteArrayOutputStream;-><init>(I)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    move-object v7, v3

    move-object v8, v7

    .line 124
    :goto_2
    :try_start_5
    invoke-virtual/range {p1 .. p1}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v9

    invoke-virtual {v9}, Lokhttp3/ResponseBody;->source()Lokio/BufferedSource;

    move-result-object v9
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :try_start_6
    new-array v4, v4, [B

    move-wide v10, v5

    .line 129
    :cond_6
    :goto_3
    invoke-interface {v9, v4}, Lokio/BufferedSource;->read([B)I

    move-result v12

    const/4 v13, -0x1

    if-eq v12, v13, :cond_a

    .line 130
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/internal/yd;->b()Z

    move-result v13

    if-eqz v13, :cond_7

    goto :goto_5

    :cond_7
    int-to-long v13, v12

    add-long/2addr v5, v13

    const/4 v13, 0x0

    if-eqz v8, :cond_9

    .line 133
    invoke-virtual {v8, v4, v13, v12}, Ljava/io/FileOutputStream;->write([BII)V

    .line 134
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    const-wide/16 v16, 0x64

    add-long v16, v10, v16

    cmp-long v18, v14, v16

    if-lez v18, :cond_9

    .line 135
    monitor-enter p0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 136
    :try_start_7
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/internal/yd;->b()Z

    move-result v10

    if-eqz v10, :cond_8

    monitor-exit p0

    goto :goto_5

    .line 137
    :cond_8
    iget-object v10, v1, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    invoke-virtual {v10, v1, v5, v6}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;->onProgress(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;J)V

    .line 138
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 139
    monitor-exit p0

    goto :goto_4

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0

    :cond_9
    :goto_4
    if-eqz v0, :cond_6

    .line 143
    invoke-virtual {v0, v4, v13, v12}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_3

    :cond_a
    :goto_5
    if-eqz v0, :cond_b

    .line 147
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 148
    new-instance v4, Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;

    .line 149
    invoke-virtual/range {p1 .. p1}, Lokhttp3/Response;->code()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lokhttp3/Response;->headers()Lokhttp3/Headers;

    move-result-object v6

    invoke-static {v6}, Lcom/pspdfkit/internal/rj;->a(Lokhttp3/Headers;)Ljava/util/HashMap;

    move-result-object v6

    invoke-direct {v4, v5, v6, v0}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;-><init>(ILjava/util/HashMap;[B)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move-object v7, v4

    .line 150
    :cond_b
    invoke-static {v9}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    .line 151
    invoke-static {v8}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    .line 154
    sget-object v0, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->RUNNING:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    sget-object v4, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->SUCCEEDED:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    invoke-direct {v1, v0, v4}, Lcom/pspdfkit/internal/yd;->a(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;)Z

    move-result v0

    if-nez v0, :cond_c

    return-object v3

    .line 155
    :cond_c
    iget-object v0, v1, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    if-eqz v0, :cond_d

    .line 156
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;->onSuccess(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Ljava/lang/String;)V

    .line 158
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_6

    .line 159
    :cond_d
    iget-object v0, v1, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    if-eqz v0, :cond_e

    .line 160
    invoke-virtual {v0, v1, v7}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;->onResponse(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;)V

    :cond_e
    :goto_6
    return-object v7

    :catchall_2
    move-exception v0

    goto :goto_9

    :catchall_3
    move-exception v0

    goto :goto_a

    :catch_2
    move-object v9, v3

    :catch_3
    move-object v0, v7

    move-object v7, v8

    goto :goto_7

    :catchall_4
    move-exception v0

    move-object v8, v3

    goto :goto_a

    :catch_4
    move-object v0, v3

    move-object v7, v0

    move-object v9, v7

    .line 161
    :goto_7
    :try_start_9
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 162
    sget-object v2, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->RUNNING:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    sget-object v4, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->FAILED:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    invoke-direct {v1, v2, v4}, Lcom/pspdfkit/internal/yd;->a(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;)Z

    move-result v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    if-nez v2, :cond_f

    .line 171
    invoke-static {v9}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    .line 172
    invoke-static {v7}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    return-object v3

    :cond_f
    if-nez v0, :cond_10

    .line 173
    :try_start_a
    new-instance v0, Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;

    .line 174
    invoke-virtual/range {p1 .. p1}, Lokhttp3/Response;->code()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lokhttp3/Response;->headers()Lokhttp3/Headers;

    move-result-object v4

    invoke-static {v4}, Lcom/pspdfkit/internal/rj;->a(Lokhttp3/Headers;)Ljava/util/HashMap;

    move-result-object v4

    invoke-direct {v0, v2, v4, v3}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;-><init>(ILjava/util/HashMap;[B)V

    .line 175
    :cond_10
    iget-object v2, v1, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    if-eqz v2, :cond_11

    .line 176
    sget-object v4, Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;->CONNECTION_DROPPED:Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;

    invoke-virtual {v2, v1, v4, v3, v0}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;->onFailure(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;)V

    goto :goto_8

    .line 177
    :cond_11
    iget-object v2, v1, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    if-eqz v2, :cond_12

    .line 178
    sget-object v4, Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;->CONNECTION_DROPPED:Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;

    invoke-virtual {v2, v1, v4, v3, v0}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;->onFailure(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    .line 182
    :cond_12
    :goto_8
    invoke-static {v9}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    .line 183
    invoke-static {v7}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    return-object v3

    :catchall_5
    move-exception v0

    move-object v8, v7

    :goto_9
    move-object v3, v9

    :goto_a
    move-object v7, v8

    .line 184
    :goto_b
    invoke-static {v3}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    .line 185
    invoke-static {v7}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    .line 186
    throw v0
.end method

.method static a(Lcom/pspdfkit/internal/ud;Ljava/lang/String;Ljava/util/HashMap;Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;)Lcom/pspdfkit/internal/yd;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/ud;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;",
            ")",
            "Lcom/pspdfkit/internal/yd;"
        }
    .end annotation

    .line 1
    new-instance v9, Lcom/pspdfkit/internal/yd;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v3, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/internal/yd;-><init>(Lcom/pspdfkit/internal/ud;ILjava/lang/String;[BLjava/lang/String;Ljava/util/HashMap;Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;)V

    return-object v9
.end method

.method static a(Lcom/pspdfkit/internal/ud;Ljava/lang/String;[BLjava/lang/String;Ljava/util/HashMap;Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;)Lcom/pspdfkit/internal/yd;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/ud;",
            "Ljava/lang/String;",
            "[B",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;",
            ")",
            "Lcom/pspdfkit/internal/yd;"
        }
    .end annotation

    .line 2
    new-instance v9, Lcom/pspdfkit/internal/yd;

    const/4 v2, 0x3

    const/4 v7, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/internal/yd;-><init>(Lcom/pspdfkit/internal/ud;ILjava/lang/String;[BLjava/lang/String;Ljava/util/HashMap;Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;)V

    return-object v9
.end method

.method static a(Lcom/pspdfkit/internal/ud;Ljava/lang/String;[BLjava/lang/String;Ljava/util/HashMap;Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;)Lcom/pspdfkit/internal/yd;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/ud;",
            "Ljava/lang/String;",
            "[B",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;",
            "Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;",
            ")",
            "Lcom/pspdfkit/internal/yd;"
        }
    .end annotation

    .line 3
    new-instance v9, Lcom/pspdfkit/internal/yd;

    const/4 v2, 0x2

    move-object v0, v9

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p6

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/internal/yd;-><init>(Lcom/pspdfkit/internal/ud;ILjava/lang/String;[BLjava/lang/String;Ljava/util/HashMap;Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;)V

    return-object v9
.end method

.method static a(Lcom/pspdfkit/internal/ud;Ljava/lang/String;[BLjava/util/HashMap;Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;)Lcom/pspdfkit/internal/yd;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/ud;",
            "Ljava/lang/String;",
            "[B",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;",
            ")",
            "Lcom/pspdfkit/internal/yd;"
        }
    .end annotation

    .line 4
    new-instance v9, Lcom/pspdfkit/internal/yd;

    const/4 v2, 0x4

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/internal/yd;-><init>(Lcom/pspdfkit/internal/ud;ILjava/lang/String;[BLjava/lang/String;Ljava/util/HashMap;Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;)V

    return-object v9
.end method

.method private declared-synchronized a()Lokhttp3/Call;
    .locals 6

    monitor-enter p0

    .line 193
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->i:Lokhttp3/Call;

    if-nez v0, :cond_7

    .line 194
    new-instance v0, Lokhttp3/Request$Builder;

    invoke-direct {v0}, Lokhttp3/Request$Builder;-><init>()V

    .line 195
    iget-object v1, p0, Lcom/pspdfkit/internal/yd;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    const/4 v1, 0x0

    .line 197
    iget-object v2, p0, Lcom/pspdfkit/internal/yd;->d:Ljava/util/HashMap;

    if-eqz v2, :cond_2

    .line 198
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 199
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    goto :goto_0

    .line 200
    :cond_1
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 201
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "Content-Type"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 202
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v1

    goto :goto_0

    .line 206
    :cond_2
    iget v2, p0, Lcom/pspdfkit/internal/yd;->b:I

    invoke-static {v2}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result v2

    if-eqz v2, :cond_6

    const/4 v3, 0x1

    if-eq v2, v3, :cond_5

    const/4 v3, 0x2

    if-eq v2, v3, :cond_4

    const/4 v3, 0x3

    if-eq v2, v3, :cond_3

    goto :goto_1

    .line 217
    :cond_3
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/yd;->a(Lokhttp3/MediaType;)Lokhttp3/RequestBody;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->delete(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    goto :goto_1

    .line 218
    :cond_4
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/yd;->a(Lokhttp3/MediaType;)Lokhttp3/RequestBody;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->put(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    goto :goto_1

    .line 221
    :cond_5
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/yd;->a(Lokhttp3/MediaType;)Lokhttp3/RequestBody;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->post(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    goto :goto_1

    .line 222
    :cond_6
    invoke-virtual {v0}, Lokhttp3/Request$Builder;->get()Lokhttp3/Request$Builder;

    .line 235
    :goto_1
    iget-object v1, p0, Lcom/pspdfkit/internal/yd;->a:Lcom/pspdfkit/internal/ud;

    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ud;->a(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/yd;->i:Lokhttp3/Call;

    .line 237
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->i:Lokhttp3/Call;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Lokhttp3/MediaType;)Lokhttp3/RequestBody;
    .locals 2

    .line 238
    iget v0, p0, Lcom/pspdfkit/internal/yd;->b:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 239
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    iget v0, p0, Lcom/pspdfkit/internal/yd;->b:I

    invoke-static {v0}, Lcom/pspdfkit/internal/xd;->a(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Can\'t create request body for method: "

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 241
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->e:[B

    if-eqz v0, :cond_2

    .line 242
    new-instance v1, Lcom/pspdfkit/internal/vd;

    invoke-direct {v1, p0, p1, v0}, Lcom/pspdfkit/internal/vd;-><init>(Lcom/pspdfkit/internal/yd;Lokhttp3/MediaType;[B)V

    return-object v1

    .line 243
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 244
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/pspdfkit/internal/yd;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 245
    new-instance v1, Lcom/pspdfkit/internal/wd;

    invoke-direct {v1, p0, p1, v0}, Lcom/pspdfkit/internal/wd;-><init>(Lcom/pspdfkit/internal/yd;Lokhttp3/MediaType;Ljava/io/File;)V

    return-object v1

    .line 246
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Body data was not specified."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(Lokio/Source;Lokio/BufferedSink;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 247
    :try_start_0
    invoke-static {p1}, Lokio/Okio;->buffer(Lokio/Source;)Lokio/BufferedSource;

    move-result-object v0

    const/16 v1, 0x4000

    new-array v1, v1, [B

    const-wide/16 v2, 0x0

    move-wide v4, v2

    .line 252
    :cond_0
    :goto_0
    invoke-interface {v0, v1}, Lokio/BufferedSource;->read([B)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    int-to-long v7, v6

    add-long/2addr v2, v7

    const/4 v7, 0x0

    .line 254
    invoke-interface {p2, v1, v7, v6}, Lokio/BufferedSink;->write([BII)Lokio/BufferedSink;

    .line 255
    iget-object v6, p0, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    if-eqz v6, :cond_0

    .line 256
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x64

    add-long/2addr v8, v4

    cmp-long v10, v6, v8

    if-lez v10, :cond_0

    .line 257
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 258
    :try_start_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/yd;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    monitor-exit p0

    goto :goto_1

    .line 259
    :cond_1
    iget-object v4, p0, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    invoke-virtual {v4, p0, v2, v3}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;->onProgress(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;J)V

    .line 260
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 261
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception p2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 265
    :cond_2
    :goto_1
    invoke-static {p1}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    return-void

    :catchall_1
    move-exception p2

    invoke-static {p1}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    .line 266
    throw p2
.end method

.method private a(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;)Z
    .locals 1

    .line 187
    monitor-enter p0

    .line 188
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->j:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    if-ne v0, p1, :cond_0

    .line 189
    iput-object p2, p0, Lcom/pspdfkit/internal/yd;->j:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    const/4 p1, 0x1

    .line 190
    monitor-exit p0

    return p1

    .line 192
    :cond_0
    monitor-exit p0

    const/4 p1, 0x0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private b(Lokhttp3/Response;Z)Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;
    .locals 6

    .line 1
    sget-object v0, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->RUNNING:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    sget-object v1, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->FAILED:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/yd;->a(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    return-object v1

    .line 3
    :cond_1
    invoke-virtual {p1}, Lokhttp3/Response;->code()I

    move-result p2

    const/16 v0, 0x190

    if-lt p2, v0, :cond_6

    .line 8
    invoke-virtual {p1}, Lokhttp3/Response;->code()I

    move-result p2

    const/16 v2, 0x1f4

    if-lt p2, v2, :cond_2

    const/16 v2, 0x257

    if-gt p2, v2, :cond_2

    .line 10
    sget-object p2, Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;->SERVER_HICCUP:Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;

    goto :goto_0

    :cond_2
    if-lt p2, v0, :cond_3

    const/16 v0, 0x1f3

    if-gt p2, v0, :cond_3

    .line 12
    sget-object p2, Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;->INVALID_REQUEST:Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;

    goto :goto_0

    .line 13
    :cond_3
    sget-object p2, Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;->UNKNOWN:Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;

    .line 14
    :goto_0
    invoke-virtual {p1}, Lokhttp3/Response;->message()Ljava/lang/String;

    move-result-object v0

    .line 19
    :try_start_0
    invoke-virtual {p1}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/ResponseBody;->bytes()[B

    move-result-object v2

    .line 20
    new-instance v3, Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;

    .line 21
    invoke-virtual {p1}, Lokhttp3/Response;->code()I

    move-result v4

    invoke-virtual {p1}, Lokhttp3/Response;->headers()Lokhttp3/Headers;

    move-result-object v5

    invoke-static {v5}, Lcom/pspdfkit/internal/rj;->a(Lokhttp3/Headers;)Ljava/util/HashMap;

    move-result-object v5

    invoke-direct {v3, v4, v5, v2}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;-><init>(ILjava/util/HashMap;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 22
    :catch_0
    new-instance v3, Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;

    .line 23
    invoke-virtual {p1}, Lokhttp3/Response;->code()I

    move-result v2

    invoke-virtual {p1}, Lokhttp3/Response;->headers()Lokhttp3/Headers;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/rj;->a(Lokhttp3/Headers;)Ljava/util/HashMap;

    move-result-object p1

    invoke-direct {v3, v2, p1, v1}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;-><init>(ILjava/util/HashMap;[B)V

    .line 24
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    if-eqz p1, :cond_4

    .line 25
    invoke-virtual {p1, p0, v3}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;->onResponse(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;)V

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    invoke-virtual {p1, p0, p2, v0, v3}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;->onFailure(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;)V

    .line 28
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    if-eqz p1, :cond_5

    .line 29
    invoke-virtual {p1, p0, v3}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;->onResponse(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;)V

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    invoke-virtual {p1, p0, p2, v0, v3}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;->onFailure(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;)V

    :cond_5
    return-object v3

    .line 31
    :cond_6
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Publishing error response with unsupported response code: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 32
    invoke-virtual {p1}, Lokhttp3/Response;->code()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private declared-synchronized b()Z
    .locals 2

    monitor-enter p0

    .line 33
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->j:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    sget-object v1, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->RUNNING:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->IDLE:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final cancel()V
    .locals 3

    .line 1
    sget-object v0, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->RUNNING:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    sget-object v1, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->CANCELLED:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/yd;->a(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/yd;->a()Lokhttp3/Call;

    move-result-object v0

    invoke-interface {v0}, Lokhttp3/Call;->cancel()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 4
    sget-object v2, Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;->USER_CANCELLED:Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;

    invoke-virtual {v0, p0, v2, v1, v1}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;->onFailure(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;)V

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    if-eqz v0, :cond_2

    .line 7
    sget-object v2, Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;->USER_CANCELLED:Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;

    invoke-virtual {v0, p0, v2, v1, v1}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;->onFailure(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;)V

    :cond_2
    return-void
.end method

.method public final getBodyData()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->e:[B

    return-object v0
.end method

.method public final getDownloadEventHandler()Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    return-object v0
.end method

.method public final getFilePath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final getHeaders()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/yd;->a()Lokhttp3/Call;

    move-result-object v0

    invoke-interface {v0}, Lokhttp3/Call;->request()Lokhttp3/Request;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Request;->headers()Lokhttp3/Headers;

    move-result-object v0

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/rj;->a(Lokhttp3/Headers;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public final getRequestState()Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->j:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    return-object v0
.end method

.method public final getUploadEventHandler()Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    return-object v0
.end method

.method public final getUri()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final onFailure(Lokhttp3/Call;Ljava/io/IOException;)V
    .locals 3

    .line 1
    sget-object p1, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->RUNNING:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    sget-object v0, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->FAILED:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/yd;->a(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 3
    sget-object v1, Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;->CONNECTION_DROPPED:Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, p0, v1, v2, v0}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;->onFailure(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;)V

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    if-eqz p1, :cond_1

    .line 6
    sget-object v1, Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;->CONNECTION_DROPPED:Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p0, v1, p2, v0}, Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;->onFailure(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequest;Lcom/pspdfkit/instant/internal/jni/NativeHTTPError;Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;)V

    :cond_1
    return-void
.end method

.method public final onResponse(Lokhttp3/Call;Lokhttp3/Response;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/internal/yd;->a(Lokhttp3/Call;Lokhttp3/Response;Z)Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;

    return-void
.end method

.method public final stallThisThread()Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;
    .locals 4

    .line 1
    sget-object v0, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->IDLE:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    sget-object v1, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->RUNNING:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/yd;->a(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 4
    :cond_0
    iput-object v1, p0, Lcom/pspdfkit/internal/yd;->h:Lcom/pspdfkit/instant/internal/jni/NativeHTTPUploadEventHandler;

    .line 5
    iput-object v1, p0, Lcom/pspdfkit/internal/yd;->g:Lcom/pspdfkit/instant/internal/jni/NativeHTTPDownloadEventHandler;

    .line 11
    :try_start_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/yd;->a()Lokhttp3/Call;

    move-result-object v0

    .line 12
    invoke-direct {p0}, Lcom/pspdfkit/internal/yd;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v1

    .line 13
    :cond_1
    invoke-interface {v0}, Lokhttp3/Call;->execute()Lokhttp3/Response;

    move-result-object v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v3, 0x1

    .line 16
    :try_start_1
    invoke-direct {p0, v0, v2, v3}, Lcom/pspdfkit/internal/yd;->a(Lokhttp3/Call;Lokhttp3/Response;Z)Lcom/pspdfkit/instant/internal/jni/NativeHTTPResponse;

    move-result-object v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v2, v1

    .line 18
    :goto_0
    :try_start_2
    iget-object v3, p0, Lcom/pspdfkit/internal/yd;->i:Lokhttp3/Call;

    invoke-virtual {p0, v3, v0}, Lcom/pspdfkit/internal/yd;->onFailure(Lokhttp3/Call;Ljava/io/IOException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 20
    :goto_1
    invoke-static {v2}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    return-object v1

    :catchall_1
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-static {v1}, Lcom/pspdfkit/internal/lb;->a(Ljava/io/Closeable;)V

    .line 21
    throw v0
.end method

.method public final declared-synchronized start()Z
    .locals 2

    monitor-enter p0

    .line 1
    :try_start_0
    sget-object v0, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->IDLE:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    sget-object v1, Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;->RUNNING:Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/yd;->a(Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;Lcom/pspdfkit/instant/internal/jni/NativeHTTPRequestState;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/yd;->a()Lokhttp3/Call;

    move-result-object v0

    invoke-interface {v0, p0}, Lokhttp3/Call;->enqueue(Lokhttp3/Callback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

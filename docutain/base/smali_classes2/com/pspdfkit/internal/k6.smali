.class public final Lcom/pspdfkit/internal/k6;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/em;
.implements Lcom/pspdfkit/internal/ne;
.implements Lcom/pspdfkit/listeners/DocumentListener;
.implements Lcom/pspdfkit/ui/inspector/PropertyInspectorCoordinatorLayoutController$PropertyInspectorLifecycleListener;
.implements Lcom/pspdfkit/internal/views/contentediting/a$a;
.implements Lcom/pspdfkit/internal/views/annotations/d$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/k6$a;,
        Lcom/pspdfkit/internal/k6$b;
    }
.end annotation


# instance fields
.field private A:Z

.field private B:Lcom/pspdfkit/internal/os;

.field private C:Z

.field private D:Z

.field private E:F

.field private final F:Landroid/graphics/RectF;

.field private final G:Landroid/graphics/Rect;

.field private H:Ljava/lang/Boolean;

.field private final b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

.field private final c:I

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/graphics/Paint;

.field private final f:F

.field private final g:Landroid/graphics/Paint;

.field private final h:F

.field private i:F

.field private j:F

.field private final k:Lcom/pspdfkit/internal/lq;

.field private l:Lcom/pspdfkit/internal/dm;

.field private m:Ljava/util/UUID;

.field private n:Ljava/util/UUID;

.field private o:Ljava/lang/Integer;

.field private p:Ljava/lang/Integer;

.field private q:Lcom/pspdfkit/internal/views/contentediting/a;

.field private final r:Lcom/pspdfkit/internal/o7;

.field private final s:[F

.field private t:Landroid/graphics/Matrix;

.field private u:J

.field private final v:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private final w:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/util/UUID;",
            "Lcom/pspdfkit/internal/k6$b;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/util/UUID;",
            "Lcom/pspdfkit/internal/rt;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/util/UUID;",
            "Lcom/pspdfkit/internal/pt;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$3Im9e-LXzGuiewLlcf-YRuFU4ac(Lcom/pspdfkit/internal/k6;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/k6;->l(Lcom/pspdfkit/internal/k6;)V

    return-void
.end method

.method public static synthetic $r8$lambda$4XpyP2o3WGvEsOrv7YE_AMsjZ7c(Lcom/pspdfkit/internal/k6;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/k6;->m(Lcom/pspdfkit/internal/k6;)V

    return-void
.end method

.method public static synthetic $r8$lambda$F4wWPpwy4RoAoex6sKaWuuH6Av0(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/k6;->a(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static synthetic $r8$lambda$LrkeLf9YLCiyAiGzmFCrq4Q6GFo(Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/internal/jni/NativeContentEditingResult;Lio/reactivex/rxjava3/core/SingleEmitter;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/internal/jni/NativeContentEditingResult;Lio/reactivex/rxjava3/core/SingleEmitter;)V

    return-void
.end method

.method public static synthetic $r8$lambda$RyseWogLqdqBhl-FHnaH1cytE4k(Lcom/pspdfkit/internal/k6;Landroid/graphics/RectF;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Landroid/graphics/RectF;)V

    return-void
.end method

.method public static synthetic $r8$lambda$SBb_d5RWWaASlj8TJo_JDAVnBVQ(Lcom/pspdfkit/internal/k6;Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Landroid/view/View;Landroid/view/MotionEvent;)V

    return-void
.end method

.method public static synthetic $r8$lambda$VdQE2atEDvhAkNnW-kcbFRFjo8I(Lcom/pspdfkit/internal/k6;Lio/reactivex/rxjava3/core/SingleEmitter;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Lio/reactivex/rxjava3/core/SingleEmitter;)V

    return-void
.end method

.method public static synthetic $r8$lambda$X7gSnPJfzZLXycld7kOkHg6ydo8(Landroid/view/MotionEvent;Lcom/pspdfkit/internal/k6;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/k6;->a(Landroid/view/MotionEvent;Lcom/pspdfkit/internal/k6;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$pxnv9A_teEybsdb5TQrvyciZybE(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$g;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$g;)V

    return-void
.end method

.method public static synthetic $r8$lambda$xa854UVEC8xbBy7y_y2oD4KMWD0(Lcom/pspdfkit/internal/k6;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/k6;->k(Lcom/pspdfkit/internal/k6;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;I)V
    .locals 6

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    .line 3
    iput p2, p0, Lcom/pspdfkit/internal/k6;->c:I

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Float;

    const/high16 v1, 0x40800000    # 4.0f

    .line 13
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/k6;->d:Ljava/util/List;

    .line 26
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k6;->j()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/k6;->i:F

    .line 35
    new-instance v0, Lcom/pspdfkit/internal/x6;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->d()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/pspdfkit/internal/x6;-><init>(Landroid/content/Context;)V

    .line 37
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "{"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "}__com.pspdfkit.internal.ContentModeHandler.SAVED_STATE_FRAGMENT_TAG"

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 40
    new-instance v3, Lcom/pspdfkit/internal/lq;

    .line 41
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v4

    invoke-virtual {v4}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v4

    .line 42
    invoke-direct {v3, v4, p2, p0}, Lcom/pspdfkit/internal/lq;-><init>(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/pspdfkit/internal/ne;)V

    iput-object v3, p0, Lcom/pspdfkit/internal/k6;->k:Lcom/pspdfkit/internal/lq;

    const/16 p2, 0x9

    new-array p2, p2, [F

    .line 74
    iput-object p2, p0, Lcom/pspdfkit/internal/k6;->s:[F

    .line 77
    new-instance p2, Landroid/graphics/Matrix;

    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/k6;->t:Landroid/graphics/Matrix;

    .line 84
    invoke-static {p2}, Lcom/pspdfkit/internal/k6;->b(Landroid/graphics/Matrix;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/pspdfkit/internal/k6;->u:J

    .line 98
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/k6;->w:Ljava/util/HashMap;

    .line 101
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k6;->j()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/pspdfkit/internal/bv$a;->a(Landroid/content/Context;)V

    .line 102
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    const-string p2, "handler.fragment.configuration"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->v:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 104
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k6;->j()Landroid/content/Context;

    move-result-object p2

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-static {p2, v4}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result p2

    iput p2, p0, Lcom/pspdfkit/internal/k6;->f:F

    .line 105
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k6;->j()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result p2

    iput p2, p0, Lcom/pspdfkit/internal/k6;->h:F

    .line 107
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/x6;->b()I

    move-result p2

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/x6;->a()I

    move-result p2

    .line 108
    :goto_0
    invoke-static {p0, p2}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;I)Landroid/graphics/Paint;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/k6;->e:Landroid/graphics/Paint;

    .line 112
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/x6;->d()I

    move-result p1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/internal/x6;->c()I

    move-result p1

    .line 113
    :goto_1
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;I)Landroid/graphics/Paint;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->g:Landroid/graphics/Paint;

    .line 116
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->s()V

    .line 117
    invoke-virtual {v3}, Lcom/pspdfkit/internal/lq;->c()Lcom/pspdfkit/internal/me;

    .line 118
    new-instance p1, Lcom/pspdfkit/internal/o7;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/k6;->j()Landroid/content/Context;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/internal/k6$a;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/k6$a;-><init>(Lcom/pspdfkit/internal/k6;)V

    invoke-direct {p1, p2, v0}, Lcom/pspdfkit/internal/o7;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/o7$c;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->r:Lcom/pspdfkit/internal/o7;

    .line 197
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->x:Ljava/util/HashMap;

    .line 198
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->y:Ljava/util/Map;

    .line 515
    iput-boolean v2, p0, Lcom/pspdfkit/internal/k6;->C:Z

    .line 677
    iput v1, p0, Lcom/pspdfkit/internal/k6;->E:F

    .line 776
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->F:Landroid/graphics/RectF;

    .line 783
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->G:Landroid/graphics/Rect;

    return-void
.end method

.method static a(Lcom/pspdfkit/internal/k6;I)Landroid/graphics/Paint;
    .locals 1

    .line 490
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 491
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 492
    new-instance p0, Landroid/graphics/Paint;

    invoke-direct {p0}, Landroid/graphics/Paint;-><init>()V

    .line 493
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 494
    invoke-virtual {p0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-object p0
.end method

.method static a(Lcom/pspdfkit/internal/k6;Lcom/pspdfkit/internal/pt;Landroid/graphics/Matrix;Lcom/pspdfkit/utils/Size;)Lcom/pspdfkit/internal/o6;
    .locals 9

    .line 7816
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->v:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v6

    .line 7817
    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    .line 7818
    iget v2, p0, Lcom/pspdfkit/internal/k6;->c:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    .line 7819
    invoke-virtual/range {v1 .. v8}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(ILcom/pspdfkit/internal/pt;Landroid/graphics/Matrix;Lcom/pspdfkit/utils/Size;ZLcom/pspdfkit/internal/uq;Lcom/pspdfkit/internal/m7;)Lcom/pspdfkit/internal/o6;

    move-result-object p0

    return-object p0
.end method

.method private final a(FFLjava/util/Collection;)Lcom/pspdfkit/internal/qt;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF",
            "Ljava/util/Collection<",
            "+",
            "Lcom/pspdfkit/internal/qt;",
            ">;)",
            "Lcom/pspdfkit/internal/qt;"
        }
    .end annotation

    .line 7831
    invoke-interface {p3}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 7832
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_2

    return-object v1

    :cond_2
    const/high16 v2, 0x40800000    # 4.0f

    .line 7833
    invoke-static {v0, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result v0

    .line 7834
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, p1, p2, p1, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    neg-float p1, v0

    .line 7835
    invoke-virtual {v2, p1, p1}, Landroid/graphics/RectF;->inset(FF)V

    .line 7838
    iget p1, p0, Lcom/pspdfkit/internal/k6;->c:I

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "fingerect "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    new-array v0, p2, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.ContentEditing"

    invoke-static {v3, p1, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8427
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    move-object v0, p3

    check-cast v0, Lcom/pspdfkit/internal/qt;

    .line 8428
    invoke-virtual {v0}, Lcom/pspdfkit/internal/qt;->b()Lcom/pspdfkit/utils/PageRect;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v0}, Lcom/pspdfkit/internal/qt;->d()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "intersecting with "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, " ("

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ") "

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, p2, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8429
    invoke-virtual {v0}, Lcom/pspdfkit/internal/qt;->b()Lcom/pspdfkit/utils/PageRect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/graphics/RectF;->intersects(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v1, p3

    .line 9016
    :cond_4
    check-cast v1, Lcom/pspdfkit/internal/qt;

    return-object v1
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/k6;)Lcom/pspdfkit/internal/views/contentediting/a;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    return-object p0
.end method

.method private final a(Landroid/graphics/Canvas;Lcom/pspdfkit/internal/qt;)V
    .locals 8

    .line 9573
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->w:Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/qt;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/k6$b;

    if-eqz v0, :cond_3

    .line 9576
    invoke-virtual {v0}, Lcom/pspdfkit/internal/k6$b;->a()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/pspdfkit/internal/k6;->u:J

    const-string v5, "bitmap"

    const/4 v6, 0x0

    cmp-long v7, v1, v3

    if-nez v7, :cond_1

    .line 9578
    invoke-virtual {v0}, Lcom/pspdfkit/internal/k6$b;->b()Lcom/pspdfkit/internal/ip;

    move-result-object p2

    .line 9579
    iget-object p2, p2, Lcom/pspdfkit/internal/ip;->b:Landroid/graphics/Bitmap;

    if-eqz p2, :cond_0

    goto :goto_0

    .line 9580
    :cond_0
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p2, v6

    .line 9581
    :goto_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/k6$b;->b()Lcom/pspdfkit/internal/ip;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ip;->c()F

    move-result v1

    .line 9582
    invoke-virtual {v0}, Lcom/pspdfkit/internal/k6$b;->b()Lcom/pspdfkit/internal/ip;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ip;->d()F

    move-result v0

    .line 9583
    invoke-virtual {p1, p2, v1, v0, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 9584
    :cond_1
    invoke-virtual {p2}, Lcom/pspdfkit/internal/qt;->e()Lcom/pspdfkit/internal/bv;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/bv;->c()Lcom/pspdfkit/utils/PageRect;

    move-result-object p2

    .line 9585
    invoke-virtual {p2}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object p2

    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->G:Landroid/graphics/Rect;

    invoke-virtual {p2, v1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 9586
    invoke-virtual {v0}, Lcom/pspdfkit/internal/k6$b;->b()Lcom/pspdfkit/internal/ip;

    move-result-object p2

    .line 9587
    iget-object p2, p2, Lcom/pspdfkit/internal/ip;->b:Landroid/graphics/Bitmap;

    if-eqz p2, :cond_2

    goto :goto_1

    .line 9588
    :cond_2
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p2, v6

    .line 9589
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->G:Landroid/graphics/Rect;

    invoke-virtual {p1, p2, v6, v0, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_3
    :goto_2
    return-void
.end method

.method private static final a(Landroid/view/MotionEvent;Lcom/pspdfkit/internal/k6;Landroid/view/View;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 9598
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->setAction(I)V

    const-string v0, "event"

    .line 9599
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p2, p0}, Lcom/pspdfkit/internal/k6;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 9600
    invoke-virtual {p0}, Landroid/view/MotionEvent;->recycle()V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$g;)V
    .locals 1

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "<anonymous parameter 1>"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/jni/NativeContentEditingResult;Lcom/pspdfkit/utils/Size;)V
    .locals 1

    .line 3540
    new-instance v0, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda7;

    invoke-direct {v0, p2, p1}, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/internal/jni/NativeContentEditingResult;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->create(Lio/reactivex/rxjava3/core/SingleOnSubscribe;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 3548
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->o()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 3549
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 3550
    new-instance p2, Lcom/pspdfkit/internal/k6$c;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/k6$c;-><init>(Lcom/pspdfkit/internal/k6;)V

    new-instance v0, Lcom/pspdfkit/internal/k6$d;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/k6$d;-><init>(Lcom/pspdfkit/internal/k6;)V

    invoke-virtual {p1, p2, v0}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/k6;Landroid/graphics/RectF;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$pdfRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 498
    iget-object p0, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/RectF;)V

    :cond_0
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/k6;Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "event"

    .line 9596
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/k6;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 9597
    new-instance v0, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda0;

    invoke-direct {v0, p2, p0, p1}, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda0;-><init>(Landroid/view/MotionEvent;Lcom/pspdfkit/internal/k6;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/k6;Lcom/pspdfkit/internal/views/contentediting/a;)V
    .locals 3

    .line 2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextblockId()Ljava/util/UUID;

    move-result-object v0

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v1

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 6
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/k6;->B:Lcom/pspdfkit/internal/os;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 7
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->onFinishEditingContentBlock(Ljava/util/UUID;)V

    const/4 p1, 0x0

    .line 8
    invoke-direct {p0, v1, p1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/pt;Z)V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/k6;Lcom/pspdfkit/internal/views/contentediting/a;Landroid/view/MotionEvent;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/views/contentediting/a;Landroid/view/MotionEvent;)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/k6;Lio/reactivex/rxjava3/core/SingleEmitter;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 915
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    iget p0, p0, Lcom/pspdfkit/internal/k6;->c:I

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(I)Lcom/pspdfkit/internal/o6;

    move-result-object p0

    invoke-interface {p1, p0}, Lio/reactivex/rxjava3/core/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/k6;Ljava/lang/Boolean;)V
    .locals 0

    .line 12
    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->H:Ljava/lang/Boolean;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/k6;Ljava/util/List;Lcom/pspdfkit/internal/jni/NativeContentEditingResult;)V
    .locals 7

    .line 916
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 917
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    goto/16 :goto_4

    .line 918
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 919
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Lcom/pspdfkit/internal/rt;

    .line 920
    invoke-virtual {v5}, Lcom/pspdfkit/internal/rt;->f()Lcom/pspdfkit/internal/ut;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/internal/ut;->b()Lcom/pspdfkit/internal/bd;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/internal/bd;->a()F

    move-result v5

    const/4 v6, 0x0

    cmpg-float v5, v5, v6

    if-nez v5, :cond_3

    const/4 v3, 0x1

    :cond_3
    if-eqz v3, :cond_2

    .line 1775
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1776
    :cond_4
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 2633
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 2634
    move-object v5, v2

    check-cast v5, Lcom/pspdfkit/internal/rt;

    .line 2635
    invoke-virtual {v5}, Lcom/pspdfkit/internal/rt;->a()Ljava/util/UUID;

    move-result-object v5

    .line 3493
    invoke-virtual {p1, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 3494
    :cond_5
    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->x:Ljava/util/HashMap;

    .line 3495
    iget-object p1, p0, Lcom/pspdfkit/internal/k6;->B:Lcom/pspdfkit/internal/os;

    if-eqz p1, :cond_7

    .line 3496
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->e()Z

    move-result v0

    if-ne v0, v4, :cond_6

    const/4 v3, 0x1

    :cond_6
    if-eqz v3, :cond_7

    .line 3497
    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->d()V

    .line 3498
    :cond_7
    iget-object p1, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object p1

    goto :goto_3

    :cond_8
    move-object p1, v1

    :goto_3
    if-eqz p1, :cond_9

    .line 3499
    iget v0, p0, Lcom/pspdfkit/internal/k6;->c:I

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v1

    :cond_9
    if-nez v1, :cond_a

    goto :goto_4

    .line 3500
    :cond_a
    invoke-direct {p0, p2, v1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/jni/NativeContentEditingResult;Lcom/pspdfkit/utils/Size;)V

    :goto_4
    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/k6;Ljava/util/Map;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/k6;->a(Ljava/util/Map;)V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/k6;Ljava/util/UUID;)V
    .locals 0

    .line 11
    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->m:Ljava/util/UUID;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/k6;Ljava/util/UUID;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/k6;->a(Ljava/util/UUID;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/k6;Z)V
    .locals 0

    .line 10
    iput-boolean p1, p0, Lcom/pspdfkit/internal/k6;->D:Z

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/pt;Z)V
    .locals 12

    .line 3522
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 3523
    iget v2, p0, Lcom/pspdfkit/internal/k6;->c:I

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v0

    move-object v6, v0

    goto :goto_1

    :cond_1
    move-object v6, v1

    :goto_1
    if-nez v6, :cond_2

    return-void

    .line 3524
    :cond_2
    iget-wide v10, p0, Lcom/pspdfkit/internal/k6;->u:J

    if-eqz p2, :cond_3

    .line 3525
    iget-object p2, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->d()Landroid/content/Context;

    move-result-object p2

    .line 3526
    sget v0, Lcom/pspdfkit/R$color;->pspdf__gray_30:I

    invoke-static {p2, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p2

    .line 3527
    new-instance v1, Lcom/pspdfkit/internal/uq;

    invoke-direct {v1, p2}, Lcom/pspdfkit/internal/uq;-><init>(I)V

    .line 3528
    new-instance v0, Lcom/pspdfkit/internal/m7;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/m7;-><init>(I)V

    move-object v9, v0

    move-object v8, v1

    goto :goto_2

    :cond_3
    move-object v8, v1

    move-object v9, v8

    .line 3532
    :goto_2
    iget-object v5, p0, Lcom/pspdfkit/internal/k6;->t:Landroid/graphics/Matrix;

    .line 3534
    iget-object p2, p0, Lcom/pspdfkit/internal/k6;->v:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v7

    .line 3535
    iget-object v2, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    .line 3536
    iget v3, p0, Lcom/pspdfkit/internal/k6;->c:I

    move-object v4, p1

    .line 3537
    invoke-virtual/range {v2 .. v9}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(ILcom/pspdfkit/internal/pt;Landroid/graphics/Matrix;Lcom/pspdfkit/utils/Size;ZLcom/pspdfkit/internal/uq;Lcom/pspdfkit/internal/m7;)Lcom/pspdfkit/internal/o6;

    move-result-object p2

    .line 3538
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->w:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object p1

    new-instance v1, Lcom/pspdfkit/internal/k6$b;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/o6;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/ip;

    invoke-direct {v1, p2, v10, v11}, Lcom/pspdfkit/internal/k6$b;-><init>(Lcom/pspdfkit/internal/ip;J)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3539
    iget-object p1, p0, Lcom/pspdfkit/internal/k6;->B:Lcom/pspdfkit/internal/os;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    :cond_4
    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/views/contentediting/a;Landroid/view/MotionEvent;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 9594
    :cond_0
    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object p2

    .line 9595
    new-instance v0, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda8;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/k6;Landroid/view/View;Landroid/view/MotionEvent;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private static final a(Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/internal/jni/NativeContentEditingResult;Lio/reactivex/rxjava3/core/SingleEmitter;)V
    .locals 3

    const-string v0, "$pageSize"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$nativeContentEditingResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emitter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3551
    invoke-static {p0}, Lcom/pspdfkit/internal/ad$a;->a(Lcom/pspdfkit/utils/Size;)Lcom/pspdfkit/internal/q6;

    move-result-object p0

    .line 3552
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/q6;->b(Lcom/pspdfkit/internal/jni/NativeContentEditingResult;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 4407
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 4408
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/pspdfkit/internal/pt;

    .line 4409
    invoke-virtual {v1}, Lcom/pspdfkit/internal/pt;->i()Lcom/pspdfkit/internal/st;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/st;->c()Lcom/pspdfkit/internal/bd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/bd;->a()F

    move-result v1

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_0

    .line 5264
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5265
    :cond_2
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 6121
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 6122
    move-object v1, v0

    check-cast v1, Lcom/pspdfkit/internal/pt;

    .line 6123
    invoke-virtual {v1}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object v1

    .line 6980
    invoke-virtual {p0, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 6981
    :cond_3
    invoke-interface {p2, p0}, Lio/reactivex/rxjava3/core/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method private final a(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/util/UUID;",
            "Lcom/pspdfkit/internal/pt;",
            ">;)V"
        }
    .end annotation

    .line 6982
    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->y:Ljava/util/Map;

    .line 6983
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    .line 7805
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/pt;

    .line 7806
    invoke-virtual {v0}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v0}, Lcom/pspdfkit/internal/qt;->d()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " - "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.ContentEditing"

    invoke-static {v3, v2, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7807
    invoke-virtual {v0}, Lcom/pspdfkit/internal/qt;->b()Lcom/pspdfkit/utils/PageRect;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->t:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/utils/PageRect;->updateScreenRect(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 7809
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/k6;->x:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/HashMap;->clear()V

    .line 7812
    iget-object p1, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->e()Z

    move-result p1

    if-ne p1, v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    if-eqz v1, :cond_2

    .line 7814
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->u()V

    :cond_2
    return-void
.end method

.method private final a(Ljava/util/UUID;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 12

    const/4 v0, 0x0

    .line 9018
    iput-object v0, p0, Lcom/pspdfkit/internal/k6;->n:Ljava/util/UUID;

    .line 9022
    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->w:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 9024
    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->n:Ljava/util/UUID;

    .line 9025
    iput-object p2, p0, Lcom/pspdfkit/internal/k6;->o:Ljava/lang/Integer;

    .line 9026
    iput-object p3, p0, Lcom/pspdfkit/internal/k6;->p:Ljava/lang/Integer;

    goto/16 :goto_2

    .line 9028
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->y:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/pt;

    if-nez v1, :cond_1

    return-void

    .line 9029
    :cond_1
    iget-object v2, p0, Lcom/pspdfkit/internal/k6;->y:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/pt;

    if-eqz v2, :cond_4

    .line 9030
    iget-object v3, p0, Lcom/pspdfkit/internal/k6;->w:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    goto/16 :goto_0

    .line 9031
    :cond_2
    iget-object v11, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    if-nez v11, :cond_3

    goto/16 :goto_0

    .line 9033
    :cond_3
    new-instance v0, Lcom/pspdfkit/internal/views/contentediting/a;

    .line 9034
    iget-object v3, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->d()Landroid/content/Context;

    move-result-object v4

    .line 9035
    iget-object v5, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    iget v6, p0, Lcom/pspdfkit/internal/k6;->c:I

    invoke-virtual {v5}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v9

    iget v10, p0, Lcom/pspdfkit/internal/k6;->E:F

    move-object v3, v0

    move-object v7, v2

    move-object v8, p0

    invoke-direct/range {v3 .. v10}, Lcom/pspdfkit/internal/views/contentediting/a;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;ILcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/views/contentediting/a$a;Lcom/pspdfkit/internal/fl;F)V

    .line 9037
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/views/annotations/d;->setEditTextViewListener(Lcom/pspdfkit/internal/views/annotations/d$a;)V

    .line 9038
    invoke-virtual {v2}, Lcom/pspdfkit/internal/pt;->e()Lcom/pspdfkit/internal/bv;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/bv;->c()Lcom/pspdfkit/utils/PageRect;

    move-result-object v3

    .line 9039
    invoke-virtual {v3}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0, v3}, Landroidx/appcompat/widget/AppCompatEditText;->setX(F)V

    .line 9040
    invoke-virtual {v2}, Lcom/pspdfkit/internal/pt;->e()Lcom/pspdfkit/internal/bv;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/bv;->c()Lcom/pspdfkit/utils/PageRect;

    move-result-object v3

    .line 9041
    invoke-virtual {v3}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/RectF;->right:F

    invoke-virtual {v0, v3}, Landroidx/appcompat/widget/AppCompatEditText;->setY(F)V

    .line 9042
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    .line 9043
    invoke-virtual {v2}, Lcom/pspdfkit/internal/pt;->e()Lcom/pspdfkit/internal/bv;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/bv;->c()Lcom/pspdfkit/utils/PageRect;

    move-result-object v4

    .line 9044
    invoke-virtual {v4}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    .line 9045
    invoke-virtual {v2}, Lcom/pspdfkit/internal/pt;->e()Lcom/pspdfkit/internal/bv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/bv;->c()Lcom/pspdfkit/utils/PageRect;

    move-result-object v2

    .line 9046
    invoke-virtual {v2}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    .line 9047
    invoke-direct {v3, v4, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v2, 0x0

    .line 9051
    invoke-virtual {v0, v2}, Landroidx/appcompat/widget/AppCompatEditText;->setX(F)V

    .line 9052
    invoke-virtual {v0, v2}, Landroidx/appcompat/widget/AppCompatEditText;->setY(F)V

    .line 9053
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 9057
    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_4
    :goto_0
    if-eqz v0, :cond_7

    .line 9058
    iget-object v2, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->b()Ljava/util/ArrayList;

    move-result-object v2

    .line 9543
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/k6;

    .line 9544
    invoke-static {v3, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 9545
    invoke-direct {v3}, Lcom/pspdfkit/internal/k6;->i()V

    goto :goto_1

    .line 9546
    :cond_6
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/d;->c()V

    .line 9548
    iput-object v0, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    .line 9549
    iget-object v2, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {v2, p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->onStartEditingContentBlock(Ljava/util/UUID;)V

    const/4 v2, 0x1

    .line 9550
    invoke-virtual {p0, p2, p3, v2}, Lcom/pspdfkit/internal/k6;->a(Ljava/lang/Integer;Ljava/lang/Integer;Z)Z

    move-result p2

    if-nez p2, :cond_7

    .line 9551
    iget-object v2, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionStart()I

    move-result v4

    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v5

    invoke-virtual {v1}, Lcom/pspdfkit/internal/pt;->f()Lcom/pspdfkit/internal/jt;

    move-result-object v6

    const/4 v7, 0x1

    move-object v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->onContentSelectionChange(Ljava/util/UUID;IILcom/pspdfkit/internal/jt;Z)V

    :cond_7
    :goto_2
    return-void
.end method

.method private static final a(Lkotlin/jvm/functions/Function0;)V
    .locals 1

    const-string v0, "$tmp0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9572
    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method

.method private final a(Z)V
    .locals 3

    .line 7820
    iget-boolean v0, p0, Lcom/pspdfkit/internal/k6;->C:Z

    if-ne p1, v0, :cond_0

    return-void

    .line 7822
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/k6;->C:Z

    .line 7824
    iget-object p1, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    if-eqz p1, :cond_1

    .line 7825
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    iget-boolean v1, p0, Lcom/pspdfkit/internal/k6;->C:Z

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/dm$e;->a(Z)V

    .line 7828
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    iget v1, p0, Lcom/pspdfkit/internal/k6;->c:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->invalidateCacheForPage(I)V

    .line 7830
    new-instance v0, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda2;

    invoke-direct {v0}, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda2;-><init>()V

    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/dm;->a(ZLcom/pspdfkit/internal/im$c;)V

    :cond_1
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    const-string v0, "child"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9590
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object p1

    .line 9591
    invoke-virtual {p0}, Landroid/view/View;->getX()F

    move-result v0

    neg-float v0, v0

    invoke-virtual {p0}, Landroid/view/View;->getY()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 9592
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p0

    .line 9593
    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    return p0
.end method

.method private static b(Landroid/graphics/Matrix;)J
    .locals 2

    .line 2
    invoke-virtual {p0}, Landroid/graphics/Matrix;->toShortString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/k6;)Ljava/util/UUID;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->m()Ljava/util/UUID;

    move-result-object p0

    return-object p0
.end method

.method static synthetic b(Lcom/pspdfkit/internal/k6;Ljava/util/UUID;)V
    .locals 1

    const/4 v0, 0x0

    .line 6
    invoke-direct {p0, p1, v0, v0}, Lcom/pspdfkit/internal/k6;->a(Ljava/util/UUID;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/k6;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/pspdfkit/internal/k6;->D:Z

    return p0
.end method

.method public static final synthetic d(Lcom/pspdfkit/internal/k6;)Ljava/util/Map;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->n()Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/pspdfkit/internal/k6;)Ljava/lang/Integer;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/k6;->p:Ljava/lang/Integer;

    return-object p0
.end method

.method public static final synthetic f(Lcom/pspdfkit/internal/k6;)Ljava/lang/Integer;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/k6;->o:Ljava/lang/Integer;

    return-object p0
.end method

.method public static final synthetic g(Lcom/pspdfkit/internal/k6;)Ljava/util/UUID;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/k6;->n:Ljava/util/UUID;

    return-object p0
.end method

.method public static final synthetic h(Lcom/pspdfkit/internal/k6;)Ljava/util/HashMap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/k6;->w:Ljava/util/HashMap;

    return-object p0
.end method

.method public static final synthetic i(Lcom/pspdfkit/internal/k6;)Lcom/pspdfkit/internal/os;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/k6;->B:Lcom/pspdfkit/internal/os;

    return-object p0
.end method

.method private final i()V
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    if-nez v0, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextblockId()Ljava/util/UUID;

    move-result-object v1

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v2

    .line 5
    iget-object v3, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    if-eqz v3, :cond_1

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->B:Lcom/pspdfkit/internal/os;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 7
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->onFinishEditingContentBlock(Ljava/util/UUID;)V

    const/4 v0, 0x0

    .line 8
    invoke-direct {p0, v2, v0}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/pt;Z)V

    :goto_0
    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    return-void
.end method

.method static j(Lcom/pspdfkit/internal/k6;)V
    .locals 1

    .line 2
    iget-object p0, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->b()Ljava/util/ArrayList;

    move-result-object p0

    .line 488
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/k6;

    .line 489
    invoke-direct {v0}, Lcom/pspdfkit/internal/k6;->i()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static final k(Lcom/pspdfkit/internal/k6;)V
    .locals 4

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/k6;->z:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    iget v0, p0, Lcom/pspdfkit/internal/k6;->c:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Finished textblock rendering page "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.ContentEditing"

    invoke-static {v3, v0, v2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    iget-boolean v0, p0, Lcom/pspdfkit/internal/k6;->A:Z

    if-eqz v0, :cond_0

    .line 6
    iput-boolean v1, p0, Lcom/pspdfkit/internal/k6;->A:Z

    .line 8
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->u()V

    :cond_0
    return-void
.end method

.method private static final l(Lcom/pspdfkit/internal/k6;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/k6;->z:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private final m()Ljava/util/UUID;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method private static final m(Lcom/pspdfkit/internal/k6;)V
    .locals 4

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget v0, p0, Lcom/pspdfkit/internal/k6;->c:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OnComplete textblock rendering page "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.ContentEditing"

    invoke-static {v3, v0, v2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/k6;->a(Z)V

    return-void
.end method

.method private final n()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/util/UUID;",
            "Lcom/pspdfkit/internal/pt;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    iget v1, p0, Lcom/pspdfkit/internal/k6;->c:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->b(I)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private final o()Lio/reactivex/rxjava3/core/Scheduler;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm;->e()Z

    move-result v1

    if-ne v1, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_2

    const/16 v1, 0xf

    goto :goto_2

    :cond_2
    const/4 v1, 0x3

    .line 3
    :goto_2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    .line 5
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->newThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    const-string v0, "newThread()"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_4
    return-object v1
.end method

.method private final r()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->y:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->x:Ljava/util/HashMap;

    .line 3
    :cond_2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextblockId()Ljava/util/UUID;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/qt;

    if-nez v1, :cond_3

    return-void

    .line 4
    :cond_3
    invoke-virtual {v1}, Lcom/pspdfkit/internal/qt;->e()Lcom/pspdfkit/internal/bv;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/bv;->c()Lcom/pspdfkit/utils/PageRect;

    move-result-object v1

    .line 5
    invoke-virtual {v1}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v1

    const-string v2, "textBlock.pageRect.screenRect"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    iget v2, v1, Landroid/graphics/RectF;->left:F

    .line 7
    invoke-virtual {v0}, Landroidx/appcompat/widget/AppCompatEditText;->getX()F

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    cmpg-float v3, v3, v2

    if-nez v3, :cond_4

    const/4 v3, 0x1

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    :goto_1
    if-nez v3, :cond_5

    .line 8
    invoke-virtual {v0, v2}, Landroidx/appcompat/widget/AppCompatEditText;->setX(F)V

    .line 11
    :cond_5
    iget v2, v1, Landroid/graphics/RectF;->top:F

    .line 12
    invoke-virtual {v0}, Landroidx/appcompat/widget/AppCompatEditText;->getY()F

    move-result v3

    cmpg-float v3, v3, v2

    if-nez v3, :cond_6

    goto :goto_2

    :cond_6
    const/4 v4, 0x0

    :goto_2
    if-nez v4, :cond_7

    .line 13
    invoke-virtual {v0, v2}, Landroidx/appcompat/widget/AppCompatEditText;->setY(F)V

    .line 20
    :cond_7
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    const v3, 0x3f866666    # 1.05f

    mul-float v2, v2, v3

    invoke-static {v2}, Lkotlin/math/MathKt;->roundToInt(F)I

    move-result v2

    .line 21
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    mul-float v1, v1, v3

    invoke-static {v1}, Lkotlin/math/MathKt;->roundToInt(F)I

    move-result v1

    .line 23
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v2, v3, :cond_8

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v1, v3, :cond_9

    .line 24
    :cond_8
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 25
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iput v2, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 26
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 27
    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_9
    return-void
.end method

.method private final s()V
    .locals 5

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/k6;->i:F

    iget v1, p0, Lcom/pspdfkit/internal/k6;->E:F

    mul-float v0, v0, v1

    iput v0, p0, Lcom/pspdfkit/internal/k6;->j:F

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->e:Landroid/graphics/Paint;

    iget v2, p0, Lcom/pspdfkit/internal/k6;->f:F

    mul-float v2, v2, v1

    .line 3
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/internal/k6;->d:Ljava/util/List;

    .line 977
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 978
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 979
    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v4

    mul-float v4, v4, v1

    .line 980
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    .line 1954
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1955
    :cond_0
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->toFloatArray(Ljava/util/Collection;)[F

    move-result-object v1

    .line 1956
    new-instance v2, Landroid/graphics/DashPathEffect;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1957
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->g:Landroid/graphics/Paint;

    iget v1, p0, Lcom/pspdfkit/internal/k6;->h:F

    .line 1958
    iget v2, p0, Lcom/pspdfkit/internal/k6;->E:F

    mul-float v1, v1, v2

    .line 1959
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method

.method private final t()V
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/k6;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->create(Lio/reactivex/rxjava3/core/SingleOnSubscribe;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->o()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 4
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 5
    new-instance v1, Lcom/pspdfkit/internal/k6$g;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/k6$g;-><init>(Lcom/pspdfkit/internal/k6;)V

    new-instance v2, Lcom/pspdfkit/internal/k6$h;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/k6$h;-><init>(Lcom/pspdfkit/internal/k6;)V

    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private final u()V
    .locals 13

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 2
    iget v2, p0, Lcom/pspdfkit/internal/k6;->c:I

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    if-nez v0, :cond_2

    return-void

    .line 3
    :cond_2
    iget-object v2, p0, Lcom/pspdfkit/internal/k6;->t:Landroid/graphics/Matrix;

    .line 4
    iget-wide v3, p0, Lcom/pspdfkit/internal/k6;->u:J

    .line 7
    iget-object v5, p0, Lcom/pspdfkit/internal/k6;->z:Lio/reactivex/rxjava3/disposables/Disposable;

    const/4 v6, 0x1

    if-eqz v5, :cond_3

    .line 9
    invoke-interface {v5}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v7

    if-nez v7, :cond_3

    .line 10
    iput-boolean v6, p0, Lcom/pspdfkit/internal/k6;->A:Z

    .line 12
    invoke-static {v5}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    return-void

    .line 13
    :cond_3
    iget-object v5, p0, Lcom/pspdfkit/internal/k6;->y:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_4

    goto :goto_2

    :cond_4
    move-object v5, v1

    :goto_2
    const/4 v7, 0x0

    if-eqz v5, :cond_9

    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v5

    if-nez v5, :cond_5

    goto :goto_5

    .line 17
    :cond_5
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->m()Ljava/util/UUID;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 18
    iget-object v9, p0, Lcom/pspdfkit/internal/k6;->y:Ljava/util/Map;

    invoke-interface {v9, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/pspdfkit/internal/pt;

    if-eqz v8, :cond_6

    .line 19
    invoke-interface {v5, v8}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 20
    invoke-interface {v5, v7, v8}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 672
    :cond_6
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 673
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    move-object v10, v9

    check-cast v10, Lcom/pspdfkit/internal/pt;

    .line 674
    iget-object v11, p0, Lcom/pspdfkit/internal/k6;->w:Ljava/util/HashMap;

    invoke-virtual {v10}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/pspdfkit/internal/k6$b;

    if-eqz v10, :cond_8

    invoke-virtual {v10}, Lcom/pspdfkit/internal/k6$b;->a()J

    move-result-wide v10

    cmp-long v12, v10, v3

    if-nez v12, :cond_8

    const/4 v10, 0x1

    goto :goto_4

    :cond_8
    const/4 v10, 0x0

    :goto_4
    xor-int/2addr v10, v6

    if-eqz v10, :cond_7

    .line 1323
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1324
    :cond_9
    :goto_5
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v8

    .line 1325
    :cond_a
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_b

    move-object v1, v8

    :cond_b
    if-nez v1, :cond_c

    return-void

    .line 1327
    :cond_c
    iget v5, p0, Lcom/pspdfkit/internal/k6;->c:I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Starting textblock rendering page "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-array v6, v7, [Ljava/lang/Object;

    const-string v7, "PSPDFKit.ContentEditing"

    invoke-static {v7, v5, v6}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1329
    invoke-static {v1}, Lio/reactivex/rxjava3/core/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v1

    .line 1330
    new-instance v5, Lcom/pspdfkit/internal/k6$i;

    invoke-direct {v5, p0, v2, v0}, Lcom/pspdfkit/internal/k6$i;-><init>(Lcom/pspdfkit/internal/k6;Landroid/graphics/Matrix;Lcom/pspdfkit/utils/Size;)V

    invoke-virtual {v1, v5}, Lio/reactivex/rxjava3/core/Observable;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 1333
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->o()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 1334
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 1335
    new-instance v1, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/k6;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 1345
    new-instance v1, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/k6;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->doOnDispose(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 1349
    new-instance v1, Lcom/pspdfkit/internal/k6$j;

    invoke-direct {v1, p0, v3, v4}, Lcom/pspdfkit/internal/k6$j;-><init>(Lcom/pspdfkit/internal/k6;J)V

    new-instance v2, Lcom/pspdfkit/internal/k6$k;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/k6$k;-><init>(Lcom/pspdfkit/internal/k6;)V

    new-instance v3, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda6;

    invoke-direct {v3, p0}, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/k6;)V

    invoke-virtual {v0, v1, v2, v3}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    .line 1350
    iput-object v0, p0, Lcom/pspdfkit/internal/k6;->z:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/16 v0, 0x17

    return v0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 4

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 499
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->y:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->x:Ljava/util/HashMap;

    .line 500
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->m()Ljava/util/UUID;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/qt;

    if-eqz v0, :cond_3

    .line 501
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/k6;->a(Landroid/graphics/Canvas;Lcom/pspdfkit/internal/qt;)V

    .line 502
    invoke-virtual {v0}, Lcom/pspdfkit/internal/qt;->e()Lcom/pspdfkit/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/bv;->c()Lcom/pspdfkit/utils/PageRect;

    move-result-object v0

    .line 503
    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v0

    const-string v1, "it.pageRect.screenRect"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 504
    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->F:Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 505
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->F:Landroid/graphics/RectF;

    iget v1, p0, Lcom/pspdfkit/internal/k6;->j:F

    neg-float v1, v1

    invoke-virtual {v0, v1, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 506
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->F:Landroid/graphics/RectF;

    .line 507
    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 509
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1a

    if-lt v1, v3, :cond_2

    .line 510
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipOutRect(Landroid/graphics/RectF;)Z

    goto :goto_1

    .line 513
    :cond_2
    sget-object v1, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;Landroid/graphics/Region$Op;)Z

    .line 514
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->y:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    move-object v2, v0

    :cond_4
    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/pspdfkit/internal/k6;->x:Ljava/util/HashMap;

    .line 515
    :cond_5
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 890
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/qt;

    .line 891
    invoke-virtual {v1}, Lcom/pspdfkit/internal/qt;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->m()Ljava/util/UUID;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    goto :goto_2

    .line 895
    :cond_6
    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/k6;->a(Landroid/graphics/Canvas;Lcom/pspdfkit/internal/qt;)V

    .line 896
    invoke-virtual {v1}, Lcom/pspdfkit/internal/qt;->e()Lcom/pspdfkit/internal/bv;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/bv;->c()Lcom/pspdfkit/utils/PageRect;

    move-result-object v1

    .line 897
    invoke-virtual {v1}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/k6;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_2

    :cond_7
    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 6

    const-string v0, "pageToScreenMatrix"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->t:Landroid/graphics/Matrix;

    .line 16
    invoke-virtual {p1}, Landroid/graphics/Matrix;->toShortString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/String;)J

    move-result-wide v0

    .line 17
    iput-wide v0, p0, Lcom/pspdfkit/internal/k6;->u:J

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->t:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->s:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->s:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 20
    iput v0, p0, Lcom/pspdfkit/internal/k6;->E:F

    .line 21
    iget-object v2, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    if-nez v2, :cond_0

    return-void

    .line 27
    :cond_0
    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->getZoomScale()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ContentEditingHandler MatrixScale = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, ", PageScale = "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.ContentEditing"

    .line 28
    invoke-static {v3, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->s()V

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->y:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->x:Ljava/util/HashMap;

    .line 35
    :cond_2
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 477
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/qt;

    .line 478
    invoke-virtual {v1}, Lcom/pspdfkit/internal/qt;->b()Lcom/pspdfkit/utils/PageRect;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/pspdfkit/utils/PageRect;->updateScreenRect(Landroid/graphics/Matrix;)V

    .line 480
    invoke-virtual {v1}, Lcom/pspdfkit/internal/qt;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->m()Ljava/util/UUID;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 481
    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    if-eqz v1, :cond_4

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->getZoomScale()F

    move-result v3

    invoke-virtual {v1, v3, p1}, Lcom/pspdfkit/internal/views/contentediting/a;->a(FLandroid/graphics/Matrix;)V

    .line 482
    :cond_4
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->r()V

    goto :goto_1

    .line 486
    :cond_5
    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->e()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 487
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->u()V

    .line 489
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/internal/k6;->B:Lcom/pspdfkit/internal/os;

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    :cond_7
    return-void
.end method

.method public final a(Landroid/graphics/RectF;)V
    .locals 2

    const-string v0, "pdfRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 495
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    if-nez v0, :cond_0

    return-void

    .line 497
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda9;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/internal/k6;Landroid/graphics/RectF;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/os;)V
    .locals 3

    const-string v0, "specialModeView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 898
    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->B:Lcom/pspdfkit/internal/os;

    .line 899
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->k:Lcom/pspdfkit/internal/lq;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/lq;->b()V

    .line 900
    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object p1

    .line 901
    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 902
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_1

    .line 903
    iget v0, p0, Lcom/pspdfkit/internal/k6;->c:I

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    .line 904
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->n()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/ad$a;->a(Ljava/util/List;Lcom/pspdfkit/utils/Size;)V

    .line 907
    :cond_2
    iget v0, p0, Lcom/pspdfkit/internal/k6;->c:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering content editing mode page "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.ContentEditing"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 909
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object p1

    const-string v1, "pageLayout.state.document"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/k6;)V

    .line 911
    iget-object p1, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 914
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->t()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/av;Z)V
    .locals 2

    const-string v0, "textBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "updateInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3501
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 3502
    iget v1, p0, Lcom/pspdfkit/internal/k6;->c:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    return-void

    .line 3503
    :cond_2
    invoke-virtual {p1, p2, v1}, Lcom/pspdfkit/internal/pt;->a(Lcom/pspdfkit/internal/av;Lcom/pspdfkit/utils/Size;)V

    .line 3504
    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt;->e()Lcom/pspdfkit/internal/bv;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/bv;->c()Lcom/pspdfkit/utils/PageRect;

    move-result-object p2

    .line 3505
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->t:Landroid/graphics/Matrix;

    invoke-virtual {p2, v0}, Lcom/pspdfkit/utils/PageRect;->updateScreenRect(Landroid/graphics/Matrix;)V

    .line 3507
    iget-object p2, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    iget v0, p0, Lcom/pspdfkit/internal/k6;->c:I

    invoke-virtual {p2, v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(ILcom/pspdfkit/internal/pt;)V

    .line 3509
    iget-object p2, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    if-eqz p2, :cond_4

    .line 3510
    invoke-virtual {p2}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextblockId()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3511
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->r()V

    if-nez p3, :cond_4

    const/4 p3, 0x1

    .line 3513
    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/views/contentediting/a;->c(Z)V

    goto :goto_1

    .line 3517
    :cond_3
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->i()V

    .line 3521
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k6;->q()Z

    move-result p2

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/pt;Z)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/jt;IIZ)V
    .locals 7

    const-string v0, "textBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "styleInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7815
    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object v2

    move v3, p3

    move v4, p4

    move-object v5, p2

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->onContentSelectionChange(Ljava/util/UUID;IILcom/pspdfkit/internal/jt;Z)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9017
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->r:Lcom/pspdfkit/internal/o7;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/o7;->a(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public final a(Ljava/lang/Integer;Ljava/lang/Integer;Z)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 9552
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    if-eqz v1, :cond_3

    if-eqz p2, :cond_1

    .line 9555
    new-instance v0, Lcom/pspdfkit/internal/k6$e;

    invoke-direct {v0, v1, p1, p2}, Lcom/pspdfkit/internal/k6$e;-><init>(Lcom/pspdfkit/internal/views/contentediting/a;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/pspdfkit/internal/k6$f;

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/internal/k6$f;-><init>(Lcom/pspdfkit/internal/views/contentediting/a;Ljava/lang/Integer;)V

    :goto_0
    if-eqz p3, :cond_2

    .line 9568
    new-instance p1, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda1;

    invoke-direct {p1, v0}, Lcom/pspdfkit/internal/k6$$ExternalSyntheticLambda1;-><init>(Lkotlin/jvm/functions/Function0;)V

    const-wide/16 p2, 0xfa

    invoke-virtual {v1, p1, p2, p3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 9571
    :cond_2
    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :goto_1
    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method public final b(Landroid/view/MotionEvent;)Lcom/pspdfkit/internal/qt;
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->y:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->x:Ljava/util/HashMap;

    .line 5
    :cond_1
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/pspdfkit/internal/k6;->a(FFLjava/util/Collection;)Lcom/pspdfkit/internal/qt;

    move-result-object p1

    return-object p1
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final d()Z
    .locals 3

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/k6;->c:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Leaving content editing mode page "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.ContentEditing"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/k6;->B:Lcom/pspdfkit/internal/os;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->z:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->i()V

    const/4 v1, 0x1

    .line 6
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/k6;->a(Z)V

    .line 7
    iput-object v0, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/ui/PdfFragment;->removeDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->a(Lcom/pspdfkit/internal/k6;)V

    return v1
.end method

.method public final f()Z
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->m:Ljava/util/UUID;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->m()Ljava/util/UUID;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/k6;->m:Ljava/util/UUID;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final g()V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final getPageIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/k6;->c:I

    return v0
.end method

.method public final h()V
    .locals 3

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/k6;->c:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Leaving content editing mode page "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.ContentEditing"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->i()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->b(Lcom/pspdfkit/internal/k6;)V

    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/pspdfkit/internal/k6;->l:Lcom/pspdfkit/internal/dm;

    return-void
.end method

.method public final j()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->d()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lcom/pspdfkit/internal/views/contentediting/a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    return-object v0
.end method

.method public final l()Lcom/pspdfkit/internal/jt;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/pt;->f()Lcom/pspdfkit/internal/jt;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method public final onDisplayPropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 1

    const-string v0, "inspector"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/pt;Z)V

    :cond_1
    return-void
.end method

.method public synthetic onDocumentClick()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentClick(Lcom/pspdfkit/listeners/DocumentListener;)Z

    move-result v0

    return v0
.end method

.method public synthetic onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentLoadFailed(Lcom/pspdfkit/listeners/DocumentListener;Ljava/lang/Throwable;)V

    return-void
.end method

.method public synthetic onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentLoaded(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public synthetic onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSave(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result p1

    return p1
.end method

.method public synthetic onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSaveCancelled(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public synthetic onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSaveFailed(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V

    return-void
.end method

.method public synthetic onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSaved(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public synthetic onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentZoomed(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;IF)V

    return-void
.end method

.method public final onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    const-string p2, "document"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->i()V

    return-void
.end method

.method public synthetic onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onPageClick(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    return p1
.end method

.method public synthetic onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onPageUpdated(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;I)V

    return-void
.end method

.method public final onPreparePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 1

    const-string v0, "inspector"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final onRemovePropertyInspector(Lcom/pspdfkit/ui/inspector/PropertyInspector;)V
    .locals 3

    const-string v0, "inspector"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextBlock()Lcom/pspdfkit/internal/pt;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_2

    const/4 v2, 0x0

    .line 2
    invoke-direct {p0, v0, v2}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/pt;Z)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->H:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    .line 8
    instance-of v0, p1, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$ContentEditingPropertyInspector;

    if-eqz v0, :cond_2

    .line 11
    check-cast p1, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$ContentEditingPropertyInspector;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$ContentEditingPropertyInspector;->wasClosedByCloseButton()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/pspdfkit/internal/k6;->i()V

    goto :goto_1

    .line 12
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/contentediting/DefaultContentEditingInspectorController$ContentEditingPropertyInspector;->wasClosedByBackButton()Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/pspdfkit/internal/k6;->H:Ljava/lang/Boolean;

    .line 13
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/k6;->H:Ljava/lang/Boolean;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 16
    :cond_3
    iput-object v1, p0, Lcom/pspdfkit/internal/k6;->H:Ljava/lang/Boolean;

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)Z
    .locals 5

    const-string v0, "savedInstanceState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "com.pspdfkit.internal.ContentModeHandler.EDITED_TEXTBLOCK_ID"

    .line 1
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_3

    const-string v2, "com.pspdfkit.internal.ContentModeHandler.EDITED_TEXTBLOCK_SELECTION_START"

    .line 4
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v1

    :goto_1
    const-string v3, "com.pspdfkit.internal.ContentModeHandler.EDITED_TEXTBLOCK_SELECTION_END"

    .line 6
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 7
    :cond_2
    invoke-direct {p0, v0, v2, v1}, Lcom/pspdfkit/internal/k6;->a(Ljava/util/UUID;Ljava/lang/Integer;Ljava/lang/Integer;)V

    const/4 p1, 0x1

    return p1

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextblockId()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.pspdfkit.internal.ContentModeHandler.EDITED_TEXTBLOCK_ID"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionStart()I

    move-result v1

    const-string v2, "com.pspdfkit.internal.ContentModeHandler.EDITED_TEXTBLOCK_SELECTION_START"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4
    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v0

    const-string v1, "com.pspdfkit.internal.ContentModeHandler.EDITED_TEXTBLOCK_SELECTION_END"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public final p()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->q:Lcom/pspdfkit/internal/views/contentediting/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final q()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6;->b:Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;->c()Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/ContentEditingInspectorController;->isContentEditingInspectorVisible()Z

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

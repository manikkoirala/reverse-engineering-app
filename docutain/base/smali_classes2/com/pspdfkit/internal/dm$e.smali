.class public final Lcom/pspdfkit/internal/dm$e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/dm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "e"
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/zf;

.field private b:Lcom/pspdfkit/utils/Size;

.field private final c:Landroid/graphics/RectF;

.field private final d:I

.field private final e:Ljava/util/ArrayList;

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private h:F

.field private final i:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

.field private j:Z

.field private k:Z


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/dm$e;)Lcom/pspdfkit/internal/zf;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/dm$e;->a:Lcom/pspdfkit/internal/zf;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/dm$e;)Lcom/pspdfkit/utils/Size;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/dm$e;->b:Lcom/pspdfkit/utils/Size;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/dm$e;)Landroid/graphics/RectF;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/dm$e;->c:Landroid/graphics/RectF;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/dm$e;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/dm$e;->d:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/internal/dm$e;)F
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/dm$e;->h:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fputb(Lcom/pspdfkit/internal/dm$e;Lcom/pspdfkit/utils/Size;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/dm$e;->b:Lcom/pspdfkit/utils/Size;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputh(Lcom/pspdfkit/internal/dm$e;F)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/dm$e;->h:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputj(Lcom/pspdfkit/internal/dm$e;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/dm$e;->j:Z

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/utils/Size;IFLcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/dm$e;->j:Z

    .line 5
    iput-boolean v0, p0, Lcom/pspdfkit/internal/dm$e;->k:Z

    .line 25
    iput-object p1, p0, Lcom/pspdfkit/internal/dm$e;->a:Lcom/pspdfkit/internal/zf;

    .line 26
    iput-object p2, p0, Lcom/pspdfkit/internal/dm$e;->b:Lcom/pspdfkit/utils/Size;

    .line 27
    iput p3, p0, Lcom/pspdfkit/internal/dm$e;->d:I

    .line 28
    iput p4, p0, Lcom/pspdfkit/internal/dm$e;->h:F

    .line 30
    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    .line 31
    new-instance p2, Landroid/graphics/RectF;

    iget p3, p1, Lcom/pspdfkit/utils/Size;->height:F

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    const/4 p4, 0x0

    invoke-direct {p2, p4, p3, p1, p4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object p2, p0, Lcom/pspdfkit/internal/dm$e;->c:Landroid/graphics/RectF;

    .line 34
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/dm$e;->f:Ljava/util/ArrayList;

    .line 37
    new-instance p1, Ljava/util/ArrayList;

    invoke-virtual {p5}, Lcom/pspdfkit/configuration/PdfConfiguration;->getExcludedAnnotationTypes()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/dm$e;->e:Ljava/util/ArrayList;

    .line 40
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/dm$e;->g:Ljava/util/ArrayList;

    .line 43
    iput-object p6, p0, Lcom/pspdfkit/internal/dm$e;->i:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/zf;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->a:Lcom/pspdfkit/internal/zf;

    return-object v0
.end method

.method final a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method final a(Lcom/pspdfkit/annotations/AnnotationType;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 6
    iput-boolean p1, p0, Lcom/pspdfkit/internal/dm$e;->k:Z

    return-void
.end method

.method final b(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method final b(Lcom/pspdfkit/annotations/AnnotationType;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final b()Z
    .locals 1

    .line 4
    iget-boolean v0, p0, Lcom/pspdfkit/internal/dm$e;->k:Z

    return v0
.end method

.method public final c()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dm$e;->d:I

    return v0
.end method

.method final c(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->g:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->f:Ljava/util/ArrayList;

    .line 3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final d()Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->i:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    return-object v0
.end method

.method public final e()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final f()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 4
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/dm$e;->f:Ljava/util/ArrayList;

    .line 5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/dm$e;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    .line 7
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v2

    .line 8
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public final g()Lcom/pspdfkit/utils/Size;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$e;->b:Lcom/pspdfkit/utils/Size;

    return-object v0
.end method

.method public final h()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dm$e;->h:F

    return v0
.end method

.method public final i()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/dm$e;->j:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "State{pageIndex="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/pspdfkit/internal/dm$e;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", unscaledPageLayoutSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/internal/dm$e;->b:Lcom/pspdfkit/utils/Size;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pageRect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/pspdfkit/internal/dm$e;->c:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

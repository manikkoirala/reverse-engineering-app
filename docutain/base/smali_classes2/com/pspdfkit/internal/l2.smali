.class public final Lcom/pspdfkit/internal/l2;
.super Lcom/pspdfkit/internal/g2;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/g2<",
        "Lcom/pspdfkit/internal/k2;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/qf;Landroid/util/SparseIntArray;Lcom/pspdfkit/internal/k4$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/qf;",
            "Landroid/util/SparseIntArray;",
            "Lcom/pspdfkit/internal/k4$a<",
            "-",
            "Lcom/pspdfkit/internal/k2;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-class v0, Lcom/pspdfkit/internal/k2;

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/pspdfkit/internal/g2;-><init>(Lcom/pspdfkit/internal/qf;Landroid/util/SparseIntArray;Ljava/lang/Class;Lcom/pspdfkit/internal/k4$a;)V

    return-void
.end method


# virtual methods
.method public final c(Lcom/pspdfkit/internal/ja;)Z
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/k2;

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/g2;->c:Lcom/pspdfkit/internal/qf;

    iget v1, p1, Lcom/pspdfkit/internal/zl;->a:I

    invoke-interface {v0, v1}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAnnotations(I)Ljava/util/List;

    move-result-object v0

    .line 3
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(Lcom/pspdfkit/internal/p0;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p1, Lcom/pspdfkit/internal/k2;->c:I

    iget p1, p1, Lcom/pspdfkit/internal/k2;->d:I

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-le v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :catch_0
    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final d(Lcom/pspdfkit/internal/ja;)Z
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/k2;

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/g2;->c:Lcom/pspdfkit/internal/qf;

    iget v1, p1, Lcom/pspdfkit/internal/zl;->a:I

    invoke-interface {v0, v1}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAnnotations(I)Ljava/util/List;

    move-result-object v0

    .line 3
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(Lcom/pspdfkit/internal/p0;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p1, Lcom/pspdfkit/internal/k2;->c:I

    iget p1, p1, Lcom/pspdfkit/internal/k2;->d:I

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-le v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :catch_0
    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected final f(Lcom/pspdfkit/internal/ja;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;
        }
    .end annotation

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/k2;

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/g2;->c:Lcom/pspdfkit/internal/qf;

    iget v1, p1, Lcom/pspdfkit/internal/zl;->a:I

    iget v2, p1, Lcom/pspdfkit/internal/k2;->c:I

    iget p1, p1, Lcom/pspdfkit/internal/k2;->d:I

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0, v1, v2, p1}, Lcom/pspdfkit/internal/r1;->moveAnnotation(III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 4
    :catch_0
    new-instance p1, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;

    const-string v0, "Could not perform redo action on z-index change."

    invoke-direct {p1, v0}, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected final g(Lcom/pspdfkit/internal/ja;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;
        }
    .end annotation

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/k2;

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/g2;->c:Lcom/pspdfkit/internal/qf;

    iget v1, p1, Lcom/pspdfkit/internal/zl;->a:I

    iget v2, p1, Lcom/pspdfkit/internal/k2;->d:I

    iget p1, p1, Lcom/pspdfkit/internal/k2;->c:I

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0, v1, v2, p1}, Lcom/pspdfkit/internal/r1;->moveAnnotation(III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 4
    :catch_0
    new-instance p1, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;

    const-string v0, "Could not perform undo action on z-index change."

    invoke-direct {p1, v0}, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

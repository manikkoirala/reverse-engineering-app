.class public abstract Lcom/pspdfkit/internal/k4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/tu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/k4$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/pspdfkit/internal/ja;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/internal/tu<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/pspdfkit/internal/k4$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/k4$a<",
            "-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$S_BJpBqeEXXPpDiiBggPu3mkpNk(Lcom/pspdfkit/internal/k4;Lcom/pspdfkit/internal/ja;Lcom/pspdfkit/internal/k4$a;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/k4;->a(Lcom/pspdfkit/internal/ja;Lcom/pspdfkit/internal/k4$a;)V

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .line 1
    const-class v0, Lcom/pspdfkit/internal/v5;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/k4;-><init>(Ljava/lang/Class;Lcom/pspdfkit/internal/k4$a;)V

    return-void
.end method

.method protected constructor <init>(Ljava/lang/Class;Lcom/pspdfkit/internal/k4$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Lcom/pspdfkit/internal/k4$a<",
            "-TT;>;)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "editClass"

    .line 3
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/k4;->a:Ljava/lang/Class;

    .line 5
    iput-object p2, p0, Lcom/pspdfkit/internal/k4;->b:Lcom/pspdfkit/internal/k4$a;

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/ja;Lcom/pspdfkit/internal/k4$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 4
    :try_start_0
    invoke-interface {p2, p0, p1}, Lcom/pspdfkit/internal/k4$a;->a(Lcom/pspdfkit/internal/k4;Lcom/pspdfkit/internal/ja;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.UndoRedo"

    const-string v1, "UndoExecutorListener listener threw unexpected exception"

    .line 6
    invoke-static {v0, p1, v1, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private e(Lcom/pspdfkit/internal/ja;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k4;->b:Lcom/pspdfkit/internal/k4$a;

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 5
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/k4$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/k4$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/k4;Lcom/pspdfkit/internal/ja;)V

    .line 6
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "TT;>;"
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/k4;->a:Ljava/lang/Class;

    return-object v0
.end method

.method public final a(Lcom/pspdfkit/internal/ja;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/k4;->f(Lcom/pspdfkit/internal/ja;)V

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/k4;->e(Lcom/pspdfkit/internal/ja;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/ja;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/k4;->g(Lcom/pspdfkit/internal/ja;)V

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/k4;->e(Lcom/pspdfkit/internal/ja;)V

    return-void
.end method

.method protected abstract f(Lcom/pspdfkit/internal/ja;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;
        }
    .end annotation
.end method

.method protected abstract g(Lcom/pspdfkit/internal/ja;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;
        }
    .end annotation
.end method

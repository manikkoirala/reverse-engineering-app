.class public Lcom/pspdfkit/internal/c0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/pspdfkit/annotations/Annotation;


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/c0;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method private constructor <init>(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/c0;->a:Lcom/pspdfkit/annotations/Annotation;

    return-void
.end method

.method public static a(Landroid/content/ClipData;Lcom/pspdfkit/internal/c0;)Lcom/pspdfkit/internal/c0;
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v0

    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/ClipDescription;->hasMimeType(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 3
    invoke-virtual {p0, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object p0

    if-nez p0, :cond_0

    return-object v2

    .line 6
    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/internal/rs;

    if-eqz v0, :cond_1

    .line 8
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/internal/rs;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/rs;->e()Landroid/net/Uri;

    move-result-object v0

    .line 9
    invoke-virtual {p0, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p1

    .line 12
    :cond_1
    new-instance p1, Lcom/pspdfkit/internal/rs;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/rs;-><init>(Landroid/net/Uri;)V

    return-object p1

    .line 13
    :cond_2
    invoke-virtual {p0}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v0

    const-string v3, "text/plain"

    invoke-virtual {v0, v3}, Landroid/content/ClipDescription;->hasMimeType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 15
    invoke-virtual {p0, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object p0

    .line 16
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    return-object v2

    .line 18
    :cond_3
    instance-of v0, p1, Lcom/pspdfkit/internal/lc;

    if-eqz v0, :cond_4

    .line 20
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/internal/lc;

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/lc;->b(Ljava/lang/String;)V

    return-object p1

    .line 24
    :cond_4
    new-instance p1, Lcom/pspdfkit/internal/lc;

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/lc;-><init>(Ljava/lang/String;)V

    return-object p1

    :cond_5
    return-object v2
.end method

.method public static a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/c0;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-nez v0, :cond_2

    .line 29
    instance-of v0, p0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v0, :cond_0

    .line 30
    new-instance v0, Lcom/pspdfkit/internal/lc;

    check-cast p0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/lc;-><init>(Lcom/pspdfkit/annotations/FreeTextAnnotation;)V

    return-object v0

    .line 31
    :cond_0
    instance-of v0, p0, Lcom/pspdfkit/annotations/StampAnnotation;

    if-eqz v0, :cond_1

    .line 32
    new-instance v0, Lcom/pspdfkit/internal/rs;

    check-cast p0, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/rs;-><init>(Lcom/pspdfkit/annotations/StampAnnotation;)V

    return-object v0

    .line 34
    :cond_1
    new-instance v0, Lcom/pspdfkit/internal/c0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/c0;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    return-object v0

    .line 35
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Annotation must be detached from document before it can be added to clipboard!"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public a()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/internal/c0;->a:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method protected final b(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/c0;->a:Lcom/pspdfkit/annotations/Annotation;

    return-void
.end method

.method public b()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/c0;->a:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/c0;->a:Lcom/pspdfkit/annotations/Annotation;

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

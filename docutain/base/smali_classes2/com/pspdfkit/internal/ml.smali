.class public final Lcom/pspdfkit/internal/ml;
.super Lcom/pspdfkit/internal/hj;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ml$e;,
        Lcom/pspdfkit/internal/ml$a;,
        Lcom/pspdfkit/internal/ml$c;,
        Lcom/pspdfkit/internal/ml$b;,
        Lcom/pspdfkit/internal/ml$d;
    }
.end annotation


# instance fields
.field private final d:Landroidx/recyclerview/widget/RecyclerView;

.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final f:Lcom/pspdfkit/internal/ml$b;

.field private final g:Lcom/pspdfkit/internal/ml$c;

.field private final h:Lcom/pspdfkit/internal/ml$a;

.field private final i:Landroid/view/LayoutInflater;

.field private final j:I

.field private k:I

.field private l:Z

.field private m:I

.field private final n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/internal/ml$e;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field private p:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$A2B164_xrUN9IF7qwgjJ1fZ8Mhk(Lcom/pspdfkit/internal/ml;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ml;->c(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$LGwN9QlrHKFBx4FTMBmQZt_IwPE(Lcom/pspdfkit/internal/ml;Ljava/lang/String;Landroidx/core/util/Pair;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ml;->a(Ljava/lang/String;Landroidx/core/util/Pair;)V

    return-void
.end method

.method public static synthetic $r8$lambda$VSw3bNwhmFoeoyAvZqr349xHfd4(Lcom/pspdfkit/internal/ml;Ljava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ml;->b(Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic $r8$lambda$av83tZHQ39QlClOhynSNZ1x9yQw(Lcom/pspdfkit/internal/ml;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ml;->b(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$axQGME_O70qF0Hh2YzNpQ1FYMVQ(Lcom/pspdfkit/internal/ml;Ljava/util/List;)Landroidx/core/util/Pair;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ml;->a(Ljava/util/List;)Landroidx/core/util/Pair;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$ptUSZdAqIh6ARa7iZFSRk8b19G4(Lcom/pspdfkit/internal/ml;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ml;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$uDd2xhRjCau2Oon_0FWl2d9TMQ4(Lcom/pspdfkit/internal/ml;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ml;->a(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Landroidx/recyclerview/widget/RecyclerView;Lcom/pspdfkit/internal/ml$b;Lcom/pspdfkit/internal/ml$c;Lcom/pspdfkit/internal/ml$a;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/OutlineElement;",
            ">;",
            "Landroidx/recyclerview/widget/RecyclerView;",
            "Lcom/pspdfkit/internal/ml$b;",
            "Lcom/pspdfkit/internal/ml$c;",
            "Lcom/pspdfkit/internal/ml$a;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/hj;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/pspdfkit/internal/ml;->m:I

    .line 7
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ml;->o:Z

    const-string v1, "layout_inflater"

    .line 20
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/pspdfkit/internal/ml;->i:Landroid/view/LayoutInflater;

    const/16 v1, 0x10

    .line 21
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/ml;->j:I

    .line 22
    iput-object p3, p0, Lcom/pspdfkit/internal/ml;->d:Landroidx/recyclerview/widget/RecyclerView;

    .line 23
    iput-object p4, p0, Lcom/pspdfkit/internal/ml;->f:Lcom/pspdfkit/internal/ml$b;

    .line 24
    iput-object p5, p0, Lcom/pspdfkit/internal/ml;->g:Lcom/pspdfkit/internal/ml$c;

    .line 25
    iput-object p6, p0, Lcom/pspdfkit/internal/ml;->h:Lcom/pspdfkit/internal/ml$a;

    .line 26
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/pspdfkit/internal/ml;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 27
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ml;->n:Ljava/util/ArrayList;

    .line 28
    invoke-direct {p0, p7, p2}, Lcom/pspdfkit/internal/ml;->a(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)Landroidx/core/util/Pair;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 28
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 31
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 32
    new-instance v4, Lcom/pspdfkit/internal/ml$e;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/document/OutlineElement;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v2, v6, v6}, Lcom/pspdfkit/internal/ml$e;-><init>(Lcom/pspdfkit/document/OutlineElement;ILcom/pspdfkit/internal/ml$e;Lcom/pspdfkit/internal/ml$e-IA;)V

    invoke-direct {p0, v4, v0}, Lcom/pspdfkit/internal/ml;->b(Lcom/pspdfkit/internal/ml$e;Ljava/util/ArrayList;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 36
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ml;->n:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    :goto_1
    if-ltz p1, :cond_6

    .line 37
    iget-object v3, p0, Lcom/pspdfkit/internal/ml;->n:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/ml$e;

    .line 38
    iget-object v4, v3, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    invoke-virtual {v4}, Lcom/pspdfkit/document/OutlineElement;->isExpanded()Z

    move-result v4

    if-nez v4, :cond_5

    .line 39
    invoke-static {v3}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ml$e;)Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 40
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    goto/16 :goto_5

    .line 45
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 47
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 48
    invoke-static {v3}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ml$e;)Ljava/util/List;

    move-result-object v6

    .line 49
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    :goto_2
    if-ltz v6, :cond_2

    .line 50
    invoke-static {v3}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ml$e;)Ljava/util/List;

    move-result-object v7

    .line 51
    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/internal/ml$e;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, -0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    .line 55
    :goto_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_4

    .line 56
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/internal/ml$e;

    .line 57
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    .line 58
    invoke-static {v7}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ml$e;)Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 59
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {v7}, Lcom/pspdfkit/internal/ml$e;->a()Z

    move-result v8

    if-nez v8, :cond_3

    .line 60
    invoke-static {v7}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ml$e;)Ljava/util/List;

    move-result-object v8

    .line 61
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    :goto_4
    if-ltz v8, :cond_3

    .line 62
    invoke-static {v7}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ml$e;)Ljava/util/List;

    move-result-object v9

    .line 63
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/pspdfkit/internal/ml$e;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, -0x1

    goto :goto_4

    .line 67
    :cond_3
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_3

    .line 70
    :cond_4
    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    invoke-static {v3, v6}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fputd(Lcom/pspdfkit/internal/ml$e;I)V

    :cond_5
    :goto_5
    add-int/lit8 p1, p1, -0x1

    goto/16 :goto_1

    .line 72
    :cond_6
    new-instance p1, Landroidx/core/util/Pair;

    invoke-direct {p1, v0, v1}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method private a(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Lcom/pspdfkit/internal/ml$e;)Lcom/pspdfkit/internal/ml$d;
    .locals 8

    .line 89
    check-cast p1, Lcom/pspdfkit/internal/ml$d;

    .line 91
    iget-object v0, p1, Lcom/pspdfkit/internal/ml$d;->b:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    invoke-virtual {v1}, Lcom/pspdfkit/document/OutlineElement;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p1, Lcom/pspdfkit/internal/ml$d;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    invoke-virtual {v1}, Lcom/pspdfkit/document/OutlineElement;->getPageLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v0, p1, Lcom/pspdfkit/internal/ml$d;->b:Landroid/widget/TextView;

    .line 94
    iget-object v1, p2, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    invoke-virtual {v1}, Lcom/pspdfkit/document/OutlineElement;->getColor()I

    move-result v1

    const/high16 v2, -0x1000000

    if-eq v1, v2, :cond_0

    .line 95
    iget-object v1, p2, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    invoke-virtual {v1}, Lcom/pspdfkit/document/OutlineElement;->getColor()I

    move-result v1

    goto :goto_0

    .line 96
    :cond_0
    iget v1, p0, Lcom/pspdfkit/internal/ml;->k:I

    .line 97
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 98
    iget-object v0, p1, Lcom/pspdfkit/internal/ml$d;->b:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    invoke-virtual {v1}, Lcom/pspdfkit/document/OutlineElement;->getStyle()I

    move-result v1

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 100
    iget-object v0, p2, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    invoke-virtual {v0}, Lcom/pspdfkit/document/OutlineElement;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    .line 101
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/Action;->getType()Lcom/pspdfkit/annotations/actions/ActionType;

    move-result-object v0

    sget-object v4, Lcom/pspdfkit/annotations/actions/ActionType;->GOTO:Lcom/pspdfkit/annotations/actions/ActionType;

    if-ne v0, v4, :cond_6

    .line 102
    iget-object v0, p1, Lcom/pspdfkit/internal/ml$d;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 103
    iget-object v0, p1, Lcom/pspdfkit/internal/ml$d;->c:Landroid/widget/TextView;

    iget-object v5, p2, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    .line 104
    invoke-virtual {v5}, Lcom/pspdfkit/document/OutlineElement;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 105
    invoke-virtual {v6}, Lcom/pspdfkit/annotations/actions/Action;->getType()Lcom/pspdfkit/annotations/actions/ActionType;

    move-result-object v7

    if-eq v7, v4, :cond_1

    goto :goto_2

    .line 107
    :cond_1
    invoke-virtual {v5}, Lcom/pspdfkit/document/OutlineElement;->getPageLabel()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/pspdfkit/internal/ml;->l:Z

    if-nez v3, :cond_2

    goto :goto_1

    .line 109
    :cond_2
    invoke-virtual {v5}, Lcom/pspdfkit/document/OutlineElement;->getPageLabel()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 110
    :cond_3
    :goto_1
    check-cast v6, Lcom/pspdfkit/annotations/actions/GoToAction;

    invoke-virtual {v6}, Lcom/pspdfkit/annotations/actions/GoToAction;->getPageIndex()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 111
    :cond_4
    :goto_2
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p1, Lcom/pspdfkit/internal/ml$d;->c:Landroid/widget/TextView;

    .line 113
    iget-object v3, p2, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    invoke-virtual {v3}, Lcom/pspdfkit/document/OutlineElement;->getColor()I

    move-result v3

    if-eq v3, v2, :cond_5

    .line 114
    iget-object v2, p2, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    invoke-virtual {v2}, Lcom/pspdfkit/document/OutlineElement;->getColor()I

    move-result v2

    goto :goto_3

    .line 115
    :cond_5
    iget v2, p0, Lcom/pspdfkit/internal/ml;->k:I

    .line 116
    :goto_3
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_4

    .line 118
    :cond_6
    iget-object v0, p1, Lcom/pspdfkit/internal/ml$d;->c:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 119
    :goto_4
    iget p2, p2, Lcom/pspdfkit/internal/ml$e;->b:I

    if-nez p2, :cond_7

    .line 120
    iget-object p2, p1, Lcom/pspdfkit/internal/ml$d;->d:Landroid/view/View;

    invoke-virtual {p2, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_5

    .line 121
    :cond_7
    iget v0, p0, Lcom/pspdfkit/internal/ml;->j:I

    mul-int v0, v0, p2

    .line 122
    iget-object p2, p1, Lcom/pspdfkit/internal/ml$d;->d:Landroid/view/View;

    invoke-virtual {p2, v0, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    :goto_5
    return-object p1
.end method

.method private synthetic a(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/pspdfkit/internal/ml;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildLayoutPosition(Landroid/view/View;)I

    move-result p1

    if-lez p1, :cond_1

    .line 82
    invoke-virtual {p0}, Lcom/pspdfkit/internal/hj;->getItemCount()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 84
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/hj;->c(I)Lcom/pspdfkit/internal/hj$a;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/hj$a;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    const/high16 p1, 0x43340000    # 180.0f

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 85
    :goto_0
    invoke-virtual {p2, p1}, Landroid/view/View;->setRotation(F)V

    .line 88
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ml;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method private static a(Lcom/pspdfkit/internal/ml$e;Ljava/util/ArrayList;)V
    .locals 2

    if-nez p0, :cond_0

    return-void

    .line 144
    :cond_0
    invoke-static {p0}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fgete(Lcom/pspdfkit/internal/ml$e;)Lcom/pspdfkit/internal/ml$e;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/ml;->a(Lcom/pspdfkit/internal/ml$e;Ljava/util/ArrayList;)V

    .line 145
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 147
    new-instance v0, Lcom/pspdfkit/internal/ml$e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/ml$e;-><init>(Lcom/pspdfkit/internal/ml$e;Lcom/pspdfkit/internal/ml$e-IA;)V

    .line 148
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 150
    :cond_1
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/ml$e;

    const/4 p1, 0x0

    .line 151
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fputd(Lcom/pspdfkit/internal/ml$e;I)V

    :goto_0
    return-void
.end method

.method private synthetic a(Ljava/lang/String;Landroidx/core/util/Pair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 73
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p2, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/Collection;

    iget-object p2, p2, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p2, Ljava/util/HashMap;

    invoke-virtual {p0, v0, p2}, Lcom/pspdfkit/internal/hj;->a(Ljava/util/Collection;Ljava/util/HashMap;)V

    .line 77
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ml;->a(Ljava/lang/String;)V

    .line 80
    iget-object p1, p0, Lcom/pspdfkit/internal/ml;->h:Lcom/pspdfkit/internal/ml$a;

    invoke-interface {p1}, Lcom/pspdfkit/internal/ml$a;->a()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0, p2}, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/ml;Ljava/util/List;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    .line 22
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const/4 v1, 0x5

    .line 23
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    .line 24
    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    .line 25
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/ml;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v7, p1

    const/4 v8, 0x1

    .line 17
    iput-boolean v8, v0, Lcom/pspdfkit/internal/ml;->o:Z

    .line 18
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 19
    iget-object v1, v0, Lcom/pspdfkit/internal/ml;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/pspdfkit/internal/ml$e;

    .line 20
    iget-object v1, v11, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    invoke-virtual {v1}, Lcom/pspdfkit/document/OutlineElement;->getTitle()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    if-nez v12, :cond_1

    goto :goto_4

    .line 21
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v14

    if-nez v14, :cond_2

    goto :goto_2

    .line 26
    :cond_2
    invoke-virtual {v7, v13}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v15

    .line 27
    invoke-virtual {v7, v13}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v6

    .line 29
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v14

    move v5, v1

    :goto_1
    if-ltz v5, :cond_5

    .line 31
    invoke-virtual {v12, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-eq v1, v15, :cond_3

    if-eq v1, v6, :cond_3

    move/from16 v17, v5

    move/from16 v16, v6

    goto :goto_3

    :cond_3
    const/4 v2, 0x1

    const/16 v16, 0x0

    move-object v1, v12

    move v3, v5

    move-object/from16 v4, p1

    move/from16 v17, v5

    move/from16 v5, v16

    move/from16 v16, v6

    move v6, v14

    .line 36
    invoke-virtual/range {v1 .. v6}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v1

    if-eqz v1, :cond_4

    :goto_2
    const/4 v13, 0x1

    goto :goto_4

    :cond_4
    :goto_3
    add-int/lit8 v5, v17, -0x1

    move/from16 v6, v16

    goto :goto_1

    :cond_5
    :goto_4
    if-eqz v13, :cond_0

    .line 37
    invoke-static {v11}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fgete(Lcom/pspdfkit/internal/ml$e;)Lcom/pspdfkit/internal/ml$e;

    move-result-object v1

    invoke-static {v1, v9}, Lcom/pspdfkit/internal/ml;->a(Lcom/pspdfkit/internal/ml$e;Ljava/util/ArrayList;)V

    .line 38
    new-instance v1, Lcom/pspdfkit/internal/ml$e;

    const/4 v2, 0x0

    invoke-direct {v1, v11, v2}, Lcom/pspdfkit/internal/ml$e;-><init>(Lcom/pspdfkit/internal/ml$e;Lcom/pspdfkit/internal/ml$e-IA;)V

    .line 39
    invoke-static {v1}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ml$e;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 40
    invoke-static {v1}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ml$e;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fputd(Lcom/pspdfkit/internal/ml$e;I)V

    .line 41
    :cond_6
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_7
    return-object v9
.end method

.method private synthetic b(Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .line 7
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ml;->o:Z

    if-eqz v0, :cond_0

    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ml;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ml;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildLayoutPosition(Landroid/view/View;)I

    move-result v0

    .line 12
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/hj;->d(I)V

    .line 13
    invoke-static {p2}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    const-wide/16 v1, 0x96

    .line 14
    invoke-virtual {v0, v1, v2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 15
    invoke-virtual {p2}, Landroid/view/View;->getRotation()F

    move-result v1

    const/high16 v2, 0x43340000    # 180.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    const/4 v2, 0x0

    :cond_1
    invoke-virtual {v0, v2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->rotation(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/ml;Landroid/view/View;Landroid/view/View;)V

    .line 16
    invoke-virtual {v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    :cond_2
    return-void
.end method

.method private b(Lcom/pspdfkit/internal/ml$e;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/ml$e;",
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/internal/ml$e;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ml;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/ml$e;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ml$e;)Ljava/util/List;

    move-result-object p1

    .line 4
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ml$e;

    .line 5
    invoke-direct {p0, v0, p2}, Lcom/pspdfkit/internal/ml;->b(Lcom/pspdfkit/internal/ml$e;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private synthetic b(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 42
    invoke-virtual {p0}, Lcom/pspdfkit/internal/hj;->a()V

    .line 43
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/hj;->a(Ljava/util/Collection;)V

    .line 44
    iget-object v0, p0, Lcom/pspdfkit/internal/ml;->g:Lcom/pspdfkit/internal/ml$c;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/ml$c;->a(Z)V

    return-void
.end method

.method private synthetic c(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/internal/ml;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildLayoutPosition(Landroid/view/View;)I

    move-result p1

    const/4 p2, -0x1

    if-eq p1, p2, :cond_0

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/hj;->b(I)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 3
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/hj;->c(I)Lcom/pspdfkit/internal/hj$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/ml$e;

    .line 4
    iget-object p2, p0, Lcom/pspdfkit/internal/ml;->f:Lcom/pspdfkit/internal/ml$b;

    iget-object p1, p1, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    invoke-interface {p2, p1}, Lcom/pspdfkit/internal/ml$b;->a(Lcom/pspdfkit/document/OutlineElement;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/pspdfkit/internal/ml;->p:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 124
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 125
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object p1, p0, Lcom/pspdfkit/internal/ml;->g:Lcom/pspdfkit/internal/ml$c;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/ml$c;->a(Z)V

    return-void

    .line 130
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ml;Ljava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 141
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 142
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ml;)V

    .line 143
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/ml;->p:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 2

    const/4 v0, 0x0

    .line 152
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ml;->o:Z

    .line 153
    iget-object v1, p0, Lcom/pspdfkit/internal/ml;->g:Lcom/pspdfkit/internal/ml$c;

    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/ml$c;->a(Z)V

    .line 154
    invoke-virtual {p0}, Lcom/pspdfkit/internal/hj;->a()V

    .line 155
    iget-object v1, p0, Lcom/pspdfkit/internal/ml;->n:Ljava/util/ArrayList;

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/hj;->a(Ljava/util/Collection;)V

    .line 156
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/hj;->a(Ljava/util/ArrayList;Z)V

    return-void
.end method

.method public final b(Z)V
    .locals 0

    .line 6
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ml;->l:Z

    return-void
.end method

.method public final e(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ml;->k:I

    return-void
.end method

.method public final f(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ml;->m:I

    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/hj;->c(I)Lcom/pspdfkit/internal/hj$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/hj$a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/hj;->c(I)Lcom/pspdfkit/internal/hj$a;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/hj$a;->getChildren()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 3

    .line 1
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ml;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-ne v0, v1, :cond_0

    .line 4
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/hj;->c(I)Lcom/pspdfkit/internal/hj$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/ml$e;

    .line 6
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ml;->a(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Lcom/pspdfkit/internal/ml$e;)Lcom/pspdfkit/internal/ml$d;

    move-result-object p1

    .line 7
    iget-object p1, p1, Lcom/pspdfkit/internal/ml$d;->a:Landroid/widget/ImageView;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 23
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "unknown viewType"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 24
    :cond_1
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/hj;->c(I)Lcom/pspdfkit/internal/hj$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/ml$e;

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ml;->a(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Lcom/pspdfkit/internal/ml$e;)Lcom/pspdfkit/internal/ml$d;

    move-result-object p1

    .line 28
    iget-object v0, p1, Lcom/pspdfkit/internal/ml$d;->a:Landroid/widget/ImageView;

    iget-boolean v2, p0, Lcom/pspdfkit/internal/ml;->o:Z

    xor-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 29
    invoke-virtual {p2}, Lcom/pspdfkit/internal/ml$e;->a()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 30
    iget-object p1, p1, Lcom/pspdfkit/internal/ml$d;->a:Landroid/widget/ImageView;

    const/high16 p2, 0x43340000    # 180.0f

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setRotation(F)V

    goto :goto_0

    .line 32
    :cond_2
    iget-object p1, p1, Lcom/pspdfkit/internal/ml$d;->a:Landroid/widget/ImageView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setRotation(F)V

    :goto_0
    return-void
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    goto :goto_0

    .line 1
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "unknown viewType"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 2
    :cond_1
    :goto_0
    sget p2, Lcom/pspdfkit/R$layout;->pspdf__outline_pager_outline_list_item:I

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ml;->i:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 4
    new-instance p2, Lcom/pspdfkit/internal/ml$d;

    iget v0, p0, Lcom/pspdfkit/internal/ml;->m:I

    invoke-direct {p2, p1, v0}, Lcom/pspdfkit/internal/ml$d;-><init>(Landroid/view/View;I)V

    .line 10
    sget v0, Lcom/pspdfkit/R$id;->pspdf__outline_expand_group:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/ml;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    new-instance v0, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/ml$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/ml;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p2
.end method

.class public final Lcom/pspdfkit/internal/bf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/Date;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/instant/internal/jni/NativeComment;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "nativeComment"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeComment;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/bf;->a:Ljava/lang/String;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeComment;->getAuthorName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/bf;->b:Ljava/lang/String;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeComment;->getContent()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/bf;->c:Ljava/lang/String;

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeComment;->getCreatedAt()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/bf;->d:Ljava/util/Date;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeComment;->getUpdatedAt()Ljava/util/Date;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bf;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bf;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/util/Date;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bf;->d:Ljava/util/Date;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bf;->c:Ljava/lang/String;

    return-object v0
.end method

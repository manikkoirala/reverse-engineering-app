.class public final Lcom/pspdfkit/internal/q2$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/q2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/q2$a$a;,
        Lcom/pspdfkit/internal/q2$a$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/q2$a$b;


# instance fields
.field private final a:Ljava/util/UUID;

.field private final b:Lcom/pspdfkit/internal/cb;

.field private final c:Lcom/pspdfkit/internal/jt;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/q2$a$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/q2$a$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/q2$a;->Companion:Lcom/pspdfkit/internal/q2$a$b;

    return-void
.end method

.method public synthetic constructor <init>(ILjava/util/UUID;Lcom/pspdfkit/internal/cb;Lcom/pspdfkit/internal/jt;)V
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    and-int/lit8 v0, p1, 0x7

    const/4 v1, 0x7

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/q2$a$a;->a:Lcom/pspdfkit/internal/q2$a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/q2$a$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/q2$a;->a:Ljava/util/UUID;

    iput-object p3, p0, Lcom/pspdfkit/internal/q2$a;->b:Lcom/pspdfkit/internal/cb;

    iput-object p4, p0, Lcom/pspdfkit/internal/q2$a;->c:Lcom/pspdfkit/internal/jt;

    return-void
.end method

.method public constructor <init>(Ljava/util/UUID;Lcom/pspdfkit/internal/cb;Lcom/pspdfkit/internal/jt;)V
    .locals 1

    const-string v0, "textBlockId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "externalControlState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formatModifications"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/q2$a;->a:Ljava/util/UUID;

    .line 5
    iput-object p2, p0, Lcom/pspdfkit/internal/q2$a;->b:Lcom/pspdfkit/internal/cb;

    .line 6
    iput-object p3, p0, Lcom/pspdfkit/internal/q2$a;->c:Lcom/pspdfkit/internal/jt;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/q2$a;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/qu;->a:Lcom/pspdfkit/internal/qu;

    iget-object v1, p0, Lcom/pspdfkit/internal/q2$a;->a:Ljava/util/UUID;

    const/4 v2, 0x0

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/cb$a;->a:Lcom/pspdfkit/internal/cb$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/q2$a;->b:Lcom/pspdfkit/internal/cb;

    const/4 v2, 0x1

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/jt$a;->a:Lcom/pspdfkit/internal/jt$a;

    iget-object p0, p0, Lcom/pspdfkit/internal/q2$a;->c:Lcom/pspdfkit/internal/jt;

    const/4 v1, 0x2

    invoke-interface {p1, p2, v1, v0, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/sj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic a:Z = true


# direct methods
.method public static synthetic $r8$lambda$YmpNTeNlVegB5h33E_3Ee-thcJs(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Boolean;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/sj;->a(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Lcom/pspdfkit/LicenseFeature;
    .locals 1

    .line 274
    sget-object v0, Lcom/pspdfkit/internal/sj$a;->i:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 304
    :pswitch_0
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->CONTENT_EDITING:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 305
    :pswitch_1
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->MEASUREMENT_TOOLS:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 306
    :pswitch_2
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->ELECTRONIC_SIGNATURES:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 307
    :pswitch_3
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->READER_VIEW:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 308
    :pswitch_4
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->WEBKIT_HTML_CONVERSION:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 309
    :pswitch_5
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->REDACTION:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 310
    :pswitch_6
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->COMPARISON:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 311
    :pswitch_7
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->FORMS:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 312
    :pswitch_8
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->DOCUMENT_EDITING:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 313
    :pswitch_9
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->IMAGE_DOCUMENT:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 314
    :pswitch_a
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->ANNOTATION_REPLIES:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 315
    :pswitch_b
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->INDEXED_FTS:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 316
    :pswitch_c
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->ANNOTATION_EDITING:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 317
    :pswitch_d
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->DIGITAL_SIGNATURES:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    .line 318
    :pswitch_e
    sget-object p0, Lcom/pspdfkit/LicenseFeature;->PDF_CREATION:Lcom/pspdfkit/LicenseFeature;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/pspdfkit/internal/jni/NativeAnnotationType;)Lcom/pspdfkit/annotations/AnnotationType;
    .locals 1

    .line 105
    const-class v0, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v0, p0}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/AnnotationType;

    return-object p0
.end method

.method public static a(Lcom/pspdfkit/internal/jni/NativeAnnotationReviewSummary;)Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;
    .locals 5

    .line 263
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 265
    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativeAnnotationReviewSummary;->getReviewNames()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 266
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/jni/NativeAuthorState;

    .line 267
    invoke-static {}, Lcom/pspdfkit/annotations/note/AuthorState;->values()[Lcom/pspdfkit/annotations/note/AuthorState;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget-object v3, v4, v3

    .line 268
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 270
    :cond_0
    new-instance v1, Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

    .line 271
    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativeAnnotationReviewSummary;->getCurrentUserState()Lcom/pspdfkit/internal/jni/NativeAuthorState;

    move-result-object p0

    .line 272
    invoke-static {}, Lcom/pspdfkit/annotations/note/AuthorState;->values()[Lcom/pspdfkit/annotations/note/AuthorState;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget-object p0, v2, p0

    .line 273
    invoke-direct {v1, v0, p0}, Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;-><init>(Ljava/util/Map;Lcom/pspdfkit/annotations/note/AuthorState;)V

    return-object v1
.end method

.method public static a(Lcom/pspdfkit/internal/jni/NativePageBinding;)Lcom/pspdfkit/document/PageBinding;
    .locals 3

    .line 447
    sget-object v0, Lcom/pspdfkit/internal/sj$a;->o:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 453
    sget-object p0, Lcom/pspdfkit/document/PageBinding;->RIGHT_EDGE:Lcom/pspdfkit/document/PageBinding;

    return-object p0

    .line 455
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received unknown native page binding: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456
    :cond_1
    sget-object p0, Lcom/pspdfkit/document/PageBinding;->LEFT_EDGE:Lcom/pspdfkit/document/PageBinding;

    return-object p0

    .line 457
    :cond_2
    sget-object p0, Lcom/pspdfkit/document/PageBinding;->UNKNOWN:Lcom/pspdfkit/document/PageBinding;

    return-object p0
.end method

.method public static a(Lcom/pspdfkit/internal/jni/NativePDFVersion;)Lcom/pspdfkit/document/PdfVersion;
    .locals 2

    .line 107
    sget-boolean v0, Lcom/pspdfkit/internal/sj;->a:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativePDFVersion;->getMajorVersion()B

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p0, Ljava/lang/AssertionError;

    invoke-direct {p0}, Ljava/lang/AssertionError;-><init>()V

    throw p0

    .line 108
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativePDFVersion;->getMinorVersion()B

    move-result p0

    packed-switch p0, :pswitch_data_0

    .line 126
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Unrecognised version."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 127
    :pswitch_0
    sget-object p0, Lcom/pspdfkit/document/PdfVersion;->PDF_1_7:Lcom/pspdfkit/document/PdfVersion;

    return-object p0

    .line 128
    :pswitch_1
    sget-object p0, Lcom/pspdfkit/document/PdfVersion;->PDF_1_6:Lcom/pspdfkit/document/PdfVersion;

    return-object p0

    .line 129
    :pswitch_2
    sget-object p0, Lcom/pspdfkit/document/PdfVersion;->PDF_1_5:Lcom/pspdfkit/document/PdfVersion;

    return-object p0

    .line 130
    :pswitch_3
    sget-object p0, Lcom/pspdfkit/document/PdfVersion;->PDF_1_4:Lcom/pspdfkit/document/PdfVersion;

    return-object p0

    .line 131
    :pswitch_4
    sget-object p0, Lcom/pspdfkit/document/PdfVersion;->PDF_1_3:Lcom/pspdfkit/document/PdfVersion;

    return-object p0

    .line 132
    :pswitch_5
    sget-object p0, Lcom/pspdfkit/document/PdfVersion;->PDF_1_2:Lcom/pspdfkit/document/PdfVersion;

    return-object p0

    .line 133
    :pswitch_6
    sget-object p0, Lcom/pspdfkit/document/PdfVersion;->PDF_1_1:Lcom/pspdfkit/document/PdfVersion;

    return-object p0

    .line 134
    :pswitch_7
    sget-object p0, Lcom/pspdfkit/document/PdfVersion;->PDF_1_0:Lcom/pspdfkit/document/PdfVersion;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/pspdfkit/internal/jni/NativeFormType;)Lcom/pspdfkit/forms/FormType;
    .locals 1

    .line 106
    const-class v0, Lcom/pspdfkit/forms/FormType;

    invoke-static {v0, p0}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/forms/FormType;

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;)Lcom/pspdfkit/internal/jni/NativeAnnotationAppearanceStream;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 386
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->isBitmap()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 387
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 389
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object p0

    invoke-static {p0}, Lcom/pspdfkit/internal/jni/NativeImageFactory;->fromDataProvider(Lcom/pspdfkit/document/providers/DataProvider;)Landroid/util/Pair;

    move-result-object p0

    iget-object p0, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeImage;

    .line 390
    new-instance p1, Lcom/pspdfkit/internal/jni/NativeAnnotationAppearanceStream;

    invoke-direct {p1, p0, v1}, Lcom/pspdfkit/internal/jni/NativeAnnotationAppearanceStream;-><init>(Lcom/pspdfkit/internal/jni/NativeImage;Lcom/pspdfkit/internal/jni/NativeDataDescriptor;)V

    return-object p1

    .line 392
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->getUri()Landroid/net/Uri;

    move-result-object p1

    .line 393
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/jni/NativeImageFactory;->fromUri(Landroid/content/Context;Landroid/net/Uri;)Landroid/util/Pair;

    move-result-object p0

    iget-object p0, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast p0, Lcom/pspdfkit/internal/jni/NativeImage;

    .line 394
    new-instance p1, Lcom/pspdfkit/internal/jni/NativeAnnotationAppearanceStream;

    invoke-direct {p1, p0, v1}, Lcom/pspdfkit/internal/jni/NativeAnnotationAppearanceStream;-><init>(Lcom/pspdfkit/internal/jni/NativeImage;Lcom/pspdfkit/internal/jni/NativeDataDescriptor;)V

    return-object p1

    .line 398
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 399
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object p0

    invoke-static {p0, v1}, Lcom/pspdfkit/internal/s7;->createNativeDataDescriptor(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    move-result-object p0

    goto :goto_0

    .line 401
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 403
    new-instance p0, Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/pspdfkit/internal/jni/NativeDataDescriptor;-><init>(Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeDataProvider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 405
    :cond_3
    new-instance p0, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;

    .line 406
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;->getUri()Landroid/net/Uri;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;-><init>(Landroid/net/Uri;)V

    .line 407
    invoke-static {p0, v1}, Lcom/pspdfkit/internal/s7;->createNativeDataDescriptor(Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    move-result-object p0

    .line 411
    :goto_0
    new-instance p1, Lcom/pspdfkit/internal/jni/NativeAnnotationAppearanceStream;

    invoke-direct {p1, v1, p0}, Lcom/pspdfkit/internal/jni/NativeAnnotationAppearanceStream;-><init>(Lcom/pspdfkit/internal/jni/NativeImage;Lcom/pspdfkit/internal/jni/NativeDataDescriptor;)V

    return-object p1
.end method

.method public static a(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/jni/NativeAnnotationPager;
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 8
    :cond_1
    new-instance p0, Lcom/pspdfkit/internal/qj;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/qj;-><init>(Ljava/util/ArrayList;)V

    return-object p0
.end method

.method public static a(Lcom/pspdfkit/signatures/EncryptionAlgorithm;)Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;
    .locals 1

    .line 210
    sget-object v0, Lcom/pspdfkit/internal/sj$a;->g:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    .line 218
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;->UNKNOWN:Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;

    return-object p0

    .line 219
    :cond_0
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;->ECDSA:Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;

    return-object p0

    .line 220
    :cond_1
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;->DSA:Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;

    return-object p0

    .line 221
    :cond_2
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;->RSA:Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;

    return-object p0
.end method

.method public static a(Lcom/pspdfkit/signatures/HashAlgorithm;)Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;
    .locals 1

    if-eqz p0, :cond_0

    .line 171
    sget-object v0, Lcom/pspdfkit/internal/sj$a;->e:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 185
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;->UNKNOWN:Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;

    return-object p0

    .line 186
    :pswitch_0
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;->SHA512:Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;

    return-object p0

    .line 187
    :pswitch_1
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;->SHA384:Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;

    return-object p0

    .line 188
    :pswitch_2
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;->SHA256:Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;

    return-object p0

    .line 189
    :pswitch_3
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;->SHA224:Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;

    return-object p0

    .line 190
    :pswitch_4
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;->SHA160:Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;

    return-object p0

    .line 191
    :pswitch_5
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;->MD5:Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/graphics/Bitmap$CompressFormat;)Lcom/pspdfkit/internal/jni/NativeImageEncoding;
    .locals 3

    .line 375
    sget-object v0, Lcom/pspdfkit/internal/sj$a;->n:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 381
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeImageEncoding;->WEBP:Lcom/pspdfkit/internal/jni/NativeImageEncoding;

    return-object p0

    .line 383
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown compression format:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384
    :cond_1
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeImageEncoding;->PNG:Lcom/pspdfkit/internal/jni/NativeImageEncoding;

    return-object p0

    .line 385
    :cond_2
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeImageEncoding;->JPEG:Lcom/pspdfkit/internal/jni/NativeImageEncoding;

    return-object p0
.end method

.method public static a(Lcom/pspdfkit/document/processor/ocr/OcrLanguage;)Lcom/pspdfkit/internal/jni/NativeOcrLanguage;
    .locals 3

    .line 39
    sget-object v0, Lcom/pspdfkit/internal/sj$a;->c:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "OCR language "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " is not supported on Android"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :pswitch_0
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->WELSH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 85
    :pswitch_1
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->TURKISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 86
    :pswitch_2
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->SWEDISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 87
    :pswitch_3
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->SPANISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 88
    :pswitch_4
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->SLOVENIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 89
    :pswitch_5
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->SLOVAK:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 90
    :pswitch_6
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->SERBIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 91
    :pswitch_7
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->PORTUGUESE:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 92
    :pswitch_8
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->POLISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 93
    :pswitch_9
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->NORWEGIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 94
    :pswitch_a
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->MALAY:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 95
    :pswitch_b
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->ITALIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 96
    :pswitch_c
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->INDONESIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 97
    :pswitch_d
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->GERMAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 98
    :pswitch_e
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->FRENCH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 99
    :pswitch_f
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->FINNISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 100
    :pswitch_10
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->ENGLISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 101
    :pswitch_11
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->DUTCH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 102
    :pswitch_12
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->DANISH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 103
    :pswitch_13
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->CZECH:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    .line 104
    :pswitch_14
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeOcrLanguage;->CROATIAN:Lcom/pspdfkit/internal/jni/NativeOcrLanguage;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/pspdfkit/document/PdfBox;)Lcom/pspdfkit/internal/jni/NativePDFBoxType;
    .locals 3

    .line 24
    sget-object v0, Lcom/pspdfkit/internal/sj$a;->b:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 35
    sget-object p0, Lcom/pspdfkit/internal/jni/NativePDFBoxType;->TRIMBOX:Lcom/pspdfkit/internal/jni/NativePDFBoxType;

    return-object p0

    .line 34
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Pdf box type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " is not supported on Android"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_1
    sget-object p0, Lcom/pspdfkit/internal/jni/NativePDFBoxType;->BLEEDBOX:Lcom/pspdfkit/internal/jni/NativePDFBoxType;

    return-object p0

    .line 37
    :cond_2
    sget-object p0, Lcom/pspdfkit/internal/jni/NativePDFBoxType;->MEDIABOX:Lcom/pspdfkit/internal/jni/NativePDFBoxType;

    return-object p0

    .line 38
    :cond_3
    sget-object p0, Lcom/pspdfkit/internal/jni/NativePDFBoxType;->CROPBOX:Lcom/pspdfkit/internal/jni/NativePDFBoxType;

    return-object p0
.end method

.method public static a(Lcom/pspdfkit/document/PageBinding;)Lcom/pspdfkit/internal/jni/NativePageBinding;
    .locals 3

    .line 458
    sget-object v0, Lcom/pspdfkit/internal/sj$a;->p:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 464
    sget-object p0, Lcom/pspdfkit/internal/jni/NativePageBinding;->RIGHTEDGE:Lcom/pspdfkit/internal/jni/NativePageBinding;

    return-object p0

    .line 466
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received unknown page binding: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 467
    :cond_1
    sget-object p0, Lcom/pspdfkit/internal/jni/NativePageBinding;->LEFTEDGE:Lcom/pspdfkit/internal/jni/NativePageBinding;

    return-object p0

    .line 468
    :cond_2
    sget-object p0, Lcom/pspdfkit/internal/jni/NativePageBinding;->UNKNOWN:Lcom/pspdfkit/internal/jni/NativePageBinding;

    return-object p0
.end method

.method public static a(Lcom/pspdfkit/internal/g4;Ljava/util/EnumSet;)Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/g4;",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)",
            "Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 319
    iget-object v1, v0, Lcom/pspdfkit/internal/g4;->m:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    move-object v8, v1

    goto :goto_1

    .line 320
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 321
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/AnnotationType;

    .line 322
    const-class v4, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    invoke-static {v4, v3}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 323
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v8, v2

    .line 324
    :goto_1
    sget-boolean v1, Lcom/pspdfkit/internal/sj;->a:Z

    if-nez v1, :cond_3

    if-eqz v8, :cond_2

    goto :goto_2

    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 325
    :cond_3
    :goto_2
    invoke-virtual/range {p1 .. p1}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/AnnotationType;

    .line 326
    const-class v3, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    invoke-static {v3, v2}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    .line 327
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 328
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    const/4 v1, 0x0

    .line 334
    instance-of v2, v0, Lcom/pspdfkit/internal/l8;

    if-nez v2, :cond_6

    .line 335
    iget-object v1, v0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    iget v2, v0, Lcom/pspdfkit/internal/g4;->d:I

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->b(I)I

    move-result v1

    int-to-byte v1, v1

    move v9, v1

    goto :goto_4

    :cond_6
    const/4 v9, 0x0

    .line 338
    :goto_4
    sget-object v1, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->RENDER_ANNOTATIONS:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    sget-object v2, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->RENDER_TEXT_NATIVE:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    sget-object v3, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->USE_CLEAR_TYPE_AA:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    sget-object v4, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->REVERSE_BYTE_ORDER:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    invoke-static {v1, v2, v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v10

    .line 343
    iget-boolean v1, v0, Lcom/pspdfkit/internal/g4;->p:Z

    if-eqz v1, :cond_7

    sget-object v1, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->RENDER_GRAYSCALE:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    invoke-virtual {v10, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 344
    :cond_7
    iget-boolean v1, v0, Lcom/pspdfkit/internal/g4;->o:Z

    if-eqz v1, :cond_8

    sget-object v1, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->RENDER_INVERTED_COLORS:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    invoke-virtual {v10, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 345
    :cond_8
    iget v1, v0, Lcom/pspdfkit/internal/g4;->g:I

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    const/16 v2, 0xff

    if-ge v1, v2, :cond_9

    sget-object v1, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->PREMULTIPLY_ALPHA:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    invoke-virtual {v10, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 346
    :cond_9
    iget-boolean v1, v0, Lcom/pspdfkit/internal/g4;->q:Z

    if-eqz v1, :cond_a

    sget-object v1, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->DRAW_REDACT_AS_REDACTED:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    invoke-virtual {v10, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 347
    :cond_a
    iget-boolean v1, v0, Lcom/pspdfkit/internal/g4;->s:Z

    if-eqz v1, :cond_b

    sget-object v1, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->DONT_RENDER_TEXT_OBJECTS:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    invoke-virtual {v10, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 348
    :cond_b
    iget-object v1, v0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->o()Z

    move-result v1

    if-nez v1, :cond_c

    .line 349
    sget-object v1, Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;->RENDER_ON_ORIGINAL_DOCUMENT:Lcom/pspdfkit/internal/jni/NativePageRenderingFlags;

    invoke-virtual {v10, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 350
    :cond_c
    new-instance v1, Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;

    iget v2, v0, Lcom/pspdfkit/internal/g4;->g:I

    .line 351
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 352
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeFormRenderingConfig;

    iget-object v12, v0, Lcom/pspdfkit/internal/g4;->h:Ljava/lang/Integer;

    iget-object v13, v0, Lcom/pspdfkit/internal/g4;->j:Ljava/lang/Integer;

    iget-object v14, v0, Lcom/pspdfkit/internal/g4;->k:Ljava/lang/Integer;

    iget-object v15, v0, Lcom/pspdfkit/internal/g4;->i:Ljava/lang/Integer;

    iget-boolean v2, v0, Lcom/pspdfkit/internal/g4;->r:Z

    move-object v11, v6

    move/from16 v16, v2

    invoke-direct/range {v11 .. v16}, Lcom/pspdfkit/internal/jni/NativeFormRenderingConfig;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Z)V

    .line 353
    iget-object v7, v0, Lcom/pspdfkit/internal/g4;->l:Ljava/util/ArrayList;

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;-><init>(Ljava/lang/Integer;Lcom/pspdfkit/internal/jni/NativeFormRenderingConfig;Ljava/util/ArrayList;Ljava/util/ArrayList;BLjava/util/EnumSet;)V

    return-object v1
.end method

.method public static a(Ljava/security/KeyStore$PrivateKeyEntry;)Lcom/pspdfkit/internal/jni/NativePrivateKey;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateEncodingException;
        }
    .end annotation

    .line 231
    invoke-virtual {p0}, Ljava/security/KeyStore$PrivateKeyEntry;->getPrivateKey()Ljava/security/PrivateKey;

    move-result-object p0

    .line 233
    invoke-interface {p0}, Ljava/security/Key;->getFormat()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "PKCS#1"

    .line 239
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "PKCS#8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 242
    invoke-interface {p0}, Ljava/security/Key;->getEncoded()[B

    move-result-object p0

    sget-object v0, Lcom/pspdfkit/internal/jni/NativePrivateKeyEncoding;->PKCS8:Lcom/pspdfkit/internal/jni/NativePrivateKeyEncoding;

    .line 243
    invoke-static {p0, v0}, Lcom/pspdfkit/internal/jni/NativePrivateKey;->createFromRawPrivateKey([BLcom/pspdfkit/internal/jni/NativePrivateKeyEncoding;)Lcom/pspdfkit/internal/jni/NativePrivateKey;

    move-result-object p0

    goto :goto_0

    .line 251
    :cond_0
    new-instance p0, Ljava/security/cert/CertificateEncodingException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid private key format: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " Only PKCS#8 and PKCS#1 are supported!"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/security/cert/CertificateEncodingException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 252
    :cond_1
    invoke-interface {p0}, Ljava/security/Key;->getEncoded()[B

    move-result-object p0

    sget-object v0, Lcom/pspdfkit/internal/jni/NativePrivateKeyEncoding;->PKCS1:Lcom/pspdfkit/internal/jni/NativePrivateKeyEncoding;

    .line 253
    invoke-static {p0, v0}, Lcom/pspdfkit/internal/jni/NativePrivateKey;->createFromRawPrivateKey([BLcom/pspdfkit/internal/jni/NativePrivateKeyEncoding;)Lcom/pspdfkit/internal/jni/NativePrivateKey;

    move-result-object p0

    :goto_0
    if-eqz p0, :cond_2

    return-object p0

    .line 261
    :cond_2
    new-instance p0, Ljava/security/cert/CertificateEncodingException;

    const-string v0, "Could not decode private key."

    invoke-direct {p0, v0}, Ljava/security/cert/CertificateEncodingException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 262
    :cond_3
    new-instance p0, Ljava/security/cert/CertificateEncodingException;

    const-string v0, "Could not get the private key format from the key pair. This is most likely due to using the Android KeyStore instead of the PSPDFKit `SignatureManager` to add the key to the system."

    invoke-direct {p0, v0}, Ljava/security/cert/CertificateEncodingException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)Lcom/pspdfkit/internal/jni/NativeProcessOperation;
    .locals 3

    .line 9
    sget-object v0, Lcom/pspdfkit/internal/sj$a;->a:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 20
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeProcessOperation;->PRINT:Lcom/pspdfkit/internal/jni/NativeProcessOperation;

    return-object p0

    .line 19
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Annotation processing mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " is not supported on Android"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_1
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeProcessOperation;->REMOVE:Lcom/pspdfkit/internal/jni/NativeProcessOperation;

    return-object p0

    .line 22
    :cond_2
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeProcessOperation;->FLATTEN:Lcom/pspdfkit/internal/jni/NativeProcessOperation;

    return-object p0

    .line 23
    :cond_3
    sget-object p0, Lcom/pspdfkit/internal/jni/NativeProcessOperation;->EMBED:Lcom/pspdfkit/internal/jni/NativeProcessOperation;

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lcom/pspdfkit/signatures/SignatureAppearance;)Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 412
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->getSignatureAppearanceMode()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureAppearanceMode;

    move-result-object v0

    .line 413
    sget-object v1, Lcom/pspdfkit/internal/sj$a;->m:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 417
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeSignatureAppearanceMode;->SIGNATURE_AND_DESCRIPTION:Lcom/pspdfkit/internal/jni/NativeSignatureAppearanceMode;

    goto :goto_0

    .line 419
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string v1, "Passed in unknown option: "

    invoke-direct {p1, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 420
    :cond_1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeSignatureAppearanceMode;->DESCRIPTION_ONLY:Lcom/pspdfkit/internal/jni/NativeSignatureAppearanceMode;

    .line 421
    :goto_0
    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;->create(Lcom/pspdfkit/internal/jni/NativeSignatureAppearanceMode;)Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;

    move-result-object v0

    .line 423
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->reuseExistingSignatureAppearanceStream()Z

    move-result v1

    .line 424
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;->setReuseExistingSignatureAppearanceStream(Z)V

    .line 426
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->showSignatureLocation()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;->setShowSignatureLocation(Z)V

    .line 427
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->showSignatureReason()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;->setShowSignatureReason(Z)V

    .line 428
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->showSignDate()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;->setShowSignDate(Z)V

    .line 429
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->showSignerName()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;->setShowSignerName(Z)V

    .line 430
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->getSignatureGraphic()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 433
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->getSignatureGraphic()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    move-result-object v1

    .line 434
    invoke-static {p0, v1}, Lcom/pspdfkit/internal/sj;->a(Landroid/content/Context;Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;)Lcom/pspdfkit/internal/jni/NativeAnnotationAppearanceStream;

    move-result-object v1

    .line 436
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;->setSignatureGraphic(Lcom/pspdfkit/internal/jni/NativeAnnotationAppearanceStream;)V

    .line 438
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->getSignatureWatermark()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 441
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->getSignatureWatermark()Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;

    move-result-object v1

    .line 442
    invoke-static {p0, v1}, Lcom/pspdfkit/internal/sj;->a(Landroid/content/Context;Lcom/pspdfkit/signatures/SignatureAppearance$SignatureGraphic;)Lcom/pspdfkit/internal/jni/NativeAnnotationAppearanceStream;

    move-result-object p0

    .line 444
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;->setSignatureWatermark(Lcom/pspdfkit/internal/jni/NativeAnnotationAppearanceStream;)V

    .line 446
    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/SignatureAppearance;->showWatermark()Z

    move-result p0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/jni/NativeSignatureAppearance;->setShowWatermark(Z)V

    return-object v0
.end method

.method public static a(Lcom/pspdfkit/signatures/BiometricSignatureData;)Lcom/pspdfkit/internal/jni/NativeSignatureBiometricProperties;
    .locals 9

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 135
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/BiometricSignatureData;->getPressurePoints()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    if-nez v1, :cond_1

    move-object v1, v0

    goto :goto_0

    .line 136
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-gt v6, v5, :cond_2

    goto :goto_0

    .line 137
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 138
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    div-int/2addr v7, v4

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    sub-int/2addr v7, v3

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v6

    .line 141
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/BiometricSignatureData;->getTimePoints()Ljava/util/List;

    move-result-object v6

    if-nez v6, :cond_3

    move-object v6, v0

    goto :goto_1

    .line 142
    :cond_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-gt v7, v5, :cond_4

    goto :goto_1

    .line 143
    :cond_4
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 144
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    div-int/2addr v2, v4

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v3

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v6, v7

    :goto_1
    if-nez v6, :cond_5

    move-object v2, v0

    goto :goto_3

    .line 147
    :cond_5
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    invoke-direct {v2, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 148
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    .line 149
    invoke-virtual {v7}, Ljava/lang/Long;->floatValue()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 150
    :cond_6
    :goto_3
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeSignatureBiometricProperties;

    if-nez v1, :cond_7

    move-object v1, v0

    goto :goto_4

    .line 151
    :cond_7
    instance-of v7, v1, Ljava/util/ArrayList;

    if-eqz v7, :cond_8

    check-cast v1, Ljava/util/ArrayList;

    goto :goto_4

    .line 152
    :cond_8
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v1, v7

    :goto_4
    if-nez v2, :cond_9

    move-object v2, v0

    .line 153
    :cond_9
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/BiometricSignatureData;->getTouchRadius()Ljava/lang/Float;

    move-result-object v7

    .line 154
    invoke-virtual {p0}, Lcom/pspdfkit/signatures/BiometricSignatureData;->getInputMethod()Lcom/pspdfkit/signatures/BiometricSignatureData$InputMethod;

    move-result-object p0

    if-eqz p0, :cond_e

    .line 155
    sget-object v8, Lcom/pspdfkit/internal/sj$a;->d:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget p0, v8, p0

    if-eq p0, v3, :cond_d

    if-eq p0, v4, :cond_c

    if-eq p0, v5, :cond_b

    const/4 v3, 0x4

    if-eq p0, v3, :cond_a

    goto :goto_5

    .line 163
    :cond_a
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeSignatureInputMethod;->APPLEPENCIL:Lcom/pspdfkit/internal/jni/NativeSignatureInputMethod;

    goto :goto_5

    .line 164
    :cond_b
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeSignatureInputMethod;->MOUSE:Lcom/pspdfkit/internal/jni/NativeSignatureInputMethod;

    goto :goto_5

    .line 165
    :cond_c
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeSignatureInputMethod;->THIRDPARTYSTYLUS:Lcom/pspdfkit/internal/jni/NativeSignatureInputMethod;

    goto :goto_5

    .line 166
    :cond_d
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeSignatureInputMethod;->FINGER:Lcom/pspdfkit/internal/jni/NativeSignatureInputMethod;

    .line 167
    :cond_e
    :goto_5
    invoke-direct {v6, v1, v2, v7, v0}, Lcom/pspdfkit/internal/jni/NativeSignatureBiometricProperties;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Float;Lcom/pspdfkit/internal/jni/NativeSignatureInputMethod;)V

    return-object v6
.end method

.method public static a(Lcom/pspdfkit/internal/jni/NativeEncryptionAlgorithm;)Lcom/pspdfkit/signatures/EncryptionAlgorithm;
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    .line 222
    sget-object v1, Lcom/pspdfkit/internal/sj$a;->h:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget p0, v1, p0

    const/4 v1, 0x1

    if-eq p0, v1, :cond_2

    const/4 v1, 0x2

    if-eq p0, v1, :cond_1

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    return-object v0

    .line 228
    :cond_0
    sget-object p0, Lcom/pspdfkit/signatures/EncryptionAlgorithm;->ECDSA:Lcom/pspdfkit/signatures/EncryptionAlgorithm;

    return-object p0

    .line 229
    :cond_1
    sget-object p0, Lcom/pspdfkit/signatures/EncryptionAlgorithm;->DSA:Lcom/pspdfkit/signatures/EncryptionAlgorithm;

    return-object p0

    .line 230
    :cond_2
    sget-object p0, Lcom/pspdfkit/signatures/EncryptionAlgorithm;->RSA:Lcom/pspdfkit/signatures/EncryptionAlgorithm;

    return-object p0

    :cond_3
    return-object v0
.end method

.method public static a(Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;)Lcom/pspdfkit/signatures/HashAlgorithm;
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 192
    sget-object v1, Lcom/pspdfkit/internal/sj$a;->f:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget p0, v1, p0

    packed-switch p0, :pswitch_data_0

    return-object v0

    .line 204
    :pswitch_0
    sget-object p0, Lcom/pspdfkit/signatures/HashAlgorithm;->SHA512:Lcom/pspdfkit/signatures/HashAlgorithm;

    return-object p0

    .line 205
    :pswitch_1
    sget-object p0, Lcom/pspdfkit/signatures/HashAlgorithm;->SHA384:Lcom/pspdfkit/signatures/HashAlgorithm;

    return-object p0

    .line 206
    :pswitch_2
    sget-object p0, Lcom/pspdfkit/signatures/HashAlgorithm;->SHA256:Lcom/pspdfkit/signatures/HashAlgorithm;

    return-object p0

    .line 207
    :pswitch_3
    sget-object p0, Lcom/pspdfkit/signatures/HashAlgorithm;->SHA224:Lcom/pspdfkit/signatures/HashAlgorithm;

    return-object p0

    .line 208
    :pswitch_4
    sget-object p0, Lcom/pspdfkit/signatures/HashAlgorithm;->SHA160:Lcom/pspdfkit/signatures/HashAlgorithm;

    return-object p0

    .line 209
    :pswitch_5
    sget-object p0, Lcom/pspdfkit/signatures/HashAlgorithm;->MD5:Lcom/pspdfkit/signatures/HashAlgorithm;

    return-object p0

    :cond_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static synthetic a(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 168
    invoke-virtual {p0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/Enum;

    array-length p0, p0

    .line 169
    invoke-virtual {p1}, Ljava/lang/Enum;->getDeclaringClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/Enum;

    array-length p1, p1

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 170
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/EnumSet;)Ljava/util/EnumSet;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/document/search/CompareOptions;",
            ">;)",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeCompareOptionsFlags;",
            ">;"
        }
    .end annotation

    .line 354
    const-class v0, Lcom/pspdfkit/internal/jni/NativeCompareOptionsFlags;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 355
    invoke-virtual {p0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/document/search/CompareOptions;

    .line 356
    sget-object v2, Lcom/pspdfkit/internal/sj$a;->l:[I

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x1

    if-eq v2, v3, :cond_3

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 371
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeCompareOptionsFlags;->REGULAR_EXPRESSION:Lcom/pspdfkit/internal/jni/NativeCompareOptionsFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 370
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled compare option for "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 372
    :cond_1
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeCompareOptionsFlags;->SMART_SEARCH:Lcom/pspdfkit/internal/jni/NativeCompareOptionsFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 373
    :cond_2
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeCompareOptionsFlags;->DIACRITIC_INSENSITIVE:Lcom/pspdfkit/internal/jni/NativeCompareOptionsFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 374
    :cond_3
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeCompareOptionsFlags;->CASE_INSENSITIVE:Lcom/pspdfkit/internal/jni/NativeCompareOptionsFlags;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    return-object v0
.end method

.method public static b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 2

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/sj$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/sj$$ExternalSyntheticLambda0;-><init>(Ljava/lang/Class;Ljava/lang/Enum;)V

    const-string v1, "condition"

    .line 6
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-virtual {p0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/Enum;

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget-object p0, p0, p1

    return-object p0
.end method

.method public static b(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 6

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 9
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/jni/NativeEditingChange;

    .line 10
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeEditingChange;->getOperation()Lcom/pspdfkit/internal/jni/NativeEditingOperation;

    move-result-object v2

    .line 11
    sget-object v3, Lcom/pspdfkit/internal/sj$a;->k:[I

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget v3, v3, v4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-ne v3, v4, :cond_0

    .line 24
    sget-object v2, Lcom/pspdfkit/undo/EditingOperation;->INSERTREFERENCE:Lcom/pspdfkit/undo/EditingOperation;

    goto :goto_1

    .line 23
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unhandled state for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 25
    :cond_1
    sget-object v2, Lcom/pspdfkit/undo/EditingOperation;->ROTATE:Lcom/pspdfkit/undo/EditingOperation;

    goto :goto_1

    .line 26
    :cond_2
    sget-object v2, Lcom/pspdfkit/undo/EditingOperation;->INSERT:Lcom/pspdfkit/undo/EditingOperation;

    goto :goto_1

    .line 27
    :cond_3
    sget-object v2, Lcom/pspdfkit/undo/EditingOperation;->MOVE:Lcom/pspdfkit/undo/EditingOperation;

    goto :goto_1

    .line 28
    :cond_4
    sget-object v2, Lcom/pspdfkit/undo/EditingOperation;->REMOVE:Lcom/pspdfkit/undo/EditingOperation;

    .line 29
    :goto_1
    new-instance v3, Lcom/pspdfkit/undo/EditingChange;

    .line 31
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeEditingChange;->getAffectedPageIndex()I

    move-result v4

    .line 32
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeEditingChange;->getPageIndexDestination()I

    move-result v5

    .line 33
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeEditingChange;->getPageReferenceSourceIndex()I

    move-result v1

    invoke-direct {v3, v2, v4, v5, v1}, Lcom/pspdfkit/undo/EditingChange;-><init>(Lcom/pspdfkit/undo/EditingOperation;III)V

    .line 34
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    return-object v0
.end method

.method public static b(Ljava/util/EnumSet;)Ljava/util/EnumSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeAPStreamGenerationOptions;",
            ">;)",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator$AppearanceStreamGenerationOptions;",
            ">;"
        }
    .end annotation

    .line 1
    const-class v0, Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator$AppearanceStreamGenerationOptions;

    .line 2
    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 3
    invoke-virtual {p0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Enum;

    .line 4
    invoke-static {v0, v2}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static c(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/internal/jni/NativeRectDescriptor;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 4
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/jni/NativeRectDescriptor;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeRectDescriptor;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static c(Ljava/util/EnumSet;)Ljava/util/EnumSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeDocumentPermissions;",
            ">;)",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/document/DocumentPermissions;",
            ">;"
        }
    .end annotation

    .line 5
    const-class v0, Lcom/pspdfkit/document/DocumentPermissions;

    .line 6
    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 7
    invoke-virtual {p0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Enum;

    .line 8
    invoke-static {v0, v2}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static d(Ljava/util/EnumSet;)Ljava/util/EnumSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/document/DocumentPermissions;",
            ">;)",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/jni/NativeDocumentPermissions;",
            ">;"
        }
    .end annotation

    .line 1
    const-class v0, Lcom/pspdfkit/internal/jni/NativeDocumentPermissions;

    .line 2
    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 3
    invoke-virtual {p0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Enum;

    .line 4
    invoke-static {v0, v2}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.class public final Lcom/pspdfkit/internal/nf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroidx/fragment/app/FragmentManager;

.field private b:Lcom/pspdfkit/internal/lf$c;

.field private c:Lcom/pspdfkit/internal/lf;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/FragmentManager;)V
    .locals 1

    const-string v0, "fragmentManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/pspdfkit/internal/nf;->a:Landroidx/fragment/app/FragmentManager;

    const-string v0, "com.pspdfkit.internal.document.image.IntentChooserImagePickerFragment.FRAGMENT_TAG"

    .line 23
    invoke-virtual {p1, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/lf;

    .line 24
    iput-object p1, p0, Lcom/pspdfkit/internal/nf;->c:Lcom/pspdfkit/internal/lf;

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/lf$c;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/nf;->b:Lcom/pspdfkit/internal/lf$c;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/nf;->c:Lcom/pspdfkit/internal/lf;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/lf;->a(Lcom/pspdfkit/internal/lf$c;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    const-string v0, "chooserTitle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/nf;->c:Lcom/pspdfkit/internal/lf;

    const-string v1, "com.pspdfkit.internal.document.image.IntentChooserImagePickerFragment.FRAGMENT_TAG"

    if-nez v0, :cond_1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/nf;->a:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/lf;

    if-nez v0, :cond_0

    .line 7
    new-instance v0, Lcom/pspdfkit/internal/lf;

    invoke-direct {v0}, Lcom/pspdfkit/internal/lf;-><init>()V

    .line 8
    :cond_0
    iput-object v0, p0, Lcom/pspdfkit/internal/nf;->c:Lcom/pspdfkit/internal/lf;

    .line 14
    :cond_1
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/lf;->a(Ljava/lang/String;)V

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/nf;->b:Lcom/pspdfkit/internal/lf$c;

    if-eqz p1, :cond_2

    .line 16
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/lf;->a(Lcom/pspdfkit/internal/lf$c;)V

    .line 19
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/nf;->a:Landroidx/fragment/app/FragmentManager;

    .line 20
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/kc;->a(Landroidx/fragment/app/FragmentManager;Landroidx/fragment/app/Fragment;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 29
    iget-object p1, p0, Lcom/pspdfkit/internal/nf;->a:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->executePendingTransactions()Z

    .line 31
    :cond_3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/lf;->b()Z

    return-void
.end method

.class public final Lcom/pspdfkit/internal/gi;
.super Lcom/pspdfkit/internal/e4;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/e4<",
        "Lcom/pspdfkit/internal/on;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 1

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "toolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/e4;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/16 v0, 0x12

    return v0
.end method

.method public final e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_POLYGON:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method public final t()Lcom/pspdfkit/internal/c4;
    .locals 7

    .line 1
    new-instance v6, Lcom/pspdfkit/internal/pn;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getColor()I

    move-result v1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFillColor()I

    move-result v2

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getThickness()F

    move-result v3

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getAlpha()F

    move-result v4

    .line 6
    sget-object v5, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->SOLID:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-object v0, v6

    .line 7
    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/pn;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    .line 16
    new-instance v0, Lcom/pspdfkit/internal/ri;

    .line 17
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v1

    .line 18
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFloatPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v2

    .line 19
    sget-object v3, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->AREA:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    .line 20
    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/internal/ri;-><init>(Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;Lcom/pspdfkit/annotations/measurements/MeasurementMode;)V

    .line 21
    invoke-virtual {v6, v0}, Lcom/pspdfkit/internal/h4;->a(Lcom/pspdfkit/internal/ri;)V

    .line 28
    new-instance v0, Lcom/pspdfkit/internal/on;

    invoke-direct {v0, v6}, Lcom/pspdfkit/internal/on;-><init>(Lcom/pspdfkit/internal/pn;)V

    return-object v0
.end method
